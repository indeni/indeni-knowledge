import unittest
import automation.workflows.cross_vendor_config_change_on_standby.paloaltonetworks.cross_vendor_config_change_on_standby_mock_data as mock
import automation.workflows.cross_vendor_config_change_on_standby.paloaltonetworks.cross_vendor_config_change_on_standby_parser as parser


class StandbyConfigChangeReasonTests(unittest.TestCase):

    def test_check_member_active_when_local_active(self):
        data = mock.CHECK_LOCAL_ACTIVE_PAIR_1
        self.assertEqual(parser.check_local_active(data[0]), data[1])

    def test_check_member_active_when_local_not_active(self):
        data = mock.CHECK_LOCAL_ACTIVE_PAIR_2
        self.assertEqual(parser.check_local_active(data[0]), data[1])

    def test_get_num_of_lines_that_differ(self):
        data = mock.CHECK_DIFF_PAIR_1
        self.assertEqual(parser.get_num_of_lines_that_differ(data[0]), data[1])
        data = mock.CHECK_DIFF_PAIR_2
        self.assertEqual(parser.get_num_of_lines_that_differ(data[0]), data[1])


if __name__ == '__main__':
    unittest.main()
