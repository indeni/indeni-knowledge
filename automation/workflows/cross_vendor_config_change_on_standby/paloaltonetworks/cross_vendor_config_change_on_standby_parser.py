from re import findall, MULTILINE
from parser_service.public.helper_methods import parse_data_as_xml


def check_variant_matches(**kwargs) -> bool:
    # This should work on all gateways in a cluster
    tags = kwargs.get('my_device_tags', None)
    if tags is None:
        return False
    if tags.get('product') != 'firewall':
        return False
    if not tags.get('cluster-id'):
        return False
    return True


def get_num_of_lines_that_differ(raw_data):
    res = parse_data_as_xml(raw_data)
    if res:
        diffs = res['response']['result']['device-diffs']
        if diffs is None:
            return 0
        return len(diffs)
    return 0


def check_local_active(raw_data):
    res = parse_data_as_xml(raw_data)
    if res:
        return res['response']['result']['group']['local-info']['state'] == 'active'
    return
