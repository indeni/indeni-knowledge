CHECK_LOCAL_ACTIVE_PAIR_1 = ("""
   <response status="success">
      <result>
         <enabled>yes</enabled>
         <group>
            <mode>Active-Passive</mode>
            <local-info>
               <url-compat>Match</url-compat>
               <app-version>7999-0000</app-version>
               <gpclient-version>Not Installed</gpclient-version>
               <build-rel>8.0.5</build-rel>
               <ha2-port>ethernet1/4</ha2-port>
               <av-version>0</av-version>
               <url-version>0000.00.00.000</url-version>
               <active-passive>
                  <passive-link-state>shutdown</passive-link-state>
                  <monitor-fail-holddown>1</monitor-fail-holddown>
               </active-passive>
               <platform-model>PA-VM</platform-model>
               <av-compat>Match</av-compat>
               <vpnclient-compat>Match</vpnclient-compat>
               <ha1-ipaddr>10.255.0.2/24</ha1-ipaddr>
               <vm-license>none</vm-license>
               <ha2-macaddr>00:50:56:90:39:e2</ha2-macaddr>
               <monitor-fail-holdup>0</monitor-fail-holdup>
               <priority>200</priority>
               <preempt-hold>1</preempt-hold>
               <state>active</state>
               <version>1</version>
               <promotion-hold>2000</promotion-hold>
               <threat-compat>Match</threat-compat>
               <state-sync>Complete</state-sync>
               <addon-master-holdup>500</addon-master-holdup>
               <heartbeat-interval>2000</heartbeat-interval>
               <ha1-link-mon-intv>3000</ha1-link-mon-intv>
               <hello-interval>8000</hello-interval>
               <ha1-port>ethernet1/3</ha1-port>
               <ha1-encrypt-imported>no</ha1-encrypt-imported>
               <mgmt-ip>192.168.195.71/24</mgmt-ip>
               <vpnclient-version>Not Installed</vpnclient-version>
               <preempt-flap-cnt>0</preempt-flap-cnt>
               <nonfunc-flap-cnt>0</nonfunc-flap-cnt>
               <threat-version>0</threat-version>
               <ha1-macaddr>00:50:56:90:68:92</ha1-macaddr>
               <state-duration>2517495</state-duration>
               <max-flaps>3</max-flaps>
               <ha1-encrypt-enable>no</ha1-encrypt-enable>
               <mgmt-ipv6 />
               <state-sync-type>ethernet</state-sync-type>
               <preemptive>no</preemptive>
               <gpclient-compat>Match</gpclient-compat>
               <mode>Active-Passive</mode>
               <build-compat>Match</build-compat>
               <app-compat>Match</app-compat>
            </local-info>
            <peer-info>
               <app-version>7999-0000</app-version>
               <gpclient-version>Not Installed</gpclient-version>
               <url-version>0000.00.00.000</url-version>
               <build-rel>8.0.5</build-rel>
               <state-reason>User requested</state-reason>
               <platform-model>PA-VM</platform-model>
               <vm-license>none</vm-license>
               <ha2-macaddr>00:50:56:90:f3:06</ha2-macaddr>
               <priority>100</priority>
               <state>suspended</state>
               <version>1</version>
               <conn-status>up</conn-status>
               <av-version>0</av-version>
               <vpnclient-version>Not Installed</vpnclient-version>
               <mgmt-ip>192.168.195.70/24</mgmt-ip>
               <conn-ha2>
                  <conn-status>up</conn-status>
                  <conn-ka-enbled>no</conn-ka-enbled>
                  <conn-primary>yes</conn-primary>
                  <conn-desc>link status</conn-desc>
               </conn-ha2>
               <threat-version>0</threat-version>
               <ha1-macaddr>00:50:56:90:40:1e</ha1-macaddr>
               <conn-ha1>
                  <conn-status>up</conn-status>
                  <conn-primary>yes</conn-primary>
                  <conn-desc>heartbeat status</conn-desc>
               </conn-ha1>
               <state-duration>57</state-duration>
               <ha1-ipaddr>10.255.0.1</ha1-ipaddr>
               <mgmt-ipv6 />
               <preemptive>no</preemptive>
               <mode>Active-Passive</mode>
            </peer-info>
            <link-monitoring>
               <fail-cond>any</fail-cond>
               <enabled>no</enabled>
               <groups />
            </link-monitoring>
            <path-monitoring>
               <vwire />
               <fail-cond>any</fail-cond>
               <vlan />
               <enabled>no</enabled>
               <vrouter />
            </path-monitoring>
            <running-sync>synchronized</running-sync>
            <running-sync-enabled>yes</running-sync-enabled>
         </group>
      </result>
   </response>
""", True)

CHECK_LOCAL_ACTIVE_PAIR_2 = ("""
  <response status="success">
   <result>
      <enabled>yes</enabled>
      <group>
         <mode>Active-Passive</mode>
         <local-info>
            <url-compat>Match</url-compat>
            <app-version>7999-0000</app-version>
            <gpclient-version>Not Installed</gpclient-version>
            <build-rel>8.0.5</build-rel>
            <ha2-port>ethernet1/4</ha2-port>
            <av-version>0</av-version>
            <url-version>0000.00.00.000</url-version>
            <active-passive>
               <passive-link-state>shutdown</passive-link-state>
               <monitor-fail-holddown>1</monitor-fail-holddown>
            </active-passive>
            <platform-model>PA-VM</platform-model>
            <av-compat>Match</av-compat>
            <vpnclient-compat>Match</vpnclient-compat>
            <ha1-ipaddr>10.255.0.2/24</ha1-ipaddr>
            <vm-license>none</vm-license>
            <ha2-macaddr>00:50:56:90:39:e2</ha2-macaddr>
            <monitor-fail-holdup>0</monitor-fail-holdup>
            <priority>200</priority>
            <preempt-hold>1</preempt-hold>
            <state>passive</state>
            <version>1</version>
            <promotion-hold>2000</promotion-hold>
            <threat-compat>Match</threat-compat>
            <state-sync>Complete</state-sync>
            <addon-master-holdup>500</addon-master-holdup>
            <heartbeat-interval>2000</heartbeat-interval>
            <ha1-link-mon-intv>3000</ha1-link-mon-intv>
            <hello-interval>8000</hello-interval>
            <ha1-port>ethernet1/3</ha1-port>
            <ha1-encrypt-imported>no</ha1-encrypt-imported>
            <mgmt-ip>192.168.195.71/24</mgmt-ip>
            <vpnclient-version>Not Installed</vpnclient-version>
            <preempt-flap-cnt>0</preempt-flap-cnt>
            <nonfunc-flap-cnt>0</nonfunc-flap-cnt>
            <threat-version>0</threat-version>
            <ha1-macaddr>00:50:56:90:68:92</ha1-macaddr>
            <state-duration>2517495</state-duration>
            <max-flaps>3</max-flaps>
            <ha1-encrypt-enable>no</ha1-encrypt-enable>
            <mgmt-ipv6 />
            <state-sync-type>ethernet</state-sync-type>
            <preemptive>no</preemptive>
            <gpclient-compat>Match</gpclient-compat>
            <mode>Active-Passive</mode>
            <build-compat>Match</build-compat>
            <app-compat>Match</app-compat>
         </local-info>
         <peer-info>
            <app-version>7999-0000</app-version>
            <gpclient-version>Not Installed</gpclient-version>
            <url-version>0000.00.00.000</url-version>
            <build-rel>8.0.5</build-rel>
            <state-reason>User requested</state-reason>
            <platform-model>PA-VM</platform-model>
            <vm-license>none</vm-license>
            <ha2-macaddr>00:50:56:90:f3:06</ha2-macaddr>
            <priority>100</priority>
            <state>suspended</state>
            <version>1</version>
            <conn-status>up</conn-status>
            <av-version>0</av-version>
            <vpnclient-version>Not Installed</vpnclient-version>
            <mgmt-ip>192.168.195.70/24</mgmt-ip>
            <conn-ha2>
               <conn-status>up</conn-status>
               <conn-ka-enbled>no</conn-ka-enbled>
               <conn-primary>yes</conn-primary>
               <conn-desc>link status</conn-desc>
            </conn-ha2>
            <threat-version>0</threat-version>
            <ha1-macaddr>00:50:56:90:40:1e</ha1-macaddr>
            <conn-ha1>
               <conn-status>up</conn-status>
               <conn-primary>yes</conn-primary>
               <conn-desc>heartbeat status</conn-desc>
            </conn-ha1>
            <state-duration>57</state-duration>
            <ha1-ipaddr>10.255.0.1</ha1-ipaddr>
            <mgmt-ipv6 />
            <preemptive>no</preemptive>
            <mode>Active-Passive</mode>
         </peer-info>
         <link-monitoring>
            <fail-cond>any</fail-cond>
            <enabled>no</enabled>
            <groups />
         </link-monitoring>
         <path-monitoring>
            <vwire />
            <fail-cond>any</fail-cond>
            <vlan />
            <enabled>no</enabled>
            <vrouter />
         </path-monitoring>
         <running-sync>synchronized</running-sync>
         <running-sync-enabled>yes</running-sync-enabled>
      </group>
   </result>
</response>
    """, False)

CHECK_DIFF_PAIR_1 = ("""
<response status="success">
   <result>
      <ha-request cmd="op">
         <operations xml="yes">
            <show>
               <config>
                  <synced />
               </config>
            </show>
         </operations>
      </ha-request>
      <device-diffs><![CDATA[--- /tmp/remote-config-for-diff-curly.xml 2020-03-16 20:53:22.000000000 -0700 +++ /tmp/peer-remote-config-for-diff-curly.xml 2020-03-16 20:53:22.000000000 -0700 @@ -1087,11 +1087,11 @@ application any; service application-default; hip-profiles any; action allow; category any; - source [ 10.0.0.0-10.255.255.255 172.16.0.0-172.31.255.255]; + source 172.16.0.0-172.31.255.255; destination any; } trust-to-untrust bec8e8ca-c84a-4f2b-87d6-3139e231365f { profile-setting { profiles { @@ -1106,11 +1106,11 @@ from trust; source any; destination any; source-user any; category any; - application [ 3pc 51.com-games]; + application [ any sip]; service application-default; hip-profiles any; action allow; qos { marking {]]></device-diffs>
   </result>
</response>
""", 713)

CHECK_DIFF_PAIR_2 = ("""
<response status="success">
   <result>
      <ha-request cmd="op">
         <operations xml="yes">
            <show>
               <config>
                  <synced />
               </config>
            </show>
         </operations>
      </ha-request>
      <device-diffs><![CDATA[ ]]></device-diffs>
   </result>
</response>
""", 0)
