id: clusterxl_pnote_down_checkpoint
friendly_name: VSX Pnote(s) down
start_block: get_device_tags

blocks:
  get_device_tags:
    type: device_tags
    name: Get device tags
    register_to: my_device_tags
    go_to: make_variant_matches

  make_variant_matches:
    type: logic
    name: Match device against variant
    args: [my_device_tags]
    method: check_variant_matches
    register_to: bool_variant_matches
    go_to: check_if_variant_matches

  check_if_variant_matches:
    type: if
    name: Check if device matches requirements
    condition: bool_variant_matches
    then_go_to: get_issue_items
    else_go_to: variant_conclusion

  variant_conclusion:
    type: conclusion
    name: Incompatible workflow
    triage_conclusion: |
      This workflow is incompatible with the device.
    triage_remediation_steps: |
      Contact Indeni and request that compatibility be added.

  get_issue_items:
    type: issue_items
    name: Get issue items
    register_to: my_items
    go_to: start_loop

  start_loop:
    type: foreach
    name: Start loop
    register_item_to: cluster_pnote_and_vs_name
    start_block: check_vs_name
    blocks:
      check_vs_name:
        type: logic
        name: Get VS name with pnote issue
        args: [cluster_pnote_and_vs_name]
        method: get_vs_name
        register_to: vs_name
        go_to: parse_vs_list

      parse_vs_list:
        type: device_task
        name: Get VS list
        runner:
          type: SSH
          command: vsx stat -l
        parser:
          method: get_vs_list
          args: []
        register_to: vs_list_details
        go_to: get_vs_id

      get_vs_id:
        type: logic
        name: Get VS id
        args: [vs_list_details, vs_name]
        method: get_vs_id_from_vs_name
        register_to: vs_id
        go_to: check_admin_defined_or_user_space_pnote

      check_admin_defined_or_user_space_pnote:
        type: logic
        name: Check admin defined pnote
        args: [cluster_pnote_and_vs_name]
        method: check_admin_defined_pnote
        register_to: admin_defined_pnote
        go_to: check_if_admin_defined_pnote

      check_if_admin_defined_pnote:
        type: if
        name: Check if admin defined pnote
        condition: admin_defined_pnote
        then_go_to: conclusion_admin_defined_pnote
        else_go_to: check_vsx_pnote

      conclusion_admin_defined_pnote:
        type: conclusion
        name: Admin defined pnote down
        triage_conclusion: |
          This pnote was activated by the firewall admin/admin defined user space process is down
        triage_remediation_steps: |
          1. Check with the firewall admin if the pnote can be deactivated now.
              Unregister the pnote with command: cphaconf set_pnote -d <Name of Critical Device> unregister
              If the pnote is admin_down it can also be unregistered with command clusterXL_admin up.
          2. Firewall admin might have activated monitoring of some user space process. Please the $FWDIR/conf/cpha_proc_list
             file for the monitored process and check why the process was monitored and if the process needs
             to be monitored get the process to run again. Involve Check Point support if the issue is not identifed.

      check_vsx_pnote:
        type: logic
        name: VSX pnote active
        args: [cluster_pnote_and_vs_name]
        method: check_vsx_pnote
        register_to: vsx_pnote
        go_to: check_if_vsx_pnote

      check_if_vsx_pnote:
        type: if
        name: Check if VSX pnote is active
        condition: vsx_pnote
        then_go_to: collect_vs_with_issue
        else_go_to: check_fullsync_pnote

      collect_vs_with_issue:
        type: device_task
        name: Parse VS with issue
        runner:
          type: SSH
          command: cphaprob stat
        parser:
          method: parse_vs_with_issue
          args: []
        register_to: parse_vs_with_issue
        go_to: conclusion_vs_with_issue

      conclusion_vs_with_issue:
        type: conclusion
        name: Some other VS is affected on the Machine
        triage_conclusion:
          The pnote indicates one or more VS have pnotes is problem state
        triage_remediation_steps:
          Check the VS listed in the 'Problemtic Elements' field for the pnote on the VS and solve the pnote issue on the VS

      check_fullsync_pnote:
        type: logic
        name:  Check Fullsync pnote active
        args: [cluster_pnote_and_vs_name]
        method: check_fullsync_pnote
        register_to: check_fullsync_pnote
        go_to: check_if_fullsync_pnote

      check_if_fullsync_pnote:
        type: if
        name: Check if Fullsync pnote is active
        condition: check_fullsync_pnote
        then_go_to: collect_sync_stats
        else_go_to: check_policy_pnote

      collect_sync_stats:
        type: device_task
        name: Collect cluster sync status
        runner:
          type: SSH
          command: clish -c 'set virtual-system {{vs_id}}'; cphaprob syncstat
        parser:
          method: parse_sync_status
          args: []
        register_to: parse_cluster_sync_status
        go_to: check_sync_status_off

      check_sync_status_off:
        type: logic
        name: Check sync status on the member
        args: [parse_cluster_sync_status]
        method: check_sync_status
        register_to: cluster_sync_status
        go_to: check_if_sync_status_off

      check_if_sync_status_off:
        type: if
        name: Check if Sync status is on
        condition: cluster_sync_status
        then_go_to: conclusion_delta_sync_issues_maybe_present
        else_go_to: conclusion_fullsync_off

      conclusion_fullsync_off:
        type: conclusion
        name: Fullsync off
        triage_conclusion: |
          Fullsync identifed off due to {{cluster_sync_status}}
        triage_remediation_steps: |
          1. Check the MTU on the interface on Sync interface as well as all the network devices
             and configure them to be same
          2. If the sync mechanism was not fully initialized after cpstart, cpstop, policy installtion or reboot try enabling the
             Sync on the cluster member with command fw -d ctl setsync start
          3. If the issue persist collect the following files from both the cluster memebers
             and open a Check Point support case -
             /var/log/messages*
             $FWDIR/log/fwd.elg*
             CPinfo file


      conclusion_delta_sync_issues_maybe_present:
        type: conclusion
        name: Fullsync not off
        triage_conclusion:
          Cluster might be losing synchronisation due to other issue
        triage_remediation_steps:
          1. Check the "Drops", "Sync at risk" sections as well sent and recived
             "retransmission requests" fields for counter values in the command cphaprob syncstat.
             High values of more than 30% of the retransmission requests could indicate connectivity issue over the network
          2. Try reseting the counter with the command cphaprob -reset syncstat and check if the counters increase
             rapidly over few hours
          3. If the issue persist collect the following files from both the cluster memebers
             and open a Check Point support case -
             /var/log/messages*
             $FWDIR/log/fwd.elg*
             CPinfo file

      check_policy_pnote:
        type: logic
        name: Check Policy pnote active
        args: [cluster_pnote_and_vs_name]
        method: check_policy_pnote
        register_to: check_policy_pnote
        go_to: check_if_policy_pnote

      check_if_policy_pnote:
        type: if
        name: Check if Policy pnote is active
        condition: check_policy_pnote
        then_go_to: collect_policy_status
        else_go_to: check_fwd_pnote

      collect_policy_status:
        type: device_task
        name: Check if policy installed on the member
        runner:
          type: SSH
          command: clish -c 'set virtual-system {{vs_id}}'; fw stat
        parser:
          method: parse_policy_status
          args: []
        register_to: policy_status
        go_to: check_policy_status

      check_policy_status:
        type: logic
        name: Check policy status on the member
        args: [policy_status]
        method: check_policy_status
        register_to: policy_status_installed
        go_to: check_if_policy_status_installed

      check_if_policy_status_installed:
        type: if
        name: Check if policy is installed correctly
        condition: policy_status_installed
        then_go_to: conclusion_policy_issue_not_identified
        else_go_to: conlusion_policy_not_installed

      conclusion_policy_issue_not_identified:
        type: conclusion
        name: Policy installed correctly
        triage_conclusion: |
          Additional investigation is needed in the issue
        triage_remediation_steps:
          1. Check for diffrent policy or diffrent version of policy being installed on
             the cluster member by running command "fw stat" on both the cluster members.
             Compare policy name and date
          2. Try installing the policy again over both the member and look for failures in SmartDashboard
          3. Check sk154435 or sk33893 for most common issues with policy installtion
             https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk154435&partition=Advanced&product=ClusterXL
             https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk33893&partition=Advanced&product=ClusterXL
          4. If the issue persist open a Check Point support case

      conlusion_policy_not_installed:
        type: conclusion
        name: Correct policy is not installed on the cluster member
        triage_conclusion: |
          Install correct policy on the cluster member
        triage_remediation_steps:
          1. Install policy on the cluster members and check the policy is installed
          on both the cluster member by running the command "fw stat" and look for policy name and date
          2. Check sk154435 or 33893 for most common issue with policy installtion
          https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk154435&partition=Advanced&product=ClusterXL
          https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk33893&partition=Advanced&product=ClusterXL
          3. If the issue persist open a Check Point support case


      check_fwd_pnote:
        type: logic
        name: Check fwd pnote active
        args: [cluster_pnote_and_vs_name]
        method: check_fwd_pnote
        register_to: check_fwd_pnote
        go_to: check_if_fwd_pnote

      check_if_fwd_pnote:
        type: if
        name: Check if fwd pnote is active
        condition: check_fwd_pnote
        then_go_to: collect_fwd_status
        else_go_to: check_cphad_pnote

      collect_fwd_status:
        type: device_task
        name: Check fwd daemon status
        runner:
          type: SSH
          command: cpwd_admin list -ctx {{vs_id}}
        parser:
          method: parse_fwd_satus
          args: []
        register_to: fwd_status
        go_to: check_fwd_status

      check_fwd_status:
        type: logic
        name: Check fwd status
        args: [fwd_status]
        method: check_fwd_status
        register_to: fwd_daemon_up
        go_to: check_if_fwd_dameon_up

      check_if_fwd_dameon_up:
        type: if
        name: Check if fwd daemon is up
        condition: fwd_daemon_up
        then_go_to: conclusion_fwd_daemon_up
        else_go_to: conclusion_fwd_daemon_down

      conclusion_fwd_daemon_up:
        type: conclusion
        name: The fwd daemon is up
        triage_conclusion: |
          The fwd daemon is up and pnote is reported additional remediation steps needed
        triage_remediation_steps:
          1. Look for fwd process core dumps in the folder /var/log/dump/usermode/
          2. Check if the global parameter 'fwha_use_drv_pnotes' is enabled with command  'fw ctl get int fwha_use_drv_pnotes'
             as mentioned in sk92878.
          3. In a maintainace window collect fwd debug as per sk86321 and open a Check Point support case
             with CPINFO as well as the $FWDIR/log/fwd.elg file


      conclusion_fwd_daemon_down:
        type: conclusion
        name: The fwd daemon is down
        triage_conclusion: |
          The fwd daemon is down and pnote exists
        triage_remediation_steps:
          1. Look for fwd process core dumps in the folder /var/log/dump/usermode/
          2. Attempt to initiate the proces w
          3. In a maintainace window collect fwd debug as per sk86321 and open a Check Point support case
             with CPINFO as well as the $FWDIR/log/fwd.elg file

      check_cphad_pnote:
        type: logic
        name: Check cphad pnote active
        args: [cluster_pnote_and_vs_name]
        method: check_cphad_pnote
        register_to: cphad_pnote
        go_to: check_if_cphad_pnote

      check_if_cphad_pnote:
        type: if
        name: Check if cphad pnote is active
        condition: cphad_pnote
        then_go_to: conclusion_cphad_pnote_detected
        else_go_to: check_routed_pnote

      conclusion_cphad_pnote_detected:
        type: conclusion
        name: Problem with cphad pnote detected
        triage_conclusion: |
          The cphamcset daemon did not report its state on time and  thus pnote exists
        triage_remediation_steps:
          1. Look for cphamcset process core dumps in the folder /var/log/dump/usermode/
          2. Check for high load on machine's CPU as it can interfere with reports from CPHAD pnote
          3. Attempt to initiate the proces with command cphastart (Running this command can cause cluster failover)
          4. Look for diffrence in hotfixes on the cluster members
          5. Collect outputs as mentioned in sk62570 and open a Check Point support case
             with CPINFO as well as the $FWDIR/log/cphamcset.elg file
             https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk62570&partition=Advanced&product=ClusterXL


      check_routed_pnote:
        type: logic
        name: Check routed pnote active
        args: [cluster_pnote_and_vs_name]
        method: check_routed_pnote
        register_to: check_routed_pnote
        go_to: check_if_routed_pnote

      check_if_routed_pnote:
        type: if
        name: Check if routed pnote is active
        condition: check_routed_pnote
        then_go_to: collect_routed_status
        else_go_to: check_cvpnd_pnote

      collect_routed_status:
        type: device_task
        name: Check routed daemon status
        runner:
          type: SSH
          command: ps aux | grep -i routed
        parser:
          method: parse_routed_status
          args: []
        register_to: routed_status
        go_to: check_routed_status

      check_routed_status:
        type: logic
        name: Check routed status
        args: [routed_status, vs_id]
        method: check_routed_status
        register_to: routed_daemon_up
        go_to: check_if_routed_dameon_up

      check_if_routed_dameon_up:
        type: if
        name: Check if routed daemon is up
        condition: routed_daemon_up
        then_go_to: conclusion_routed_daemon_up
        else_go_to: conclusion_routed_daemon_down

      conclusion_routed_daemon_up:
        type: conclusion
        name: Routed daemon is up
        triage_conclusion: |
          The routed daemon is up but pnote exists additional remediation steps needed
        triage_remediation_steps:
          1. Look for routed process core dumps in the folder /var/log/dump/usermode/
          2. Compare the output for the command "show routed cluster-state detailed" on both the member to check
             routed diffrences on both the members.
          3. If no issue is identifed open a open a Check Point support case with CPINFO as well as the following files
             - /var/log/routed.log
             - /var/log/routed_messages
             - /etc/routed.conf


      conclusion_routed_daemon_down:
        type: conclusion
        name: The routed daemon is down
        triage_conclusion: |
          The routed daemon is down and pnote exists
        triage_remediation_steps:
          1. Look for routed process core dumps in the folder /var/log/dump/usermode/
          2. Attempt to initiate the proces with command "tellpm process:routed t" (Running this command can cause cluster failover)
          3. Ensure the "PNOTE Reporting" setting is not activated as mentioned in sk92878
          4. If no issue is identifed open a open a Check Point support case with CPINFO as well as the following files
             - /var/log/routed.log
             - /var/log/routed_messages
             - /etc/routed.conf


      check_cvpnd_pnote:
        type: logic
        name: Check cvpnd pnote active
        args: [cluster_pnote_and_vs_name]
        method: check_cvpnd_pnote
        register_to: check_cvpnd_pnote
        go_to: check_if_cvpnd_pnote

      check_if_cvpnd_pnote:
        type: if
        name: Check if cvpnd pnote is active
        condition: check_cvpnd_pnote
        then_go_to: conclusion_cvpnd_pnote_problem
        else_go_to: check_ted_pnote


      conclusion_cvpnd_pnote_problem:
        type: conclusion
        name: Conclusion cvpnd pnote problem
        triage_conclusion: |
          The cpvpnd pnote is detected in problem state
        triage_remediation_steps:
          1. Look for cvpnd process core dumps in the folder /var/log/dump/usermode/
          2. If no issue is identifed open a open a Check Point support case with CPINFO as well as the following files
             - /var/log/messages*
             - /var/log/dmesg
             - /var/log/boot.log
             - $CVPNDIR/log/cvpnd.elg
             - $CVPNDIR/log/httpd.log
             - $CVPNDIR/log/trace_log/*
             - $CVPNDIR/conf/httpd.conf
             - $CVPNDIR/conf/cvpnd.C
             - $CPDIR/log/cpwd.elg
             - CPinfo file from each cluster member
             - CPinfo file from MGMT server

      check_ted_pnote:
        type: logic
        name: Check ted pnote active
        args: [cluster_pnote_and_vs_name]
        method: check_ted_pnote
        register_to: check_ted_pnote
        go_to: check_if_ted_pnote

      check_if_ted_pnote:
        type: if
        name: Check if ted pnote is active
        condition: check_ted_pnote
        then_go_to: conclusion_ted_pnote_problem
        else_go_to: check_instances_or_corexl_pnote


      conclusion_ted_pnote_problem:
        type: conclusion
        name: Conclusion ted pnote problem
        triage_conclusion: |
          The ted pnote is detected in problem state
        triage_remediation_steps:
          1. Look for ted/temain process core dumps in the folder /var/log/dump/usermode/
          2. If no issue is identifed open a open a Check Point support case with CPINFO as well as the following files
             - /var/log/messages*
             - /var/log/dmesg
             - /var/log/boot.log
             - $FWDIR/log/ted.elg*
             - $CPDIR/log/cpwd.elg
             - CPinfo file from each cluster member
             - CPinfo file from MGMT server

      check_instances_or_corexl_pnote:
        type: logic
        name: Check instances or CoreXL pnote active
        args: [cluster_pnote_and_vs_name]
        method: check_instances_or_corexl_pnote
        register_to: check_instances_or_corexl_pnote
        go_to: check_if_instances_or_corexl_pnote

      check_if_instances_or_corexl_pnote:
        type: if
        name: Check if instances or CoreXL pnote is active
        condition: check_instances_or_corexl_pnote
        then_go_to: conclusion_instances_or_corexl_pnote_problem
        else_go_to: check_fib_pnote


      conclusion_instances_or_corexl_pnote_problem:
        type: conclusion
        name: Conclusion_instances_pnote_problem
        triage_conclusion: |
          The instances or corexl pnote is detected in problem state
        triage_remediation_steps:
          1. Verify that the CoreXL configuration is identical on the members of this VSX cluster / on the same Virtual Systems
             of this cluster using command fw ctl multik stat
          2. Follow sk25977 Connecting multiple clusters to the same network segment (same VLAN, same switch)
             to isolate this VSX cluster from other clusters on this network segment.

      check_fib_pnote:
        type: logic
        name: Check FIB pnote active
        args: [cluster_pnote_and_vs_name]
        method: check_fib_pnote
        register_to: check_fib_pnote
        go_to: check_if_fib_pnote

      check_if_fib_pnote:
        type: if
        name: Check if FIB pnote
        condition: check_fib_pnote
        then_go_to: conclusion_fib_problem
        else_go_to: check_amw_iterator_pnote


      conclusion_fib_problem:
        type: conclusion
        name: Conclusion FIB pnote problem
        triage_conclusion: |
          The FIB pnote is detected in problem state
        triage_remediation_steps:
          1. Allow the FIB service (TCP port 2010) to communicate between cluster members, add the following explicit
             security rule in the SmartDashboard (preferably, at the top of the rulebase) and refer to SK31243 for additional information

      check_amw_iterator_pnote:
        type: logic
        name: Check AMW or Iterator pnote active
        args: [cluster_pnote_and_vs_name]
        method: check_amw_iterator_pnote
        register_to: check_amw_iterator_pnote
        go_to: check_if_amw_iterator_pnote

      check_if_amw_iterator_pnote:
        type: if
        name: Check if AMW or Iterator pnote is active
        condition: check_amw_iterator_pnote
        then_go_to: conclusion_amw_iterator_problem
        else_go_to: check_hibernating_pnote


      conclusion_amw_iterator_problem:
        type: conclusion
        name: Conclusion AMW or Iterator pnote problem
        triage_conclusion: |
          The AMW or Iterator pnote is detected in problem state
        triage_remediation_steps:
          1. Open a Check Point support case with following files
             - Output of the dmesg command
             - All /var/log/messages* files
             - All $FWDIR/log/fwk.elg* files from the context of each Virtual System and Virtual Switch
             - cpinfo


      check_hibernating_pnote:
        type: logic
        name: Check Hibernating pnote active
        args: [cluster_pnote_and_vs_name]
        method: check_hibernating_pnote
        register_to: check_hibernating_pnote
        go_to: check_if_hibernating_pnote

      check_if_hibernating_pnote:
        type: if
        name: Check if Hibernating pnote
        condition: check_hibernating_pnote
        then_go_to: conclusion_hibernating_problem
        else_go_to: check_host_monitor_pnote


      conclusion_hibernating_problem:
        type: conclusion
        name: Conclusion Hibernating pnote active
        triage_conclusion: |
          The Hibernating is detected on the VS
        triage_remediation_steps:
          This Virtual System is in "Backup" (hibernated) state on this Cluster Member.

      check_host_monitor_pnote:
        type: logic
        name: Check host_monitor pnote active
        args: [cluster_pnote_and_vs_name]
        method: check_host_monitor_pnote
        register_to: check_host_monitor_pnote
        go_to: check_if_host_monitor_pnote

      check_if_host_monitor_pnote:
        type: if
        name: Check if host_monitor pnote
        condition: check_host_monitor_pnote
        then_go_to: conclusion_host_monitor_problem
        else_go_to: conclusion_pnote_issue_not_identifed

      conclusion_host_monitor_problem:
        type: conclusion
        name: Conclusion host_monitor_problem pnote active
        triage_conclusion: |
          At lt one of the monitored IP addresses on this Cluster Member did not reply to at least one ping.
        triage_remediation_steps:
          1. Check the ip address configured in the $FWDIR/conf/cpha_hosts file and ensure
             all the configured ip adresses respond to ping request from the cluster member

      conclusion_pnote_issue_not_identifed:
        type: conclusion
        name: Conclusion pnote issue not identifed
        triage_conclusion: |
          The pnote issue not identfied
        triage_remediation_steps:
          1. No issue is currently present. The pnote might have been in problem state before the triage was concluded.
          2. Open a Check Point support case with following files for further investigation
             - Output of the dmesg command
             - All /var/log/messages* files
             - All $FWDIR/log/fwk.elg* files from the context of each Virtual System and Virtual Switch
             - CPinfo file from each cluster member
             - CPinfo file from MGMT server
