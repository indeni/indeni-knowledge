#From Issue Items
ISSUE_VS_NAME_DATA_1 = """admin_down (VS_1)"""

GET_VS_NAME_PAIR_1 = (ISSUE_VS_NAME_DATA_1, 'VS_1')

ISSUE_VS_NAME_DATA_2 = """VSX (R80.20_VSX)"""

GET_VS_NAME_PAIR_2 = (ISSUE_VS_NAME_DATA_2, 'R80.20_VSX' )

#SSH command: vsx stat -l

VS_LIST_DATA_1 = """
VSID:            0   
VRID:            0   
Type:            VSX Gateway
Name:            R80.20_VSX
Security Policy: SP_policy
Installed at:     1Jul2020 17:39:34
SIC Status:      Trust
Connections number: 47
Connections peak:   75
Connections limit:  14900

VSID:            1   
VRID:            1   
Type:            Virtual System
Name:            VS_1
Security Policy: SP_policy
Installed at:     1Jul2020 17:40:43
SIC Status:      Trust
Connections number: 0
Connections peak:   0
Connections limit:  14900
"""
GET_VS_LIST_PAIR_1 = (VS_LIST_DATA_1,[{'vs_id': '0', 'vs_name': 'R80'}, {'vs_id': '1', 'vs_name': 'VS_1'}])

VS_LIST_DATA_2 = """

VSID:            0   
VRID:            0   
Type:            VSX Gateway
Name:            R80.20_VSX
Security Policy: SP_policy
Installed at:     1Jul2020 17:39:34
SIC Status:      Trust
Connections number: 47
Connections peak:   75
Connections limit:  14900

VSID:            1   
VRID:            1   
Type:            Virtual System
Name:            VS_1
Security Policy: SP_policy
Installed at:     1Jul2020 17:40:43
SIC Status:      Trust
Connections number: 0
Connections peak:   0
Connections limit:  14900

VSID:            2   
VRID:            2   
Type:            Virtual System
Name:            VS_2
Security Policy: SP_policy
Installed at:     1Jul2020 17:40:43
SIC Status:      Trust
Connections number: 0
Connections peak:   0
Connections limit:  14900
"""

GET_VS_LIST_PAIR_2 = (VS_LIST_DATA_2,[{'vs_id': '0', 'vs_name': 'R80'},
 {'vs_id': '1', 'vs_name': 'VS_1'},
 {'vs_id': '2', 'vs_name': 'VS_2'}])

# VS_NAME and VS_LIST from methods

VS_ID_FROM_VS_NAME_1 = [{'vs_id': '0', 'vs_name': 'R80'},
 {'vs_id': '1', 'vs_name': 'VS_1'},
 {'vs_id': '2', 'vs_name': 'VS_2'}]

GET_VS_ID_FROM_VS_NAME_PAIR_1 = ((VS_ID_FROM_VS_NAME_1,'VS_1' ), 1)

VS_ID_FROM_VS_NAME_2 = [{'vs_id': '0', 'vs_name': 'R80'},
 {'vs_id': '1', 'vs_name': 'VS_1'},
 {'vs_id': '2', 'vs_name': 'VS_2'}]

GET_VS_ID_FROM_VS_NAME_PAIR_2 = ((VS_ID_FROM_VS_NAME_2, 'VS_2'), 2)

# issue item cluster_pnote_and_vs_name
ADMIN_DEFINED_PNOTE_DATA_1 = 'admin_down (VS_1)'

GET_CHECK_ADMIN_DEFINED_PNOTE_PAIR_1 = (ADMIN_DEFINED_PNOTE_DATA_1, True)


ADMIN_DEFINED_PNOTE_DATA_2 = 'routed (VS_1)'
GET_CHECK_ADMIN_DEFINED_PNOTE_PAIR_2 = (ADMIN_DEFINED_PNOTE_DATA_2, False)

# issue item cluster_pnote_and_vs_name
VSX_PNOTE_DATA_1 = 'VSX (R80_20_VSX)'

GET_CHECK_VSX_PNOTE_PAIR_1 = (VSX_PNOTE_DATA_1, True)


VSX_PNOTE_DATA_2 = 'routed (VS_1)'

GET_CHECK_VSX_PNOTE_PAIR_2 = (VSX_PNOTE_DATA_2, False)

# SSH command: cphaprob stat
VS_WITH_ISSUE_DATA_1 = """
 
Cluster Mode:   HA Over LS

ID         Unique Address  Assigned Load   State          Name                                              

1          192.0.2.1       0%              DOWN           CP-R80.20SP-VSX-SGM1-ch01-01
15 (local) 192.0.2.15      100%            ACTIVE(!)      CP-R80.20SP-VSX-SGM1-ch02-01


Active PNOTEs: VSX

Last member state change event:
   Event Code:                 CLUS-111505
   State change:               ACTIVE -> ACTIVE(!)
   Reason for state change:    VSX PNOTE due to problem in Virtual System 1
   Event time:                 Sun Jul  5 09:18:54 2020

Last member failover event:
   Member in failure:          Member 1 
   Reason:                     VSX PNOTE
   Event time:                 Thu Jul  2 12:50:01 2020

Cluster failover count:
   Failover counter:           5
   Time of counter reset:      Thu Jun 11 14:49:24 2020 (reboot)
"""

GET_PARSE_VS_WITH_ISSUE_PAIR_1 = (VS_WITH_ISSUE_DATA_1, {'reason_for_vsx_pnote': 'VSX PNOTE due to problem in Virtual System 1'})

VS_WITH_ISSUE_DATA_2 = """

Cluster Mode:   HA Over LS

ID         Unique Address  Assigned Load   State          Name                                              

1          192.0.2.1       0%              DOWN           CP-R80.20SP-VSX-SGM1-ch01-01
15 (local) 192.0.2.15      100%            ACTIVE(!)      CP-R80.20SP-VSX-SGM1-ch02-01


Active PNOTEs: VSX

Last member state change event:
   Event Code:                 CLUS-111505
   State change:               ACTIVE -> ACTIVE(!)
   Reason for state change:    VSX PNOTE due to problem in Virtual System 2
   Event time:                 Sun Jul  5 09:18:54 2020

Last member failover event:
   Member in failure:          Member 1 
   Reason:                     VSX PNOTE
   Event time:                 Thu Jul  2 12:50:01 2020

Cluster failover count:
   Failover counter:           5
   Time of counter reset:      Thu Jun 11 14:49:24 2020 (reboot)

"""

GET_PARSE_VS_WITH_ISSUE_PAIR_2 = (VS_WITH_ISSUE_DATA_2, {'reason_for_vsx_pnote': 'VSX PNOTE due to problem in Virtual System 2'})

# issue item cluster_pnote_and_vs_name
FULLSYNC_PNOTE_DATA_1 = 'Fullsync (VS_1)'

GET_FULLSYNC_PNOTE_PAIR_1 = (FULLSYNC_PNOTE_DATA_1, True)


FULLSYNC_PNOTE_DATA_2 = 'routed (VS_1)'

GET_FULLSYNC_PNOTE_PAIR_2 = (FULLSYNC_PNOTE_DATA_2, False)

# SSH clish -c 'set virtual-system {{vs_id}}'; cphaprob syncstat

SYNCSTAT_STATUS_DATA_1 = """

Context is set to vsid 1

Delta Sync Statistics 

Sync status: OK

Drops:
Lost updates.................................  0
Lost bulk update events......................  0
Oversized updates not sent...................  0

Sync at risk:
Sent reject notifications....................  0
Received reject notifications................  0

Sent messages:
Total generated sync messages................  9574
Sent retransmission requests.................  8
Sent retransmission updates..................  9
Peak fragments per update....................  1

Received messages:
Total received updates.......................  5509
Received retransmission requests.............  18

Queue sizes (num of updates):
Sending queue size...........................  1024
Receiving queue size.........................  512
Fragments queue size.........................  50

Timers:
Delta Sync interval (ms).....................  100

Reset on Wed Jul  1 17:39:37 2020 (triggered by fullsync).
"""
GET_SYNCSTAT_DATA_PAIR_1 = (SYNCSTAT_STATUS_DATA_1, {'fullsync_status': 'OK', 'status_reason': ''})

SYNCSTAT_STATUS_DATA_2 = """

Context is set to vsid 1

Delta Sync Statistics 

Sync status: Off - Cluster module not started

Drops:
Lost updates.................................  0
Lost bulk update events......................  0
Oversized updates not sent...................  0

Sync at risk:
Sent reject notifications....................  0
Received reject notifications................  0

Sent messages:
Total generated sync messages................  9574
Sent retransmission requests.................  8
Sent retransmission updates..................  9
Peak fragments per update....................  1

Received messages:
Total received updates.......................  5509
Received retransmission requests.............  18

Queue sizes (num of updates):
Sending queue size...........................  1024
Receiving queue size.........................  512
Fragments queue size.........................  50

Timers:
Delta Sync interval (ms).....................  100

Reset on Wed Jul  1 17:39:37 2020 (triggered by fullsync).
"""

GET_SYNCSTAT_DATA_PAIR_2 = (SYNCSTAT_STATUS_DATA_2, {'fullsync_status': 'Off', 'status_reason': ' Cluster module not started'})

# Argument parse_cluster_sync_status
SYNC_STATUS_DATA_1 = {'fullsync_status': 'Off', 'status_reason': ' Cluster module not started'}
GET_SYNC_STATUS_PAIR_1 = (SYNC_STATUS_DATA_1, True)

SYNC_STATUS_DATA_2 = {'fullsync_status': 'Fullsync', 'status_reason': 'in progress'}
GET_SYNC_STATUS_PAIR_2 = (SYNC_STATUS_DATA_2, False)


# issue item cluster_pnote_and_vs_name
POLICY_PNOTE_DATA_1 = 'Policy (VS_1)'

GET_POLICY_PNOTE_PAIR_1 = (POLICY_PNOTE_DATA_1, True)


POLICY_PNOTE_DATA_2 = 'routed (VS_1)'

GET_POLICY_PNOTE_PAIR_2 = (POLICY_PNOTE_DATA_2, False)

# SSH command: clish -c 'set virtual-system {{vs_id}}'; fw stat
PARSE_POLICY_STATUS_DATA_1 = """
Context is set to vsid 1
HOST      POLICY           DATE              
localhost SP_policy         1Jul2020 17:39:34 :  [>eth1-Mgmt4] [<eth1-Mgmt4] [>Sync] [<Sync]
"""

GET_PARSE_POLICY_STATUS_PAIR_1 = (PARSE_POLICY_STATUS_DATA_1, {'policy_name': 'SP_policy'})

PARSE_POLICY_STATUS_DATA_2 = """
Context is set to vsid 1
HOST      POLICY           DATE
localhost -         - :  [>eth1-Mgmt4] [<eth1-Mgmt4] [>Sync] [<Sync] 
"""
GET_PARSE_POLICY_STATUS_PAIR_2 = (PARSE_POLICY_STATUS_DATA_2, {'policy_name': '-'} )

# Argument policy_status

POLICY_STATUS_DATA_1 = {'policy_name': 'SP_policy'}

GET_POLICY_STATUS_PAIR_1 = (POLICY_STATUS_DATA_1, True)

POLICY_STATUS_DATA_2 = {'policy_name': '-'}

GET_POLICY_STATUS_PAIR_2 = (POLICY_STATUS_DATA_2, False)

# issue item cluster_pnote_and_vs_name
FWD_PNOTE_DATA_1 = 'fwd (VS_1)'

GET_FWD_PNOTE_PAIR_1 = (FWD_PNOTE_DATA_1, True)


FWD_PNOTE_DATA_2 = 'routed (VS_1)'

GET_FWD_PNOTE_PAIR_2 = (FWD_PNOTE_DATA_2, False)

#SSH command: cpwd_admin list -ctx {{vs_id}}

PARSE_FWD_STATUS_DATA_1 = """
APP        CTX        PID    STAT  #START  START_TIME             MON  COMMAND             
FWK_WD     1          31584  E     1       [12:20:21] 24/6/2020   N    fwk_wd -i 1 -i6 0   
CPD        1          22782  E     1       [17:40:45] 1/7/2020    Y    cpd                 
FWD        1          1506   E     1       [18:30:20] 2/7/2020    N    fwd                 
CPVIEWD    1          22788  E     1       [17:40:45] 1/7/2020    N    cpviewd             
MPDAEMON   1          22791  E     1       [17:40:45] 1/7/2020    N    mpdaemon /opt/CPshrd-R80.20/CTX/CTX00001/log/mpdaemon.elg /opt/CPshrd-R80.20/CTX/CTX00001/conf/mpdaemon.conf
CIHS       1          22793  E     1       [17:40:45] 1/7/2020    N    ci_http_server -j -f /opt/CPsuite-R80.20/fw1/CTX/CTX00001/conf/cihs.conf
"""
GET_PARSE_FWD_STATUS_PAIR_1 = (PARSE_FWD_STATUS_DATA_1, {'fwd_status': 'E'})

PARSE_FWD_STATUS_DATA_2 = """
APP        CTX        PID    STAT  #START  START_TIME             MON  COMMAND             
FWK_WD     1          31584  E     1       [12:20:21] 24/6/2020   N    fwk_wd -i 1 -i6 0   
CPD        1          22782  E     1       [17:40:45] 1/7/2020    Y    cpd                 
FWD        1          0   T     0       [18:30:20] 2/7/2020    N    fwd                 
CPVIEWD    1          22788  E     1       [17:40:45] 1/7/2020    N    cpviewd             
MPDAEMON   1          22791  E     1       [17:40:45] 1/7/2020    N    mpdaemon /opt/CPshrd-R80.20/CTX/CTX00001/log/mpdaemon.elg /opt/CPshrd-R80.20/CTX/CTX00001/conf/mpdaemon.conf
CIHS       1          22793  E     1       [17:40:45] 1/7/2020    N    ci_http_server -j -f /opt/CPsuite-R80.20/fw1/CTX/CTX00001/conf/cihs.conf
"""
GET_PARSE_FWD_STATUS_PAIR_2 = (PARSE_FWD_STATUS_DATA_2, {'fwd_status': 'T'})


# Argument fwd_status
FWD_DAEMON_DATA_1 = {'fwd_status': 'E'}
GET_FWD_DAEMON_PAIR_1 = (FWD_DAEMON_DATA_1, True)

FWD_DAEMON_DATA_2 = {'fwd_status': 'T'}
GET_FWD_DAEMON_PAIR_2 = (FWD_DAEMON_DATA_2, False)


# # issue item cluster_pnote_and_vs_name
CPHAD_PNOTE_DATA_1 = 'cphad (VS_1)'

GET_CPHAD_PNOTE_PAIR_1 = (CPHAD_PNOTE_DATA_1, True)


CPHAD_PNOTE_DATA_2 = 'routed (VS_1)'

GET_CPHAD_PNOTE_PAIR_2 = (CPHAD_PNOTE_DATA_2, False)


#  issue item cluster_pnote_and_vs_name
ROUTED_PNOTE_DATA_1 = 'routed (VS_1)'

GET_ROUTED_PNOTE_PAIR_1 = (ROUTED_PNOTE_DATA_1, True)


ROUTED_PNOTE_DATA_2 = 'cvpnd (VS_1)'

GET_ROUTED_PNOTE_PAIR_2 = (ROUTED_PNOTE_DATA_2, False)

#SSH command: ps aux | grep -i routed
PARSE_ROUTED_STATUS_DATA_1 =  """
admin     1856  0.0  0.0   1736   560 pts/32   S+   14:29   0:00 grep -i routed
admin     9875  0.0  0.1  39344 10836 ?        S<   14:12   0:00 /bin/routed -i 1 -f /etc/routed1.conf -h 1
admin     9875  0.0  0.1  39344 10836 ?        S<   14:12   0:00 /bin/routed -i 2 -f /etc/routed1.conf -h 2
admin    16165  0.0  0.1  39064  9036 ?        S<s  14:12   0:00 /bin/routed -N
admin    16352  0.0  0.1  49976 12300 ?        S<l  14:12   0:00 /bin/routed -i default -f /etc/routed0.conf -h 1
"""

GET_PARSE_ROUTED_PNOTE_PAIR_1 = (PARSE_ROUTED_STATUS_DATA_1, [{'routed_state': '1'}, {'routed_state': '2'}, {'routed_state': 'default'}])

PARSE_ROUTED_STATUS_DATA_2 = """
admin    16165  0.0  0.1  39064  9036 ?        S<s  14:12   0:00 /bin/routed -N
admin    16352  0.0  0.1  49976 12300 ?        S<l  14:12   0:00 /bin/routed -i default -f /etc/routed0.conf -h 1
"""
GET_PARSE_ROUTED_PNOTE_PAIR_2 = (PARSE_ROUTED_STATUS_DATA_2, [{'routed_state': 'default'}])


# Arguments routed_status, vs_id
ROUTED_DAEMON_DATA_1 = [{'routed_state': '1'}, {'routed_state': '2'}, {'routed_state': 'default'}]
GET_ROUTED_DAEMON_PAIR_1 = ((ROUTED_DAEMON_DATA_1, 1), True)

ROUTED_DAEMON_DATA_2 = [{'routed_state': 'default'}]
GET_ROUTED_DAEMON_PAIR_2 = ((ROUTED_DAEMON_DATA_2, 1), False)

#  issue item cluster_pnote_and_vs_name
CVPND_PNOTE_DATA_1 = 'cvpnd (VS_1)'

GET_CVPND_PNOTE_PAIR_1 = (CVPND_PNOTE_DATA_1, True)


CVPND_PNOTE_DATA_2 = 'routed (VS_1)'

GET_CVPND_PNOTE_PAIR_2 = (CVPND_PNOTE_DATA_2, False)

#  issue item cluster_pnote_and_vs_name
TED_PNOTE_DATA_1 = 'cvpnd (VS_1)'

GET_TED_PNOTE_PAIR_1 = (TED_PNOTE_DATA_1, False)


TED_PNOTE_DATA_2 = 'ted (VS_1)'

GET_TED_PNOTE_PAIR_2 = (TED_PNOTE_DATA_2, True)


#  issue item cluster_pnote_and_vs_name
INSTANCES_OR_COREXL_PNOTE_DATA_1 = 'Instances (VS_1)'

GET_INSTANCES_OR_COREXL_PNOTE_PAIR_1 = (INSTANCES_OR_COREXL_PNOTE_DATA_1, True)


INSTANCES_OR_COREXL_PNOTE_DATA_2 = 'ted (VS_1)'

GET_INSTANCES_OR_COREXL_PNOTE_PAIR_2 = (INSTANCES_OR_COREXL_PNOTE_DATA_2, False)


#  issue item cluster_pnote_and_vs_name
FIB_PNOTE_DATA_1 = 'FIB (VS_1)'

GET_FIB_PNOTE_PAIR_1 = (FIB_PNOTE_DATA_1, True)


FIB_PNOTE_DATA_2 = 'ted (VS_1)'

GET_FIB_PNOTE_PAIR_2 = (FIB_PNOTE_DATA_2, False)


#  issue item cluster_pnote_and_vs_name
AMW_ITERATOR_CHASSIS_MONITOR_PNOTE_DATA_1 = 'AMW (VS_1)'

GET_AMW_ITERATOR_CHASSIS_MONITOR_PNOTE_PAIR_1 = (AMW_ITERATOR_CHASSIS_MONITOR_PNOTE_DATA_1, True)


AMW_ITERATOR_CHASSIS_MONITOR_PNOTE_DATA_2 = 'Iterator (VS_1)'

GET_AMW_ITERATOR_CHASSIS_MONITOR_PNOTE_PAIR_2 = (AMW_ITERATOR_CHASSIS_MONITOR_PNOTE_DATA_2, True)


#  issue item cluster_pnote_and_vs_name
HIBERNATING_PNOTE_DATA_1 = 'Hibernating (VS_1)'

GET_HIBERNATING_PNOTE_PAIR_1 = (HIBERNATING_PNOTE_DATA_1, True)

HIBERNATING_PNOTE_DATA_2 = 'Iterator (VS_1)'

GET_HIBERNATING_PNOTE_PAIR_2 = (HIBERNATING_PNOTE_DATA_2, False)


#  issue item cluster_pnote_and_vs_name
HOST_MONITOR_PNOTE_DATA_1 = 'host_monitor (VS_1)'

GET_HOST_MONITOR_PNOTE_PAIR_1 = (HOST_MONITOR_PNOTE_DATA_1, True)

HOST_MONITOR_PNOTE_DATA_2 = 'Iterator (VS_1)'

GET_HOST_MONITOR_PNOTE_PAIR_2 = (HOST_MONITOR_PNOTE_DATA_2, False)

