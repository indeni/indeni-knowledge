import unittest
import automation.workflows.clusterxl_pnote_down_vsx.checkpoint.clusterxl_pnote_down_vsx_parser_mock_data as mock
import automation.workflows.clusterxl_pnote_down_vsx.checkpoint.clusterxl_pnote_down_vsx_parser as parser

class ClusterXLDownParserTests(unittest.TestCase):


    def test_get_vs_name(self):
        pair1 = mock.GET_VS_NAME_PAIR_1
        pair2 = mock.GET_VS_NAME_PAIR_2
        self.assertEqual(pair1[1], parser.get_vs_name(pair1[0]))
        self.assertEqual(pair2[1], parser.get_vs_name(pair2[0]))


    def test_get_vs_list(self):
        pair1 = mock.GET_VS_LIST_PAIR_1
        pair2 = mock.GET_VS_LIST_PAIR_2
        self.assertEqual(pair1[1], parser.get_vs_list(pair1[0]))
        self.assertEqual(pair2[1], parser.get_vs_list(pair2[0]))


    def test_get_vs_id_from_vs_name(self):
        pair1 = mock.GET_VS_ID_FROM_VS_NAME_PAIR_1
        pair2 = mock.GET_VS_ID_FROM_VS_NAME_PAIR_2
        self.assertEqual(pair1[1], parser.get_vs_id_from_vs_name(pair1[0][0], pair1[0][1]))
        self.assertEqual(pair2[1], parser.get_vs_id_from_vs_name(pair2[0][0], pair2[0][1]))


    def test_check_admin_defined_pnote(self):
        pair1 = mock.GET_CHECK_ADMIN_DEFINED_PNOTE_PAIR_1
        pair2 = mock.GET_CHECK_ADMIN_DEFINED_PNOTE_PAIR_2
        self.assertEqual(pair1[1], parser.check_admin_defined_pnote(pair1[0]))
        self.assertEqual(pair2[1], parser.check_admin_defined_pnote(pair2[0]))


    def test_check_vsx_pnote(self):
        pair1 = mock.GET_CHECK_VSX_PNOTE_PAIR_1
        pair2 = mock.GET_CHECK_VSX_PNOTE_PAIR_2
        self.assertEqual(pair1[1], parser.check_vsx_pnote(pair1[0]))
        self.assertEqual(pair2[1], parser.check_vsx_pnote(pair2[0]))


    def test_parse_vs_with_issue(self):
        pair1 = mock.GET_PARSE_VS_WITH_ISSUE_PAIR_1
        pair2 = mock.GET_PARSE_VS_WITH_ISSUE_PAIR_2
        self.assertEqual(pair1[1], parser.parse_vs_with_issue(pair1[0]))
        self.assertEqual(pair2[1], parser.parse_vs_with_issue(pair2[0]))


    def test_check_fullsync_pnote(self):
        pair1 = mock.GET_FULLSYNC_PNOTE_PAIR_1
        pair2 = mock.GET_FULLSYNC_PNOTE_PAIR_2
        self.assertEqual(pair1[1], parser.check_fullsync_pnote(pair1[0]))
        self.assertEqual(pair2[1], parser.check_fullsync_pnote(pair2[0]))


    def test_parse_sync_status(self):
        pair1 = mock.GET_SYNCSTAT_DATA_PAIR_1
        pair2 = mock.GET_SYNCSTAT_DATA_PAIR_2
        self.assertEqual(pair1[1], parser.parse_sync_status(pair1[0]))
        self.assertEqual(pair2[1], parser.parse_sync_status(pair2[0]))


    def test_check_sync_status(self):
        pair1 = mock.GET_SYNC_STATUS_PAIR_1
        pair2 = mock.GET_SYNC_STATUS_PAIR_2
        self.assertEqual(pair1[1], parser.check_sync_status(pair1[0]))
        self.assertEqual(pair2[1], parser.check_sync_status(pair2[0]))

    def test_check_policy_pnote(self):
        pair1 = mock.GET_POLICY_PNOTE_PAIR_1
        pair2 = mock.GET_POLICY_PNOTE_PAIR_2
        self.assertEqual(pair1[1], parser.check_policy_pnote(pair1[0]))
        self.assertEqual(pair2[1], parser.check_policy_pnote(pair2[0]))


    def test_parse_policy_status(self):
        pair1 = mock.GET_PARSE_POLICY_STATUS_PAIR_1
        pair2 = mock.GET_PARSE_POLICY_STATUS_PAIR_2
        self.assertEqual(pair1[1], parser.parse_policy_status(pair1[0]))
        self.assertEqual(pair2[1], parser.parse_policy_status(pair2[0]))


    def test_check_policy_status(self):
        pair1 = mock.GET_POLICY_STATUS_PAIR_1
        pair2 = mock.GET_POLICY_STATUS_PAIR_2
        self.assertEqual(pair1[1], parser.check_policy_status(pair1[0]))
        self.assertEqual(pair2[1], parser.check_policy_status(pair2[0]))


    def test_check_fwd_pnote(self):
        pair1 = mock.GET_FWD_PNOTE_PAIR_1
        pair2 = mock.GET_FWD_PNOTE_PAIR_2
        self.assertEqual(pair1[1], parser.check_fwd_pnote(pair1[0]))
        self.assertEqual(pair2[1], parser.check_fwd_pnote(pair2[0]))

    def test_parse_fwd_status(self):
        pair1 = mock.GET_PARSE_FWD_STATUS_PAIR_1
        pair2 = mock.GET_PARSE_FWD_STATUS_PAIR_2
        self.assertEqual(pair1[1], parser.parse_fwd_satus(pair1[0]))
        self.assertEqual(pair2[1], parser.parse_fwd_satus(pair2[0]))

    def test_check_fwd_status(self):
        pair1 = mock.GET_FWD_DAEMON_PAIR_1
        pair2 = mock.GET_FWD_DAEMON_PAIR_2
        self.assertEqual(pair1[1], parser.check_fwd_status(pair1[0]))
        self.assertEqual(pair2[1], parser.check_fwd_status(pair2[0]))

    def test_check_cphad_pnote(self):
        pair1 = mock.GET_CPHAD_PNOTE_PAIR_1
        pair2 = mock.GET_CPHAD_PNOTE_PAIR_2
        self.assertEqual(pair1[1], parser.check_cphad_pnote(pair1[0]))
        self.assertEqual(pair2[1], parser.check_cphad_pnote(pair2[0]))

    def test_check_routed_pnote(self):
        pair1 = mock.GET_ROUTED_PNOTE_PAIR_1
        pair2 = mock.GET_ROUTED_PNOTE_PAIR_2
        self.assertEqual(pair1[1], parser.check_routed_pnote(pair1[0]))
        self.assertEqual(pair2[1], parser.check_routed_pnote(pair2[0]))

    def test_parse_routed_status(self):
        pair1 = mock.GET_PARSE_ROUTED_PNOTE_PAIR_1
        pair2 = mock.GET_PARSE_ROUTED_PNOTE_PAIR_2
        self.assertEqual(pair1[1], parser.parse_routed_status(pair1[0]))
        self.assertEqual(pair2[1], parser.parse_routed_status(pair2[0]))


    def test_check_routed_status(self):
        pair1 = mock.GET_ROUTED_DAEMON_PAIR_1
        pair2 = mock.GET_ROUTED_DAEMON_PAIR_2
        self.assertEqual(pair1[1], parser.check_routed_status(pair1[0][0],pair2[0][1]))
        self.assertEqual(pair2[1],parser.check_routed_status(pair2[0][0], pair2[0][1]))


    def test_check_cvpnd_pnote(self):
        pair1 = mock.GET_CVPND_PNOTE_PAIR_1
        pair2 = mock.GET_CVPND_PNOTE_PAIR_2
        self.assertEqual(pair1[1], parser.check_cvpnd_pnote(pair1[0]))
        self.assertEqual(pair2[1], parser.check_cvpnd_pnote(pair2[0]))


    def test_check_ted_pnote(self):
        pair1 = mock.GET_TED_PNOTE_PAIR_1
        pair2 = mock.GET_TED_PNOTE_PAIR_2
        self.assertEqual(pair1[1], parser.check_ted_pnote(pair1[0]))
        self.assertEqual(pair2[1], parser.check_ted_pnote(pair2[0]))


    def test_check_instances_or_corexl_pnote(self):
        pair1 = mock.GET_INSTANCES_OR_COREXL_PNOTE_PAIR_1
        pair2 = mock.GET_INSTANCES_OR_COREXL_PNOTE_PAIR_2
        self.assertEqual(pair1[1], parser.check_instances_or_corexl_pnote(pair1[0]))
        self.assertEqual(pair2[1], parser.check_instances_or_corexl_pnote(pair2[0]))


    def test_check_fib_pnote(self):
        pair1 = mock.GET_FIB_PNOTE_PAIR_1
        pair2 = mock.GET_FIB_PNOTE_PAIR_2
        self.assertEqual(pair1[1], parser.check_fib_pnote(pair1[0]))
        self.assertEqual(pair2[1], parser.check_fib_pnote(pair2[0]))


    def test_check_amw_iterator_chassis_monitor_pnote(self):
        pair1 = mock.GET_AMW_ITERATOR_CHASSIS_MONITOR_PNOTE_PAIR_1
        pair2 = mock.GET_AMW_ITERATOR_CHASSIS_MONITOR_PNOTE_PAIR_2
        self.assertEqual(pair1[1], parser.check_amw_iterator_pnote(pair1[0]))
        self.assertEqual(pair2[1], parser.check_amw_iterator_pnote(pair2[0]))


    def test_check_hibernating_pnote(self):
        pair1 = mock.GET_HIBERNATING_PNOTE_PAIR_1
        pair2 = mock.GET_HIBERNATING_PNOTE_PAIR_2
        self.assertEqual(pair1[1], parser.check_hibernating_pnote(pair1[0]))
        self.assertEqual(pair2[1], parser.check_hibernating_pnote(pair2[0]))


    def test_check_host_monitor_pnote(self):
        pair1 = mock.GET_HOST_MONITOR_PNOTE_PAIR_1
        pair2 = mock.GET_HOST_MONITOR_PNOTE_PAIR_2
        self.assertEqual(pair1[1], parser.check_host_monitor_pnote(pair1[0]))
        self.assertEqual(pair2[1], parser.check_host_monitor_pnote(pair2[0]))