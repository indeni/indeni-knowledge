from parser_service.public.helper_methods import *

def check_variant_matches(**kwargs) -> bool:
    # This should work on all gateways
    tags = kwargs.get('my_device_tags', None)
    if tags is None:
        return False
    if tags.get('chassis') == 'true':
        return False
    if tags.get('vsx') != 'true':
        return False
    return True

def get_vs_name(cluster_pnote_and_vs_name: str) -> object:
    if cluster_pnote_and_vs_name:
        data = parse_data_as_object(cluster_pnote_and_vs_name, 'checkpoint_gaia_pnote_vs_name_from_issue_item.textfsm')
        if data:
            vs_name_recived = data['vs_name']
            vs_name_cleaned_1 = vs_name_recived.replace('(', '')
            vs_name = (vs_name_cleaned_1.replace(')', '')).strip()
            return vs_name
    return False


def get_vs_list(raw_data: str) -> object:
    if raw_data:
        vs_list_details = parse_data_as_list(raw_data, 'checkpoint_gaia_vs_list_details.textfsm')
        if vs_list_details:
            return vs_list_details
    return False

def get_vs_id_from_vs_name(vs_list_details: list, vs_name: str) -> int:
    if vs_list_details and vs_name:
        for details in vs_list_details:
            if vs_name == details['vs_name']:
                vs_id = int(details['vs_id'])
                return vs_id
    return False


def check_admin_defined_pnote(cluster_pnote_and_vs_name) -> bool:
    if cluster_pnote_and_vs_name:
        admin_defined_pnote = parse_data_as_object(cluster_pnote_and_vs_name,
                                                   'checkpoint_gaia_pnote_vs_name_from_issue_item.textfsm')
        if admin_defined_pnote:
            if admin_defined_pnote['pnote'] not in ['Problem Notification', 'Init', 'Interface Active Check','Load Balancing Configuration', 'Recovery Delay', 'CoreXL Configuration', 'Fullsync', 'Policy', 'fwd', 'cphad', 'routed', 'cvpnd', 'ted', 'VSX', 'Instances', 'host_monitor', 'FIB', 'Hibernating']:
                return True
    return False


def check_vsx_pnote(cluster_pnote_and_vs_name:str) -> bool:
    if cluster_pnote_and_vs_name:
        vsx_pnote = parse_data_as_object(cluster_pnote_and_vs_name, 'checkpoint_gaia_pnote_vs_name_from_issue_item.textfsm')
        if vsx_pnote:
            if vsx_pnote['pnote'] == 'VSX':
                return True
    return False


def parse_vs_with_issue(raw_data: str) -> object:
    if raw_data:
        parse_vs_with_issue = parse_data_as_object(raw_data, 'checkpoint_gaia_vsx_pnote_cphaprob_stat.textfsm')
        if parse_vs_with_issue:
            return parse_vs_with_issue
    return False


def check_fullsync_pnote(cluster_pnote_and_vs_name:str) -> bool:
    if cluster_pnote_and_vs_name:
        fullsync_pnote = parse_data_as_object(cluster_pnote_and_vs_name, 'checkpoint_gaia_pnote_vs_name_from_issue_item.textfsm')
        if fullsync_pnote:
            if fullsync_pnote['pnote'] == 'Fullsync':
                return True
    return False


def parse_sync_status(raw_data: str) -> object:
    if raw_data:
        parse_cluster_sync_status = parse_data_as_object(raw_data, 'checkpoint_gaia_syncstat_status.textfsm')
        if parse_cluster_sync_status:
            return parse_cluster_sync_status
    return False


def check_sync_status(parse_cluster_sync_status: object) -> bool:
    if parse_cluster_sync_status:
        if parse_cluster_sync_status['fullsync_status'] == 'OK':
            return True
    return False


def check_policy_pnote(cluster_pnote_and_vs_name: str) -> bool:
    if cluster_pnote_and_vs_name:
        policy_pnote = parse_data_as_object(cluster_pnote_and_vs_name, 'checkpoint_gaia_pnote_vs_name_from_issue_item.textfsm')
        if policy_pnote:
            if policy_pnote['pnote'] == 'Policy':
                return True
    return False


def parse_policy_status(raw_data: str) -> object:
    if raw_data:
        policy_status = parse_data_as_object(raw_data, 'checkpoint_gaia_policy_fw_stat.textfsm')
        if policy_status:
            return policy_status
    return False


def check_policy_status(policy_status) -> bool:
    if policy_status:
        if policy_status['policy_name'] not in ['-', 'InitialPolicy']:
            return True
    return False


def check_fwd_pnote(cluster_pnote_and_vs_name: str) -> bool:
    if cluster_pnote_and_vs_name:
        fwd_pnote = parse_data_as_object(cluster_pnote_and_vs_name, 'checkpoint_gaia_pnote_vs_name_from_issue_item.textfsm')
        if fwd_pnote:
            if fwd_pnote['pnote'] == 'fwd':
                return True
    return False


def parse_fwd_satus(raw_data: str) -> object:
    if raw_data:
        fwd_status = parse_data_as_object(raw_data, 'checkpoint_gaia_cpwd_admin_fwd_status.textfsm')
        if fwd_status:
            return fwd_status
    return False


def check_fwd_status(fwd_status: str) -> bool:
    if fwd_status:
        if fwd_status['fwd_status'] == 'E':
            return True
    return False


def check_cphad_pnote(cluster_pnote_and_vs_name: str) -> bool:
    if cluster_pnote_and_vs_name:
        cphad_pnote = parse_data_as_object(cluster_pnote_and_vs_name, 'checkpoint_gaia_pnote_vs_name_from_issue_item.textfsm')
        if cphad_pnote:
            if cphad_pnote['pnote'] == 'cphad':
                return True
    return False


def check_routed_pnote(cluster_pnote_and_vs_name: str) -> bool:
    if cluster_pnote_and_vs_name:
        routed_pnote = parse_data_as_object(cluster_pnote_and_vs_name, 'checkpoint_gaia_pnote_vs_name_from_issue_item.textfsm')
        if routed_pnote:
            if routed_pnote['pnote'] == 'routed':
                return True
    return False

def parse_routed_status(raw_data: str) -> object:
    if raw_data:
        routed_status = parse_data_as_list(raw_data, 'checkpoint_gaia_routed_status.textfsm')
        if routed_status:
            return routed_status
    return False

def check_routed_status(routed_status: list, vs_id: int) -> bool:
    if routed_status and vs_id:
        for status in routed_status:
            if str(vs_id) == status['routed_state']:
                return True
    return False


def check_cvpnd_pnote(cluster_pnote_and_vs_name: str) -> bool:
    if cluster_pnote_and_vs_name:
        cvpnd_pnote = parse_data_as_object(cluster_pnote_and_vs_name, 'checkpoint_gaia_pnote_vs_name_from_issue_item.textfsm')
        if cvpnd_pnote:
            if cvpnd_pnote['pnote'] == 'cvpnd':
                return True
    return False

def check_ted_pnote(cluster_pnote_and_vs_name: str) -> bool:
    if cluster_pnote_and_vs_name:
        ted_pnote = parse_data_as_object(cluster_pnote_and_vs_name, 'checkpoint_gaia_pnote_vs_name_from_issue_item.textfsm')
        if ted_pnote:
            if ted_pnote['pnote'] == 'ted':
                return True
    return False


def check_instances_or_corexl_pnote(cluster_pnote_and_vs_name: str) -> bool:
    if cluster_pnote_and_vs_name:
        instances_or_corexl_pnote_pnote = parse_data_as_object(cluster_pnote_and_vs_name, 'checkpoint_gaia_pnote_vs_name_from_issue_item.textfsm')
        if instances_or_corexl_pnote_pnote:
            if instances_or_corexl_pnote_pnote['pnote'] in ['CoreXL Configuration', 'Instances']:
                return True
    return False


def check_fib_pnote(cluster_pnote_and_vs_name: str) -> bool:
    if cluster_pnote_and_vs_name:
        fib_pnote = parse_data_as_object(cluster_pnote_and_vs_name, 'checkpoint_gaia_pnote_vs_name_from_issue_item.textfsm')
        if fib_pnote:
            if fib_pnote['pnote'] == 'FIB':
                return True
    return False

def check_amw_iterator_pnote(cluster_pnote_and_vs_name: str) -> bool:
    if cluster_pnote_and_vs_name:
        amw_iterator_pnote = parse_data_as_object(cluster_pnote_and_vs_name, 'checkpoint_gaia_pnote_vs_name_from_issue_item.textfsm')
        if amw_iterator_pnote:
            if amw_iterator_pnote['pnote'] in ['Iterator', 'AMW']:
                return True
    return False

def check_hibernating_pnote(cluster_pnote_and_vs_name: str) -> bool:
    if cluster_pnote_and_vs_name:
        hibernating_pnote = parse_data_as_object(cluster_pnote_and_vs_name, 'checkpoint_gaia_pnote_vs_name_from_issue_item.textfsm')
        if hibernating_pnote:
            if hibernating_pnote['pnote'] == 'Hibernating':
                return True
    return False

def check_host_monitor_pnote(cluster_pnote_and_vs_name: str) -> bool:
    if cluster_pnote_and_vs_name:
        host_monitor_pnote = parse_data_as_object(cluster_pnote_and_vs_name, 'checkpoint_gaia_pnote_vs_name_from_issue_item.textfsm')
        if host_monitor_pnote:
            if host_monitor_pnote['pnote'] == 'host_monitor':
                return True
    return False


