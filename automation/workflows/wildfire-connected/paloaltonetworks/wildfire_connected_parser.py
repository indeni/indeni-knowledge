import re
from typing import List
from dateutil import parser
from datetime import datetime, timedelta
from parser_service.public.helper_methods import *


def check_variant_matches(**kwargs) -> bool:
    # This should work on all gateways
    tags = kwargs.get('my_device_tags', None)
    if tags is None:
        return False
    if tags.get('product') != 'firewall':
        return False
    return True


def parse_cloud_information_and_return_dict(clouds: list) -> dict:
    wildfireclouds = {}
    for wildfirecloud in clouds:
        cloud_information = {}
        cloud_name = wildfirecloud['wildfile_cloud']
        cloud_information[cloud_name] = wildfirecloud
        wildfireclouds.update(cloud_information)
    return wildfireclouds


def parse_wildfire_cloud_status(raw_data: str) -> dict:
    raw_data_parsed = parse_data_as_list(raw_data, 'wildfire_connected_show_status.textfsm')
    return parse_cloud_information_and_return_dict(raw_data_parsed)


def parse_wildfire_cloud_registration(raw_data: str) -> dict:
    raw_data_parsed = parse_data_as_list(raw_data, 'wildfire_connected_test_registration.textfsm')
    return parse_cloud_information_and_return_dict(raw_data_parsed)


def parse_compare_cloud_status(wildfire_cloud_status: dict, cloud_type:str) -> str:
    ok_status = ['Idle', 'Getting reports', 'Registering', 'Downloading server list', 'Disabled due to configuration']
    cloud_status = wildfire_cloud_status[cloud_type]['status']
    return cloud_status in ok_status


def parse_verify_server_address(wildfire_cloud_status: dict, cloud_type:str) -> bool:
    # If FQDN - check it ends with '.paloaltonetworks.com'
    if cloud_type == "Public":
        server = wildfire_cloud_status['Public']['server']
        if server.endswith('.paloaltonetworks.com'):
            return True
        else:
            return False
    else:
        server = wildfire_cloud_status['Private']['server']
        if re.match(r"((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.?){4}",server):
            return True
        else:
            return False


def parse_verify_device_registration(wildfire_cloud_registration: dict, cloud_type:str) -> str:
    registration = wildfire_cloud_registration[cloud_type]['registration_result']
    return registration == 'successful'
