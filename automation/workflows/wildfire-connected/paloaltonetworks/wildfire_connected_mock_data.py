CLOUD_STATUS_DICT = {
        'Public': {'wildfile_cloud': 'Public', 'server': 'wildfire.paloaltonetworks.com', 'registered': 'yes', 'license': 'yes', 'status': 'Idle'},
        'Private': {'wildfile_cloud': 'Private', 'server': '', 'registered': 'no', 'license': 'no', 'status': 'Disabled due to configuration'}}

SHOW_WILDFIRE_STATUS = ("""
<response status="success"><result>
  <member>Connection info:
  Signature verification:        enable
  Server selection:              enable
  File cache:                    enable

WildFire Public Cloud:
  Server address:                wildfire.paloaltonetworks.com
  Best server:                   panos.wildfire.paloaltonetworks.com
  Device registered:             yes
  Through a proxy:               no
  Valid wildfire license:        yes
  Service route IP address:      10.11.95.31
  Global status:                 Idle
  Count of available workers:    10
  Available worker indices:      0 1 2 3 4 5 6 7 8 9
  Upload status Usage: 'I': Idle, 'U': Uploading, 'Q': Querying
    Upload worker index:           0    1    2    3    4    5    6    7    8    9
    Upload status:                 I    I    I    I    I    I    I    I    I    I
    Status time (seconds):         999+ 999+ 999+ 999+ 999+ 999+ 999+ 999+ 999+ 999+

WildFire Private Cloud:
  Server address:
  Best server:
  Device registered:             no
  Through a proxy:               no
  Valid wildfire license:        no
  Service route IP address:
  Global status:                 Disabled due to configuration
  Count of available workers:    0
  Available worker indices:
  Upload status Usage: 'I': Idle, 'U': Uploading, 'Q': Querying
    Upload worker index:           0    1    2    3    4    5    6    7    8    9
    Upload status:                 Idle Idle Idle Idle Idle Idle Idle Idle Idle Idle
    Status time (seconds):         999+ 999+ 999+ 999+ 999+ 999+ 999+ 999+ 999+ 999+

File size limit info:
  pe                                          16 MB
  apk                                         10 MB
  pdf                                       3072 KB
  ms-office                                16384 KB
  jar                                          5 MB
  flash                                        5 MB
  MacOSX                                      10 MB
  archive                                     50 MB
  linux                                       50 MB
  script                                      20 KB

Forwarding info:
  file idle time out (second):                          90
  total bytes of concurrent files:                       0
  Public Cloud:
    total file fwded :                                   0
    total file failed:                                   0
    total file skipped:                                  0
    total cloud queries:                                 0
    total cloud queries failed:                          0
    file forwarded in last minute:                       0
    bytes of concurrent files:                           0
  Private Cloud:
    total file fwded :                                   0
    total file failed:                                   0
    total file skipped:                                  0
    total cloud queries:                                 0
    total cloud queries failed:                          0
    file forwarded in last minute:                       0
    bytes of concurrent files:                           0

</member>
</result></response>
""", CLOUD_STATUS_DICT)

REGISTRATION_PUBLIC_FAILED = {
                      'Public': {'wildfile_cloud': 'Public', 'registration_result': 'failed due to invalid cloud info'},
                      'Private': {'wildfile_cloud': 'Private', 'registration_result': ''}
          }

REGISTRATION_PUBLIC_OK = {
                      'Public': {'wildfile_cloud': 'Public', 'registration_result': 'successful'},
                      'Private': {'wildfile_cloud': 'Private', 'registration_result': ''}
                      }

REGISTRATION_PRIVATE_FAILED = {
          'Public': {'wildfile_cloud': 'Public', 'registration_result': 'failed'},
          'Private': {'wildfile_cloud': 'Private', 'registration_result': 'failed'}
          }

TEST_WILDFIRE_REGISTRATION_FAILED = ("""
<response status="success"><result>
  <member>Test wildfire Public Cloud

	Testing cloud server wildfire.paloaltonetworks.com ...
	wildfire registration:         failed due to invalid cloud info

Test wildfire Private Cloud

	Cloud server is empty


</member>
</result></response>""", REGISTRATION_PUBLIC_FAILED)

TEST_WILDFIRE_REGISTRATION_OK = ("""
<response status="success"><result>
  <member>Test wildfire Public Cloud

	Testing cloud server wildfire.paloaltonetworks.com ...
	wildfire registration:         successful


	select the best server:        panos.wildfire.paloaltonetworks.com

Test wildfire Private Cloud

	Cloud server is empty


</member>
</result></response>""", REGISTRATION_PUBLIC_OK)

TEST_WILDFIRE_REGISTRATION_PRIVATE_FAILED = ("""
<response status="success">
    <result>
        <member>Test wildfire Public Cloud

	Testing cloud server wildfilre.paloaltonetworks.com ...
	wildfire registration:         failed
		Unable to resolve host

Test wildfire Private Cloud

	Testing cloud server 10.11.12.13 ...
	wildfire registration:         failed
		Connection timed out.


</member>
    </result>
</response>""", REGISTRATION_PRIVATE_FAILED)

CLOUD_REGISTRATION_OK = (REGISTRATION_PUBLIC_OK, 'Public', True)

CLOUD_REGISTRATION_FAILED = (REGISTRATION_PUBLIC_FAILED, 'Public', False)

CLOUD_REGISTRATION_PRIVATE_FAILED = (REGISTRATION_PRIVATE_FAILED, 'Private', False)

CLOUD_STATUS_PUBLIC = (CLOUD_STATUS_DICT, 'Public', True)

CLOUD_STATUS_PRIVATE = (CLOUD_STATUS_DICT, 'Private', True)

SERVER_CLOUD_PUBLIC = (CLOUD_STATUS_DICT, 'Public', True)

SERVER_CLOUD_PRIVATE = (CLOUD_STATUS_DICT, 'Private', False)
