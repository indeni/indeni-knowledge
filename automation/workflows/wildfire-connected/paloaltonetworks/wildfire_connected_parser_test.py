import unittest
import re
from indeni_workflow.remote.device_credentials import SshCredentials, MockCredentials
from indeni_workflow.remote.device_data import DeviceData
from indeni_workflow.remote.remote_type import RemoteType
from indeni_workflow.workflow import Workflow
from automation.workflows.wildfire_connected.paloaltonetworks.wildfire_connected_parser import *
import automation.workflows.wildfire_connected.paloaltonetworks.wildfire_connected_mock_data as mock


class WildfireTests(unittest.TestCase):

    def test_parse_wildfire_cloud_status(self):
        self.assertEqual(parse_wildfire_cloud_status(mock.SHOW_WILDFIRE_STATUS[0]), mock.SHOW_WILDFIRE_STATUS[1])

    def test_parse_wildfire_cloud_registration_ok(self):
        self.assertEqual(parse_wildfire_cloud_registration(mock.TEST_WILDFIRE_REGISTRATION_OK[0]), mock.TEST_WILDFIRE_REGISTRATION_OK[1])

    def test_parse_wildfire_private_cloud_registration_failed(self):
        self.assertEqual(parse_wildfire_cloud_registration(mock.TEST_WILDFIRE_REGISTRATION_PRIVATE_FAILED[0]), mock.TEST_WILDFIRE_REGISTRATION_PRIVATE_FAILED[1])

    def test_parse_compare_cloud_status_private(self):
        self.assertEqual(parse_compare_cloud_status(mock.CLOUD_STATUS_PRIVATE[0], mock.CLOUD_STATUS_PRIVATE[1]), mock.CLOUD_STATUS_PRIVATE[2])

    def test_parse_verify_wildfire_server_public(self):
        self.assertEqual(parse_verify_server_address(mock.SERVER_CLOUD_PUBLIC[0], mock.SERVER_CLOUD_PUBLIC[1]), mock.SERVER_CLOUD_PUBLIC[2])

    def test_parse_verify_wildfire_server_private(self):
        self.assertEqual(parse_verify_server_address(mock.SERVER_CLOUD_PRIVATE[0], mock.SERVER_CLOUD_PRIVATE[1]), mock.SERVER_CLOUD_PRIVATE[2])

    def test_parse_verify_device_registration_ok(self):
        self.assertEqual(parse_verify_device_registration(mock.CLOUD_REGISTRATION_OK[0], mock.CLOUD_REGISTRATION_OK[1]), mock.CLOUD_REGISTRATION_OK[2])

    def test_parse_verify_device_registration_failed(self):
        self.assertEqual(parse_verify_device_registration(mock.CLOUD_REGISTRATION_FAILED[0], mock.CLOUD_REGISTRATION_FAILED[1]), mock.CLOUD_REGISTRATION_FAILED[2])

    def test_parse_verify_device_registration_private_failed(self):
        self.assertEqual(parse_verify_device_registration(mock.CLOUD_REGISTRATION_PRIVATE_FAILED[0], mock.CLOUD_REGISTRATION_PRIVATE_FAILED[1]), mock.CLOUD_REGISTRATION_PRIVATE_FAILED[2])


if __name__ == '__main__':
    unittest.main()
