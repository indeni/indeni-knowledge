from parser_service.public import helper_methods


def check_variant_matches(**kwargs) -> bool:
    # This should work on all devices with one of the below versions
    tags = kwargs.get('my_device_tags', None)
    if tags is None:
        return False
    return True


def parse_proc_interrupts(raw_data: str) -> dict:
    dict_interface_cpu = {}

    if raw_data:
        data = helper_methods.parse_data_as_list(raw_data, 'checkpoint_interrupts.textfsm')

        if data:
            for line in data:
                cpuid = line['cpuid'].split()
                interrupts = line['interrupt'].split()
                interface = line['interface']
                i = 0

                if len(interrupts) > 0:
                    for cpu in cpuid:
                        if int(interrupts[i]) > 0:
                            if interface in dict_interface_cpu:
                                dict_interface_cpu[interface].append(int(cpu.replace('CPU', '')))
                            else:
                                dict_interface_cpu[interface] = [(int(cpu.replace('CPU', '')))]
                        i += 1
    return dict_interface_cpu


def parse_mpstat_P_ALL(raw_data: str, dict_interface_cpu) -> dict:
    dict_interface_cpu_percent_not_used = {}

    if raw_data:
        data = helper_methods.parse_data_as_list(raw_data, 'checkpoint_mpstat.textfsm')
        dict_cpu_to_usage = {}
        if data:
            for cpu_not_used in data:
                if float(cpu_not_used['kernel']) == 2.6:
                    dict_cpu_to_usage[int(cpu_not_used['cpuid'])] = float(cpu_not_used['idle'])
                elif float(cpu_not_used['kernel']) == 3.10:
                    dict_cpu_to_usage[int(cpu_not_used['cpuid'])] = float(cpu_not_used['idle3'])

            for key, values in dict_interface_cpu.items():
                temp = []
                for value in values:
                    temp.append(dict_cpu_to_usage[value])
                dict_interface_cpu_percent_not_used[key] = temp
    return dict_interface_cpu_percent_not_used


def check_if_cpu_usage_high(dict_interface_cpu_percent_not_used: dict, interface_name: str) -> bool:
    if dict_interface_cpu_percent_not_used:
        if interface_name in dict_interface_cpu_percent_not_used:
            for cpu_not_used in dict_interface_cpu_percent_not_used[interface_name]:
                cpu_usage = 100 - cpu_not_used
                if float(cpu_usage) > 80.00:
                    return True
    return False


def parse_interface_configuration(raw_data: str, interface_name: str) -> dict:
    if raw_data:
        data = helper_methods.parse_data_as_object(raw_data, 'checkpoint_ethtool_configuration.textfsm')
        if data:
            return data
    return False


def check_interface_configuration_per_best_practices(parsed_interface_configuration: dict) -> bool:
    if parsed_interface_configuration:
        if (parsed_interface_configuration['negotiate']) == 'on' and (
                parsed_interface_configuration['duplex']) == 'Half':
            return False
        elif (parsed_interface_configuration['negotiate']) == 'off' and (
                parsed_interface_configuration['duplex']) == 'Full':
            return False
        elif (parsed_interface_configuration['negotiate']) == 'off' and (
                parsed_interface_configuration['duplex']) == 'Half':
            return False
        return True
    return False


def parse_interface_buffer_size(raw_data: str, interface_name: str):
    if raw_data:
        data = helper_methods.parse_data_as_list(raw_data, 'checkpoint_ring_bufersize.textfsm')
        if data:
            return data
    return False


def check_interface_ring_buffer_size_if_max(parsed_ring_buffer_size: dict) -> bool:
    if parsed_ring_buffer_size:
        max_ring_size = 0
        for buffer in parsed_ring_buffer_size:
            if buffer['ring_limit_current'] == 'maximums':
                max_ring_size = int(buffer['rx_set'])
            elif buffer['ring_limit_current'] == '':
                curr_ring_size = int(buffer['rx_set'])
                if max_ring_size > curr_ring_size:
                    return False
                else:
                    return True
    return False
