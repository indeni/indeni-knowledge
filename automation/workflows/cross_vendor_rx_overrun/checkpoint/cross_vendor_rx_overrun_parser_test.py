import automation.workflows.cross_vendor_rx_overrun.checkpoint.cross_vendor_rx_overrun_mock_data as mock
import automation.workflows.cross_vendor_rx_overrun.checkpoint.cross_vendor_rx_overrun_parser as parser
import unittest



class RingSizeParserTest(unittest.TestCase):

    def test_parse_proc_interrupts(self):
        pair1 = mock.GET_DICT_INTERFACE_CPU_PAIR_1
        pair2 = mock.GET_DICT_INTERFACE_CPU_PAIR_2
        self.assertEqual(pair1[1], parser.parse_proc_interrupts(pair1[0]))
        self.assertEqual(pair2[1], parser.parse_proc_interrupts(pair2[0]))

    def test_parse_mpstat_P_ALL(self):
        pair1 = mock.GET_MPSTAT_P_ALL_PAIR_1
        pair2 = mock.GET_MPSTAT_P_ALL_PAIR_2
        self.assertEqual(pair1[1], parser.parse_mpstat_P_ALL(pair1[0][0], pair1[0][1]))
        self.assertEqual(pair2[1], parser.parse_mpstat_P_ALL(pair2[0][0], pair2[0][1]))

    def test_check_if_cpu_usage_high(self):
        pair1 = mock.GET_CHECK_IF_CPU_HIGH_USAGE_PAIR_1
        pair2 = mock.GET_CHECK_IF_CPU_HIGH_USAGE_PAIR_2
        self.assertEqual(pair1[1], parser.check_if_cpu_usage_high(pair1[0][0], pair1[0][1]))
        self.assertEqual(pair2[1], parser.check_if_cpu_usage_high(pair2[0][0], pair2[0][1]))

    def test_parse_interface_configuration(self):
        pair1 = mock.GET_PARSE_INTERFACE_CONFIGURATION_PAIR_1
        pair2 = mock.GET_PARSE_INTERFACE_CONFIGURATION_PAIR_2
        self.assertEqual(pair1[1], parser.parse_interface_configuration(pair1[0][0], pair1[0][1]))
        self.assertEqual(pair2[1], parser.parse_interface_configuration(pair2[0][0], pair2[0][1]))

    def test_check_interface_configuration_per_best_practices(self):
        pair1 = mock.GET_CHECK_INTERFACE_CONFIGURATION_PER_BEST_PRACTICES_PAIR_1
        pair2 = mock.GET_CHECK_INTERFACE_CONFIGURATION_PER_BEST_PRACTICES_PAIR_2
        self.assertEqual(pair1[1], parser.check_interface_configuration_per_best_practices(pair1[0]))
        self.assertEqual(pair2[1], parser.check_interface_configuration_per_best_practices(pair2[0]))

    def test_parse_interface_buffer_size(self):
        pair1 = mock.GET_PARSE_INTERFACE_BUFFER_SIZE_PAIR_1
        pair2 = mock.GET_PARSE_INTERFACE_BUFFER_SIZE_PAIR_2
        self.assertEqual(pair1[1], parser.parse_interface_buffer_size(pair1[0][0], pair1[0][1]))
        self.assertEqual(pair2[1], parser.parse_interface_buffer_size(pair2[0][0], pair2[0][1]))

    def test_check_interface_ring_buffer_size_if_max(self):
        pair1 = mock.GET_CHECK_INTERFACE_RING_BUFFER_SIZE_IF_MAX_PAIR_1
        pair2 = mock.GET_CHECK_INTERFACE_RING_BUFFER_SIZE_IF_MAX_PAIR_2
        self.assertEqual(pair1[1], parser.check_interface_ring_buffer_size_if_max(pair1[0]))
        self.assertEqual(pair2[1], parser.check_interface_ring_buffer_size_if_max(pair2[0]))


if __name__ == '__main__':
    unittest.main()

