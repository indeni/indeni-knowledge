# SSH command: cat /proc/interrupts
PROC_INTERRUPTS_1 = """
         CPU0       CPU1       CPU2       CPU3       
  0: 2083417612          0          0          0    IO-APIC-edge  timer
  1:          8          0          0          0    IO-APIC-edge  i8042
  6:          5          0          0          0    IO-APIC-edge  floppy
  8:     125235          0          0          0    IO-APIC-edge  rtc
  9:          0          0          0          0   IO-APIC-level  acpi
 12:        114          0          0          0    IO-APIC-edge  i8042
 15:      63869          0          0          0    IO-APIC-edge  ide1
 59:   86926609          0          0          0   IO-APIC-level  ioc0
139: 1410362369          0          0          0         PCI-MSI  eth0
171:  219683755          0          0          0         PCI-MSI  eth2
203:  283842874          0          0          0         PCI-MSI  eth1
219:  298607609          0          0          0         PCI-MSI  eth3
235:  126291851          0          0          0         PCI-MSI  eth4
NMI:          0          0          0          0 
LOC:  737763017  752341583  752259783  752398321 
ERR:          0
MIS:          0
"""
GET_DICT_INTERFACE_CPU_PAIR_1 = (PROC_INTERRUPTS_1,{'eth0': [0], 'eth2': [0], 'eth1': [0], 'eth3': [0], 'eth4': [0]})

# SSH command: cat /proc/interrupts
PROC_INTERRUPTS_2 = """
         CPU0       CPU1       CPU2       CPU3       
  0: 2083417612          0          0          0    IO-APIC-edge  timer
  1:          8          0          0          0    IO-APIC-edge  i8042
  6:          5          0          0          0    IO-APIC-edge  floppy
  8:     125235          0          0          0    IO-APIC-edge  rtc
  9:          0          0          0          0   IO-APIC-level  acpi
 12:        114          0          0          0    IO-APIC-edge  i8042
 15:      63869          0          0          0    IO-APIC-edge  ide1
 59:   86926609          0          0          0   IO-APIC-level  ioc0
139: 1410362369        100          0          0         PCI-MSI  eth0
171:  219683755          0          0          0         PCI-MSI  eth2
203:  283842874          0       1000          0         PCI-MSI  eth1
219:  298607609          0          0          0         PCI-MSI  eth3
235:  126291851          0          0          0         PCI-MSI  eth4
NMI:          0          0          0          0 
LOC:  737763017  752341583  752259783  752398321 
ERR:          0
MIS:          0
"""
GET_DICT_INTERFACE_CPU_PAIR_2 = (PROC_INTERRUPTS_2,{'eth0': [0, 1], 'eth2': [0], 'eth1': [0, 2], 'eth3': [0], 'eth4': [0]})

# SSH command: mpstat -P ALL
MPSTAT_P_ALL_1 = """
Linux 2.6.18-92cpx86_64 (CP-R80.30-GW1-1) 	06/15/20

03:22:34     CPU   %user   %nice    %sys %iowait    %irq   %soft  %steal   %idle    intr/s
03:22:34     all    4.12    8.27    5.18    1.88    0.07    1.42    0.00   79.07   4217.02
03:22:34       0    0.03    0.00    0.60    6.00    0.26    1.39    0.00   91.71   4189.82
03:22:34       1    5.50   11.05    6.76    0.52    0.00    1.54    0.00   76.16     13.25
03:22:34       2    5.54   10.98    6.72    0.52    0.01    1.58    0.00   76.19      7.29
03:22:34       3    5.61   11.43    6.87    0.56    0.01    1.21    0.00   75.90      6.65

"""
GET_MPSTAT_P_ALL_PAIR_1 = ((MPSTAT_P_ALL_1,GET_DICT_INTERFACE_CPU_PAIR_1[1]),{'eth0': [91.71], 'eth2': [91.71], 'eth1': [91.71], 'eth3': [91.71], 'eth4': [91.71]})


# SSH command: mpstat -P ALL

MPSTAT_P_ALL_2 = """
Linux 3.10.0-693cpx86_64 (ENG-CP-R80.20-MGMT1-1) 	06/15/20 	_x86_64_	(4 CPU)

18:44:40     CPU    %usr   %nice    %sys %iowait    %irq   %soft  %steal  %guest  %gnice   %idle
18:44:40     all    6.84    0.64    1.46    1.04    0.00    0.05    0.00    0.00    0.00   89.98
18:44:40       0    5.99    0.58    1.53    0.86    0.00    0.13    0.00    0.00    0.00   0.92
18:44:40       1    7.56    0.62    1.43    1.08    0.00    0.03    0.00    0.00    0.00   89.28
18:44:40       2    7.48    0.69    1.46    1.14    0.00    0.02    0.00    0.00    0.00   89.21
18:44:40       3    6.33    0.64    1.41    1.07    0.00    0.02    0.00    0.00    0.00   90.53
"""
GET_MPSTAT_P_ALL_PAIR_2 = ((MPSTAT_P_ALL_2, GET_DICT_INTERFACE_CPU_PAIR_2[1]), {'eth0': [0.92, 89.28], 'eth2': [0.92], 'eth1': [0.92, 89.21], 'eth3': [0.92], 'eth4': [0.92]})

# methods: parse_mpstat_P_ALL, parse_proc_interrupts
CHECK_IF_CPU_HIGH_USAGE_1 = {'eth0': [91.71], 'eth2': [91.71], 'eth1': [91.71], 'eth3': [91.71], 'eth4': [91.71]}

GET_CHECK_IF_CPU_HIGH_USAGE_PAIR_1 = ((CHECK_IF_CPU_HIGH_USAGE_1, 'eth0'), False)

# methods: parse_mpstat_P_ALL, parse_proc_interrupts
CHECK_IF_CPU_HIGH_USAGE_2 = {'eth0': [0.92, 89.28], 'eth2': [0.92], 'eth1': [0.92, 89.21], 'eth3': [0.92], 'eth4': [0.92]}
GET_CHECK_IF_CPU_HIGH_USAGE_PAIR_2 = ((CHECK_IF_CPU_HIGH_USAGE_2, 'eth3'), True)


# SSH command: ethtool eth0
PARSE_INTERFACE_CONFIGURATION_1 = """
Settings for eth0:
	Supported ports: [ TP ]
	Supported link modes:   10baseT/Half 10baseT/Full 
	                        100baseT/Half 100baseT/Full 
	                        1000baseT/Full 
	Supports auto-negotiation: Yes
	Advertised link modes:  10baseT/Half 10baseT/Full 
	                        100baseT/Half 100baseT/Full 
	                        1000baseT/Full 
	Advertised auto-negotiation: Yes
	Speed: 1000Mb/s
	Duplex: Full
	Port: Twisted Pair
	PHYAD: 1
	Transceiver: internal
	Auto-negotiation: on
	Supports Wake-on: pumbg
	Wake-on: g
	Current message level: 0x00000007 (7)
	Link detected: yes
	"""

GET_PARSE_INTERFACE_CONFIGURATION_PAIR_1 = ((PARSE_INTERFACE_CONFIGURATION_1, 'eth0'),{'duplex': 'Full', 'negotiate': 'on'})

# SSH command: ethtool eth0
PARSE_INTERFACE_CONFIGURATION_2 = """
Settings for eth0:
	Supported ports: [ TP ]
	Supported link modes:   10baseT/Half 10baseT/Full 
	                        100baseT/Half 100baseT/Full 
	                        1000baseT/Full 
	Supports auto-negotiation: Yes
	Advertised link modes:  10baseT/Half 10baseT/Full 
	                        100baseT/Half 100baseT/Full 
	                        1000baseT/Full 
	Advertised auto-negotiation: Yes
	Speed: 1000Mb/s
	Duplex: Half
	Port: Twisted Pair
	PHYAD: 1
	Transceiver: internal
	Auto-negotiation: on
	Supports Wake-on: pumbg
	Wake-on: g
	Current message level: 0x00000007 (7)
	Link detected: yes
"""

GET_PARSE_INTERFACE_CONFIGURATION_PAIR_2 = ((PARSE_INTERFACE_CONFIGURATION_2, 'eth0'),{'duplex': 'Half', 'negotiate': 'on'})



#method parse_interface_configuration:
CHECK_INTERFACE_CONFIGURATION_PER_BEST_PRACTICES_1 = {'duplex': 'Full', 'negotiate': 'on'}
GET_CHECK_INTERFACE_CONFIGURATION_PER_BEST_PRACTICES_PAIR_1 = (CHECK_INTERFACE_CONFIGURATION_PER_BEST_PRACTICES_1, True)

#method parse_interface_configuration:
CHECK_INTERFACE_CONFIGURATION_PER_BEST_PRACTICES_2 = {'duplex': 'Half', 'negotiate': 'on'}
GET_CHECK_INTERFACE_CONFIGURATION_PER_BEST_PRACTICES_PAIR_2 = (CHECK_INTERFACE_CONFIGURATION_PER_BEST_PRACTICES_2, False)


#SSH command: ethtool -g eth0:
PARSE_INTERFACE_BUFFER_SIZE_1 = """
Ring parameters for eth0:
Pre-set maximums:
RX:		4096
RX Mini:	0
RX Jumbo:	0
TX:		4096
Current hardware settings:
RX:		4096
RX Mini:	0
RX Jumbo:	0
TX:		1024
"""
GET_PARSE_INTERFACE_BUFFER_SIZE_PAIR_1 = ((PARSE_INTERFACE_BUFFER_SIZE_1, 'eth1'), [{'ring_limit_current': 'maximums', 'rx_set': '4096'}, {'ring_limit_current': '', 'rx_set': '4096'}])


#SSH command: ethtool -g eth0:
PARSE_INTERFACE_BUFFER_SIZE_2 = """
Ring parameters for eth0:
Pre-set maximums:
RX:		4096
RX Mini:	0
RX Jumbo:	0
TX:		4096
Current hardware settings:
RX:		256
RX Mini:	0
RX Jumbo:	0
TX:		1024
"""
GET_PARSE_INTERFACE_BUFFER_SIZE_PAIR_2 = ((PARSE_INTERFACE_BUFFER_SIZE_2, 'eth0'), [{'ring_limit_current': 'maximums', 'rx_set': '4096'}, {'ring_limit_current': '', 'rx_set': '256'}])


#SSH command: method check_interface_ring_buffer_size_if_max:
CHECK_INTERFACE_RING_BUFFER_SIZE_IF_MAX_1 = [{'ring_limit_current': 'maximums', 'rx_set': '4096'}, {'ring_limit_current': '', 'rx_set': '256'}]

GET_CHECK_INTERFACE_RING_BUFFER_SIZE_IF_MAX_PAIR_1 = (CHECK_INTERFACE_RING_BUFFER_SIZE_IF_MAX_1, False)

#SSH command: method check_interface_ring_buffer_size_if_max:
CHECK_INTERFACE_RING_BUFFER_SIZE_IF_MAX_2 = [{'ring_limit_current': 'maximums', 'rx_set': '4096'}, {'ring_limit_current': '', 'rx_set': '4096'}]
GET_CHECK_INTERFACE_RING_BUFFER_SIZE_IF_MAX_PAIR_2 = (CHECK_INTERFACE_RING_BUFFER_SIZE_IF_MAX_2, True)


