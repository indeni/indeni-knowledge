import unittest
import automation.workflows.cross_vendor_dns_failure.paloaltonetworks.cross_vendor_dns_failure_mock_data as mock
import automation.workflows.cross_vendor_dns_failure.paloaltonetworks.cross_vendor_dns_failure_parser as parser


class CrossVendorDNSFAilureParserTests(unittest.TestCase):


    def test_panos_dns_servers_with_interfaces(self):
        pair1 = mock.PANOS_DNS_SERVERS_WITH_INTERFACES_PAIR_1
        pair2 = mock.PANOS_DNS_SERVERS_WITH_INTERFACES_PAIR_2
        self.assertEqual(pair1[1], parser.panos_dns_servers_with_interfaces(pair1[0]))
        self.assertEqual(pair2[1], parser.panos_dns_servers_with_interfaces(pair2[0]))

    def test_panos_parse_ping_packets_lost(self):
        pair1 = mock.PANOS_PARSE_PING_PACKETS_LOST_PAIR_1
        pair2 = mock.PANOS_PARSE_PING_PACKETS_LOST_PAIR_2
        self.assertEqual(pair1[1], parser.panos_parse_ping_packets_lost(pair1[0]))
        self.assertEqual(pair2[1], parser.panos_parse_ping_packets_lost(pair2[0]))

    def panos_parse_traceroute_last_hop(self):
        pair1 = mock.PANOS_PARSE_TRACEROUTE_LAST_HOP_PAIR_1
        self.assertEqual(pair1[1], parser.parse_traceroute_last_hop(pair1[0]))


if __name__ == '__main__':
    unittest.main()