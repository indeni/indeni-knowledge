#HTTPS command: /api/?type=config&action=show&xpath=/config/devices/entry/deviceconfig/system
PANOS_DNS_SERVERS_WITH_INTERFACES_PAIR_1 = ("""<response status="success"><result><system>
        <type>
            <static/>
        </type>
        <update-server>updates.paloaltonetworks.com</update-server>
        <update-schedule>
            <threats>
            <recurring>
                <weekly>
                <day-of-week>wednesday</day-of-week>
                <at>01:02</at>
                <action>download-only</action>
                </weekly>
            </recurring>
            </threats>
        </update-schedule>
        <timezone>US/Pacific</timezone>
        <service>
            <disable-telnet>yes</disable-telnet>
            <disable-http>yes</disable-http>
        </service>
        <hostname>illab-panfwa02</hostname>
        <ip-address>10.11.95.32</ip-address>
        <netmask>255.255.255.0</netmask>
        <default-gateway>10.11.95.254</default-gateway>
        <dns-setting>
            <servers>
            <primary>8.8.8.8</primary>
            <secondary>8.8.8.8</secondary>
            </servers>
        </dns-setting>
        <ntp-servers>
            <secondary-ntp-server>
            <ntp-server-address>1.1.1.1</ntp-server-address>
            <authentication-type>
                <none/>
            </authentication-type>
            </secondary-ntp-server>
            <primary-ntp-server>
            <ntp-server-address>3.3.3.3</ntp-server-address>
            <authentication-type>
                <none/>
            </authentication-type>
            </primary-ntp-server>
        </ntp-servers>
        <panorama-server>10.11.95.29</panorama-server>
        <panorama-server-2>10.11.95.30</panorama-server-2>
        </system></result></response>
        """,
        {'servers': ['8.8.8.8', '8.8.8.8'], 
        'dns_source_interface': '', 
        'dns_source_address': ''
        })

PANOS_DNS_SERVERS_WITH_INTERFACES_PAIR_2 = ("""
        <response status="success">
            <result>
                <system>
                    <type>
                        <static/>
                    </type>
                    <update-server>updates.paloaltonetworks.com</update-server>
                    <update-schedule>
                        <threats>
                            <recurring>
                                <weekly>
                                    <day-of-week>wednesday</day-of-week>
                                    <at>01:02</at>
                                    <action>download-only</action>
                                </weekly>
                            </recurring>
                        </threats>
                    </update-schedule>
                    <timezone>US/Pacific</timezone>
                    <service>
                        <disable-telnet>yes</disable-telnet>
                        <disable-http>yes</disable-http>
                    </service>
                    <hostname>illab-panfwa02</hostname>
                    <ip-address>10.11.95.32</ip-address>
                    <netmask>255.255.255.0</netmask>
                    <default-gateway>10.11.95.254</default-gateway>
                    <dns-setting>
                        <servers>
                            <primary>8.8.8.8</primary>
                            <secondary>8.8.8.8</secondary>
                        </servers>
                    </dns-setting>
                    <ntp-servers>
                        <secondary-ntp-server>
                            <ntp-server-address>1.1.1.1</ntp-server-address>
                            <authentication-type>
                                <none/>
                            </authentication-type>
                        </secondary-ntp-server>
                        <primary-ntp-server>
                            <ntp-server-address>3.3.3.3</ntp-server-address>
                            <authentication-type>
                                <none/>
                            </authentication-type>
                        </primary-ntp-server>
                    </ntp-servers>
                    <panorama-server>10.11.95.29</panorama-server>
                    <panorama-server-2>10.11.95.30</panorama-server-2>
                    <route>
                        <service>
                            <entry name="dns">
                                <source>
                                    <address>172.30.250.1/29</address>
                                    <interface>ethernet1/1</interface>
                                </source>
                            </entry>
                        </service>
                    </route>
                </system>
            </result>
        </response>
        """,
        {
            'servers': ['8.8.8.8', '8.8.8.8'], 
            'dns_source_interface': 'ethernet1/1', 
            'dns_source_address': '172.30.250.1/29'
            })

#SSH command: ping
PANOS_PARSE_PING_PACKETS_LOST_PAIR_1 = ("""
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=51 time=59.1 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=51 time=59.3 ms

--- 8.8.8.8 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1000ms
rtt min/avg/max/mdev = 59.197/59.270/59.344/0.254 ms
""",
"0"
)

PANOS_PARSE_PING_PACKETS_LOST_PAIR_2 = ("""
PING 2.2.2.2 (2.2.2.2) 56(84) bytes of data.

--- 2.2.2.2 ping statistics ---
2 packets transmitted, 0 received, 100% packet loss, time 1000ms
""",
"100"
)

#SSH command: traceroute
PANOS_PARSE_TRACEROUTE_LAST_HOP_PAIR_1 = ("""traceroute to 172.18.10.71 (172.18.10.71), 30 hops max, 40 byte packets
 1  172.18.13.254 (172.18.13.254)  0.418 ms 0.540 ms 0.635 ms
 2  dc-01.domain.lan (172.18.10.71)  0.188 ms 0.196 ms 0.232 ms
""",
"(172.18.10.71)"
)
