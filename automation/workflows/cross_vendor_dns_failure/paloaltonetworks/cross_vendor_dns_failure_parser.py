import re
from typing import List
from dateutil import parser
from parser_service.public.helper_methods import *
import ipaddress


def check_variant_matches(**kwargs) -> bool:
    # This should work on all devices
    tags = kwargs.get('my_device_tags', None)
    if tags is None:
        return False
    return True


def panos_dns_servers_with_interfaces(raw_data: str) -> List[str]:
    parsed = parse_data_as_xml(raw_data)
    dns_servers = []
    dns_server_config = {}
    #TODO: what if no server is configured?
    dns_servers.append(parsed['response']['result']['system']['dns-setting']['servers']['primary'])
    dns_servers.append(parsed['response']['result']['system']['dns-setting']['servers']['secondary'])
    dns_server_config['servers'] = dns_servers
    #Check if some service route exists
    if 'route' in parsed['response']['result']['system'].keys():
        #Check if service route exist for DNS service
        service_routes = parsed['response']['result']['system']['route']['service']['entry']
        for entry in service_routes:
            if  service_routes['@name'] == "dns":
                #Collect data of service route
                dns_server_config['dns_source_interface'] = service_routes['source']['interface']
                dns_server_config['dns_source_address'] = service_routes['source']['address']
    else:
        dns_server_config['dns_source_interface'] = ""
        dns_server_config['dns_source_address'] = ""
    return dns_server_config

def panos_parse_ping_packets_lost(raw_data: str) -> list:
    raw_data_parsed = parse_data_as_list(raw_data, 'paloalto_panos_ping.textfsm')
    return raw_data_parsed[0]['pkt_loss']

def panos_parse_traceroute_last_hop(raw_data: str) -> str:
    data_list = raw_data.splitlines()
    for line in data_list:
        if (len(line.split()) > 0) :
            last_line = line
    return last_line.split()[2]
