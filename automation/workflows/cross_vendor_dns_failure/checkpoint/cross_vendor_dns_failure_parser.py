import ipaddress
import re


def check_variant_matches(**kwargs) -> bool:
    # This should work on all devices with one of the below versions
    tags = kwargs.get('my_device_tags', None)
    if tags is None:
        return False
    versions = [x.lower() for x in ['R77.30', 'R80.10', 'R80.20', 'R80.30', 'R76SP.30', 'R76SP.40', 'R76SP.50', 'R80.20SP', 'R80.40']]
    if tags.get('os.version', '').lower() not in versions:
        return False
    return True


def is_dns_lookup_still_failing(raw_data: str) -> bool:
    output = raw_data.strip()
    if output == '' or 'connection timed out' in output or 'no servers could be reached' in output:
        return True
    else:
        return False


def is_dns_server_configured(raw_data: str) -> bool:
    for level in ['primary', 'secondary', 'tertiary']:
        if level in raw_data:
            return True
    return False


def parse_routing_interface(raw_data: str) -> str:
    lines = raw_data.strip().split('\n')
    return lines[0].split(' ')[4]


def is_interface_admin_state_up(raw_data: str) -> bool:
    lines = raw_data.strip().split('\n')
    for line in lines:
        fixed_line = re.sub(r'\s+', ' ', line.strip())
        if 'MTU' in fixed_line:
            return fixed_line.split(' ')[0] == 'UP'
    return False


def is_interface_link_state_up(raw_data: str) -> bool:
    return re.search(r'Link detected:\s+yes', raw_data) is not None


def is_dns_ping_received(raw_data: str) -> bool:
    return '10 received' in raw_data


def is_ip_address_private(**kwargs) -> bool:
    raw_data = kwargs.get('dns_server_item', '')
    return ipaddress.IPv4Address(raw_data.strip()).is_private
