#SSH command: dig @{{dns_server_item}} www.indeni.com +noall +stats +answer +time=1 +short
IS_DNS_LOOKUP_STILL_FAILING_DATA_1 = ('', True)

IS_DNS_LOOKUP_STILL_FAILING_DATA_2 = ("""indeni.com.
67.222.18.206
""", False)

#SSH command: clish -c  "show configuration dns"
IS_DNS_SERVER_CONFIGURED_DATA_1 = ("""set dns suffix indeni.com
""", False)

IS_DNS_SERVER_CONFIGURED_DATA_2 = ("""set dns suffix indeni.com
set dns primary 8.8.8.8
""", True)

#SSH command: ip r get
PARSE_ROUTING_INTERFACE_DATA_1 = ("""150.214.94.5 via 10.11.94.254 dev eth0 src 10.11.94.49
    cache mtu 1500 advmss 1460 hoplimit 64
""", 'eth0')

PARSE_ROUTING_INTERFACE_DATA_2 = ("""150.214.94.5 via 10.11.94.254 dev eth1.6 src 10.11.94.49
    cache mtu 1500 advmss 1460 hoplimit 64
""", 'eth1.6')

#SSH command: ifconfig
IS_INTERFACE_ADMIN_STATE_UP_DATA_1 = ("""eth1.6     Link encap: Ethernet    HWaddress 00:50:56:AC:B7:2A
inet addr:30.0.0.1  Bcast
UP BROADCAST RUNNING MULTICAST  MTU:1500    Metric:1
""", True)

IS_INTERFACE_ADMIN_STATE_UP_DATA_2 = ("""eth1.6     Link encap: Ethernet    HWaddress 00:50:56:AC:B7:2A
inet addr:30.0.0.1  Bcast
BROADCAST RUNNING MULTICAST  MTU:1500    Metric:1
""", False)

#SSH command: ethtool
IS_INTERFACE_LINK_STATE_UP_DATA_1 = ("""Settings for docker0:
Cannot get wake-on-lan settings: Operation not permitted
	Link detected: no
""", False)

IS_INTERFACE_LINK_STATE_UP_DATA_2 = ("""DATA
CONTAINING SOME INFORMATION
INCLUDING Link detected: yes""", True)

#SSH command: ping
IS_DNS_PING_RECEIVED_DATA_1 = ("""PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=50 time=70.2 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=50 time=70.7 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=50 time=71.7 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=50 time=71.0 ms
64 bytes from 8.8.8.8: icmp_seq=5 ttl=50 time=70.8 ms
64 bytes from 8.8.8.8: icmp_seq=6 ttl=50 time=68.5 ms
64 bytes from 8.8.8.8: icmp_seq=7 ttl=50 time=68.7 ms
64 bytes from 8.8.8.8: icmp_seq=8 ttl=50 time=68.4 ms
64 bytes from 8.8.8.8: icmp_seq=9 ttl=50 time=68.5 ms
64 bytes from 8.8.8.8: icmp_seq=10 ttl=50 time=71.0 ms

--- 8.8.8.8 ping statistics ---
10 packets transmitted, 10 received, 0% packet loss, time 9014ms
rtt min/avg/max/mdev = 68.438/69.987/71.712/1.228 ms
""", True)

IS_DNS_PING_RECEIVED_DATA_2 = ("""PING 0.1.2.3 (0.1.2.3) 56(84) bytes of data.

--- 0.1.2.3 ping statistics ---
10 packets transmitted, 0 received, 100% packet loss, time 9203ms

➜""", False)

IS_IP_ADDRESS_PRIVATE_DATA_1 = ('10.255.255.255 ', True)
IS_IP_ADDRESS_PRIVATE_DATA_2 = ('172.31.255.255', True)
IS_IP_ADDRESS_PRIVATE_DATA_3 = ('192.168.255.255', True)
IS_IP_ADDRESS_PRIVATE_DATA_4 = ('1.1.1.1', False)
