import unittest
import automation.workflows.cross_vendor_dns_failure.checkpoint.cross_vendor_dns_failure_mock_data as mock
import automation.workflows.cross_vendor_dns_failure.checkpoint.cross_vendor_dns_failure_parser as parser



class TestDNSLookupFailureTest(unittest.TestCase):


    def test_is_dns_lookup_still_failing(self):
        pair1 = mock.IS_DNS_LOOKUP_STILL_FAILING_DATA_1
        pair2 = mock.IS_DNS_LOOKUP_STILL_FAILING_DATA_2
        self.assertEqual(pair1[1], parser.is_dns_lookup_still_failing(pair1[0]))
        self.assertEqual(pair2[1], parser.is_dns_lookup_still_failing(pair2[0]))

    def test_is_dns_server_configured(self):
        pair1 = mock.IS_DNS_SERVER_CONFIGURED_DATA_1
        pair2 = mock.IS_DNS_SERVER_CONFIGURED_DATA_1
        self.assertEqual(pair1[1], parser.is_dns_server_configured(pair1[0]))
        self.assertEqual(pair2[1], parser.is_dns_server_configured(pair2[0]))

    def test_parse_routing_interface(self):
        pair1 = mock.PARSE_ROUTING_INTERFACE_DATA_1
        pair2 = mock.PARSE_ROUTING_INTERFACE_DATA_2
        self.assertEqual(pair1[1], parser.parse_routing_interface(pair1[0]))
        self.assertEqual(pair2[1], parser.parse_routing_interface(pair2[0]))

    def test_is_interface_admin_state_up(self):
        pair1 = mock.IS_INTERFACE_ADMIN_STATE_UP_DATA_1
        pair2 = mock.IS_INTERFACE_ADMIN_STATE_UP_DATA_2
        self.assertEqual(pair1[1], parser.is_interface_admin_state_up(pair1[0]))
        self.assertEqual(pair2[1], parser.is_interface_admin_state_up(pair2[0]))

    def test_is_interface_link_state_up(self):
        pair1 = mock.IS_INTERFACE_LINK_STATE_UP_DATA_1
        pair2 = mock.IS_INTERFACE_LINK_STATE_UP_DATA_2
        self.assertEqual(pair1[1], parser.is_interface_link_state_up(pair1[0]))
        self.assertEqual(pair2[1], parser.is_interface_link_state_up(pair2[0]))

    def test_is_dns_ping_received(self):
        pair1 = mock.IS_DNS_PING_RECEIVED_DATA_1
        pair2 = mock.IS_DNS_PING_RECEIVED_DATA_2
        self.assertEqual(pair1[1], parser.is_dns_ping_received(pair1[0]))
        self.assertEqual(pair2[1], parser.is_dns_ping_received(pair2[0]))

    def test_is_ip_address_private(self):
        pair1 = mock.IS_IP_ADDRESS_PRIVATE_DATA_1
        pair2 = mock.IS_IP_ADDRESS_PRIVATE_DATA_2
        pair3 = mock.IS_IP_ADDRESS_PRIVATE_DATA_3
        pair4 = mock.IS_IP_ADDRESS_PRIVATE_DATA_4
        self.assertEqual(pair1[1], parser.is_ip_address_private(dns_server_item=pair1[0]))
        self.assertEqual(pair2[1], parser.is_ip_address_private(dns_server_item=pair2[0]))
        self.assertEqual(pair3[1], parser.is_ip_address_private(dns_server_item=pair3[0]))
        self.assertEqual(pair4[1], parser.is_ip_address_private(dns_server_item=pair4[0]))


if __name__ == '__main__':
    unittest.main()
