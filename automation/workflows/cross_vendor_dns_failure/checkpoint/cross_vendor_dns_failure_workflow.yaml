id: dns_failure_checkpoint
friendly_name: DNS lookup failure
start_block: get_device_tags

blocks:
  get_device_tags:
    type: device_tags
    name: Get device tags
    register_to: my_device_tags
    go_to: make_variant_matches

  make_variant_matches:
    type: logic
    name: Match device against variant
    args: [my_device_tags]
    method: check_variant_matches
    register_to: bool_variant_matches
    go_to: check_if_variant_matches

  check_if_variant_matches:
    type: if
    name: Check if device matches requirements
    condition: bool_variant_matches
    then_go_to: get_issue_items
    else_go_to: variant_conclusion

  variant_conclusion:
    type: conclusion
    name: Incompatible workflow
    triage_conclusion: |
      This workflow is incompatible with the device.
    triage_remediation_steps: |
      Contact Indeni and request that compatibility be added.

  get_issue_items:
    type: issue_items
    name: Get issue items
    register_to: my_items
    go_to: start_loop

  start_loop:
    type: foreach
    name: Start loop
    register_item_to: dns_server_item
    start_block: check_DNS_server_still_fail
    blocks:

      check_DNS_server_still_fail:
        type: device_task
        name:  Test DNS lookup
        runner:
          type: SSH
          command: dig @{{dns_server_item}} www.indeni.com +noall +stats +answer +time=1 +short
        parser:
          args: []
          method: is_dns_lookup_still_failing
        register_to: bool_dns_lookup_still_failing
        go_to: if_check_the_DNS_lookup_fail

      if_check_the_DNS_lookup_fail:
        type: if
        name: Check if DNS lookup still fails
        condition: bool_dns_lookup_still_failing
        then_go_to: check_DNS_configuration
        else_go_to: problem_solved

      problem_solved:
        type: conclusion
        name: DNS server is resolving name queries
        triage_conclusion: Issue is now resolved
        triage_remediation_steps: Not required, as the issue is now resolved

      check_DNS_configuration:
        type: device_task
        name: Collect DNS configuration
        runner:
          type: SSH
          command: clish -c "show configuration dns"
        parser:
          args: []
          method: is_dns_server_configured
        register_to: bool_dns_configuration_exists
        go_to: check_if_DNS_configured

      check_if_DNS_configured:
        type: if
        name: Check if DNS is configured
        condition: bool_dns_configuration_exists
        then_go_to: routing_interface_collect
        else_go_to: triage_DNS_server_not_configured

      triage_DNS_server_not_configured:
        type: conclusion
        name: DNS server is not configured
        triage_conclusion: |
          DNS server configuration mismatch between Gaia and Linux.
        triage_remediation_steps: |
            Add server as DNS server in Gaia OS:
            set dns primary|secondary|tertiary {{dns_server_item}}
            save config

      routing_interface_collect:
        type: device_task
        name: Routing interface collect
        runner:
          type: SSH
          command: ip r g {{dns_server_item}}
        parser:
          args: []
          method: parse_routing_interface
        register_to: str_interface_name
        go_to: check_interface_admin_state

      check_interface_admin_state:
        type: device_task
        name: Collect interface admin state
        runner:
          type: SSH
          command: ifconfig {{str_interface_name}}
        parser:
          args: []
          method: is_interface_admin_state_up
        register_to: bool_interface_admin_state_is_up
        go_to: if_interface_admin_state

      if_interface_admin_state:
        type: if
        name: Check if interface admin state is up
        condition: bool_interface_admin_state_is_up
        then_go_to: check_interface_link_state
        else_go_to: triage_DNS_interface_admin_state

      triage_DNS_interface_admin_state:
        type: conclusion
        name: Interface admin state is down
        triage_conclusion: |
          The device is not able to contact DNS server as the interface {{str_interface_name}} is administratively disabled.
        triage_remediation_steps: |
          Enable interface following these steps:
          clish
          set interface {{str_interface_name}} state on
          save config

      check_interface_link_state:
        type: device_task
        name: Collect interface link state
        runner:
          type: SSH
          command: ethtool {{str_interface_name}}
        parser:
          args: []
          method: is_interface_link_state_up
        register_to: bool_is_interface_link_state_up
        go_to: if_interface_link_state

      if_interface_link_state:
        type: if
        name: Check if interface link state is up
        condition: bool_is_interface_link_state_up
        then_go_to: DNS_server_connectivity
        else_go_to: triage_link_state_down

      triage_link_state_down:
        type: conclusion
        name: Interface link state down
        triage_conclusion: |
          The device is not able to contact DNS server because the interface isnot linking.
        triage_remediation_steps: |
          Check cable connected to the interface

      DNS_server_connectivity:
        type: device_task
        name: Test DNS server connectivity
        runner:
          type: SSH
          command: ping -c 10 {{dns_server_item}}
        parser:
          args: []
          method: is_dns_ping_received
        register_to: bool_is_dns_ping_received
        go_to: if_DNS_server_connectivity

      if_DNS_server_connectivity:
        type: if
        name: Check if DNS server is connected
        condition: bool_is_dns_ping_received
        then_go_to: DNS_test_resolve_name
        else_go_to: triage_routing_problem

      triage_routing_problem:
        type: conclusion
        name: Report routing path broken
        triage_conclusion: |
          Routing path is broken at some point between this device and DNS server {{dns_server_item}}
        triage_remediation_steps: |
          Check routing path with traceroute tool (expert mode required):
            - traceroute -n -w 2 -q 2 -m 30 {{dns_server_item}}

      DNS_test_resolve_name:
        type: device_task
        name: Check if different resolver can resolve the name
        runner:
          type: SSH
          command: dig @8.8.4.4 www.indeni.com +noall +stats +time=1
        parser:
          args: []
          method: is_dns_lookup_still_failing
        register_to: bool_dns_server_failed
        go_to: dns_if_name_resolve

      dns_if_name_resolve:
        type: if
        name: Check if DNS name is resolved
        condition: bool_dns_server_failed
        then_go_to: triage_need_detailed_analysis
        else_go_to: check_DNS_ip_private

      triage_need_detailed_analysis:
        type: conclusion
        name: Report further analysis needed
        triage_conclusion: |
          DNS server is well configured on the server and reachable. Extra troubleshooting should be made by firewall adminstrator.
        triage_remediation_steps: |
          All following described task will return more valuable info in this case:
          - Packet capture(tcpdump, fwmonitor)
          - Kernel debug
          - Check IPS configuration(if applicable)
          - Check HTTP/HTTPS proxy (if applicable)

      check_DNS_ip_private:
        type: logic
        name: Check if DNS server IP belongs to networks defined into RFC1918 as private
        args: [dns_server_item]
        method: is_ip_address_private
        register_to: bool_private_ip_space
        go_to: if_dns_server_item_private

      if_dns_server_item_private:
        type: if
        name: Check if DNS server is public or private
        condition: bool_private_ip_space
        then_go_to: triage_problem_in_private_configured_DNS_server
        else_go_to: triage_problem_in_public_configured_DNS_server


      triage_problem_in_private_configured_DNS_server:
        type: conclusion
        name: Report problem in private configured DNS server
        triage_conclusion: |
          DNS records are not propagated to the DNS server configured for this device(or other problems in the DNS server).
        triage_remediation_steps: |
          Contact the team who administrates your DNS.


      triage_problem_in_public_configured_DNS_server:
        type: conclusion
        name: Report problem in public configured DNS server
        triage_conclusion: |
          DNS records are not propagated to the DNS server configured in this device (or any other problem in DNS server).
        triage_remediation_steps: |
          As configured DNS is public the only one solution is to change DNS in use.
          Following commands are needed to configure DNS server:
            - set dns primary|secondary|tertiary <dns_server_item>
            - save config
