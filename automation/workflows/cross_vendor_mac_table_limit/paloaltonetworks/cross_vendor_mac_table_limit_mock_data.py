#HTTPS command: /api?type=op&cmd=<show><mac><all></all></mac></show>
PARSE_PAN_SHOW_MAC_ALL_DATA_1 = (""" <response status="success">
<result>
<max>128000</max>
<total>0</total>
<timeout>1800</timeout>
<dp>s1dp1</dp>
<entries/>
</result>
</response>""", 0.0)

PARSE_PAN_SHOW_MAC_ALL_TOTAL_DATA_1 = (""" <response status="success">
<result>
<max>128000</max>
<total>0</total>
<timeout>1800</timeout>
<dp>s1dp1</dp>
<entries/>
</result>
</response>""", 0.0)

PARSE_PAN_SHOW_MAC_ALL_TOTAL_DATA_2 = (""" <response status="success">
<result>
<max>128000</max>
<total>100</total>
<timeout>1800</timeout>
<dp>s1dp1</dp>
<entries/>
</result>
</response>""", 100.0)

#SSH command: show routing route type connect | match vlan
MAKE_ROUTE_INFO_ARR_DATA_1 = ("""10.200.200.0/24                             10.200.200.1                            0      A C              vlan.100 """,
                              [('vlan.100', '/24', '10.200.200.1')])

#SSH command: show interface {{vlans_arrar[0][0]}} | match zone
PARSE_ZONE_DETAILS_DATA_1 = ("""Zone: untrust, virtual system: vsys1
""", 'untrust')


#Non CLI outputs
CALC_CUMMULATIVE_HOSTS_PER_VLAN_DATA_1 = ([('vlan.10', '/26', '10.200.200.1'),
                                           ('vlan.20', '/24', '10.200.200.1'),
                                           ('vlan.30', '/29', '10.200.200.1'),
                                           ('vlan.40', '/28', '10.200.200.1'),
                                           ('vlan.50', '/24', '10.200.200.1')], 590)

GET_INTERFACE_DETAILS_OF_VLANS_WITH_LARGE_SUBNETS_DATA_1 = ("""10.200.200.0/16                             10.200.200.1                            0      A C              vlan.100 """,
                                                            [('vlan.100', '10.200.200.1')])

