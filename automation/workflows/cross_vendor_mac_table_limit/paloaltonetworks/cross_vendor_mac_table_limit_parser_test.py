import unittest
import automation.workflows.cross_vendor_mac_table_limit.paloaltonetworks.cross_vendor_mac_table_limit_mock_data as mock
import automation.workflows.cross_vendor_mac_table_limit.paloaltonetworks.cross_vendor_mac_table_limit_parser as parser


class HighMACCacheTests(unittest.TestCase):
    def test_parse_pan_show_mac_all(self):
        pair1 = mock.PARSE_PAN_SHOW_MAC_ALL_DATA_1
        self.assertEqual(pair1[1], parser.parse_pan_show_mac_all(show_mac_all_output=pair1[0]))

    def test_make_route_info_arr(self):
        pair1 = mock.MAKE_ROUTE_INFO_ARR_DATA_1
        self.assertEqual(pair1[1], parser.make_route_info_arr(pair1[0]))

    def test_calc_cummulative_hosts_per_vlan(self):
        pair1 = mock.CALC_CUMMULATIVE_HOSTS_PER_VLAN_DATA_1
        self.assertEqual(pair1[1], parser.calc_cummulative_hosts_per_vlan(route_info_arr=pair1[0]))

    def test_parse_zone_details(self):
        pair1 = mock.PARSE_ZONE_DETAILS_DATA_1
        self.assertEqual(pair1[1], parser.parse_zone_details(pair1[0]))

    def test_get_interface_details_of_vlans_with_large_subnets(self):
        pair1 = mock.GET_INTERFACE_DETAILS_OF_VLANS_WITH_LARGE_SUBNETS_DATA_1
        self.assertEqual(pair1[1], parser.get_interface_details_of_vlans_with_large_subnets(pair1[0]))

    def test_parse_pan_show_mac_all_total(self):
        pair1 = mock.PARSE_PAN_SHOW_MAC_ALL_TOTAL_DATA_1
        pair2 = mock.PARSE_PAN_SHOW_MAC_ALL_TOTAL_DATA_2
        self.assertEqual(pair1[1], parser.parse_pan_show_mac_all_total(show_mac_all_output=pair1[0]))
        self.assertEqual(pair2[1], parser.parse_pan_show_mac_all_total(show_mac_all_output=pair2[0]))


if __name__ == '__main__':
    unittest.main()
