from indeni_workflow.workflow_test_tool import WorkflowTestTool
import re
import xml.etree.ElementTree as et
from typing import List, Tuple


def check_variant_matches(**kwargs) -> bool:
    # This should work on all gateways
    tags = kwargs.get('my_device_tags', None)
    if tags is None:
        return False
    if tags.get('product') != 'firewall':
        return False
    return True


def identity_parser(raw_data: str) -> str:
    return raw_data


def parse_pan_show_mac_all(**kwargs) -> float:
    """
    return the percentage (as float) of mac used
    """
    raw_data = kwargs.get('show_mac_all_output')
    root = et.fromstring(raw_data)
    maximum = float(root[0].find('max').text)
    total = float(root[0].find('total').text)
    return (total / maximum) * 100


def parse_pan_show_mac_all_total(**kwargs) -> float:
    raw_data = kwargs.get('show_mac_all_output')
    root = et.fromstring(raw_data)
    total = float(root[0].find('total').text)
    return float(total)


def calc_cummulative_hosts_per_vlan(**kwargs) -> int:
    total = 0
    route_info_arr = kwargs.get('route_info_arr', [])
    for route_info in route_info_arr:
        interface, mask, route = route_info
        mask_num = int(mask.strip('/'))
        total += ((2 ** (32 - mask_num)) - 2)
    return total


def make_route_info_arr(raw_data: str) -> List[Tuple]:
    # TODO: should we only track vlans whose mask is >= 20 or <20. For now, just taking all of them.
    lines = raw_data.strip().split('\n')
    output_list = []
    for line in lines:
        fixed_split_line = re.sub(r'\s+', ' ', line.strip()).split(' ')
        ip_with_mask = fixed_split_line[0]
        route: str = fixed_split_line[1]
        interface: str = fixed_split_line[-1]
        mask: str = '/' + ip_with_mask.split('/')[-1]
        output_list.append(tuple([interface, mask, route]))
    return output_list


def parse_zone_details(raw_data: str) -> str:
    lines = raw_data.strip().split('\n')
    line = lines[0]
    return line.split(' ')[1].strip(',')


def get_interface_details_of_vlans_with_large_subnets(raw_data: str) -> List:
    route_info_arr = make_route_info_arr(raw_data)
    filtered_info_arr = []
    for route_info in route_info_arr:
        interface, mask, route = route_info
        # Remove the leading forward-slash
        if int(mask[1:]) <= 20:
            filtered_info_arr.append((interface, route))
    return filtered_info_arr
