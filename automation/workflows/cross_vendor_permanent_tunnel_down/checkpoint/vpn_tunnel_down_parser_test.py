import unittest
import automation.workflows.cross_vendor_permanent_tunnel_down.checkpoint.vpn_tunnel_down_mock_data as mock
import automation.workflows.cross_vendor_permanent_tunnel_down.checkpoint.vpn_tunnel_down_parser as parser


class DeviceTests(unittest.TestCase):

    def test_parse_tunnel_ip(self):
        self.assertEqual(parser.extract_ip(vpn_tunnel=mock.EXTRACT_TUNNEL_IP[0]), mock.EXTRACT_TUNNEL_IP[1])

    def test_parse_single_vpn_tunnel(self):
        self.assertEqual(parser.parse_single_vpn_tunnel(mock.SINGLE_VPN_DOWN_VPN_TU_LIST_TUNNELS[0],
                                                        vpn_tunnel_ip=mock.SINGLE_VPN_DOWN_VPN_TU_LIST_TUNNELS[1]),
                         mock.SINGLE_VPN_DOWN_VPN_TU_LIST_TUNNELS[2])

        self.assertEqual(parser.parse_single_vpn_tunnel(mock.SINGLE_VPN_UP_VPN_TU_LIST_TUNNELS[0],
                                                        vpn_tunnel_ip=mock.SINGLE_VPN_UP_VPN_TU_LIST_TUNNELS[1]),
                         mock.SINGLE_VPN_UP_VPN_TU_LIST_TUNNELS[2])

        self.assertEqual(parser.parse_single_vpn_tunnel(mock.MULTIPLE_VPN_1_UP_1_DOWN_VPN_TU_LIST_TUNNELS[0],
                                                        vpn_tunnel_ip=mock.MULTIPLE_VPN_1_UP_1_DOWN_VPN_TU_LIST_TUNNELS[
                                                            1]),
                         mock.MULTIPLE_VPN_1_UP_1_DOWN_VPN_TU_LIST_TUNNELS[2])

        self.assertEqual(parser.parse_single_vpn_tunnel(mock.MULTIPLE_VPN_2_UP_VPN_TU_LIST_TUNNELS[0],
                                                        vpn_tunnel_ip=mock.MULTIPLE_VPN_2_UP_VPN_TU_LIST_TUNNELS[1]),
                         mock.MULTIPLE_VPN_2_UP_VPN_TU_LIST_TUNNELS[2])

    def test_parse_ipsec_sa_status(self):
        self.assertEqual(parser.parse_sa_status(mock.SINGLE_VPN_DOWN_VPN_TU_LIST_IPSEC[0],
                                                vpn_tunnel_ip=mock.SINGLE_VPN_DOWN_VPN_TU_LIST_IPSEC[1]),
                         mock.SINGLE_VPN_DOWN_VPN_TU_LIST_IPSEC[2])

        self.assertEqual(parser.parse_sa_status(mock.SINGLE_VPN_UP_VPN_TU_LIST_IPSEC[0],
                                                vpn_tunnel_ip=mock.SINGLE_VPN_UP_VPN_TU_LIST_IPSEC[1]),
                         mock.SINGLE_VPN_UP_VPN_TU_LIST_IPSEC[2])

        self.assertEqual(parser.parse_sa_status(mock.MULTIPLE_VPN_1_UP_1_DOWN_VPN_TU_LIST_IPSEC[0],
                                                vpn_tunnel_ip=mock.MULTIPLE_VPN_1_UP_1_DOWN_VPN_TU_LIST_IPSEC[1]),
                         mock.MULTIPLE_VPN_1_UP_1_DOWN_VPN_TU_LIST_IPSEC[2])

        self.assertEqual(parser.parse_sa_status(mock.MULTIPLE_VPN_2_UP_VPN_TU_LIST_IPSEC[0],
                                                vpn_tunnel_ip=mock.MULTIPLE_VPN_2_UP_VPN_TU_LIST_IPSEC[1]),
                         mock.MULTIPLE_VPN_2_UP_VPN_TU_LIST_IPSEC[2])

    def test_parse_ike_sa_status(self):
        self.assertEqual(parser.parse_sa_status(mock.SINGLE_VPN_DOWN_VPN_TU_LIST_IKE[0],
                                                vpn_tunnel_ip=mock.SINGLE_VPN_DOWN_VPN_TU_LIST_IPSEC[1]),
                         mock.SINGLE_VPN_DOWN_VPN_TU_LIST_IPSEC[2])

        self.assertEqual(parser.parse_sa_status(mock.SINGLE_VPN_UP_VPN_TU_LIST_IKE[0],
                                                vpn_tunnel_ip=mock.SINGLE_VPN_UP_VPN_TU_LIST_IPSEC[1]),
                         mock.SINGLE_VPN_UP_VPN_TU_LIST_IPSEC[2])

        self.assertEqual(parser.parse_sa_status(mock.MULTIPLE_VPN_1_UP_1_DOWN_VPN_TU_LIST_IKE[0],
                                                vpn_tunnel_ip=mock.MULTIPLE_VPN_1_UP_1_DOWN_VPN_TU_LIST_IPSEC[1]),
                         mock.MULTIPLE_VPN_1_UP_1_DOWN_VPN_TU_LIST_IPSEC[2])

        self.assertEqual(parser.parse_sa_status(mock.MULTIPLE_VPN_2_UP_VPN_TU_LIST_IKE[0],
                                                vpn_tunnel_ip=mock.MULTIPLE_VPN_2_UP_VPN_TU_LIST_IPSEC[1]),
                         mock.MULTIPLE_VPN_2_UP_VPN_TU_LIST_IPSEC[2])


if __name__ == '__main__':
    unittest.main()
