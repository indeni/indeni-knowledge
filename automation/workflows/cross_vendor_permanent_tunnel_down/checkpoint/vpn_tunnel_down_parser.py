from parser_service.public.helper_methods import *


def check_variant_matches(**kwargs) -> bool:
    # This should work on all gateways
    tags = kwargs.get('my_device_tags', None)
    if tags is None:
        return False
    if tags.get('role-firewall') != 'true':
        return False
    if tags.get('vsx') == 'true':
        return False
    return True


def extract_ip(**kwargs) -> str:
    vpn_tunnel = kwargs.get('vpn_tunnel')
    vpn_tunnel_ip = vpn_tunnel.strip().split(' ')[1]
    return vpn_tunnel_ip[1:]


def parse_single_vpn_tunnel(raw_data: str, **kwargs) -> bool:
    vpn_tunnel_ip = kwargs.get('vpn_tunnel_ip', '')
    raw_data_parsed = parse_data_as_list(raw_data, 'checkpoint_gaia_vpn_tu_list_tunnels.textfsm')
    for tunnel in raw_data_parsed:
        if tunnel['peer_ip'] == vpn_tunnel_ip or tunnel['peer_ip_ts'] == vpn_tunnel_ip:
            if 'Out SPI' in tunnel['sa']:
                return True
    return False


def parse_sa_status(raw_data: str, **kwargs) -> bool:
    vpn_tunnel_ip = kwargs.get('vpn_tunnel_ip', '')
    raw_data_parsed = parse_data_as_list(raw_data, 'checkpoint_gaia_vpn_tu_list.textfsm')
    if raw_data_parsed is None:
        return False
    for sa in raw_data_parsed:
        if sa['peer_ip'] == vpn_tunnel_ip:
            return True
    return False
