EXTRACT_TUNNEL_IP = ("""myVPN (10.11.94.174 - CP-R80.30-GW1-2)""", '10.11.94.174')

SINGLE_VPN_DOWN_VPN_TU_LIST_TUNNELS = ("""
+-----------------------------------------+-----------------------+---------------------+
| Peer: 10.11.94.174 - CP-R80.30-GW1-2    | MSA: ffffc2002387c128 | i: 1  ref:     4    |
| Methods: ESP Tunnel AES-128 SHA1        |                       | i: 2  ref:     1    |
| My TS:   10.11.94.173                   |                       |                     |
| Peer TS: 10.11.94.174                   |                       |                     |
| MSPI:	   800001 (i:  1, p:  0)          | No outbound SA        |                     |
+-----------------------------------------+-----------------------+---------------------+
""", '10.11.94.174', False)

SINGLE_VPN_DOWN_VPN_TU_LIST_IKE = ("""
No data to display
""", '10.11.94.174', False)

SINGLE_VPN_DOWN_VPN_TU_LIST_IPSEC = ("""
No data to display
""", '10.11.94.174', False)


SINGLE_VPN_UP_VPN_TU_LIST_TUNNELS = ("""
+-----------------------------------------+-----------------------+---------------------+
| Peer: 10.11.94.174 - CP-R80.30-GW1-2    | MSA: ffffc2002387c128 | i: 0  ref:     1    |
| Methods: ESP Tunnel AES-128 SHA1        |                       | i: 1  ref:     5    |
| My TS:   10.11.94.173                   |                       | i: 2  ref:     4    |
| Peer TS: 10.11.94.174                   |                       |                     |
| MSPI:	   800001 (i:  1, p:  0)          | Out SPI: c25ff7ff     |                     |
+-----------------------------------------+-----------------------+---------------------+
""", '10.11.94.174', True)

SINGLE_VPN_UP_VPN_TU_LIST_IKE = ("""
Peer  10.11.94.174 , CP-R80.30-GW1-2 SAs:

	IKE SA <c41fd7546c1e0358,ecb5e7e12b759304>
""", '10.11.94.174', True)

SINGLE_VPN_UP_VPN_TU_LIST_IPSEC = ("""
Peer  10.11.94.174 , CP-R80.30-GW1-2 SAs:

	IKE SA <c41fd7546c1e0358,ecb5e7e12b759304>
		INBOUND:
			1. 0x605b476b	(i: 1)
		OUTBOUND:
			1. 0xc25ff7ff	(i: 1)
""", '10.11.94.174', True)

MULTIPLE_VPN_1_UP_1_DOWN_VPN_TU_LIST_TUNNELS = ("""
+-----------------------------------------+-----------------------+---------------------+
| Peer: 10.11.94.174 - CP-R80.30-GW1-2    | MSA: ffffc2002387c128 | i: 0  ref: -- 59/60 |
| Methods: ESP Tunnel AES-128 SHA1        |                       | i: 1  ref:     4    |
| My TS:   10.11.94.173                   |                       | i: 2  ref:     1    |
| Peer TS: 10.11.94.174                   |                       |                     |
| MSPI:	   800001 (i:  1, p:  0)          | No outbound SA        |                     |
+-----------------------------------------+-----------------------+---------------------+
| Peer: 10.11.94.116 - R76.50SP_GW        | MSA: ffffc2002387c220 | i: 0  ref:     4    |
| Methods: ESP Tunnel AES-128 SHA1        |                       | i: 1  ref:     3    |
| My TS:   10.11.94.173                   |                       |                     |
| Peer TS: 10.11.94.116                   |                       |                     |
| MSPI:	   800002 (i:  1, p:  0)          | Out SPI: 7060e771     |                     |
+-----------------------------------------+-----------------------+---------------------+
""", '10.11.94.116', True)

MULTIPLE_VPN_1_UP_1_DOWN_VPN_TU_LIST_IKE = ("""
Peer  10.11.94.116 , R76.50SP_GW SAs:

	IKE SA <82ca9f37f0f90ca8,bc6c1fe425eca25b>
""", '10.11.94.116', True)

MULTIPLE_VPN_1_UP_1_DOWN_VPN_TU_LIST_IPSEC = ("""
Peer  10.11.94.116 , R76.50SP_GW SAs:

	IKE SA <82ca9f37f0f90ca8,bc6c1fe425eca25b>
		INBOUND:
			1. 0xc130daf	(i: 1)
		OUTBOUND:
			1. 0x7060e771	(i: 1)
""", '10.11.94.116', True)

MULTIPLE_VPN_2_UP_VPN_TU_LIST_TUNNELS = ("""
+-----------------------------------------+-----------------------+---------------------+
| Peer: 10.11.94.174 - CP-R80.30-GW1-2    | MSA: ffffc2002387c128 | i: 0  ref:     2    |
| Methods: ESP Tunnel AES-128 SHA1        |                       | i: 1  ref:     4    |
| My TS:   10.11.94.173                   |                       | i: 2  ref:     1    |
| Peer TS: 10.11.94.174                   |                       |                     |
| MSPI:	   800001 (i:  1, p:  0)          | Out SPI: bf5fc623     |                     |
+-----------------------------------------+-----------------------+---------------------+
| Peer: 10.11.94.116 - R76.50SP_GW        | MSA: ffffc2002387c220 | i: 0  ref:     3    |
| Methods: ESP Tunnel AES-128 SHA1        |                       | i: 1  ref:     7    |
| My TS:   10.11.94.173                   |                       | i: 2  ref: -- 52/60 |
| Peer TS: 10.11.94.116                   |                       |                     |
| MSPI:	   800002 (i:  1, p:  0)          | Out SPI: 7060e771     |                     |
+-----------------------------------------+-----------------------+---------------------+
""", '10.11.94.174', True)

MULTIPLE_VPN_2_UP_VPN_TU_LIST_IKE = ("""
Peer  10.11.94.116 , R76.50SP_GW SAs:

	IKE SA <82ca9f37f0f90ca8,bc6c1fe425eca25b>

Peer  10.11.94.174 , CP-R80.30-GW1-2 SAs:

	IKE SA <1f7ce6ae2ac04df2,79aee85de8dac856>
""", '10.11.94.174', True)

MULTIPLE_VPN_2_UP_VPN_TU_LIST_IPSEC = ("""
Peer  10.11.94.116 , R76.50SP_GW SAs:

	IKE SA <82ca9f37f0f90ca8,bc6c1fe425eca25b>
		INBOUND:
			1. 0xc130daf	(i: 1)
		OUTBOUND:
			1. 0x7060e771	(i: 1)

Peer  10.11.94.174 , CP-R80.30-GW1-2 SAs:

	IKE SA <1f7ce6ae2ac04df2,79aee85de8dac856>
		INBOUND:
			1. 0x37d0c29e	(i: 1)
		OUTBOUND:
			1. 0xbf5fc623	(i: 1)
""", '10.11.94.174', True)



