import re
from collections import defaultdict
from parser_service.public.helper_methods import *

def check_variant_matches(**kwargs) -> bool:
    # This should work on all gateways with one of the below versions
    tags = kwargs.get('my_device_tags', None)
    if tags is None:
        return False
    versions = [x.lower() for x in [ 'R80.40', 'R81', 'R81.10', 'R81.20']]
    if tags.get('os.version', '').lower() not in versions:
        return False
    if tags.get('role-firewall') != 'true':
        return False
    return True


def get_cpu_utilization(raw_data: str,cpu_id: str) -> tuple:

    idle = 0
    total_cpus = 0
    if cpu_id == "all-average":
        cpu_id = "all"
    lines = raw_data.split('\n')
    for index, line in enumerate(lines, start=0):
        line_list = re.sub(r'\s+', ' ', line).split(' ')
        if index == 0:
            position = line_list.index('%idle')
        elif line_list[0]==cpu_id:
            idle = line_list[position]
        if line_list[0] != "" :
            total_cpus = line_list[0]
    return (round(100 - float(idle),2), int(total_cpus)+1)

def bool_is_utilization_across_all_cpus(raw_data: str) -> bool:
    high_across_all = True
    lines = raw_data.split('\n')
    fixed_lines = []
    for line in lines:
        if '----------' in line or line.strip() == '':
            continue
        else:
            fixed_lines.append(line.strip())

    index_of_headline = None
    for i, line in enumerate(fixed_lines):
        if 'Idle Time(%)' in line:
            index_of_headline = i

    if index_of_headline is None:
        return high_across_all

    index_of_idle = fixed_lines[index_of_headline].split('|').index('Idle Time(%)')

    for cpu_line in fixed_lines[index_of_headline+1:]:
        idle = cpu_line.split('|')[index_of_idle]
        if float(idle) > 30:
            high_across_all = False

    return high_across_all

def get_securexl_and_templates_enabled(raw_data: str) -> bool:
    lines = raw_data.split('\n')
    both_enabled = 'enabled' in lines[3]
    for line in lines:
        if line.startswith('Accept Templates'):
            return both_enabled and 'enabled' in line
    return both_enabled

def identity_parser(raw_data: str) -> str:
    return raw_data.strip()

def is_redistribution_automatic(raw_data: str) -> bool:
    data = raw_data.strip()
    return data == '' or 'No such file or directory' in data

def is_corexl_running(raw_data: str) -> bool:
    data = raw_data.strip()
    if 'CoreXL is disabled' in data:
        return False
    lines = raw_data.strip().split('\n')
    for line in lines[2:]:
        split_line = line.split('|')
        if 'No' in split_line[1]:
            return False
    return True

def is_corexl_license_match_num_cores(raw_data: str) -> bool:
    lines = raw_data.split('\n')
    # if no licence exists
    if lines[1].strip() == '':
        return False

    licensed_cores_match = re.search(r'CPSG-C-(\d+)', lines[1])
    if licensed_cores_match is None:
        licensed_cores_num = 0
    else:
        licensed_cores_num = int(licensed_cores_match.group(1))

    matches = re.findall(r'processor\s+:\s+(\d+)', raw_data)
    num_processors = len(matches)
    return num_processors <= licensed_cores_num


def is_interrupt_load_imbalanced(raw_data: str) -> bool:
    cpu_num_to_interrupts = defaultdict(int)
    lines = raw_data.split('\n')
    cpu_line = re.sub(r'\s+', ' ', lines[0].strip())
    num_cpus = len(cpu_line.split(' '))
    for line in lines[1:]:
        fixed_line = re.sub(r'\s+', ' ', line.strip()).split(' ')
        if len(fixed_line) < num_cpus:
            continue
        for cpu in range(num_cpus):
            cpu_num_to_interrupts[cpu] += int(fixed_line[cpu + 1])
    values_list = list(cpu_num_to_interrupts.values())
    for i in range(len(values_list) - 1):
        if values_list[i] != values_list[i+1]:
            return True
    return False


def is_some_process_consuming_high_cpu(raw_data: str) -> bool:
    threshold = 80
    lines = raw_data.strip().split('\n')
    if len(lines) < 2:
        return False
    stripped_split_line2 = re.sub(r'\s+', ' ', lines[1].strip()).split(' ')
    return float(stripped_split_line2[4]) > threshold


def collect_affinities(raw_data: str,cpu_id: str) -> tuple:
    raw_data_parsed = parse_data_as_list(raw_data, 'checkpoint_gaia_fw_ctl_affinity_l.textfsm')
    items_filtered = [x for x in raw_data_parsed if x['cpu_id']==cpu_id ]
    if len(items_filtered) == 1:
        if 'VS' in items_filtered[0]['item']:
            stripped_vs = items_filtered[0]['item'].split('_')
            return ('VS', stripped_vs[1])
        return ('interface', items_filtered[0]['item'])
    return ('None','')

def searchKey(e):
    return e['throughput_percentage']

def get_cpmonitor_top_talkers(raw_data: str) -> list:
    if raw_data:
        data = parse_data_as_list(raw_data, 'checkpoint_cpmonitor_file.textfsm')
        if data:
            data.sort(reverse=True, key=searchKey)
            data_returned = []
            for i in range (0, 5):
                talker_item = []
                talker_item = [data[i]['throughput_percentage'], data[i]['src_ip'],data[i]['dst_ip'],data[i]['proto'],data[i]['dst_port']]
                data_returned.append(talker_item)
            return data_returned