#SSH command: mpstat -P ALL 1 1 | grep Average | sed 's/Average:\s*//'
MPSTAT_P_ALL_OUT_1 = """CPU   %user   %nice    %sys %iowait    %irq   %soft  %steal   %idle    intr/s
all    0.50    0.76    0.76    0.00    0.25    0.50    0.00   97.23   1990.00
0    2.00    3.00    3.00    0.00    1.00    2.00    0.00   89.00   1989.00
1    0.00    0.00    0.00    0.00    0.00    0.00    0.00   99.00      0.00
2    0.00    0.00    0.00    0.00    0.00    0.00    0.00   99.00      0.00
3    0.00    0.00    0.00    0.00    0.00    0.00    0.00   99.00      0.00
"""

GET_CPU_UTILIZATION_PAIR_1 = ((MPSTAT_P_ALL_OUT_1,"all-average"), (2.77, 4))

MPSTAT_P_ALL_OUT_2 = """CPU   %user   %nice    %sys %iowait    %irq   %soft  %steal   %idle    intr/s
all    88.22    0.76    0.76    0.00    0.25    0.50    0.00   10.20   1990.00
0    2.00    3.00    3.00    0.00    1.00    2.00    0.00   89.00   1989.00
1    0.00    0.00    0.00    0.00    0.00    0.00    0.00   99.00      0.00
2    0.00    0.00    0.00    0.00    0.00    0.00    0.00   99.00      0.00
3    0.00    0.00    0.00    0.00    0.00    0.00    0.00   99.00      0.00
"""

GET_CPU_UTILIZATION_PAIR_2 = ((MPSTAT_P_ALL_OUT_2,"all-average"), (89.80, 4))

MPSTAT_P_ALL_OUT_3 = """CPU   %user   %nice    %sys %iowait    %irq   %soft  %steal   %idle    intr/s
all    0.50    0.76    0.76    0.00    0.25    0.50    0.00   97.23   1990.00
0    2.00    3.00    3.00    0.00    1.00    2.00    0.00   89.00   1989.00
1    0.00    0.00    0.00    0.00    0.00    0.00    0.00   99.00      0.00
2    0.00    0.00    0.00    0.00    0.00    0.00    0.00   99.00      0.00
3    0.00    0.00    0.00    0.00    0.00    0.00    0.00   99.00      0.00
"""

GET_CPU_UTILIZATION_PAIR_3 = ((MPSTAT_P_ALL_OUT_3,"2"), (1.0, 4))

MPSTAT_P_ALL_OUT_4 ="""CPU   %user   %nice    %sys %iowait    %irq   %soft  %steal   %idle    intr/s
all    0.50    0.76    0.76    0.00    0.25    0.50    0.00   97.23   1990.00
0    2.00    3.00    3.00    0.00    1.00    2.00    0.00   89.00   1989.00
1    0.00    0.00    0.00    0.00    0.00    0.00    0.00   99.00      0.00
2    0.00    0.00    0.00    0.00    0.00    0.00    0.00   99.00      0.00
3    0.00    91.00    0.00    0.00    0.00    0.00    0.00    9.00      0.00
"""

GET_CPU_UTILIZATION_PAIR_4 = ((MPSTAT_P_ALL_OUT_4,"3"), (91.0, 4))

#SSH command: cpstat -f multi_cpu os
CPSTAT_F_MULTI_CPU_OS_OUT_1 = """


Processors load
---------------------------------------------------------------------------------
|CPU#|User Time(%)|System Time(%)|Idle Time(%)|Usage(%)|Run queue|Interrupts/sec|
---------------------------------------------------------------------------------
|   1|           5|             4|          92|       8|        ?|          2674|
|   2|           0|             0|         100|       0|        ?|          2674|
|   3|           0|             0|         100|       0|        ?|          2674|
|   4|           0|             0|         100|       0|        ?|          2674|
---------------------------------------------------------------------------------

"""

IS_UTILIZATION_ACROSS_ALL_CPUS_PAIR_1 = (CPSTAT_F_MULTI_CPU_OS_OUT_1, False)

CPSTAT_F_MULTI_CPU_OS_OUT_2 =  """


Processors load
---------------------------------------------------------------------------------
|CPU#|User Time(%)|System Time(%)|Idle Time(%)|Usage(%)|Run queue|Interrupts/sec|
---------------------------------------------------------------------------------
|   1|           5|             4|           0|       8|        ?|          2674|
|   2|           0|             0|           0|       0|        ?|          2674|
|   3|           0|             0|           0|       0|        ?|          2674|
|   4|           0|             0|           0|       0|        ?|          2674|
---------------------------------------------------------------------------------

"""

IS_UTILIZATION_ACROSS_ALL_CPUS_PAIR_2 = (CPSTAT_F_MULTI_CPU_OS_OUT_2, True)

#SSH command: fwaccel stat -a -t -v
FWACCEL_STAT_A_T_V_OUT_1 ="""+-----------------------------------------------------------------------------+
|Id|Name |Status     |Interfaces               |Features                      |
+-----------------------------------------------------------------------------+
|0 |SND  |no license |eth0,eth1,eth2           |Acceleration,Cryptography     |
|  |     |           |                         |Crypto: Tunnel,UDPEncap,MD5,  |
|  |     |           |                         |SHA1,NULL,3DES,DES,CAST,      |
|  |     |           |                         |CAST-40,AES-128,AES-256,ESP,  |
|  |     |           |                         |LinkSelection,DynamicVPN,     |
|  |     |           |                         |NatTraversal,AES-XCBC,SHA256  |
+-----------------------------------------------------------------------------+

Accept Templates : enabled
Drop Templates   : disabled
NAT Templates    : enabled
"""

GET_SECUREXL_AND_TEMPLATES_ENABLED_PAIR_1 = (FWACCEL_STAT_A_T_V_OUT_1, False)

FWACCEL_STAT_A_T_V_OUT_2 = """+-----------------------------------------------------------------------------+
|Id|Name |Status     |Interfaces               |Features                      |
+-----------------------------------------------------------------------------+
|0 |SND  |enabled    |eth0,eth1,eth2           |Acceleration,Cryptography     |
|  |     |           |                         |Crypto: Tunnel,UDPEncap,MD5,  |
|  |     |           |                         |SHA1,NULL,3DES,DES,CAST,      |
|  |     |           |                         |CAST-40,AES-128,AES-256,ESP,  |
|  |     |           |                         |LinkSelection,DynamicVPN,     |
|  |     |           |                         |NatTraversal,AES-XCBC,SHA256  |
+-----------------------------------------------------------------------------+
Accept Templates : enabled
Drop Templates   : disabled
NAT Templates    : enabled
"""

GET_SECUREXL_AND_TEMPLATES_ENABLED_PAIR_2 = (FWACCEL_STAT_A_T_V_OUT_2, True)

#SSH command: cat $PPKDIR/boot/modules/sim_aff.conf
CAT_SIM_AFF_CONF_OUT_1 = """cat: /opt/CPppak-R80.20/boot/modules/sim_aff.conf: No such file or directory
"""
IS_REDISTRIBUTION_AUTOMATIC_PAIR_1 = (CAT_SIM_AFF_CONF_OUT_1, True)

CAT_SIM_AFF_CONF_OUT_2 = """eth0 00000002,00000000,00000000,00000000,00000000,00000000,00000000,00000000 67
eth1 00000004,00000000,00000000,00000000,00000000,00000000,00000000,00000000 75
eth2 00000008,00000000,00000000,00000000,00000000,00000000,00000000,00000000 83
eth3 00000001,00000000,00000000,00000000,00000000,00000000,00000000,00000000 59
"""

IS_REDISTRIBUTION_AUTOMATIC_PAIR_2 = (CAT_SIM_AFF_CONF_OUT_2, False)

FW_CTL_AFFINITY_L_OUT_1 = """Interface eth0: CPU 0
Interface eth1: CPU 0
VS_0 fwk: CPU 0
VS_1 fwk: CPU 0
VS_2: CPU 2
VS_2 fwk: CPU 2
VS_3 fwk: CPU 0
"""

COLLECT_AFFINITIES_PAIR_1 = ((FW_CTL_AFFINITY_L_OUT_1, '2'),('VS','2'))

FW_CTL_AFFINITY_L_OUT_2 = """Interface eth0: CPU 0
Interface eth1: CPU 0
Interface eth2: CPU 0
Interface eth5: CPU 0
Interface eth3: CPU 0
Interface eth6: CPU 0
Interface eth4: CPU 0
Interface eth7: CPU 0
The current license only permits using CPU 0.
"""

COLLECT_AFFINITIES_PAIR_2 = ((FW_CTL_AFFINITY_L_OUT_2, '0'),('None',''))

#SSH command: fw ctl multik stat
FW_CTL_MULTIK_STAT_OUT_1 = """ CoreXL is disabled

"""

IS_COREXL_RUNNING_PAIR_1 = ( FW_CTL_MULTIK_STAT_OUT_1,False)

FW_CTL_MULTIK_STAT_OUT_2 = """ID | Active  | CPU    | Connections | Peak
----------------------------------------------
 0 | Yes     | 3      |          27 |       50
 1 | No      | -      |          16 |       53
 2 | No      | -      |          21 |       57
 """

IS_COREXL_RUNNING_PAIR_2 = (FW_CTL_MULTIK_STAT_OUT_2, False)

FW_CTL_MULTIK_STAT_OUT_3 = """ID | Active  | CPU    | Connections | Peak
----------------------------------------------
 0 | Yes     | 3      |          27 |       50
 1 | Yes     | -      |          16 |       53
 2 | Yes     | -      |          21 |       57
 """

IS_COREXL_RUNNING_PAIR_3 = (FW_CTL_MULTIK_STAT_OUT_3, True)

#SSH command: cplic print; sleep 2; cat /proc/cpuinfo
CPLIC_PRINT_CAT_CPUINFO_OUT_1 = """Host             Expiration  Features

processor	: 0
vendor_id	: GenuineIntel
cpu family	: 6
model		: 85
model name	: Intel(R) Xeon(R) Gold 5120 CPU @ 2.20GHz
stepping	: 4
cpu MHz		: 2194.719
cache size	: 19712 KB
fpu		: yes
fpu_exception	: yes
cpuid level	: 22
wp		: yes
flags		: fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush mmx fxsr sse sse2 ss syscall nx pdpe1gb rdtscp lm constant_tsc pni cx16 popcnt lahf_lm altmovcr8 misalignsse
bogomips	: 4391.08
clflush size	: 64
cache_alignment	: 64
address sizes	: 43 bits physical, 48 bits virtual
power management:

processor	: 1
vendor_id	: GenuineIntel
cpu family	: 6
model		: 85
model name	: Intel(R) Xeon(R) Gold 5120 CPU @ 2.20GHz
stepping	: 4
cpu MHz		: 2194.719
cache size	: 19712 KB
fpu		: yes
fpu_exception	: yes
cpuid level	: 22
wp		: yes
flags		: fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush mmx fxsr sse sse2 ss syscall nx pdpe1gb rdtscp lm constant_tsc pni cx16 popcnt lahf_lm altmovcr8 misalignsse
bogomips	: 4390.08
clflush size	: 64
cache_alignment	: 64
address sizes	: 43 bits physical, 48 bits virtual
power management:

processor	: 2
vendor_id	: GenuineIntel
cpu family	: 6
model		: 85
model name	: Intel(R) Xeon(R) Gold 5120 CPU @ 2.20GHz
stepping	: 4
cpu MHz		: 2194.719
cache size	: 19712 KB
fpu		: yes
fpu_exception	: yes
cpuid level	: 22
wp		: yes
flags		: fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush mmx fxsr sse sse2 ss syscall nx pdpe1gb rdtscp lm constant_tsc pni cx16 popcnt lahf_lm altmovcr8 misalignsse
bogomips	: 4387.66
clflush size	: 64
cache_alignment	: 64
address sizes	: 43 bits physical, 48 bits virtual
power management:

processor	: 3
vendor_id	: GenuineIntel
cpu family	: 6
model		: 85
model name	: Intel(R) Xeon(R) Gold 5120 CPU @ 2.20GHz
stepping	: 4
cpu MHz		: 2194.719
cache size	: 19712 KB
fpu		: yes
fpu_exception	: yes
cpuid level	: 22
wp		: yes
flags		: fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush mmx fxsr sse sse2 ss syscall nx pdpe1gb rdtscp lm constant_tsc pni cx16 popcnt lahf_lm altmovcr8 misalignsse
bogomips	: 4391.59
clflush size	: 64
cache_alignment	: 64
address sizes	: 43 bits physical, 48 bits virtual
power management:

"""

IS_COREXL_LICENSE_MATCH_NUM_CORES_PAIR_1 = (CPLIC_PRINT_CAT_CPUINFO_OUT_1, False)

CPLIC_PRINT_CAT_CPUINFO_OUT_2 = """Host Expiration Features
10.11.94.79 19Jan2020 CPSG-C-8-U CPSB-FW CPSB-VPN CPSB-IPSA CPSB-DLP CPSB-SSLVPN-U CPSB-IA CPSB-ADNC CPSG-VSX-25S CPSB-SWB CPSB-IPS CPSB-AV CPSB-URLF CPSB-ASPM CPSB-APCL CPSB-ABOT CK-32F7BA64433C

Contract Coverage:

# ID Expiration SKU
===+===========+============+====================
1 | 0G2SY18 | 4Dec2015 | CPSB-DLP-EVAL
+-----------+------------+--------------------
|Covers: CPSG-C-8-U CPSB-FW CPSB-VPN CPSB-IPSA CPSB-DLP CPSB-SSLVPN-U CPSB-IA CPSB-ADNC CPSG-VSX-25S CPSB-SWB CPSB-IPS CPSB-AV CPSB-URLF CPSB-ASPM CPSB-APCL CPSB-ABOT CK-32F7BA64433C

processor : 0
vendor_id : GenuineIntel
cpu family : 6
model : 85
model name : Intel(R) Xeon(R) Gold 5118 CPU @ 2.30GHz
stepping : 4
cpu MHz : 2294.365
cache size : 16896 KB
physical id : 0
siblings : 4
core id : 0
cpu cores : 4
fpu : yes
fpu_exception : yes
cpuid level : 22
wp : yes
flags : fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush mmx fxsr sse sse2 ss ht syscall nx pdpe1gb rdtscp lm constant_tsc pni cx16 popcnt lahf_lm altmovcr8 misalignsse
bogomips : 4592.96
clflush size : 64
cache_alignment : 64
address sizes : 43 bits physical, 48 bits virtual
power management:

processor : 1
vendor_id : GenuineIntel
cpu family : 6
model : 85
model name : Intel(R) Xeon(R) Gold 5118 CPU @ 2.30GHz
stepping : 4
cpu MHz : 2294.365
cache size : 16896 KB
physical id : 0
siblings : 4
core id : 1
cpu cores : 4
fpu : yes
fpu_exception : yes
cpuid level : 22
wp : yes
flags : fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush mmx fxsr sse sse2 ss ht syscall nx pdpe1gb rdtscp lm constant_tsc pni cx16 popcnt lahf_lm altmovcr8 misalignsse
bogomips : 4589.02
clflush size : 64
cache_alignment : 64
address sizes : 43 bits physical, 48 bits virtual
power management:

processor : 2
vendor_id : GenuineIntel
cpu family : 6
model : 85
model name : Intel(R) Xeon(R) Gold 5118 CPU @ 2.30GHz
stepping : 4
cpu MHz : 2294.365
cache size : 16896 KB
physical id : 0
siblings : 4
core id : 2
cpu cores : 4
fpu : yes
fpu_exception : yes
cpuid level : 22
wp : yes
flags : fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush mmx fxsr sse sse2 ss ht syscall nx pdpe1gb rdtscp lm constant_tsc pni cx16 popcnt lahf_lm altmovcr8 misalignsse
bogomips : 4590.82
clflush size : 64
cache_alignment : 64
address sizes : 43 bits physical, 48 bits virtual
power management:

processor : 3
vendor_id : GenuineIntel
cpu family : 6
model : 85
model name : Intel(R) Xeon(R) Gold 5118 CPU @ 2.30GHz
stepping : 4
cpu MHz : 2294.365
cache size : 16896 KB
physical id : 0
siblings : 4
core id : 3
cpu cores : 4
fpu : yes
fpu_exception : yes
cpuid level : 22
wp : yes
flags : fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush mmx fxsr sse sse2 ss ht syscall nx pdpe1gb rdtscp lm constant_tsc pni cx16 popcnt lahf_lm altmovcr8 misalignsse
bogomips : 4589.38
clflush size : 64
cache_alignment : 64
address sizes : 43 bits physical, 48 bits virtual
power management:
"""

IS_COREXL_LICENSE_MATCH_NUM_CORES_PAIR_2 = (CPLIC_PRINT_CAT_CPUINFO_OUT_2, True)

#SSH command: cat /proc/interrupts | grep -E "CPU|eth"
CAT_INTERRUMPS_GREP_CPU_ETH_OUT_1 = """           CPU0       CPU1       CPU2       CPU3
 59:  284744607          0          0          0   IO-APIC-level  ioc0, eth2
 67:  954122444          0          0          0   IO-APIC-level  eth0
 75:  285858258          0          0          0   IO-APIC-level  eth1
"""
IS_INTERRUPT_LOAD_IMBALANCED_PAIR_1 = (CAT_INTERRUMPS_GREP_CPU_ETH_OUT_1,False)

CAT_INTERRUMPS_GREP_CPU_ETH_OUT_2 = """           CPU0       CPU1       CPU2       CPU3
 59:  500              500        500        500   IO-APIC-level  ioc0, eth2
 67:  0                  0          0          0   IO-APIC-level  eth0
 75:  0                  0          0          0   IO-APIC-level  eth1
"""

IS_INTERRUPT_LOAD_IMBALANCED_PAIR_2 = (CAT_INTERRUMPS_GREP_CPU_ETH_OUT_2, True)

#SSH command: ps -Ao user,uid,comm,pid,pcpu,tty --sort=-pcpu | head -n 20
PS_AO_PCPU_OUT_1 = """USER       UID COMMAND           PID %CPU TT
admin        0 bash             8672  1.0 pts/16
admin        0 fw_worker_0      4507  0.8 ?
admin        0 cpd              5973  0.5 ?
admin        0 sshd             8610  0.2 ?
admin        0 fw_full          6110  0.2 ?
admin        0 confd            5474  0.1 ?
admin        0 routed           6872  0.1 ?
admin        0 sshd             7228  0.1 ?
admin        0 sshd            25981  0.1 ?
admin        0 sshd            19917  0.1 ?
admin        0 sshd             2396  0.1 ?
admin        0 sshd            22384  0.1 ?
admin        0 sshd            14469  0.1 ?
admin        0 sshd            12543  0.0 ?
admin        0 events/0           14  0.0 ?
admin        0 bash            17437  0.0 pts/1
admin        0 routed           6866  0.0 ?
admin        0 bash            12998  0.0 pts/2
admin        0 cpview_historyd  5963  0.0 ?
"""

IS_SOME_PROCESS_CONSUMING_HIGH_CPU_PAIR_1 = (PS_AO_PCPU_OUT_1, False)

PS_AO_PCPU_OUT_2 = """USER       UID COMMAND           PID %CPU TT
admin        0 bash             8672  85.0 pts/16
admin        0 fw_worker_0      4507  0.8 ?
admin        0 cpd              5973  0.5 ?
admin        0 sshd             8610  0.2 ?
admin        0 fw_full          6110  0.2 ?
admin        0 confd            5474  0.1 ?
admin        0 routed           6872  0.1 ?
admin        0 sshd             7228  0.1 ?
admin        0 sshd            25981  0.1 ?
admin        0 sshd            19917  0.1 ?
admin        0 sshd             2396  0.1 ?
admin        0 sshd            22384  0.1 ?
admin        0 sshd            14469  0.1 ?
admin        0 sshd            12543  0.0 ?
admin        0 events/0           14  0.0 ?
admin        0 bash            17437  0.0 pts/1
admin        0 routed           6866  0.0 ?
admin        0 bash            12998  0.0 pts/2
admin        0 cpview_historyd  5963  0.0 ?
"""

IS_SOME_PROCESS_CONSUMING_HIGH_CPU_PAIR_2 = (PS_AO_PCPU_OUT_2, True)


CPMONITOR_OUT = """ ==================================
|            Dump Info             |
 ==================================
| Name: output_test.pcap           |
| Start time: 10/18/20 00:00:47.82 |
| End   time: 10/18/20 00:00:51.82 |
 ==================================
 ==============================================================
|                         Total Usage                          |
 ==============================================================
| Capture duration:        4.1 seconds                         |
| Total packets:           1130                                |
| Total packets size:      154 KB                              |
| Avg. PPS:                282.43 [packets/second]             |
| Max PPS:                 516 [packets/second] at 00:00:50.82 |
| Avg. CPS:                0.75 [conns/second]                 |
| Max CPS:                 2 [conns/second] at 00:00:48.82     |
| Avg. throughput:         308 Kb [bits/second]                |
| Max throughput:          529 Kb [bits/second] at 00:00:47.82 |
| Avg. concurrent conns:   2.50 [conns/second]                 |
| Max concurrent conns:    3 [conns/second] at 00:00:48.82     |
| Sort method:             throughput                          |
| Unsupported entries:     0                                   |
 ==============================================================
Top Connections
 ==========================================================================================================================
|    source     | port  |    dest     | port  | ipp | packets (%) [c2s/s2c] |  size (%) [c2s/s2c]   |  avg   |    desc.    |
 ==========================================================================================================================
| 10.10.10.118  | 42478 | 10.11.80.1  |    22 | TCP |   573 (50%) [47%/53%] | 81 KB (52%) [17%/83%] | 53/229 | ssh         |
| 10.11.80.23   | 44968 | 10.11.80.1  |    22 | TCP |   551 (48%) [43%/57%] | 71 KB (46%) [17%/83%] | 54/194 | ssh         |
| 10.11.80.254  |  5353 | 224.0.0.251 |  5353 | UDP |      1 (0%) [100%/0%] |  348 B (0%) [100%/0%] |  348/0 | mdns        |
| 192.168.250.1 | 33458 | 10.11.80.1  |    22 | TCP |      3 (0%) [66%/34%] |  280 B (0%) [37%/63%] | 52/176 | ssh         |
| 10.11.80.8    | 48568 | 10.11.80.1  | 18192 | TCP |      2 (0%) [50%/50%] |  138 B (0%) [62%/38%] |  86/52 | Check Point |
 ==========================================================================================================================
Top destinations
 =======================================================================
|     IP      | packets (%) [c2s/s2c] |   size (%) [c2s/s2c]   |  avg   |
 =======================================================================
| 10.11.80.1  |  1129 (99%) [45%/55%] | 153 KB (99%) [17%/83%] | 54/210 |
| 224.0.0.251 |      1 (0%) [100%/0%] |   348 B (0%) [100%/0%] |  348/0 |
 =======================================================================
Top Services
 =======================================================================================
| service | ipp | packets (%) [c2s/s2c] |   size (%) [c2s/s2c]   |  avg   |    desc.    |
 =======================================================================================
|      22 | TCP |  1127 (99%) [45%/55%] | 153 KB (99%) [17%/83%] | 54/211 | ssh         |
|    5353 | UDP |      1 (0%) [100%/0%] |   348 B (0%) [100%/0%] |  348/0 | mdns        |
|   18192 | TCP |      2 (0%) [50%/50%] |   138 B (0%) [62%/38%] |  86/52 | Check Point |
 =======================================================================================
 ==================
| TCP Stat | Count |
 ==================
| ACK      |   510 |
| ACK|PUSH |   619 |
 ==================
"""
CPMONITOR_PARSED = [
	[
	"52",
	"10.10.10.118",
	"10.11.80.1",
	"TCP",
	"22",
	],
	[
    "46",
	"10.11.80.23",
	"10.11.80.1",
	"TCP",
	"22",
	],
	[
    "0",
	"10.11.80.254",
	"224.0.0.251",
	"UDP",
	"5353",
	],
	[
    "0",
	"192.168.250.1",
	"10.11.80.1",
	"TCP",
	"22",
	],
	[
    "0",
	"10.11.80.8",
	"10.11.80.1",
	"TCP",
	"18192",
	]
]

CPMONITOR_PAIR1 = (CPMONITOR_OUT,  CPMONITOR_PARSED)