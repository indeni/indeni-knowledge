import unittest
import automation.workflows.high_per_core_cpu_use_by_device.checkpoint.high_per_core_cpu_use_by_device_mock_data as mock
import automation.workflows.high_per_core_cpu_use_by_device.checkpoint.high_per_core_cpu_use_by_device_parser as parser


class HighPerCoreCPUParserTests(unittest.TestCase):
    def test_get_cpu_utilization(self):
        pair1 = mock.GET_CPU_UTILIZATION_PAIR_1
        pair2 = mock.GET_CPU_UTILIZATION_PAIR_2
        pair3 = mock.GET_CPU_UTILIZATION_PAIR_3
        pair4 = mock.GET_CPU_UTILIZATION_PAIR_4
        self.assertEqual(pair1[1], parser.get_cpu_utilization(pair1[0][0],pair1[0][1]), 3)
        self.assertEqual(pair2[1], parser.get_cpu_utilization(pair2[0][0],pair2[0][1]), 3)
        self.assertEqual(pair3[1], parser.get_cpu_utilization(pair3[0][0],pair3[0][1]), 3)
        self.assertEqual(pair4[1], parser.get_cpu_utilization(pair4[0][0],pair4[0][1]), 3)

    def test_bool_is_utilization_across_all_cpus(self):
        pair1 = mock.IS_UTILIZATION_ACROSS_ALL_CPUS_PAIR_1
        pair2 = mock.IS_UTILIZATION_ACROSS_ALL_CPUS_PAIR_2
        self.assertEqual(pair1[1], parser.bool_is_utilization_across_all_cpus(pair1[0]))
        self.assertEqual(pair2[1], parser.bool_is_utilization_across_all_cpus(pair2[0]))

    def test_get_securexl_and_templates_enabled(self):
        pair1 = mock.GET_SECUREXL_AND_TEMPLATES_ENABLED_PAIR_1
        pair2 = mock.GET_SECUREXL_AND_TEMPLATES_ENABLED_PAIR_2
        self.assertEqual(pair1[1], parser.get_securexl_and_templates_enabled(pair1[0]))
        self.assertEqual(pair2[1], parser.get_securexl_and_templates_enabled(pair2[0]))

    def test_is_redistribution_automatic(self):
        pair1 = mock.IS_REDISTRIBUTION_AUTOMATIC_PAIR_1
        pair2 = mock.IS_REDISTRIBUTION_AUTOMATIC_PAIR_2
        self.assertEqual(pair1[1], parser.is_redistribution_automatic(pair1[0]))
        self.assertEqual(pair2[1], parser.is_redistribution_automatic(pair2[0]))

    def test_is_corexl_running(self):
        pair1 = mock.IS_COREXL_RUNNING_PAIR_1
        pair2 = mock.IS_COREXL_RUNNING_PAIR_2
        pair3 = mock.IS_COREXL_RUNNING_PAIR_3
        self.assertEqual(pair1[1], parser.is_corexl_running(pair1[0]))
        self.assertEqual(pair2[1], parser.is_corexl_running(pair2[0]))
        self.assertEqual(pair3[1], parser.is_corexl_running(pair3[0]))

    def test_is_corexl_license_match_num_cores(self):
        pair1 = mock.IS_COREXL_LICENSE_MATCH_NUM_CORES_PAIR_1
        pair2 = mock.IS_COREXL_LICENSE_MATCH_NUM_CORES_PAIR_2
        self.assertEqual(pair1[1], parser.is_corexl_license_match_num_cores(pair1[0]))
        self.assertEqual(pair2[1], parser.is_corexl_license_match_num_cores(pair2[0]))

    def test_is_some_process_consuming_high_cpu(self):
        pair1 = mock.IS_SOME_PROCESS_CONSUMING_HIGH_CPU_PAIR_1
        pair2 = mock.IS_SOME_PROCESS_CONSUMING_HIGH_CPU_PAIR_2
        self.assertEqual(pair1[1], parser.is_some_process_consuming_high_cpu(pair1[0]))
        self.assertEqual(pair2[1], parser.is_some_process_consuming_high_cpu(pair2[0]))

    def test_collect_affinities(self):
        pair1 = mock.COLLECT_AFFINITIES_PAIR_1
        pair2 = mock.COLLECT_AFFINITIES_PAIR_2
        self.assertEqual(pair1[1], parser.collect_affinities(pair1[0][0],pair1[0][1]))
        self.assertEqual(pair2[1], parser.collect_affinities(pair2[0][0],pair2[0][1]))

    def test_get_cpmonitor_top_talkers(self):
        pair1 = mock.CPMONITOR_PAIR1
        self.assertListEqual(pair1[1], parser.get_cpmonitor_top_talkers(pair1[0]))

if __name__ == '__main__':
    unittest.main()
