id: discarded_logs_paloaltonetworks
friendly_name: Discarded logs
start_block: get_device_tags

blocks:
  get_device_tags:
    type: device_tags
    name: Get device tags
    register_to: my_device_tags
    go_to: make_variant_matches

  make_variant_matches:
    type: logic
    name: Match device against variant
    args: [my_device_tags]
    method: check_variant_matches
    register_to: bool_variant_matches
    go_to: check_if_variant_matches

  check_if_variant_matches:
    type: if
    name: Check if device matches requirements
    condition: bool_variant_matches
    then_go_to: get_logging_rates
    else_go_to: variant_conclusion

  variant_conclusion:
    type: conclusion
    name: Incompatible workflow
    triage_conclusion: |
      This workflow is incompatible with the device.
    triage_remediation_steps: |
      Contact Indeni and request that compatibility be added.

  get_logging_rates:
    type: device_task
    name: Get max logging rate
    runner:
      type: HTTPS
      command: '/api?type=op&cmd=<show><running><logging/></running></show>'
    parser:
      method: panos_parse_show_running_logging
      args: []
    register_to: show_running_logging_out
    go_to: check_if_logging_high

  check_if_logging_high:
    type: if
    name: Check if logging is too high
    condition: int(show_running_logging_out['LOG_SENT_RATE']) > int(show_running_logging_out['MAX_LOG_RATE'])
    then_go_to: report_logging_high
    else_go_to: get_session_utilization

  report_logging_high:
    type: conclusion
    name: Report current logging rate is high
    triage_conclusion: |
      The current logging rate is greater than the Max. Logging rate.
    triage_remediation_steps: |
      Check traffic pattern and determine if hardware upgrade is required.
      As a workaround, you can disable logging for some type of traffic like DNS/Ping.
      Consider to enable "Log Container page only" for URL Profiles.

  get_session_utilization:
    type: device_task
    name: Check the session utilization value
    runner:
      type: HTTPS
      command: /api/?type=op&cmd=<show><session><info></info></session></show>
    parser:
      method: get_session_utilization
      args: []
    register_to: session_utilization
    go_to: check_if_session_utilization_high

  check_if_session_utilization_high:
    type: if
    name: Check if the session utilization is greater or equal to 70%
    condition: float(session_utilization) >= 70
    then_go_to: report_session_utilization_high
    else_go_to: get_log_collectors

  report_session_utilization_high:
    type: conclusion
    name: Current Session Utilization is high
    triage_conclusion: |
      Utilizing more than 70% of session table is considered high.
    triage_remediation_steps: |
      Ensure that the Firewall resources are sufficient for the current traffic volume.
      Check traffic pattern and determine if hardware upgrade is required.

  get_log_collectors:
    type: device_task
    name: Check Log Collectors
    runner:
      type: HTTPS
      command: /api/?type=op&cmd=<show><logging-status></logging-status></show>
    parser:
      method: get_log_collectors
      args: []
    register_to: has_active_and_connected
    go_to: check_if_active_and_connected

  check_if_active_and_connected:
    type: if
    name: Check if the log collector is connected and active
    condition: has_active_and_connected == "True"
    then_go_to: advanced_trouble_shooting
    else_go_to: log_collector_not_active

  advanced_trouble_shooting:
    type: conclusion
    name: Advanced troubleshooting is needed
    triage_conclusion: |
      Please engage Palo Alto Networks support to analyze the logs.
    triage_remediation_steps: |
      Please raise a support ticket with Palo Alto Networks.
      Generate a new tech support file and upload it to the case.

  log_collector_not_active:
    type: conclusion
    name: Log Collector is not active
    triage_conclusion: |
      The log collector might not be running or not be reachable.
    triage_remediation_steps: |
      Verify that the log collector is reachable and accepting logs from the device.
