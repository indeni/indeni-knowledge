import unittest
import automation.workflows.panw_logs_discarded.paloaltonetworks.panw_logs_discarded_mock_data as mock
import automation.workflows.panw_logs_discarded.paloaltonetworks.panw_logs_discarded_parser as parser


class PANLogsDiscardedReasonATEOnDeviceTests(unittest.TestCase):


    def test_panos_parse_show_running_logging(self):
        pair1 = mock.GET_MAX_LOGGING_RATE_PAIR_1
        pair2 = mock.GET_MAX_LOGGING_RATE_PAIR_2
        self.assertEqual(parser.panos_parse_show_running_logging (pair1[0]), pair1[1])
        self.assertEqual(parser.panos_parse_show_running_logging (pair2[0]), pair2[1])

    def test_get_session_utilization(self):
        pair1 = mock.GET_SESSION_UTILIZATION_PAIR_1
        pair2 = mock.GET_SESSION_UTILIZATION_PAIR_2
        pair3 = mock.GET_SESSION_UTILIZATION_PAIR_3
        self.assertEqual(parser.get_session_utilization (pair1[0]), pair1[1])
        self.assertEqual(parser.get_session_utilization (pair2[0]), pair2[1])
        self.assertEqual(parser.get_session_utilization (pair3[0]), pair3[1])

    def test_get_log_collectors(self):
        pair1 = mock.GET_LOG_COLLECTOR_PAIR_1
        pair2 = mock.GET_LOG_COLLECTOR_PAIR_2
        self.assertEqual(parser.get_log_collectors (pair1[0]), pair1[1])
        self.assertEqual(parser.get_log_collectors (pair2[0]), pair2[1])


if __name__ == '__main__':
    unittest.main()
