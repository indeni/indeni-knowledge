from parser_service.public.helper_methods import *
import re


def check_variant_matches(**kwargs) -> bool:
    # This should work on all gateways
    tags = kwargs.get('my_device_tags', None)
    if tags is None:
        return False
    if tags.get('product') != 'firewall':
        return False
    return True


def panos_parse_show_running_logging(raw_data: str) -> dict:
    xml_data = parse_data_as_xml(raw_data)
    return parse_data_as_object(xml_data['response']['result']['member'], 'paloalto_panos_show_running_logging.textfsm')

def get_session_utilization(raw_data: str) -> str:
    if raw_data:
        raw_data_parsed = parse_data_as_xml(raw_data)
        return "{:.2f}".format(int(raw_data_parsed['response']['result']['num-installed'])*100/int(raw_data_parsed['response']['result']['num-max']))


def get_log_collectors(raw_data: str) -> str:
    if raw_data:
        raw_data_parsed = parse_data_as_xml(raw_data)
        for line in raw_data_parsed['response']['result'].split('\n'):
            if "'Log Collection log forwarding agent' is active and connected to" in line:
                return "True"
        return "False"
