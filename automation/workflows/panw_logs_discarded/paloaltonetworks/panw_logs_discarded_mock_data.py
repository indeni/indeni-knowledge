#SSH command: show running logging
SHOW_RUNNING_LOGGING_OUT_1 = ("""
<response status="success">
<result>
<member>Max. logging rate:            1280       cnt/s
Max. packet logging rate:     2560       KB/s
Traffic log generation rate:  0          cnt/s  0         Byte/s
Threat log generation rate:   0          cnt/s  0         Byte/s
Analytics generation rate:    0          cnt/s  0         Byte/s
Log sent rate:                1281       cnt/s  0         Byte/s
Threat Log sent rate:         0          cnt/s  0         Byte/s
Analytics sent rate:          0          cnt/s  0         Byte/s
Log send failure rate:        0          cnt/s  0         Byte/s
Threat Log send failure rate: 0          cnt/s  0         Byte/s
Analytics send failure rate:  0          cnt/s  0         Byte/s
Current traffic log count:    0
Current threat log count:     0
Current analytics log count:  0
Random traffic log drop:      off
Log suppression:              on
default-policy-logging:       off
</member>
</result>
</response>
""")

SHOW_RUNNING_LOGGING_OUT_2 = ("""<response status="success">
<result>
<member>Max. logging rate:            1280       cnt/s
Max. packet logging rate:     2560       KB/s
Traffic log generation rate:  0          cnt/s  0         Byte/s
Threat log generation rate:   0          cnt/s  0         Byte/s
Analytics generation rate:    0          cnt/s  0         Byte/s
Log sent rate:                640        cnt/s  0         Byte/s
Threat Log sent rate:         0          cnt/s  0         Byte/s
Analytics sent rate:          0          cnt/s  0         Byte/s
Log send failure rate:        0          cnt/s  0         Byte/s
Threat Log send failure rate: 0          cnt/s  0         Byte/s
Analytics send failure rate:  0          cnt/s  0         Byte/s
Current traffic log count:    0
Current threat log count:     0
Current analytics log count:  0
Random traffic log drop:      off
Log suppression:              on
default-policy-logging:       off
</member>
</result>
</response>
""")

GET_MAX_LOGGING_RATE_PAIR_1 = (SHOW_RUNNING_LOGGING_OUT_1, {'MAX_LOG_RATE': '1280', 'LOG_SENT_RATE': '1281'})

GET_MAX_LOGGING_RATE_PAIR_2 = (SHOW_RUNNING_LOGGING_OUT_2, {'MAX_LOG_RATE': '1280', 'LOG_SENT_RATE': '640'})

#SSH command: show session info | match "Session table utilization"
SHOW_SESSION_INFO_UTILIZATION_OUT_1 = ("""
<response status="success">
<result>
<tmo-5gcdelete>15</tmo-5gcdelete>
<tmo-sctpshutdown>60</tmo-sctpshutdown>
<tcp-nonsyn-rej>True</tcp-nonsyn-rej>
<tmo-tcpinit>5</tmo-tcpinit>
<tmo-tcp>3600</tmo-tcp>
<pps>0</pps>
<tmo-tcp-delayed-ack>250</tmo-tcp-delayed-ack>
<num-max>819200</num-max>
<age-scan-thresh>80</age-scan-thresh>
<tmo-tcphalfclosed>120</tmo-tcphalfclosed>
<num-active>1</num-active>
<tmo-sctp>3600</tmo-sctp>
<dis-def>60</dis-def>
<num-mcast>0</num-mcast>
<icmp-unreachable-rate>200</icmp-unreachable-rate>
<tmo-tcptimewait>15</tmo-tcptimewait>
<age-scan-ssf>8</age-scan-ssf>
<tmo-udp>30</tmo-udp>
<vardata-rate>10485760</vardata-rate>
<age-scan-tmo>10</age-scan-tmo>
<dis-sctp>30</dis-sctp>
<dp>*.dp0</dp>
<dis-tcp>90</dis-tcp>
<tcp-reject-siw-thresh>4</tcp-reject-siw-thresh>
<num-udp>0</num-udp>
<tmo-sctpcookie>60</tmo-sctpcookie>
<num-http2-5gc>0</num-http2-5gc>
<tmo-icmp>6</tmo-icmp>
<max-pending-mcast>0</max-pending-mcast>
<age-accel-thresh>80</age-accel-thresh>
<tcp-diff-syn-rej>True</tcp-diff-syn-rej>
<tunnel-accel>True</tunnel-accel>
<num-gtpc>0</num-gtpc>
<oor-action>drop</oor-action>
<tmo-def>30</tmo-def>
<num-predict>0</num-predict>
<age-accel-en>True</age-accel-en>
<age-accel-tsf>2</age-accel-tsf>
<hw-offload>True</hw-offload>
<num-icmp>0</num-icmp>
<num-gtpu-active>0</num-gtpu-active>
<tmo-cp>30</tmo-cp>
<tcp-strict-rst>True</tcp-strict-rst>
<tmo-sctpinit>5</tmo-sctpinit>
<strict-checksum>True</strict-checksum>
<tmo-tcp-unverif-rst>30</tmo-tcp-unverif-rst>
<num-bcast>0</num-bcast>
<ipv6-fw>True</ipv6-fw>
<cps>0</cps>
<num-installed>1</num-installed>
<num-tcp>0</num-tcp>
<dis-udp>60</dis-udp>
<num-sctp-assoc>0</num-sctp-assoc>
<num-sctp-sess>0</num-sctp-sess>
<tcp-reject-siw-enable>False</tcp-reject-siw-enable>
<tmo-tcphandshake>10</tmo-tcphandshake>
<hw-udp-offload>True</hw-udp-offload>
<kbps>0</kbps>
<num-gtpu-pending>0</num-gtpu-pending>
</result>
</response>
""")

SHOW_SESSION_INFO_UTILIZATION_OUT_2 = ("""
<response status="success">
<result>
<tmo-5gcdelete>15</tmo-5gcdelete>
<tmo-sctpshutdown>60</tmo-sctpshutdown>
<tcp-nonsyn-rej>True</tcp-nonsyn-rej>
<tmo-tcpinit>5</tmo-tcpinit>
<tmo-tcp>3600</tmo-tcp>
<pps>0</pps>
<tmo-tcp-delayed-ack>250</tmo-tcp-delayed-ack>
<num-max>819200</num-max>
<age-scan-thresh>80</age-scan-thresh>
<tmo-tcphalfclosed>120</tmo-tcphalfclosed>
<num-active>1</num-active>
<tmo-sctp>3600</tmo-sctp>
<dis-def>60</dis-def>
<num-mcast>0</num-mcast>
<icmp-unreachable-rate>200</icmp-unreachable-rate>
<tmo-tcptimewait>15</tmo-tcptimewait>
<age-scan-ssf>8</age-scan-ssf>
<tmo-udp>30</tmo-udp>
<vardata-rate>10485760</vardata-rate>
<age-scan-tmo>10</age-scan-tmo>
<dis-sctp>30</dis-sctp>
<dp>*.dp0</dp>
<dis-tcp>90</dis-tcp>
<tcp-reject-siw-thresh>4</tcp-reject-siw-thresh>
<num-udp>0</num-udp>
<tmo-sctpcookie>60</tmo-sctpcookie>
<num-http2-5gc>0</num-http2-5gc>
<tmo-icmp>6</tmo-icmp>
<max-pending-mcast>0</max-pending-mcast>
<age-accel-thresh>80</age-accel-thresh>
<tcp-diff-syn-rej>True</tcp-diff-syn-rej>
<tunnel-accel>True</tunnel-accel>
<num-gtpc>0</num-gtpc>
<oor-action>drop</oor-action>
<tmo-def>30</tmo-def>
<num-predict>0</num-predict>
<age-accel-en>True</age-accel-en>
<age-accel-tsf>2</age-accel-tsf>
<hw-offload>True</hw-offload>
<num-icmp>0</num-icmp>
<num-gtpu-active>0</num-gtpu-active>
<tmo-cp>30</tmo-cp>
<tcp-strict-rst>True</tcp-strict-rst>
<tmo-sctpinit>5</tmo-sctpinit>
<strict-checksum>True</strict-checksum>
<tmo-tcp-unverif-rst>30</tmo-tcp-unverif-rst>
<num-bcast>0</num-bcast>
<ipv6-fw>True</ipv6-fw>
<cps>0</cps>
<num-installed>573440</num-installed>
<num-tcp>0</num-tcp>
<dis-udp>60</dis-udp>
<num-sctp-assoc>0</num-sctp-assoc>
<num-sctp-sess>0</num-sctp-sess>
<tcp-reject-siw-enable>False</tcp-reject-siw-enable>
<tmo-tcphandshake>10</tmo-tcphandshake>
<hw-udp-offload>True</hw-udp-offload>
<kbps>0</kbps>
<num-gtpu-pending>0</num-gtpu-pending>
</result>
</response>
""")

SHOW_SESSION_INFO_UTILIZATION_OUT_3 = ("""
<response status="success">
<result>
<tmo-5gcdelete>15</tmo-5gcdelete>
<tmo-sctpshutdown>60</tmo-sctpshutdown>
<tcp-nonsyn-rej>True</tcp-nonsyn-rej>
<tmo-tcpinit>5</tmo-tcpinit>
<tmo-tcp>3600</tmo-tcp>
<pps>0</pps>
<tmo-tcp-delayed-ack>250</tmo-tcp-delayed-ack>
<num-max>819200</num-max>
<age-scan-thresh>80</age-scan-thresh>
<tmo-tcphalfclosed>120</tmo-tcphalfclosed>
<num-active>1</num-active>
<tmo-sctp>3600</tmo-sctp>
<dis-def>60</dis-def>
<num-mcast>0</num-mcast>
<icmp-unreachable-rate>200</icmp-unreachable-rate>
<tmo-tcptimewait>15</tmo-tcptimewait>
<age-scan-ssf>8</age-scan-ssf>
<tmo-udp>30</tmo-udp>
<vardata-rate>10485760</vardata-rate>
<age-scan-tmo>10</age-scan-tmo>
<dis-sctp>30</dis-sctp>
<dp>*.dp0</dp>
<dis-tcp>90</dis-tcp>
<tcp-reject-siw-thresh>4</tcp-reject-siw-thresh>
<num-udp>0</num-udp>
<tmo-sctpcookie>60</tmo-sctpcookie>
<num-http2-5gc>0</num-http2-5gc>
<tmo-icmp>6</tmo-icmp>
<max-pending-mcast>0</max-pending-mcast>
<age-accel-thresh>80</age-accel-thresh>
<tcp-diff-syn-rej>True</tcp-diff-syn-rej>
<tunnel-accel>True</tunnel-accel>
<num-gtpc>0</num-gtpc>
<oor-action>drop</oor-action>
<tmo-def>30</tmo-def>
<num-predict>0</num-predict>
<age-accel-en>True</age-accel-en>
<age-accel-tsf>2</age-accel-tsf>
<hw-offload>True</hw-offload>
<num-icmp>0</num-icmp>
<num-gtpu-active>0</num-gtpu-active>
<tmo-cp>30</tmo-cp>
<tcp-strict-rst>True</tcp-strict-rst>
<tmo-sctpinit>5</tmo-sctpinit>
<strict-checksum>True</strict-checksum>
<tmo-tcp-unverif-rst>30</tmo-tcp-unverif-rst>
<num-bcast>0</num-bcast>
<ipv6-fw>True</ipv6-fw>
<cps>0</cps>
<num-installed>491520</num-installed>
<num-tcp>0</num-tcp>
<dis-udp>60</dis-udp>
<num-sctp-assoc>0</num-sctp-assoc>
<num-sctp-sess>0</num-sctp-sess>
<tcp-reject-siw-enable>False</tcp-reject-siw-enable>
<tmo-tcphandshake>10</tmo-tcphandshake>
<hw-udp-offload>True</hw-udp-offload>
<kbps>0</kbps>
<num-gtpu-pending>0</num-gtpu-pending>
</result>
</response>
""")

GET_SESSION_UTILIZATION_PAIR_1 = (SHOW_SESSION_INFO_UTILIZATION_OUT_1, '0.00')

GET_SESSION_UTILIZATION_PAIR_2 = (SHOW_SESSION_INFO_UTILIZATION_OUT_2, '70.00')

GET_SESSION_UTILIZATION_PAIR_3 = (SHOW_SESSION_INFO_UTILIZATION_OUT_3, '60.00')

#SSH command: request log-collector-forwarding status
REQUEST_LOG_COLLECTOR_FORWARDING_STATUS_OUT_1 = ("""
<response status="success">
<result>----------------------------------------------------------------------------------------------------------------------------- Type Last Log Created Last Log Fwded Last Seq Num Fwded Last Seq Num Acked Total Logs Fwded ----------------------------------------------------------------------------------------------------------------------------- > CMS 0 Not Sending to CMS 0 > CMS 1 Not Sending to CMS 1 >Log Collector Not Sending to Log Collector </result>
</response>
""")

REQUEST_LOG_COLLECTOR_FORWARDING_STATUS_OUT_2 = ("""
<response status="success">
<result>----------------------------------------------------------------------------------------------------------------------------- Type Last Log Created Last Log Fwded Last Seq Num Fwded Last Seq Num Acked Total Logs Fwded ----------------------------------------------------------------------------------------------------------------------------- > CMS 0 Not Sending to CMS 0 > CMS 1 Not Sending to CMS 1 >Log Collector 'Log Collection log forwarding agent' is active and connected to 10.10.21.9 </result>
</response>
""")

GET_LOG_COLLECTOR_PAIR_1 = (REQUEST_LOG_COLLECTOR_FORWARDING_STATUS_OUT_1, "False")

GET_LOG_COLLECTOR_PAIR_2 = (REQUEST_LOG_COLLECTOR_FORWARDING_STATUS_OUT_2, "True")