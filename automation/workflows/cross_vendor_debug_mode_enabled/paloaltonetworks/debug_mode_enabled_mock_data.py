DEFAULT_LOGGING_LEVEL_PAIR_1 = ("""
       masterd -       s1.mp.2463-0 - info
     sysdagent -       s1.mp.2569-0 - info
        dagger -       s1.mp.2568-1 - info
plugin_api_server -       s1.mp.2581-0 - info
      vm_agent -       s1.mp.2580-3 - info
         ehmon -       s1.mp.2632-0 - info
         chasd -       s1.mp.2633-1 - info
      brdagent -       s1.mp.2942-0 - info
        crypto -       s1.mp.3058-0 - info
          comm -      s1.mp.3322-23 - error
       useridd -       s1.mp.3408-1 - info
      mgmtsrvr -       s1.mp.3466-0 - info
      mgmtsrvr -       s1.mp.3466-2 - info
      ha_agent -       s1.mp.3756-1 - debug
        l2ctrl -       s1.mp.3755-0 - info
       devsrvr -       s1.mp.3405-3 - info
         ifmgr -       s1.mp.3753-0 - info
        rasmgr -       s1.mp.3751-0 - info
          dhcp -       s1.mp.3759-0 - info
        routed -       s1.mp.3762-1 - info
          satd -       s1.mp.3757-0 - info
       varrcvr -       s1.mp.3754-2 - info
        sslmgr -       s1.mp.3758-2 - info
         pppoe -       s1.mp.3761-0 - warn
        keymgr -       s1.mp.3752-1 - info
      dnsproxy -       s1.mp.3760-0 - warn
        ikemgr -       s1.mp.3746-1 - info
           bfd -      s1.mp.3747-23 - info
          tund -      s1.mp.3749-23 - info
       mprelay -      s1.mp.3748-23 - info
         authd -       s1.mp.3763-0 - debug
       unknown -       s1.mp.4059-2 - warn
       unknown -       s1.mp.4060-2 - warn
       unknown -       s1.mp.4061-2 - warn
       logrcvr -       s1.mp.6577-1 - info
       unknown -      s1.mp.25739-0 - error
       unknown -      s1.mp.25739-2 - error
       unknown -      s1.mp.21929-0 - error
       unknown -      s1.mp.21929-2 - error
       unknown -      s1.mp.31711-0 - error
       unknown -      s1.mp.31711-2 - error
       unknown -      s1.mp.21565-2 - warn
        sslvpn -      s1.mp.25019-0 - info
        sslvpn -      s1.mp.25019-2 - info
         snmpd -       s1.mp.8727-0 - warn
       unknown -       s1.mp.8939-2 - info
       unknown -       s1.mp.8938-2 - info
       unknown -      s1.mp.31500-0 - error
       unknown -      s1.mp.31500-2 - error
       unknown -       s1.mp.3285-0 - error
       unknown -       s1.mp.3285-2 - error
""", {
      'masterd': 'info',
      'sysdagent': 'info',
      'dagger': 'info',
      'plugin_api_server': 'info',
      'vm_agent': 'info',
      'ehmon': 'info',
      'chasd': 'info',
      'brdagent': 'info',
      'crypto': 'info',
      'comm': 'error',
      'useridd': 'info',
      'mgmtsrvr': 'info',
      'ha_agent': 'debug',
      'l2ctrl': 'info',
      'devsrvr': 'info',
      'ifmgr': 'info',
      'rasmgr': 'info',
      'dhcp': 'info',
      'routed': 'info',
      'satd': 'info',
      'varrcvr': 'info',
      'sslmgr': 'info',
      'pppoe': 'warn',
      'keymgr': 'info',
      'dnsproxy': 'warn',
      'ikemgr': 'info',
      'bfd': 'info',
      'tund': 'info',
      'mprelay': 'info',
      'authd': 'debug',
      'logrcvr': 'info',
      'sslvpn': 'info',
      'snmpd': 'warn'})

    
DEVICE_LOGGING_LEVEL_PAIR_1 = ({
                                'masterd': 'info',
                                'sysdagent': 'info',
                                'dagger': 'info',
                                'plugin_api_server': 'info',
                                'vm_agent': 'info',
                                'ehmon': 'info',
                                'chasd': 'info',
                                'brdagent': 'info',
                                'crypto': 'info',
                                'comm': 'error',
                                'useridd': 'info',
                                'mgmtsrvr': 'info',
                                'ha_agent': 'debug',
                                'l2ctrl': 'info',
                                'devsrvr': 'info',
                                'ifmgr': 'info',
                                'rasmgr': 'info',
                                'dhcp': 'info',
                                'routed': 'info',
                                'satd': 'info',
                                'varrcvr': 'info',
                                'sslmgr': 'info',
                                'pppoe': 'warn',
                                'keymgr': 'info',
                                'dnsproxy': 'warn',
                                'ikemgr': 'info',
                                'bfd': 'info',
                                'tund': 'info',
                                'mprelay': 'info',
                                'authd': 'debug',
                                'logrcvr': 'info',
                                'sslvpn': 'info',
                                'snmpd': 'warn'}, 'l2ctrl', True)

DEVICE_LOGGING_LEVEL_PAIR_2 = ({
                                'masterd': 'info',
                                'sysdagent': 'info',
                                'dagger': 'info',
                                'plugin_api_server': 'info',
                                'vm_agent': 'info',
                                'ehmon': 'info',
                                'chasd': 'info',
                                'brdagent': 'info',
                                'crypto': 'info',
                                'comm': 'error',
                                'useridd': 'info',
                                'mgmtsrvr': 'info',
                                'ha_agent': 'debug',
                                'l2ctrl': 'debug',
                                'devsrvr': 'info',
                                'ifmgr': 'info',
                                'rasmgr': 'info',
                                'dhcp': 'info',
                                'routed': 'info',
                                'satd': 'info',
                                'varrcvr': 'info',
                                'sslmgr': 'info',
                                'pppoe': 'warn',
                                'keymgr': 'info',
                                'dnsproxy': 'warn',
                                'ikemgr': 'info',
                                'bfd': 'info',
                                'tund': 'info',
                                'mprelay': 'info',
                                'authd': 'debug',
                                'logrcvr': 'info',
                                'sslvpn': 'info',
                                'snmpd': 'warn'}, 'l2ctrl', False)