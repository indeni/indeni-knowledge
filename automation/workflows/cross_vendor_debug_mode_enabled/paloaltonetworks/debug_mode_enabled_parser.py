import re
from typing import List
from parser_service.public.helper_methods import *

DEFAULT_DEBUG_LEVELS = {
    'masterd': 'info',
    'sysdagent': 'info',
    'dagger': 'info',
    'plugin_api_server': 'info',
    'vm_agent': 'info',
    'ehmon': 'info',
    'chasd': 'info',
    'brdagent': 'info',
    'crypto': 'info',
    'comm': 'error',
    'useridd': 'info',
    'mgmtsrvr': 'info',
    'ha_agent': 'debug',
    'l2ctrl': 'info',
    'devsrvr': 'info',
    'ifmgr': 'info',
    'rasmgr': 'info',
    'dhcp': 'info',
    'routed': 'info',
    'satd': 'info',
    'varrcvr': 'info',
    'sslmgr': 'info',
    'pppoe': 'warn',
    'keymgr': 'info',
    'dnsproxy': 'warn',
    'ikemgr': 'info',
    'bfd': 'info',
    'tund': 'info',
    'mprelay': 'info',
    'authd': 'debug',
    'logrcvr': 'info',
    'sslvpn': 'info',
    'snmpd': 'warn'}


def check_variant_matches(**kwargs) -> bool:
    # This should work on all gateways
    tags = kwargs.get('my_device_tags', None)
    if tags is None:
        return False
    if tags.get('product') != 'firewall':
        return False
    return True


def parse_get_logging_level(raw_data: str, path='debug_mode_enabled.textfsm') -> str:
    raw_data_parsed = parse_data_as_list(raw_data, path)
    services = {}

    for service_entry in raw_data_parsed:
        if 'unknown' not in service_entry['service']:
            services.update({service_entry['service']: service_entry['debug_level']})
    return services


def parse_check_logging_level_setting(**kwargs) -> bool:
    device_logging_levels = kwargs.get('logging_levels')
    my_debug_item = kwargs.get('my_logging_level_item')
    # IKP-4115, sometimes the issue item is in a different format, need to extract relevant part (between dots)
    if '.' in my_debug_item:
        my_debug_item = my_debug_item.split('.')[1]
    return DEFAULT_DEBUG_LEVELS[my_debug_item] == device_logging_levels[my_debug_item]
