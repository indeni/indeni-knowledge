import unittest
import re
import automation.workflows.cross_vendor_debug_mode_enabled.paloaltonetworks.debug_mode_enabled_mock_data as mock
import automation.workflows.cross_vendor_debug_mode_enabled.paloaltonetworks.debug_mode_enabled_parser as parser

class RealDeviceTests(unittest.TestCase):

    def test_parse_get_logging_mode(self):
        self.assertEqual(parser.parse_get_logging_level(mock.DEFAULT_LOGGING_LEVEL_PAIR_1[0],
                                                        path='automation/workflows/cross_vendor_debug_mode_enabled/paloaltonetworks/debug_mode_enabled.textfsm'),
                         mock.DEFAULT_LOGGING_LEVEL_PAIR_1[1])

    def test_parse_check_logging_setting_ok_l2ctl(self):
        self.assertEqual(parser.parse_check_logging_level_setting(logging_levels=mock.DEVICE_LOGGING_LEVEL_PAIR_1[0], my_logging_level_item=mock.DEVICE_LOGGING_LEVEL_PAIR_1[1]), mock.DEVICE_LOGGING_LEVEL_PAIR_1[2])

    def test_parse_check_logging_setting_failed_l2ctl(self):
        self.assertEqual(parser.parse_check_logging_level_setting(logging_levels=mock.DEVICE_LOGGING_LEVEL_PAIR_2[0], my_logging_level_item=mock.DEVICE_LOGGING_LEVEL_PAIR_2[1]), mock.DEVICE_LOGGING_LEVEL_PAIR_2[2])


if __name__ == '__main__':
    unittest.main()
