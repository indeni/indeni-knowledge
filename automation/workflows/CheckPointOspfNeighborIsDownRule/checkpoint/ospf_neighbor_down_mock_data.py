#Issue items
GET_ISSUE_ITEMS_DATA_1 = """
10.11.94.189 priority: 1 address: 10.11.94.187 (EXSTART)"""

GET_ISSUE_ITEMS_PAIR1 = (GET_ISSUE_ITEMS_DATA_1, '10.11.94.187')

GET_ISSUE_ITEMS_DATA_2 = """
10.11.94.189 priority: 1 address: 10.11.94.187 (DOWN)"""

GET_ISSUE_ITEMS_PAIR_2 = (GET_ISSUE_ITEMS_DATA_2, '10.11.94.187')

#SSH command: clish -c "show ospf neighbor {{ospf_neighbor}}
SHOW_OSPF_NEIGHBOR_1 = """
---------------------------- OSPF Instance default -----------------------------

Neighbor state flag: G - graceful restart  
Neighbor ID       Pri   State          Dead    Address           Interface         Errors  
10.11.94.14       1     2WAY           38      10.11.94.48       10.11.94.173      51  

------------------------------- OSPF Instance 1 --------------------------------

No such neighbor 10.11.94.48
"""
GET_SHOW_OSPF_NEIGHBOR_PAIR_1 = ((SHOW_OSPF_NEIGHBOR_1, '10.11.94.48'), '2WAY')

SHOW_OSPF_NEIGHBOR_2 = """
---------------------------- OSPF Instance default -----------------------------

Neighbor state flag: G - graceful restart  
Neighbor ID       Pri   State          Dead    Address           Interface         Errors  
10.11.94.14       1     DOWN           38      10.11.94.48       10.11.94.173      51  

------------------------------- OSPF Instance 1 --------------------------------

No such neighbor 10.11.94.48
"""
GET_SHOW_OSPF_NEIGHBOR_PAIR_2 = ((SHOW_OSPF_NEIGHBOR_2,'10.11.94.48'), 'DOWN')

# From argument ospf_neighbor_status
OSPF_NEIGHBOR_FULL_OR_2WAY_1 = 'FULL'

GET_OSPF_NEIGHBOR_FULL_OR_2WAY_PAIR_1 = (OSPF_NEIGHBOR_FULL_OR_2WAY_1, True)

OSPF_NEIGHBOR_FULL_OR_2WAY_2 = '2WAY'

GET_OSPF_NEIGHBOR_FULL_OR_2WAY_PAIR_2 = (OSPF_NEIGHBOR_FULL_OR_2WAY_2, True)

# From argument ospf_neighbor_status
OSPF_NEIGHBOR_EXCHANGE_OR_EXSTART_1 = 'EXCHANGE'

GET_OSPF_NEIGHBOR_EXCHANGE_OR_EXSTART_PAIR_1 = (OSPF_NEIGHBOR_EXCHANGE_OR_EXSTART_1, True)

OSPF_NEIGHBOR_EXCHANGE_OR_EXSTART_2 = 'EXSTART'

GET_OSPF_NEIGHBOR_EXCHANGE_OR_EXSTART_PAIR_2 = (OSPF_NEIGHBOR_EXCHANGE_OR_EXSTART_2, True)

# SSH command: show ospf errors dd

PARSE_OSPF_DD_ERRORS_1 = """
---------------------------- OSPF Instance default -----------------------------

Database Description Errors

    ASEinStub                 0            Type7inNonNSSA            0        
    MTU                       0            BadLSType                 0        
    NotDuplicate              0            BadSize                   0        
    OptionsMismatch           0            DuplicateLSA              0        
    DD Duplicate Router ID    0            InitSet                   21       
    Runt                      0            MasterMismatch            0        
    SlaveSeq                  0            MasterSeq                 0        
    DD TooLow                 413      


------------------------------- OSPF Instance 1 --------------------------------

Database Description Errors

    ASEinStub                 0            Type7inNonNSSA            0        
    MTU                       0            BadLSType                 0        
    NotDuplicate              0            BadSize                   0        
    OptionsMismatch           0            DuplicateLSA              0        
    DD Duplicate Router ID    0            InitSet                   0        
    Runt                      0            MasterMismatch            0        
    SlaveSeq                  0            MasterSeq                 0        
    DD TooLow                 0
---------------------------- OSPF Instance default -----------------------------

Database Description Errors

    ASEinStub                 0            Type7inNonNSSA            0        
    MTU                       0            BadLSType                 0        
    NotDuplicate              0            BadSize                   0        
    OptionsMismatch           0            DuplicateLSA              0        
    DD Duplicate Router ID    0            InitSet                   21       
    Runt                      0            MasterMismatch            0        
    SlaveSeq                  0            MasterSeq                 0        
    DD TooLow                 413      


------------------------------- OSPF Instance 1 --------------------------------

Database Description Errors

    ASEinStub                 0            Type7inNonNSSA            0        
    MTU                       0            BadLSType                 0        
    NotDuplicate              0            BadSize                   0        
    OptionsMismatch           0            DuplicateLSA              0        
    DD Duplicate Router ID    0            InitSet                   0        
    Runt                      0            MasterMismatch            0        
    SlaveSeq                  0            MasterSeq                 0        
    DD TooLow                 0
     
     """

GET_PARSE_OSPF_DD_ERRORS_PAIR_1 = (PARSE_OSPF_DD_ERRORS_1, [{'instance': 'default', 'mastermismatch': '0', 'mtu': '0'},
 {'instance': '1', 'mastermismatch': '0', 'mtu': '0'},
 {'instance': 'default', 'mastermismatch': '0', 'mtu': '0'},
 {'instance': '1', 'mastermismatch': '0', 'mtu': '0'}])

PARSE_OSPF_DD_ERRORS_2 = """
---------------------------- OSPF Instance default -----------------------------

Database Description Errors

    ASEinStub                 0            Type7inNonNSSA            0        
    MTU                       0            BadLSType                 0
    NotDuplicate              0            BadSize                   0
    OptionsMismatch           0            DuplicateLSA              0
    DD Duplicate Router ID    0            InitSet                   21
    Runt                      0            MasterMismatch            0
    SlaveSeq                  0            MasterSeq                 0
    DD TooLow                 413


------------------------------- OSPF Instance 1 --------------------------------

Database Description Errors

    ASEinStub                 0            Type7inNonNSSA            0
    MTU                       0            BadLSType                 0
    NotDuplicate              0            BadSize                   0
    OptionsMismatch           0            DuplicateLSA              0
    DD Duplicate Router ID    0            InitSet                   0
    Runt                      0            MasterMismatch            0
    SlaveSeq                  0            MasterSeq                 0
    DD TooLow                 0
---------------------------- OSPF Instance default -----------------------------

Database Description Errors

    ASEinStub                 0            Type7inNonNSSA            0
    MTU                       0            BadLSType                 0
    NotDuplicate              0            BadSize                   0
    OptionsMismatch           0            DuplicateLSA              0
    DD Duplicate Router ID    0            InitSet                   21
    Runt                      0            MasterMismatch            0
    SlaveSeq                  0            MasterSeq                 0
    DD TooLow                 413


------------------------------- OSPF Instance 1 --------------------------------

Database Description Errors

    ASEinStub                 0            Type7inNonNSSA            0
    MTU                       100            BadLSType                 0
    NotDuplicate              0            BadSize                   0
    OptionsMismatch           0            DuplicateLSA              0
    DD Duplicate Router ID    0            InitSet                   0
    Runt                      0            MasterMismatch            0
    SlaveSeq                  0            MasterSeq                 0
    DD TooLow                 0
"""
GET_PARSE_OSPF_DD_ERRORS_PAIR_2 = (PARSE_OSPF_DD_ERRORS_2, [{'instance': 'default', 'mastermismatch': '0', 'mtu': '0'},
 {'instance': '1', 'mastermismatch': '0', 'mtu': '0'},
 {'instance': 'default', 'mastermismatch': '0', 'mtu': '0'},
 {'instance': '1', 'mastermismatch': '0', 'mtu': '100'}])

# From argument lsit_ospf_instance_mtu_and_master_mismatch

MTU_MASTER_MISMATCH_ERRORS_1 = [{'instance': 'default', 'mastermismatch': '0', 'mtu': '0'},
 {'instance': '1', 'mastermismatch': '0', 'mtu': '0'},
 {'instance': 'default', 'mastermismatch': '0', 'mtu': '0'},
 {'instance': '1', 'mastermismatch': '0', 'mtu': '0'}]

GET_MTU_MASTER_MISMATCH_ERRORS_PAIR_1 = (MTU_MASTER_MISMATCH_ERRORS_1, False)

MTU_MASTER_MISMATCH_ERRORS_2 = [{'instance': 'default', 'mastermismatch': '0', 'mtu': '0'},
 {'instance': '1', 'mastermismatch': '0', 'mtu': '0'},
 {'instance': 'default', 'mastermismatch': '0', 'mtu': '0'},
 {'instance': '1', 'mastermismatch': '0', 'mtu': '100'}]

GET_MTU_MASTER_MISMATCH_ERRORS_PAIR_2 = (MTU_MASTER_MISMATCH_ERRORS_2, True)


# From argument ospf_neighbor_status

OSPF_NEIGHBOR_LOADING_1 = 'LOADING'

GET_OSPF_NEIGHBOR_LOADING_PAIR_1 = (OSPF_NEIGHBOR_LOADING_1, True)

OSPF_NEIGHBOR_LOADING_2 = 'ATTEMPT'

GET_OSPF_NEIGHBOR_LOADING_PAIR_2 = (OSPF_NEIGHBOR_LOADING_2, False)

# SSH command: clish -c "show ospf errors lsr"; clish -c "show ospf errors dd"; sleep 2; clish -c "show ospf errors lsr"; clish -c "show ospf errors dd"

PARSE_OSPF_LSU_MTU_ERROR_1 = """
---------------------------- OSPF Instance default -----------------------------

Link State Request Errors

    LSR Duplicate Router ID   0            BadSize                   0        
    BadState                  0            Empty Request             0        


------------------------------- OSPF Instance 1 --------------------------------

Link State Request Errors

    LSR Duplicate Router ID   0            BadSize                   0        
    BadState                  0            Empty Request             0        


---------------------------- OSPF Instance default -----------------------------

Database Description Errors

    ASEinStub                 0            Type7inNonNSSA            0        
    MTU                       0            BadLSType                 0        
    NotDuplicate              0            BadSize                   0        
    OptionsMismatch           0            DuplicateLSA              0        
    DD Duplicate Router ID    0            InitSet                   21       
    Runt                      0            MasterMismatch            0        
    SlaveSeq                  0            MasterSeq                 0        
    DD TooLow                 418      


------------------------------- OSPF Instance 1 --------------------------------

Database Description Errors

    ASEinStub                 0            Type7inNonNSSA            0        
    MTU                       0            BadLSType                 0        
    NotDuplicate              0            BadSize                   0        
    OptionsMismatch           0            DuplicateLSA              0        
    DD Duplicate Router ID    0            InitSet                   0        
    Runt                      0            MasterMismatch            0        
    SlaveSeq                  0            MasterSeq                 0        
    DD TooLow                 0        


---------------------------- OSPF Instance default -----------------------------

Link State Request Errors

    LSR Duplicate Router ID   0            BadSize                   0        
    BadState                  0            Empty Request             0        


------------------------------- OSPF Instance 1 --------------------------------

Link State Request Errors

    LSR Duplicate Router ID   0            BadSize                   0        
    BadState                  0            Empty Request             0        


---------------------------- OSPF Instance default -----------------------------

Database Description Errors

    ASEinStub                 0            Type7inNonNSSA            0        
    MTU                       0            BadLSType                 0        
    NotDuplicate              0            BadSize                   0        
    OptionsMismatch           0            DuplicateLSA              0        
    DD Duplicate Router ID    0            InitSet                   21       
    Runt                      0            MasterMismatch            0        
    SlaveSeq                  0            MasterSeq                 0        
    DD TooLow                 418      


------------------------------- OSPF Instance 1 --------------------------------

Database Description Errors

    ASEinStub                 0            Type7inNonNSSA            0        
    MTU                       0            BadLSType                 0        
    NotDuplicate              0            BadSize                   0        
    OptionsMismatch           0            DuplicateLSA              0        
    DD Duplicate Router ID    0            InitSet                   0        
    Runt                      0            MasterMismatch            0        
    SlaveSeq                  0            MasterSeq                 0        
    DD TooLow                 0        

"""

GET_PARSE_OSPF_LSU_MTU_ERROR_PAIR_1 = (PARSE_OSPF_LSU_MTU_ERROR_1, [{'bad_size': '0',
  'bad_state': '0',
  'duplicate_lsr_router_id': '0',
  'empty_request': '0',
  'instance': 'default',
  'mtu': ''},
 {'bad_size': '0',
  'bad_state': '0',
  'duplicate_lsr_router_id': '0',
  'empty_request': '0',
  'instance': '1',
  'mtu': ''},
 {'bad_size': '',
  'bad_state': '',
  'duplicate_lsr_router_id': '',
  'empty_request': '',
  'instance': 'default',
  'mtu': '0'},
 {'bad_size': '',
  'bad_state': '',
  'duplicate_lsr_router_id': '',
  'empty_request': '',
  'instance': '1',
  'mtu': '0'},
 {'bad_size': '0',
  'bad_state': '0',
  'duplicate_lsr_router_id': '0',
  'empty_request': '0',
  'instance': 'default',
  'mtu': ''},
 {'bad_size': '0',
  'bad_state': '0',
  'duplicate_lsr_router_id': '0',
  'empty_request': '0',
  'instance': '1',
  'mtu': ''},
 {'bad_size': '',
  'bad_state': '',
  'duplicate_lsr_router_id': '',
  'empty_request': '',
  'instance': 'default',
  'mtu': '0'},
 {'bad_size': '',
  'bad_state': '',
  'duplicate_lsr_router_id': '',
  'empty_request': '',
  'instance': '1',
  'mtu': '0'}])


PARSE_OSPF_LSU_MTU_ERROR_2 = """
---------------------------- OSPF Instance default -----------------------------

Link State Request Errors

    LSR Duplicate Router ID   0            BadSize                   0        
    BadState                  0            Empty Request             0        


------------------------------- OSPF Instance 1 --------------------------------

Link State Request Errors

    LSR Duplicate Router ID   0            BadSize                   0        
    BadState                  0            Empty Request             0        


---------------------------- OSPF Instance default -----------------------------

Database Description Errors

    ASEinStub                 0            Type7inNonNSSA            0        
    MTU                       0            BadLSType                 0        
    NotDuplicate              0            BadSize                   0        
    OptionsMismatch           0            DuplicateLSA              0        
    DD Duplicate Router ID    0            InitSet                   21       
    Runt                      0            MasterMismatch            0        
    SlaveSeq                  0            MasterSeq                 0        
    DD TooLow                 418      


------------------------------- OSPF Instance 1 --------------------------------

Database Description Errors

    ASEinStub                 0            Type7inNonNSSA            0        
    MTU                       0            BadLSType                 0        
    NotDuplicate              0            BadSize                   0        
    OptionsMismatch           0            DuplicateLSA              0        
    DD Duplicate Router ID    0            InitSet                   0        
    Runt                      0            MasterMismatch            0        
    SlaveSeq                  0            MasterSeq                 0        
    DD TooLow                 0        


---------------------------- OSPF Instance default -----------------------------

Link State Request Errors

    LSR Duplicate Router ID   0            BadSize                   10        
    BadState                  0            Empty Request             0        


------------------------------- OSPF Instance 1 --------------------------------

Link State Request Errors

    LSR Duplicate Router ID   0            BadSize                   0        
    BadState                  0            Empty Request             0        


---------------------------- OSPF Instance default -----------------------------

Database Description Errors

    ASEinStub                 0            Type7inNonNSSA            0        
    MTU                       0            BadLSType                 0        
    NotDuplicate              0            BadSize                   0        
    OptionsMismatch           0            DuplicateLSA              0        
    DD Duplicate Router ID    0            InitSet                   21       
    Runt                      0            MasterMismatch            0        
    SlaveSeq                  0            MasterSeq                 0        
    DD TooLow                 418      


------------------------------- OSPF Instance 1 --------------------------------

Database Description Errors

    ASEinStub                 0            Type7inNonNSSA            0        
    MTU                       0            BadLSType                 0        
    NotDuplicate              0            BadSize                   0        
    OptionsMismatch           0            DuplicateLSA              0        
    DD Duplicate Router ID    0            InitSet                   0        
    Runt                      0            MasterMismatch            0        
    SlaveSeq                  0            MasterSeq                 0        
    DD TooLow                 0        

"""

GET_PARSE_OSPF_LSU_MTU_ERROR_PAIR_2 = (PARSE_OSPF_LSU_MTU_ERROR_2, [{'bad_size': '0',
  'bad_state': '0',
  'duplicate_lsr_router_id': '0',
  'empty_request': '0',
  'instance': 'default',
  'mtu': ''},
 {'bad_size': '0',
  'bad_state': '0',
  'duplicate_lsr_router_id': '0',
  'empty_request': '0',
  'instance': '1',
  'mtu': ''},
 {'bad_size': '',
  'bad_state': '',
  'duplicate_lsr_router_id': '',
  'empty_request': '',
  'instance': 'default',
  'mtu': '0'},
 {'bad_size': '',
  'bad_state': '',
  'duplicate_lsr_router_id': '',
  'empty_request': '',
  'instance': '1',
  'mtu': '0'},
 {'bad_size': '10',
  'bad_state': '0',
  'duplicate_lsr_router_id': '0',
  'empty_request': '0',
  'instance': 'default',
  'mtu': ''},
 {'bad_size': '0',
  'bad_state': '0',
  'duplicate_lsr_router_id': '0',
  'empty_request': '0',
  'instance': '1',
  'mtu': ''},
 {'bad_size': '',
  'bad_state': '',
  'duplicate_lsr_router_id': '',
  'empty_request': '',
  'instance': 'default',
  'mtu': '0'},
 {'bad_size': '',
  'bad_state': '',
  'duplicate_lsr_router_id': '',
  'empty_request': '',
  'instance': '1',
  'mtu': '0'}])

# From argument list_ospf_lsr_and_mtu_errors

OSPF_NEIGHBOR_LSR_AND_MTU_ERRORS_1 = [{'bad_size': '0',
  'bad_state': '0',
  'duplicate_lsr_router_id': '0',
  'empty_request': '0',
  'instance': 'default',
  'mtu': ''},
                                      {'bad_size': '0',
  'bad_state': '0',
  'duplicate_lsr_router_id': '0',
  'empty_request': '0',
  'instance': '1',
  'mtu': ''},
                                      {'bad_size': '',
  'bad_state': '',
  'duplicate_lsr_router_id': '',
  'empty_request': '',
  'instance': 'default',
  'mtu': '0'},
                                      {'bad_size': '',
  'bad_state': '',
  'duplicate_lsr_router_id': '',
  'empty_request': '',
  'instance': '1',
  'mtu': '0'},
                                      {'bad_size': '0',
  'bad_state': '0',
  'duplicate_lsr_router_id': '0',
  'empty_request': '0',
  'instance': 'default',
  'mtu': ''},
                                      {'bad_size': '0',
  'bad_state': '0',
  'duplicate_lsr_router_id': '0',
  'empty_request': '0',
  'instance': '1',
  'mtu': ''},
                                      {'bad_size': '',
  'bad_state': '',
  'duplicate_lsr_router_id': '',
  'empty_request': '',
  'instance': 'default',
  'mtu': '0'},
                                      {'bad_size': '',
  'bad_state': '',
  'duplicate_lsr_router_id': '',
  'empty_request': '',
  'instance': '1',
  'mtu': '0'}]

GET_OSPF_NEIGHBOR_LSR_AND_MTU_ERRORS_PAIR_1 = (OSPF_NEIGHBOR_LSR_AND_MTU_ERRORS_1, False)

OSPF_NEIGHBOR_LSR_AND_MTU_ERRORS_2 = [{'bad_size': '0',
  'bad_state': '0',
  'duplicate_lsr_router_id': '0',
  'empty_request': '0',
  'instance': 'default',
  'mtu': ''},
 {'bad_size': '0',
  'bad_state': '0',
  'duplicate_lsr_router_id': '0',
  'empty_request': '0',
  'instance': '1',
  'mtu': ''},
 {'bad_size': '',
  'bad_state': '',
  'duplicate_lsr_router_id': '',
  'empty_request': '',
  'instance': 'default',
  'mtu': '0'},
 {'bad_size': '',
  'bad_state': '',
  'duplicate_lsr_router_id': '',
  'empty_request': '',
  'instance': '1',
  'mtu': '0'},
 {'bad_size': '10',
  'bad_state': '0',
  'duplicate_lsr_router_id': '0',
  'empty_request': '0',
  'instance': 'default',
  'mtu': ''},
 {'bad_size': '0',
  'bad_state': '0',
  'duplicate_lsr_router_id': '0',
  'empty_request': '0',
  'instance': '1',
  'mtu': ''},
 {'bad_size': '',
  'bad_state': '',
  'duplicate_lsr_router_id': '',
  'empty_request': '',
  'instance': 'default',
  'mtu': '0'},
 {'bad_size': '',
  'bad_state': '',
  'duplicate_lsr_router_id': '',
  'empty_request': '',
  'instance': '1',
  'mtu': '0'}]

GET_OSPF_NEIGHBOR_LSR_AND_MTU_ERRORS_PAIR_2 = (OSPF_NEIGHBOR_LSR_AND_MTU_ERRORS_2, True)


# From argument ospf_neighbor_status

OSPF_NEIGHBOR_INIT_ATTEMPT_DOWN_1 = 'INIT'

GET_OSPF_NEIGHBOR_INIT_ATTEMPT_DOWN_PAIR_1 = (OSPF_NEIGHBOR_INIT_ATTEMPT_DOWN_1, True)

OSPF_NEIGHBOR_INIT_ATTEMPT_DOWN_2 = 'ATTEMPT'

GET_OSPF_NEIGHBOR_INIT_ATTEMPT_DOWN_PAIR_2 = (OSPF_NEIGHBOR_INIT_ATTEMPT_DOWN_2, True)

OSPF_NEIGHBOR_INIT_ATTEMPT_DOWN_3 = 'DOWN'

GET_OSPF_NEIGHBOR_INIT_ATTEMPT_DOWN_PAIR_3 = (OSPF_NEIGHBOR_INIT_ATTEMPT_DOWN_3, True)

# SSH command: clish -c "show ospf errors hello"; sleep 2; clish -c "show ospf errors hello"

PARSE_OSPF_NEIGHBOR_HELLO_ERRORS_1 = """
---------------------------- OSPF Instance default -----------------------------

Hello Protocol Errors

    Bad Size                  0            Network Mask Mismatch     0
    Dead Interval Mismatch    0            Hello Duplicate Router ID 47932
    External Option Mismatch  0            NSSA Option Mismatch      0
    Runt                      0            Hello Timer Mismatch      0


------------------------------- OSPF Instance 1 --------------------------------

Hello Protocol Errors

    Bad Size                  0            Network Mask Mismatch     0
    Dead Interval Mismatch    0            Hello Duplicate Router ID 0
    External Option Mismatch  0            NSSA Option Mismatch      0
    Runt                      0            Hello Timer Mismatch      0


---------------------------- OSPF Instance default -----------------------------

Hello Protocol Errors

    Bad Size                  0            Network Mask Mismatch     0
    Dead Interval Mismatch    0            Hello Duplicate Router ID 47932
    External Option Mismatch  0            NSSA Option Mismatch      0
    Runt                      0            Hello Timer Mismatch      0


------------------------------- OSPF Instance 1 --------------------------------

Hello Protocol Errors

    Bad Size                  0            Network Mask Mismatch     0
    Dead Interval Mismatch    0            Hello Duplicate Router ID 0
    External Option Mismatch  0            NSSA Option Mismatch      0
    Runt                      0            Hello Timer Mismatch      0
"""

GET_PARSE_OSPF_NEIGHBOR_HELLO_ERRORS_PAIR_1 = (PARSE_OSPF_NEIGHBOR_HELLO_ERRORS_1, [{'bad_size': '0',
  'dead_interval_mismatch': '0',
  'duplicate_router_id': '47932',
  'external_option_mismatch': '0',
  'hello_timer_mismatch': '0',
  'instance': 'default',
  'network_mask_mismatch': '0',
  'nssa_option_mismatch': '0',
  'runt': '0'},
                                                                                    {'bad_size': '0',
  'dead_interval_mismatch': '0',
  'duplicate_router_id': '0',
  'external_option_mismatch': '0',
  'hello_timer_mismatch': '0',
  'instance': '1',
  'network_mask_mismatch': '0',
  'nssa_option_mismatch': '0',
  'runt': '0'},
                                                                                    {'bad_size': '0',
  'dead_interval_mismatch': '0',
  'duplicate_router_id': '47932',
  'external_option_mismatch': '0',
  'hello_timer_mismatch': '0',
  'instance': 'default',
  'network_mask_mismatch': '0',
  'nssa_option_mismatch': '0',
  'runt': '0'},
                                                                                    {'bad_size': '0',
  'dead_interval_mismatch': '0',
  'duplicate_router_id': '0',
  'external_option_mismatch': '0',
  'hello_timer_mismatch': '0',
  'instance': '1',
  'network_mask_mismatch': '0',
  'nssa_option_mismatch': '0',
  'runt': '0'}])

PARSE_OSPF_NEIGHBOR_HELLO_ERRORS_2 = """
---------------------------- OSPF Instance default -----------------------------

Hello Protocol Errors

    Bad Size                  0            Network Mask Mismatch     0
    Dead Interval Mismatch    0            Hello Duplicate Router ID 47932
    External Option Mismatch  0            NSSA Option Mismatch      0
    Runt                      0            Hello Timer Mismatch      0


------------------------------- OSPF Instance 1 --------------------------------

Hello Protocol Errors

    Bad Size                  0            Network Mask Mismatch     0
    Dead Interval Mismatch    0            Hello Duplicate Router ID 0
    External Option Mismatch  0            NSSA Option Mismatch      0
    Runt                      0            Hello Timer Mismatch      0


---------------------------- OSPF Instance default -----------------------------

Hello Protocol Errors

    Bad Size                  0            Network Mask Mismatch     0
    Dead Interval Mismatch    0            Hello Duplicate Router ID 47932
    External Option Mismatch  0            NSSA Option Mismatch      0
    Runt                      0            Hello Timer Mismatch      0


------------------------------- OSPF Instance 1 --------------------------------

Hello Protocol Errors

    Bad Size                  0            Network Mask Mismatch     0
    Dead Interval Mismatch    0            Hello Duplicate Router ID 0
    External Option Mismatch  0            NSSA Option Mismatch      0
    Runt                      1000            Hello Timer Mismatch      0

"""

GET_PARSE_OSPF_NEIGHBOR_HELLO_ERRORS_PAIR_2 = (PARSE_OSPF_NEIGHBOR_HELLO_ERRORS_2, [{'bad_size': '0',
  'dead_interval_mismatch': '0',
  'duplicate_router_id': '47932',
  'external_option_mismatch': '0',
  'hello_timer_mismatch': '0',
  'instance': 'default',
  'network_mask_mismatch': '0',
  'nssa_option_mismatch': '0',
  'runt': '0'},
                                                                                    {'bad_size': '0',
  'dead_interval_mismatch': '0',
  'duplicate_router_id': '0',
  'external_option_mismatch': '0',
  'hello_timer_mismatch': '0',
  'instance': '1',
  'network_mask_mismatch': '0',
  'nssa_option_mismatch': '0',
  'runt': '0'},
                                                                                    {'bad_size': '0',
  'dead_interval_mismatch': '0',
  'duplicate_router_id': '47932',
  'external_option_mismatch': '0',
  'hello_timer_mismatch': '0',
  'instance': 'default',
  'network_mask_mismatch': '0',
  'nssa_option_mismatch': '0',
  'runt': '0'},
                                                                                    {'bad_size': '0',
  'dead_interval_mismatch': '0',
  'duplicate_router_id': '0',
  'external_option_mismatch': '0',
  'hello_timer_mismatch': '0',
  'instance': '1',
  'network_mask_mismatch': '0',
  'nssa_option_mismatch': '0',
  'runt': '1000'}])

# From argument ist_ospf_hello_errors

OSPF_NEIGHBOR_HELLO_ERRORS_1 = [{'bad_size': '0',
  'dead_interval_mismatch': '0',
  'duplicate_router_id': '47932',
  'external_option_mismatch': '0',
  'hello_timer_mismatch': '0',
  'instance': 'default',
  'network_mask_mismatch': '0',
  'nssa_option_mismatch': '0',
  'runt': '0'},
                                                                                    {'bad_size': '0',
  'dead_interval_mismatch': '0',
  'duplicate_router_id': '0',
  'external_option_mismatch': '0',
  'hello_timer_mismatch': '0',
  'instance': '1',
  'network_mask_mismatch': '0',
  'nssa_option_mismatch': '0',
  'runt': '0'},
                                                                                    {'bad_size': '0',
  'dead_interval_mismatch': '0',
  'duplicate_router_id': '47932',
  'external_option_mismatch': '0',
  'hello_timer_mismatch': '0',
  'instance': 'default',
  'network_mask_mismatch': '0',
  'nssa_option_mismatch': '0',
  'runt': '0'},
                                                                                    {'bad_size': '0',
  'dead_interval_mismatch': '0',
  'duplicate_router_id': '0',
  'external_option_mismatch': '0',
  'hello_timer_mismatch': '0',
  'instance': '1',
  'network_mask_mismatch': '0',
  'nssa_option_mismatch': '0',
  'runt': '0'}]

GET_OSPF_NEIGHBOR_HELLO_ERRORS_PAIR_1 = (OSPF_NEIGHBOR_HELLO_ERRORS_1, False)

OSPF_NEIGHBOR_HELLO_ERRORS_2 = [{'bad_size': '0',
  'dead_interval_mismatch': '0',
  'duplicate_router_id': '47932',
  'external_option_mismatch': '0',
  'hello_timer_mismatch': '0',
  'instance': 'default',
  'network_mask_mismatch': '0',
  'nssa_option_mismatch': '0',
  'runt': '0'},
                                                                                    {'bad_size': '0',
  'dead_interval_mismatch': '0',
  'duplicate_router_id': '0',
  'external_option_mismatch': '0',
  'hello_timer_mismatch': '0',
  'instance': '1',
  'network_mask_mismatch': '0',
  'nssa_option_mismatch': '0',
  'runt': '0'},
                                                                                    {'bad_size': '0',
  'dead_interval_mismatch': '0',
  'duplicate_router_id': '47932',
  'external_option_mismatch': '0',
  'hello_timer_mismatch': '0',
  'instance': 'default',
  'network_mask_mismatch': '0',
  'nssa_option_mismatch': '0',
  'runt': '0'},
                                                                                    {'bad_size': '0',
  'dead_interval_mismatch': '0',
  'duplicate_router_id': '0',
  'external_option_mismatch': '0',
  'hello_timer_mismatch': '0',
  'instance': '1',
  'network_mask_mismatch': '0',
  'nssa_option_mismatch': '0',
  'runt': '1000'}]

GET_OSPF_NEIGHBOR_HELLO_ERRORS_PAIR_2 =(OSPF_NEIGHBOR_HELLO_ERRORS_2, True)

# From argument

OSPF_NEIGHBOR_STATE_NOT_KNOWN_1 = """
RTGRTG0019  Routing daemon is busy. Please try again later.
"""
GET_OSPF_NEIGHBOR_STATE_NOT_KNOWN_PAIR_1 = (OSPF_NEIGHBOR_STATE_NOT_KNOWN_1, True)

OSPF_NEIGHBOR_STATE_NOT_KNOWN_2 = """
---------------------------- OSPF Instance default -----------------------------

No neighbors found

------------------------------- OSPF Instance 1 --------------------------------

No neighbors found

"""

GET_OSPF_NEIGHBOR_STATE_NOT_KNOWN_PAIR_2 = (OSPF_NEIGHBOR_STATE_NOT_KNOWN_2, True)

#SSH command: ps aux | grep -i routed

PARSE_ROUTED_STATUS_1 = """
admin    21120  0.1  0.2  51468  8708 ?        S<sl 04:39   0:00 /bin/routed -N
admin    21132  0.0  0.3  73300 12260 ?        S<l  04:39   0:00 /bin/routed -i default -f /etc/routed0.conf -h 0
admin    28291  0.0  0.0   1740   588 pts/9    S+   04:46   0:00 grep -i routed
"""

GET_PARSE_ROUTED_STATUS_PAIR_1 = (PARSE_ROUTED_STATUS_1, [{'routed_status': '/bin/routed'}, {'routed_status': '/bin/routed'}])

PARSE_ROUTED_STATUS_2 = """
admin    28828  0.0  0.0   1744   564 pts/9    S+   04:47   0:00 grep -i routed
"""

GET_PARSE_ROUTED_STATUS_PAIR_2 = (PARSE_ROUTED_STATUS_2, False)

# From argument routed_state_parsed

ROUTED_STATUS_1 = [{'routed_status': '/bin/routed'}, {'routed_status': '/bin/routed'}]


GET_ROUTED_STATUS_PAIR_1 = (ROUTED_STATUS_1, True)

ROUTED_STATUS_2 = False

GET_ROUTED_STATUS_PAIR_2 = (ROUTED_STATUS_2, False)