from parser_service.public import helper_methods


def check_variant_matches(**kwargs) -> bool:
    # This should work on all devices with one of the below versions
    tags = kwargs.get('my_device_tags', None)
    if tags is None:
        return False
    versions = [x.lower() for x in ['R80.20', 'R80.30', 'R80.20SP', 'R80.40']]
    if tags.get('os.version', '').lower() not in versions:
        return False
    return True


def get_ospf_neighbor(ospf_neighbor_entry: str) -> list:
    if ospf_neighbor_entry:
        data = helper_methods.parse_data_as_object(ospf_neighbor_entry,
                                                   'checkpoint_gaia_ospf_neighbor_from_issue_item.textfsm')
        if data:
            ospf_neighbor = data['ospf_neighbor']
            return ospf_neighbor
    return False


def parse_ospf_neighbor_status(raw_data: str, ospf_neighbor: str) -> str:
    if raw_data and ospf_neighbor:
        data = helper_methods.parse_data_as_object(raw_data, 'checkpoint_gaia_get_ospf_neighbor.textfsm')
        if data:
            ospf_neighbor_status = data['state']
            return ospf_neighbor_status
    return False


def get_ospf_neighbor_full_or_2way(ospf_neighbor_status: str) -> bool:
    if ospf_neighbor_status:
        if ospf_neighbor_status in ['FULL', '2WAY']:
            return True
    return False


def get_neighbor_status_exchange_or_exstart(ospf_neighbor_status: str) -> bool:
    if ospf_neighbor_status:
        if ospf_neighbor_status in ['EXCHANGE', 'EXSTART']:
            return True
    return False


def parse_ospf_dd_errors(raw_data: str) -> list:
    if raw_data:
        list_ospf_instance_mtu_and_master_mismatch = helper_methods.parse_data_as_list(raw_data,
                                                                                       'checkpoint_gaia_show_ospf_errors_dd.textfsm')
        if list_ospf_instance_mtu_and_master_mismatch:
            return list_ospf_instance_mtu_and_master_mismatch

    return False


def get_mtu_master_mismatch_errors(list_ospf_instance_mtu_and_master_mismatch: list) -> bool:
    if list_ospf_instance_mtu_and_master_mismatch:
        i = 0
        j = int(len(list_ospf_instance_mtu_and_master_mismatch) / 2)
        for id in range(0, int(len(list_ospf_instance_mtu_and_master_mismatch) / 2)):
            if list_ospf_instance_mtu_and_master_mismatch[i] != list_ospf_instance_mtu_and_master_mismatch[j]:
                return True
            i += 1
            j += 1
    return False


def get_neighbor_status_loading(ospf_neighbor_status: str) -> bool:
    if ospf_neighbor_status:
        if ospf_neighbor_status in ['LOADING']:
            return True
    return False


def parse_ospf_lsr_mtu_error(raw_data: str) -> list:
    if raw_data:
        list_ospf_lsr_and_mtu_errors = helper_methods.parse_data_as_list(raw_data,
                                                                         'checkpoint_gaia_get_ospf_errors_lsr_mtu.textfsm')
        if list_ospf_lsr_and_mtu_errors:
            return list_ospf_lsr_and_mtu_errors
    return False


def get_lsr_mtu_error(list_ospf_lsr_and_mtu_errors: list) -> bool:
    if list_ospf_lsr_and_mtu_errors:
        i = 0
        j = int(len(list_ospf_lsr_and_mtu_errors) / 2)
        for id in range(0, int(len(list_ospf_lsr_and_mtu_errors) / 2)):
            if list_ospf_lsr_and_mtu_errors[i] != list_ospf_lsr_and_mtu_errors[j]:
                return True
            i += 1
            j += 1
    return False


def get_neighbor_status_init_attempt_or_down(ospf_neighbor_status: str) -> bool:
    if ospf_neighbor_status:
        if ospf_neighbor_status in ['INIT', 'ATTEMPT', 'DOWN']:
            return True
    return False


def parse_ospf_hello_errors(raw_data: str) -> list:
    if raw_data:
        list_ospf_hello_errors = helper_methods.parse_data_as_list(raw_data,
                                                                   'checkpoint_gaia_get_ospf_errors_hello.textfsm')
        if list_ospf_hello_errors:
            return list_ospf_hello_errors
    return False


def get_hello_error(list_ospf_hello_errors: list) -> bool:
    if list_ospf_hello_errors:
        i = 0
        j = int(len(list_ospf_hello_errors) / 2)
        for id in range(0, int(len(list_ospf_hello_errors) / 2)):
            if list_ospf_hello_errors[i] != list_ospf_hello_errors[j]:
                return True
            i += 1
            j += 1
    return False


def get_neighbor_status_not_known(ospf_neighbor_status: str) -> bool:
    if ospf_neighbor_status:
        if ospf_neighbor_status not in ['INIT', 'ATTEMPT', 'DOWN', 'LOADING', 'EXCHANGE', 'EXSTART', 'FULL', '2WAY']:
            return True
    return False


def parse_routed_status(raw_data: str) -> list:
    if raw_data:
        routed_state_parsed = helper_methods.parse_data_as_list(raw_data, 'checkpoint_gaia_get_routed_status.textfsm')
        if routed_state_parsed:
            return routed_state_parsed
    return False


def get_routed_status(routed_state_parsed: list) -> bool:
    if routed_state_parsed:
        for state in routed_state_parsed:
            if state['routed_status'] == '/bin/routed':
                return True
    return False
