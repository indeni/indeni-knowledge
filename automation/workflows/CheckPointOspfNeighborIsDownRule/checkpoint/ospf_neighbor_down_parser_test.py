import unittest
import automation.workflows.CheckPointOspfNeighborIsDownRule.checkpoint.ospf_neighbor_down_parser as parser
import automation.workflows.CheckPointOspfNeighborIsDownRule.checkpoint.ospf_neighbor_down_mock_data as mock

class OSPFNeignborDownTests(unittest.TestCase):

    def test_get_ospf_neighbor(self):
        pair1 = mock.GET_ISSUE_ITEMS_PAIR1
        pair2 = mock.GET_ISSUE_ITEMS_PAIR_2
        self.assertEqual(pair1[1], parser.get_ospf_neighbor(pair1[0]))
        self.assertEqual(pair2[1], parser.get_ospf_neighbor(pair2[0]))

    def test_parse_ospf_neighbor_status(self):
        pair1 = mock.GET_SHOW_OSPF_NEIGHBOR_PAIR_1
        pair2 = mock.GET_SHOW_OSPF_NEIGHBOR_PAIR_2
        self.assertEqual(pair1[1], parser.parse_ospf_neighbor_status(pair1[0][0], pair1[0][1]))
        self.assertEqual(pair2[1], parser.parse_ospf_neighbor_status(pair2[0][0], pair2[0][1]))

    def test_get_ospf_neighbor_full_or_2way(self):
        pair1 = mock.GET_OSPF_NEIGHBOR_FULL_OR_2WAY_PAIR_1
        pair2 = mock.GET_OSPF_NEIGHBOR_FULL_OR_2WAY_PAIR_2
        self.assertEqual(pair1[1], parser.get_ospf_neighbor_full_or_2way(pair1[0]))
        self.assertEqual(pair2[1], parser.get_ospf_neighbor_full_or_2way(pair2[0]))

    def test_get_neighbor_status_exchange_or_exstart(self):
        pair1 = mock.GET_OSPF_NEIGHBOR_EXCHANGE_OR_EXSTART_PAIR_1
        pair2 = mock.GET_OSPF_NEIGHBOR_EXCHANGE_OR_EXSTART_PAIR_2
        self.assertEqual(pair1[1], parser.get_neighbor_status_exchange_or_exstart(pair1[0]))
        self.assertEqual(pair2[1], parser.get_neighbor_status_exchange_or_exstart(pair2[0]))

    def test_parse_ospf_dd_errors(self):
        pair1 = mock.GET_PARSE_OSPF_DD_ERRORS_PAIR_1
        pair2 = mock.GET_PARSE_OSPF_DD_ERRORS_PAIR_2
        self.assertEqual(pair1[1], parser.parse_ospf_dd_errors(pair1[0]))
        self.assertEqual(pair2[1], parser.parse_ospf_dd_errors(pair2[0]))

    def test_get_mtu_master_mismatch_errors(self):
        pair1 = mock.GET_MTU_MASTER_MISMATCH_ERRORS_PAIR_1
        pair2 = mock.GET_MTU_MASTER_MISMATCH_ERRORS_PAIR_2
        self.assertEqual(pair1[1], parser.get_mtu_master_mismatch_errors(pair1[0]))
        self.assertEqual(pair2[1], parser.get_mtu_master_mismatch_errors(pair2[0]))

    def test_get_neighbor_status_loading(self):
        pair1 = mock.GET_OSPF_NEIGHBOR_LOADING_PAIR_1
        pair2 = mock.GET_OSPF_NEIGHBOR_LOADING_PAIR_2
        self.assertEqual(pair1[1],parser.get_neighbor_status_loading(pair1[0]))
        self.assertEqual(pair2[1], parser.get_neighbor_status_loading(pair2[0]))

    def test_parse_ospf_lsr_mtu_error(self):
        pair1 = mock.GET_PARSE_OSPF_LSU_MTU_ERROR_PAIR_1
        pair2 = mock.GET_PARSE_OSPF_LSU_MTU_ERROR_PAIR_2
        self.assertEqual(pair1[1], parser.parse_ospf_lsr_mtu_error(pair1[0]))
        self.assertEqual(pair2[1], parser.parse_ospf_lsr_mtu_error(pair2[0]))

    def test_get_lsr_and_mtu_error(self):
        pair1 = mock.GET_OSPF_NEIGHBOR_LSR_AND_MTU_ERRORS_PAIR_1
        pair2 = mock.GET_OSPF_NEIGHBOR_LSR_AND_MTU_ERRORS_PAIR_2
        self.assertEqual(pair1[1], parser.get_lsr_mtu_error(pair1[0]))
        self.assertEqual(pair2[1], parser.get_lsr_mtu_error(pair2[0]))

    def test_get_neighbor_status_init_attempt_or_down(self):
        pair1 = mock.GET_OSPF_NEIGHBOR_INIT_ATTEMPT_DOWN_PAIR_1
        pair2 = mock.GET_OSPF_NEIGHBOR_INIT_ATTEMPT_DOWN_PAIR_2
        pair3 = mock.GET_OSPF_NEIGHBOR_INIT_ATTEMPT_DOWN_PAIR_3
        self.assertEqual(pair1[1], parser.get_neighbor_status_init_attempt_or_down(pair1[0]))
        self.assertEqual(pair2[1], parser.get_neighbor_status_init_attempt_or_down(pair2[0]))
        self.assertEqual(pair3[1], parser.get_neighbor_status_init_attempt_or_down(pair3[0]))

    def test_parse_ospf_hello_errors(self):
        pair1 = mock.GET_PARSE_OSPF_NEIGHBOR_HELLO_ERRORS_PAIR_1
        pair2 = mock.GET_PARSE_OSPF_NEIGHBOR_HELLO_ERRORS_PAIR_2
        self.assertEqual(pair1[1], parser.parse_ospf_hello_errors(pair1[0]))
        self.assertEqual(pair2[1], parser.parse_ospf_hello_errors(pair2[0]))

    def test_get_hello_errors(self):
        pair1 = mock.GET_OSPF_NEIGHBOR_HELLO_ERRORS_PAIR_1
        pair2 = mock.GET_OSPF_NEIGHBOR_HELLO_ERRORS_PAIR_2
        self.assertEqual(pair1[1], parser.get_hello_error(pair1[0]))
        self.assertEqual(pair2[1], parser.get_hello_error(pair2[0]))

    def test_get_neighbor_status_not_known(self):
        pair1 = mock.GET_OSPF_NEIGHBOR_STATE_NOT_KNOWN_PAIR_1
        pair2 = mock.GET_OSPF_NEIGHBOR_STATE_NOT_KNOWN_PAIR_2
        self.assertEqual(pair1[1], parser.get_neighbor_status_not_known(pair1[0]))
        self.assertEqual(pair2[1], parser.get_neighbor_status_not_known(pair2[0]))

    def test_parse_routed_status(self):
        pair1 = mock.GET_PARSE_ROUTED_STATUS_PAIR_1
        pair2 = mock.GET_PARSE_ROUTED_STATUS_PAIR_2
        self.assertEqual(pair1[1], parser.parse_routed_status(pair1[0]))
        self.assertEqual(pair2[1], parser.parse_routed_status(pair2[0]))

    def test_get_routed_status(self):
        pair1 = mock.GET_ROUTED_STATUS_PAIR_1
        pair2 = mock.GET_ROUTED_STATUS_PAIR_2
        self.assertEqual(pair1[1], parser.get_routed_status(pair1[0]))
        self.assertEqual(pair2[1], parser.get_routed_status(pair2[0]))



if __name__ == '__main__':
    unittest.main()