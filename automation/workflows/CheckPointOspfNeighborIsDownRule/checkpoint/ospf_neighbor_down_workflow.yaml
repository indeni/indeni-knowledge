id: ospf_neighbor_down_checkpoint
friendly_name: OSPF neighbor(s) down
start_block: get_device_tags

blocks:
  get_device_tags:
    type: device_tags
    name: Get device tags
    register_to: my_device_tags
    go_to: make_variant_matches

  make_variant_matches:
    type: logic
    name: Match device against variant
    args: [my_device_tags]
    method: check_variant_matches
    register_to: bool_variant_matches
    go_to: check_if_variant_matches

  check_if_variant_matches:
    type: if
    name: Check if device matches requirements
    condition: bool_variant_matches
    then_go_to: get_issue_items
    else_go_to: variant_conclusion

  variant_conclusion:
    type: conclusion
    name: Incompatible workflow
    triage_conclusion: |
      This workflow is incompatible with the device.
    triage_remediation_steps: |
      Contact Indeni and request that compatibility be added

  get_issue_items:
    type: issue_items
    name: Get issue items
    register_to: my_items
    go_to: start_loop

  start_loop:
    type: foreach
    name: Start loop
    register_item_to: ospf_neighbor_entry
    start_block: get_ospf_neighbor
    blocks:

      get_ospf_neighbor:
        type: logic
        name:  Get OSPF neighbor
        method: get_ospf_neighbor
        args: [ospf_neighbor_entry]
        register_to: ospf_neighbor
        go_to: get_current_status_ospf_neighbor

      get_current_status_ospf_neighbor:
        type: device_task
        name: Get OSPF neighbor status
        runner:
          type: SSH
          command: clish -c "show ospf neighbor {{ospf_neighbor}}"
        parser:
          method: parse_ospf_neighbor_status
          args: [ospf_neighbor]
        register_to: ospf_neighbor_status
        go_to: verify_ospf_neighbor_status_full_or_2way

      verify_ospf_neighbor_status_full_or_2way:
        type: logic
        name:  Verify OSPF neighbor state is 'FULL' or '2WAY'
        method: get_ospf_neighbor_full_or_2way
        args: [ospf_neighbor_status]
        register_to: neighbor_status_full_or_2way
        go_to: check_if_ospf_neighbor_full_or_2way

      check_if_ospf_neighbor_full_or_2way:
        type: if
        name: Check if OSPF neighbor state is 'FULL' or '2WAY'
        condition: neighbor_status_full_or_2way
        then_go_to: conclusion_ospf_neighbor_stable_now
        else_go_to: evaluate_neighbor_status_exchange_or_exstart

      conclusion_ospf_neighbor_stable_now:
        type: conclusion
        name: OSPF neighbor stable now
        triage_conclusion: |
         'FULL' or '2WAY' are stable OPSF states
        triage_remediation_steps: |
          No action is required as the issue is not present anymore

      evaluate_neighbor_status_exchange_or_exstart:
        type: logic
        name:  Verify OSPF neighbor state is 'EXCHANGE' or 'EXSTART'
        method: get_neighbor_status_exchange_or_exstart
        args: [ospf_neighbor_status]
        register_to: neighbor_status_exchange_or_exstart
        go_to: check_if_ospf_neighbor_exchange_or_exstart

      check_if_ospf_neighbor_exchange_or_exstart:
        type: if
        name: Check if OSPF neighbor state is'EXCHANGE' or 'EXSTART'
        condition: neighbor_status_exchange_or_exstart
        then_go_to: get_dd_errors
        else_go_to: evaluate_neighbor_status_loading

      get_dd_errors:
        type: device_task
        name: Get OSPF Database Descriptor errors
        runner:
          type: SSH
          command: clish -c "show ospf errors dd"; sleep 2; clish -c "show ospf errors dd"
        parser:
          method: parse_ospf_dd_errors
          args: []
        register_to: list_ospf_instance_mtu_and_master_mismatch
        go_to: evaluate_mtu_and_master_mismatch

      evaluate_mtu_and_master_mismatch:
        type: logic
        name:  Verify if the OSPF database descriptor errors increase
        method: get_mtu_master_mismatch_errors
        args: [list_ospf_instance_mtu_and_master_mismatch]
        register_to: ospf_mtu_and_master_mismatch_errors_increase
        go_to: check_if_ospf_mtu_and_master_mismatch_increase

      check_if_ospf_mtu_and_master_mismatch_increase:
        type: if
        name: Check if OSPF database descriptor error increase
        condition: ospf_mtu_and_master_mismatch_errors_increase
        then_go_to: conclusion_mtu_master_mismatch_errors_increased_identified
        else_go_to: conclusion_additional_exstart_exchange_investigation_needed

      conclusion_mtu_master_mismatch_errors_increased_identified:
        type: conclusion
        name: Increase in MTU and MasterMismatch errors identified
        triage_conclusion: |
          Increase in MTU or Master mismatch errors identified
        triage_remediation_steps: |
          Increase in the MTU or Master mismatch errors are known reasons for OSPF
          neighbor to be stuck in EXCHANGE or EXSTART state.
          MTU size issue can be fixed by matching interface MTU for both the neighbors.
          Try to assign a higher router-id manualy to one of the routers to avoid conflicts
          in master slave selections

      conclusion_additional_exstart_exchange_investigation_needed:
        type: conclusion
        name: Additional EXSTART/EXCHANGE state investigation needed
        triage_conclusion: |
          Other factors need to be evaluated for the current neighbor state
        triage_remediation_steps: |
          Please open a Check Point support case for further investigation of the issue with information
          mentioned in the sk84520
          https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk84520&partition=Advanced&product=Security

      evaluate_neighbor_status_loading:
        type: logic
        name:  Verify OSPF neighbor state is 'LOADING'
        method: get_neighbor_status_loading
        args: [ospf_neighbor_status]
        register_to: ospf_neighbor_status_loading
        go_to: check_if_ospf_neighbor_loading

      check_if_ospf_neighbor_loading:
        type: if
        name: Check if OSPF neighbor state is "LOADING"
        condition: ospf_neighbor_status_loading
        then_go_to: get_lsr_mtu_errors
        else_go_to: evaluate_neighbor_status_init_attempt_or_down

      get_lsr_mtu_errors:
        type: device_task
        name: Get OSPF LSR and MTU errors
        runner:
          type: SSH
          command: clish -c "show ospf errors lsr"; clish -c "show ospf errors dd"; sleep 2; clish -c "show ospf errors lsr"; clish -c "show ospf errors dd"
        parser:
          method: parse_ospf_lsr_mtu_error
          args: []
        register_to: list_ospf_lsr_and_mtu_errors
        go_to: evaluate_lsr_and_mtu_errors

      evaluate_lsr_and_mtu_errors:
        type: logic
        name:  Verify if OSPF LSR and MTU errors increase
        method: get_lsr_mtu_error
        args: [list_ospf_lsr_and_mtu_errors]
        register_to: ospf_mtu_and_master_mismatch_errors_increase
        go_to: check_if_ospf_lsr_error

      check_if_ospf_lsr_error:
        type: if
        name: Check if OSPF LSR and MTU errors increase
        condition: ospf_mtu_and_master_mismatch_errors_increase
        then_go_to: conclusion_ospf_lsr_and_mtu_errors_increase
        else_go_to: conclusion_additional_loading_investigation_needed

      conclusion_ospf_lsr_and_mtu_errors_increase:
        type: conclusion
        name: Increase in LSR or MTU errors identified
        triage_conclusion: |
          LSR errors or MTU size are known issues for this OSPF state
        triage_remediation_steps: |
          LSR errors are caused due to any of the following reasons:
          1. A device between the neighbors, such as a switch, is corrupting the packet.
          2. Either the sending router’s interface is bad or the error is caused by a software bug.
          3. The receiving router is calculating the wrong checksum. This could indicate receiving router’s interface is
          bad or the error is caused by a software bug.
          Most of the time, this problem is fixed by replacing hardware.

          MTU size issue can be fixed by matching interface MTU for both the neighbors

      conclusion_additional_loading_investigation_needed:
        type: conclusion
        name: Additional LOADING state investigation needed
        triage_conclusion: |
          Other factors need to be evaluated for the current neighbor state
        triage_remediation_steps: |
          Please open a Check Point support case for further investigation of the issue with information
          mentioned in the sk84520
          https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk84520&partition=Advanced&product=Security

      evaluate_neighbor_status_init_attempt_or_down:
        type: logic
        name:  Verify OSPF neighbor state is 'INIT', 'ATTEMPT' or 'DOWN'
        method: get_neighbor_status_init_attempt_or_down
        args: [ospf_neighbor_status]
        register_to: ospf_neighbor_status_init_attempt_or_down
        go_to: check_if_ospf_neighbor_init_or_attempt_down

      check_if_ospf_neighbor_init_or_attempt_down:
        type: if
        name: Check OSPF neighbor state is 'INIT', 'ATTEMPT' or 'DOWN'
        condition: ospf_neighbor_status_init_attempt_or_down
        then_go_to: get_hello_errors
        else_go_to: evaluate_neighbor_status_not_identified

      get_hello_errors:
        type: device_task
        name: Get OSPF 'HELLO' errors
        runner:
          type: SSH
          command: clish -c "show ospf errors hello"; sleep 2; clish -c "show ospf errors hello"
        parser:
          method: parse_ospf_hello_errors
          args: []
        register_to: list_ospf_hello_errors
        go_to: evaluate_hello_errors

      evaluate_hello_errors:
        type: logic
        name:  Verify if OSPF 'HELLO' errors increase
        method: get_hello_error
        args: [list_ospf_hello_errors]
        register_to: ospf_hello_errors_increase
        go_to: check_if_ospf_hello_error

      check_if_ospf_hello_error:
        type: if
        name: Check if OSPF 'HELLO' errors increase
        condition: ospf_hello_errors_increase
        then_go_to: conclusion_hello_errors_increase
        else_go_to: conclusion_additional_init_attempt_or_down_investigation_needed

      conclusion_hello_errors_increase:
        type: conclusion
        name: Increase in OSPF 'HELLO' errors identified
        triage_conclusion: |
          OSPF 'HELLO' are known issues for this OSPF state
        triage_remediation_steps: |
          HELLO errors are caused due to any of the following reasons:
          1 Misconfiguration in HELLO timers on both the routers
          2 Missing or misconfiguration in the authentication authentication
          3 Connectivity issue between the 2 routers causing loss of OSPF multicast and unicast packets

      conclusion_additional_init_attempt_or_down_investigation_needed:
        type: conclusion
        name: Additional 'INIT', 'ATTEMPT' or 'DOWN' investigation needed
        triage_conclusion: |
          Other factors need to be evaluated for the current neighbor state
        triage_remediation_steps: |
          Please open a Check Point support case for further investigation of the issue with information
          mentioned in the sk84520
          https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk84520&partition=Advanced&product=Security

      evaluate_neighbor_status_not_identified:
        type: logic
        name:  Verify OSPF neighbor state is not known
        method: get_neighbor_status_not_known
        args: [ospf_neighbor_status]
        register_to: ospf_neighbor_status_not_known
        go_to: get_routed_status

      get_routed_status:
        type: device_task
        name: Get routed status
        runner:
          type: SSH
          command: ps aux | grep -i routed
        parser:
          method: parse_routed_status
          args: []
        register_to: routed_state_parsed
        go_to: evaluate_routed_status

      evaluate_routed_status:
        type: logic
        name:  Verify routed state
        method: get_routed_status
        args: [routed_state_parsed]
        register_to: routed_state_on
        go_to: check_if_routed_state_on

      check_if_routed_state_on:
        type: if
        name: Check if routed state on
        condition: routed_state_on
        then_go_to: routed_state_off
        else_go_to: issue_not_identfied

      routed_state_off:
        type: conclusion
        name: Process routed detected off
        triage_conclusion: Routed state off can cause many unknown routing issues
        triage_remediation_steps: |
          Try turning on the routed process with the command
          #tellpm process:routed t
          If the routed continues to be 'off'  by itself open a Check Point support case
          to investigate the issue with 'routed' core dump files from /var/log/dump/usermode/ folder

      issue_not_identfied:
        type: conclusion
        name: Issue not identifed
        triage_conclusion: |
          Issue not identifed additional investigation would be needed
        triage_remediation_steps: |
          Please open a Check Point support case for further investigation of the issue with information
          mentioned in the sk84520
          https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk84520&partition=Advanced&product=Security






