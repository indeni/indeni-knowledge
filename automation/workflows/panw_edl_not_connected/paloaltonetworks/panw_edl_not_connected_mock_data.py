PANOS_XPATH_CONFIG_EXTERNAL_LIST_OUT_1 = ("""
<response status="success">
    <result>
        <external-list>
            <entry name="HTTP_test">
                <type>
                    <ip>
                        <recurring>
                            <five-minute/>
                        </recurring>
                        <url>https://panwdbl.appspot.com/lists/mdl.txt</url>
                    </ip>
                </type>
            </entry>
            <entry name="HTTPS_test">
                <type>
                    <ip>
                        <recurring>
                            <five-minute/>
                        </recurring>
                        <url>https://www.spamhaus.org/drop/edrop.txt</url>
                    </ip>
                </type>
            </entry>
            <entry name="Fake_EDL_type_domain">
                <type>
                    <domain>
                        <recurring>
                            <hourly/>
                        </recurring>
                        <url>http://the_fake_url_2.com</url>
                    </domain>
                </type>
            </entry>
        </external-list>
    </result>
</response>
""", [("HTTP_test", "https://panwdbl.appspot.com/lists/mdl.txt"), ("HTTPS_test", "https://www.spamhaus.org/drop/edrop.txt"), ("Fake_EDL_type_domain", "http://the_fake_url_2.com")])

PANOS_XPATH_CONFIG_EXTERNAL_LIST_OUT_2 = ("""
<response status="error">
    <msg>
        <line>No such node</line>
    </msg>
</response>
""", [])


PANOS_XPATH_CONFIG_PROXY_OUT_1 = ("""
<response status="success">
    <result>
        <secure-proxy-server>10.11.12.13</secure-proxy-server>
    </result>
</response>
""", ["10.11.12.13"])

PANOS_XPATH_CONFIG_PROXY_OUT_2 = ("""
<response status="error">
    <msg>
        <line>No such node</line>
    </msg>
</response>
""", [])

PANOS_XPATH_CONFIG_SERVICE_ROUTE_OUT_1 = ("""
<response status="success">
    <result>
        <source>
            <address>2.2.2.2</address>
            <interface>tunnel.2</interface>
        </source>
    </result>
</response>
""", [("2.2.2.2","tunnel.2")])

PANOS_XPATH_CONFIG_SERVICE_ROUTE_OUT_2 = ("""
<response status="error">
    <msg>
        <line>No such node</line>
    </msg>
</response>
""", [])

PANOS_XPATH_CONFIG_DEFAULT_GATEWAY_OUT_1 = ("""
<response status="success">
    <result>
        <default-gateway>10.11.95.254</default-gateway>
    </result>
</response>
""", ["10.11.95.254"])

PANOS_XPATH_CONFIG_DEFAULT_GATEWAY_OUT_2 = ("""
<response status="error">
    <msg>
        <line>No such node</line>
    </msg>
</response>
""", [])

PANOS_XPATH_CONFIG_DNS_OUT_1 = ("""
<response status="success">
    <result>
        <servers>
            <primary>8.8.8.8</primary>
            <secondary>8.8.4.4</secondary>
        </servers>
    </result>
</response>
""", ["8.8.8.8","8.8.4.4"])

PANOS_XPATH_CONFIG_DNS_OUT_2 = ("""
<response status="error">
    <msg>
        <line>No such node</line>
    </msg>
</response>
""", [])


PANOS_PING_OUT_1 = """<response status="success">
    <result>PING panwdbl.appspot.com (216.58.205.244) 56(84) bytes of data.
64 bytes from fra15s24-in-f20.1e100.net (216.58.205.244): icmp_seq=1 ttl=51 time=52.7 ms
64 bytes from fra15s24-in-f244.1e100.net (216.58.205.244): icmp_seq=2 ttl=51 time=52.7 ms
64 bytes from fra15s24-in-f20.1e100.net (216.58.205.244): icmp_seq=3 ttl=51 time=52.8 ms
64 bytes from fra15s24-in-f20.1e100.net (216.58.205.244): icmp_seq=4 ttl=51 time=52.7 ms
64 bytes from fra15s24-in-f20.1e100.net (216.58.205.244): icmp_seq=5 ttl=51 time=52.7 ms

--- panwdbl.appspot.com ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 3999ms
rtt min/avg/max/mdev = 52.702/52.752/52.845/0.256 ms
</result>
</response>"""

PANOS_PING_OUT_2 = """<response status="success">
    <result>PING panwdbl.appspot.com (216.58.205.244) 56(84) bytes of data.
64 bytes from fra15s24-in-f20.1e100.net (216.58.205.244): icmp_seq=1 ttl=51 time=52.7 ms
64 bytes from fra15s24-in-f244.1e100.net (216.58.205.244): icmp_seq=2 ttl=51 time=52.7 ms
64 bytes from fra15s24-in-f20.1e100.net (216.58.205.244): icmp_seq=3 ttl=51 time=52.8 ms
64 bytes from fra15s24-in-f20.1e100.net (216.58.205.244): icmp_seq=4 ttl=51 time=52.7 ms


--- panwdbl.appspot.com ping statistics ---
5 packets transmitted, 4 received, 20% packet loss, time 3999ms
rtt min/avg/max/mdev = 52.702/52.752/52.845/0.256 ms
</result>
</response>"""

PANOS_PING_PAIR_1 = (PANOS_PING_OUT_1, True)

PANOS_PING_PAIR_2 = (PANOS_PING_OUT_2, False)




GET_URL_EDL_PAIR_1 = (
    ("HTTPS_test",[("HTTP_test", "https://panwdbl.appspot.com/lists/mdl.txt"), ("HTTPS_test", "https://www.spamhaus.org/drop/edrop.txt"), ("Fake_EDL_type_domain", "http://the_fake_url_2.com")]),
    "spamhaus.org")
GET_URL_EDL_PAIR_2 = (
    ("HTTP_test",[("HTTP_test", "https://panwdbl.appspot.com/lists/mdl.txt"), ("HTTPS_test", "https://www.spamhaus.org/drop/edrop.txt"), ("Fake_EDL_type_domain", "http://the_fake_url_2.com")]),
    "panwdbl.appspot.com")

GET_DNS_SERVER_RESOLUTION_PAIR_1 = ("""
<response status="success">
    <result>216.58.205.244</result>
</response>
""","216.58.205.244")

GET_DNS_SERVER_RESOLUTION_PAIR_2 = ("""
<response status="success">
    <result>
        <value type="ipv4">172.217.23.148</value>
        <value type="ipv6">2a00:1450:4001:81e::2014</value>
    </result>
</response>
""","172.217.23.148")

GET_DNS_SERVER_RESOLUTION_PAIR_3 = ("""
<response status="success">
    <result>
        <value type="ipv4">ipv4 not resolved</value>
        <value type="ipv6">ipv6 not resolved</value>
    </result>
</response>
""","ipv4 not resolved")

GET_LOG_QUERY_JOB_ID_PAIR_1 = ("""
<response status="success" code="19">
    <result>
        <msg>
            <line>query job enqueued with jobid 66</line>
        </msg>
        <job>66</job>
    </result>
</response>
""","66")

PARSE_LOG_QUERY_LAST_10_MIN_PAIR_1 = ("""
<response status="success">
    <result>
        <job>
            <tenq>16:19:57</tenq>
            <tdeq>16:19:57</tdeq>
            <tlast>16:19:57</tlast>
            <status>FIN</status>
            <id>66</id>
        </job>
        <log>
            <logs count="20" progress="100">
                <entry logid="6830193719535266275">
                    <domain>1</domain>
                    <receive_time>2020/05/24 16:19:25</receive_time>
                    <serial>007051000065957</serial>
                    <seqno>12799910</seqno>
                    <actionflags>0x0</actionflags>
                    <is-logging-service>no</is-logging-service>
                    <type>SYSTEM</type>
                    <subtype>general</subtype>
                    <config_ver>0</config_ver>
                    <time_generated>2020/05/24 16:19:25</time_generated>
                    <dg_hier_level_1>0</dg_hier_level_1>
                    <dg_hier_level_2>0</dg_hier_level_2>
                    <dg_hier_level_3>0</dg_hier_level_3>
                    <dg_hier_level_4>0</dg_hier_level_4>
                    <device_name>illab-panfwa01</device_name>
                    <vsys_id>0</vsys_id>
                    <vsys>vsys1</vsys>
                    <eventid>general</eventid>
                    <fmt>0</fmt>
                    <id>0</id>
                    <module>general</module>
                    <severity>informational</severity>
                    <opaque>EDL(HTTPS_test) No changes to list file</opaque>
                </entry>
                <entry logid="6830193719535266274">
                    <domain>1</domain>
                    <receive_time>2020/05/24 16:19:25</receive_time>
                    <serial>007051000065957</serial>
                    <seqno>12799909</seqno>
                    <actionflags>0x0</actionflags>
                    <is-logging-service>no</is-logging-service>
                    <type>SYSTEM</type>
                    <subtype>general</subtype>
                    <config_ver>0</config_ver>
                    <time_generated>2020/05/24 16:19:25</time_generated>
                    <dg_hier_level_1>0</dg_hier_level_1>
                    <dg_hier_level_2>0</dg_hier_level_2>
                    <dg_hier_level_3>0</dg_hier_level_3>
                    <dg_hier_level_4>0</dg_hier_level_4>
                    <device_name>illab-panfwa01</device_name>
                    <vsys_id>0</vsys_id>
                    <vsys>vsys1</vsys>
                    <eventid>general</eventid>
                    <fmt>0</fmt>
                    <id>0</id>
                    <module>general</module>
                    <severity>medium</severity>
                    <opaque>EDL(HTTPS_test) Either EDL file was not updated at remote end or Downloaded file is not a text file. Using old copy for refresh.. EDL(vsys1/HTTPS_test ip) Either EDL file was not updated at remote end or Downloaded file is not a text file. Using old copy for refresh.. EDL(vsys1/HTTPS_test ip) Either EDL file was not updated at remote end or Downloaded file is not a text file. Using</opaque>
                </entry>
                <entry logid="6830193719535265920">
                    <domain>1</domain>
                    <receive_time>2020/05/24 16:14:24</receive_time>
                    <serial>007051000065957</serial>
                    <seqno>12799555</seqno>
                    <actionflags>0x0</actionflags>
                    <is-logging-service>no</is-logging-service>
                    <type>SYSTEM</type>
                    <subtype>general</subtype>
                    <config_ver>0</config_ver>
                    <time_generated>2020/05/24 16:14:24</time_generated>
                    <dg_hier_level_1>0</dg_hier_level_1>
                    <dg_hier_level_2>0</dg_hier_level_2>
                    <dg_hier_level_3>0</dg_hier_level_3>
                    <dg_hier_level_4>0</dg_hier_level_4>
                    <device_name>illab-panfwa01</device_name>
                    <vsys_id>0</vsys_id>
                    <vsys>vsys1</vsys>
                    <eventid>general</eventid>
                    <fmt>0</fmt>
                    <id>0</id>
                    <module>general</module>
                    <severity>informational</severity>
                    <opaque>EDL(HTTPS_test) No changes to list file</opaque>
                </entry>
                <entry logid="6830193719535265919">
                    <domain>1</domain>
                    <receive_time>2020/05/24 16:14:24</receive_time>
                    <serial>007051000065957</serial>
                    <seqno>12799554</seqno>
                    <actionflags>0x0</actionflags>
                    <is-logging-service>no</is-logging-service>
                    <type>SYSTEM</type>
                    <subtype>general</subtype>
                    <config_ver>0</config_ver>
                    <time_generated>2020/05/24 16:14:24</time_generated>
                    <dg_hier_level_1>0</dg_hier_level_1>
                    <dg_hier_level_2>0</dg_hier_level_2>
                    <dg_hier_level_3>0</dg_hier_level_3>
                    <dg_hier_level_4>0</dg_hier_level_4>
                    <device_name>illab-panfwa01</device_name>
                    <vsys_id>0</vsys_id>
                    <vsys>vsys1</vsys>
                    <eventid>general</eventid>
                    <fmt>0</fmt>
                    <id>0</id>
                    <module>general</module>
                    <severity>medium</severity>
                    <opaque>EDL(HTTPS_test) Either EDL file was not updated at remote end or Downloaded file is not a text file. Using old copy for refresh.. EDL(vsys1/HTTPS_test ip) Either EDL file was not updated at remote end or Downloaded file is not a text file. Using old copy for refresh.. EDL(vsys1/HTTPS_test ip) Either EDL file was not updated at remote end or Downloaded file is not a text file. Using</opaque>
                </entry>
                <entry logid="6830193719535265569">
                    <domain>1</domain>
                    <receive_time>2020/05/24 16:09:21</receive_time>
                    <serial>007051000065957</serial>
                    <seqno>12799204</seqno>
                    <actionflags>0x0</actionflags>
                    <is-logging-service>no</is-logging-service>
                    <type>SYSTEM</type>
                    <subtype>general</subtype>
                    <config_ver>0</config_ver>
                    <time_generated>2020/05/24 16:09:21</time_generated>
                    <dg_hier_level_1>0</dg_hier_level_1>
                    <dg_hier_level_2>0</dg_hier_level_2>
                    <dg_hier_level_3>0</dg_hier_level_3>
                    <dg_hier_level_4>0</dg_hier_level_4>
                    <device_name>illab-panfwa01</device_name>
                    <vsys_id>0</vsys_id>
                    <vsys>vsys1</vsys>
                    <eventid>general</eventid>
                    <fmt>0</fmt>
                    <id>0</id>
                    <module>general</module>
                    <severity>informational</severity>
                    <opaque>EDL(HTTPS_test) No changes to list file</opaque>
                </entry>
                <entry logid="6830193719535265568">
                    <domain>1</domain>
                    <receive_time>2020/05/24 16:09:21</receive_time>
                    <serial>007051000065957</serial>
                    <seqno>12799203</seqno>
                    <actionflags>0x0</actionflags>
                    <is-logging-service>no</is-logging-service>
                    <type>SYSTEM</type>
                    <subtype>general</subtype>
                    <config_ver>0</config_ver>
                    <time_generated>2020/05/24 16:09:21</time_generated>
                    <dg_hier_level_1>0</dg_hier_level_1>
                    <dg_hier_level_2>0</dg_hier_level_2>
                    <dg_hier_level_3>0</dg_hier_level_3>
                    <dg_hier_level_4>0</dg_hier_level_4>
                    <device_name>illab-panfwa01</device_name>
                    <vsys_id>0</vsys_id>
                    <vsys>vsys1</vsys>
                    <eventid>general</eventid>
                    <fmt>0</fmt>
                    <id>0</id>
                    <module>general</module>
                    <severity>medium</severity>
                    <opaque>EDL(HTTPS_test) Either EDL file was not updated at remote end or Downloaded file is not a text file. Using old copy for refresh.. EDL(vsys1/HTTPS_test ip) Either EDL file was not updated at remote end or Downloaded file is not a text file. Using old copy for refresh.. EDL(vsys1/HTTPS_test ip) Either EDL file was not updated at remote end or Downloaded file is not a text file. Using</opaque>
                </entry>
                <entry logid="6830193719535265224">
                    <domain>1</domain>
                    <receive_time>2020/05/24 16:04:20</receive_time>
                    <serial>007051000065957</serial>
                    <seqno>12798859</seqno>
                    <actionflags>0x0</actionflags>
                    <is-logging-service>no</is-logging-service>
                    <type>SYSTEM</type>
                    <subtype>general</subtype>
                    <config_ver>0</config_ver>
                    <time_generated>2020/05/24 16:04:20</time_generated>
                    <dg_hier_level_1>0</dg_hier_level_1>
                    <dg_hier_level_2>0</dg_hier_level_2>
                    <dg_hier_level_3>0</dg_hier_level_3>
                    <dg_hier_level_4>0</dg_hier_level_4>
                    <device_name>illab-panfwa01</device_name>
                    <vsys_id>0</vsys_id>
                    <vsys>vsys1</vsys>
                    <eventid>general</eventid>
                    <fmt>0</fmt>
                    <id>0</id>
                    <module>general</module>
                    <severity>informational</severity>
                    <opaque>EDL(HTTPS_test) No changes to list file</opaque>
                </entry>
                <entry logid="6830193719535265223">
                    <domain>1</domain>
                    <receive_time>2020/05/24 16:04:20</receive_time>
                    <serial>007051000065957</serial>
                    <seqno>12798858</seqno>
                    <actionflags>0x0</actionflags>
                    <is-logging-service>no</is-logging-service>
                    <type>SYSTEM</type>
                    <subtype>general</subtype>
                    <config_ver>0</config_ver>
                    <time_generated>2020/05/24 16:04:20</time_generated>
                    <dg_hier_level_1>0</dg_hier_level_1>
                    <dg_hier_level_2>0</dg_hier_level_2>
                    <dg_hier_level_3>0</dg_hier_level_3>
                    <dg_hier_level_4>0</dg_hier_level_4>
                    <device_name>illab-panfwa01</device_name>
                    <vsys_id>0</vsys_id>
                    <vsys>vsys1</vsys>
                    <eventid>general</eventid>
                    <fmt>0</fmt>
                    <id>0</id>
                    <module>general</module>
                    <severity>medium</severity>
                    <opaque>EDL(HTTPS_test) Either EDL file was not updated at remote end or Downloaded file is not a text file. Using old copy for refresh.. EDL(vsys1/HTTPS_test ip) Either EDL file was not updated at remote end or Downloaded file is not a text file. Using old copy for refresh.. EDL(vsys1/HTTPS_test ip) Either EDL file was not updated at remote end or Downloaded file is not a text file. Using</opaque>
                </entry>
                <entry logid="6830193719535265017">
                    <domain>1</domain>
                    <receive_time>2020/05/24 15:59:15</receive_time>
                    <serial>007051000065957</serial>
                    <seqno>12798652</seqno>
                    <actionflags>0x0</actionflags>
                    <is-logging-service>no</is-logging-service>
                    <type>SYSTEM</type>
                    <subtype>general</subtype>
                    <config_ver>0</config_ver>
                    <time_generated>2020/05/24 15:59:15</time_generated>
                    <dg_hier_level_1>0</dg_hier_level_1>
                    <dg_hier_level_2>0</dg_hier_level_2>
                    <dg_hier_level_3>0</dg_hier_level_3>
                    <dg_hier_level_4>0</dg_hier_level_4>
                    <device_name>illab-panfwa01</device_name>
                    <vsys_id>0</vsys_id>
                    <vsys>vsys1</vsys>
                    <eventid>general</eventid>
                    <fmt>0</fmt>
                    <id>0</id>
                    <module>general</module>
                    <severity>informational</severity>
                    <opaque>EDL(HTTPS_test) No changes to list file</opaque>
                </entry>
                <entry logid="6830193719535265016">
                    <domain>1</domain>
                    <receive_time>2020/05/24 15:59:15</receive_time>
                    <serial>007051000065957</serial>
                    <seqno>12798651</seqno>
                    <actionflags>0x0</actionflags>
                    <is-logging-service>no</is-logging-service>
                    <type>SYSTEM</type>
                    <subtype>general</subtype>
                    <config_ver>0</config_ver>
                    <time_generated>2020/05/24 15:59:15</time_generated>
                    <dg_hier_level_1>0</dg_hier_level_1>
                    <dg_hier_level_2>0</dg_hier_level_2>
                    <dg_hier_level_3>0</dg_hier_level_3>
                    <dg_hier_level_4>0</dg_hier_level_4>
                    <device_name>illab-panfwa01</device_name>
                    <vsys_id>0</vsys_id>
                    <vsys>vsys1</vsys>
                    <eventid>general</eventid>
                    <fmt>0</fmt>
                    <id>0</id>
                    <module>general</module>
                    <severity>medium</severity>
                    <opaque>EDL(HTTPS_test) Either EDL file was not updated at remote end or Downloaded file is not a text file. Using old copy for refresh.. EDL(vsys1/HTTPS_test ip) Either EDL file was not updated at remote end or Downloaded file is not a text file. Using old copy for refresh.. EDL(vsys1/HTTPS_test ip) Either EDL file was not updated at remote end or Downloaded file is not a text file. Using</opaque>
                </entry>
                <entry logid="6830193719535264655">
                    <domain>1</domain>
                    <receive_time>2020/05/24 15:54:14</receive_time>
                    <serial>007051000065957</serial>
                    <seqno>12798290</seqno>
                    <actionflags>0x0</actionflags>
                    <is-logging-service>no</is-logging-service>
                    <type>SYSTEM</type>
                    <subtype>general</subtype>
                    <config_ver>0</config_ver>
                    <time_generated>2020/05/24 15:54:14</time_generated>
                    <dg_hier_level_1>0</dg_hier_level_1>
                    <dg_hier_level_2>0</dg_hier_level_2>
                    <dg_hier_level_3>0</dg_hier_level_3>
                    <dg_hier_level_4>0</dg_hier_level_4>
                    <device_name>illab-panfwa01</device_name>
                    <vsys_id>0</vsys_id>
                    <vsys>vsys1</vsys>
                    <eventid>general</eventid>
                    <fmt>0</fmt>
                    <id>0</id>
                    <module>general</module>
                    <severity>informational</severity>
                    <opaque>EDL(HTTPS_test) No changes to list file</opaque>
                </entry>
                <entry logid="6830193719535264654">
                    <domain>1</domain>
                    <receive_time>2020/05/24 15:54:14</receive_time>
                    <serial>007051000065957</serial>
                    <seqno>12798289</seqno>
                    <actionflags>0x0</actionflags>
                    <is-logging-service>no</is-logging-service>
                    <type>SYSTEM</type>
                    <subtype>general</subtype>
                    <config_ver>0</config_ver>
                    <time_generated>2020/05/24 15:54:14</time_generated>
                    <dg_hier_level_1>0</dg_hier_level_1>
                    <dg_hier_level_2>0</dg_hier_level_2>
                    <dg_hier_level_3>0</dg_hier_level_3>
                    <dg_hier_level_4>0</dg_hier_level_4>
                    <device_name>illab-panfwa01</device_name>
                    <vsys_id>0</vsys_id>
                    <vsys>vsys1</vsys>
                    <eventid>general</eventid>
                    <fmt>0</fmt>
                    <id>0</id>
                    <module>general</module>
                    <severity>medium</severity>
                    <opaque>EDL(HTTPS_test) Either EDL file was not updated at remote end or Downloaded file is not a text file. Using old copy for refresh.. EDL(vsys1/HTTPS_test ip) Either EDL file was not updated at remote end or Downloaded file is not a text file. Using old copy for refresh.. EDL(vsys1/HTTPS_test ip) Either EDL file was not updated at remote end or Downloaded file is not a text file. Using</opaque>
                </entry>
                <entry logid="6830193719535264325">
                    <domain>1</domain>
                    <receive_time>2020/05/24 15:49:12</receive_time>
                    <serial>007051000065957</serial>
                    <seqno>12797960</seqno>
                    <actionflags>0x0</actionflags>
                    <is-logging-service>no</is-logging-service>
                    <type>SYSTEM</type>
                    <subtype>general</subtype>
                    <config_ver>0</config_ver>
                    <time_generated>2020/05/24 15:49:12</time_generated>
                    <dg_hier_level_1>0</dg_hier_level_1>
                    <dg_hier_level_2>0</dg_hier_level_2>
                    <dg_hier_level_3>0</dg_hier_level_3>
                    <dg_hier_level_4>0</dg_hier_level_4>
                    <device_name>illab-panfwa01</device_name>
                    <vsys_id>0</vsys_id>
                    <vsys>vsys1</vsys>
                    <eventid>general</eventid>
                    <fmt>0</fmt>
                    <id>0</id>
                    <module>general</module>
                    <severity>informational</severity>
                    <opaque>EDL(HTTPS_test) Refresh job success</opaque>
                </entry>
                <entry logid="6830193719535264290">
                    <domain>1</domain>
                    <receive_time>2020/05/24 15:48:47</receive_time>
                    <serial>007051000065957</serial>
                    <seqno>12797925</seqno>
                    <actionflags>0x0</actionflags>
                    <is-logging-service>no</is-logging-service>
                    <type>SYSTEM</type>
                    <subtype>general</subtype>
                    <config_ver>0</config_ver>
                    <time_generated>2020/05/24 15:48:47</time_generated>
                    <dg_hier_level_1>0</dg_hier_level_1>
                    <dg_hier_level_2>0</dg_hier_level_2>
                    <dg_hier_level_3>0</dg_hier_level_3>
                    <dg_hier_level_4>0</dg_hier_level_4>
                    <device_name>illab-panfwa01</device_name>
                    <vsys_id>0</vsys_id>
                    <vsys>vsys1</vsys>
                    <eventid>general</eventid>
                    <fmt>0</fmt>
                    <id>0</id>
                    <module>general</module>
                    <severity>informational</severity>
                    <opaque>EDL(HTTPS_test) EDL Fetch job done</opaque>
                </entry>
                <entry logid="6830193719535263754">
                    <domain>1</domain>
                    <receive_time>2020/05/24 15:41:10</receive_time>
                    <serial>007051000065957</serial>
                    <seqno>12797389</seqno>
                    <actionflags>0x0</actionflags>
                    <is-logging-service>no</is-logging-service>
                    <type>SYSTEM</type>
                    <subtype>general</subtype>
                    <config_ver>0</config_ver>
                    <time_generated>2020/05/24 15:41:10</time_generated>
                    <dg_hier_level_1>0</dg_hier_level_1>
                    <dg_hier_level_2>0</dg_hier_level_2>
                    <dg_hier_level_3>0</dg_hier_level_3>
                    <dg_hier_level_4>0</dg_hier_level_4>
                    <device_name>illab-panfwa01</device_name>
                    <vsys_id>0</vsys_id>
                    <vsys>vsys1</vsys>
                    <eventid>general</eventid>
                    <fmt>0</fmt>
                    <id>0</id>
                    <module>general</module>
                    <severity>informational</severity>
                    <opaque>EDL(HTTPS_test) No changes to list file</opaque>
                </entry>
                <entry logid="6830193719535263753">
                    <domain>1</domain>
                    <receive_time>2020/05/24 15:41:10</receive_time>
                    <serial>007051000065957</serial>
                    <seqno>12797388</seqno>
                    <actionflags>0x0</actionflags>
                    <is-logging-service>no</is-logging-service>
                    <type>SYSTEM</type>
                    <subtype>general</subtype>
                    <config_ver>0</config_ver>
                    <time_generated>2020/05/24 15:41:10</time_generated>
                    <dg_hier_level_1>0</dg_hier_level_1>
                    <dg_hier_level_2>0</dg_hier_level_2>
                    <dg_hier_level_3>0</dg_hier_level_3>
                    <dg_hier_level_4>0</dg_hier_level_4>
                    <device_name>illab-panfwa01</device_name>
                    <vsys_id>0</vsys_id>
                    <vsys>vsys1</vsys>
                    <eventid>general</eventid>
                    <fmt>0</fmt>
                    <id>0</id>
                    <module>general</module>
                    <severity>medium</severity>
                    <opaque>EDL(HTTPS_test) Unable to fetch external dynamic list. Timeout was reached. Using old copy for refresh.</opaque>
                </entry>
                <entry logid="6830193719535263386">
                    <domain>1</domain>
                    <receive_time>2020/05/24 15:35:59</receive_time>
                    <serial>007051000065957</serial>
                    <seqno>12797021</seqno>
                    <actionflags>0x0</actionflags>
                    <is-logging-service>no</is-logging-service>
                    <type>SYSTEM</type>
                    <subtype>general</subtype>
                    <config_ver>0</config_ver>
                    <time_generated>2020/05/24 15:35:59</time_generated>
                    <dg_hier_level_1>0</dg_hier_level_1>
                    <dg_hier_level_2>0</dg_hier_level_2>
                    <dg_hier_level_3>0</dg_hier_level_3>
                    <dg_hier_level_4>0</dg_hier_level_4>
                    <device_name>illab-panfwa01</device_name>
                    <vsys_id>0</vsys_id>
                    <vsys>vsys1</vsys>
                    <eventid>general</eventid>
                    <fmt>0</fmt>
                    <id>0</id>
                    <module>general</module>
                    <severity>informational</severity>
                    <opaque>EDL(HTTPS_test) No changes to list file</opaque>
                </entry>
                <entry logid="6830193719535263385">
                    <domain>1</domain>
                    <receive_time>2020/05/24 15:35:59</receive_time>
                    <serial>007051000065957</serial>
                    <seqno>12797020</seqno>
                    <actionflags>0x0</actionflags>
                    <is-logging-service>no</is-logging-service>
                    <type>SYSTEM</type>
                    <subtype>general</subtype>
                    <config_ver>0</config_ver>
                    <time_generated>2020/05/24 15:35:59</time_generated>
                    <dg_hier_level_1>0</dg_hier_level_1>
                    <dg_hier_level_2>0</dg_hier_level_2>
                    <dg_hier_level_3>0</dg_hier_level_3>
                    <dg_hier_level_4>0</dg_hier_level_4>
                    <device_name>illab-panfwa01</device_name>
                    <vsys_id>0</vsys_id>
                    <vsys>vsys1</vsys>
                    <eventid>general</eventid>
                    <fmt>0</fmt>
                    <id>0</id>
                    <module>general</module>
                    <severity>medium</severity>
                    <opaque>EDL(HTTPS_test) Unable to fetch external dynamic list. Timeout was reached. Using old copy for refresh.</opaque>
                </entry>
                <entry logid="6830193719535263040">
                    <domain>1</domain>
                    <receive_time>2020/05/24 15:30:47</receive_time>
                    <serial>007051000065957</serial>
                    <seqno>12796675</seqno>
                    <actionflags>0x0</actionflags>
                    <is-logging-service>no</is-logging-service>
                    <type>SYSTEM</type>
                    <subtype>general</subtype>
                    <config_ver>0</config_ver>
                    <time_generated>2020/05/24 15:30:47</time_generated>
                    <dg_hier_level_1>0</dg_hier_level_1>
                    <dg_hier_level_2>0</dg_hier_level_2>
                    <dg_hier_level_3>0</dg_hier_level_3>
                    <dg_hier_level_4>0</dg_hier_level_4>
                    <device_name>illab-panfwa01</device_name>
                    <vsys_id>0</vsys_id>
                    <vsys>vsys1</vsys>
                    <eventid>general</eventid>
                    <fmt>0</fmt>
                    <id>0</id>
                    <module>general</module>
                    <severity>informational</severity>
                    <opaque>EDL(HTTPS_test) Refresh job success</opaque>
                </entry>
                <entry logid="6830193719535262981">
                    <domain>1</domain>
                    <receive_time>2020/05/24 15:29:22</receive_time>
                    <serial>007051000065957</serial>
                    <seqno>12796616</seqno>
                    <actionflags>0x0</actionflags>
                    <is-logging-service>no</is-logging-service>
                    <type>SYSTEM</type>
                    <subtype>general</subtype>
                    <config_ver>0</config_ver>
                    <time_generated>2020/05/24 15:29:22</time_generated>
                    <dg_hier_level_1>0</dg_hier_level_1>
                    <dg_hier_level_2>0</dg_hier_level_2>
                    <dg_hier_level_3>0</dg_hier_level_3>
                    <dg_hier_level_4>0</dg_hier_level_4>
                    <device_name>illab-panfwa01</device_name>
                    <vsys_id>0</vsys_id>
                    <vsys>vsys1</vsys>
                    <eventid>general</eventid>
                    <fmt>0</fmt>
                    <id>0</id>
                    <module>general</module>
                    <severity>informational</severity>
                    <opaque>EDL(HTTPS_test) EDL Fetch job done</opaque>
                </entry>
            </logs>
        </log>
        <meta>
            <devices>
                <entry name="localhost.localdomain">
                    <hostname>localhost.localdomain</hostname>
                    <vsys>
                        <entry name="vsys1">
                            <display-name>vsys1</display-name>
                        </entry>
                    </vsys>
                </entry>
            </devices>
        </meta>
    </result>
</response>
""","2")

PARSE_LOG_QUERY_LAST_10_MIN_PAIR_2 = ("""
<response status=\"success\"><result>
  <job>
    <tenq>17:07:14</tenq>
    <tdeq>17:07:14</tdeq>
    <tlast>16:00:00</tlast>
    <status>ACT</status>
    <id>1</id>
  </job>
  <log>
    <logs count=\"0\" progress=\"0\"/>
  </log>
  <meta>
    <devices>
      <entry name=\"localhost.localdomain\">
        <hostname>localhost.localdomain</hostname>
        <vsys>
          <entry name=\"vsys1\">
            <display-name>vsys1</display-name>
          </entry>
        </vsys>
      </entry>
    </devices>
  </meta>
</result></response>
""","0")
