import unittest
import automation.workflows.panw_edl_not_connected.paloaltonetworks.panw_edl_not_connected_mock_data as mock
import automation.workflows.panw_edl_not_connected.paloaltonetworks.panw_edl_not_connected_parser as parser


class EDLNotConnectedParserTests(unittest.TestCase):

    def test_get_edl_url(self):
        pair1 = mock.PANOS_XPATH_CONFIG_EXTERNAL_LIST_OUT_1
        pair2 = mock.PANOS_XPATH_CONFIG_EXTERNAL_LIST_OUT_2
        self.assertEqual(parser.get_edl_url(pair1[0]), pair1[1])
        self.assertEqual(parser.get_edl_url(pair2[0]), pair2[1])

    def test_get_proxy(self):
        pair1 = mock.PANOS_XPATH_CONFIG_PROXY_OUT_1
        pair2 = mock.PANOS_XPATH_CONFIG_PROXY_OUT_2
        self.assertEqual(parser.get_proxy(pair1[0]), pair1[1])
        self.assertEqual(parser.get_proxy(pair2[0]), pair2[1])

    def test_get_service_route(self):
        pair1 = mock.PANOS_XPATH_CONFIG_SERVICE_ROUTE_OUT_1
        pair2 = mock.PANOS_XPATH_CONFIG_SERVICE_ROUTE_OUT_2
        self.assertEqual(parser.get_service_route(pair1[0]), pair1[1])
        self.assertEqual(parser.get_service_route(pair2[0]), pair2[1])

    def test_get_default_gateway(self):
        pair1 = mock.PANOS_XPATH_CONFIG_DEFAULT_GATEWAY_OUT_1
        pair2 = mock.PANOS_XPATH_CONFIG_DEFAULT_GATEWAY_OUT_2
        self.assertEqual(parser.get_default_gateway(pair1[0]), pair1[1])
        self.assertEqual(parser.get_default_gateway(pair2[0]), pair2[1])

    def test_get_dns_servers(self):
        pair1 = mock.PANOS_XPATH_CONFIG_DNS_OUT_1
        pair2 = mock.PANOS_XPATH_CONFIG_DNS_OUT_2
        self.assertEqual(parser.get_dns_servers(pair1[0]), pair1[1])
        self.assertEqual(parser.get_dns_servers(pair2[0]), pair2[1])

    def test_get_ping_status(self):
        pair1 = mock.PANOS_PING_PAIR_1
        pair2 = mock.PANOS_PING_PAIR_2
        self.assertEqual(pair1[1], parser.get_ping_status(pair1[0]))
        self.assertEqual(pair2[1], parser.get_ping_status(pair2[0]))

    def test_get_url_edl(self):
        pair1 = mock.GET_URL_EDL_PAIR_1
        pair2 = mock.GET_URL_EDL_PAIR_1
        self.assertEqual(pair1[1], parser.get_url_edl(pair1[0][0], pair1[0][1]))
        self.assertEqual(pair2[1], parser.get_url_edl(pair2[0][0], pair2[0][1]))

    def test_get_dns_server_resolution(self):
        pair1 = mock.GET_DNS_SERVER_RESOLUTION_PAIR_1
        pair2 = mock.GET_DNS_SERVER_RESOLUTION_PAIR_2
        pair3 = mock.GET_DNS_SERVER_RESOLUTION_PAIR_3
        self.assertEqual(pair1[1], parser.get_dns_server_resolution(pair1[0]))
        self.assertEqual(pair2[1], parser.get_dns_server_resolution(pair2[0]))
        self.assertEqual(pair3[1], parser.get_dns_server_resolution(pair3[0]))

    def test_get_log_query_job_id(self):
        pair1 = mock.GET_LOG_QUERY_JOB_ID_PAIR_1
        self.assertEqual(pair1[1], parser.get_log_query_job_id(pair1[0]))

    def test_parse_log_query_last_10_min(self):
        pair1 = mock.PARSE_LOG_QUERY_LAST_10_MIN_PAIR_1
        pair2 = mock.PARSE_LOG_QUERY_LAST_10_MIN_PAIR_2
        self.assertEqual(pair1[1], parser.parse_log_query_last_10_min(pair1[0]))
        self.assertEqual(pair2[1], parser.parse_log_query_last_10_min(pair2[0]))



if __name__ == '__main__':
    unittest.main()