from parser_service.public.helper_methods import *
from urllib.parse import urlparse
from datetime import datetime

def check_variant_matches(**kwargs) -> bool:
    # This should work on all gateways
    tags = kwargs.get('my_device_tags', None)
    if tags is None:
        return False
    if tags.get('product') != 'firewall':
        return False
    return True

def get_edl_url(raw_data: str) -> list:
    edl_list = []
    if raw_data:  # We check if input is not empty
        data = parse_data_as_xml(raw_data)
        if data and data['response']['@status'] != "error":
            edl_configured = data['response']['result']['external-list']['entry']
            if type(edl_configured) != list:
                edl_configured = [edl_configured]
            for single_edl in edl_configured:
                edl_name = single_edl['@name']
                edl_type = list(single_edl['type'].keys())[0]
                edl_url = single_edl['type'][edl_type]['url']
                edl_data = (edl_name, edl_url)
                edl_list.append(edl_data)
    return edl_list

def get_proxy(raw_data: str) -> list:
    proxy_list = []
    if raw_data:  # We check if input is not empty
        data = parse_data_as_xml(raw_data)
        if data and data['response']['@status'] != "error":
            proxy = data['response']['result']['secure-proxy-server']
            proxy_list.append(proxy)
    return proxy_list


def get_service_route(raw_data: str) -> list:
    service_route_list = []
    if raw_data:  # We check if input is not empty
        data = parse_data_as_xml(raw_data)
        if data and data['response']['@status'] != "error":
            ip_address = data['response']['result']['source']['address']
            interface = data['response']['result']['source']['interface']
            service_route = (ip_address, interface)
            service_route_list.append(service_route)
    return service_route_list

def get_default_gateway(raw_data: str) -> list:
    def_gateway_list = []
    if raw_data:  # We check if input is not empty
        data = parse_data_as_xml(raw_data)
        if data and data['response']['@status'] != "error":
            def_gateway = data['response']['result']['default-gateway']
            def_gateway_list.append(def_gateway)
    return def_gateway_list

def get_dns_servers(raw_data: str) -> list:
    dns_server_list = []
    if raw_data:  # We check if input is not empty
        data = parse_data_as_xml(raw_data)
        if data and data['response']['@status'] != "error":
            dns_server_pri = data['response']['result']['servers']['primary']
            if len(dns_server_pri) > 0:
                dns_server_list.append(dns_server_pri)
            dns_server_sec = data['response']['result']['servers']['secondary']
            if len(dns_server_sec) > 0:
                dns_server_list.append(dns_server_sec)
    return dns_server_list

def get_ping_status(raw_data: str) -> bool:
    return ' 0% packet loss' in raw_data

def get_url_edl (edl_item: str, list_edl_url: list) -> str:
    for edl_configured in list_edl_url:
        if edl_configured[0] == edl_item:
            edl_url = edl_configured[1]
            edl_url_parsed = urlparse(edl_url)
            edl_url_parsed = edl_url_parsed.hostname.replace("www.", "")
            return edl_url_parsed
    return None

def get_dns_server_resolution (raw_data: str) -> str:
    ip_address = ""
    if raw_data:  # We check if input is not empty
        data = parse_data_as_xml(raw_data)
        if data and data['response']['@status'] != "error":
            try:
                ip_address = data['response']['result']['value']['@type'=='ipv4']['#text']
            except:
                ip_address = data['response']['result']
    return ip_address

def get_log_query_job_id (raw_data: str) -> str:
    if raw_data:  # We check if input is not empty
        data = parse_data_as_xml(raw_data)
        if data and data['response']['@status'] != "error":
            return data['response']['result']['job']

def parse_log_query_last_10_min (raw_data: str) -> int:
    error_entries = 0
    time_format = '%H:%M:%S'
    if raw_data:  # We check if input is not empty
        data = parse_data_as_xml(raw_data)
        if data and data['response']['@status'] != "error":
            if  data['response']['result']['log']['logs']['@count'] != "0":
                current_time = data['response']['result']['job']['tlast']
                for entry in data['response']['result']['log']['logs']['entry']:
                    entry_time = entry['receive_time']
                    entry_time = entry_time.split(' ')[1]
                    time_delta = datetime.strptime(current_time, time_format) - datetime.strptime(entry_time, time_format)
                    if time_delta.total_seconds() < 600 and entry['severity']!="informational":
                        error_entries += 1
            else:
                error_entries = data['response']['result']['log']['logs']['@count']
            return str(error_entries)
