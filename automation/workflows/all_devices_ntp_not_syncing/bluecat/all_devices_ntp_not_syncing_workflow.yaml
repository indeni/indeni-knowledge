id: ntp_not_syncing_bluecat
friendly_name: NTP not syncing
start_block: get_device_tags

blocks:
  get_device_tags:
    type: device_tags
    name: Get device tags
    register_to: my_device_tags
    go_to: make_variant_matches

  make_variant_matches:
    type: logic
    name: Match device against variant
    args: [my_device_tags]
    method: check_variant_matches
    register_to: bool_variant_matches
    go_to: check_if_variant_matches

  check_if_variant_matches:
    type: if
    name: Check if device matches requirements
    condition: bool_variant_matches
    then_go_to: collect_ntp_servers_and_stats
    else_go_to: variant_conclusion

  variant_conclusion:
    type: conclusion
    name: Incompatible workflow
    triage_conclusion: |
      This workflow is incompatible with the device.
    triage_remediation_steps: |
      Contact Indeni and request that compatibility be added.

  collect_ntp_servers_and_stats:
    type: device_task
    name: Get NTP servers and stats
    runner:
      type: SSH
      command: ntpq -np
    parser:
      method: identity_parser
      args: []
    register_to: ntp_result
    go_to: make_bool_is_ntp_up

  make_bool_is_ntp_up:
    type: logic
    name: Is NTP up
    args: [ntp_result]
    method: is_ntp_up
    register_to: bool_is_ntp_up
    go_to: get_single_issue_item

  get_single_issue_item:
    type: if
    name: Check if NTP service up
    condition: bool_is_ntp_up
    then_go_to: get_issue_items
    else_go_to: get_ntp_config

  get_ntp_config:
    type: device_task
    name: Get clish NTP configuration
    runner:
      type: SSH
      command: PsmClient node get| grep ntp-enable
    parser:
      method: is_ntp_service_enabled
      args: []
    register_to: bool_is_ntp_service_enabled
    go_to: check_if_ntp_service_enabled

  check_if_ntp_service_enabled:
    type: if
    name: Check if ntp service enabled
    condition: bool_is_ntp_service_enabled
    then_go_to: conclusion_ntp_service_crashed
    else_go_to: conclusion_ntp_service_is_active_off

  conclusion_ntp_service_crashed:
    type: conclusion
    name: NTP service crashed
    triage_conclusion: |
      NTP service is not responding
    triage_remediation_steps: |
      service ntpd restart

  conclusion_ntp_service_is_active_off:
    type: conclusion
    name: NTP configuration is active of
    triage_conclusion: |
      NTP service is not enabled
    triage_remediation_steps: |
      To configure NTP on a BDDS appliance:
      1. From the configuration drop-down menu, select a configuration.
      2. Select the Servers tab. Tabs remember the page you last worked on, so select the tab again to ensure you're on the Configuration information page.
      3. Under Servers, click the name of a BDDS. The Details tab for the server opens.
      4. Click the server name menu and select Service Configuration.
      5. Click the server name menu and select Service Configuration. The Service Configuration page opens.
      6. From the Service Type drop-down menu, select Network Time Protocol (NTP). Address Manager queries the server and returns the current values for the service settings.
      7. Under General Settings, set the following parameters:
        - Enable NTP Service: select this check box to enable the NTP service; deselect this check box to disable the NTP service.
        - NTP Server: enter the fully-qualified domain name or IP address for a remote NTP server from which Address Manager or DNS/DHCP Server will reference the time.
        - Stratum: select a stratum value for the NTP server being added. This value will be associated to an individual NTP server specified in the NTP Server field. Select Default to use the stratum value set on the remote NTP server.
          Note: Stratum values indicate the hierarchy level for the NTP server, which is the number of servers to a reference clock. This is used by the NTP client to avoid synchronization loops by preferring servers with a lower stratum.
      8. Click Add to associate a stratum value to a server and add them to the list. To remove a server, select it from the list and click Remove. The top-most NTP server will be queried first, then the second, and continues down the list. To change the order of servers in the list, select a server in the list and click Move up or Move down.
        By default, the NTP Server list contains at least the following IP addresses:
        - DNS/DHCP Server NTP list: the IP address for the Address Manager appliance managing the DNS/DHCP Server
        - Address Manager NTP list: the Local Reference Clock (127.127.1.0) on the connected server.

  get_issue_items:
    type: issue_items
    name: Get issue items
    register_to: my_items
    go_to: start_loop

  start_loop:
    type: foreach
    name: Start loop alarmed items
    register_item_to: ntp_issue_item
    start_block: make_bool_is_ntp_synchronized
    blocks:

      make_bool_is_ntp_synchronized:
        type: logic
        name: Is NTP synchronized
        args: [ntp_issue_item]
        method: is_ntp_synchronized
        register_to: bool_is_ntp_synchronized
        go_to: check_if_ntp_synchronized

      check_if_ntp_synchronized:
        type: if
        name: Check if NTP synchronized
        condition: bool_is_ntp_synchronized
        then_go_to: conclusion_ntp_server_in_sync
        else_go_to: check_ntp_server_hostname

      conclusion_ntp_server_in_sync:
        type: conclusion
        name: NTP server is synced
        triage_conclusion: |
          Problem solved after NTP re-sync
        triage_remediation_steps: |
          Please contact BlueCat support

      check_ntp_server_hostname:
        type: logic
        name: Check if NTP server is defined as hostname
        args: [ntp_issue_item]
        method: is_ntp_server_hostname
        register_to: is_ntp_server_hostname
        go_to: check_if_ntp_server_hostname

      check_if_ntp_server_hostname:
        type: if
        name: Check if NTP needs DNS to resolve hostname
        condition: is_ntp_server_hostname[0]
        then_go_to: get_ntp_server_dns_resolution
        else_go_to: get_ntp_server_source_address

      get_ntp_server_dns_resolution:
        type: device_task
        name: Get NTP server dns resolution
        runner:
          type: SSH
          command: dig +short {{ntp_issue_item}}
        parser:
          method: is_ntp_server_name_resolved
          args: []
        register_to: is_ntp_server_hostname
        go_to: check_if_ntp_server_name_resolved

      check_if_ntp_server_name_resolved:
        type: if
        name: Check if NTP server name resolved
        condition: is_ntp_server_hostname[0]
        then_go_to: get_ntp_server_source_address
        else_go_to: ntp_server_name_not_resolved

      ntp_server_name_not_resolved: # TODO : Extra steps check if DNS configured / Check if DNS enabled
        type: conclusion
        name: NTP server name not resolved
        triage_conclusion: DNS is not able to resolve NTP server name
        triage_remediation_steps: |
          Check if DNS is configured and running
          Check name resolution with other DNS servers.

      get_ntp_server_source_address:
        type: device_task
        name: Get NTP server source address
        runner:
          type: SSH
          command: ip r get {{is_ntp_server_hostname[1]}}
        parser:
          method: parse_ntp_interface_name
          args: []
        register_to: ntp_interface_name
        go_to: get_routing_interface_admin_state

      get_routing_interface_admin_state:
        type: device_task
        name: Get routing interface admin state
        runner:
          type: SSH
          command: ip link show up | grep {{ntp_interface_name}}
        parser:
          method: is_interface_admin_state_up
          args: []
        register_to: bool_is_interface_admin_state_up
        go_to: check_if_interface_admin_state_up

      check_if_interface_admin_state_up:
        type: if
        name: Check if interface admin state up
        condition: bool_is_interface_admin_state_up
        then_go_to: get_routing_interface_link_state
        else_go_to: ntp_interface_is_admin_down

      ntp_interface_is_admin_down:
        type: conclusion
        name: NTP interface is admin down
        triage_conclusion: |
          This device is not able to connect contact NTP server because interface {{ntp_interface_name}} is administratively disabled.
        triage_remediation_steps: |
          Enable interface {ntp_interface_name}}

      get_routing_interface_link_state:
        type: device_task
        name: Get routing interface link state
        runner:
          type: SSH
          command: ip link show dev {{ntp_interface_name}} | tr '<>,' ' '
        parser:
          method: is_interface_link_state_up
          args: []
        register_to: bool_is_interface_link_state_up
        go_to: check_if_interface_link_state_up

      check_if_interface_link_state_up:
        type: if
        name: Check if interface link state up
        condition: bool_is_interface_link_state_up
        then_go_to: get_ntp_server_reachable
        else_go_to: ntp_interface_is_link_down

      ntp_interface_is_link_down:
        type: conclusion
        name: NTP interface link is down
        triage_conclusion: |
          This device is not able to contact NTP server because interface {{ntp_interface_name}} has no link
        triage_remediation_steps: |
          Check cable connected to the interface

      get_ntp_server_reachable:
        type: device_task
        name: Get NTP server reachable
        runner:
          type: SSH
          command: ping -c 10 {{ntp_issue_item}}
        parser:
          method: is_ntp_server_reachable
          args: []
        register_to: bool_is_ntp_server_reachable
        go_to: check_if_ntp_server_is_reachable

      check_if_ntp_server_is_reachable:
        type: if
        name: Check if NTP server is reachable
        condition: bool_is_ntp_server_reachable
        then_go_to: get_if_ntp_service_up_and_running
        else_go_to: routing_problem

      routing_problem:
        type: conclusion
        name: Routing problem
        triage_conclusion: |
          Routing path is broken at some point between this device and the NTP server {{ntp_issue_item}}
        triage_remediation_steps: |
          Check routing path with traceroute tool.

      get_if_ntp_service_up_and_running:
        type: device_task
        name: Get if NTP service is up and running
        runner:
          type: SSH
          command: ntpdate -d {{ntp_issue_item}}
        parser:
          method: is_ntp_service_up_and_running
          args: [ntp_issue_item]
        register_to: bool_is_ntp_service_up_and_running
        go_to: check_if_ntp_service_up_and_running

      check_if_ntp_service_up_and_running:
        type: if
        name: Check if NTP service is up and running
        condition: bool_is_ntp_service_up_and_running
        then_go_to: detailed_analysis_needed
        else_go_to: ntp_server_not_responding

      detailed_analysis_needed:
        type: conclusion
        name: Detailed analysis needed
        triage_conclusion: |
          NTP server is well configured on host and reachable. Some extra troubleshooting should be made by the firewall administrator
        triage_remediation_steps: |
          * Check NTP service logs (cat /var/log/messages* | grep ntpd)
          * Packet capture (tcpdump, fwmonitor)
          * NTP authentification conf (if applicable)

      ntp_server_not_responding:
        type: conclusion
        name: NTP server not responding
        triage_conclusion: |
          NTP service is not running or is not replying to NTP requests
        triage_remediation_steps: |
          Configure different NTP server than {{ntp_issue_item}}
