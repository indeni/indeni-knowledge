import re


def check_variant_matches(**kwargs) -> bool:
    # This should work on all devices with one of the below versions
    tags = kwargs.get('my_device_tags', None)
    if tags is None:
        return False
    return True

def identity_parser(raw_data: str) -> str:
    return raw_data.strip()

def is_ntp_up(**kwargs) -> bool:
    raw_data = kwargs.get('ntp_result', '')
    return 'Connection refused' not in raw_data

def is_ntp_server_hostname(**kwargs) -> tuple:
    raw_data = kwargs.get('ntp_issue_item', '')
    if re.match(r'\d+\.\d+\.\d+\.\d+', raw_data) is not None:
        return (False, raw_data)
    else:
        return (True, raw_data)

def is_ntp_synchronized(**kwargs) -> bool:
    raw_data = kwargs.get('ntp_issue_item', '')
    lines = raw_data.split('\n')
    for line in lines:
        if line.strip().startswith('*'):
            return True
    return False

def is_ntp_service_enabled(raw_data: str) -> bool:
    return raw_data.split('=')[0] == 'node get-notify ntp-enable' and raw_data.split('=')[1] == '1'

def is_ntp_server_name_resolved(raw_data: str) -> tuple:
    return (len(raw_data) > 0, raw_data.split('\n'))

def parse_ntp_interface_name(raw_data: str) -> str:
    match = re.search(r'dev\s+(.*?)\s+src', raw_data)
    if match is not None:
        return match.group(1)
    else:
        return ""

def is_interface_admin_state_up(raw_data: str) -> bool:
    return True if len(raw_data)>0 else False

def is_interface_link_state_up(raw_data: str) -> bool:
    return re.search(r'LOWER_UP', raw_data) is not None

def is_ntp_server_reachable(raw_data: str) -> bool:
    match = re.search(r'(\d{1,3})%\spacket\sloss', raw_data)
    return match is not None and 0 <= int(match.group(1)) <= 99

def is_ntp_service_up_and_running(raw_data, **kwargs) -> bool:
    ntp_ip_address = kwargs.get('ntp_ip_address', '')
    return re.search(f'receive({ntp_ip_address})', raw_data) is not None
