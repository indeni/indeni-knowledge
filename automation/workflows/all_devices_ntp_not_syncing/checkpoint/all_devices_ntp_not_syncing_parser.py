import re


def check_variant_matches(**kwargs) -> bool:
    # This should work on all devices with one of the below versions
    tags = kwargs.get('my_device_tags', None)
    if tags is None:
        return False
    versions = [x.lower() for x in ['R77.30', 'R80.10', 'R80.20', 'R80.30', 'R76SP.30', 'R76SP.40', 'R76SP.50', 'R80.20SP', 'R80.40']]
    if tags.get('os.version', '').lower() not in versions:
        return False
    return True


def identity_parser(raw_data: str) -> str:
    return raw_data.strip()

def is_ntp_up(**kwargs) -> bool:
    raw_data = kwargs.get('ntp_result', '')
    return 'Connection refused' not in raw_data

def is_ntp_synchronized(**kwargs) -> bool:
    raw_data = kwargs.get('ntp_issue_item', '')
    lines = raw_data.split('\n')
    for line in lines:
        if line.strip().startswith('*'):
            return True
    return False

def is_ntp_service_enabled(raw_data: str) -> bool:
    lines = raw_data.split('\n')
    return len(lines) > 0 and 'off' not in lines[0]

def is_ntp_server_name_resolved(raw_data: str) -> bool:
    # temporal solution. IKP-4053 should address this
    if 'connection timed out' in raw_data:
        return False
    if "server can't find" in raw_data:
        return False
    return True

def parse_ntp_interface_name(raw_data: str) -> str:
    match = re.search(r'dev\s+(.*?)\s+src', raw_data)
    if match is not None:
        return match.group(1)
    else:
        return ""

def is_interface_admin_state_up(raw_data: str) -> bool:
    lines = raw_data.split('\n')
    for line in lines:
        if 'MTU:' in line and 'UP ' in line:
            return True
    return False

def is_interface_link_state_up(raw_data: str) -> bool:
    return re.search(r'LOWER_UP', raw_data) is not None

def is_ntp_server_reachable(raw_data: str) -> bool:
    match = re.search(r'(\d{1,3})%\spacket\sloss', raw_data)
    return match is not None and 0 <= int(match.group(1)) <= 99

def is_ntp_service_up_and_running(raw_data, **kwargs) -> bool:
    ntp_ip_address = kwargs.get('ntp_ip_address', '')
    return re.search(f'receive({ntp_ip_address})', raw_data) is not None

def is_ntp_well_configured(raw_data: str) -> bool:
    lines = raw_data.split('\n')
    for line in lines:
        if re.match(r'set\sntp\sserver\s(primary|secondary)\s(.*)\sversion\s[1234]', line.strip()) is not None:
            return True
    return False
