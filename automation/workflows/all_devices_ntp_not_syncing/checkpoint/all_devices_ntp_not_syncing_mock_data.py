#SSH command: ntpq -np
NTPQ_NP_OUT_1 = """ntpq: read: Connection refused"""

IDENTITY_PARSER_PAIR_1 = (NTPQ_NP_OUT_1,NTPQ_NP_OUT_1)

NTPQ_NP_OUT_2 = """     remote           refid      st t when poll reach   delay   offset  jitter
==============================================================================
 0.ubuntu.pool.n .POOL.          16 p    -   64    0    0.000    0.000   0.000
 1.ubuntu.pool.n .POOL.          16 p    -   64    0    0.000    0.000   0.000
 2.ubuntu.pool.n .POOL.          16 p    -   64    0    0.000    0.000   0.000
 3.ubuntu.pool.n .POOL.          16 p    -   64    0    0.000    0.000   0.000
 ntp.ubuntu.com  .POOL.          16 p    -   64    0    0.000    0.000   0.000
+162.159.200.1   10.48.8.4        3 u  537 1024  377   56.460    0.514   1.671
+162.159.200.123 10.48.8.4        3 u  442 1024  377   55.800   -0.263   1.494
-91.189.94.4     17.253.34.125    2 u  336 1024  377   64.089   -1.326  12.686
+91.189.91.157   132.163.96.1     2 u  598 1024  377  135.200   -0.355   2.565
*91.189.89.198   17.253.108.125   2 u  204 1024  373   65.801    1.043   1.555"""

IDENTITY_PARSER_PAIR_2 = (NTPQ_NP_OUT_2,  """remote           refid      st t when poll reach   delay   offset  jitter
==============================================================================
 0.ubuntu.pool.n .POOL.          16 p    -   64    0    0.000    0.000   0.000
 1.ubuntu.pool.n .POOL.          16 p    -   64    0    0.000    0.000   0.000
 2.ubuntu.pool.n .POOL.          16 p    -   64    0    0.000    0.000   0.000
 3.ubuntu.pool.n .POOL.          16 p    -   64    0    0.000    0.000   0.000
 ntp.ubuntu.com  .POOL.          16 p    -   64    0    0.000    0.000   0.000
+162.159.200.1   10.48.8.4        3 u  537 1024  377   56.460    0.514   1.671
+162.159.200.123 10.48.8.4        3 u  442 1024  377   55.800   -0.263   1.494
-91.189.94.4     17.253.34.125    2 u  336 1024  377   64.089   -1.326  12.686
+91.189.91.157   132.163.96.1     2 u  598 1024  377  135.200   -0.355   2.565
*91.189.89.198   17.253.108.125   2 u  204 1024  373   65.801    1.043   1.555""")

NTPQ_NP_OUT_3 = """ntpq: read: Connection refused"""

IDENTITY_PARSER_PAIR_3 = (NTPQ_NP_OUT_3, NTPQ_NP_OUT_3)

NTPQ_NP_OUT_4 =  """     remote           refid      st t when poll reach   delay   offset  jitter
==============================================================================
 0.ubuntu.pool.n .POOL.          16 p    -   64    0    0.000    0.000   0.000
 1.ubuntu.pool.n .POOL.          16 p    -   64    0    0.000    0.000   0.000
 2.ubuntu.pool.n .POOL.          16 p    -   64    0    0.000    0.000   0.000
 3.ubuntu.pool.n .POOL.          16 p    -   64    0    0.000    0.000   0.000
 ntp.ubuntu.com  .POOL.          16 p    -   64    0    0.000    0.000   0.000
+162.159.200.1   10.48.8.4        3 u  537 1024  377   56.460    0.514   1.671
+162.159.200.123 10.48.8.4        3 u  442 1024  377   55.800   -0.263   1.494
-91.189.94.4     17.253.34.125    2 u  336 1024  377   64.089   -1.326  12.686
+91.189.91.157   132.163.96.1     2 u  598 1024  377  135.200   -0.355   2.565
*91.189.89.198   17.253.108.125   2 u  204 1024  373   65.801    1.043   1.555"""

IDENTITY_PARSER_PAIR_4 = (NTPQ_NP_OUT_4, """remote           refid      st t when poll reach   delay   offset  jitter
==============================================================================
 0.ubuntu.pool.n .POOL.          16 p    -   64    0    0.000    0.000   0.000
 1.ubuntu.pool.n .POOL.          16 p    -   64    0    0.000    0.000   0.000
 2.ubuntu.pool.n .POOL.          16 p    -   64    0    0.000    0.000   0.000
 3.ubuntu.pool.n .POOL.          16 p    -   64    0    0.000    0.000   0.000
 ntp.ubuntu.com  .POOL.          16 p    -   64    0    0.000    0.000   0.000
+162.159.200.1   10.48.8.4        3 u  537 1024  377   56.460    0.514   1.671
+162.159.200.123 10.48.8.4        3 u  442 1024  377   55.800   -0.263   1.494
-91.189.94.4     17.253.34.125    2 u  336 1024  377   64.089   -1.326  12.686
+91.189.91.157   132.163.96.1     2 u  598 1024  377  135.200   -0.355   2.565
*91.189.89.198   17.253.108.125   2 u  204 1024  373   65.801    1.043   1.555""")

NTPQ_NP_OUT_5 = """     remote           refid      st t when poll reach   delay   offset  jitter
==============================================================================
 0.ubuntu.pool.n .POOL.          16 p    -   64    0    0.000    0.000   0.000
 1.ubuntu.pool.n .POOL.          16 p    -   64    0    0.000    0.000   0.000
 2.ubuntu.pool.n .POOL.          16 p    -   64    0    0.000    0.000   0.000
 3.ubuntu.pool.n .POOL.          16 p    -   64    0    0.000    0.000   0.000
 ntp.ubuntu.com  .POOL.          16 p    -   64    0    0.000    0.000   0.000
+162.159.200.1   10.48.8.4        3 u  537 1024  377   56.460    0.514   1.671
+162.159.200.123 10.48.8.4        3 u  442 1024  377   55.800   -0.263   1.494
-91.189.94.4     17.253.34.125    2 u  336 1024  377   64.089   -1.326  12.686
+91.189.91.157   132.163.96.1     2 u  598 1024  377  135.200   -0.355   2.565
 8.8.8.8         17.253.108.125   2 u  204 1024  373   65.801    1.043   1.555"""

IDENTITY_PARSER_PAIR_5 = (NTPQ_NP_OUT_5,  """remote           refid      st t when poll reach   delay   offset  jitter
==============================================================================
 0.ubuntu.pool.n .POOL.          16 p    -   64    0    0.000    0.000   0.000
 1.ubuntu.pool.n .POOL.          16 p    -   64    0    0.000    0.000   0.000
 2.ubuntu.pool.n .POOL.          16 p    -   64    0    0.000    0.000   0.000
 3.ubuntu.pool.n .POOL.          16 p    -   64    0    0.000    0.000   0.000
 ntp.ubuntu.com  .POOL.          16 p    -   64    0    0.000    0.000   0.000
+162.159.200.1   10.48.8.4        3 u  537 1024  377   56.460    0.514   1.671
+162.159.200.123 10.48.8.4        3 u  442 1024  377   55.800   -0.263   1.494
-91.189.94.4     17.253.34.125    2 u  336 1024  377   64.089   -1.326  12.686
+91.189.91.157   132.163.96.1     2 u  598 1024  377  135.200   -0.355   2.565
 8.8.8.8         17.253.108.125   2 u  204 1024  373   65.801    1.043   1.555""")

#SSH command: clish -c "show configuration ntp"
SHOW_CONFIG_NTP_OUT_1 = """set ntp active off"""

IS_NTP_SERVICE_ENABLED_PAIR_1 = (SHOW_CONFIG_NTP_OUT_1, False)

IS_NTP_WELL_CONFIGURED_PAIR_1 = (SHOW_CONFIG_NTP_OUT_1, False)

SHOW_CONFIG_NTP_OUT_2 = """set ntp active on
        set ntp server primary time.google.com version 4
        set ntp server secondary hora.roa.es version 1"""

IS_NTP_SERVICE_ENABLED_PAIR_2 = (SHOW_CONFIG_NTP_OUT_2, True)

IS_NTP_WELL_CONFIGURED_PAIR_2 = (SHOW_CONFIG_NTP_OUT_2, True)

#SSH command: nslookup
NSLOOKUP_OUT_1 =  """Server:		8.8.8.8
        Address:	8.8.8.8#53

        Non-authoritative answer:
        Name:	time.google.com
        Address: 216.239.35.4
        Name:	time.google.com
        Address: 216.239.35.8
        Name:	time.google.com
        Address: 216.239.35.0
        Name:	time.google.com
        Address: 216.239.35.12

        """

IS_NTP_SERVER_NAME_RESOLVED_PAIR_1 = (NSLOOKUP_OUT_1, True)

NSLOOKUP_OUT_2 = """Server:		8.8.8.8
Address:	8.8.8.8#53

** server can't find 8.8.8.8: NXDOMAIN

"""

IS_NTP_SERVER_NAME_RESOLVED_PAIR_2 = (NSLOOKUP_OUT_2, False)

#SSH command: ip r get
IP_R_GET_OUT_1 ="""8.8.8.8 via 10.11.94.254 dev eth0  src 10.11.94.202 
            cache  ipid 0x631d mtu 1500 advmss 1460 hoplimit 64"""

NTP_INTERFACE_NAME_PAIR_1 = (IP_R_GET_OUT_1, "eth0")

IP_R_GET_OUT_2 = """RTNETLINK answers: Invalid argument"""

NTP_INTERFACE_NAME_PAIR_2 = (IP_R_GET_OUT_2, "")

#SSH command: ifconfig
IFCONFIG_OUT_1 = """eth0        Link encap:Ethernet  HWaddr 00:50:56:AC:46:D1  
                    inet addr:10.10.1.1  Bcast:10.10.1.255  Mask:255.255.255.0
                    UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
                    RX packets:220249739 errors:0 dropped:0 overruns:0 frame:0
                    TX packets:11184300 errors:0 dropped:0 overruns:0 carrier:0
                    collisions:0 txqueuelen:1000 
                    RX bytes:18931341302 (17.6 GiB)  TX bytes:905533148 (863.5 MiB)"""

IS_INTERFACE_ADMIN_STATE_UP_PAIR_1 = (IFCONFIG_OUT_1, True)

IFCONFIG_OUT_2 = """eth0        Link encap:Ethernet  HWaddr 00:50:56:AC:46:D1  
                    inet addr:10.10.1.1  Bcast:10.10.1.255  Mask:255.255.255.0
                    BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
                    RX packets:220249739 errors:0 dropped:0 overruns:0 frame:0
                    TX packets:11184300 errors:0 dropped:0 overruns:0 carrier:0
                    collisions:0 txqueuelen:1000 
                    RX bytes:18931341302 (17.6 GiB)  TX bytes:905533148 (863.5 MiB)"""

IS_INTERFACE_ADMIN_STATE_UP_PAIR_2 = (IFCONFIG_OUT_2, False)

#SSH command: ip link show
IP_LINK_SHOW_1 = """ip link show bond0
10: bond0: <BROADCAST,MULTICAST,MASTER,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP mode DEFAULT qlen 1000
    link/ether 00:50:56:91:99:be brd ff:ff:ff:ff:ff:ff
"""

IS_INTERFACE_LINK_STATE_UP_PAIR_1 = (IP_LINK_SHOW_1, True)

IP_LINK_SHOW_2 = """ip link show bond0
10: bond0: <NO-CARRIER,BROADCAST,MULTICAST,MASTER,UP> mtu 1500 qdisc noqueue state DOWN mode DEFAULT qlen 1000
    link/ether 00:50:56:91:99:be brd ff:ff:ff:ff:ff:ff
"""

IS_INTERFACE_LINK_STATE_UP_PAIR_2 = (IP_LINK_SHOW_2, False)

#SSH command: ping
PING_OUT_1 = """PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
        64 bytes from 8.8.8.8: icmp_seq=1 ttl=51 time=57.4 ms
        64 bytes from 8.8.8.8: icmp_seq=2 ttl=51 time=57.4 ms
        64 bytes from 8.8.8.8: icmp_seq=3 ttl=51 time=57.3 ms
        64 bytes from 8.8.8.8: icmp_seq=4 ttl=51 time=57.3 ms
        64 bytes from 8.8.8.8: icmp_seq=5 ttl=51 time=57.5 ms
        64 bytes from 8.8.8.8: icmp_seq=6 ttl=51 time=57.4 ms
        64 bytes from 8.8.8.8: icmp_seq=7 ttl=51 time=57.4 ms
        64 bytes from 8.8.8.8: icmp_seq=8 ttl=51 time=57.4 ms
        64 bytes from 8.8.8.8: icmp_seq=9 ttl=51 time=61.6 ms
        64 bytes from 8.8.8.8: icmp_seq=10 ttl=51 time=58.2 ms

        --- 8.8.8.8 ping statistics ---
        10 packets transmitted, 10 received, 0% packet loss, time 9003ms
        rtt min/avg/max/mdev = 57.354/57.939/61.624/1.273 ms
        """

IS_NTP_SERVER_REACHABLE_PAIR_1 = (PING_OUT_1, True)

PING_OUT_2 =  """PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.

        --- 8.8.8.8 ping statistics ---
        10 packets transmitted, 0 received, 100% packet loss, time 8999ms

        """

IS_NTP_SERVER_REACHABLE_PAIR_2 = (PING_OUT_2, False)

#SSH command: ntpdate
NTPDATE_OUT_1 = """22 Jan 15:29:32 ntpdate[8170]: ntpdate 4.2.2p1@1.1570-? Tue Sep  4 13:14:40 UTC 2018 (1)
        Looking for host 216.239.35.0 and service ntp
        host found : time1.google.com
        transmit(216.239.35.0)
        receive(216.239.35.0)
        transmit(216.239.35.0)
        receive(216.239.35.0)
        transmit(216.239.35.0)
        receive(216.239.35.0)
        transmit(216.239.35.0)
        receive(216.239.35.0)
        transmit(216.239.35.0)
        server 216.239.35.0, port 123
        stratum 1, precision -20, leap 00, trust 000
        refid [GOOG], delay 0.08539, dispersion 0.00073
        transmitted 4, in filter 4
        reference time:    e1d2cd70.0922affd  Wed, Jan 22 2020 15:32:32.035
        originate timestamp: e1d2cd70.0922b000  Wed, Jan 22 2020 15:32:32.035
        transmit timestamp:  e1d2ccbc.78e39b45  Wed, Jan 22 2020 15:29:32.472
        filter delay:  0.08557  0.08684  0.08871  0.08539 
                 0.00000  0.00000  0.00000  0.00000 
        filter offset: 179.5345 179.5338 179.5320 179.5335
                 0.000000 0.000000 0.000000 0.000000
        delay 0.08539, dispersion 0.00073
        offset 179.533577

        22 Jan 15:29:32 ntpdate[8170]: step time server 216.239.35.0 offset 179.533577 sec
        """

IS_NTP_SERVICE_UP_AND_RUNNING_PAIR_1 = ((NTPDATE_OUT_1, "216.239.35.0"), False )

NTPDATE_OUT_2 = """22 Jan 15:30:49 ntpdate[9594]: ntpdate 4.2.2p1@1.1570-? Tue Sep  4 13:14:40 UTC 2018 (1)
        Looking for host 8.8.8.8 and service ntp
        host found : dns.google
        transmit(8.8.8.8)
        transmit(8.8.8.8)
        transmit(8.8.8.8)
        transmit(8.8.8.8)
        transmit(8.8.8.8)
        8.8.8.8: Server dropped: no data
        server 8.8.8.8, port 123
        stratum 0, precision 0, leap 00, trust 000
        refid [8.8.8.8], delay 0.00000, dispersion 64.00000
        transmitted 4, in filter 4
        reference time:    00000000.00000000  Thu, Feb  7 2036  8:28:16.000
        originate timestamp: 00000000.00000000  Thu, Feb  7 2036  8:28:16.000
        transmit timestamp:  e1d2cd0c.f238433d  Wed, Jan 22 2020 15:30:52.946
        filter delay:  0.00000  0.00000  0.00000  0.00000 
                 0.00000  0.00000  0.00000  0.00000 
        filter offset: 0.000000 0.000000 0.000000 0.000000
                 0.000000 0.000000 0.000000 0.000000
        delay 0.00000, dispersion 64.00000
        offset 0.000000

        22 Jan 15:30:53 ntpdate[9594]: no server suitable for synchronization found
        """

IS_NTP_SERVICE_UP_AND_RUNNING_PAIR_2 = ((NTPDATE_OUT_2, "8.8.8.8"), False )

#Logic blocks pairs
IS_NTP_UP_PAIR_1 = (NTPQ_NP_OUT_1, False)

IS_NTP_UP_PAIR_2 = (NTPQ_NP_OUT_2, True)

IS_NTP_UP_PAIR_3 = (NTPQ_NP_OUT_3, False)

IS_NTP_UP_PAIR_4 = (NTPQ_NP_OUT_4, True)

IS_NTP_UP_PAIR_5 = (NTPQ_NP_OUT_5, True)

IS_NTP_SYNCHRONIZED_PAIR_1 = (NTPQ_NP_OUT_1, False)

IS_NTP_SYNCHRONIZED_PAIR_2 = (NTPQ_NP_OUT_2, True)

IS_NTP_SYNCHRONIZED_PAIR_3 = (NTPQ_NP_OUT_3, False)

IS_NTP_SYNCHRONIZED_PAIR_4 = (NTPQ_NP_OUT_4, True)

IS_NTP_SYNCHRONIZED_PAIR_5 = (NTPQ_NP_OUT_5, False)



