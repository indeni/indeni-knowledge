import unittest
import automation.workflows.all_devices_ntp_not_syncing.checkpoint.all_devices_ntp_not_syncing_mock_data as mock
import automation.workflows.all_devices_ntp_not_syncing.checkpoint.all_devices_ntp_not_syncing_parser as parser


class NTPNotSyncingTests(unittest.TestCase):


    def test_identity_parser(self):
        self.assertEqual.__self__.maxDiff = None
        pair1 = mock.IDENTITY_PARSER_PAIR_1
        pair2 = mock.IDENTITY_PARSER_PAIR_2
        pair3 = mock.IDENTITY_PARSER_PAIR_3
        pair4 = mock.IDENTITY_PARSER_PAIR_4
        pair5 = mock.IDENTITY_PARSER_PAIR_5
        self.assertEqual(pair1[1], parser.identity_parser(pair1[0]))
        self.assertEqual(pair2[1], parser.identity_parser(pair2[0]))
        self.assertEqual(pair3[1], parser.identity_parser(pair3[0]))
        self.assertEqual(pair4[1], parser.identity_parser(pair4[0]))
        self.assertEqual(pair5[1], parser.identity_parser(pair5[0]))

    def test_is_ntp_up(self):
        pair1 = mock.IS_NTP_UP_PAIR_1
        pair2 = mock.IS_NTP_UP_PAIR_2
        pair3 = mock.IS_NTP_UP_PAIR_3
        pair4 = mock.IS_NTP_UP_PAIR_4
        pair5 = mock.IS_NTP_UP_PAIR_5
        self.assertEqual(pair1[1], parser.is_ntp_up(ntp_result=pair1[0]))
        self.assertEqual(pair2[1], parser.is_ntp_up(ntp_result=pair2[0]))
        self.assertEqual(pair3[1], parser.is_ntp_up(ntp_result=pair3[0]))
        self.assertEqual(pair4[1], parser.is_ntp_up(ntp_result=pair4[0]))
        self.assertEqual(pair5[1], parser.is_ntp_up(ntp_result=pair5[0]))

    def test_is_ntp_synchronized(self):
        pair1 = mock.IS_NTP_SYNCHRONIZED_PAIR_1
        pair2 = mock.IS_NTP_SYNCHRONIZED_PAIR_2
        pair3 = mock.IS_NTP_SYNCHRONIZED_PAIR_3
        pair4 = mock.IS_NTP_SYNCHRONIZED_PAIR_4
        pair5 = mock.IS_NTP_SYNCHRONIZED_PAIR_5
        self.assertEqual(pair1[1], parser.is_ntp_synchronized(ntp_result=pair1[0]))
        self.assertEqual(pair2[1], parser.is_ntp_synchronized(ntp_result=pair2[0]))
        self.assertEqual(pair3[1], parser.is_ntp_synchronized(ntp_result=pair3[0]))
        self.assertEqual(pair4[1], parser.is_ntp_synchronized(ntp_result=pair4[0]))
        self.assertEqual(pair5[1], parser.is_ntp_synchronized(ntp_result=pair5[0]))

    def test_is_ntp_service_enabled(self):
        pair1 = mock.IS_NTP_SERVICE_ENABLED_PAIR_1
        pair2 = mock.IS_NTP_SERVICE_ENABLED_PAIR_2
        self.assertEqual(pair1[1], parser.is_ntp_service_enabled(pair1[0]))
        self.assertEqual(pair2[1], parser.is_ntp_service_enabled(pair2[0]))

    def test_is_ntp_server_name_resolved(self):
        pair1 = mock.IS_NTP_SERVER_NAME_RESOLVED_PAIR_1
        pair2 = mock.IS_NTP_SERVER_NAME_RESOLVED_PAIR_2
        self.assertEqual(pair1[1], parser.is_ntp_server_name_resolved(pair1[0]))
        self.assertEqual(pair2[1], parser.is_ntp_server_name_resolved(pair2[0]))

    def test_parse_ntp_interface_name(self):
        pair1 = mock.NTP_INTERFACE_NAME_PAIR_1
        pair2 = mock.NTP_INTERFACE_NAME_PAIR_2
        self.assertEqual(pair1[1], parser.parse_ntp_interface_name(pair1[0]))
        self.assertEqual(pair2[1], parser.parse_ntp_interface_name(pair2[0]))

    def test_is_interface_admin_state_up(self):
        pair1 = mock.IS_INTERFACE_ADMIN_STATE_UP_PAIR_1
        pair2 = mock.IS_INTERFACE_ADMIN_STATE_UP_PAIR_2
        self.assertEqual(pair1[1], parser.is_interface_admin_state_up(pair1[0]))
        self.assertEqual(pair2[1], parser.is_interface_admin_state_up(pair2[0]))

    def test_is_interface_link_state_up(self):
        pair1 = mock.IS_INTERFACE_LINK_STATE_UP_PAIR_1
        pair2 = mock.IS_INTERFACE_LINK_STATE_UP_PAIR_2
        self.assertEqual(pair1[1], parser.is_interface_link_state_up(pair1[0]))
        self.assertEqual(pair2[1], parser.is_interface_link_state_up(pair2[0]))      

    def test_is_ntp_server_reachable(self):
        pair1 = mock.IS_NTP_SERVER_REACHABLE_PAIR_1
        pair2 = mock.IS_NTP_SERVER_REACHABLE_PAIR_2
        self.assertEqual(pair1[1], parser.is_ntp_server_reachable(pair1[0]))
        self.assertEqual(pair2[1], parser.is_ntp_server_reachable(pair2[0]))       

    def test_is_ntp_service_up_and_running(self):
        pair1 = mock.IS_NTP_SERVICE_UP_AND_RUNNING_PAIR_1
        pair2 = mock.IS_NTP_SERVICE_UP_AND_RUNNING_PAIR_2
        self.assertEqual(pair1[1], parser.is_ntp_service_up_and_running(pair1[0][0],ntp_ip_address=pair1[0][1]))
        self.assertEqual(pair2[1], parser.is_ntp_service_up_and_running(pair2[0][0],ntp_ip_address=pair2[0][1]))  

    def test_is_ntp_well_configured(self):
        pair1 = mock.IS_NTP_WELL_CONFIGURED_PAIR_1
        pair2 = mock.IS_NTP_WELL_CONFIGURED_PAIR_2
        self.assertEqual(pair1[1], parser.is_ntp_well_configured(pair1[0]))
        self.assertEqual(pair2[1], parser.is_ntp_well_configured(pair2[0]))       


if __name__ == '__main__':
    unittest.main()
