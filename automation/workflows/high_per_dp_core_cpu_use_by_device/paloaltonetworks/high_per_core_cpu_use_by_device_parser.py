from parser_service.public.helper_methods import *
import re


def check_variant_matches(**kwargs) -> bool:
    # This should work on all gateways
    tags = kwargs.get('my_device_tags', None)
    if tags is None:
        return False
    if tags.get('product') != 'firewall':
        return False
    return True


def panos_textfsm_show_running_monitor(raw_data:str) -> list:
    if raw_data:
        raw_data_parsed = parse_data_as_xml(raw_data)
        load_avgs = []
        load_maxs = []
        data_to_return =['', '', '', '', '']
        for core_load_avg in raw_data_parsed['response']['result']['resource-monitor']['data-processors']['dp0']['minute']['cpu-load-average']['entry']:
            load_avgs.append( [x for x in core_load_avg['value'].split(',')])
                #load_avgs[int(core_load_avg['coreid'])] = core_load_avg['value'].split(',')
        for core_load_max in raw_data_parsed['response']['result']['resource-monitor']['data-processors']['dp0']['minute']['cpu-load-maximum']['entry']:
            load_maxs.append( [x for x in core_load_max['value'].split(',')])
            #load_maxs[int(core_load_max['coreid'])] = core_load_max['value'].split(',')
        for minute in range(len(load_avgs[0])):
            for core_id in range(len(load_avgs)):
                if len(data_to_return[minute]) == 0:
                    data_to_return[minute]={'MINUTE': ['{} {}'.format(load_avgs[core_id][minute], load_maxs[core_id][minute])]}
                else:
                    data_to_return[minute] = {'MINUTE': ['{} {} {}'.format(data_to_return[minute]['MINUTE'][0], load_avgs[core_id][minute], load_maxs[core_id][minute])]}

    return data_to_return

def get_cpu_higher_usage(cpu_usage:list, cpu_name:str) -> dict:
    higher_usage = {}
    if ':' in cpu_name:
        cpu_name_split = cpu_name.split(':')
        higher_usage['dp'] = cpu_name_split[0]
        higher_usage['cpu_id'] = int(cpu_name_split[1])
    else:
        higher_usage['dp'] = cpu_name
        higher_usage['cpu_id'] = 0
    higher_usage['utilization'] = 0
    for minute_values in cpu_usage:
        cpu_utilization_series = minute_values["MINUTE"]
        for cpu_utilization_value in cpu_utilization_series:
            cpu_utilization_per_minute = re.split('\s+', cpu_utilization_value)
            cpu_utilization_per_minute_per_core_avg = cpu_utilization_per_minute[::2]
            cpu_count = 0
            utilization_perc = str
            for utilization_perc in cpu_utilization_per_minute_per_core_avg:
                if cpu_count == int(higher_usage['cpu_id']):
                    if (utilization_perc !=  "*" ):
                        if int(utilization_perc) >= higher_usage['utilization'] :
                            higher_usage['utilization'] = int(utilization_perc)
                cpu_count+=1
    return higher_usage

def get_session_utilization(raw_data: str) -> str:
    if raw_data:
        raw_data_parsed = parse_data_as_xml(raw_data)
        return "{:.2f}".format(int(raw_data_parsed['response']['result']['num-installed'])*100/int(raw_data_parsed['response']['result']['num-max']))

def panos_textfsm_debug_dataplane_pool_statistics(raw_data: str) -> list:
    raw_data_parsed = parse_data_as_list(raw_data, 'paloalto_panos_debug_dataplane_pool_statistics.textfsm')
    return raw_data_parsed


def get_buffer_usage(buffer_usage:list,dataplane:dict) -> float:
    usage = 0
    for dataplane_data in buffer_usage:
        if dataplane_data['DP'] == dataplane['dp']:
            buffer_now = int(dataplane_data['PKT_BUFFER_NOW'])
            buffer_max = int(dataplane_data['PKT_BUFFER_MAX'])
            utilization_perc = buffer_now/buffer_max
            usage = round(utilization_perc, 5)
    return usage
