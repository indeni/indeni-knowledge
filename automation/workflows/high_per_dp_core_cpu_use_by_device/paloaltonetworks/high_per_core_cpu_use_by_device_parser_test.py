import unittest
import automation.workflows.high_per_dp_core_cpu_use_by_device.paloaltonetworks.high_per_core_cpu_use_by_device_mock_data as mock
import automation.workflows.high_per_dp_core_cpu_use_by_device.paloaltonetworks.high_per_core_cpu_use_by_device_parser as parser


class HighPerCoreCPUParserTests(unittest.TestCase):


    def test_panos_textfsm_show_running_monitor(self):
        pair1 = mock.PANOS_TEXTFSM_SHOW_RUNNING_MONITOR_1
        pair2 = mock.PANOS_TEXTFSM_SHOW_RUNNING_MONITOR_2
        self.assertEqual(pair1[1], parser.panos_textfsm_show_running_monitor(pair1[0]))
        self.assertEqual(pair2[1], parser.panos_textfsm_show_running_monitor(pair2[0]))

    def test_get_cpu_higher_usage(self):
        pair1 = mock.CHECK_GET_CPU_HIGHER_USAGE_1
        pair2 = mock.CHECK_GET_CPU_HIGHER_USAGE_2
        pair3 = mock.CHECK_GET_CPU_HIGHER_USAGE_3
        self.assertEqual(pair1[1], parser.get_cpu_higher_usage(pair1[0][0],pair1[0][1]))
        self.assertEqual(pair2[1], parser.get_cpu_higher_usage(pair2[0][0],pair2[0][1]))
        self.assertEqual(pair3[1], parser.get_cpu_higher_usage(pair3[0][0],pair3[0][1]))

    def test_get_session_utilization(self):
        pair1 = mock.GET_SESSION_UTILIZATION_PAIR_1
        pair2 = mock.GET_SESSION_UTILIZATION_PAIR_2
        pair3 = mock.GET_SESSION_UTILIZATION_PAIR_3
        self.assertEqual(parser.get_session_utilization (pair1[0]), pair1[1])
        self.assertEqual(parser.get_session_utilization (pair2[0]), pair2[1])
        self.assertEqual(parser.get_session_utilization (pair3[0]), pair3[1])

    def test_panos_textfsm_debug_dataplane_pool_statistics(self):
        pair1 = mock.PANOS_TEXTFSM_DEBUG_DATAPLANE_POOL_STATISTICS_1
        pair2 = mock.PANOS_TEXTFSM_DEBUG_DATAPLANE_POOL_STATISTICS_2
        self.assertEqual(pair1[1], parser.panos_textfsm_debug_dataplane_pool_statistics(pair1[0]))
        self.assertEqual(pair2[1], parser.panos_textfsm_debug_dataplane_pool_statistics(pair2[0]))

    def test_get_buffer_usage(self):
        pair1 = mock.GET_BUFFER_USAGE_1
        self.assertEqual(pair1[1], parser.get_buffer_usage(pair1[0],mock.CHECK_GET_CPU_HIGHER_USAGE_3[1]))


if __name__ == '__main__':
    unittest.main()