#SSH command: show running resource-monitor minute last 5
PANOS_SHOW_RUNNING_RESOURCE_MONITOR_MINUTE_LAST_5_OUT_1 ="""<response status="success">
<result>
<resource-monitor>
<data-processors>
<dp0>
<minute>
<cpu-load-average>
<entry>
<coreid>0</coreid>
<value>0,0,0,0,0</value>
</entry>
<entry>
<coreid>1</coreid>
<value>2,2,2,2,2</value>
</entry>
</cpu-load-average>
<task/>
<cpu-load-maximum>
<entry>
<coreid>0</coreid>
<value>0,0,0,0,0</value>
</entry>
<entry>
<coreid>1</coreid>
<value>3,4,3,4,3</value>
</entry>
</cpu-load-maximum>
<resource-utilization>
<entry>
<name>session (average)</name>
<value>0,0,0,0,0</value>
</entry>
<entry>
<name>session (maximum)</name>
<value>0,0,0,0,0</value>
</entry>
<entry>
<name>packet buffer (average)</name>
<value>0,0,0,0,0</value>
</entry>
<entry>
<name>packet buffer (maximum)</name>
<value>0,0,0,0,0</value>
</entry>
<entry>
<name>packet descriptor (average)</name>
<value>0,0,0,0,0</value>
</entry>
<entry>
<name>packet descriptor (maximum)</name>
<value>0,0,0,0,0</value>
</entry>
<entry>
<name>sw tags descriptor (average)</name>
<value>0,0,0,0,0</value>
</entry>
<entry>
<name>sw tags descriptor (maximum)</name>
<value>0,0,0,0,0</value>
</entry>
</resource-utilization>
</minute>
</dp0>
</data-processors>
</resource-monitor>
</result>
</response>
"""

PANOS_SHOW_RUNNING_RESOURCE_MONITOR_MINUTE_LAST_5_OUT_2 = """<response status="success">
<result>
<resource-monitor>
<data-processors>
<dp0>
<minute>
<cpu-load-average>
<entry>
<coreid>0</coreid>
<value>0,0,0,0,0</value>
</entry>
<entry>
<coreid>1</coreid>
<value>50,46,52,76,51</value>
</entry>
<entry>
<coreid>2</coreid>
<value>49,45,50,85,50</value>
</entry>
<entry>
<coreid>3</coreid>
<value>85,81,86,99,85</value>
</entry>
<entry>
<coreid>4</coreid>
<value>84,81,86,99,86</value>
</entry>
<entry>
<coreid>5</coreid>
<value>90,88,91,99,91</value>
</entry>
</cpu-load-average>
<task/>
<cpu-load-maximum>
<entry>
<coreid>0</coreid>
<value>40,39,42,42,44</value>
</entry>
<entry>
<coreid>1</coreid>
<value>39,38,41,79,43</value>
</entry>
<entry>
<coreid>2</coreid>
<value>74,72,75,75,78</value>
</entry>
<entry>
<coreid>3</coreid>
<value>74,72,75,75,78</value>
</entry>
<entry>
<coreid>4</coreid>
<value>83,81,84,99,86</value>
</entry>
<entry>
<coreid>5</coreid>
<value>83,81,84,85,86</value>
</entry>
</cpu-load-maximum>
<resource-utilization>
<entry>
<name>session (average)</name>
<value>0,0,0,0,0</value>
</entry>
<entry>
<name>session (maximum)</name>
<value>0,0,0,0,0</value>
</entry>
<entry>
<name>packet buffer (average)</name>
<value>0,0,0,0,0</value>
</entry>
<entry>
<name>packet buffer (maximum)</name>
<value>0,0,0,0,0</value>
</entry>
<entry>
<name>packet descriptor (average)</name>
<value>0,0,0,0,0</value>
</entry>
<entry>
<name>packet descriptor (maximum)</name>
<value>0,0,0,0,0</value>
</entry>
<entry>
<name>sw tags descriptor (average)</name>
<value>0,0,0,0,0</value>
</entry>
<entry>
<name>sw tags descriptor (maximum)</name>
<value>0,0,0,0,0</value>
</entry>
</resource-utilization>
</minute>
</dp0>
</data-processors>
</resource-monitor>
</result>
</response>"""

PANOS_TEXTFSM_SHOW_RUNNING_MONITOR_1 = (PANOS_SHOW_RUNNING_RESOURCE_MONITOR_MINUTE_LAST_5_OUT_1,
[{'MINUTE': ['0 0 2 3']
},
{'MINUTE': ['0 0 2 4']
},
{'MINUTE': ['0 0 2 3']
},
{'MINUTE': ['0 0 2 4']
},
{'MINUTE': ['0 0 2 3']
}]
)

PANOS_TEXTFSM_SHOW_RUNNING_MONITOR_2 = (PANOS_SHOW_RUNNING_RESOURCE_MONITOR_MINUTE_LAST_5_OUT_2,
[{'MINUTE': ['0 40 50 39 49 74 85 74 84 83 90 83']
},
{'MINUTE': ['0 39 46 38 45 72 81 72 81 81 88 81']
},
{'MINUTE': ['0 42 52 41 50 75 86 75 86 84 91 84']
},
{'MINUTE': ['0 42 76 79 85 75 99 75 99 99 99 85']
},
{'MINUTE': ['0 44 51 43 50 78 85 78 86 86 91 86']
}]
)



SHOW_SESSION_INFO_UTILIZATION_OUT_1 = ("""
<response status="success">
<result>
<tmo-5gcdelete>15</tmo-5gcdelete>
<tmo-sctpshutdown>60</tmo-sctpshutdown>
<tcp-nonsyn-rej>True</tcp-nonsyn-rej>
<tmo-tcpinit>5</tmo-tcpinit>
<tmo-tcp>3600</tmo-tcp>
<pps>0</pps>
<tmo-tcp-delayed-ack>250</tmo-tcp-delayed-ack>
<num-max>819200</num-max>
<age-scan-thresh>80</age-scan-thresh>
<tmo-tcphalfclosed>120</tmo-tcphalfclosed>
<num-active>1</num-active>
<tmo-sctp>3600</tmo-sctp>
<dis-def>60</dis-def>
<num-mcast>0</num-mcast>
<icmp-unreachable-rate>200</icmp-unreachable-rate>
<tmo-tcptimewait>15</tmo-tcptimewait>
<age-scan-ssf>8</age-scan-ssf>
<tmo-udp>30</tmo-udp>
<vardata-rate>10485760</vardata-rate>
<age-scan-tmo>10</age-scan-tmo>
<dis-sctp>30</dis-sctp>
<dp>*.dp0</dp>
<dis-tcp>90</dis-tcp>
<tcp-reject-siw-thresh>4</tcp-reject-siw-thresh>
<num-udp>0</num-udp>
<tmo-sctpcookie>60</tmo-sctpcookie>
<num-http2-5gc>0</num-http2-5gc>
<tmo-icmp>6</tmo-icmp>
<max-pending-mcast>0</max-pending-mcast>
<age-accel-thresh>80</age-accel-thresh>
<tcp-diff-syn-rej>True</tcp-diff-syn-rej>
<tunnel-accel>True</tunnel-accel>
<num-gtpc>0</num-gtpc>
<oor-action>drop</oor-action>
<tmo-def>30</tmo-def>
<num-predict>0</num-predict>
<age-accel-en>True</age-accel-en>
<age-accel-tsf>2</age-accel-tsf>
<hw-offload>True</hw-offload>
<num-icmp>0</num-icmp>
<num-gtpu-active>0</num-gtpu-active>
<tmo-cp>30</tmo-cp>
<tcp-strict-rst>True</tcp-strict-rst>
<tmo-sctpinit>5</tmo-sctpinit>
<strict-checksum>True</strict-checksum>
<tmo-tcp-unverif-rst>30</tmo-tcp-unverif-rst>
<num-bcast>0</num-bcast>
<ipv6-fw>True</ipv6-fw>
<cps>0</cps>
<num-installed>1</num-installed>
<num-tcp>0</num-tcp>
<dis-udp>60</dis-udp>
<num-sctp-assoc>0</num-sctp-assoc>
<num-sctp-sess>0</num-sctp-sess>
<tcp-reject-siw-enable>False</tcp-reject-siw-enable>
<tmo-tcphandshake>10</tmo-tcphandshake>
<hw-udp-offload>True</hw-udp-offload>
<kbps>0</kbps>
<num-gtpu-pending>0</num-gtpu-pending>
</result>
</response>
""")

SHOW_SESSION_INFO_UTILIZATION_OUT_2 = ("""
<response status="success">
<result>
<tmo-5gcdelete>15</tmo-5gcdelete>
<tmo-sctpshutdown>60</tmo-sctpshutdown>
<tcp-nonsyn-rej>True</tcp-nonsyn-rej>
<tmo-tcpinit>5</tmo-tcpinit>
<tmo-tcp>3600</tmo-tcp>
<pps>0</pps>
<tmo-tcp-delayed-ack>250</tmo-tcp-delayed-ack>
<num-max>819200</num-max>
<age-scan-thresh>80</age-scan-thresh>
<tmo-tcphalfclosed>120</tmo-tcphalfclosed>
<num-active>1</num-active>
<tmo-sctp>3600</tmo-sctp>
<dis-def>60</dis-def>
<num-mcast>0</num-mcast>
<icmp-unreachable-rate>200</icmp-unreachable-rate>
<tmo-tcptimewait>15</tmo-tcptimewait>
<age-scan-ssf>8</age-scan-ssf>
<tmo-udp>30</tmo-udp>
<vardata-rate>10485760</vardata-rate>
<age-scan-tmo>10</age-scan-tmo>
<dis-sctp>30</dis-sctp>
<dp>*.dp0</dp>
<dis-tcp>90</dis-tcp>
<tcp-reject-siw-thresh>4</tcp-reject-siw-thresh>
<num-udp>0</num-udp>
<tmo-sctpcookie>60</tmo-sctpcookie>
<num-http2-5gc>0</num-http2-5gc>
<tmo-icmp>6</tmo-icmp>
<max-pending-mcast>0</max-pending-mcast>
<age-accel-thresh>80</age-accel-thresh>
<tcp-diff-syn-rej>True</tcp-diff-syn-rej>
<tunnel-accel>True</tunnel-accel>
<num-gtpc>0</num-gtpc>
<oor-action>drop</oor-action>
<tmo-def>30</tmo-def>
<num-predict>0</num-predict>
<age-accel-en>True</age-accel-en>
<age-accel-tsf>2</age-accel-tsf>
<hw-offload>True</hw-offload>
<num-icmp>0</num-icmp>
<num-gtpu-active>0</num-gtpu-active>
<tmo-cp>30</tmo-cp>
<tcp-strict-rst>True</tcp-strict-rst>
<tmo-sctpinit>5</tmo-sctpinit>
<strict-checksum>True</strict-checksum>
<tmo-tcp-unverif-rst>30</tmo-tcp-unverif-rst>
<num-bcast>0</num-bcast>
<ipv6-fw>True</ipv6-fw>
<cps>0</cps>
<num-installed>573440</num-installed>
<num-tcp>0</num-tcp>
<dis-udp>60</dis-udp>
<num-sctp-assoc>0</num-sctp-assoc>
<num-sctp-sess>0</num-sctp-sess>
<tcp-reject-siw-enable>False</tcp-reject-siw-enable>
<tmo-tcphandshake>10</tmo-tcphandshake>
<hw-udp-offload>True</hw-udp-offload>
<kbps>0</kbps>
<num-gtpu-pending>0</num-gtpu-pending>
</result>
</response>
""")

SHOW_SESSION_INFO_UTILIZATION_OUT_3 = ("""
<response status="success">
<result>
<tmo-5gcdelete>15</tmo-5gcdelete>
<tmo-sctpshutdown>60</tmo-sctpshutdown>
<tcp-nonsyn-rej>True</tcp-nonsyn-rej>
<tmo-tcpinit>5</tmo-tcpinit>
<tmo-tcp>3600</tmo-tcp>
<pps>0</pps>
<tmo-tcp-delayed-ack>250</tmo-tcp-delayed-ack>
<num-max>819200</num-max>
<age-scan-thresh>80</age-scan-thresh>
<tmo-tcphalfclosed>120</tmo-tcphalfclosed>
<num-active>1</num-active>
<tmo-sctp>3600</tmo-sctp>
<dis-def>60</dis-def>
<num-mcast>0</num-mcast>
<icmp-unreachable-rate>200</icmp-unreachable-rate>
<tmo-tcptimewait>15</tmo-tcptimewait>
<age-scan-ssf>8</age-scan-ssf>
<tmo-udp>30</tmo-udp>
<vardata-rate>10485760</vardata-rate>
<age-scan-tmo>10</age-scan-tmo>
<dis-sctp>30</dis-sctp>
<dp>*.dp0</dp>
<dis-tcp>90</dis-tcp>
<tcp-reject-siw-thresh>4</tcp-reject-siw-thresh>
<num-udp>0</num-udp>
<tmo-sctpcookie>60</tmo-sctpcookie>
<num-http2-5gc>0</num-http2-5gc>
<tmo-icmp>6</tmo-icmp>
<max-pending-mcast>0</max-pending-mcast>
<age-accel-thresh>80</age-accel-thresh>
<tcp-diff-syn-rej>True</tcp-diff-syn-rej>
<tunnel-accel>True</tunnel-accel>
<num-gtpc>0</num-gtpc>
<oor-action>drop</oor-action>
<tmo-def>30</tmo-def>
<num-predict>0</num-predict>
<age-accel-en>True</age-accel-en>
<age-accel-tsf>2</age-accel-tsf>
<hw-offload>True</hw-offload>
<num-icmp>0</num-icmp>
<num-gtpu-active>0</num-gtpu-active>
<tmo-cp>30</tmo-cp>
<tcp-strict-rst>True</tcp-strict-rst>
<tmo-sctpinit>5</tmo-sctpinit>
<strict-checksum>True</strict-checksum>
<tmo-tcp-unverif-rst>30</tmo-tcp-unverif-rst>
<num-bcast>0</num-bcast>
<ipv6-fw>True</ipv6-fw>
<cps>0</cps>
<num-installed>491520</num-installed>
<num-tcp>0</num-tcp>
<dis-udp>60</dis-udp>
<num-sctp-assoc>0</num-sctp-assoc>
<num-sctp-sess>0</num-sctp-sess>
<tcp-reject-siw-enable>False</tcp-reject-siw-enable>
<tmo-tcphandshake>10</tmo-tcphandshake>
<hw-udp-offload>True</hw-udp-offload>
<kbps>0</kbps>
<num-gtpu-pending>0</num-gtpu-pending>
</result>
</response>
""")

GET_SESSION_UTILIZATION_PAIR_1 = (SHOW_SESSION_INFO_UTILIZATION_OUT_1, '0.00')

GET_SESSION_UTILIZATION_PAIR_2 = (SHOW_SESSION_INFO_UTILIZATION_OUT_2, '70.00')

GET_SESSION_UTILIZATION_PAIR_3 = (SHOW_SESSION_INFO_UTILIZATION_OUT_3, '60.00')

#SSH command: debug dataplane pool statistics
PANOS_DEBUG_DATAPLANE_POOL_STATISTICS_OUT_1 = """
DP dp0:


Hardware Pools
[ 0] Packet Buffers : 85940/86016 0x8000000410000000
[ 1] Work Queue Entries : 270272/270336 0x800000041a800000
[ 2] Output Buffers : 1009/1024 0x800000041c900000
[ 3] DFA Result : 4093/4096 0x800000041ca00000
[ 4] Timer Buffers : 4096/4096 0x800000041ce00000
[ 5] PAN_FPA_LWM_POOL : 1024/1024 0x800000041d200000
[ 6] ZIP Commands : 1023/1024 0x800000041d240000
[ 7] PAN_FPA_BLAST_PO : 1024/1024 0x800000041d440000

Software Pools
[ 0] software packet buffer 0 ( 512): 32767/32768 0x800000003d4ed680
[ 1] software packet buffer 1 ( 1024): 32768/32768 0x800000003e50d780
[ 2] software packet buffer 2 ( 2048): 32768/32768 0x800000004052d880
[ 3] software packet buffer 3 (33280): 16384/16384 0x800000004454d980
[ 4] software packet buffer 4 (66048): 304/304 0x8000000064d5da80
[ 5] Shared Pool 24 ( 24): 1750000/1750000 0x8000000066085780
[ 6] Shared Pool 32 ( 32): 2055000/2055000 0x8000000068f40700
[ 7] Shared Pool 40 ( 40): 270000/270000 0x800000006d5ce080
[ 8] Shared Pool 192 ( 192): 2450000/2450000 0x800000006e122800
[ 9] Shared Pool 256 ( 256): 150000/150000 0x800000008ab16e80
[10] ZIP Results ( 184): 1024/1024 0x800000041dd61600
[11] CTD AV Block ( 1024): 32/32 0x800000041de85380
[12] Regex Results (11640): 8192/8192 0x8000000123ebe100
[13] SSH Handshake State ( 6512): 512/512 0x800000013345a280
[14] SSH State ( 3200): 4096/4096 0x8000000133788b80
[15] TCP host connections ( 176): 15/16 0x800000013440cd80

Shared Pools Statistics

User Quota Threshold Min.Alloc Cur.Alloc Max.Alloc Total-Alloc Fail-Thresh Fail-Nomem Data(Pool)-SZ
fptcp_seg 65536 0 0 0 2 2 0 0 16 (24)
inner_decode 8192 0 0 0 0 0 0 0 16 (24)
detector_threat 655360 0 0 0 0 0 0 0 24 (24)
vm_vcheck 524288 0 0 0 0 0 0 0 24 (24)
ctd_patmatch 524288 0 0 0 0 0 0 0 24 (24)
proxy_pktmr 24576 0 0 0 0 0 0 0 16 (24)
vm_field 2097152 0 0 0 32103 59002403 0 0 32 (32)
decode_filter 262144 0 0 0 0 0 0 0 40 (40)
appid_session 524288 0 0 0 11 3150188 0 0 104 (192)
appid_dfa_state 524288 0 0 0 0 0 0 0 184 (192)
cpat_state 131072 1960000 65536 0 0 0 0 0 184 (192)
ctd_flow 524288 0 0 0 80460 3150188 0 0 176 (192)
ctd_flow_state 524288 0 0 0 0 0 0 0 192 (192)
ctd_dlp_flow 131072 1715000 65536 0 0 0 0 0 192 (192)
proxy_flow 49152 0 0 0 0 0 0 0 192 (192)
ssl_hs_st 24576 0 0 0 0 0 0 0 192 (192)
ssl_key_block 49152 0 0 0 0 0 0 0 192 (192)
ssl_st 49152 0 0 0 0 0 0 0 192 (192)
ssl_hs_mac 54067 0 0 0 0 0 0 0 variable
timer_chunk 131072 0 0 0 1 1 0 0 256 (256)
hash_decode 16384 0 0 0 0 0 0 0 104 (192)

Memory Pool Size 397485KB, start address 0x8000000000000000
alloc size 72219, max 12945179
fixed buf allocator, size 407022008
sz allocator, page size 32768, max alloc 4096 quant 64
pool 0 element size 64 avail list 1 full list 1
pool 1 element size 128 avail list 8 full list 0
pool 2 element size 192 avail list 1 full list 0
pool 3 element size 256 avail list 1 full list 0
pool 4 element size 320 avail list 1 full list 0
pool 5 element size 384 avail list 1 full list 0
pool 10 element size 704 avail list 1 full list 0
pool 16 element size 1088 avail list 1 full list 0
parent allocator
alloc size 551528, max 20998760
malloc allocator
current usage 557056 max. usage 21004288, free chunks 12404, total chunks 12421

Mem-Pool-Type MAX.SZ(B) Threshold Min.Alloc Cur.SZ(B) Cur.Alloc Total-Alloc Fail-Thresh Fail-Nomem Local-Reuse(cache)
ctd_dlp_buf 8323072 203512720 4161536 0 0 0 0 0 0 (0)
sml_regfile 113246208 0 0 0 0 6300376 0 0 232118 (8)
proxy 158166592 0 0 65155 803 1642 0 0 0 (0)
l7_data 2777088 0 0 0 0 0 0 0 0 (0)
l7_misc 93208648 284917808 46604324 0 0 0 0 0 0 (0)
cfg_name_cache 566272 284917808 283136 0 0 0 0 0 0 (0)
scantracker 491520 244215264 245760 0 0 0 0 0 0 (0)
appinfo 14155776 284917808 7077888 0 0 0 0 0 0 (0)
dns 2097152 284917808 1048576 0 0 0 0 0 0 (0)

Cache-Type MAX-Entries Cur-Entries Cur.SZ(B) Insert-Failure Mem-Pool-Type
ssl_server_cert 16384 0 0 0 l7_misc
ssl_cert_cn 25000 0 0 0 l7_misc
ssl_cert_cache 1024 0 0 0 proxy
ssl_sess_cache 10000 0 0 0 proxy
proxy_exclude 1024 0 0 0 proxy
proxy_notify 8192 0 0 0 proxy
ctd_block_answer 16384 0 0 0 l7_misc
username_cache 4096 0 0 0 cfg_name_cache
threatname_cache 4096 0 0 0 cfg_name_cache
hipname_cache 256 0 0 0 cfg_name_cache
ctd_cp 16384 0 0 0 l7_misc
ctd_driveby 4096 0 0 0 l7_misc
ctd_pcap 1024 0 0 0 l7_misc
ctd_sml 8192 0 0 0 l7_misc
ctd_url 524288 0 0 0 l7_misc
app_tracker 65536 0 0 0 l7_misc
threat_tracker 4096 0 0 0 l7_misc
scan_tracker 4096 0 0 0 scantracker
app_info 65536 0 0 0 appinfo
dns_v4 10000 0 0 0 dns
dns_v6 10000 0 0 0 dns
dns_id 1024 0 0 0 dns
tcp_mcb 8256 0 0 0 l7_misc


DP dp1:


Hardware Pools
[ 0] Packet Buffers : 262064/262144 0x8000000020c00000
[ 1] Work Queue Entries : 491452/491520 0x8000000410000000
[ 2] Output Buffers : 1009/1024 0x8000000413c00000
[ 3] DFA Result : 4093/4096 0x8000000413d00000
[ 4] Timer Buffers : 4096/4096 0x8000000414100000
[ 5] PAN_FPA_LWM_POOL : 1024/1024 0x8000000414500000
[ 6] ZIP Commands : 1023/1024 0x8000000414540000
[ 7] PAN_FPA_BLAST_PO : 1024/1024 0x8000000414740000

Software Pools
[ 0] software packet buffer 0 ( 512): 49151/49152 0x800000007e20c680
[ 1] software packet buffer 1 ( 1024): 49152/49152 0x800000007fa3c780
[ 2] software packet buffer 2 ( 2048): 49152/49152 0x8000000082a6c880
[ 3] software packet buffer 3 (33280): 32768/32768 0x8000000088a9c980
[ 4] software packet buffer 4 (66048): 432/432 0x80000000c9abca80
[ 5] Shared Pool 24 ( 24): 4000000/4000000 0x80000000cb5f4780
[ 6] Shared Pool 32 ( 32): 4200000/4200000 0x80000000d20c4480
[ 7] Shared Pool 40 ( 40): 1050000/1050000 0x80000000db0f6680
[ 8] Shared Pool 192 ( 192): 5470000/5470000 0x80000000ddd05c80
[ 9] Shared Pool 256 ( 256): 150000/150000 0x800000011db79e80
[10] ZIP Results ( 184): 1024/1024 0x8000000417b4a600
[11] CTD AV Block ( 1024): 32/32 0x8000000417c6e380
[12] Regex Results (11640): 8192/8192 0x8000000417c97100
[13] SSH Handshake State ( 6512): 512/512 0x80000001ba9c4280
[14] SSH State ( 3200): 4096/4096 0x80000001bacf2b80
[15] TCP host connections ( 176): 15/16 0x80000001bb976d80

Shared Pools Statistics

User Quota Threshold Min.Alloc Cur.Alloc Max.Alloc Total-Alloc Fail-Thresh Fail-Nomem Data(Pool)-SZ
fptcp_seg 131072 0 0 0 2 2 0 0 16 (24)
inner_decode 28672 0 0 0 0 0 0 0 16 (24)
detector_threat 1048576 0 0 0 0 0 0 0 24 (24)
vm_vcheck 1048576 0 0 0 0 0 0 0 24 (24)
ctd_patmatch 1835008 0 0 0 0 0 0 0 24 (24)
proxy_pktmr 32768 0 0 0 0 0 0 0 16 (24)
vm_field 4194304 0 0 0 137361 106176732 0 0 32 (32)
decode_filter 1048576 0 0 0 0 0 0 0 40 (40)
appid_session 1835008 0 0 0 11 6059456 0 0 104 (192)
appid_dfa_state 1835008 0 0 0 0 0 0 0 184 (192)
cpat_state 458752 4376000 229376 0 0 0 0 0 184 (192)
ctd_flow 1835008 0 0 0 181557 6059456 0 0 176 (192)
ctd_flow_state 1835008 0 0 0 0 0 0 0 192 (192)
ctd_dlp_flow 458752 3829000 229376 0 0 0 0 0 192 (192)
proxy_flow 65536 0 0 0 0 0 0 0 192 (192)
ssl_hs_st 32768 0 0 0 0 0 0 0 192 (192)
ssl_key_block 65536 0 0 0 0 0 0 0 192 (192)
ssl_st 65536 0 0 0 0 0 0 0 192 (192)
ssl_hs_mac 72089 0 0 0 0 0 0 0 variable
timer_chunk 131072 0 0 0 1 1 0 0 256 (256)
hash_decode 16384 0 0 0 0 0 0 0 104 (192)

Memory Pool Size 907309KB, start address 0x8000000000000000
alloc size 72155, max 29120715
fixed buf allocator, size 929081784
sz allocator, page size 32768, max alloc 4096 quant 64
pool 0 element size 64 avail list 1 full list 1
pool 1 element size 128 avail list 5 full list 0
pool 2 element size 192 avail list 1 full list 0
pool 3 element size 256 avail list 1 full list 0
pool 4 element size 320 avail list 1 full list 0
pool 5 element size 384 avail list 1 full list 0
pool 10 element size 704 avail list 1 full list 0
pool 16 element size 1088 avail list 1 full list 0
parent allocator
alloc size 453224, max 46983784
malloc allocator
current usage 458752 max. usage 46989312, free chunks 28339, total chunks 28353

Mem-Pool-Type MAX.SZ(B) Threshold Min.Alloc Cur.SZ(B) Cur.Alloc Total-Alloc Fail-Thresh Fail-Nomem Local-Reuse(cache)
ctd_dlp_buf 29130752 464542608 14565376 0 0 0 0 0 0 (0)
sml_regfile 396361728 0 0 0 0 12118912 0 0 430324 (7)
proxy 178089536 0 0 65171 803 1642 0 0 0 (0)
l7_data 2777088 0 0 0 0 0 0 0 0 (0)
l7_misc 285098056 650359651 142549028 0 0 0 0 0 0 (0)
cfg_name_cache 566272 650359651 283136 0 0 0 0 0 0 (0)
scantracker 491520 557451129 245760 0 0 0 0 0 0 (0)
appinfo 14155776 650359651 7077888 0 0 0 0 0 0 (0)
dns 4194304 650359651 2097152 0 0 0 0 0 0 (0)

Cache-Type MAX-Entries Cur-Entries Cur.SZ(B) Insert-Failure Mem-Pool-Type
ssl_server_cert 16384 0 0 0 l7_misc
ssl_cert_cn 25000 0 0 0 l7_misc
ssl_cert_cache 1024 0 0 0 proxy
ssl_sess_cache 10000 0 0 0 proxy
proxy_exclude 1024 0 0 0 proxy
proxy_notify 8192 0 0 0 proxy
ctd_block_answer 16384 0 0 0 l7_misc
username_cache 4096 0 0 0 cfg_name_cache
threatname_cache 4096 0 0 0 cfg_name_cache
hipname_cache 256 0 0 0 cfg_name_cache
ctd_cp 16384 0 0 0 l7_misc
ctd_driveby 4096 0 0 0 l7_misc
ctd_pcap 1024 0 0 0 l7_misc
ctd_sml 8192 0 0 0 l7_misc
ctd_url 2097152 0 0 0 l7_misc
app_tracker 65536 0 0 0 l7_misc
threat_tracker 4096 0 0 0 l7_misc
scan_tracker 4096 0 0 0 scantracker
app_info 65536 0 0 0 appinfo
dns_v4 20000 0 0 0 dns
dns_v6 20000 0 0 0 dns
dns_id 1024 0 0 0 dns
tcp_mcb 8256 0 0 0 l7_misc


DP dp2:


Hardware Pools
[ 0] Packet Buffers : 262062/262144 0x8000000020c00000
[ 1] Work Queue Entries : 491466/491520 0x8000000410000000
[ 2] Output Buffers : 1009/1024 0x8000000413c00000
[ 3] DFA Result : 4093/4096 0x8000000413d00000
[ 4] Timer Buffers : 4096/4096 0x8000000414100000
[ 5] PAN_FPA_LWM_POOL : 1024/1024 0x8000000414500000
[ 6] ZIP Commands : 1023/1024 0x8000000414540000
[ 7] PAN_FPA_BLAST_PO : 1024/1024 0x8000000414740000

Software Pools
[ 0] software packet buffer 0 ( 512): 49151/49152 0x800000007e20c680
[ 1] software packet buffer 1 ( 1024): 49152/49152 0x800000007fa3c780
[ 2] software packet buffer 2 ( 2048): 49152/49152 0x8000000082a6c880
[ 3] software packet buffer 3 (33280): 32768/32768 0x8000000088a9c980
[ 4] software packet buffer 4 (66048): 432/432 0x80000000c9abca80
[ 5] Shared Pool 24 ( 24): 4000000/4000000 0x80000000cb5f4780
[ 6] Shared Pool 32 ( 32): 4200000/4200000 0x80000000d20c4480
[ 7] Shared Pool 40 ( 40): 1050000/1050000 0x80000000db0f6680
[ 8] Shared Pool 192 ( 192): 5470000/5470000 0x80000000ddd05c80
[ 9] Shared Pool 256 ( 256): 150000/150000 0x800000011db79e80
[10] ZIP Results ( 184): 1024/1024 0x8000000417b4a600
[11] CTD AV Block ( 1024): 32/32 0x8000000417c6e380
[12] Regex Results (11640): 8192/8192 0x8000000417c97100
[13] SSH Handshake State ( 6512): 512/512 0x80000001ba9c4280
[14] SSH State ( 3200): 4096/4096 0x80000001bacf2b80
[15] TCP host connections ( 176): 15/16 0x80000001bb976d80

Shared Pools Statistics

User Quota Threshold Min.Alloc Cur.Alloc Max.Alloc Total-Alloc Fail-Thresh Fail-Nomem Data(Pool)-SZ
fptcp_seg 131072 0 0 0 2 2 0 0 16 (24)
inner_decode 28672 0 0 0 0 0 0 0 16 (24)
detector_threat 1048576 0 0 0 0 0 0 0 24 (24)
vm_vcheck 1048576 0 0 0 0 0 0 0 24 (24)
ctd_patmatch 1835008 0 0 0 0 0 0 0 24 (24)
proxy_pktmr 32768 0 0 0 0 0 0 0 16 (24)
vm_field 4194304 0 0 0 137420 106119556 0 0 32 (32)
decode_filter 1048576 0 0 0 0 0 0 0 40 (40)
appid_session 1835008 0 0 0 11 6070153 0 0 104 (192)
appid_dfa_state 1835008 0 0 0 0 0 0 0 184 (192)
cpat_state 458752 4376000 229376 0 0 0 0 0 184 (192)
ctd_flow 1835008 0 0 0 183682 6070153 0 0 176 (192)
ctd_flow_state 1835008 0 0 0 0 0 0 0 192 (192)
ctd_dlp_flow 458752 3829000 229376 0 0 0 0 0 192 (192)
proxy_flow 65536 0 0 0 0 0 0 0 192 (192)
ssl_hs_st 32768 0 0 0 0 0 0 0 192 (192)
ssl_key_block 65536 0 0 0 0 0 0 0 192 (192)
ssl_st 65536 0 0 0 0 0 0 0 192 (192)
ssl_hs_mac 72089 0 0 0 0 0 0 0 variable
timer_chunk 131072 0 0 0 1 1 0 0 256 (256)
hash_decode 16384 0 0 0 0 0 0 0 104 (192)

Memory Pool Size 907309KB, start address 0x8000000000000000
alloc size 72243, max 29460723
fixed buf allocator, size 929081784
sz allocator, page size 32768, max alloc 4096 quant 64
pool 0 element size 64 avail list 1 full list 1
pool 1 element size 128 avail list 5 full list 0
pool 2 element size 192 avail list 1 full list 0
pool 3 element size 256 avail list 1 full list 0
pool 4 element size 320 avail list 1 full list 0
pool 5 element size 384 avail list 1 full list 0
pool 10 element size 704 avail list 1 full list 0
pool 16 element size 1088 avail list 1 full list 0
parent allocator
alloc size 453224, max 47540840
malloc allocator
current usage 458752 max. usage 47546368, free chunks 28339, total chunks 28353

Mem-Pool-Type MAX.SZ(B) Threshold Min.Alloc Cur.SZ(B) Cur.Alloc Total-Alloc Fail-Thresh Fail-Nomem Local-Reuse(cache)
ctd_dlp_buf 29130752 464542608 14565376 0 0 0 0 0 0 (0)
sml_regfile 396361728 0 0 0 0 12140306 0 0 433491 (8)
proxy 178089536 0 0 65179 803 1642 0 0 0 (0)
l7_data 2777088 0 0 0 0 0 0 0 0 (0)
l7_misc 285098056 650359651 142549028 0 0 0 0 0 0 (0)
cfg_name_cache 566272 650359651 283136 0 0 0 0 0 0 (0)
scantracker 491520 557451129 245760 0 0 0 0 0 0 (0)
appinfo 14155776 650359651 7077888 0 0 0 0 0 0 (0)
dns 4194304 650359651 2097152 0 0 0 0 0 0 (0)

Cache-Type MAX-Entries Cur-Entries Cur.SZ(B) Insert-Failure Mem-Pool-Type
ssl_server_cert 16384 0 0 0 l7_misc
ssl_cert_cn 25000 0 0 0 l7_misc
ssl_cert_cache 1024 0 0 0 proxy
ssl_sess_cache 10000 0 0 0 proxy
proxy_exclude 1024 0 0 0 proxy
proxy_notify 8192 0 0 0 proxy
ctd_block_answer 16384 0 0 0 l7_misc
username_cache 4096 0 0 0 cfg_name_cache
threatname_cache 4096 0 0 0 cfg_name_cache
hipname_cache 256 0 0 0 cfg_name_cache
ctd_cp 16384 0 0 0 l7_misc
ctd_driveby 4096 0 0 0 l7_misc
ctd_pcap 1024 0 0 0 l7_misc
ctd_sml 8192 0 0 0 l7_misc
ctd_url 2097152 0 0 0 l7_misc
app_tracker 65536 0 0 0 l7_misc
threat_tracker 4096 0 0 0 l7_misc
scan_tracker 4096 0 0 0 scantracker
app_info 65536 0 0 0 appinfo
dns_v4 20000 0 0 0 dns
dns_v6 20000 0 0 0 dns
dns_id 1024 0 0 0 dns
tcp_mcb 8256 0 0 0 l7_misc
"""


PANOS_DEBUG_DATAPLANE_POOL_STATISTICS_OUT_2 = """
DP dp0:


Hardware Pools
[ 0] Packet Buffers : 23456/86016 0x8000000410000000
[ 1] Work Queue Entries : 20272/270336 0x800000041a800000
[ 2] Output Buffers : 3/1024 0x800000041c900000
[ 3] DFA Result : 4093/4096 0x800000041ca00000
[ 4] Timer Buffers : 4096/4096 0x800000041ce00000
[ 5] PAN_FPA_LWM_POOL : 1024/1024 0x800000041d200000
[ 6] ZIP Commands : 1023/1024 0x800000041d240000
[ 7] PAN_FPA_BLAST_PO : 1024/1024 0x800000041d440000

Software Pools
[ 0] software packet buffer 0 ( 512): 32767/32768 0x800000003d4ed680
[ 1] software packet buffer 1 ( 1024): 32768/32768 0x800000003e50d780
[ 2] software packet buffer 2 ( 2048): 32768/32768 0x800000004052d880
[ 3] software packet buffer 3 (33280): 16384/16384 0x800000004454d980
[ 4] software packet buffer 4 (66048): 304/304 0x8000000064d5da80
[ 5] Shared Pool 24 ( 24): 1750000/1750000 0x8000000066085780
[ 6] Shared Pool 32 ( 32): 2055000/2055000 0x8000000068f40700
[ 7] Shared Pool 40 ( 40): 270000/270000 0x800000006d5ce080
[ 8] Shared Pool 192 ( 192): 2450000/2450000 0x800000006e122800
[ 9] Shared Pool 256 ( 256): 150000/150000 0x800000008ab16e80
[10] ZIP Results ( 184): 1024/1024 0x800000041dd61600
[11] CTD AV Block ( 1024): 32/32 0x800000041de85380
[12] Regex Results (11640): 8192/8192 0x8000000123ebe100
[13] SSH Handshake State ( 6512): 512/512 0x800000013345a280
[14] SSH State ( 3200): 4096/4096 0x8000000133788b80
[15] TCP host connections ( 176): 15/16 0x800000013440cd80

Shared Pools Statistics

User Quota Threshold Min.Alloc Cur.Alloc Max.Alloc Total-Alloc Fail-Thresh Fail-Nomem Data(Pool)-SZ
fptcp_seg 65536 0 0 0 2 2 0 0 16 (24)
inner_decode 8192 0 0 0 0 0 0 0 16 (24)
detector_threat 655360 0 0 0 0 0 0 0 24 (24)
vm_vcheck 524288 0 0 0 0 0 0 0 24 (24)
ctd_patmatch 524288 0 0 0 0 0 0 0 24 (24)
proxy_pktmr 24576 0 0 0 0 0 0 0 16 (24)
vm_field 2097152 0 0 0 32103 59002403 0 0 32 (32)
decode_filter 262144 0 0 0 0 0 0 0 40 (40)
appid_session 524288 0 0 0 11 3150188 0 0 104 (192)
appid_dfa_state 524288 0 0 0 0 0 0 0 184 (192)
cpat_state 131072 1960000 65536 0 0 0 0 0 184 (192)
ctd_flow 524288 0 0 0 80460 3150188 0 0 176 (192)
ctd_flow_state 524288 0 0 0 0 0 0 0 192 (192)
ctd_dlp_flow 131072 1715000 65536 0 0 0 0 0 192 (192)
proxy_flow 49152 0 0 0 0 0 0 0 192 (192)
ssl_hs_st 24576 0 0 0 0 0 0 0 192 (192)
ssl_key_block 49152 0 0 0 0 0 0 0 192 (192)
ssl_st 49152 0 0 0 0 0 0 0 192 (192)
ssl_hs_mac 54067 0 0 0 0 0 0 0 variable
timer_chunk 131072 0 0 0 1 1 0 0 256 (256)
hash_decode 16384 0 0 0 0 0 0 0 104 (192)

Memory Pool Size 397485KB, start address 0x8000000000000000
alloc size 72219, max 12945179
fixed buf allocator, size 407022008
sz allocator, page size 32768, max alloc 4096 quant 64
pool 0 element size 64 avail list 1 full list 1
pool 1 element size 128 avail list 8 full list 0
pool 2 element size 192 avail list 1 full list 0
pool 3 element size 256 avail list 1 full list 0
pool 4 element size 320 avail list 1 full list 0
pool 5 element size 384 avail list 1 full list 0
pool 10 element size 704 avail list 1 full list 0
pool 16 element size 1088 avail list 1 full list 0
parent allocator
alloc size 551528, max 20998760
malloc allocator
current usage 557056 max. usage 21004288, free chunks 12404, total chunks 12421

Mem-Pool-Type MAX.SZ(B) Threshold Min.Alloc Cur.SZ(B) Cur.Alloc Total-Alloc Fail-Thresh Fail-Nomem Local-Reuse(cache)
ctd_dlp_buf 8323072 203512720 4161536 0 0 0 0 0 0 (0)
sml_regfile 113246208 0 0 0 0 6300376 0 0 232118 (8)
proxy 158166592 0 0 65155 803 1642 0 0 0 (0)
l7_data 2777088 0 0 0 0 0 0 0 0 (0)
l7_misc 93208648 284917808 46604324 0 0 0 0 0 0 (0)
cfg_name_cache 566272 284917808 283136 0 0 0 0 0 0 (0)
scantracker 491520 244215264 245760 0 0 0 0 0 0 (0)
appinfo 14155776 284917808 7077888 0 0 0 0 0 0 (0)
dns 2097152 284917808 1048576 0 0 0 0 0 0 (0)

Cache-Type MAX-Entries Cur-Entries Cur.SZ(B) Insert-Failure Mem-Pool-Type
ssl_server_cert 16384 0 0 0 l7_misc
ssl_cert_cn 25000 0 0 0 l7_misc
ssl_cert_cache 1024 0 0 0 proxy
ssl_sess_cache 10000 0 0 0 proxy
proxy_exclude 1024 0 0 0 proxy
proxy_notify 8192 0 0 0 proxy
ctd_block_answer 16384 0 0 0 l7_misc
username_cache 4096 0 0 0 cfg_name_cache
threatname_cache 4096 0 0 0 cfg_name_cache
hipname_cache 256 0 0 0 cfg_name_cache
ctd_cp 16384 0 0 0 l7_misc
ctd_driveby 4096 0 0 0 l7_misc
ctd_pcap 1024 0 0 0 l7_misc
ctd_sml 8192 0 0 0 l7_misc
ctd_url 524288 0 0 0 l7_misc
app_tracker 65536 0 0 0 l7_misc
threat_tracker 4096 0 0 0 l7_misc
scan_tracker 4096 0 0 0 scantracker
app_info 65536 0 0 0 appinfo
dns_v4 10000 0 0 0 dns
dns_v6 10000 0 0 0 dns
dns_id 1024 0 0 0 dns
tcp_mcb 8256 0 0 0 l7_misc


DP dp1:


Hardware Pools
[ 0] Packet Buffers : 262064/262144 0x8000000020c00000
[ 1] Work Queue Entries : 491452/491520 0x8000000410000000
[ 2] Output Buffers : 1009/1024 0x8000000413c00000
[ 3] DFA Result : 4093/4096 0x8000000413d00000
[ 4] Timer Buffers : 4096/4096 0x8000000414100000
[ 5] PAN_FPA_LWM_POOL : 1024/1024 0x8000000414500000
[ 6] ZIP Commands : 1023/1024 0x8000000414540000
[ 7] PAN_FPA_BLAST_PO : 1024/1024 0x8000000414740000

Software Pools
[ 0] software packet buffer 0 ( 512): 49151/49152 0x800000007e20c680
[ 1] software packet buffer 1 ( 1024): 49152/49152 0x800000007fa3c780
[ 2] software packet buffer 2 ( 2048): 49152/49152 0x8000000082a6c880
[ 3] software packet buffer 3 (33280): 32768/32768 0x8000000088a9c980
[ 4] software packet buffer 4 (66048): 432/432 0x80000000c9abca80
[ 5] Shared Pool 24 ( 24): 4000000/4000000 0x80000000cb5f4780
[ 6] Shared Pool 32 ( 32): 4200000/4200000 0x80000000d20c4480
[ 7] Shared Pool 40 ( 40): 1050000/1050000 0x80000000db0f6680
[ 8] Shared Pool 192 ( 192): 5470000/5470000 0x80000000ddd05c80
[ 9] Shared Pool 256 ( 256): 150000/150000 0x800000011db79e80
[10] ZIP Results ( 184): 1024/1024 0x8000000417b4a600
[11] CTD AV Block ( 1024): 32/32 0x8000000417c6e380
[12] Regex Results (11640): 8192/8192 0x8000000417c97100
[13] SSH Handshake State ( 6512): 512/512 0x80000001ba9c4280
[14] SSH State ( 3200): 4096/4096 0x80000001bacf2b80
[15] TCP host connections ( 176): 15/16 0x80000001bb976d80

Shared Pools Statistics

User Quota Threshold Min.Alloc Cur.Alloc Max.Alloc Total-Alloc Fail-Thresh Fail-Nomem Data(Pool)-SZ
fptcp_seg 131072 0 0 0 2 2 0 0 16 (24)
inner_decode 28672 0 0 0 0 0 0 0 16 (24)
detector_threat 1048576 0 0 0 0 0 0 0 24 (24)
vm_vcheck 1048576 0 0 0 0 0 0 0 24 (24)
ctd_patmatch 1835008 0 0 0 0 0 0 0 24 (24)
proxy_pktmr 32768 0 0 0 0 0 0 0 16 (24)
vm_field 4194304 0 0 0 137361 106176732 0 0 32 (32)
decode_filter 1048576 0 0 0 0 0 0 0 40 (40)
appid_session 1835008 0 0 0 11 6059456 0 0 104 (192)
appid_dfa_state 1835008 0 0 0 0 0 0 0 184 (192)
cpat_state 458752 4376000 229376 0 0 0 0 0 184 (192)
ctd_flow 1835008 0 0 0 181557 6059456 0 0 176 (192)
ctd_flow_state 1835008 0 0 0 0 0 0 0 192 (192)
ctd_dlp_flow 458752 3829000 229376 0 0 0 0 0 192 (192)
proxy_flow 65536 0 0 0 0 0 0 0 192 (192)
ssl_hs_st 32768 0 0 0 0 0 0 0 192 (192)
ssl_key_block 65536 0 0 0 0 0 0 0 192 (192)
ssl_st 65536 0 0 0 0 0 0 0 192 (192)
ssl_hs_mac 72089 0 0 0 0 0 0 0 variable
timer_chunk 131072 0 0 0 1 1 0 0 256 (256)
hash_decode 16384 0 0 0 0 0 0 0 104 (192)

Memory Pool Size 907309KB, start address 0x8000000000000000
alloc size 72155, max 29120715
fixed buf allocator, size 929081784
sz allocator, page size 32768, max alloc 4096 quant 64
pool 0 element size 64 avail list 1 full list 1
pool 1 element size 128 avail list 5 full list 0
pool 2 element size 192 avail list 1 full list 0
pool 3 element size 256 avail list 1 full list 0
pool 4 element size 320 avail list 1 full list 0
pool 5 element size 384 avail list 1 full list 0
pool 10 element size 704 avail list 1 full list 0
pool 16 element size 1088 avail list 1 full list 0
parent allocator
alloc size 453224, max 46983784
malloc allocator
current usage 458752 max. usage 46989312, free chunks 28339, total chunks 28353

Mem-Pool-Type MAX.SZ(B) Threshold Min.Alloc Cur.SZ(B) Cur.Alloc Total-Alloc Fail-Thresh Fail-Nomem Local-Reuse(cache)
ctd_dlp_buf 29130752 464542608 14565376 0 0 0 0 0 0 (0)
sml_regfile 396361728 0 0 0 0 12118912 0 0 430324 (7)
proxy 178089536 0 0 65171 803 1642 0 0 0 (0)
l7_data 2777088 0 0 0 0 0 0 0 0 (0)
l7_misc 285098056 650359651 142549028 0 0 0 0 0 0 (0)
cfg_name_cache 566272 650359651 283136 0 0 0 0 0 0 (0)
scantracker 491520 557451129 245760 0 0 0 0 0 0 (0)
appinfo 14155776 650359651 7077888 0 0 0 0 0 0 (0)
dns 4194304 650359651 2097152 0 0 0 0 0 0 (0)

Cache-Type MAX-Entries Cur-Entries Cur.SZ(B) Insert-Failure Mem-Pool-Type
ssl_server_cert 16384 0 0 0 l7_misc
ssl_cert_cn 25000 0 0 0 l7_misc
ssl_cert_cache 1024 0 0 0 proxy
ssl_sess_cache 10000 0 0 0 proxy
proxy_exclude 1024 0 0 0 proxy
proxy_notify 8192 0 0 0 proxy
ctd_block_answer 16384 0 0 0 l7_misc
username_cache 4096 0 0 0 cfg_name_cache
threatname_cache 4096 0 0 0 cfg_name_cache
hipname_cache 256 0 0 0 cfg_name_cache
ctd_cp 16384 0 0 0 l7_misc
ctd_driveby 4096 0 0 0 l7_misc
ctd_pcap 1024 0 0 0 l7_misc
ctd_sml 8192 0 0 0 l7_misc
ctd_url 2097152 0 0 0 l7_misc
app_tracker 65536 0 0 0 l7_misc
threat_tracker 4096 0 0 0 l7_misc
scan_tracker 4096 0 0 0 scantracker
app_info 65536 0 0 0 appinfo
dns_v4 20000 0 0 0 dns
dns_v6 20000 0 0 0 dns
dns_id 1024 0 0 0 dns
tcp_mcb 8256 0 0 0 l7_misc


DP dp2:


Hardware Pools
[ 0] Packet Buffers : 262062/262144 0x8000000020c00000
[ 1] Work Queue Entries : 491466/491520 0x8000000410000000
[ 2] Output Buffers : 1009/1024 0x8000000413c00000
[ 3] DFA Result : 4093/4096 0x8000000413d00000
[ 4] Timer Buffers : 4096/4096 0x8000000414100000
[ 5] PAN_FPA_LWM_POOL : 1024/1024 0x8000000414500000
[ 6] ZIP Commands : 1023/1024 0x8000000414540000
[ 7] PAN_FPA_BLAST_PO : 1024/1024 0x8000000414740000

Software Pools
[ 0] software packet buffer 0 ( 512): 49151/49152 0x800000007e20c680
[ 1] software packet buffer 1 ( 1024): 49152/49152 0x800000007fa3c780
[ 2] software packet buffer 2 ( 2048): 49152/49152 0x8000000082a6c880
[ 3] software packet buffer 3 (33280): 32768/32768 0x8000000088a9c980
[ 4] software packet buffer 4 (66048): 432/432 0x80000000c9abca80
[ 5] Shared Pool 24 ( 24): 4000000/4000000 0x80000000cb5f4780
[ 6] Shared Pool 32 ( 32): 4200000/4200000 0x80000000d20c4480
[ 7] Shared Pool 40 ( 40): 1050000/1050000 0x80000000db0f6680
[ 8] Shared Pool 192 ( 192): 5470000/5470000 0x80000000ddd05c80
[ 9] Shared Pool 256 ( 256): 150000/150000 0x800000011db79e80
[10] ZIP Results ( 184): 1024/1024 0x8000000417b4a600
[11] CTD AV Block ( 1024): 32/32 0x8000000417c6e380
[12] Regex Results (11640): 8192/8192 0x8000000417c97100
[13] SSH Handshake State ( 6512): 512/512 0x80000001ba9c4280
[14] SSH State ( 3200): 4096/4096 0x80000001bacf2b80
[15] TCP host connections ( 176): 15/16 0x80000001bb976d80

Shared Pools Statistics

User Quota Threshold Min.Alloc Cur.Alloc Max.Alloc Total-Alloc Fail-Thresh Fail-Nomem Data(Pool)-SZ
fptcp_seg 131072 0 0 0 2 2 0 0 16 (24)
inner_decode 28672 0 0 0 0 0 0 0 16 (24)
detector_threat 1048576 0 0 0 0 0 0 0 24 (24)
vm_vcheck 1048576 0 0 0 0 0 0 0 24 (24)
ctd_patmatch 1835008 0 0 0 0 0 0 0 24 (24)
proxy_pktmr 32768 0 0 0 0 0 0 0 16 (24)
vm_field 4194304 0 0 0 137420 106119556 0 0 32 (32)
decode_filter 1048576 0 0 0 0 0 0 0 40 (40)
appid_session 1835008 0 0 0 11 6070153 0 0 104 (192)
appid_dfa_state 1835008 0 0 0 0 0 0 0 184 (192)
cpat_state 458752 4376000 229376 0 0 0 0 0 184 (192)
ctd_flow 1835008 0 0 0 183682 6070153 0 0 176 (192)
ctd_flow_state 1835008 0 0 0 0 0 0 0 192 (192)
ctd_dlp_flow 458752 3829000 229376 0 0 0 0 0 192 (192)
proxy_flow 65536 0 0 0 0 0 0 0 192 (192)
ssl_hs_st 32768 0 0 0 0 0 0 0 192 (192)
ssl_key_block 65536 0 0 0 0 0 0 0 192 (192)
ssl_st 65536 0 0 0 0 0 0 0 192 (192)
ssl_hs_mac 72089 0 0 0 0 0 0 0 variable
timer_chunk 131072 0 0 0 1 1 0 0 256 (256)
hash_decode 16384 0 0 0 0 0 0 0 104 (192)

Memory Pool Size 907309KB, start address 0x8000000000000000
alloc size 72243, max 29460723
fixed buf allocator, size 929081784
sz allocator, page size 32768, max alloc 4096 quant 64
pool 0 element size 64 avail list 1 full list 1
pool 1 element size 128 avail list 5 full list 0
pool 2 element size 192 avail list 1 full list 0
pool 3 element size 256 avail list 1 full list 0
pool 4 element size 320 avail list 1 full list 0
pool 5 element size 384 avail list 1 full list 0
pool 10 element size 704 avail list 1 full list 0
pool 16 element size 1088 avail list 1 full list 0
parent allocator
alloc size 453224, max 47540840
malloc allocator
current usage 458752 max. usage 47546368, free chunks 28339, total chunks 28353

Mem-Pool-Type MAX.SZ(B) Threshold Min.Alloc Cur.SZ(B) Cur.Alloc Total-Alloc Fail-Thresh Fail-Nomem Local-Reuse(cache)
ctd_dlp_buf 29130752 464542608 14565376 0 0 0 0 0 0 (0)
sml_regfile 396361728 0 0 0 0 12140306 0 0 433491 (8)
proxy 178089536 0 0 65179 803 1642 0 0 0 (0)
l7_data 2777088 0 0 0 0 0 0 0 0 (0)
l7_misc 285098056 650359651 142549028 0 0 0 0 0 0 (0)
cfg_name_cache 566272 650359651 283136 0 0 0 0 0 0 (0)
scantracker 491520 557451129 245760 0 0 0 0 0 0 (0)
appinfo 14155776 650359651 7077888 0 0 0 0 0 0 (0)
dns 4194304 650359651 2097152 0 0 0 0 0 0 (0)

Cache-Type MAX-Entries Cur-Entries Cur.SZ(B) Insert-Failure Mem-Pool-Type
ssl_server_cert 16384 0 0 0 l7_misc
ssl_cert_cn 25000 0 0 0 l7_misc
ssl_cert_cache 1024 0 0 0 proxy
ssl_sess_cache 10000 0 0 0 proxy
proxy_exclude 1024 0 0 0 proxy
proxy_notify 8192 0 0 0 proxy
ctd_block_answer 16384 0 0 0 l7_misc
username_cache 4096 0 0 0 cfg_name_cache
threatname_cache 4096 0 0 0 cfg_name_cache
hipname_cache 256 0 0 0 cfg_name_cache
ctd_cp 16384 0 0 0 l7_misc
ctd_driveby 4096 0 0 0 l7_misc
ctd_pcap 1024 0 0 0 l7_misc
ctd_sml 8192 0 0 0 l7_misc
ctd_url 2097152 0 0 0 l7_misc
app_tracker 65536 0 0 0 l7_misc
threat_tracker 4096 0 0 0 l7_misc
scan_tracker 4096 0 0 0 scantracker
app_info 65536 0 0 0 appinfo
dns_v4 20000 0 0 0 dns
dns_v6 20000 0 0 0 dns
dns_id 1024 0 0 0 dns
tcp_mcb 8256 0 0 0 l7_misc
"""
PANOS_TEXTFSM_DEBUG_DATAPLANE_POOL_STATISTICS_1 =(PANOS_DEBUG_DATAPLANE_POOL_STATISTICS_OUT_1,
[{
  'DP': 'dp0',
  'PKT_BUFFER_NOW': '85940',
  'PKT_BUFFER_MAX': '86016',
  'QUEUE_ENTRIES_NOW': '270272',
  'QUEUE_ENTRIES_MAX': '270336'
  },
  {
  'DP': 'dp1',
  'PKT_BUFFER_NOW': '262064',
  'PKT_BUFFER_MAX': '262144',
  'QUEUE_ENTRIES_NOW': '491452',
  'QUEUE_ENTRIES_MAX': '491520'
  },
  {
  'DP': 'dp2',
  'PKT_BUFFER_NOW': '262062',
  'PKT_BUFFER_MAX': '262144',
  'QUEUE_ENTRIES_NOW': '491466',
  'QUEUE_ENTRIES_MAX': '491520'
  }])

PANOS_TEXTFSM_DEBUG_DATAPLANE_POOL_STATISTICS_2 =(PANOS_DEBUG_DATAPLANE_POOL_STATISTICS_OUT_2,
[{
  'DP': 'dp0',
  'PKT_BUFFER_NOW': '23456',
  'PKT_BUFFER_MAX': '86016',
  'QUEUE_ENTRIES_NOW': '20272',
  'QUEUE_ENTRIES_MAX': '270336'
  },
  {
  'DP': 'dp1',
  'PKT_BUFFER_NOW': '262064',
  'PKT_BUFFER_MAX': '262144',
  'QUEUE_ENTRIES_NOW': '491452',
  'QUEUE_ENTRIES_MAX': '491520'
  },
  {
  'DP': 'dp2',
  'PKT_BUFFER_NOW': '262062',
  'PKT_BUFFER_MAX': '262144',
  'QUEUE_ENTRIES_NOW': '491466',
  'QUEUE_ENTRIES_MAX': '491520'
  }])

#LOGIC blocks input
CHECK_GET_CPU_HIGHER_USAGE_1 = ((PANOS_TEXTFSM_SHOW_RUNNING_MONITOR_1[1], "dp0: 1"),
{
  'dp': "dp0",
  'cpu_id': 1,
  'utilization': 2
  })

CHECK_GET_CPU_HIGHER_USAGE_2 = ((
[{'MINUTE': ['*   *   2   3']
},
{'MINUTE': ['*   *   87   4']
},
{'MINUTE': ['*   *   2   3']
},
{'MINUTE': ['*   *   2   4']
},
{'MINUTE': ['*   *   2   3']
}],"dp0: 1"),
{
  'dp': "dp0",
  'cpu_id': 1,
  'utilization': 87
  })

CHECK_GET_CPU_HIGHER_USAGE_3 = ((PANOS_TEXTFSM_SHOW_RUNNING_MONITOR_2[1],"dp0: 4"),
{
  'dp': "dp0",
  'cpu_id': 4,
  'utilization': 99
  })

GET_BUFFER_USAGE_1 = (PANOS_TEXTFSM_DEBUG_DATAPLANE_POOL_STATISTICS_1[1], 0.99912 )