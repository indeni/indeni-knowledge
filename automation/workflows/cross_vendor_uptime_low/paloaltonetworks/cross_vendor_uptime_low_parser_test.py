import unittest
import automation.workflows.cross_vendor_uptime_low.paloaltonetworks.cross_vendor_uptime_low_mock_data as mock
import automation.workflows.cross_vendor_uptime_low.paloaltonetworks.cross_vendor_uptime_low_parser as parser


class CrossVendorUptimeLowParsersTests(unittest.TestCase):


    def test_panos_get_clock(self):
        pair1 = mock.GET_CLOCK_PAIR_1
        self.assertEqual(pair1[1], parser.panos_get_clock(pair1[0]))

    def test_get_log_query_job_id(self):
        pair1 = mock.GET_LOG_QUERY_JOB_ID_PAIR_1
        self.assertEqual(pair1[1], parser.get_log_query_job_id(pair1[0]))

    def test_parse_log_query_last_1h_reboots(self):
        pair1 = mock.PARSE_LOG_QUERY_LAST_1H_REBOOTS_PAIR_1
        self.assertEqual(pair1[1], parser.parse_log_query_last_1h_reboots(pair1[0]))

    def test_panos_check_coredump(self):
        pair1 = mock.CHECK_CORE_DUMP_PAIR_1
        pair2 = mock.CHECK_CORE_DUMP_PAIR_2
        pair3 = mock.CHECK_CORE_DUMP_PAIR_3
        self.assertEqual(pair1[1], parser.panos_check_coredump(pair1[0][0],pair1[0][1]))
        self.assertEqual(pair2[1], parser.panos_check_coredump(pair2[0][0],pair2[0][1]))
        self.assertEqual(pair3[1], parser.panos_check_coredump(pair3[0][0],pair3[0][1]))

if __name__ == '__main__':
    unittest.main()
