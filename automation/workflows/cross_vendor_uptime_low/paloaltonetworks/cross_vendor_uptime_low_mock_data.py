#HTTPS command: /api/?type=op&cmd=<show><clock></clock></show>
SHOW_CLOCK_OUT_1 = """<response status=\"success\"><result>Wed Jan 22 06:18:45 PST 2020</result></response>"""

GET_CLOCK_PAIR_1 = ( SHOW_CLOCK_OUT_1,
                    "Wed Jan 22 06:18:45 PST 2020"
                    )

#SSH command: show log system receive_time in last-hour subtype equal general | match restart
SHOW_LOG_SYSTEM_OUT_1 = "2019/12/09 12:14:10 high     general        general 0  System restart requested by indeni"

SHOW_LOG_SYSTEM_OUT_2 = """2019/12/09 12:14:10 high     general        general 0  System restart requested by indeni
2019/12/12 14:17:33 high     general        general 0  System restart requested by indeni
"""

SHOW_LOG_SYSTEM_OUT_3 = """"""

#SSH command: show system files
SHOW_SYSTEM_FILES_OUT_1 = """<response status="success">
<result>
<![CDATA[ /var/cores/:
total 4.0K
drwxr-xr-x 2 root root 4.0K Jan 10 00:15 crashinfo
/var/cores/crashinfo:
total 0
/opt/var.dp2/cores/:
total 4.0K
drwxrwxrwx 2 root root 4.0K Jan 10 00:15 crashinfo
/opt/var.dp2/cores/crashinfo:
total 0
/opt/var.dp1/cores/:
total 4.0K
drwxrwxrwx 2 root root 4.0K Jan 10 00:15 crashinfo
/opt/var.dp1/cores/crashinfo:
total 0
/opt/var.cp/cores/:
total 4.0K
drwxr-xr-x 2 root root 4.0K Jan 10 00:16 crashinfo
/opt/var.cp/cores/crashinfo:
total 0
/opt/var.dp0/cores/:
total 640K
drwxr-xr-x 2 root root 4.0K Jan 19 19:51 crashinfo
-rw-r--r-- 1 root root 631K Jan 19 20:00 all_pktproc_34_8.0.15_1.tar.gz
/opt/var.dp0/cores/crashinfo:
total 72K
-rw-r--r-- 1 root root 995 Jan 10 08:44 all_pktproc_34_8.0.15_0.pcap
-rw-rw-rw- 1 root root 30K Jan 10 08:44 all_pktproc_34_8.0.15_0.info
-rw-r--r-- 1 root root 630 Jan 19 19:51 all_pktproc_34_8.0.15_1.pcap
-rw-rw-rw- 1 root root 30K Jan 19 19:51 all_pktproc_34_8.0.15_1.info
/opt/panlogs/cores/:
total 4.0K
drwxrwxrwx 2 root root 4.0K Apr 17 09:02 crashinfo
/opt/panlogs/cores/crashinfo:
total 0 ]]>
</result>
</response>"""

SHOW_SYSTEM_FILES_OUT_2 = """<response status="success">
<result>
<![CDATA[ /var/cores/:
total 4.0K
drwxr-xr-x 2 root root 4.0K Jan 10 00:15 crashinfo
/var/cores/crashinfo:
total 0
/opt/var.dp2/cores/:
total 4.0K
drwxrwxrwx 2 root root 4.0K Jan 10 00:15 crashinfo
/opt/var.dp2/cores/crashinfo:
total 0
/opt/var.dp1/cores/:
total 4.0K
drwxrwxrwx 2 root root 4.0K Jan 10 00:15 crashinfo
/opt/var.dp1/cores/crashinfo:
total 0
/opt/var.cp/cores/:
total 4.0K
drwxr-xr-x 2 root root 4.0K Jan 10 00:16 crashinfo
/opt/var.cp/cores/crashinfo:
total 0
/opt/var.dp0/cores/:
total 640K
drwxr-xr-x 2 root root 4.0K Jan 19 19:51 crashinfo
-rw-r--r-- 1 root root 631K Jan 19 20:00 all_pktproc_34_8.0.15_1.tar.gz
/opt/var.dp0/cores/crashinfo:
total 72K
-rw-r--r-- 1 root root 995 Jan 10 08:44 all_pktproc_34_8.0.15_0.pcap
-rw-rw-rw- 1 root root 30K Jan 10 08:44 all_pktproc_34_8.0.15_0.info
-rw-r--r-- 1 root root 630 Jan 19 19:51 all_pktproc_34_8.0.15_1.pcap
-rw-rw-rw- 1 root root 30K Jan 19 19:51 all_pktproc_34_8.0.15_1.info
/opt/panlogs/cores/:
total 4.0K
drwxrwxrwx 2 root root 4.0K Apr 17 09:02 crashinfo
/opt/panlogs/cores/crashinfo:
total 0 ]]>
</result>
</response>"""

SHOW_SYSTEM_FILES_OUT_3 = """<response status="success">
<result>
<![CDATA[ /var/cores/:
total 4.0K
drwxr-xr-x 2 root root 4.0K Oct  7 22:47 crashinfo

/var/cores/crashinfo:
total 0

/opt/panlogs/cores/:
total 4.0K
drwxrwxrwx 2 root root 4.0K Nov 11 02:23 crashinfo

/opt/panlogs/cores/crashinfo:
total 0]]>
</result>
</response>"""

GET_LOG_QUERY_JOB_ID_PAIR_1 = ("""
<response status="success" code="19">
    <result>
        <msg>
            <line>query job enqueued with jobid 66</line>
        </msg>
        <job>66</job>
    </result>
</response>
""","66")

PARSE_LOG_QUERY_LAST_1H_REBOOTS_PAIR_1 = ("""<response status="success">
<result>
<job>
<tenq>13:33:20</tenq>
<tdeq>13:33:20</tdeq>
<tlast>13:33:20</tlast>
<status>FIN</status>
<id>24</id>
</job>
<log>
<logs count="1" progress="100">
<entry logid="7048020716892979979">
<domain>1</domain>
<receive_time>2022/01/01 13:09:39</receive_time>
<serial>007251000160254</serial>
<seqno>1191201</seqno>
<actionflags>0x0</actionflags>
<is-logging-service>no</is-logging-service>
<type>SYSTEM</type>
<subtype>general</subtype>
<config_ver>0</config_ver>
<time_generated>2022/01/01 13:09:39</time_generated>
<high_res_timestamp>2022-01-01T13:09:39.000-08:00</high_res_timestamp>
<dg_hier_level_1>0</dg_hier_level_1>
<dg_hier_level_2>0</dg_hier_level_2>
<dg_hier_level_3>0</dg_hier_level_3>
<dg_hier_level_4>0</dg_hier_level_4>
<device_name>PAN-FW1-10.0</device_name>
<vsys_id>0</vsys_id>
<eventid>system-shutdown</eventid>
<fmt>0</fmt>
<id>1</id>
<module>general</module>
<severity>high</severity>
<opaque>The system is shutting down due to UI Initiated.</opaque>
<dg_id>0</dg_id>
<tpl_id>0</tpl_id>
</entry>
</logs>
</log>
<meta>
<devices>
<entry name="localhost.localdomain">
<hostname>localhost.localdomain</hostname>
<vsys>
<entry name="vsys1">
<display-name>vsys1</display-name>
</entry>
</vsys>
</entry>
</devices>
</meta>
</result>
</response>""","1")

CHECK_CORE_DUMP_PAIR_1 = ((SHOW_SYSTEM_FILES_OUT_1, "Wed Jan 10 08:44:45 PST 2019"),
                        ['/opt/var.dp0/cores/crashinfo/all_pktproc_34_8.0.15_0.pcap', '/opt/var.dp0/cores/crashinfo/all_pktproc_34_8.0.15_0.info']
                        )

CHECK_CORE_DUMP_PAIR_2 = ((SHOW_SYSTEM_FILES_OUT_2, "Wed Oct 10 08:44:45 PST 2019"),
                        []
                        )

CHECK_CORE_DUMP_PAIR_3 = ((SHOW_SYSTEM_FILES_OUT_3,"Fri Jan 31 11:45:22 PST 2020"),
                        []
                        )
