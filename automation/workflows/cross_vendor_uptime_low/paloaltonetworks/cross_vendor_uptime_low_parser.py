import re
from typing import List
from dateutil import parser
from datetime import datetime, timedelta
from parser_service.public.helper_methods import *


def check_variant_matches(**kwargs) -> bool:
    # This should work on all devices
    tags = kwargs.get('my_device_tags', None)
    if tags is None:
        return False
    return True

def panos_get_clock(raw_data: str) -> str:
    parsed = parse_data_as_xml(raw_data)
    date = parsed ['response']['result']
    return date

def get_log_query_job_id (raw_data: str) -> str:
    if raw_data:  # We check if input is not empty
        data = parse_data_as_xml(raw_data)
        if data and data['response']['@status'] != "error":
            return data['response']['result']['job']

def parse_log_query_last_1h_reboots(raw_data: str) -> int:
    if raw_data:  # We check if input is not empty
        data = parse_data_as_xml(raw_data)
        if data and data['response']['@status'] != "error":
            return data['response']['result']['log']['logs']['@count']

def panos_check_coredump(
        raw_data: str,
        device_time: str,
    ) -> list:

    delta = 900
    coredump_list = []
    device_datetime = parser.parse(device_time)
    raw_data_parsed = parse_data_as_xml(raw_data)
    for line in raw_data_parsed['response']['result'].split('\n'):
        # /var/cores/:
        m = re.search('(/.*):$', line)
        if m:
            dirname = m.group(1)
            if not dirname.endswith('/'):
                dirname += '/'
            continue
        # drwxr-xr-x 2 root root 4.0K Feb 12 12:37 crashinfo
        fields = line.strip().split()
        if len(fields) > 5:
            # skip directory which starts with letter d
            if line.startswith('d'):
                continue
            if re.search('core', dirname):
                if re.search(':', fields[-2]):
                    hour_min = fields[-2]
                    year = device_datetime.year
                else:
                    year = fields[-2]
                    hour_min = None
                coredump = dirname + fields[-1]
                month = fields[-4]
                day = fields[-3]
                if hour_min is not None:
                    hour, minute = hour_min.split(':')
                else:
                    hour = '0'
                    minute = '0'
                device_tz = device_time.split()[-2]
                coredump_datetime = parser.parse("%s %s %s:%s:00 %s %s" %(month, day, hour, minute, device_tz, year))
                if abs((device_datetime - coredump_datetime).total_seconds()) < delta:
                    coredump_list.append(coredump)
    return coredump_list