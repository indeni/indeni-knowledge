from datetime import date


#SSH command: uptime
UPTIME_OUT_1 = """07:36:21 up 19 days, 19:36, 14 users,  load average: 0.40, 0.27, 0.24"""

GET_DEVICE_UPTIME_PAIR_1 = ( UPTIME_OUT_1, "07:36:21")

#SSH command: cat /var/log/messages* | grep restart | cat
RESTART_LOGS_OUT_1 = """{0} 07:41:09 2020 ENG-CP-R80 syslogd 1.4.1: restart.
{0} 07:41:12 2020 ENG-CP-R80 syslogd 1.4.1: restart.""".format(date.today().strftime("%b %d"))

RESTART_LOGS_OUT_2 = """{0} 07:41:09 2020 ENG-CP-R80 syslogd 1.4.1: restart.
{0} 07:41:12 2020 ENG-CP-R80 syslogd 1.4.1: restart.""".format(date.today().strftime("%b %d"))

COLLECT_RESTART_LOGS_PAIR_1 = ((RESTART_LOGS_OUT_1, '07:44:09'), 2)

COLLECT_RESTART_LOGS_PAIR_2 = ((RESTART_LOGS_OUT_2, '09:44:09'), 0)

#SSH command: ls -lh /var/log/dump/usermode
LS_LH_LOG_DUMP_USERMODE_OUT_1 = """-rw-r--r-- 1 admin root 910K {0} 07:41 cpbackup_util.29762.core.gz
#-rw-r--r-- 1 admin root 1.6M {0} 07:35 2019 cpviewd.4565.core.gz""".format(date.today().strftime("%b %d"))

LS_LH_LOG_DUMP_USERMODE_OUT_2 = """-rw-r--r-- 1 admin root 910K {0} 09:21 cpbackup_util.29762.core.gz
#-rw-r--r-- 1 admin root 1.6M {0} 09:20  2019 cpviewd.4565.core.gz""".format(date.today().strftime("%b %d"))

COLLECT_CORE_DUMP_LOGS_PAIR_1 = ((LS_LH_LOG_DUMP_USERMODE_OUT_1, '07:44:09'), 2)

COLLECT_CORE_DUMP_LOGS_PAIR_2 = ((LS_LH_LOG_DUMP_USERMODE_OUT_2, '09:44:09'), 0)

#SSH command(32-bit) : ls -lh /var/crash/
#SSH command (64-bit): ls -lh /var/log/crash/
LS_LH_CRASH_FILES_OUT_1 = '''drwxr-xr-x 2 admin root 31 {0} 07:41 2025-09-03-08:07'''.format(date.today().strftime("%b %d"))

LS_LH_CRASH_FILES_PAIR_1 = ((LS_LH_CRASH_FILES_OUT_1, '07:44:09'), 1)

LS_LH_CRASH_FILES_OUT_2 = '''drwxr-xr-x 2 admin root 31 {0} 09:21 2025-09-03-08:07'''.format(date.today().strftime("%b %d"))

LS_LH_CRASH_FILES_PAIR_2 = ((LS_LH_CRASH_FILES_OUT_2, '09:44:09'),0)

#SSH command: clish -c "show version os edition"
SHOW_VERSION_OS_EDITION_OUT_1 = """OS edition 32-bit"""

CREATE_KERNEL_CORE_DUMP_COMMAND_PAIR_1 = (SHOW_VERSION_OS_EDITION_OUT_1, "ls -lh /var/crash/")

SHOW_VERSION_OS_EDITION_OUT_2 = """OS edition 64-bit"""

CREATE_KERNEL_CORE_DUMP_COMMAND_PAIR_2 = (SHOW_VERSION_OS_EDITION_OUT_2, "ls -lh /var/log/crash/")

#Logic blocks inputs
CRASH_FILES_LIST_OUT_1 = ['-rw-r--r-- 1 admin root 910K Jan 28 07:41 cpbackup_util.29762.core.gz', 
'-rw-r--r-- 1 admin root 1.6M Jan 28 07:35 2019 cpviewd.4565.core.gz']

CRASH_FILES_LIST_PAIR_1 = (CRASH_FILES_LIST_OUT_1, {'name': 'cpbackup_util', 'pid': '29762'})