import unittest
import cross_vendor_uptime_low_mock_data as mock
from cross_vendor_uptime_low_parser import *


class CrossVendorUptimeLowTest(unittest.TestCase):


    def test_get_device_current_time(self):
        pair1 = mock.GET_DEVICE_UPTIME_PAIR_1
        self.assertEqual(get_device_current_time(pair1[0]),pair1[1])

    def test_collect_restart_logs(self):
        pair1 = mock.COLLECT_RESTART_LOGS_PAIR_1
        pair2 = mock.COLLECT_RESTART_LOGS_PAIR_2
        self.assertEqual(len(collect_restart_logs(pair1[0][0],pair1[0][1])), pair1[1])
        self.assertEqual(len(collect_restart_logs(pair2[0][0],pair2[0][1]) ), pair2[1])

    def test_collect_core_dump_logs(self):
        pair1 = mock.COLLECT_CORE_DUMP_LOGS_PAIR_1
        pair2 = mock.COLLECT_CORE_DUMP_LOGS_PAIR_2
        self.assertEqual(len(collect_core_dump_logs(pair1[0][0],pair1[0][1])), pair1[1])
        self.assertEqual(len(collect_core_dump_logs(pair2[0][0],pair2[0][1])), pair2[1])

    def test_get_latest_crashed_process(self):
        pair1 = mock.CRASH_FILES_LIST_PAIR_1
        self.assertEqual(get_latest_crashed_process(pair1[0]), pair1[1])


    def test_collect_kernel_core_dump_logs(self):
        pair1 = mock.LS_LH_CRASH_FILES_PAIR_1
        pair2 = mock.LS_LH_CRASH_FILES_PAIR_2
        self.assertEqual(len(collect_kernel_core_dump_logs(pair1[0][0],pair1[0][1])), pair1[1])
        self.assertEqual(len(collect_kernel_core_dump_logs(pair2[0][0],pair2[0][1])), pair2[1])

    def test_create_kernel_core_dump_command(self):
        pair1 = mock.CREATE_KERNEL_CORE_DUMP_COMMAND_PAIR_1
        pair2 = mock.CREATE_KERNEL_CORE_DUMP_COMMAND_PAIR_2
        self.assertEqual(create_kernel_core_dump_command(pair1[0]), pair1[1])
        self.assertEqual(create_kernel_core_dump_command(pair2[0]), pair2[1])


if __name__ == '__main__':
    unittest.main()
