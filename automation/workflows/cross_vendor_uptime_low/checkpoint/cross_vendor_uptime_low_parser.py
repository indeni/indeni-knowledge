import re
from typing import List

from dateutil import parser


def check_variant_matches(**kwargs) -> bool:
    # This should work on all devices with one of the below versions
    tags = kwargs.get('my_device_tags', None)
    if tags is None:
        return False
    versions = [x.lower() for x in ['R77.30', 'R80.10', 'R80.20', 'R80.30', 'R76SP.30', 'R76SP.40', 'R76SP.50', 'R80.20SP', 'R80.40']]
    if tags.get('os.version', '').lower() not in versions:
        return False
    return True


def __parse_results(
        raw_data: str,
        uptime_result: str,
        interval_in_seconds: int,
        date_regex: str,
        ignore_regex: str = None) -> List[str]:
    device_time = parser.parse(uptime_result)
    lines = raw_data.split('\n')
    date_regex = re.compile(date_regex)
    ignore_regex = ignore_regex and re.compile(ignore_regex)
    entries = []
    for line in lines:
        line_date = date_regex.search(line)
        if not line_date:
            continue
        parsed_line_date = parser.parse(line_date.group(1))
        ignore_line = ignore_regex and ignore_regex.search(line)
        diff_in_seconds = (device_time - parsed_line_date).total_seconds()
        if not ignore_line and 0 < diff_in_seconds < interval_in_seconds:
            entries.append(line)
    return entries


def get_device_current_time(raw_data: str):
    return raw_data.split()[0]


def collect_restart_logs(raw_data: str, uptime_result: str) -> List[str]:
    return __parse_results(raw_data,
                           uptime_result,
                           900,
                           '([A-Za-z]{3} +[0-9]{1,2} +[0-9]{1,2}:[0-9]{1,2}:[0-9]{1,2} +20[0-9]{2})',
                           'grep')


def collect_core_dump_logs(raw_data, uptime_result: str):
    return __parse_results(raw_data,
                           uptime_result,
                           900,
                           "([A-Za-z]{3} +[0-9]{1,2} +([0-9]{1,2}:[0-9]{1,2}|20[0-9]{2}))")


def get_latest_crashed_process(core_dump_entries):
    date_regex = re.compile("([A-Za-z]{3} +[0-9]{1,2} +([0-9]{1,2}:[0-9]{1,2}|20[0-9]{2}))")
    process_regex = re.compile("(\S+)\.[0-9]+\.core")
    pid_regex = re.compile("\S+\.([0-9]+)\.core")
    latest_process_name = ''
    latest_pid = ''
    latest_date = None
    for line in core_dump_entries:
        line_date = parser.parse(date_regex.search(line).group(1))
        if not latest_date or line_date > latest_date:
            if not process_regex.search(line):
                continue
            latest_process_name = process_regex.search(line).group(1)
            latest_pid = pid_regex.search(line).group(1)
            latest_date = line_date
    return {'name': latest_process_name, 'pid': latest_pid}


def collect_kernel_core_dump_logs(raw_data, uptime_result):
    return __parse_results(raw_data,
                           uptime_result,
                           900,
                           "([A-Za-z]{3} +[0-9]{1,2} +([0-9]{1,2}:[0-9]{1,2}|20[0-9]{2}))")


def create_kernel_core_dump_command(raw_data):
    if raw_data == 'OS edition 64-bit':
        return "ls -lh /var/log/crash/"
    else:
        return "ls -lh /var/crash/"
