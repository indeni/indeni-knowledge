import unittest
import automation.workflows.cross_vendor_cluster_member_no_longer_active_non_vsx.paloaltonetworks.cross_vendor_cluster_member_no_longer_active_non_vsx_mock_data as mock
import automation.workflows.cross_vendor_cluster_member_no_longer_active_non_vsx.paloaltonetworks.cross_vendor_cluster_member_no_longer_active_non_vsx_parser as parser


class ClusterMemberNoLongerActiveTests(unittest.TestCase):

    def test_make_ha_cluster_dict(self):
        pair1 = mock.MAKE_HA_CLUSTER_DICT_PAIR_1
        pair2 = mock.MAKE_HA_CLUSTER_DICT_PAIR_2
        self.assertEqual(pair1[1], dict(parser.make_ha_cluster_dict(pair1[0])))
        self.assertEqual(pair2[1], dict(parser.make_ha_cluster_dict(pair2[0])))

    def test_get_admin_name_on_first_line(self):
        pair1 = mock.GET_ADMIN_NAME_ON_FIRST_LINE_PAIR_1
        pair2 = mock.GET_ADMIN_NAME_ON_FIRST_LINE_PAIR_2
        pair3 = mock.GET_ADMIN_NAME_ON_FIRST_LINE_PAIR_3
        self.assertEqual(pair1[1], parser.get_admin_name_on_first_line(pair1[0]))
        self.assertEqual(pair2[1], parser.get_admin_name_on_first_line(pair2[0]))
        self.assertEqual(pair3[1], parser.get_admin_name_on_first_line_https(pair3[0]))

if __name__ == '__main__':
    unittest.main()
