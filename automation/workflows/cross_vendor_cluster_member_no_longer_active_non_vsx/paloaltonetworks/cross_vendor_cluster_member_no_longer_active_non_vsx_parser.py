import re
from collections import defaultdict
from typing import Dict
from parser_service.public.helper_methods import *


def check_variant_matches(**kwargs) -> bool:
    # This should work on all non-vsx gateways in a cluster
    tags = kwargs.get('my_device_tags', None)
    if tags is None:
        return False
    if tags.get('vsx') == 'true':
        return False
    if tags.get('product') != 'firewall':
        return False
    if not tags.get('cluster-id'):
        return False
    return True


def make_ha_cluster_dict(raw_data: str) -> Dict[str, str]:
    try:
        data = parse_data_as_xml(raw_data)
        ha_dict = defaultdict(str)

        if data:
            if data['response']['result']['group']:
                haState = data['response']['result']['group']['local-info']['state']
                ha_dict['haState'] = haState
                haReason = data['response']['result']['group']['peer-info']['last-error-reason']
                ha_dict['haReason'] = haReason
                haConfigSynchronizationEnabled = data['response']['result']['group']['running-sync-enabled']
                ha_dict['haConfigSynchronizationEnabled'] = haConfigSynchronizationEnabled
                haRunningConfiguration = data['response']['result']['group']['running-sync']
                ha_dict['haRunningConfiguration'] = haRunningConfiguration
                haLmEnabled = data['response']['result']['group']['link-monitoring']['enabled']
                ha_dict['haLmEnabled'] = haLmEnabled
                haPmEnabled = data['response']['result']['group']['path-monitoring']['enabled']
                ha_dict['haPmEnabled'] = haPmEnabled
                haStateSync = data['response']['result']['group']['local-info']['state-sync']
                ha_dict['haStateSync'] = haStateSync
    except Exception:
        ha_dict['haState'] = ''
    return ha_dict


def get_admin_name_on_first_line(raw_data: str) -> str:
    lines = raw_data.strip().split('\n')
    if len(lines) == 0:
        return ''
    split_line = lines[0].split(' ')
    return split_line[-1]

def get_admin_name_on_first_line_https(raw_data: str) -> str:
    lines = []
    split_line = []
    if raw_data:  # We check if input is not empty
        data = parse_data_as_xml(raw_data)
        if data and data['response']['@status'] != "error":
            if data['response']['result']['log']['logs']['@count'] != "0":
                for entry in data['response']['result']['log']['logs']['entry']:
                    lines.append(entry['opaque'])           
        split_line = lines[0].split(' ') 
        return split_line[-1]

def get_log_query_job_id (raw_data: str) -> str:
    if raw_data:  # We check if input is not empty
        data = parse_data_as_xml(raw_data)
        if data and data['response']['@status'] != "error":
            return data['response']['result']['job']