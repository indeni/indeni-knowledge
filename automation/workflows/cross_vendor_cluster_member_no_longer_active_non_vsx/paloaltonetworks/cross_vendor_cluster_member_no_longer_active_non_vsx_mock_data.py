MAKE_HA_CLUSTER_DICT_PAIR_1 = ("""
<response status="success">
   <result>
      <enabled>yes</enabled>
      <group>
         <mode>Active-Passive</mode>
         <local-info>
            <url-compat>Mismatch</url-compat>
            <app-version>8248-6004</app-version>
            <gpclient-version>4.1.13</gpclient-version>
            <build-rel>9.0.4</build-rel>
            <ha2-port>ethernet1/4</ha2-port>
            <av-version>3287-3798</av-version>
            <ha2-keep-alive>log-only</ha2-keep-alive>
            <url-version>20200316.20332</url-version>
            <ha1-backup-ipaddr>10.11.95.32/24</ha1-backup-ipaddr>
            <active-passive>
               <passive-link-state>auto</passive-link-state>
               <monitor-fail-holddown>1</monitor-fail-holddown>
            </active-passive>
            <platform-model>PA-VM</platform-model>
            <av-compat>Match</av-compat>
            <ha2-ipaddr>172.24.245.150/30</ha2-ipaddr>
            <vpnclient-compat>Match</vpnclient-compat>
            <ha1-ipaddr>10.255.255.2/30</ha1-ipaddr>
            <ha1-backup-macaddr>00:50:56:ac:fb:44</ha1-backup-macaddr>
            <vm-license>VM-100</vm-license>
            <ha2-macaddr>00:50:56:ac:c2:55</ha2-macaddr>
            <monitor-fail-holdup>0</monitor-fail-holdup>
            <priority>200</priority>
            <preempt-hold>1</preempt-hold>
            <state>active</state>
            <version>1</version>
            <promotion-hold>2000</promotion-hold>
            <threat-compat>Match</threat-compat>
            <state-sync>Complete</state-sync>
            <addon-master-holdup>500</addon-master-holdup>
            <heartbeat-interval>2000</heartbeat-interval>
            <ha1-link-mon-intv>3000</ha1-link-mon-intv>
            <hello-interval>8000</hello-interval>
            <ha1-port>ethernet1/3</ha1-port>
            <ha1-encrypt-imported>no</ha1-encrypt-imported>
            <mgmt-ip>10.11.95.32/24</mgmt-ip>
            <vpnclient-version>Not Installed</vpnclient-version>
            <ha1-backup-port>management</ha1-backup-port>
            <preempt-flap-cnt>0</preempt-flap-cnt>
            <ha2-ka-thresh>10000</ha2-ka-thresh>
            <nonfunc-flap-cnt>0</nonfunc-flap-cnt>
            <threat-version>8248-6004</threat-version>
            <ha1-macaddr>00:50:56:ac:8f:f6</ha1-macaddr>
            <state-duration>102105</state-duration>
            <max-flaps>3</max-flaps>
            <ha1-encrypt-enable>no</ha1-encrypt-enable>
            <mgmt-ipv6 />
            <state-sync-type>ethernet</state-sync-type>
            <preemptive>no</preemptive>
            <gpclient-compat>Match</gpclient-compat>
            <mode>Active-Passive</mode>
            <ha1-backup-gateway>10.11.95.254</ha1-backup-gateway>
            <build-compat>Match</build-compat>
            <VMS>Match</VMS>
            <app-compat>Match</app-compat>
            <ha2-gateway>172.24.245.149</ha2-gateway>
         </local-info>
         <peer-info>
            <app-version>8248-6004</app-version>
            <gpclient-version>4.1.13</gpclient-version>
            <url-version>0000.00.00.000</url-version>
            <ha1-backup-ipaddr>10.11.95.31</ha1-backup-ipaddr>
            <av-version>3287-3798</av-version>
            <platform-model>PA-VM</platform-model>
            <ha2-ipaddr>172.24.245.149</ha2-ipaddr>
            <ha1-ipaddr>10.255.255.1</ha1-ipaddr>
            <vm-license>VM-100</vm-license>
            <ha2-macaddr>00:50:56:ac:7e:52</ha2-macaddr>
            <priority>100</priority>
            <state>passive</state>
            <version>1</version>
            <last-error-reason>User requested</last-error-reason>
            <build-rel>9.0.4</build-rel>
            <conn-status>up</conn-status>
            <ha1-backup-macaddr>00:50:56:ac:ce:3e</ha1-backup-macaddr>
            <vpnclient-version>Not Installed</vpnclient-version>
            <mgmt-ip>10.11.95.31/24</mgmt-ip>
            <conn-ha2>
               <conn-type>log-only</conn-type>
               <conn-primary>yes</conn-primary>
               <conn-status>up</conn-status>
               <conn-desc>keep-alive status</conn-desc>
               <conn-ka-enbled>yes</conn-ka-enbled>
               <conn-hold>0</conn-hold>
            </conn-ha2>
            <threat-version>8248-6004</threat-version>
            <ha1-macaddr>00:50:56:ac:cd:b3</ha1-macaddr>
            <conn-ha1>
               <conn-status>up</conn-status>
               <conn-primary>yes</conn-primary>
               <conn-desc>heartbeat status</conn-desc>
            </conn-ha1>
            <state-duration>55903</state-duration>
            <mgmt-ipv6 />
            <VMS>1.0.6</VMS>
            <last-error-state>suspended</last-error-state>
            <preemptive>no</preemptive>
            <mode>Active-Passive</mode>
            <conn-ha1-backup>
               <conn-status>up</conn-status>
               <conn-desc>heartbeat status</conn-desc>
            </conn-ha1-backup>
         </peer-info>
         <link-monitoring>
            <fail-cond>any</fail-cond>
            <enabled>yes</enabled>
            <groups>
               <entry>
                  <interface>
                     <entry>
                        <status>up</status>
                        <name>ethernet1/1</name>
                     </entry>
                     <entry>
                        <status>up</status>
                        <name>ethernet1/2</name>
                     </entry>
                  </interface>
                  <fail-cond>any</fail-cond>
                  <enabled>yes</enabled>
                  <name>LinkMonitor-A</name>
               </entry>
            </groups>
         </link-monitoring>
         <path-monitoring>
            <vwire />
            <fail-cond>any</fail-cond>
            <vlan />
            <enabled>no</enabled>
            <vrouter />
         </path-monitoring>
         <running-sync>synchronized</running-sync>
         <running-sync-enabled>yes</running-sync-enabled>
      </group>
   </result>
</response>
""", {'haState': 'active',
      'haConfigSynchronizationEnabled': 'yes',
      'haRunningConfiguration': 'synchronized',
      'haReason': 'User requested',
      'haLmEnabled': 'yes',
      'haPmEnabled': 'no',
      'haStateSync': 'Complete'})


MAKE_HA_CLUSTER_DICT_PAIR_2 = ("""
<response status="success">
   <result>
      <enabled>yes</enabled>
      <group>
         <mode>Active-Passive</mode>
         <local-info>
            <url-compat>Mismatch</url-compat>
            <app-version>8248-6004</app-version>
            <gpclient-version>4.1.13</gpclient-version>
            <build-rel>9.0.4</build-rel>
            <ha2-port>ethernet1/4</ha2-port>
            <av-version>3287-3798</av-version>
            <ha2-keep-alive>log-only</ha2-keep-alive>
            <url-version>20200316.20332</url-version>
            <ha1-backup-ipaddr>10.11.95.32/24</ha1-backup-ipaddr>
            <active-passive>
               <passive-link-state>auto</passive-link-state>
               <monitor-fail-holddown>1</monitor-fail-holddown>
            </active-passive>
            <platform-model>PA-VM</platform-model>
            <av-compat>Match</av-compat>
            <ha2-ipaddr>172.24.245.150/30</ha2-ipaddr>
            <vpnclient-compat>Match</vpnclient-compat>
            <ha1-ipaddr>10.255.255.2/30</ha1-ipaddr>
            <ha1-backup-macaddr>00:50:56:ac:fb:44</ha1-backup-macaddr>
            <vm-license>VM-100</vm-license>
            <ha2-macaddr>00:50:56:ac:c2:55</ha2-macaddr>
            <monitor-fail-holdup>0</monitor-fail-holdup>
            <priority>200</priority>
            <preempt-hold>1</preempt-hold>
            <state>passive</state>
            <version>1</version>
            <promotion-hold>2000</promotion-hold>
            <threat-compat>Match</threat-compat>
            <state-sync>Complete</state-sync>
            <addon-master-holdup>500</addon-master-holdup>
            <heartbeat-interval>2000</heartbeat-interval>
            <ha1-link-mon-intv>3000</ha1-link-mon-intv>
            <hello-interval>8000</hello-interval>
            <ha1-port>ethernet1/3</ha1-port>
            <ha1-encrypt-imported>no</ha1-encrypt-imported>
            <mgmt-ip>10.11.95.32/24</mgmt-ip>
            <vpnclient-version>Not Installed</vpnclient-version>
            <ha1-backup-port>management</ha1-backup-port>
            <preempt-flap-cnt>0</preempt-flap-cnt>
            <ha2-ka-thresh>10000</ha2-ka-thresh>
            <nonfunc-flap-cnt>0</nonfunc-flap-cnt>
            <threat-version>8248-6004</threat-version>
            <ha1-macaddr>00:50:56:ac:8f:f6</ha1-macaddr>
            <state-duration>102105</state-duration>
            <max-flaps>3</max-flaps>
            <ha1-encrypt-enable>no</ha1-encrypt-enable>
            <mgmt-ipv6 />
            <state-sync-type>ethernet</state-sync-type>
            <preemptive>no</preemptive>
            <gpclient-compat>Match</gpclient-compat>
            <mode>Active-Passive</mode>
            <ha1-backup-gateway>10.11.95.254</ha1-backup-gateway>
            <build-compat>Match</build-compat>
            <VMS>Match</VMS>
            <app-compat>Match</app-compat>
            <ha2-gateway>172.24.245.149</ha2-gateway>
         </local-info>
         <peer-info>
            <app-version>8248-6004</app-version>
            <gpclient-version>4.1.13</gpclient-version>
            <url-version>0000.00.00.000</url-version>
            <ha1-backup-ipaddr>10.11.95.31</ha1-backup-ipaddr>
            <av-version>3287-3798</av-version>
            <platform-model>PA-VM</platform-model>
            <ha2-ipaddr>172.24.245.149</ha2-ipaddr>
            <ha1-ipaddr>10.255.255.1</ha1-ipaddr>
            <vm-license>VM-100</vm-license>
            <ha2-macaddr>00:50:56:ac:7e:52</ha2-macaddr>
            <priority>100</priority>
            <state>active</state>
            <version>1</version>
            <last-error-reason>User requested</last-error-reason>
            <build-rel>9.0.4</build-rel>
            <conn-status>up</conn-status>
            <ha1-backup-macaddr>00:50:56:ac:ce:3e</ha1-backup-macaddr>
            <vpnclient-version>Not Installed</vpnclient-version>
            <mgmt-ip>10.11.95.31/24</mgmt-ip>
            <conn-ha2>
               <conn-type>log-only</conn-type>
               <conn-primary>yes</conn-primary>
               <conn-status>up</conn-status>
               <conn-desc>keep-alive status</conn-desc>
               <conn-ka-enbled>yes</conn-ka-enbled>
               <conn-hold>0</conn-hold>
            </conn-ha2>
            <threat-version>8248-6004</threat-version>
            <ha1-macaddr>00:50:56:ac:cd:b3</ha1-macaddr>
            <conn-ha1>
               <conn-status>up</conn-status>
               <conn-primary>yes</conn-primary>
               <conn-desc>heartbeat status</conn-desc>
            </conn-ha1>
            <state-duration>55903</state-duration>
            <mgmt-ipv6 />
            <VMS>1.0.6</VMS>
            <last-error-state>suspended</last-error-state>
            <preemptive>no</preemptive>
            <mode>Active-Passive</mode>
            <conn-ha1-backup>
               <conn-status>up</conn-status>
               <conn-desc>heartbeat status</conn-desc>
            </conn-ha1-backup>
         </peer-info>
         <link-monitoring>
            <fail-cond>any</fail-cond>
            <enabled>yes</enabled>
            <groups>
               <entry>
                  <interface>
                     <entry>
                        <status>up</status>
                        <name>ethernet1/1</name>
                     </entry>
                     <entry>
                        <status>up</status>
                        <name>ethernet1/2</name>
                     </entry>
                  </interface>
                  <fail-cond>any</fail-cond>
                  <enabled>yes</enabled>
                  <name>LinkMonitor-A</name>
               </entry>
            </groups>
         </link-monitoring>
         <path-monitoring>
            <vwire />
            <fail-cond>any</fail-cond>
            <vlan />
            <enabled>no</enabled>
            <vrouter />
         </path-monitoring>
         <running-sync>synchronized</running-sync>
         <running-sync-enabled>yes</running-sync-enabled>
      </group>
   </result>
</response>
""", {'haState': 'passive',
      'haConfigSynchronizationEnabled': 'yes',
      'haRunningConfiguration': 'synchronized',
      'haReason': 'User requested',
      'haLmEnabled': 'yes',
      'haPmEnabled': 'no',
      'haStateSync': 'Complete'})

GET_ADMIN_NAME_ON_FIRST_LINE_PAIR_1 = ("""2019/11/06 23:48:14 medium   general        general 0  HA state set to suspended by admin
2019/11/06 23:21:02 medium   general        general 0  HA state set to suspended by hansolo""", 'admin')

GET_ADMIN_NAME_ON_FIRST_LINE_PAIR_2 = ("""2019/11/06 23:48:14 medium   general        general 0  HA state set to suspended by adm1n
2019/11/06 23:21:02 medium   general        general 0  HA state set to suspended by hansolo""", 'adm1n')

GET_ADMIN_NAME_ON_FIRST_LINE_PAIR_3 = ("""
<response status="success">
	<result>
		<job>
			<tenq>10:41:22</tenq>
			<tdeq>10:41:22</tdeq>
			<tlast>10:41:22</tlast>
			<status>FIN</status>
			<id>912</id>
		</job>
		<log>
			<logs count="2" progress="100">
				<entry logid="7111105196536635639">
					<domain>1</domain>
					<receive_time>2022/06/20 10:33:28</receive_time>
					<serial>007251000160261</serial>
					<seqno>1696789</seqno>
					<actionflags>0x0</actionflags>
					<is-logging-service>no</is-logging-service>
					<type>SYSTEM</type>
					<subtype>general</subtype>
					<config_ver>0</config_ver>
					<time_generated>2022/06/20 10:33:28</time_generated>
					<high_res_timestamp>2022-06-20T10:33:28.000-07:00</high_res_timestamp>
					<dg_hier_level_1>0</dg_hier_level_1>
					<dg_hier_level_2>0</dg_hier_level_2>
					<dg_hier_level_3>0</dg_hier_level_3>
					<dg_hier_level_4>0</dg_hier_level_4>
					<device_name>PAN-FW2-10.0</device_name>
					<vsys_id>0</vsys_id>
					<eventid>general</eventid>
					<fmt>0</fmt>
					<id>0</id>
					<module>general</module>
					<severity>medium</severity>
					<opaque>HA state set to suspended by admin</opaque>
					<dg_id>0</dg_id>
					<tpl_id>0</tpl_id>
				</entry>
				<entry logid="7111105196536635623">
					<domain>1</domain>
					<receive_time>2022/06/20 10:32:45</receive_time>
					<serial>007251000160261</serial>
					<seqno>1696773</seqno>
					<actionflags>0x0</actionflags>
					<is-logging-service>no</is-logging-service>
					<type>SYSTEM</type>
					<subtype>general</subtype>
					<config_ver>0</config_ver>
					<time_generated>2022/06/20 10:32:45</time_generated>
					<high_res_timestamp>2022-06-20T10:32:45.000-07:00</high_res_timestamp>
					<dg_hier_level_1>0</dg_hier_level_1>
					<dg_hier_level_2>0</dg_hier_level_2>
					<dg_hier_level_3>0</dg_hier_level_3>
					<dg_hier_level_4>0</dg_hier_level_4>
					<device_name>PAN-FW2-10.0</device_name>
					<vsys_id>0</vsys_id>
					<eventid>general</eventid>
					<fmt>0</fmt>
					<id>0</id>
					<module>general</module>
					<severity>medium</severity>
					<opaque>HA state set to suspended by indeni</opaque>
					<dg_id>0</dg_id>
					<tpl_id>0</tpl_id>
				</entry>
			</logs>
		</log>
		<meta>
			<devices>
				<entry name="localhost.localdomain">
					<hostname>localhost.localdomain</hostname>
					<vsys>
						<entry name="vsys1">
							<display-name>vsys1</display-name>
						</entry>
					</vsys>
				</entry>
			</devices>
		</meta>
	</result>
</response>                  
""", 'admin')