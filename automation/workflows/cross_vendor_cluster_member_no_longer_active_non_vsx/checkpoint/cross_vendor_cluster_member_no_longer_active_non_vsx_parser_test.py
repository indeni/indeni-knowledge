import unittest
import automation.workflows.cross_vendor_cluster_member_no_longer_active_non_vsx.checkpoint.cross_vendor_cluster_member_no_longer_active_non_vsx_parser as parser
import automation.workflows.cross_vendor_cluster_member_no_longer_active_non_vsx.checkpoint.cross_vendor_cluster_member_no_longer_active_non_vsx_mock_data as mock

class ClusterMemberNoLongerActiveTests(unittest.TestCase):
    def test_parse_collect_ha_cluster_state(self):
        mock_data = [mock.LOST_ACTIVE_ATTENTION, mock.DOWN_ACTIVE, mock.STANDBY_ACTIVE, mock.ACTIVE_ATTENTION_DOWN, mock.ACTIVE_ATTENTION_LOST, mock.ACTIVE_ATTENTION_READY, mock.ACTIVE_ATTENTION_INIT, mock.ACTIVE_ATTENTION_MISSING]
        for testdata in mock_data:
            self.assertEqual(parser.parse_collect_ha_cluster_state(testdata[0]), testdata[1])

    def test_parse_check_pnote_list(self):
        mock_data = [mock.KNOWN_PNOTE_CAUSING_STANDBY_DOWN_1, mock.KNOWN_PNOTE_CAUSING_STANDBY_DOWN_2]
        for testdata in mock_data:
            self.assertEqual(parser.parse_check_pnote_list(testdata[0]), testdata[1])


if __name__ == '__main__':
    unittest.main()
