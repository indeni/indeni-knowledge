LOST_ACTIVE_ATTENTION = ("""Cluster Mode:   High Availability (Active Up) with IGMP Membership

ID         Unique Address  Assigned Load   State          Name                                              

1          10.11.94.49        0%              LOST           CP-R80.20-GW8-1
2 (local)  10.11.94.50     100%            ACTIVE(!)         CP-R80.20-GW8-2""", {'standby_state': 'LOST', 'active_state': 'ACTIVE(!)'})

DOWN_ACTIVE = ("""Cluster Mode:   High Availability (Active Up) with IGMP Membership

ID         Unique Address  Assigned Load   State          Name                                              

1          20.1.1.1        0%              DOWN           CP-R80.20-GW8-1
2 (local)  10.11.94.50     100%            ACTIVE         CP-R80.20-GW8-2""", {'standby_state': 'DOWN', 'active_state': 'ACTIVE'})

STANDBY_ACTIVE = ("""Cluster Mode:   High Availability (Active Up) with IGMP Membership

ID         Unique Address  Assigned Load   State          Name                                              

1          20.1.1.1        0%              STANDBY          CP-R80.20-GW8-1
2 (local)  10.11.94.50     100%            ACTIVE         CP-R80.20-GW8-2""", {'standby_state': 'STANDBY', 'active_state': 'ACTIVE'})

ACTIVE_ATTENTION_DOWN = ("""
Cluster Mode:   High Availability (Active Up) with IGMP Membership

ID         Unique Address  Assigned Load   State          Name                                              

1 (local)  10.10.1.1       100%            ACTIVE(!)      ENG-CP-R80.20-GW1-1
2          10.10.1.2       0%              DOWN           ENG-CP-R80.20-GW1-2""", {'active_state': 'ACTIVE(!)', 'standby_state': 'DOWN'})

ACTIVE_ATTENTION_LOST = ("""
Cluster Mode:   High Availability (Active Up) with IGMP Membership

ID         Unique Address  Assigned Load   State          Name                                              

1 (local)  10.10.1.1       100%            ACTIVE(!)      ENG-CP-R80.20-GW1-1
2          10.10.1.2       0%              LOST           ENG-CP-R80.20-GW1-2""", {'active_state': 'ACTIVE(!)', 'standby_state': 'LOST'})

ACTIVE_ATTENTION_READY = ("""
Cluster Mode:   High Availability (Active Up) with IGMP Membership

ID         Unique Address  Assigned Load   State          Name                                              

1 (local)  10.10.1.1       100%            ACTIVE(!)      ENG-CP-R80.20-GW1-1
2          10.10.1.2       0%              READY          ENG-CP-R80.20-GW1-2""", {'active_state': 'ACTIVE(!)', 'standby_state': 'READY'})

ACTIVE_ATTENTION_INIT = ("""
Cluster Mode:   High Availability (Active Up) with IGMP Membership

ID         Unique Address  Assigned Load   State          Name                                              

1 (local)  10.10.1.1       100%            ACTIVE(!)      ENG-CP-R80.20-GW1-1
2          10.10.1.2       0%              INIT           ENG-CP-R80.20-GW1-2""", {'active_state': 'ACTIVE(!)', 'standby_state': 'INIT'})

ACTIVE_ATTENTION_MISSING = ("""
Cluster Mode:   High Availability (Active Up) with IGMP Membership

ID         Unique Address  Assigned Load   State          Name                                              

1 (local)  10.10.1.1       100%            ACTIVE(!)      ENG-CP-R80.20-GW1-1""", {'active_state': 'ACTIVE(!)'})

KNOWN_PNOTE_CAUSING_STANDBY_DOWN_1 = ("""Built-in Devices:
Device Name: Interface Active Check
Current state: problem

Registered Devices:
Device Name: Fullsync
Registration number: 0
Timeout: none
Current state: problem
Time since last report: 56794.7 sec
""", True)

KNOWN_PNOTE_CAUSING_STANDBY_DOWN_2 = ("""There are no pnotes in problem state""", False)

CHECK_MEMBER_ACTIVE_ATTENTION = (LOST_ACTIVE_ATTENTION[1], True)
