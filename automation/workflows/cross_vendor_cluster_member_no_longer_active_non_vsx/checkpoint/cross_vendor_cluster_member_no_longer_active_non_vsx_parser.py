import re
from typing import List
from dateutil import parser
from datetime import datetime, timedelta
from parser_service.public.helper_methods import *
from collections import defaultdict

def check_variant_matches(**kwargs) -> bool:
    # This should work on all gateway devices with one of the below versions
    tags = kwargs.get('my_device_tags', None)
    if tags is None:
        return False
    versions = [x.lower() for x in ['R77.30', 'R80.10', 'R80.20', 'R80.30', 'R80.40']]
    if tags.get('os.version', '').lower() not in versions:
        return False
    if tags.get('role-firewall') != 'true':
        return False
    if tags.get('vrrp') == 'true':
        return False
    return True


def parse_collect_ha_cluster_state(raw_data: str) -> dict:
    devices = parse_data_as_list(raw_data, 'cluster_member_no_longer_active.textfsm')
    devices_dict = defaultdict(str)
    for device in devices:
        if 'active' in device['state'].lower():
            devices_dict.update({'active_state': device['state'].lower()})
        else:
            devices_dict.update({'standby_state': device['state'].lower()})
    return devices_dict


def parse_check_pnote_list(raw_data: str) -> str:
    return 'There are no pnotes in problem state' not in raw_data