import unittest
import automation.workflows.TemperatureNearingMaximum.paloaltonetworks.TemperatureNearingMaximum_mock_data as mock
import automation.workflows.TemperatureNearingMaximum.paloaltonetworks.TemperatureNearingMaximum_parser as parser


class TemperatureNearingMaximumTests(unittest.TestCase):

    def test_panos_get_fan_alarm(self):
        pair1 = mock.PANOS_GET_FAN_ALARM_PAIR_1
        self.assertEqual(pair1[1], parser.panos_get_fan_alarm(pair1[0]))


if __name__ == '__main__':
    unittest.main()
