import re
from typing import List
from dateutil import parser
from parser_service.public.helper_methods import *


def check_variant_matches(**kwargs) -> bool:
    # This should work on all gateways
    tags = kwargs.get('my_device_tags', None)
    if tags is None:
        return False
    if tags.get('product') != 'firewall':
        return False
    return True


def panos_get_fan_alarm(raw_data: str) -> dict:
    parsed = parse_data_as_xml(raw_data)
    fan_data = {}
    all_fans = fan_data['description'] = parsed['response']['result']['fan']['Slot1']['entry']
    if type(all_fans) != list:
        all_fans = [all_fans]
    fan = all_fans[0]
    fan_data['alarm_state'] = fan['alarm']
    # If there is more than 1 fan? Just take the first one
    fan_data['description'] = fan['description']
    fan_data['rpm'] = fan['RPMs']
    fan_data['min'] = fan['min']
    return fan_data
