#HTTPS command: '/api?type=op&cmd=<show><system><environmentals></environmentals></system></show>
SHOW_SYSTEM_ENVIRONMENTALS_OUT_1 = """<response status="success">
<result>
<thermal>
<Slot1>
<entry>
<slot>1</slot>
<description>6220 Core Temperature</description>
<min>5.0</min>
<max>95.0</max>
<alarm>False</alarm>
<DegreesC>48.0</DegreesC>
</entry>
<entry>
<slot>1</slot>
<description>System Temperature</description>
<min>5.0</min>
<max>70.0</max>
<alarm>False</alarm>
<DegreesC>33.5</DegreesC>
</entry>
</Slot1>
</thermal>
<fan>
<Slot1>
<entry>
<slot>1</slot>
<alarm>True</alarm>
<description>Fan RPM</description>
<RPMs>3879</RPMs>
<min>1500</min>
</entry>
</Slot1>
</fan>
<power>
<Slot1>
<entry>
<slot>1</slot>
<description>1.0V CPU Core</description>
<min>0.95</min>
<Volts>1.00933333333</Volts>
<alarm>False</alarm>
<max>1.05</max>
</entry>
<entry>
<slot>1</slot>
<description>12V Power Rail</description>
<min>11.1</min>
<Volts>11.6989333333</Volts>
<alarm>False</alarm>
<max>12.6</max>
</entry>
<entry>
<slot>1</slot>
<description>5V Power Rail</description>
<min>4.6</min>
<Volts>5.0064</Volts>
<alarm>False</alarm>
<max>5.4</max>
</entry>
<entry>
<slot>1</slot>
<description>3.3V Power Rail</description>
<min>2.97</min>
<Volts>3.30133333333</Volts>
<alarm>False</alarm>
<max>3.63</max>
</entry>
<entry>
<slot>1</slot>
<description>2.5V Power Rail</description>
<min>2.25</min>
<Volts>2.52266666667</Volts>
<alarm>False</alarm>
<max>2.75</max>
</entry>
<entry>
<slot>1</slot>
<description>1.5V DDR</description>
<min>1.35</min>
<Volts>1.586</Volts>
<alarm>False</alarm>
<max>1.65</max>
</entry>
<entry>
<slot>1</slot>
<Volts>3.30133333333</Volts>
<alarm>False</alarm>
<description>3.3V RTC Battery</description>
</entry>
</Slot1>
</power>
</result>
</response>"""

PANOS_GET_FAN_ALARM_PAIR_1 = (SHOW_SYSTEM_ENVIRONMENTALS_OUT_1,
{'alarm_state': 'True', 
  'description': 'Fan RPM', 
  'rpm': '3879', 
  'min': '1500'
}
)