from typing import Tuple
from parser_service.public.helper_methods import *

def check_variant_matches(**kwargs) -> bool:
    # This should work on all gateways
    tags = kwargs.get('my_device_tags', None)
    if tags is None:
        return False
    if tags.get('routing-bgp') != 'true':
        return False
    return True


def get_bgp_peer_status(raw_data: str, bgp_peer: str) -> str:
    raw_data_parsed = parse_data_as_list(raw_data, 'checkpoint_gaia_show_bgp_peer.textfsm')
    if raw_data_parsed:
        for peer in raw_data_parsed:
            if peer['bgp_peer_id'] != bgp_peer:
                break
            else:
                return peer['bgp_peer_state']


def collect_routing_info(raw_data: str) -> dict:
    raw_data_parsed = parse_data_as_object(raw_data, 'checkpoint_gaia_ip_r_get.textfsm')
    return raw_data_parsed


def is_ip_in_arp_table(raw_data, bgp_peer: str) -> bool:
    raw_data_parsed = parse_data_as_list(raw_data, 'checkpoint_gaia_arp_an.textfsm')
    if raw_data_parsed:
        for arp_entry in raw_data_parsed:
            if arp_entry['ip_address'] == bgp_peer:
                return True
    return False



def extract_drops_collisions_present_and_carrier_counter_increase(raw_data: str) -> tuple:
    raw_data_parsed = parse_data_as_list(raw_data, 'checkpoint_gaia_ethtool_error_drop_collision_carrier.textfsm')
    if raw_data_parsed:
        for counter in raw_data_parsed[0]:
            if raw_data_parsed[0][counter] != raw_data_parsed[1][counter]:
                return (True,counter)
        return (False,'')


def make_bool_unicast_packets_received(raw_data: str) -> bool:
    raw_data_parsed = parse_data_as_object( raw_data, 'checkpoint_gaia_arping_c_3_I.textfsm' )
    if raw_data_parsed:
        if raw_data_parsed['num_probes'] != '':
            if raw_data_parsed['num_probes'] == raw_data_parsed['num_responses']:
                return True
        return False



def make_bool_connection_bgp_port_established(raw_data: str) -> bool:
    raw_data_parsed = parse_data_as_list(raw_data, 'checkpoint_gaia_netstat_nap.textfsm')
    if raw_data_parsed:
            for connection in raw_data_parsed:
                if ( connection ['remote_port'] == '179' ) and ( connection['state'] == 'ESTABLISHED' ):
                    return True
    return False


def make_bool_is_remote_peer_pingable(raw_data: str) -> bool:
    raw_data_parsed = parse_data_as_object(raw_data, 'checkpoint_gaia_ping_c_10.textfsm')
    if raw_data_parsed:
        if raw_data_parsed['pkt_loss'] == '0':
            return True
    return False




