import unittest
import automation.workflows.cross_vendor_bgp_peer_down.checkpoint.cross_vendor_bgp_peer_down_mock_data as mock
import automation.workflows.cross_vendor_bgp_peer_down.checkpoint.cross_vendor_bgp_peer_down_parser as parser
from indeni_workflow.remote.device_credentials import HttpsCredentials, SshCredentials, MockCredentials, MockInput
from indeni_workflow.remote.device_data import DeviceData
from indeni_workflow.remote.remote_type import RemoteType
from indeni_workflow.workflow_test_tool import WorkflowTestTool
from indeni_workflow.workflow_response import *
import ipaddress
import re
import unittest
from typing import Tuple


ATE = 'bgp_peer_down_workflow.yaml'


class BGPPeerDownParserTests(unittest.TestCase):


    def test_get_bgp_peer_status(self):
        pair1 = mock.GET_BGP_PEER_STATUS_PAIR_1
        pair2 = mock.GET_BGP_PEER_STATUS_PAIR_2
        self.assertEqual(pair1[1], parser.get_bgp_peer_status(pair1[0][0],pair1[0][1]))
        self.assertEqual(pair2[1], parser.get_bgp_peer_status(pair2[0][0],pair1[0][1]))


    def test_collect_routing_info(self):
        pair1 = mock.COLLECT_ROUTING_INFO_PAIR_1
        pair2 = mock.COLLECT_ROUTING_INFO_PAIR_2
        pair3 = mock.COLLECT_ROUTING_INFO_PAIR_3
        self.assertEqual(pair1[1], parser.collect_routing_info(pair1[0]))
        self.assertEqual(pair2[1], parser.collect_routing_info(pair2[0]))
        self.assertEqual(pair3[1], parser.collect_routing_info(pair3[0]))

    def test_is_ip_in_arp_table(self):
        pair1 = mock.IS_IP_IN_ARP_TABLE_PAIR_1
        pair2 = mock.IS_IP_IN_ARP_TABLE_PAIR_2
        pair3 = mock.IS_IP_IN_ARP_TABLE_PAIR_3
        self.assertEqual(pair1[1], parser.is_ip_in_arp_table(pair1[0][0],pair1[0][1]))
        self.assertEqual(pair2[1], parser.is_ip_in_arp_table(pair2[0][0],pair2[0][1]))
        self.assertEqual(pair3[1], parser.is_ip_in_arp_table(pair3[0][0],pair3[0][1]))

    def test_extract_drops_collisions_present_and_carrier_counter_increase(self):
        pair1 = mock.EXTRACT_DROPS_COLLISIONS_PRESENT_AND_CARRIER_COUNTER_INCREASE_PAIR_1
        pair2 = mock.EXTRACT_DROPS_COLLISIONS_PRESENT_AND_CARRIER_COUNTER_INCREASE_PAIR_2
        pair3 = mock.EXTRACT_DROPS_COLLISIONS_PRESENT_AND_CARRIER_COUNTER_INCREASE_PAIR_3
        self.assertEqual(pair1[1], parser.extract_drops_collisions_present_and_carrier_counter_increase(pair1[0]))
        self.assertEqual(pair2[1], parser.extract_drops_collisions_present_and_carrier_counter_increase(pair2[0]))
        self.assertEqual(pair3[1], parser.extract_drops_collisions_present_and_carrier_counter_increase(pair3[0]))

    def test_make_bool_unicast_packets_received(self):
        pair1 = mock.MAKE_BOOL_UNICAST_PACKETS_RECEIVED_PAIR_1
        pair2 = mock.MAKE_BOOL_UNICAST_PACKETS_RECEIVED_PAIR_2
        pair3 = mock.MAKE_BOOL_UNICAST_PACKETS_RECEIVED_PAIR_3
        self.assertEqual(pair1[1], parser.make_bool_unicast_packets_received(pair1[0]))
        self.assertEqual(pair2[1], parser.make_bool_unicast_packets_received(pair2[0]))
        self.assertEqual(pair3[1], parser.make_bool_unicast_packets_received(pair3[0]))

    def test_make_bool_connection_bgp_port_established(self):
        pair1 = mock.MAKE_BOOL_CONNECTION_BGP_PORT_ESTABLISHED_DATA_1
        pair2 = mock.MAKE_BOOL_CONNECTION_BGP_PORT_ESTABLISHED_DATA_2
        pair3 = mock.MAKE_BOOL_CONNECTION_BGP_PORT_ESTABLISHED_DATA_3
        self.assertEqual(pair1[1], parser.make_bool_connection_bgp_port_established(pair1[0]))
        self.assertEqual(pair2[1], parser.make_bool_connection_bgp_port_established(pair2[0]))
        self.assertEqual(pair3[1], parser.make_bool_connection_bgp_port_established(pair3[0]))

    def test_make_bool_is_remote_peer_pingable(self):
        pair1 = mock.MAKE_BOOL_IS_REMOTE_PEER_PINGABLE_DATA_1
        pair2 = mock.MAKE_BOOL_IS_REMOTE_PEER_PINGABLE_DATA_2
        self.assertEqual(pair1[1], parser.make_bool_is_remote_peer_pingable(pair1[0]))
        self.assertEqual(pair2[1], parser.make_bool_is_remote_peer_pingable(pair2[0]))


        

if __name__ == '__main__':
    unittest.main()

