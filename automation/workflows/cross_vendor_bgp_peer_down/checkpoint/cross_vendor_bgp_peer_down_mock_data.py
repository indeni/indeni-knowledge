#SSH command: clish -c "show bgp peers"
SHOW_BGP_PEERS_DATA_1 = """
Flags: R - Peer restarted, W - Waiting for End-Of-RIB from Peer

PeerID           AS           Routes  ActRts  State             InUpds  OutUpds  Uptime  
10.11.94.86      65000        0       0       Active            0       0        00:00:00  
10.11.94.90      65000        0       0       Active            0       0        00:00:00  
10.11.94.91      65000        0       0       Active            0       0        00:00:00  
10.11.94.190     65000        0       0       Active            0       0        00:00:00  
77.77.77.77      65000        0       0       Active            0       0        00:00:00
"""

GET_BGP_PEER_STATUS_PAIR_1 = (
    (SHOW_BGP_PEERS_DATA_1,
    '10.11.94.86'),
    'Active'
)

#SSH command: clish -c "show bgp peers"
SHOW_BGP_PEERS_DATA_2 = """
Flags: R - Peer restarted, W - Waiting for End-Of-RIB from Peer

PeerID           AS           Routes  ActRts  State             InUpds  OutUpds  Uptime  
10.11.94.86      65000        0       0       Established       2       1        00:00:06  
10.11.94.90      65000        0       0       Established       2       1        00:00:05  
10.11.94.91      65000        0       0       Active            0       0        00:00:00  
10.11.94.190     65000        0       0       Active            0       0        00:00:00  
77.77.77.77      65000        0       0       Active            0       0        00:00:00
"""

GET_BGP_PEER_STATUS_PAIR_2 = (
    (SHOW_BGP_PEERS_DATA_2,
    '10.11.94.86'),
    'Established'
)


#SSH command: ip r get {{bgp_peer}}
IP_R_GET_DATA_1 = """
10.10.94.86 via 10.11.94.254 dev eth0  src 10.11.94.49
    cache  mtu 1500 advmss 1460 hoplimit 64
"""

COLLECT_ROUTING_INFO_PAIR_1 = (
    IP_R_GET_DATA_1,
    {'destination_ip': '10.10.94.86',
    'next_hop_ip': '10.11.94.254',
    'interface': 'eth0',
    'source_ip': '10.11.94.49',
    'mtu': '1500',
    'advmss': '1460',
    'hoplimit': '64',
    'unreachable': ''
    }
)

#SSH command: ip r get {{bgp_peer}}
IP_R_GET_DATA_2 = """
10.11.94.86 dev eth0  src 10.11.94.49
    cache  mtu 1500 advmss 1460 hoplimit 64
"""

COLLECT_ROUTING_INFO_PAIR_2 = (
    IP_R_GET_DATA_2,
    {'destination_ip': '10.11.94.86',
    'next_hop_ip': '',
    'interface': 'eth0',
    'source_ip': '10.11.94.49',
    'mtu': '1500',
    'advmss': '1460',
    'hoplimit': '64',
    'unreachable': ''
    }
)

#SSH command: ip r get {{bgp_peer}}
IP_R_GET_DATA_3 = """
RTNETLINK answers: Network is unreachable
"""

COLLECT_ROUTING_INFO_PAIR_3 = (
    IP_R_GET_DATA_3,
    {'destination_ip': '',
    'next_hop_ip': '',
    'interface': '',
    'source_ip': '',
    'mtu': '',
    'advmss': '',
    'hoplimit': '',
    'unreachable': 'Network is unreachable'
    }
)

#SSH command: arp -an | grep {{bgp_peer}}
ARP_AN_DATA_1 = """
? (10.11.94.86) at 00:50:56:AC:8E:D0 [ether] on eth0
"""

IS_IP_IN_ARP_TABLE_PAIR_1 = (
    (ARP_AN_DATA_1,'10.11.94.86'),
    True
)

#SSH command: arp -an | grep {{bgp_peer}}
ARP_AN_DATA_2 = """
"""

IS_IP_IN_ARP_TABLE_PAIR_2 = (
    (ARP_AN_DATA_2,'10.11.94.86'),
    False
)

ARP_AN_DATA_3 = """
? (10.3.3.155) at 00:50:56:A0:36:E7 [ether] on eth0
? (172.16.20.231) at 00:0c:29:c0:94:bf on eth7.2
? (10.3.3.211) at <incomplete> on eth0
? (172.16.20.22) at 00:bc:29:c0:94:bf permanent
"""

IS_IP_IN_ARP_TABLE_PAIR_3 = (
    (ARP_AN_DATA_3,'10.3.3.211'),
    True
)

#SSH command: ethtool -S {{tuple_interface_and_is_l2[0]}} | grep -E 'error|drop|collision|carrier'; sleep 2; ethtool -S {{tuple_interface_and_is_l2[0]}} | grep -E 'error|drop|collision|carrier'
ETHTOOL_S_INTERFACE_ERRORS_DATA_1 = """
     rx_errors: 5
     tx_errors: 0
     tx_dropped: 0
     collisions: 0
     rx_length_errors: 0
     rx_over_errors: 0
     rx_crc_errors: 0
     rx_frame_errors: 0
     rx_missed_errors: 0
     tx_aborted_errors: 0
     tx_carrier_errors: 0
     tx_fifo_errors: 0
     tx_heartbeat_errors: 0
     tx_window_errors: 0
     rx_long_length_errors: 0
     rx_short_length_errors: 0
     rx_align_errors: 0
     rx_csum_offload_errors: 0
     dropped_smbus: 0
     rx_errors: 10
     tx_errors: 0
     tx_dropped: 0
     collisions: 0
     rx_length_errors: 0
     rx_over_errors: 0
     rx_crc_errors: 0
     rx_frame_errors: 0
     rx_missed_errors: 0
     tx_aborted_errors: 0
     tx_carrier_errors: 0
     tx_fifo_errors: 0
     tx_heartbeat_errors: 0
     tx_window_errors: 0
     rx_long_length_errors: 0
     rx_short_length_errors: 0
     rx_align_errors: 0
     rx_csum_offload_errors: 0
     dropped_smbus: 0
"""

EXTRACT_DROPS_COLLISIONS_PRESENT_AND_CARRIER_COUNTER_INCREASE_PAIR_1 = (
    ETHTOOL_S_INTERFACE_ERRORS_DATA_1,
    (True,'rx_errors')
)


#SSH command: ethtool -S {{tuple_interface_and_is_l2[0]}} | grep -E 'error|drop|collision|carrier'; sleep 2; ethtool -S {{tuple_interface_and_is_l2[0]}} | grep -E 'error|drop|collision|carrier'
ETHTOOL_S_INTERFACE_ERRORS_DATA_2 = """
     rx_errors: 0
     tx_errors: 0
     tx_dropped: 0
     collisions: 0
     rx_length_errors: 0
     rx_over_errors: 0
     rx_crc_errors: 0
     rx_frame_errors: 0
     rx_missed_errors: 0
     tx_aborted_errors: 0
     tx_carrier_errors: 5
     tx_fifo_errors: 0
     tx_heartbeat_errors: 0
     tx_window_errors: 0
     rx_long_length_errors: 0
     rx_short_length_errors: 0
     rx_align_errors: 0
     rx_csum_offload_errors: 0
     dropped_smbus: 0
     rx_errors: 0
     tx_errors: 0
     tx_dropped: 0
     collisions: 0
     rx_length_errors: 0
     rx_over_errors: 0
     rx_crc_errors: 0
     rx_frame_errors: 0
     rx_missed_errors: 0
     tx_aborted_errors: 0
     tx_carrier_errors: 10
     tx_fifo_errors: 0
     tx_heartbeat_errors: 0
     tx_window_errors: 0
     rx_long_length_errors: 0
     rx_short_length_errors: 0
     rx_align_errors: 0
     rx_csum_offload_errors: 0
     dropped_smbus: 0
"""

EXTRACT_DROPS_COLLISIONS_PRESENT_AND_CARRIER_COUNTER_INCREASE_PAIR_2 = (
    ETHTOOL_S_INTERFACE_ERRORS_DATA_2,
    (True,'tx_carrier_errors')
)

#SSH command: ethtool -S {{tuple_interface_and_is_l2[0]}} | grep -E 'error|drop|collision|carrier'; sleep 2; ethtool -S {{tuple_interface_and_is_l2[0]}} | grep -E 'error|drop|collision|carrier'
ETHTOOL_S_INTERFACE_ERRORS_DATA_3 = """
     rx_errors: 0
     tx_errors: 0
     tx_dropped: 0
     collisions: 0
     rx_length_errors: 0
     rx_over_errors: 0
     rx_crc_errors: 0
     rx_frame_errors: 0
     rx_missed_errors: 0
     tx_aborted_errors: 0
     tx_carrier_errors: 0
     tx_fifo_errors: 0
     tx_heartbeat_errors: 0
     tx_window_errors: 0
     rx_long_length_errors: 0
     rx_short_length_errors: 0
     rx_align_errors: 0
     rx_csum_offload_errors: 0
     dropped_smbus: 0
     rx_errors: 0
     tx_errors: 0
     tx_dropped: 0
     collisions: 0
     rx_length_errors: 0
     rx_over_errors: 0
     rx_crc_errors: 0
     rx_frame_errors: 0
     rx_missed_errors: 0
     tx_aborted_errors: 0
     tx_carrier_errors: 0
     tx_fifo_errors: 0
     tx_heartbeat_errors: 0
     tx_window_errors: 0
     rx_long_length_errors: 0
     rx_short_length_errors: 0
     rx_align_errors: 0
     rx_csum_offload_errors: 0
     dropped_smbus: 0
"""

EXTRACT_DROPS_COLLISIONS_PRESENT_AND_CARRIER_COUNTER_INCREASE_PAIR_3 = (
    ETHTOOL_S_INTERFACE_ERRORS_DATA_3,
    (False,'')
)

#SSH command: arping -c 3 -I {{tuple_interface_and_is_l2[0]}} {{bgp_peer}}
ARP_PING_C_3_I_DATA_1 = """
ARPING 10.11.94.86 from 10.11.94.49 eth0
Unicast reply from 10.11.94.86 [00:50:56:AC:8E:D0]  109.488ms
Unicast reply from 10.11.94.86 [00:50:56:AC:8E:D0]  0.867ms
Unicast reply from 10.11.94.86 [00:50:56:AC:8E:D0]  0.744ms
Sent 3 probes (1 broadcast(s))
Received 3 response(s)
"""
MAKE_BOOL_UNICAST_PACKETS_RECEIVED_PAIR_1 = (
    ARP_PING_C_3_I_DATA_1,
    True
    )


#SSH command: arping -c 3 -I {{tuple_interface_and_is_l2[0]}} {{bgp_peer}}
ARP_PING_C_3_I_DATA_2 = """
ARPING 10.11.94.86 from 10.11.94.49 eth0
"""

MAKE_BOOL_UNICAST_PACKETS_RECEIVED_PAIR_2 = (
    ARP_PING_C_3_I_DATA_2,
    False
    )

#SSH command: arping -c 3 -I {{tuple_interface_and_is_l2[0]}} {{bgp_peer}}
ARP_PING_C_3_I_DATA_3 = """
ARPING 10.11.94.86 from 10.11.94.49 eth0
Sent 3 probes (3 broadcast(s))
Received 0 response(s)
"""

MAKE_BOOL_UNICAST_PACKETS_RECEIVED_PAIR_3 = (
    ARP_PING_C_3_I_DATA_3,
    False
    )

#SSH command: netstat -nap | grep :179 | grep {{bgp_peer}}
NETSTAT_NAP_DATA_1 = """
tcp        0      0 10.11.94.49:35215           10.11.94.86:179             ESTABLISHED 13600/routed
"""

MAKE_BOOL_CONNECTION_BGP_PORT_ESTABLISHED_DATA_1 = (
    NETSTAT_NAP_DATA_1,
    True
)

#SSH command: netstat -nap | grep :179 | grep {{bgp_peer}}
NETSTAT_NAP_DATA_2 = """
tcp        0      0 10.11.94.49:35215           10.11.94.86:179             TIME_WAIT 13600/routed
""",

MAKE_BOOL_CONNECTION_BGP_PORT_ESTABLISHED_DATA_2 = (
    NETSTAT_NAP_DATA_2,
    False
)

#SSH command: netstat -nap | grep :179 | grep {{bgp_peer}}
NETSTAT_NAP_DATA_3 = """
""",

MAKE_BOOL_CONNECTION_BGP_PORT_ESTABLISHED_DATA_3 = (
    NETSTAT_NAP_DATA_3,
    False)

#SSH command: ping -c 10 {{bgp_peer}}
PING_C_10_DATA_1 = """
PING 10.11.94.86 (10.11.94.86) 56(84) bytes of data.
64 bytes from 10.11.94.86: icmp_seq=1 ttl=64 time=1.69 ms
64 bytes from 10.11.94.86: icmp_seq=2 ttl=64 time=62.9 ms
64 bytes from 10.11.94.86: icmp_seq=3 ttl=64 time=2.99 ms
64 bytes from 10.11.94.86: icmp_seq=4 ttl=64 time=16.5 ms
64 bytes from 10.11.94.86: icmp_seq=5 ttl=64 time=92.9 ms
64 bytes from 10.11.94.86: icmp_seq=6 ttl=64 time=13.3 ms
64 bytes from 10.11.94.86: icmp_seq=7 ttl=64 time=25.0 ms
64 bytes from 10.11.94.86: icmp_seq=8 ttl=64 time=0.485 ms
64 bytes from 10.11.94.86: icmp_seq=9 ttl=64 time=131 ms
64 bytes from 10.11.94.86: icmp_seq=10 ttl=64 time=1.44 ms

--- 10.11.94.86 ping statistics ---
10 packets transmitted, 10 received, 0% packet loss, time 9248ms
rtt min/avg/max/mdev = 0.485/34.923/131.684/43.408 ms
"""


MAKE_BOOL_IS_REMOTE_PEER_PINGABLE_DATA_1 = (
    PING_C_10_DATA_1,
    True
)

#SSH command: ping -c 10 {{bgp_peer}}
PING_C_10_DATA_2 = """
PING 10.11.94.86 (10.11.94.86) 56(84) bytes of data.

--- 10.11.94.86 ping statistics ---
10 packets transmitted, 0 received, 100% packet loss, time 9203ms
"""

MAKE_BOOL_IS_REMOTE_PEER_PINGABLE_DATA_2 = (
    PING_C_10_DATA_2,
    False
)
