import unittest
import automation.workflows.cross_vendor_bgp_peer_down.paloaltonetworks.cross_vendor_bgp_peer_down_mock_data as mock
from automation.workflows.cross_vendor_bgp_peer_down.paloaltonetworks.cross_vendor_bgp_peer_down_parser import *
from indeni_workflow.remote.device_credentials import HttpsCredentials, SshCredentials, MockCredentials, MockInput
from indeni_workflow.remote.device_data import DeviceData
from indeni_workflow.remote.remote_type import RemoteType
from indeni_workflow.workflow_test_tool import WorkflowTestTool
from indeni_workflow.workflow_response import *
import ipaddress
import re
import unittest
from typing import Tuple


ATE = 'bgp_peer_down_workflow.yaml'


class BGPPeerDownParserTests(unittest.TestCase):


    def test_get_single_bgp_peer_tuple(self):
        pair1 = mock.PANOS_SHOW_ROUTING_PROTOCOL_BGP_SUMMARY_MATCH_PEER_PAIR_1
        pair2 = mock.PANOS_SHOW_ROUTING_PROTOCOL_BGP_SUMMARY_MATCH_PEER_PAIR_2
        pair3 = mock.PANOS_SHOW_ROUTING_PROTOCOL_BGP_SUMMARY_MATCH_PEER_PAIR_3
        pair4 = mock.PANOS_SHOW_ROUTING_PROTOCOL_BGP_SUMMARY_MATCH_PEER_PAIR_4
        self.assertEqual(pair1[1], get_single_bgp_peer_tuple(pair1[0][0],pair1[0][1]))
        self.assertEqual(pair2[1], get_single_bgp_peer_tuple(pair2[0][0],pair2[0][1]))
        self.assertEqual(pair3[1], get_single_bgp_peer_tuple(pair3[0][0],pair3[0][1]))
        self.assertEqual(pair4[1], get_single_bgp_peer_tuple(pair4[0][0],pair4[0][1]))

    def test_get_local_address_of_down_peer(self):
        pair1 = mock.PANOS_SHOW_ROUTING_PROTOCOL_BGP_PEER_PEER_NAME_PAIR_1
        pair2 = mock.PANOS_SHOW_ROUTING_PROTOCOL_BGP_PEER_PEER_NAME_PAIR_2
        self.assertEqual(pair1[1], get_local_address_of_down_peer(pair1[0]))
        self.assertEqual(pair2[1], get_local_address_of_down_peer(pair2[0]))

    def test_get_ping_status(self):
        pair1 = mock.PANOS_PING_PAIR_1
        pair2 = mock.PANOS_PING_PAIR_2
        self.assertEqual(pair1[1], get_ping_status(pair1[0]))
        self.assertEqual(pair2[1], get_ping_status(pair2[0]))  

    def test_get_interface_status(self):
        pair1 = mock.PANOS_SHOW_INTERFACE_ALL_PAIR_1
        pair2 = mock.PANOS_SHOW_INTERFACE_ALL_PAIR_2
        self.assertEqual(pair1[1], get_interface_status(pair1[0][0], str_local_ip_for_bgp=pair1[0][1]))
        self.assertEqual(pair2[1], get_interface_status(pair2[0][0], str_local_ip_for_bgp=pair2[0][1]))

    def test_get_if_firewall_blocking_bgp(self):
        pair1 = mock.PANOS_SHOW_LOG_TRAFFIC_PAIR_1
        pair2 = mock.PANOS_SHOW_LOG_TRAFFIC_PAIR_2
        pair3 = mock.PANOS_SHOW_LOG_TRAFFIC_PAIR_3
        self.assertEqual(pair1[1], get_if_firewall_blocking_bgp(pair1[0]))
        self.assertEqual(pair2[1], get_if_firewall_blocking_bgp(pair2[0]))
        self.assertEqual(pair3[1], get_if_firewall_blocking_bgp(pair3[0]))

    def test_get_if_errors_exist_in_routing_log(self):
        pair1 = mock.PANOS_GREP_CONTEXT_20_MP_LOG_ROUTED_LOG_PATTERN_PAIR_1
        pair2 = mock.PANOS_GREP_CONTEXT_20_MP_LOG_ROUTED_LOG_PATTERN_PAIR_2
        self.assertEqual(pair1[1], get_if_errors_exist_in_routing_log(pair1[0]))
        self.assertEqual(pair2[1], get_if_errors_exist_in_routing_log(pair2[0]))


if __name__ == '__main__':
    unittest.main()

