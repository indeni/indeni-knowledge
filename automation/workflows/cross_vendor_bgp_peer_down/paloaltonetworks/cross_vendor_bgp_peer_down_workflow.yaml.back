id: bgp_peer_down_paloaltonetworks
friendly_name: bgp_peer_down_paloaltonetworks
start_block: get_device_tags

blocks:

  get_device_tags:
    type: device_tags
    name: Get device tags
    register_to: my_device_tags
    go_to: get_issue_items

  get_issue_items:
    type: issue_items
    name: Get issue items
    register_to: my_items
    go_to: start_loop

  start_loop:
    type: foreach
    name: Start loop
    register_item_to: bgp_peer
    start_block: get_bgp_peers
    blocks:

      get_bgp_peers:
        type: device_task
        name: Get BGP peers
        runner:
          type: HTTPS
          command: /api/?type=op&cmd=<show><routing><protocol><bgp><peer></peer></bgp></protocol></routing></show>
        parser:
          method: get_single_bgp_peer_tuple
          args: [bgp_peer]
        register_to: tuple_bgp_status_and_ip
        go_to: check_if_peer_is_down

      check_if_peer_is_down:
        type: if
        name: Check if peer is down
        condition: tuple_bgp_status_and_ip[0] == 'Down'
        then_go_to: get_local_ip_for_bgp_peering
        else_go_to: conclusion_all_peers_established

      conclusion_all_peers_established:
        type: conclusion
        name: All peers are established
        triage_conclusion: The peers are up and require no troubleshooting
        triage_remediation_steps: |
          You may want to open a ticket with Palo Alto Networks support especially if the issue recurs.

      get_local_ip_for_bgp_peering:
        type: device_task
        name: Get local IP for BGP peering
        runner:
          type: HTTPS
          command: /api/?type=op&cmd=<show><routing><protocol><bgp><peer><peer-name>{{bgp_peer}}</peer-name></peer></bgp></protocol></routing></show>
        parser:
          method: get_local_address_of_down_peer
          args: []
        register_to: str_local_ip_for_bgp
        go_to: check_if_is_ipv6

      check_if_is_ipv6:
        type: if
        name: Check if IP is IPv6
        condition: |
          ':' in str_local_ip_for_bgp
        then_go_to: get_ping_bgp_peer_form_local_interface_ip_address_ipv6
        else_go_to: get_ping_bgp_peer_from_local_interface_ip_address

      get_ping_bgp_peer_from_local_interface_ip_address:
        type: device_task
        name: Get ping status of BGP peer from local interface IP address
        runner:
          type: SSH
          command: ping count 3 source {{str_local_ip_for_bgp}} host {{tuple_bgp_status_and_ip[1]}}
        parser:
          method: get_ping_status
          args: []
        register_to: bool_ping_is_success
        go_to: check_if_ping_is_success

      get_ping_bgp_peer_form_local_interface_ip_address_ipv6:
        type: device_task
        name: Get ping status of BGP peer from local interface IP address
        runner:
          type: SSH
          command: ping inet6 yes count 3 source {{str_local_ip_for_bgp}} host {{tuple_bgp_status_and_ip[1]}}
        parser:
          method: get_ping_status
          args: []
        register_to: bool_ping_is_success
        go_to: check_if_ping_is_success

      check_if_ping_is_success:
        type: if
        name: Check if ping was successful
        condition: bool_ping_is_success
        then_go_to: get_interface_status
        else_go_to: conclusion_firewall_failed_to_ping_peer

      conclusion_firewall_failed_to_ping_peer:
        type: conclusion
        name: Firewall failed to ping the peer
        triage_conclusion: Ping failed form the PA local IP to the Peer IP
        triage_remediation_steps: |
          Continue to determine potential causes such as interface down or firewall rule blocking traffic.

      get_interface_status:
        type: device_task
        name: Get interface status
        runner:
          type: HTTPS
          command: /api/?type=op&cmd=<show><interface></interface></show
        parser:
          method: get_interface_status
          args: [str_local_ip_for_bgp]
        register_to: str_interface_status
        go_to: check_if_interface_up

      check_if_interface_up:
        type: if
        name: Check if interface is up
        condition: "str_interface_status == 'up'"
        then_go_to: get_if_firewall_rule_is_blocking_bgp_from_firewall
        else_go_to: conclusion_interface_connecting_to_bgp_peer_is_down

      conclusion_interface_connecting_to_bgp_peer_is_down:
        type: conclusion
        name: The interface connecting to the BGP peer is down.
        triage_conclusion: The interface connecting to the BGP peer is down.
        triage_remediation_steps: |
          Check neighboring switch for physical connectivity and also check Indeni for other alerts.

      get_if_firewall_rule_is_blocking_bgp_from_firewall:
        type: device_task
        name: Get if firewall rule is blocking BGP from firewall
        runner:
          type: SSH
          command: show log traffic src in {{str_local_ip_for_bgp}} dst in {{tuple_bgp_status_and_ip[1]}} port equal 179 direction equal backward receive_time in last-hour
        parser:
          method: get_if_firewall_blocking_bgp
          args: []
        register_to: bool_is_firewall_blocking_bgp_from_firewall
        go_to: get_if_firewall_rule_is_blocking_bgp_from_peer

      get_if_firewall_rule_is_blocking_bgp_from_peer:
        type: device_task
        name: Get if firewall rule is blocking BGP from peer
        runner:
          type: SSH
          command: show log traffic src in {{tuple_bgp_status_and_ip[1]}} dst in {{str_local_ip_for_bgp}} port equal 179 direction equal backward receive_time in last-hour
        parser:
          method: get_if_firewall_blocking_bgp
          args: []
        register_to: bool_is_firewall_blocking_bgp_from_peer
        go_to: check_if_firewall_blocking_bgp_traffic

      check_if_firewall_blocking_bgp_traffic:
        type: if
        name: Check if firewall is blocking BGP traffic
        condition: bool_is_firewall_blocking_bgp_from_peer or bool_is_firewall_blocking_bgp_from_firewall
        then_go_to: conclusion_firewall_blocking_traffic_to_or_from_bgp_peer
        else_go_to: get_if_any_errors_exist_in_routing_log

      conclusion_firewall_blocking_traffic_to_or_from_bgp_peer:
        type: conclusion
        name: The firewall is blocking traffic to or from the BGP peer.
        triage_conclusion: The firewall is blocking traffic to or from the BGP peer.
        triage_remediation_steps: |
          Create a rule for BGP that permits traffic to and from the Local and Peer IP addresses destined to TCP port 179

      get_if_any_errors_exist_in_routing_log:
        type: device_task
        name: Get if any errors exist in the routing log
        runner:
          type: SSH
          command: grep context 20 mp-log routed.log pattern {{tuple_bgp_status_and_ip[1]}}
        parser:
          method: get_if_errors_exist_in_routing_log
          args: []
        register_to: bool_errors_exist_in_routing_log
        go_to: check_if_errors_exist_in_routing_log

      check_if_errors_exist_in_routing_log:
        type: if
        name: Check if errors exist in routing log
        condition: bool_errors_exist_in_routing_log
        then_go_to: conclusion_bgp_down_cause_not_found_log_entries
        else_go_to: conclusion_bgp_down_cause_not_found

      conclusion_bgp_down_cause_not_found:
        type: conclusion
        name: The BGP Peer is down, cause not found.
        triage_conclusion: BGP peer down cause not found
        triage_remediation_steps: |
          Check connected hardware configurations and status and open a support call with Palo Alto Networks.

      conclusion_bgp_down_cause_not_found_log_entries:
        type: conclusion
        name: The BGP Peer is down, cause not found - Log Entries
        triage_conclusion: |
          BGP peer down cause not found but log entries discovered.
        triage_remediation_steps: |
          Issues my have been found in the routed.log
          - Please, manually run "less mp-log routed.log" for more line context/information around the log entries found.
          - Check connected hardware configuration and status and open a support call with Palo Alto Networks.
          - Verify the authentication profile password matches between the peers.