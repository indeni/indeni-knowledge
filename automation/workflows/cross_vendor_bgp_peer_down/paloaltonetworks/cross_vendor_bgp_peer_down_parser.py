import ipaddress
import re
import unittest
from typing import Tuple
from parser_service.public.helper_methods import *


def get_single_bgp_peer_tuple(raw_data: str, bgp_peer: str) -> Tuple[str, str]:
    """
    Parse a tuple of (Status, IP)
    :param raw_data: input str
    :return: the tuple
    """
    data = parse_data_as_xml(raw_data)
    if data:
        peer_entries = data['response']['result'] or []
        for peer in peer_entries:
            if isinstance(peer_entries[peer], list): 
                for single_peer in peer_entries[peer]:
                    if  str(single_peer['@peer']) == bgp_peer:
                        ip_address = single_peer['peer-address']
                        ip_address = ip_address.split(':')[0]
                        status = single_peer['status']
            else:
                if peer_entries[peer]['@peer'] == bgp_peer:
                    ip_address = peer_entries[peer]['peer-address']
                    ip_address = ip_address.split(':')[0]
                    status = peer_entries[peer]['status']
            return status, ip_address
    return 'Down', ''


def get_local_address_of_down_peer(raw_data: str) -> str:
    data = parse_data_as_xml(raw_data)
    if data:
        ip_address = data['response']['result']['entry']['local-address']
        ip_address = ip_address.split(':')[0]
        return ip_address
    return ''


def get_ping_status(raw_data: str) -> bool:
    return ' 0% packet loss' in raw_data


def get_interface_status(raw_data: str, **kwargs) -> str:
    str_local_ip_for_bgp = kwargs.get('str_local_ip_for_bgp')
    if str_local_ip_for_bgp is None:
        return 'down'

    interface_for_ip = ''
    data = parse_data_as_xml(raw_data)
    ifnet_entries = data['response']['result']['ifnet']
    for entry in ifnet_entries:
        for interface in ifnet_entries[entry]:
            ip_address = interface['ip'].split('/')[0]
            if ip_address == str_local_ip_for_bgp:
                interface_for_ip = interface['name']

    hw_entries = data['response']['result']['hw']
    for entry in hw_entries:
        for interface in hw_entries[entry]:
            if interface['name'] == interface_for_ip:
                state_for_interface = interface['state']
                return state_for_interface
    return 'down'


def get_if_firewall_blocking_bgp(raw_data: str) -> bool:
    lines = raw_data.split('\n')
    index = None
    for idx, line in enumerate(lines):
        if '==========' in line:
            index = idx
            break
    remaining_lines = '\n'.join(lines[index + 1:])
    return 'deny' in remaining_lines


def get_if_errors_exist_in_routing_log(raw_data: str) -> bool:
    lines = raw_data.strip().split('\n')
    for line in lines:
        if 'EXCEPTION' in line:
            return True
    return False


def is_ipv6(raw_data: str) -> bool:
    try:
        ip = ipaddress.ip_address(raw_data.strip())
        return type(ip) == ipaddress.IPv6Address
    except ValueError:
        return False
