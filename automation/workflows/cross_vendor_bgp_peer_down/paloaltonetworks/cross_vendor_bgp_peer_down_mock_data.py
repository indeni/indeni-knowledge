PANOS_SHOW_ROUTING_PROTOCOL_BGP_SUMMARY_MATCH_PEER_OUT_1 = """
<response status="success">
   <result>
      <entry peer="VyOS-Rtr" vr="default">
         <peer-group>VyOS-Peer-Group</peer-group>
         <peer-router-id>172.30.250.2</peer-router-id>
         <remote-as>65535</remote-as>
         <status>Established</status>
         <status-duration>91371</status-duration>
         <password-set>no</password-set>
         <passive>no</passive>
         <multi-hop-ttl>1</multi-hop-ttl>
         <peer-address>172.30.250.2:38387</peer-address>
         <local-address>172.30.250.1:179</local-address>
         <reflector-client>not-client</reflector-client>
         <same-confederation>no</same-confederation>
         <aggregate-confed-as>yes</aggregate-confed-as>
         <peering-type>Unspecified</peering-type>
         <connect-retry-interval>1</connect-retry-interval>
         <open-delay>0</open-delay>
         <idle-hold>15</idle-hold>
         <prefix-limit>5000</prefix-limit>
         <holdtime>90</holdtime>
         <holdtime-config>90</holdtime-config>
         <keepalive>30</keepalive>
         <keepalive-config>30</keepalive-config>
         <msg-update-in>7</msg-update-in>
         <msg-update-out>2</msg-update-out>
         <msg-total-in>3056</msg-total-in>
         <msg-total-out>3499</msg-total-out>
         <last-update-age>24</last-update-age>
         <last-error>Cease (6) : connection rejected (5)</last-error>
         <status-flap-counts>2</status-flap-counts>
         <established-counts>1</established-counts>
         <ORF-entry-received>0</ORF-entry-received>
         <nexthop-self>no</nexthop-self>
         <nexthop-thirdparty>yes</nexthop-thirdparty>
         <nexthop-peer>no</nexthop-peer>
         <config>
            <remove-private-as>yes</remove-private-as>
         </config>
         <peer-capability>
            <list>
               <capability>Multiprotocol Extensions(1)</capability>
               <value>IPv4 Unicast</value>
            </list>
            <list>
               <capability>Route Refresh(2)</capability>
               <value>yes</value>
            </list>
            <list>
               <capability>4-Byte AS Number(65)</capability>
               <value>65535</value>
            </list>
            <list>
               <capability>Route Refresh (Cisco)(128)</capability>
               <value>yes</value>
            </list>
         </peer-capability>
         <prefix-counter>
            <entry afi-safi="bgpAfiIpv4-unicast">
               <incoming-total>4143</incoming-total>
               <incoming-accepted>4143</incoming-accepted>
               <incoming-rejected>0</incoming-rejected>
               <policy-rejected>0</policy-rejected>
               <outgoing-total>1</outgoing-total>
               <outgoing-advertised>1</outgoing-advertised>
            </entry>
         </prefix-counter>
      </entry>
   </result>
</response>
"""

PANOS_SHOW_ROUTING_PROTOCOL_BGP_SUMMARY_MATCH_PEER_OUT_2 = """
<response status="success">
   <result>
      <entry peer="VyOS-Rtr3" vr="default">
         <peer-group>VyOS-Peer-Group</peer-group>
         <peer-router-id>172.30.250.2</peer-router-id>
         <remote-as>65535</remote-as>
         <status>Established</status>
         <status-duration>91371</status-duration>
         <password-set>no</password-set>
         <passive>no</passive>
         <multi-hop-ttl>1</multi-hop-ttl>
         <peer-address>172.30.250.3:38387</peer-address>
         <local-address>172.30.250.1:179</local-address>
         <reflector-client>not-client</reflector-client>
         <same-confederation>no</same-confederation>
         <aggregate-confed-as>yes</aggregate-confed-as>
         <peering-type>Unspecified</peering-type>
         <connect-retry-interval>1</connect-retry-interval>
         <open-delay>0</open-delay>
         <idle-hold>15</idle-hold>
         <prefix-limit>5000</prefix-limit>
         <holdtime>90</holdtime>
         <holdtime-config>90</holdtime-config>
         <keepalive>30</keepalive>
         <keepalive-config>30</keepalive-config>
         <msg-update-in>7</msg-update-in>
         <msg-update-out>2</msg-update-out>
         <msg-total-in>3056</msg-total-in>
         <msg-total-out>3499</msg-total-out>
         <last-update-age>24</last-update-age>
         <last-error>Cease (6) : connection rejected (5)</last-error>
         <status-flap-counts>2</status-flap-counts>
         <established-counts>1</established-counts>
         <ORF-entry-received>0</ORF-entry-received>
         <nexthop-self>no</nexthop-self>
         <nexthop-thirdparty>yes</nexthop-thirdparty>
         <nexthop-peer>no</nexthop-peer>
         <config>
            <remove-private-as>yes</remove-private-as>
         </config>
         <peer-capability>
            <list>
               <capability>Multiprotocol Extensions(1)</capability>
               <value>IPv4 Unicast</value>
            </list>
            <list>
               <capability>Route Refresh(2)</capability>
               <value>yes</value>
            </list>
            <list>
               <capability>4-Byte AS Number(65)</capability>
               <value>65535</value>
            </list>
            <list>
               <capability>Route Refresh (Cisco)(128)</capability>
               <value>yes</value>
            </list>
         </peer-capability>
         <prefix-counter>
            <entry afi-safi="bgpAfiIpv4-unicast">
               <incoming-total>4143</incoming-total>
               <incoming-accepted>4143</incoming-accepted>
               <incoming-rejected>0</incoming-rejected>
               <policy-rejected>0</policy-rejected>
               <outgoing-total>1</outgoing-total>
               <outgoing-advertised>1</outgoing-advertised>
            </entry>
         </prefix-counter>
      </entry>
   </result>
</response>
"""

PANOS_SHOW_ROUTING_PROTOCOL_BGP_SUMMARY_MATCH_PEER_OUT_3 = """
                    <response status="success">
                        <result>
                            <entry peer="VyOS-Rtr" vr="default">
                                <peer-group>VyOS-Peer-Group</peer-group>
                                <peer-router-id>172.30.250.2</peer-router-id>
                                <remote-as>65535</remote-as>
                                <status>Connect</status>
                                <status-duration>142</status-duration>
                                <password-set>no</password-set>
                                <passive>no</passive>
                                <multi-hop-ttl>1</multi-hop-ttl>
                                <peer-address>172.30.250.2</peer-address>
                                <local-address>172.30.250.1</local-address>
                                <reflector-client>not-client</reflector-client>
                                <same-confederation>no</same-confederation>
                                <aggregate-confed-as>yes</aggregate-confed-as>
                                <peering-type>Unspecified</peering-type>
                                <connect-retry-interval>1</connect-retry-interval>
                                <open-delay>0</open-delay>
                                <idle-hold>15</idle-hold>
                                <prefix-limit>5000</prefix-limit>
                                <holdtime>90</holdtime>
                                <holdtime-config>90</holdtime-config>
                                <keepalive>30</keepalive>
                                <keepalive-config>30</keepalive-config>
                                <msg-update-in>14</msg-update-in>
                                <msg-update-out>2</msg-update-out>
                                <msg-total-in>47</msg-total-in>
                                <msg-total-out>60</msg-total-out>
                                <last-update-age>151</last-update-age>
                                <last-error>Cease (6) : administrative reset (4)</last-error>
                                <status-flap-counts>439</status-flap-counts>
                                <established-counts>2</established-counts>
                                <ORF-entry-received>0</ORF-entry-received>
                                <nexthop-self>no</nexthop-self>
                                <nexthop-thirdparty>yes</nexthop-thirdparty>
                                <nexthop-peer>no</nexthop-peer>
                                <config>
                                    <remove-private-as>yes</remove-private-as>
                                </config>
                                <peer-capability></peer-capability>
                                <prefix-counter></prefix-counter>
                            </entry>
                        </result>
                    </response>

"""

PANOS_SHOW_ROUTING_PROTOCOL_BGP_SUMMARY_MATCH_PEER_OUT_4 = """
                  <response status="success">
                     <result>
                        <entry peer="jorge-peer" vr="default">
                              <peer-group>test_Jorge_BGP</peer-group>
                              <peer-router-id>0.0.0.0</peer-router-id>
                              <remote-as>65535</remote-as>
                              <status>Active</status>
                              <status-duration>0</status-duration>
                              <password-set>no</password-set>
                              <passive>no</passive>
                              <multi-hop-ttl>1</multi-hop-ttl>
                              <peer-address>172.30.250.7</peer-address>
                              <local-address>10.250.250.1</local-address>
                              <reflector-client>not-client</reflector-client>
                              <same-confederation>no</same-confederation>
                              <aggregate-confed-as>yes</aggregate-confed-as>
                              <peering-type>Unspecified</peering-type>
                              <connect-retry-interval>1</connect-retry-interval>
                              <open-delay>0</open-delay>
                              <idle-hold>15</idle-hold>
                              <prefix-limit>5000</prefix-limit>
                              <holdtime>0</holdtime>
                              <holdtime-config>90</holdtime-config>
                              <keepalive>0</keepalive>
                              <keepalive-config>30</keepalive-config>
                              <msg-update-in>0</msg-update-in>
                              <msg-update-out>0</msg-update-out>
                              <msg-total-in>0</msg-total-in>
                              <msg-total-out>0</msg-total-out>
                              <last-update-age>359</last-update-age>
                              <last-error></last-error>
                              <status-flap-counts>920</status-flap-counts>
                              <established-counts>0</established-counts>
                              <ORF-entry-received>0</ORF-entry-received>
                              <nexthop-self>no</nexthop-self>
                              <nexthop-thirdparty>yes</nexthop-thirdparty>
                              <nexthop-peer>no</nexthop-peer>
                              <config>
                                 <remove-private-as>yes</remove-private-as>
                              </config>
                              <peer-capability></peer-capability>
                              <prefix-counter></prefix-counter>
                        </entry>
                        <entry peer="VyOS-Rtr" vr="default">
                              <peer-group>VyOS-Peer-Group</peer-group>
                              <peer-router-id>0.0.0.0</peer-router-id>
                              <remote-as>65535</remote-as>
                              <status>Connect</status>
                              <status-duration>0</status-duration>
                              <password-set>no</password-set>
                              <passive>no</passive>
                              <multi-hop-ttl>1</multi-hop-ttl>
                              <peer-address>172.30.250.5</peer-address>
                              <local-address>172.30.250.1</local-address>
                              <reflector-client>not-client</reflector-client>
                              <same-confederation>no</same-confederation>
                              <aggregate-confed-as>yes</aggregate-confed-as>
                              <peering-type>Unspecified</peering-type>
                              <connect-retry-interval>1</connect-retry-interval>
                              <open-delay>0</open-delay>
                              <idle-hold>15</idle-hold>
                              <prefix-limit>5000</prefix-limit>
                              <holdtime>0</holdtime>
                              <holdtime-config>90</holdtime-config>
                              <keepalive>0</keepalive>
                              <keepalive-config>30</keepalive-config>
                              <msg-update-in>0</msg-update-in>
                              <msg-update-out>0</msg-update-out>
                              <msg-total-in>0</msg-total-in>
                              <msg-total-out>0</msg-total-out>
                              <last-update-age>31717</last-update-age>
                              <last-error></last-error>
                              <status-flap-counts>40621</status-flap-counts>
                              <established-counts>0</established-counts>
                              <ORF-entry-received>0</ORF-entry-received>
                              <nexthop-self>no</nexthop-self>
                              <nexthop-thirdparty>yes</nexthop-thirdparty>
                              <nexthop-peer>no</nexthop-peer>
                              <config>
                                 <remove-private-as>yes</remove-private-as>
                              </config>
                              <peer-capability></peer-capability>
                              <prefix-counter></prefix-counter>
                        </entry>
                     </result>
                  </response>
"""

PANOS_SHOW_ROUTING_PROTOCOL_BGP_SUMMARY_MATCH_PEER_PAIR_1 = ((PANOS_SHOW_ROUTING_PROTOCOL_BGP_SUMMARY_MATCH_PEER_OUT_1, 'VyOS-Rtr'), ('Established', '172.30.250.2'))

PANOS_SHOW_ROUTING_PROTOCOL_BGP_SUMMARY_MATCH_PEER_PAIR_2 = ((PANOS_SHOW_ROUTING_PROTOCOL_BGP_SUMMARY_MATCH_PEER_OUT_2, 'VyOS-Rtr3'), ('Established', '172.30.250.3'))

PANOS_SHOW_ROUTING_PROTOCOL_BGP_SUMMARY_MATCH_PEER_PAIR_3 = ((PANOS_SHOW_ROUTING_PROTOCOL_BGP_SUMMARY_MATCH_PEER_OUT_3, 'VyOS-Rtr'), ('Connect', '172.30.250.2'))

PANOS_SHOW_ROUTING_PROTOCOL_BGP_SUMMARY_MATCH_PEER_PAIR_4 = ((PANOS_SHOW_ROUTING_PROTOCOL_BGP_SUMMARY_MATCH_PEER_OUT_4, 'jorge-peer'), ('Active', '172.30.250.7'))

PANOS_SHOW_ROUTING_PROTOCOL_BGP_PEER_PEER_NAME_OUT_1 = """
<response status="success">
   <result>
      <entry peer="VyOS-Rtr" vr="default">
         <peer-group>VyOS-Peer-Group</peer-group>
         <peer-router-id>172.30.250.2</peer-router-id>
         <remote-as>65535</remote-as>
         <status>Established</status>
         <status-duration>93387</status-duration>
         <password-set>no</password-set>
         <passive>no</passive>
         <multi-hop-ttl>1</multi-hop-ttl>
         <peer-address>172.30.250.2:38387</peer-address>
         <local-address>172.30.250.1:179</local-address>
         <reflector-client>not-client</reflector-client>
         <same-confederation>no</same-confederation>
         <aggregate-confed-as>yes</aggregate-confed-as>
         <peering-type>Unspecified</peering-type>
         <connect-retry-interval>1</connect-retry-interval>
         <open-delay>0</open-delay>
         <idle-hold>15</idle-hold>
         <prefix-limit>5000</prefix-limit>
         <holdtime>90</holdtime>
         <holdtime-config>90</holdtime-config>
         <keepalive>30</keepalive>
         <keepalive-config>30</keepalive-config>
         <msg-update-in>7</msg-update-in>
         <msg-update-out>2</msg-update-out>
         <msg-total-in>3124</msg-total-in>
         <msg-total-out>3577</msg-total-out>
         <last-update-age>0</last-update-age>
         <last-error>Cease (6) : connection rejected (5)</last-error>
         <status-flap-counts>2</status-flap-counts>
         <established-counts>1</established-counts>
         <ORF-entry-received>0</ORF-entry-received>
         <nexthop-self>no</nexthop-self>
         <nexthop-thirdparty>yes</nexthop-thirdparty>
         <nexthop-peer>no</nexthop-peer>
         <config>
            <remove-private-as>yes</remove-private-as>
         </config>
         <peer-capability>
            <list>
               <capability>Multiprotocol Extensions(1)</capability>
               <value>IPv4 Unicast</value>
            </list>
            <list>
               <capability>Route Refresh(2)</capability>
               <value>yes</value>
            </list>
            <list>
               <capability>4-Byte AS Number(65)</capability>
               <value>65535</value>
            </list>
            <list>
               <capability>Route Refresh (Cisco)(128)</capability>
               <value>yes</value>
            </list>
         </peer-capability>
         <prefix-counter>
            <entry afi-safi="bgpAfiIpv4-unicast">
               <incoming-total>4143</incoming-total>
               <incoming-accepted>4143</incoming-accepted>
               <incoming-rejected>0</incoming-rejected>
               <policy-rejected>0</policy-rejected>
               <outgoing-total>1</outgoing-total>
               <outgoing-advertised>1</outgoing-advertised>
            </entry>
         </prefix-counter>
      </entry>
   </result>
</response>
"""

PANOS_SHOW_ROUTING_PROTOCOL_BGP_PEER_PEER_NAME_OUT_2 = """
<response status="success">
   <result>
      <entry peer="VyOS-Rtr" vr="default">
         <peer-group>VyOS-Peer-Group</peer-group>
         <peer-router-id>172.30.250.2</peer-router-id>
         <remote-as>65535</remote-as>
         <status>Established</status>
         <status-duration>93387</status-duration>
         <password-set>no</password-set>
         <passive>no</passive>
         <multi-hop-ttl>1</multi-hop-ttl>
         <peer-address>172.30.250.2:38387</peer-address>
         <local-address>172.30.250.3:179</local-address>
         <reflector-client>not-client</reflector-client>
         <same-confederation>no</same-confederation>
         <aggregate-confed-as>yes</aggregate-confed-as>
         <peering-type>Unspecified</peering-type>
         <connect-retry-interval>1</connect-retry-interval>
         <open-delay>0</open-delay>
         <idle-hold>15</idle-hold>
         <prefix-limit>5000</prefix-limit>
         <holdtime>90</holdtime>
         <holdtime-config>90</holdtime-config>
         <keepalive>30</keepalive>
         <keepalive-config>30</keepalive-config>
         <msg-update-in>7</msg-update-in>
         <msg-update-out>2</msg-update-out>
         <msg-total-in>3124</msg-total-in>
         <msg-total-out>3577</msg-total-out>
         <last-update-age>0</last-update-age>
         <last-error>Cease (6) : connection rejected (5)</last-error>
         <status-flap-counts>2</status-flap-counts>
         <established-counts>1</established-counts>
         <ORF-entry-received>0</ORF-entry-received>
         <nexthop-self>no</nexthop-self>
         <nexthop-thirdparty>yes</nexthop-thirdparty>
         <nexthop-peer>no</nexthop-peer>
         <config>
            <remove-private-as>yes</remove-private-as>
         </config>
         <peer-capability>
            <list>
               <capability>Multiprotocol Extensions(1)</capability>
               <value>IPv4 Unicast</value>
            </list>
            <list>
               <capability>Route Refresh(2)</capability>
               <value>yes</value>
            </list>
            <list>
               <capability>4-Byte AS Number(65)</capability>
               <value>65535</value>
            </list>
            <list>
               <capability>Route Refresh (Cisco)(128)</capability>
               <value>yes</value>
            </list>
         </peer-capability>
         <prefix-counter>
            <entry afi-safi="bgpAfiIpv4-unicast">
               <incoming-total>4143</incoming-total>
               <incoming-accepted>4143</incoming-accepted>
               <incoming-rejected>0</incoming-rejected>
               <policy-rejected>0</policy-rejected>
               <outgoing-total>1</outgoing-total>
               <outgoing-advertised>1</outgoing-advertised>
            </entry>
         </prefix-counter>
      </entry>
   </result>
</response>
"""

PANOS_SHOW_ROUTING_PROTOCOL_BGP_PEER_PEER_NAME_PAIR_1 = (PANOS_SHOW_ROUTING_PROTOCOL_BGP_PEER_PEER_NAME_OUT_1, '172.30.250.1')

PANOS_SHOW_ROUTING_PROTOCOL_BGP_PEER_PEER_NAME_PAIR_2 = (PANOS_SHOW_ROUTING_PROTOCOL_BGP_PEER_PEER_NAME_OUT_2, '172.30.250.3')

PANOS_PING_OUT_1 = """PING 149.2.30.2 (66.17.96.225) from 66.17.96.226 : 56(84) bytes of data.
64 bytes from 149.2.30.2: icmp_seq=1 ttl=255 time=0.621 ms
64 bytes from 149.2.30.2: icmp_seq=2 ttl=255 time=0.619 ms
64 bytes from 149.2.30.2: icmp_seq=3 ttl=255 time=0.661 ms

--- 149.2.30.2 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 1999ms
rtt min/avg/max/mdev = 0.619/0.633/0.661/0.034 ms"""

PANOS_PING_OUT_2 = """PING 149.2.30.2 (66.17.96.225) from 66.17.96.226 : 56(84) bytes of data.
64 bytes from 149.2.30.2: icmp_seq=1 ttl=255 time=0.621 ms
64 bytes from 149.2.30.2: icmp_seq=2 ttl=255 time=0.619 ms

--- 149.2.30.2 ping statistics ---
3 packets transmitted, 2 received, 33% packet loss, time 1999ms
rtt min/avg/max/mdev = 0.619/0.633/0.661/0.034 ms"""

PANOS_PING_PAIR_1 = (PANOS_PING_OUT_1, True)

PANOS_PING_PAIR_2 = (PANOS_PING_OUT_2, False)

PANOS_SHOW_INTERFACE_ALL_OUT_1 = """
<response status="success">
   <result>
      <ifnet>
         <entry>
            <name>ethernet1/1</name>
            <zone>untrust</zone>
            <fwd>vr:default</fwd>
            <vsys>1</vsys>
            <dyn-addr />
            <addr6 />
            <tag>0</tag>
            <ip>172.30.250.1/29</ip>
            <id>16</id>
            <addr />
         </entry>
         <entry>
            <name>ethernet1/2</name>
            <zone>trust</zone>
            <fwd>vr:default</fwd>
            <vsys>1</vsys>
            <dyn-addr />
            <addr6 />
            <tag>0</tag>
            <ip>10.250.240.1/20</ip>
            <id>17</id>
            <addr />
         </entry>
         <entry>
            <name>ethernet1/3</name>
            <zone />
            <fwd>ha</fwd>
            <vsys>1</vsys>
            <dyn-addr />
            <addr6 />
            <tag>0</tag>
            <ip>10.255.255.2/30</ip>
            <id>18</id>
            <addr />
         </entry>
         <entry>
            <name>ethernet1/4</name>
            <zone />
            <fwd>ha</fwd>
            <vsys>1</vsys>
            <dyn-addr />
            <addr6 />
            <tag>0</tag>
            <ip>172.24.245.150/30</ip>
            <id>19</id>
            <addr />
         </entry>
         <entry>
            <name>ethernet1/5</name>
            <zone>User-Vlan</zone>
            <fwd>vlan:Users</fwd>
            <vsys>1</vsys>
            <dyn-addr />
            <addr6 />
            <tag>0</tag>
            <ip>N/A</ip>
            <id>20</id>
            <addr />
         </entry>
         <entry>
            <name>vlan</name>
            <zone />
            <fwd>N/A</fwd>
            <vsys>1</vsys>
            <dyn-addr />
            <addr6 />
            <tag>0</tag>
            <ip>N/A</ip>
            <id>1</id>
            <addr />
         </entry>
         <entry>
            <name>vlan.170</name>
            <zone>trust</zone>
            <fwd>vr:default</fwd>
            <vsys>1</vsys>
            <dyn-addr />
            <addr6 />
            <tag>0</tag>
            <ip>192.168.170.1/24</ip>
            <id>260</id>
            <addr />
         </entry>
         <entry>
            <name>vlan.300</name>
            <zone>trust</zone>
            <fwd>vr:default</fwd>
            <vsys>1</vsys>
            <dyn-addr />
            <addr6 />
            <tag>0</tag>
            <ip>192.168.169.1/24</ip>
            <id>257</id>
            <addr />
         </entry>
         <entry>
            <name>loopback</name>
            <zone />
            <fwd>N/A</fwd>
            <vsys>1</vsys>
            <dyn-addr />
            <addr6 />
            <tag>0</tag>
            <ip>N/A</ip>
            <id>3</id>
            <addr />
         </entry>
         <entry>
            <name>tunnel</name>
            <zone />
            <fwd>N/A</fwd>
            <vsys>1</vsys>
            <dyn-addr />
            <addr6 />
            <tag>0</tag>
            <ip>N/A</ip>
            <id>4</id>
            <addr />
         </entry>
         <entry>
            <name>tunnel.1</name>
            <zone>untrust</zone>
            <fwd>vr:default</fwd>
            <vsys>1</vsys>
            <dyn-addr />
            <addr6 />
            <tag>0</tag>
            <ip>N/A</ip>
            <id>258</id>
            <addr />
         </entry>
         <entry>
            <name>tunnel.2</name>
            <zone>trust</zone>
            <fwd>vr:default</fwd>
            <vsys>1</vsys>
            <dyn-addr />
            <addr6 />
            <tag>0</tag>
            <ip>2.2.2.2/32</ip>
            <id>261</id>
            <addr />
         </entry>
         <entry>
            <name>tunnel.10</name>
            <zone>vpn</zone>
            <fwd>vr:Virtual Router 1</fwd>
            <vsys>1</vsys>
            <dyn-addr />
            <addr6 />
            <tag>0</tag>
            <ip>N/A</ip>
            <id>262</id>
            <addr />
         </entry>
      </ifnet>
      <hw>
         <entry>
            <name>ethernet1/1</name>
            <duplex>full</duplex>
            <type>0</type>
            <state>up</state>
            <st>1000/full/up</st>
            <mac>00:50:56:ac:d5:37</mac>
            <mode>(autoneg)</mode>
            <speed>1000</speed>
            <id>16</id>
         </entry>
         <entry>
            <name>ethernet1/2</name>
            <duplex>full</duplex>
            <type>0</type>
            <state>up</state>
            <st>1000/full/up</st>
            <mac>00:50:56:ac:14:8f</mac>
            <mode>(autoneg)</mode>
            <speed>1000</speed>
            <id>17</id>
         </entry>
         <entry>
            <name>ethernet1/3</name>
            <duplex>full</duplex>
            <type>0</type>
            <state>up</state>
            <st>1000/full/up</st>
            <mac>00:50:56:ac:8f:f6</mac>
            <mode>(autoneg)</mode>
            <speed>1000</speed>
            <id>18</id>
         </entry>
         <entry>
            <name>ethernet1/4</name>
            <duplex>full</duplex>
            <type>0</type>
            <state>up</state>
            <st>1000/full/up</st>
            <mac>00:50:56:ac:c2:55</mac>
            <mode>(autoneg)</mode>
            <speed>1000</speed>
            <id>19</id>
         </entry>
         <entry>
            <name>ethernet1/5</name>
            <duplex>full</duplex>
            <type>0</type>
            <state>up</state>
            <st>1000/full/up</st>
            <mac>00:50:56:ac:e5:a7</mac>
            <mode>(autoneg)</mode>
            <speed>1000</speed>
            <id>20</id>
         </entry>
         <entry>
            <name>vlan</name>
            <duplex>[n/a]</duplex>
            <type>3</type>
            <state>up</state>
            <st>[n/a]/[n/a]/up</st>
            <mac>00:1b:17:00:01:01</mac>
            <mode>(unknown)</mode>
            <speed>[n/a]</speed>
            <id>1</id>
         </entry>
         <entry>
            <name>loopback</name>
            <duplex>[n/a]</duplex>
            <type>5</type>
            <state>up</state>
            <st>[n/a]/[n/a]/up</st>
            <mac>00:1b:17:00:01:03</mac>
            <mode>(unknown)</mode>
            <speed>[n/a]</speed>
            <id>3</id>
         </entry>
         <entry>
            <name>tunnel</name>
            <duplex>[n/a]</duplex>
            <type>6</type>
            <state>up</state>
            <st>[n/a]/[n/a]/up</st>
            <mac>00:1b:17:00:01:04</mac>
            <mode>(unknown)</mode>
            <speed>[n/a]</speed>
            <id>4</id>
         </entry>
      </hw>
   </result>
</response>
"""

PANOS_SHOW_INTERFACE_ALL_OUT_2 = """
<response status="success">
   <result>
      <ifnet>
         <entry>
            <name>ethernet1/1</name>
            <zone>untrust</zone>
            <fwd>vr:default</fwd>
            <vsys>1</vsys>
            <dyn-addr />
            <addr6 />
            <tag>0</tag>
            <ip>172.30.250.1/29</ip>
            <id>16</id>
            <addr />
         </entry>
         <entry>
            <name>ethernet1/2</name>
            <zone>trust</zone>
            <fwd>vr:default</fwd>
            <vsys>1</vsys>
            <dyn-addr />
            <addr6 />
            <tag>0</tag>
            <ip>10.250.240.1/20</ip>
            <id>17</id>
            <addr />
         </entry>
         <entry>
            <name>ethernet1/3</name>
            <zone />
            <fwd>ha</fwd>
            <vsys>1</vsys>
            <dyn-addr />
            <addr6 />
            <tag>0</tag>
            <ip>10.255.255.2/30</ip>
            <id>18</id>
            <addr />
         </entry>
         <entry>
            <name>ethernet1/4</name>
            <zone />
            <fwd>ha</fwd>
            <vsys>1</vsys>
            <dyn-addr />
            <addr6 />
            <tag>0</tag>
            <ip>172.24.245.150/30</ip>
            <id>19</id>
            <addr />
         </entry>
         <entry>
            <name>ethernet1/5</name>
            <zone>User-Vlan</zone>
            <fwd>vlan:Users</fwd>
            <vsys>1</vsys>
            <dyn-addr />
            <addr6 />
            <tag>0</tag>
            <ip>N/A</ip>
            <id>20</id>
            <addr />
         </entry>
         <entry>
            <name>vlan</name>
            <zone />
            <fwd>N/A</fwd>
            <vsys>1</vsys>
            <dyn-addr />
            <addr6 />
            <tag>0</tag>
            <ip>N/A</ip>
            <id>1</id>
            <addr />
         </entry>
         <entry>
            <name>vlan.170</name>
            <zone>trust</zone>
            <fwd>vr:default</fwd>
            <vsys>1</vsys>
            <dyn-addr />
            <addr6 />
            <tag>0</tag>
            <ip>192.168.170.1/24</ip>
            <id>260</id>
            <addr />
         </entry>
         <entry>
            <name>vlan.300</name>
            <zone>trust</zone>
            <fwd>vr:default</fwd>
            <vsys>1</vsys>
            <dyn-addr />
            <addr6 />
            <tag>0</tag>
            <ip>192.168.169.1/24</ip>
            <id>257</id>
            <addr />
         </entry>
         <entry>
            <name>loopback</name>
            <zone />
            <fwd>N/A</fwd>
            <vsys>1</vsys>
            <dyn-addr />
            <addr6 />
            <tag>0</tag>
            <ip>N/A</ip>
            <id>3</id>
            <addr />
         </entry>
         <entry>
            <name>tunnel</name>
            <zone />
            <fwd>N/A</fwd>
            <vsys>1</vsys>
            <dyn-addr />
            <addr6 />
            <tag>0</tag>
            <ip>N/A</ip>
            <id>4</id>
            <addr />
         </entry>
         <entry>
            <name>tunnel.1</name>
            <zone>untrust</zone>
            <fwd>vr:default</fwd>
            <vsys>1</vsys>
            <dyn-addr />
            <addr6 />
            <tag>0</tag>
            <ip>N/A</ip>
            <id>258</id>
            <addr />
         </entry>
         <entry>
            <name>tunnel.2</name>
            <zone>trust</zone>
            <fwd>vr:default</fwd>
            <vsys>1</vsys>
            <dyn-addr />
            <addr6 />
            <tag>0</tag>
            <ip>2.2.2.2/32</ip>
            <id>261</id>
            <addr />
         </entry>
         <entry>
            <name>tunnel.10</name>
            <zone>vpn</zone>
            <fwd>vr:Virtual Router 1</fwd>
            <vsys>1</vsys>
            <dyn-addr />
            <addr6 />
            <tag>0</tag>
            <ip>N/A</ip>
            <id>262</id>
            <addr />
         </entry>
      </ifnet>
      <hw>
         <entry>
            <name>ethernet1/1</name>
            <duplex>full</duplex>
            <type>0</type>
            <state>down</state>
            <st>1000/full/up</st>
            <mac>00:50:56:ac:d5:37</mac>
            <mode>(autoneg)</mode>
            <speed>1000</speed>
            <id>16</id>
         </entry>
         <entry>
            <name>ethernet1/2</name>
            <duplex>full</duplex>
            <type>0</type>
            <state>up</state>
            <st>1000/full/up</st>
            <mac>00:50:56:ac:14:8f</mac>
            <mode>(autoneg)</mode>
            <speed>1000</speed>
            <id>17</id>
         </entry>
         <entry>
            <name>ethernet1/3</name>
            <duplex>full</duplex>
            <type>0</type>
            <state>up</state>
            <st>1000/full/up</st>
            <mac>00:50:56:ac:8f:f6</mac>
            <mode>(autoneg)</mode>
            <speed>1000</speed>
            <id>18</id>
         </entry>
         <entry>
            <name>ethernet1/4</name>
            <duplex>full</duplex>
            <type>0</type>
            <state>up</state>
            <st>1000/full/up</st>
            <mac>00:50:56:ac:c2:55</mac>
            <mode>(autoneg)</mode>
            <speed>1000</speed>
            <id>19</id>
         </entry>
         <entry>
            <name>ethernet1/5</name>
            <duplex>full</duplex>
            <type>0</type>
            <state>up</state>
            <st>1000/full/up</st>
            <mac>00:50:56:ac:e5:a7</mac>
            <mode>(autoneg)</mode>
            <speed>1000</speed>
            <id>20</id>
         </entry>
         <entry>
            <name>vlan</name>
            <duplex>[n/a]</duplex>
            <type>3</type>
            <state>up</state>
            <st>[n/a]/[n/a]/up</st>
            <mac>00:1b:17:00:01:01</mac>
            <mode>(unknown)</mode>
            <speed>[n/a]</speed>
            <id>1</id>
         </entry>
         <entry>
            <name>loopback</name>
            <duplex>[n/a]</duplex>
            <type>5</type>
            <state>up</state>
            <st>[n/a]/[n/a]/up</st>
            <mac>00:1b:17:00:01:03</mac>
            <mode>(unknown)</mode>
            <speed>[n/a]</speed>
            <id>3</id>
         </entry>
         <entry>
            <name>tunnel</name>
            <duplex>[n/a]</duplex>
            <type>6</type>
            <state>up</state>
            <st>[n/a]/[n/a]/up</st>
            <mac>00:1b:17:00:01:04</mac>
            <mode>(unknown)</mode>
            <speed>[n/a]</speed>
            <id>4</id>
         </entry>
      </hw>
   </result>
</response>
"""

PANOS_SHOW_INTERFACE_ALL_PAIR_1 = ((PANOS_SHOW_INTERFACE_ALL_OUT_1, '172.30.250.1'), 'up')


PANOS_SHOW_INTERFACE_ALL_PAIR_2 = ((PANOS_SHOW_INTERFACE_ALL_OUT_2, '172.30.250.1'), 'down')

# SSH command: show log traffic src in {{str_local_ip_for_bgp}} dst in {{tuple_bgp_status_and_ip[1]}} port equal 179 direction equal backward receive_time in last-hour
PANOS_SHOW_LOG_TRAFFIC_OUT_1 = """Time                App             From                            Src Port          Source
Rule                Action          To                              Dst Port          Destination
                    Src User        Dst User                        End Reason
====================================================================================================
2019/09/21 10:05:39 bgp    ISPB                          50318             149.2.30.1
Permit_BGP_Peering- allow           ISPB                          179               149.2.30.2
                                                                    tcp-rst-from-server"""

PANOS_SHOW_LOG_TRAFFIC_OUT_2 = """Time                App             From                            Src Port          Source
Rule                Action          To                              Dst Port          Destination
                    Src User        Dst User                        End Reason
====================================================================================================
2019/10/12 20:44:04 app-override    ISPB_v6                         45036             2644:40:2000:3::d
Permit_BGP_Peering- deny           ISPB_v6                          179               2644:40:2000:3::c"""

PANOS_SHOW_LOG_TRAFFIC_OUT_3 = """Time                App             From                            Src Port          Source
Rule                Action          To                              Dst Port          Destination
                    Src User        Dst User                        End Reason
===================================================================================================="""

PANOS_SHOW_LOG_TRAFFIC_PAIR_1 = (PANOS_SHOW_LOG_TRAFFIC_OUT_1, False)

PANOS_SHOW_LOG_TRAFFIC_PAIR_2 = (PANOS_SHOW_LOG_TRAFFIC_OUT_2, True)

PANOS_SHOW_LOG_TRAFFIC_PAIR_3 = (PANOS_SHOW_LOG_TRAFFIC_OUT_3, False)

# SSH command: grep context 20 mp-log routed.log pattern

PANOS_GREP_CONTEXT_20_MP_LOG_ROUTED_LOG_PATTERN_OUT_1 = """sckrecv.c 1249 :at 00:05:41, 7 November 2019 (323350 ms)
Processed an ATG_SCK_SOCKET IPS OK.
Socket ID = 1567
Socket type = 0X00000001
Socket family = 0X00000002
Socket protocol = 0X00000000
Application handle = 0X01600001
Stub socket handle = 0X037A0001
Local inet address = 149.2.30.1
Local port = 0
Remote inet address = 149.2.30.2
Remote port = 179

**** AUDIT 0x0303 - 76 (0000) **** I:00000db4 F:00000002
sckutil.c 1612 :at 00:05:41, 7 November 2019 (323737 ms)
Closed a socket
Socket ID = 1565
Socket type = 0X00000001
Socket family = 0X0000000A
Socket protocol = 0X00000000

--
sckrecv.c 1249 :at 00:05:42, 7 November 2019 (324225 ms)
Processed an ATG_SCK_SOCKET IPS OK.
Socket ID = 1567
Socket type = 0X00000001
Socket family = 0X00000002
Socket protocol = 0X00000000
Application handle = 0X01600001
Stub socket handle = 0X03800001
Local inet address = 149.2.30.1
Local port = 0
Remote inet address = 149.2.30.2
Remote port = 179

**** AUDIT 0x0303 - 76 (0000) **** I:00000dd1 F:00000002
sckutil.c 1612 :at 00:05:42, 7 November 2019 (324535 ms)
Closed a socket
Socket ID = 1565
Socket type = 0X00000001
Socket family = 0X0000000A
Socket protocol = 0X00000000

--
sckrecv.c 1249 :at 00:05:43, 7 November 2019 (325087 ms)
Processed an ATG_SCK_SOCKET IPS OK.
Socket ID = 1567
Socket type = 0X00000001
Socket family = 0X00000002
Socket protocol = 0X00000000
Application handle = 0X01600001
Stub socket handle = 0X03890001
Local inet address = 149.2.30.1
Local port = 0
Remote inet address = 149.2.30.2
Remote port = 179

**** AUDIT 0x0303 - 76 (0000) **** I:00000dee F:00000002
sckutil.c 1612 :at 00:05:43, 7 November 2019 (325277 ms)
Closed a socket
Socket ID = 1565
Socket type = 0X00000001
Socket family = 0X0000000A
Socket protocol = 0X00000000

--
Previous state = 0X01
New state = 0X00
FSM Input = 0X01

**** AUDIT 0x4102 - 49 (0000) **** I:00000e09 F:00000002
qbnmcfsm.c 1642 :at 00:05:43, 7 November 2019 (325441 ms)
A connection's FSM state has deteriorated.
Entity index = 0X00000001
Local address = 149.2.30.1
Local port = 0
Remote address = 149.2.30.2
Remote port = 0
Scope ID = 0
Incoming? = False
Previous state = 0X01
New state = 0X00
FSM Input = 0X01

**** AUDIT 0x4102 - 49 (0000) **** I:00000e0c F:00000002
qbnmcfsm.c 1642 :at 00:05:43, 7 November 2019 (325441 ms)
A connection's FSM state has deteriorated.
--
sckrecv.c 1249 :at 00:10:32, 7 November 2019 (614735 ms)
Processed an ATG_SCK_SOCKET IPS OK.
Socket ID = 1566
Socket type = 0X00000001
Socket family = 0X00000002
Socket protocol = 0X00000000
Application handle = 0X055B0001
Stub socket handle = 0X04620001
Local inet address = 149.2.30.1
Local port = 0
Remote inet address = 149.2.30.2
Remote port = 179

**** AUDIT 0x0303 - 7 (0000) **** I:00001329 F:00000002
sckrecv.c 1249 :at 00:10:32, 7 November 2019 (614735 ms)
Processed an ATG_SCK_SOCKET IPS OK.
Socket ID = 1567
Socket type = 0X00000001
Socket family = 0X0000000A
Socket protocol = 0X00000000
Application handle = 0X056A0001
--
sckrecv.c 1249 :at 00:10:33, 7 November 2019 (615484 ms)
Processed an ATG_SCK_SOCKET IPS OK.
Socket ID = 1566
Socket type = 0X00000001
Socket family = 0X00000002
Socket protocol = 0X00000000
Application handle = 0X055B0001
Stub socket handle = 0X03610001
Local inet address = 149.2.30.1
Local port = 0
Remote inet address = 149.2.30.2
Remote port = 179

2019-11-07 00:10:33.728 -0600 sending bfd admindown
2019-11-07 00:10:33.728 -0600 passive-link-state is auto. retry bfd ha 1 out of 5
**** AUDIT 0x0303 - 55 (0002) **** -:-------- F:00000002
sckorig2.c 847 :at 00:10:33, 7 November 2019 (615495 ms)
New TCP or SCTP connection.
Socket ID = 1566
Local address type = 0X01
Local IP address = 149.2.30.1
Local port = 57861
Remote address type = 0X01
Remote IP address = 149.2.30.2
Remote port = 179

**** AUDIT 0x4102 - 67 (0000) **** I:000013a1 F:00000002
qbnmsnd2.c 453 :at 00:10:33, 7 November 2019 (615496 ms)
An OPEN message is being sent to a neighbor.
NM entity index = 1
Configured local address = 149.2.30.1
Configured local port = 0
Selected local address = 149.2.30.1
Selected local port = 57861
Remote address = 149.2.30.2
Remote port = 0
Scope ID = 0
Remote AS number = 0
Remote BGP ID = 0X00000000

**** AUDIT 0x0303 - 76 (0000) **** I:000013a4 F:00000002
sckutil.c 1612 :at 00:10:33, 7 November 2019 (615560 ms)
Closed a socket
Socket ID = 1565
Socket type = 0X00000001
--
2019-11-07 00:10:37.765 -0600 Sending requst to refresh forwarding table PID 17895424
2019-11-07 00:10:37.765 -0600 Sending requst to refresh forwarding table PID 17874944
**** AUDIT 0x4102 - 55 (0000) **** I:000013fc F:00000002
qbnmmsg.c 815 :at 00:10:38, 7 November 2019 (620080 ms)
A valid OPEN message has been received from a neighbor.
NM entity index = 1
Configured local address = 149.2.30.1
Configured local port = 0
Selected local address = 149.2.30.1
Selected local port = 57861
Remote address = 149.2.30.2
Remote port = 0
Scope ID = 0
Incoming connection? = False
Remote AS number = 29860
Remote BGP ID = 0X90E4F384
Received hold time = 360
Number of Capabilities = 5
Restart capable? = True
Capabilities offset = 31
Open Message packet =
FFFFFFFF FFFFFFFF FFFFFFFF FFFFFFFF 003F0104 04D70168 90E4F384 22020601
04000100 01020280 00020202 00020641 04000004 D7020840 06007800 010180

**** AUDIT 0x4102 - 48 (0000) **** I:000013fc F:00000002
qbnmcfsm.c 1170 :at 00:10:38, 7 November 2019 (620080 ms)
A connection has entered or left Established state.
Established:? True
Local address: 149.2.30.1
Local port: 0
Remote address: 149.2.30.2
Remote port: 0
Scope ID: 0
Neg hold time: 90
Passive:? False
Entity index: 0X00000001

**** AUDIT 0x4102 - 92 (0000) **** I:000013ff F:00000002
qbnmnfsm.c 1862 :at 00:10:38, 7 November 2019 (620080 ms)
BGP 1 established a session with peer 149.2.30.2.
Configured local address and port: 149.2.30.1:0
Connection local address and port: 149.2.30.1:57861
Configured remote address and port: 149.2.30.2:0
Connection remote port: 179
Scope ID: 0
Local AS number: 65400
Remote AS number: 29860

**** AUDIT 0x4107 - 37 (0000) **** I:000022ff F:00000002
qbdcphs1.c 142 :at 00:10:46, 7 November 2019 (627952 ms)
An End-of-RIB (EOR) marker has been received from a peer.
RIB Manager entity index: 0X00000001
Neighbor Index: 1
Neighbor IP address: 149.2.30.2
AFI: 1
SAFI: 1

**** AUDIT 0x4101 - 51 (0000) **** I:000022ff F:00000040
qbrmrst.c 766 :at 00:10:46, 7 November 2019 (627952 ms)
DC-BGP RIB Manager has completed all restart processing.
RM entity index = 0X00000001

**** AUDIT 0x4104 - 8 (0000) **** I:000022ff F:00000002
qbdcphas.c 1039 :at 00:10:46, 7 November 2019 (627952 ms)
admin@BGP-Router-01(active)> grep context 10 mp-log routed.log pattern 149.2.30.2 """

PANOS_GREP_CONTEXT_20_MP_LOG_ROUTED_LOG_PATTERN_OUT_2 = """**** EXCEPTION 0x4102 - 71 (0000) **** T:064292ac F:00000020
qbnmsnd2.c 167 :at 13:08:52, 9 September 2019 (672468732 ms)
A NOTIFICATION message is being sent to a neighbor due to an unexpected
problem.
NM entity index = 1
Local address = 149.2.30.1
Local port = 0
Remote address = 149.2.30.2
Remote port = 0
Scope ID = 0
Remote AS number = 1239
Remote BGP ID = 0X90E4F384
Error code = Hold Timer Expired (4)
Error subcode = Unspecific (0)"""

PANOS_GREP_CONTEXT_20_MP_LOG_ROUTED_LOG_PATTERN_PAIR_1 = (PANOS_GREP_CONTEXT_20_MP_LOG_ROUTED_LOG_PATTERN_OUT_1, False)

PANOS_GREP_CONTEXT_20_MP_LOG_ROUTED_LOG_PATTERN_PAIR_2 = (PANOS_GREP_CONTEXT_20_MP_LOG_ROUTED_LOG_PATTERN_OUT_2, True)
