import unittest
import automation.workflows.cross_vendor_next_hop_router_inaccessible.checkpoint.cross_vendor_next_hop_router_inaccessible_parser as parser
import automation.workflows.cross_vendor_next_hop_router_inaccessible.checkpoint.cross_vendor_next_hop_router_inaccessible_mock_data as mock


class CrossVendorNextHopRouterInaccessibleTests(unittest.TestCase):


    def test_is_next_hop_still_inaccessible(self):
        pair1 = mock.IF_NEXT_HOP_STILL_ACCESSIBLE_PAIR_1
        self.assertEqual(parser.is_next_hop_still_inaccessible(pair1[0]), pair1[1])

    def test_is_arp_cache_table_limit_reached(self):
        pair1 = mock.IS_ARP_CACHE_TABLE_LIMIT_REACHED_PAIR_1
        pair2 = mock.IS_ARP_CACHE_TABLE_LIMIT_REACHED_PAIR_2
        self.assertEqual(parser.is_arp_cache_table_limit_reached(pair1[0]), pair1[1])
        self.assertEqual(parser.is_arp_cache_table_limit_reached(pair2[0]), pair2[1])

    def test_get_next_hop_source_interface(self):
        pair1 = mock.GET_NEXT_HOP_SOURCE_INTERFACE_PAIR_1
        self.assertEqual(parser.get_next_hop_source_interface(pair1[0]), pair1[1])


    def test_identity_parser(self):
        pair1 = mock.IDENTITY_PARSER_PAIR_1
        self.assertEqual(parser.identity_parser(pair1[0]), pair1[1])

    def test_get_arp_table_interface(self):
        pair1 = mock.GET_ARP_TABLE_INTERFACE_PAIR_1
        self.assertEqual(parser.get_arp_table_interface(pair1[0]), pair1[1])
        with self.assertRaises(ValueError):
            parser.get_arp_table_interface(mock.GET_ARP_TABLE_INTERFACE_PAIR_2)

    def test_is_errors_on_interface(self):
        pair1 = mock.IS_ERRORS_ON_INTERFACE_PAIR_1
        pair2 = mock.IS_ERRORS_ON_INTERFACE_PAIR_2
        self.assertEqual(parser.is_errors_on_interface(str_ethtool_output=pair1[0]), pair1[1])
        self.assertEqual(parser.is_errors_on_interface(str_ethtool_output=pair2[0]), pair2[1])

    def test_is_carrier_counter_increased(self):
        pair1 = mock.CARRIER_COUNTER_INCREASED_PAIR_1
        pair2 = mock.CARRIER_COUNTER_INCREASED_PAIR_2
        self.assertEqual(parser.is_carrier_counter_increased(str_ethtool_output=pair1[0]), pair1[1])
        self.assertEqual(parser.is_carrier_counter_increased(str_ethtool_output=pair2[0]), pair2[1])

    def test_is_packets_received(self):
        pair1 = mock.IS_PACKETS_RECEIVED_PAIR_1
        self.assertEqual(parser.is_packets_received(pair1[0]), pair1[1])


if __name__ == '__main__':
    unittest.main()
