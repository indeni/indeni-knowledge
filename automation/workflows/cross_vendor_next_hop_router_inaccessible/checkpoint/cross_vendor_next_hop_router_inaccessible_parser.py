import re


def check_variant_matches(**kwargs) -> bool:
    # This should work on all devices with one of the below versions
    tags = kwargs.get('my_device_tags', None)
    if tags is None:
        return False
    versions = [x.lower() for x in ['R77.30', 'R80.10', 'R80.20', 'R80.30', 'R76SP.30', 'R76SP.40', 'R76SP.50', 'R80.20SP', 'R80.40']]
    if tags.get('os.version', '').lower() not in versions:
        return False
    return True


def is_next_hop_still_inaccessible(raw_data: str) -> bool:
    return raw_data.strip() != ''


def is_arp_cache_table_limit_reached(raw_data: str) -> bool:
    lines = raw_data.strip().split('\n')
    try:
        limit = int(lines[0].split(' ')[-1])
        used = int(lines[-1])
        return used >= limit
    except (ValueError, IndexError):
        # TODO: what should we do if we failed to parse the output of a command?
        return False


def get_next_hop_source_interface(raw_data: str) -> str:
    lines = raw_data.split('\n')
    if len(lines) > 0 and len(lines[0].split(' ')) > 2:
        return lines[0].split(' ')[2]
    else:
        return ''


def get_arp_table_interface(raw_data: str) -> str:
    # Prefer to crash over return random data
    if raw_data.strip() == '':
        raise ValueError
    lines = raw_data.split('\n')
    return lines[0].split(' ')[-1].strip()


def identity_parser(raw_data: str) -> str:
    return raw_data


def is_errors_on_interface(**kwargs) -> bool:
    """
    Return true if Errors|Drops|Collisions occurred, false otherwise
    """
    lines = kwargs.get('str_ethtool_output', '')
    for line in lines.split('\n'):
        if 'carrier_errors' in line:
            continue
        match = re.search(r'(errors|dropped|collisions|dropped_smbus):\s+[1-9]\d*', line)
        if match is not None:
            return True
    return False


def is_carrier_counter_increased(**kwargs) -> bool:
    lines = kwargs.get('str_ethtool_output', '').split('\n')
    carriers1 = None
    carriers2 = None
    carriers_found = 0
    for line in lines:
        if 'carrier' in line:
            if carriers_found == 0:
                carriers1 = line.split(' ')[-1]
                carriers_found += 1
            else:
                carriers2 = line.split(' ')[-1]

    try:
        carriers1 = int(carriers1)
        carriers2 = int(carriers2)
        return carriers1 < carriers2
    except (ValueError, TypeError):
        return False


def is_packets_received(raw_data: str) -> bool:
    return 'Unicast reply from' in raw_data
