#SSH command: arp -an 
ARP_AN_OUT_1 = """? (10.11.94.254) at 40:a6:e8:f5:43:56 [ether] on eth0"""

IF_NEXT_HOP_STILL_ACCESSIBLE_PAIR_1 = (ARP_AN_OUT_1, True)

GET_ARP_TABLE_INTERFACE_PAIR_1 = (ARP_AN_OUT_1, "eth0")

GET_ARP_TABLE_INTERFACE_PAIR_2 = ""

#SSH command: clish -c "show configuration arp"; arp -an | wc -l
SHOW_CONFIGURATION_ARP_AND_COUNT_ARP_ENTRIES_OUT_1 = """set arp table cache-size 4096
set arp table validity-timeout 60
set arp announce 2
26
"""

IS_ARP_CACHE_TABLE_LIMIT_REACHED_PAIR_1 = (SHOW_CONFIGURATION_ARP_AND_COUNT_ARP_ENTRIES_OUT_1, False)

SHOW_CONFIGURATION_ARP_AND_COUNT_ARP_ENTRIES_OUT_2 = """set arp table cache-size 1024
set arp table validity-timeout 60
set arp announce 2
1024
"""
IS_ARP_CACHE_TABLE_LIMIT_REACHED_PAIR_2 = (SHOW_CONFIGURATION_ARP_AND_COUNT_ARP_ENTRIES_OUT_2, True)

SHOW_CONFIGURATION_ARP_AND_COUNT_ARP_ENTRIES_OUT_3 = """Cause A handled Exception"""

IS_ARP_CACHE_TABLE_LIMIT_REACHED_PAIR_3 = (SHOW_CONFIGURATION_ARP_AND_COUNT_ARP_ENTRIES_OUT_3, False)

#SSH command: ip r get
IP_R_GET_OUT_1 = """10.11.94.254 dev eth0 src 10.11.94.200 
    cache 
"""

GET_NEXT_HOP_SOURCE_INTERFACE_PAIR_1 = (IP_R_GET_OUT_1, "eth0")

GET_NEXT_HOP_SOURCE_INTERFACE_2 = ( '10.11.94.254 dev')

GET_NEXT_HOP_SOURCE_INTERFACE_3 = ("")

#SSH command: ip link show
IP_LINK_SHOW_OUT_1 = """2:eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP mode DEFAULT qlen 1000
    link/ether 00:50:56:ac:9d:1c brd ff:ff:ff:ff:ff:ff
"""

IDENTITY_PARSER_PAIR_1 = (IP_LINK_SHOW_OUT_1, IP_LINK_SHOW_OUT_1)


#SSH command: ethtool -S <interface_name> | grep -E 'error|drop|collision|carrier'
ETHTOOL_S_OUT_1  = """     rx_errors: 0
     tx_errors: 0
     tx_dropped: 0
     collisions: 0
     rx_length_errors: 0
     rx_over_errors: 0
     rx_crc_errors: 0
     rx_frame_errors: 0
     rx_missed_errors: 0
     tx_aborted_errors: 0
     tx_carrier_errors: 0
     tx_fifo_errors: 0
     tx_heartbeat_errors: 0
     tx_window_errors: 0
     rx_long_length_errors: 0
     rx_short_length_errors: 0
     rx_align_errors: 0
     rx_csum_offload_errors: 0
     dropped_smbus: 0
     rx_errors: 0
     tx_errors: 0
     tx_dropped: 0
     collisions: 0
     rx_length_errors: 0
     rx_over_errors: 0
     rx_crc_errors: 0
     rx_frame_errors: 0
     rx_missed_errors: 0
     tx_aborted_errors: 0
     tx_carrier_errors: 0
     tx_fifo_errors: 0
     tx_heartbeat_errors: 0
     tx_window_errors: 0
     rx_long_length_errors: 0
     rx_short_length_errors: 0
     rx_align_errors: 0
     rx_csum_offload_errors: 0
     dropped_smbus: 0
"""

IS_ERRORS_ON_INTERFACE_PAIR_1 = (ETHTOOL_S_OUT_1, False)

CARRIER_COUNTER_INCREASED_PAIR_1 = (ETHTOOL_S_OUT_1, False)

ETHTOOL_S_OUT_2  = """     rx_errors: 99
     tx_errors: 0
     tx_dropped: 0
     collisions: 0
     rx_length_errors: 0
     rx_over_errors: 0
     rx_crc_errors: 0
     rx_frame_errors: 0
     rx_missed_errors: 0
     tx_aborted_errors: 0
     tx_carrier_errors: 0
     tx_fifo_errors: 0
     tx_heartbeat_errors: 0
     tx_window_errors: 0
     rx_long_length_errors: 0
     rx_short_length_errors: 0
     rx_align_errors: 0
     rx_csum_offload_errors: 0
     dropped_smbus: 0
     rx_errors: 0
     tx_errors: 0
     tx_dropped: 0
     collisions: 0
     rx_length_errors: 0
     rx_over_errors: 0
     rx_crc_errors: 0
     rx_frame_errors: 0
     rx_missed_errors: 0
     tx_aborted_errors: 0
     tx_carrier_errors: 78
     tx_fifo_errors: 0
     tx_heartbeat_errors: 0
     tx_window_errors: 0
     rx_long_length_errors: 0
     rx_short_length_errors: 0
     rx_align_errors: 0
     rx_csum_offload_errors: 0
     dropped_smbus: 0
"""

IS_ERRORS_ON_INTERFACE_PAIR_2 = (ETHTOOL_S_OUT_2, True)

CARRIER_COUNTER_INCREASED_PAIR_2 = (ETHTOOL_S_OUT_2, True)

#SSH command: arping -c 3 -I <interface_name>
ARPING_C_3_I_OUT_1 = """ARPING 10.11.94.254 from 10.11.94.200 eth0
Unicast reply from 10.11.94.254 [40:A6:E8:F5:43:56]  1.745ms
Unicast reply from 10.11.94.254 [40:A6:E8:F5:43:56]  4.708ms
Unicast reply from 10.11.94.254 [40:A6:E8:F5:43:56]  1.777ms
Sent 3 probes (1 broadcast(s))
Received 3 response(s)
"""

IS_PACKETS_RECEIVED_PAIR_1 = (ARPING_C_3_I_OUT_1, True)