id: next_hop_router_inaccessible_checkpoint
friendly_name: Next hop is inaccessible
start_block: get_device_tags

blocks:
  get_device_tags:
    type: device_tags
    name: Get device tags
    register_to: my_device_tags
    go_to: make_variant_matches

  make_variant_matches:
    type: logic
    name: Match device against variant
    args: [my_device_tags]
    method: check_variant_matches
    register_to: bool_variant_matches
    go_to: check_if_variant_matches

  check_if_variant_matches:
    type: if
    name: Check if device matches requirements
    condition: bool_variant_matches
    then_go_to: get_issue_items
    else_go_to: variant_conclusion

  variant_conclusion:
    type: conclusion
    name: Incompatible workflow
    triage_conclusion: |
      This workflow is incompatible with the device.
    triage_remediation_steps: |
      Contact Indeni and request that compatibility be added.

  get_issue_items:
    type: issue_items
    name: Get issue items
    register_to: my_items
    go_to: start_loop

  start_loop:
    type: foreach
    name: Start loop
    register_item_to: next_hop_item
    start_block: get_if_next_hop_still_inaccessible
    blocks:

      get_if_next_hop_still_inaccessible:
        type: device_task
        name: Get if next hop still inaccessible
        runner:
          type: SSH
          command: arp -an | grep {{next_hop_item}} | cat
        parser:
          method: is_next_hop_still_inaccessible
          args: []
        register_to: bool_is_next_hop_still_inaccessible
        go_to: check_if_next_hop_still_inaccessible

      check_if_next_hop_still_inaccessible:
        type: if
        name: Check if next hop is still inaccessible
        condition: bool_is_next_hop_still_inaccessible
        then_go_to: get_arp_cache_table_reaches_entry_limit
        else_go_to: conclusion_next_hop_is_accessible

      conclusion_next_hop_is_accessible:
        type: conclusion
        name: Next hop is accessible
        triage_conclusion: Issue is now resolved
        triage_remediation_steps: Not required, as the issue is now resolved

      get_arp_cache_table_reaches_entry_limit:
        type: device_task
        name: Get ARP cache table reaches entry limit
        runner:
          type: SSH
          command: clish -c "show configuration arp"; arp -an | wc -l
        parser:
          method: is_arp_cache_table_limit_reached
          args: []
        register_to: bool_is_arp_cache_table_limit_reached
        go_to: check_if_arp_cache_table_limit_reached

      check_if_arp_cache_table_limit_reached:
        type: if
        name: Check if ARP cache table limit reached
        condition: bool_is_arp_cache_table_limit_reached
        then_go_to: conclusion_arp_cache_table_full
        else_go_to: get_next_hop_source_interface

      conclusion_arp_cache_table_full:
        type: conclusion
        name: ARP cache table full
        triage_conclusion: |
          ARP cache table has reached the limit configured.
        triage_remediation_steps: |
          Configure a higher limit. Make sure to configure a value large enough to accommodate at least 100 dynamic entries, in addition to the maximum number of static entries (Gaia Admin Guide, Configuring ARP section).

      get_next_hop_source_interface:
        type: device_task
        name: Get next hop source interface
        runner:
          type: SSH
          command: ip r get {{next_hop_item}}
        parser:
          method: get_next_hop_source_interface
          args: []
        register_to: next_hop_source_interface
        go_to: check_if_next_hop_source_interface_returned

      check_if_next_hop_source_interface_returned:
        type: if
        name: Check if next hop source interface returned
        condition: next_hop_source_interface != ''
        then_go_to: get_arp_table_interface
        else_go_to: conclusion_no_interface_configured

      conclusion_no_interface_configured:
        type: conclusion
        name: No interface configured
        triage_conclusion: |
          There is no interface configured in the same subnet as {{next_hop_item}}.
        triage_remediation_steps: |
          Configure one interface in the correct subnet to allow communication.

      get_arp_table_interface:
        type: device_task
        name: Get arp table interface
        runner:
          type: SSH
          command: arp -an | grep {{next_hop_item}} | cat
        parser:
          method: get_arp_table_interface
          args: []
        register_to: arp_table_interface
        go_to: check_if_arp_table_interface_equals_routing_interface

      check_if_arp_table_interface_equals_routing_interface:
        type: if
        name: Check if ARP table interface equals routing interface
        condition: next_hop_source_interface == arp_table_interface
        then_go_to: get_ip_link_show_output
        else_go_to: conclusion_multiple_interfaces_with_same_range_of_ips

      conclusion_multiple_interfaces_with_same_range_of_ips:
        type: conclusion
        name: Multiple interfaces with same range of IPs
        triage_conclusion: |
          At least two interfaces are configured in same subnet or overlapping a range of IPs.
        triage_remediation_steps:  |
          Check which interface(s) are configured incorrectly.

      get_ip_link_show_output:
        type: device_task
        name: Get ip link show output
        runner:
          type: SSH
          command: ip link show {{next_hop_source_interface}}
        parser:
          method: identity_parser
          args: []
        register_to: str_ip_link_show_interface_output
        go_to: check_if_interface_admin_state_up

      check_if_interface_admin_state_up:
        type: if
        name: Check if interface admin state up
        condition: "'UP' in str_ip_link_show_interface_output"
        then_go_to: check_if_interface_link_state_up
        else_go_to: conclusion_interface_to_next_hop_is_admin_down

      conclusion_interface_to_next_hop_is_admin_down:
        type: conclusion
        name: Interface to next hop is admin down
        triage_conclusion: |
          This device is not able to contact next-hop host because interace {{next_hop_source_interface}} is administratively disabled.
        triage_remediation_steps: |2
          Enable interface
            In clish:
              set interface {{next_hop_source_interface}} state on
              save config

      check_if_interface_link_state_up:
        type: if
        name: Check if interface link state is up
        condition: "'LOWER_UP' in str_ip_link_show_interface_output"
        then_go_to: check_if_arp_enabled_on_interface
        else_go_to: conclusion_interface_to_next_hop_link_is_down

      conclusion_interface_to_next_hop_link_is_down:
        type: conclusion
        name: Interface to next hop link is down
        triage_conclusion: |
          This device is not able to contact next-hop host because interface {{next_hop_source_interface}} has no link.
        triage_remediation_steps: |
          Check cable connected to the interface.

      check_if_arp_enabled_on_interface:
        type: if
        name: Check if ARP enabled on interface
        condition: "'NOARP' not in str_ip_link_show_interface_output"
        then_go_to: get_interface_counters
        else_go_to: conclusion_arp_disabled_on_interface

      conclusion_arp_disabled_on_interface:
        type: conclusion
        name: ARP disabled on interface
        triage_conclusion: Interface to next-hop has ARP disabled
        triage_remediation_steps: |2
          Enable ARP on interface
            Expert:
              ip link set {{next_hop_source_interface}} arp on

      get_interface_counters:
        type: device_task
        name: Get interface counters
        runner:
          type: SSH
          command: ethtool -S {{next_hop_source_interface}} | grep -E 'error|drop|collision|carrier' | cat; sleep 2; ethtool -S {{next_hop_source_interface}} | grep -E 'error|drop|collision|carrier' | cat
        parser:
          method: identity_parser
          args: []
        register_to: str_ethtool_output
        go_to: get_if_errors_on_interface

      get_if_errors_on_interface:
        type: logic
        name: Get if errors on interface
        method: is_errors_on_interface
        args: [str_ethtool_output]
        register_to: bool_errors_on_interface
        go_to: check_if_errors_on_interface

      check_if_errors_on_interface:
        type: if
        name: Check if errors on interface
        condition: bool_errors_on_interface
        then_go_to: conclusion_errors_on_interface
        else_go_to: get_if_carrier_counter_increases

      conclusion_errors_on_interface:
        type: conclusion
        name: Errors on interface
        triage_conclusion: |
          The device is not able to contact next hop host because interface {{next_hop_source_interface}} has errors on interface.
        triage_remediation_steps: |
          Check cable and port on device.

      get_if_carrier_counter_increases:
        type: logic
        name: Get if carrier counter increased
        method: is_carrier_counter_increased
        args: [str_ethtool_output]
        register_to: bool_carrier_counter_increased
        go_to: check_if_carrier_counter_increases

      check_if_carrier_counter_increases:
        type: if
        name: Check if carrier counter increases
        condition: bool_carrier_counter_increased
        then_go_to: conclusion_interface_flapping
        else_go_to: get_arp_protocol

      conclusion_interface_flapping:
        type: conclusion
        name: Interface flapping
        triage_conclusion: |
          {{next_hop_source_interface}} on this device carrier counter indicates interface is flapping.
        triage_remediation_steps: |
          Check cable and port on device.

      get_arp_protocol:
        type: device_task
        name: Test ARP protocol connectivity
        runner:
          type: SSH
          command: arping -c 3 -I {{next_hop_source_interface}} {{next_hop_item}}
        parser:
          method: is_packets_received
          args: []
        register_to: is_packets_received
        go_to: check_if_packets_recieved

      check_if_packets_recieved:
        type: if
        name: Check if packets are received
        condition: is_packets_received
        then_go_to: conclusion_arp_table_not_updated
        else_go_to: conclusion_detailed_analysis_needed

      conclusion_arp_table_not_updated:
        type: conclusion
        name: ARP table not updated
        triage_conclusion: |
          ARP request is replied by {{next_hop_item}} but table is not updated with the host MAC address.
        triage_remediation_steps: |
          Collect all test outputs info and open a service request to Check Point support.

      conclusion_detailed_analysis_needed:
        type: conclusion
        name: Detailed analysis needed
        triage_conclusion: |
          Extra troubleshooting should ne made by the firewall administrator.
        triage_remediation_steps: |
          - Check duplex and negotiation on connected device
          - Packet capture (tcpdump, fwmonitor)
          - Kernel debug
