import re
from typing import List
from parser_service.public.helper_methods import *


def check_variant_matches(**kwargs) -> bool:
    # This should work on all management devices
    tags = kwargs.get('my_device_tags', None)
    if tags is None:
        return False
    if tags.get('product') in ['firewall', 'load-balancer']:
        return False
    return True


def panos_get_all_device_info(raw_data: str) -> list:
    parsed = parse_data_as_xml(raw_data)
    disconnected_devices = []
    devices = parsed['response']['result']['devices']['entry']
    for device in devices:
        if device['connected'] == 'no':
            disconnected_device_ip = device['ip-address']
            disconnected_devices.append(disconnected_device_ip)
    return disconnected_devices


def check_disconnected_devices(disconnected_devices: list, firewall_item: str) -> bool:
    if disconnected_devices and firewall_item:
        if firewall_item in disconnected_devices:
            return True
    return False


def panos_get_mgmt_ip_address(raw_data: str) -> str:
    parsed = parse_data_as_xml(raw_data)
    ip_address = parsed['response']['result']['system']['ip-address']
    return ip_address


def parse_traceroute_to_firewall(raw_data: str, firewall_item_ip: str) -> bool:
    last_line = raw_data.splitlines()[-1]
    if firewall_item_ip in last_line:
        return True
    return False


def parse_ping_to_firewall(raw_data: str) -> bool:
    return 'received, 0% packet loss' in raw_data


def check_if_reachable(raw_data: str) -> bool:
    return 'bytes from' in raw_data

def clear_fw_hostname(firewall_item: str) -> str:
    if firewall_item:
        data = parse_data_as_object(firewall_item, 'clear_ip_address_from_firewall_item.textfsm')
        if data:
            return data['ip_address']
    else:
        return None