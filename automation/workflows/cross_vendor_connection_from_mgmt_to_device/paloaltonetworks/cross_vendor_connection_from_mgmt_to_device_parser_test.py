import unittest
import automation.workflows.cross_vendor_connection_from_mgmt_to_device.paloaltonetworks.cross_vendor_connection_from_mgmt_to_device_mock_data as mock
import automation.workflows.cross_vendor_connection_from_mgmt_to_device.paloaltonetworks.cross_vendor_connection_from_mgmt_to_device_parser as parser
import re


class RealDeviceTests(unittest.TestCase):

    def test_get_mgmt_ip_address(self):
        test_data = mock.SHOW_SYSTEM_INFO_MGMT_IP
        self.assertEqual(parser.panos_get_mgmt_ip_address(test_data[0]), test_data[1])

    def test_get_all_device_info_connected_yes(self):
        test_data = mock.SHOW_DEVICES_ALL_CONNECTED
        self.assertEqual(parser.panos_get_all_device_info(test_data[0]), test_data[1])

    def test_parse_traceroute_to_firewall(self):
        test_data = mock.TRACEROUTE_OK
        self.assertEqual(parser.parse_traceroute_to_firewall(test_data[0], test_data[1]), test_data[2])
        test_data = mock.TRACEROUTE_FAILED
        self.assertEqual(parser.parse_traceroute_to_firewall(test_data[0], test_data[1]), test_data[2])

    def test_parse_ping_to_firewall(self):
        test_data = mock.PING_OK
        self.assertEqual(parser.parse_ping_to_firewall(test_data[0]), test_data[1])
        test_data = mock.PING_FAILED
        self.assertEqual(parser.parse_ping_to_firewall(test_data[0]), test_data[1])


if __name__ == '__main__':
    unittest.main()