#  HTTPS command: /api?type=op&cmd=<show><system><info></info></system></show>

SHOW_SYSTEM_INFO_PANORAMA_OUT = ("""
<response status=\"success\">
    <result>
        <devices>
            <entry name=\"007200003195\">
                <serial>007200003195</serial>
                <connected>yes</connected>
                <unsupported-version>no</unsupported-version>
                <hostname>Macadamia</hostname>
                <ip-address>10.3.1.100</ip-address>
                <mac-addr/>
                <uptime>0 days, 0:01:40</uptime>
                <family>vm</family>
                <model>Panorama</model>
                <sw-version>6.1.2</sw-version>
                <app-version>490-2616</app-version>
                <av-version>1505-1980</av-version>
                <wildfire-version>56153-62868</wildfire-version>
                <threat-version>490-2616</threat-version>
                <url-db>paloaltonetworks</url-db>
                <url-filtering-version>0000.00.00.000</url-filtering-version>
                <logdb-version>6.1.3</logdb-version>
                <vpnclient-package-version/>
                <global-protect-client-package-version>0.0.0</global-protect-client-package-version>
                <vpn-disable-mode>no</vpn-disable-mode>
                <operational-mode>normal</operational-mode>
                <multi-vsys>no</multi-vsys>
                <vsys>
                    <entry name=\"vsys1\">
                        <display-name>vsys1</display-name>
                        <shared-policy-status/>
                        <shared-policy-md5sum>2d07ea397fec3ed60f0a1b1132427aa9</shared-policy-md5sum>
                    </entry>
                </vsys>
            </entry>
        </devices>
    </result>
</response>""")


SHOW_SYSTEM_INFO_MGMT_IP = (SHOW_SYSTEM_INFO_PANORAMA_OUT, '10.3.1.100')

#  HTTPS command: /api?type=op&cmd=<show><devices><all></all></devices><%2Fshow>&key=${api-key}
SHOW_DEVICES_ALL_CONNECTED_OUT = ("""
<response status=\"success\">
    <result>
        <devices>
            <entry name=\"007200003195\">
                <serial>007200003195</serial>
                <connected>yes</connected>
                <unsupported-version>no</unsupported-version>
                <hostname>Macadamia</hostname>
                <ip-address>10.3.1.1</ip-address>
                <mac-addr/>
                <uptime>0 days, 0:01:40</uptime>
                <family>vm</family>
                <model>PA-VM</model>
                <sw-version>6.1.2</sw-version>
                <app-version>490-2616</app-version>
                <av-version>1505-1980</av-version>
                <wildfire-version>56153-62868</wildfire-version>
                <threat-version>490-2616</threat-version>
                <url-db>paloaltonetworks</url-db>
                <url-filtering-version>0000.00.00.000</url-filtering-version>
                <logdb-version>6.1.3</logdb-version>
                <vpnclient-package-version/>
                <global-protect-client-package-version>0.0.0</global-protect-client-package-version>
                <vpn-disable-mode>no</vpn-disable-mode>
                <operational-mode>normal</operational-mode>
                <multi-vsys>no</multi-vsys>
                <vsys>
                    <entry name=\"vsys1\">
                        <display-name>vsys1</display-name>
                        <shared-policy-status/>
                        <shared-policy-md5sum>2d07ea397fec3ed60f0a1b1132427aa9</shared-policy-md5sum>
                    </entry>
                </vsys>
            </entry>
             <entry name=\"007200003196\">
                <serial>007200003196</serial>
                <connected>no</connected>
                <unsupported-version>no</unsupported-version>
                <hostname>Macadamia</hostname>
                <ip-address>10.3.1.2</ip-address>
                <mac-addr/>
                <uptime>0 days, 0:01:40</uptime>
                <family>vm</family>
                <model>PA-VM</model>
                <sw-version>6.1.2</sw-version>
                <app-version>490-2616</app-version>
                <av-version>1505-1980</av-version>
                <wildfire-version>56153-62868</wildfire-version>
                <threat-version>490-2616</threat-version>
                <url-db>paloaltonetworks</url-db>
                <url-filtering-version>0000.00.00.000</url-filtering-version>
                <logdb-version>6.1.3</logdb-version>
                <vpnclient-package-version/>
                <global-protect-client-package-version>0.0.0</global-protect-client-package-version>
                <vpn-disable-mode>no</vpn-disable-mode>
                <operational-mode>normal</operational-mode>
                <multi-vsys>no</multi-vsys>
                <vsys>
                    <entry name=\"vsys1\">
                        <display-name>vsys1</display-name>
                        <shared-policy-status/>
                        <shared-policy-md5sum>2d07ea397fec3ed60f0a1b1132427aa9</shared-policy-md5sum>
                    </entry>
                </vsys>
            </entry>
             <entry name=\"007200003197\">
                <serial>007200003197</serial>
                <connected>yes</connected>
                <unsupported-version>no</unsupported-version>
                <hostname>Macadamia</hostname>
                <ip-address>10.3.1.3</ip-address>
                <mac-addr/>
                <uptime>0 days, 0:01:40</uptime>
                <family>vm</family>
                <model>PA-VM</model>
                <sw-version>6.1.2</sw-version>
                <app-version>490-2616</app-version>
                <av-version>1505-1980</av-version>
                <wildfire-version>56153-62868</wildfire-version>
                <threat-version>490-2616</threat-version>
                <url-db>paloaltonetworks</url-db>
                <url-filtering-version>0000.00.00.000</url-filtering-version>
                <logdb-version>6.1.3</logdb-version>
                <vpnclient-package-version/>
                <global-protect-client-package-version>0.0.0</global-protect-client-package-version>
                <vpn-disable-mode>no</vpn-disable-mode>
                <operational-mode>normal</operational-mode>
                <multi-vsys>no</multi-vsys>
                <vsys>
                    <entry name=\"vsys1\">
                        <display-name>vsys1</display-name>
                        <shared-policy-status/>
                        <shared-policy-md5sum>2d07ea397fec3ed60f0a1b1132427aa9</shared-policy-md5sum>
                    </entry>
                </vsys>
            </entry>
         <entry name=\"007200003198\">
                <serial>007200003198</serial>
                <connected>no</connected>
                <unsupported-version>no</unsupported-version>
                <hostname>Macadamia</hostname>
                <ip-address>10.3.1.4</ip-address>
                <mac-addr/>
                <uptime>0 days, 0:01:40</uptime>
                <family>vm</family>
                <model>PA-VM</model>
                <sw-version>6.1.2</sw-version>
                <app-version>490-2616</app-version>
                <av-version>1505-1980</av-version>
                <wildfire-version>56153-62868</wildfire-version>
                <threat-version>490-2616</threat-version>
                <url-db>paloaltonetworks</url-db>
                <url-filtering-version>0000.00.00.000</url-filtering-version>
                <logdb-version>6.1.3</logdb-version>
                <vpnclient-package-version/>
                <global-protect-client-package-version>0.0.0</global-protect-client-package-version>
                <vpn-disable-mode>no</vpn-disable-mode>
                <operational-mode>normal</operational-mode>
                <multi-vsys>no</multi-vsys>
                <vsys>
                    <entry name=\"vsys1\">
                        <display-name>vsys1</display-name>
                        <shared-policy-status/>
                        <shared-policy-md5sum>2d07ea397fec3ed60f0a1b1132427aa9</shared-policy-md5sum>
                    </entry>
                </vsys>
            </entry>
        </devices>
    </result>
</response>
""")


SHOW_DEVICES_ALL_CONNECTED = (SHOW_DEVICES_ALL_CONNECTED_OUT, ['10.3.1.2', '10.3.1.4'])

TRACEROUTE_OK = ("""
traceroute to 10.32.1.5 (10.32.1.5)1, 30 hops max, 40 byte packets
1 172.16.10.1 (172.16.10.40) 121.652 ms 121.624 ms 121.583 ms
2 172.18.255.2 (172.18.255.2) 124.098 ms 124.062 ms 124.279 ms
3 172.31.254.3 (172.31.254.3) 123.532 ms 123.586 ms 124.119 ms
4 10.32.25.1 (10.32.25.1) 0.292 ms 0.338 ms 0.386 ms
5 10.32.1.5 (10.32.1.5) 2.910 ms 2.963 ms 3.067 ms
""", '10.32.1.5', True)

TRACEROUTE_FAILED = ("""
traceroute to 10.32.1.5 (172.16.10.40), 30 hops max, 40 byte packets
1 172.16.10.1 (172.16.10.40) 121.652 ms 121.624 ms 121.583 ms
2 172.18.255.2 (172.18.255.2) 124.098 ms 124.062 ms 124.279 ms
3 172.31.254.3 (172.31.254.3) 123.532 ms 123.586 ms 124.119 ms
4 * * *
5 * * *
6 * * *
7 * * *
""", '10.32.1.5', False)

PING_OK = ("""PING 10.3.1.11 (10.3.1.11) 56(84) bytes of data.
64 bytes from 10.3.1.11: icmp_seq=1 ttl=64 time=0.069 ms
64 bytes from 10.3.1.11: icmp_seq=2 ttl=64 time=0.041 ms
64 bytes from 10.3.1.11: icmp_seq=3 ttl=64 time=0.040 ms

--- 10.3.1.11 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2032ms
rtt min/avg/max/mdev = 0.040/0.050/0.069/0.013 ms
""", True)

PING_FAILED = ("""PING 10.3.1.11 (10.3.1.11) 56(84) bytes of data.

--- 10.3.1.11 ping statistics ---
3 packets transmitted, 0 received, 100% packet loss, time 2050ms""", False)