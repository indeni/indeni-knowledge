id: mgmt_device_not_connected_paloaltonetworks
friendly_name: Gateway is not communicating with the management
start_block: get_device_tags

blocks:
  get_device_tags:
    type: device_tags
    name: Get device tags
    register_to: my_device_tags
    go_to: make_variant_matches

  make_variant_matches:
    type: logic
    name: Match device against variant
    args: [my_device_tags]
    method: check_variant_matches
    register_to: bool_variant_matches
    go_to: check_if_variant_matches

  check_if_variant_matches:
    type: if
    name: Check if device matches requirements
    condition: bool_variant_matches
    then_go_to: get_system_info
    else_go_to: variant_conclusion

  variant_conclusion:
    type: conclusion
    name: Incompatible workflow
    triage_conclusion: |
      This workflow is incompatible with the device.
    triage_remediation_steps: |
      Contact Indeni and request that compatibility be added.

  get_system_info:
    type: device_task
    name: Get management IP address
    runner:
      type: HTTPS
      command: '/api?type=op&cmd=<show><system><info></info></system></show>'
    parser:
      method: panos_get_mgmt_ip_address
      args: []
    register_to: mgmt_ip_address
    go_to: ping_management

  ping_management:
    type: device_task
    name: Test connectivity to management server
    runner:
      type: HTTPS
      command: /api/?type=op&cmd=<cms-ping><host>{{mgmt_ip_address}}</host></cms-ping>
    parser:
      method: check_if_reachable
      args: []
    register_to: mgmt_ping_result
    go_to: check_if_panorama_reachable

  check_if_panorama_reachable:
    type: if
    name: Check if Panorama is reachable
    condition: mgmt_ping_result
    then_go_to: get_remote_port_status
    else_go_to: conclusion_panorama_unreachable

  conclusion_panorama_unreachable:
    type: conclusion
    name: Panorama is unreachable
    triage_conclusion: |
      Panorama is not reachable.
    triage_remediation_steps: |
      Please check if {{ mgmt_ip_address }} is reacheable and provisioned properly to manage firewall devices.
  
  get_remote_port_status:
    type: port_check
    name: Get remote port status
    server: "{{mgmt_ip_address}}"
    port: 3978
    register_to: panorama_port_status
    go_to: check_if_remote_port_is_open

  check_if_remote_port_is_open:
    type: if
    name: Check if Panorama port is open
    condition: panorama_port_status
    then_go_to: get_all_device_info
    else_go_to: conclusion_panorama_port_not_open

  conclusion_panorama_port_not_open:
    type: conclusion
    name: Panorama port is not open
    triage_conclusion: |
      Panorama port is not open
    triage_remediation_steps: |
      Please check if port 3978 is open on {{ mgmt_ip_address }}.
  
  get_all_device_info:
    type: device_task
    name: Get all device info from the management server
    runner:
      type: HTTPS
      command: '/api?type=op&cmd=<show><devices><all></all></devices></show>'
    parser:
      method: panos_get_all_device_info
      args: []
    register_to: disconnected_devices
    go_to: get_issue_items 

  get_issue_items:
    type: issue_items
    name: Get issue items
    register_to: my_items
    go_to: start_loop

  start_loop:
    type: foreach
    name: Start loop
    register_item_to: firewall_item
    start_block: clear_host_name
    blocks:

      clear_host_name:
        type: logic
        name: Clear FW host IP address
        args: [firewall_item]
        method: clear_fw_hostname
        register_to: firewall_item_ip
        go_to: verify_device_disconnected

      verify_device_disconnected:
        type: logic
        name: Check firewall in disconnected items
        args: [disconnected_devices, firewall_item]
        method: check_disconnected_devices
        register_to: bool_disconnected_devices
        go_to: if_device_disconnected

      if_device_disconnected:
        type: if 
        name: Check if device still disconnected
        condition: bool_disconnected_devices
        then_go_to: conclusion_remote_device_reachable
        else_go_to: ping_to_firewall

      ping_to_firewall:
        type: device_task
        name: Ping to firewall
        runner:
          type: HTTPS
          command: /api/?type=op&cmd=<cms-ping><host>{{firewall_item_ip}}</host></cms-ping>
        parser:
          method: parse_ping_to_firewall
          args: []
        register_to: ping_result
        go_to: check_if_ping_succeded

      check_if_ping_succeded:
        type: if 
        name: Check if ping to device succeded
        condition: ping_result
        then_go_to: conclusion_remote_device_reachable_1
        else_go_to: traceroute_to_firewall

      traceroute_to_firewall:
        type: device_task
        name: Traceroute to firewall
        runner:
          type: SSH
          command: traceroute source {{mgmt_ip_address}} host {{firewall_item_ip}}
        parser:
          method: parse_traceroute_to_firewall
          args: [firewall_item_ip]
        register_to: traceroute_result
        go_to: check_if_traceroute_succeded

      check_if_traceroute_succeded:
        type: if 
        name: Check if traceroute to device succeded
        condition: traceroute_result
        then_go_to: conclusion_remote_device_reachable_2
        else_go_to: conclusion_routing_issue_may_exist

      conclusion_routing_issue_may_exist:
        type: conclusion
        name: Routing issue may exist between hosts
        triage_conclusion: |
          Routing issue likely exists between between Panorama and firewall
        triage_remediation_steps: |
          -Troubleshooting routing on layer 3 devices listed as last hops responding for existence of routes containing the panorama and fireall ip subnets.
          -If management profile of the management interfaces on all devices do not  allow pings from each other ensure that they do.
          -Firewall or Panorama may have gone donwn.
          -Check for additional Indeni notifications.

      conclusion_remote_device_reachable:
        type: conclusion
        name: Remote device is reachable
        triage_conclusion: |
          The remote device {{firewall_item}} is reachable.
        triage_remediation_steps: |
          The remote device is reachable, the issue seems solved now.

      conclusion_remote_device_reachable_1:
        type: conclusion
        name: Remote device is reachable
        triage_conclusion: |
          The remote device {{firewall_item}} is reachable.
        triage_remediation_steps: |
          The remote device is reachable, the issue seems solved now.

      conclusion_remote_device_reachable_2:
        type: conclusion
        name: Remote device is reachable
        triage_conclusion: |
          The remote device {{firewall_item}} is reachable.
        triage_remediation_steps: |
          The remote device is reachable, the issue seems solved now.

