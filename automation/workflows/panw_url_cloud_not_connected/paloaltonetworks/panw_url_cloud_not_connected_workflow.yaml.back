id: url_cloud_not_connected_paloaltonetworks
friendly_name: URL Cloud not connected
start_block: get_device_tags

blocks:
  get_device_tags:
    type: device_tags
    name: Get device tags
    register_to: my_device_tags
    go_to: collect_url_cloud_status

  collect_url_cloud_status:
    type: device_task
    name: Collect URL-Cloud status
    runner:
      type: HTTPS
      command: '/api/?type=op&cmd=<show><url-cloud><status></status></url-cloud></show>'
    parser:
      method: parse_url_cloud_status
      args: []
    register_to: url_cloud_status
    go_to: check_url_status

  check_url_status:
    type: if
    name: Check if url-cloud is connected
    condition: url_cloud_status
    then_go_to: url_cloud_connected
    else_go_to: check_url_server_dns

  url_cloud_connected:
    type: conclusion
    name: URL-Cloud is connected
    triage_conclusion: 'URL-Cloud status is connected'
    triage_remediation_steps: ""

  check_url_server_dns:
    type: device_task
    name: Check URL cloud DNS
    runner:
      type: SSH
      command: 'ping count 10 host serverlist.urlcloud.paloaltonetworks.com'
    parser:
      method: parse_url_cloud_dns
      args: []
    register_to: url_cloud_dns
    go_to: test_url_server_dns_status

  test_url_server_dns_status:
    type: if
    name: Check if url-cloud server can be resolved
    condition: url_cloud_dns
    then_go_to: test_url_server_reachability
    else_go_to: url_cloud_server_can_not_be_resolved

  url_cloud_server_can_not_be_resolved:
    type: conclusion
    name: URL-Cloud server FQDN can not be resolved
    triage_conclusion: 'URL-Cloud server FQDN can not be resolved'
    triage_remediation_steps: |
            Please ensure the DNS resolver is reachable and if it can resolve serverlist.urlcloud.paloaltonetworks.com.

  test_url_server_reachability:
    type: device_task
    name: Check URL cloud reachability
    runner:
      type: SSH
      command: 'ping count 10 host serverlist.urlcloud.paloaltonetworks.com'
    parser:
      method: parse_url_cloud_reachability
      args: []
    register_to: url_cloud_reachability
    go_to: check_reachability_status

  check_reachability_status:
    type: if
    name: Check if url-cloud server is reachable
    condition: url_cloud_reachability
    then_go_to: url_cloud_reachable
    else_go_to: test_url_categorization

  url_cloud_reachable:
    type: conclusion
    name: URL-Cloud server is reachable
    triage_conclusion: 'URL-Cloud server is reachable'
    triage_remediation_steps: ""

  test_url_categorization:
    type: device_task
    name: Test URL Categorization
    runner:
      type: SSH
      command: 'test url indeni.com'
    parser:
      method: parse_test_url_categorization
      args: []
    register_to: indeni_url_category
    go_to: check_url_lookup

  check_url_lookup:
    type: if
    name: Check if URL lookup is working
    condition: indeni_url_category
    then_go_to: url_lookup_is_functional
    else_go_to: issue_can_not_be_determined

  url_lookup_is_functional:
    type: conclusion
    name: PANDB Lookup is working
    triage_conclusion: 'PANDB Lookup is working'
    triage_remediation_steps: ""

  issue_can_not_be_determined:
    type: conclusion
    name: Connection to URL Cloud is down, and unable to determine the root cause
    triage_conclusion: 'Unable to determine the root cause, manual investigation is required.'
    triage_remediation_steps: |
            Please check the output of 'show url-cloud status' and contact Palo Alto Networks support team if the status is not connected