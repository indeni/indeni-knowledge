import unittest
import automation.workflows.panw_url_cloud_not_connected.paloaltonetworks.panw_url_cloud_not_connected_mock_data as mock
import automation.workflows.panw_url_cloud_not_connected.paloaltonetworks.panw_url_cloud_not_connected_parser as parser



class UrlCloudNotConnectedParserTests(unittest.TestCase):

    def test_parse_url_cloud_status(self):
        self.assertEqual(parser.parse_url_cloud_status(mock.PARSE_URL_CLOUD_STATUS_1[0]), mock.PARSE_URL_CLOUD_STATUS_1[1])
        self.assertEqual(parser.parse_url_cloud_status(mock.PARSE_URL_CLOUD_STATUS_2[0]), mock.PARSE_URL_CLOUD_STATUS_2[1])

    def test_parse_url_cloud_dns(self):
        self.assertEqual(parser.parse_url_cloud_dns(mock.PARSE_URL_CLOUD_DNS_1[0]), mock.PARSE_URL_CLOUD_DNS_1[1])
        self.assertEqual(parser.parse_url_cloud_dns(mock.PARSE_URL_CLOUD_DNS_2[0]), mock.PARSE_URL_CLOUD_DNS_2[1])

    def test_parse_url_cloud_reachability(self):
        self.assertEqual(parser.parse_url_cloud_reachability(mock.PARSE_URL_CLOUD_REACHABILITY_1[0]), mock.PARSE_URL_CLOUD_REACHABILITY_1[1])
        self.assertEqual(parser.parse_url_cloud_reachability(mock.PARSE_URL_CLOUD_REACHABILITY_2[0]), mock.PARSE_URL_CLOUD_REACHABILITY_2[1])


    def test_parse_test_url_categorization(self):
        self.assertEqual(parser.parse_test_url_categorization(mock.PARSE_TEST_URL_CATEGORIZATION_1[0]), mock.PARSE_TEST_URL_CATEGORIZATION_1[1])
        self.assertEqual(parser.parse_test_url_categorization(mock.PARSE_TEST_URL_CATEGORIZATION_2[0]), mock.PARSE_TEST_URL_CATEGORIZATION_2[1])


if __name__ == '__main__':
    unittest.main()