PARSE_URL_CLOUD_STATUS_1 = ("""
<response cmd="status" status="success"><result><url-cloud-status>
	 <type>PAN-DB URL Filtering</type>
	<license>valid</license>
	<current-cloud-server>serverlist.urlcloud.paloaltonetworks.com</current-cloud-server>
	<cloud-connection>connected</cloud-connection>
	<cloud-mode>public</cloud-mode>
	 <url-database-version-device>20200308.20028</url-database-version-device>
	 <url-database-version-cloud>20200308.20028</url-database-version-cloud>
	 <url-database-last-update-time>2020/03/07 17:48:47 </url-database-last-update-time>
	 <url-database-status>good</url-database-status>
	 <url-protocol-version-device>pan/0.0.2</url-protocol-version-device>
	 <url-protocol-version-cloud>pan/2.0.0</url-protocol-version-cloud>
	 <protocol-compatibility-status>compatible</protocol-compatibility-status>
</url-cloud-status>
</result></response>
""", True)

PARSE_URL_CLOUD_STATUS_2 = ("""
<response cmd="status" status="success"><result><url-cloud-status>
	 <type>PAN-DB URL Filtering</type>
	<license>valid</license>
	<cloud-connection>not connected</cloud-connection>
	 <url-database-version-device>20200302.20105</url-database-version-device>
	 <url-database-version-cloud>20200302.20105</url-database-version-cloud>
	 <url-database-last-update-time>2020/03/01 22:57:07 </url-database-last-update-time>
	 <url-database-status>good</url-database-status>
	 <url-protocol-version-device>pan/0.0.2</url-protocol-version-device>
	 <url-protocol-version-cloud>pan/2.0.0</url-protocol-version-cloud>
	 <protocol-compatibility-status>compatible</protocol-compatibility-status>
</url-cloud-status>
</result></response>
""", False)

PARSE_URL_CLOUD_DNS_1 = ("""
<response status="success">
<result>
<value type="ipv4">ipv4 not resolved</value>
<value type="ipv6">ipv6 not resolved</value>
</result>
</response>
""", {'success': 0, 'ip_address': 'ipv4 not resolved'})


PARSE_URL_CLOUD_DNS_2 = ("""
<response status="success">
<result>
<value type="ipv4">154.59.123.123</value>
<value type="ipv6">ipv6 not resolved</value>
</result>
</response>
""", {'success': 1, 'ip_address': '154.59.123.123'})



PARSE_URL_CLOUD_REACHABILITY_1 = ("""
<response status="success">
<result>PING 154.59.123.123(172.59.123.123) 56(84) bytes of data. --- 154.59.123.123 ping statistics --- 10 packets transmitted, 0 received, 100% packet loss, time 9000ms </result>
</response>
""", False)

PARSE_URL_CLOUD_REACHABILITY_2 = ("""
<response status="success">
<result>PING 154.59.123.123 (154.59.123.123) 56(84) bytes of data. 64 bytes from 154.59.123.123: icmp_seq=1 ttl=53 time=58.7 ms 64 bytes from 154.59.123.123: icmp_seq=2 ttl=53 time=58.4 ms 64 bytes from 154.59.123.123: icmp_seq=3 ttl=53 time=58.3 ms 64 bytes from 154.59.123.123: icmp_seq=4 ttl=53 time=58.4 ms 64 bytes from 154.59.123.123: icmp_seq=5 ttl=53 time=58.2 ms 64 bytes from 154.59.123.123: icmp_seq=6 ttl=53 time=58.4 ms 64 bytes from 154.59.123.123: icmp_seq=7 ttl=53 time=58.6 ms 64 bytes from 154.59.123.123: icmp_seq=8 ttl=53 time=58.3 ms 64 bytes from 154.59.123.123: icmp_seq=9 ttl=53 time=58.3 ms 64 bytes from 154.59.123.123: icmp_seq=10 ttl=53 time=58.4 ms --- 154.59.123.123 ping statistics --- 10 packets transmitted, 10 received, 0% packet loss, time 9005ms rtt min/avg/max/mdev = 58.282/58.445/58.725/0.255 ms </result>
</response>
""", True)

PARSE_TEST_URL_CATEGORIZATION_1 = ("""
<response cmd="status" status="success">
<result>indeni.com not-resolved (Base db) mlav_flag=0 expires in 5 seconds indeni.com cloud-unavailable (Cloud db) </result>
</response>
""", False)

PARSE_TEST_URL_CATEGORIZATION_2 = ("""
<response cmd="status" status="success">
<result>indeni.com not-resolved (Base db) expires in 4 seconds indeni.com computer-and-internet-info low-risk (Cloud db)</result>
</response>
""", True)