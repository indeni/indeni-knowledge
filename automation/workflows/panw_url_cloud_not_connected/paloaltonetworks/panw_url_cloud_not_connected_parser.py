from parser_service.public import helper_methods
from indeni_workflow.workflow_test_tool import WorkflowTestTool
import re

def parse_url_cloud_status(raw_data: str) -> bool:
    if raw_data:
        data = helper_methods.parse_data_as_xml(raw_data)
        status = data['response']['result']['url-cloud-status']['cloud-connection']
    return re.search(r'not connected', status) is None

def parse_url_cloud_dns(raw_data: str) -> bool:
    if raw_data:
        raw_data_parsed = helper_methods.parse_data_as_xml(raw_data)
        found = {}
        for single_result in raw_data_parsed['response']['result']['value']:
            if single_result['@type'] == 'ipv4':
                if single_result['#text'] == 'ipv4 not resolved':
                    found['success'] = 0
                else:
                    found['success'] = 1
                found['ip_address'] = single_result['#text']
        return found

def parse_url_cloud_reachability(raw_data: str) -> bool:
    if raw_data:
        return re.search(r'10 received', helper_methods.parse_data_as_xml(raw_data)['response']['result']) is not None

def parse_test_url_categorization(raw_data: str) -> bool:
    if raw_data:
        return re.search(r'cloud-unavailable', helper_methods.parse_data_as_xml(raw_data)['response']['result']) is None
