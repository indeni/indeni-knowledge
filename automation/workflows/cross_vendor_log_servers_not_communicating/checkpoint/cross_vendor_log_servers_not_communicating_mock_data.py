# SSH command: netstat -na | grep -E "257|18192"
NETSTAT_NA_GREP_257_18192_OUT_1 = """
    tcp        0      0 0.0.0.0:257                 0.0.0.0:*                   LISTEN
    tcp        0      0 0.0.0.0:62573               0.0.0.0:*                   LISTEN
    tcp        0      0 0.0.0.0:18192               0.0.0.0:*                   LISTEN
    tcp        0      0 10.11.94.10:18192           10.11.94.30:50263           REJECTED
    tcp        0      0 10.11.94.10:18192           10.11.94.30:33232           REJECTED
    tcp        0      0 10.11.94.10:43577           10.11.94.30:257             REJECTED
    tcp        0      0 10.11.94.10:18192           10.11.94.45:44601           REJECTED
    tcp        0      0 10.11.94.10:18192           10.11.94.31:257             REJECTED
"""

COLLECT_LOGGING_COMMUNICATION_STATUS_PAIR_1 = (
(NETSTAT_NA_GREP_257_18192_OUT_1, '10.11.94.10'), {'log_established': [], 'sic_established': []})

NETSTAT_NA_GREP_257_18192_OUT_2 = """tcp        0      0 0.0.0.0:257                 0.0.0.0:*                   LISTEN
#tcp        0      0 0.0.0.0:62573               0.0.0.0:*                   LISTEN
#tcp        0      0 0.0.0.0:18192               0.0.0.0:*                   LISTEN
#tcp        0      0 10.11.94.10:18192           10.11.94.30:50263           REJECTED
#tcp        0      0 10.11.94.10:18192           10.11.94.30:33232           ESTABLISHED
#tcp        0      0 10.11.94.10:43577           10.11.94.30:257             REJECTED
#tcp        0      0 10.11.94.10:18192           10.11.94.45:44601           REJECTED
#tcp        0      0 10.11.94.10:43577           10.11.94.31:257             ESTABLISHED"""

COLLECT_LOGGING_COMMUNICATION_STATUS_PAIR_2 = (
(NETSTAT_NA_GREP_257_18192_OUT_2, '10.11.94.31',), {'log_established': ['10.11.94.31'], 'sic_established': []})

# SSH command: cat $FWDIR/conf/masters
CAT_FWDIR_CONF_MASTERS_OUT_1 = """
[Policy]
asdasdasd asd asd
[Log]
10.11.94.30
10.11.94.31
"""

COLLECT_LOG_SERVERS_CONFIGURED_PAIR_1 = (CAT_FWDIR_CONF_MASTERS_OUT_1, ['10.11.94.30', '10.11.94.31'])

# SSH command: ps aux | grep fwd | grep -v grep | awk '{print $2}' | xargs -I {} ps -p {} -o %cpu,%mem
PS_AUX_CPU_MEM_OUT_1 = """    %CPU %MEM
    3.6  1.2
"""

COLLECT_FWD_PROCESS_STATUS_PAIR_1 = (PS_AUX_CPU_MEM_OUT_1, {'cpu_usage': 3.6, 'memory_usage': 1.2})

# SSH command: ip route get 10.11.94.31
IP_R_GET_OUT_1 = """
    10.11.94.31 dev eth0  src 10.11.94.10
    cache  mtu 1500 advmss 1460 hoplimit 64
"""

COLLECT_ROUTING_INTERFACE_PAIR_1 = ((IP_R_GET_OUT_1, "10.11.94.31"), "eth0")

# SSH command: ip route get 130.0.81.141
IP_R_GET_OUT_2 = """
    130.0.81.141 via 37.24.242.233 dev eth7 src 37.24.242.234
    cache  mtu 1500 advmss 1460 hoplimit 64
"""

COLLECT_ROUTING_INTERFACE_PAIR_2 = ((IP_R_GET_OUT_2, "130.0.81.141"), "eth7")

# SSH  command: ping -c 10 10.11.94.31
PING_OUT_1 = """
    64 bytes from 10.11.94.30: icmp_seq=1 ttl=64 time=1.23 ms
    64 bytes from 10.11.94.30: icmp_seq=2 ttl=64 time=0.480 ms
    64 bytes from 10.11.94.30: icmp_seq=3 ttl=64 time=0.430 ms
    --- 10.11.94.30 ping statistics ---
    10 packets transmitted, 20 received, 0% packet loss, time 9012ms
    rtt min/avg/max/mdev = 0.427/0.830/1.978/0.543 ms
"""

COLLECT_CONNECTION_STATUS_PAIR_1 = (PING_OUT_1, 20)

# SSH  command: ip link show eth0
IP_LINK_SHOW_OUT_1 = """
    2: eth0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc mq state UP group default qlen 1000
        link/ether 00:50:56:ac:f2:b1 brd ff:ff:ff:ff:ff:ff
"""

COLLECT_INTERFACE_ADMIN_STATE_PAIR_1 = (
(IP_LINK_SHOW_OUT_1, "eth0"), {'admin_is_down': False, 'admin_is_enabled': True})

IP_LINK_SHOW_OUT_2 = """2: eth0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc mq state DOWN group default qlen 1000"""

COLLECT_INTERFACE_ADMIN_STATE_PAIR_2 = ((IP_LINK_SHOW_OUT_2, "eth0"), {'admin_is_down': True, 'admin_is_enabled': True})

# SSH  command: ethtool -S eth0 | grep -E 'error|drop|collision|carrier'
ETHTOOL_OUT_1 = """
    rx_errors: 0
    tx_errors: 0
    rx_dropped: 0
    tx_dropped: 4
    collisions: 0
"""

COLLECT_INTERFACE_ERRORS_PAIR_1 = (ETHTOOL_OUT_1, 4)

ETHTOOL_OUT_2 = """'
    rx_errors: 2
    tx_errors: 0
    rx_dropped: 0
    tx_dropped: 4
    collisions: 0
"""

COLLECT_INTERFACE_ERRORS_PAIR_2 = (ETHTOOL_OUT_2, 6)

# Logic block data

GET_SERVER_TO_INVESTIGATE_PAIR_1 = ('10.11.94.31', "10.11.94.31")
