import unittest
import automation.workflows.cross_vendor_log_servers_not_communicating.checkpoint.cross_vendor_log_servers_not_communicating_parser as parser
import automation.workflows.cross_vendor_log_servers_not_communicating.checkpoint.cross_vendor_log_servers_not_communicating_mock_data as mock


class CrossVendorLogServersNotCommunicatingTest(unittest.TestCase):

    def test_collect_logging_communication_status(self):
        pair1 = mock.COLLECT_LOGGING_COMMUNICATION_STATUS_PAIR_1
        pair2 = mock.COLLECT_LOGGING_COMMUNICATION_STATUS_PAIR_2
        self.assertEqual(parser.collect_logging_communication_status(pair1[0][0], log_server_item=pair1[0][1]),
                         pair1[1])
        self.assertEqual(parser.collect_logging_communication_status(pair2[0][0], log_server_item=pair2[0][1]),
                         pair2[1])

    def test_collect_log_servers_configured(self):
        pair1 = mock.COLLECT_LOG_SERVERS_CONFIGURED_PAIR_1
        self.assertEqual(parser.collect_log_servers_configured(pair1[0]), pair1[1])

    def test_collect_fwd_process_status(self):
        pair1 = mock.COLLECT_FWD_PROCESS_STATUS_PAIR_1
        self.assertEqual(parser.collect_fwd_process_status(pair1[0]), pair1[1])

    def test_get_server_to_investigate(self):
        pair1 = mock.GET_SERVER_TO_INVESTIGATE_PAIR_1
        self.assertEqual(parser.get_server_to_investigate(log_server_item=pair1[0]), pair1[1])

    def test_collect_routing_interface_1(self):
        pair1 = mock.COLLECT_ROUTING_INTERFACE_PAIR_1
        self.assertEqual(parser.collect_routing_interface(pair1[0][0], server_ip=pair1[0][1]), pair1[1])

    def test_collect_routing_interface_2(self):
        pair1 = mock.COLLECT_ROUTING_INTERFACE_PAIR_2
        self.assertEqual(parser.collect_routing_interface(pair1[0][0], server_ip=pair1[0][1]), pair1[1])

    def test_collect_connection_status(self):
        pair1 = mock.COLLECT_CONNECTION_STATUS_PAIR_1
        self.assertEqual(parser.collect_connection_status(pair1[0]), pair1[1])

    def test_collect_interface_admin_state(self):
        pair1 = mock.COLLECT_INTERFACE_ADMIN_STATE_PAIR_1
        pair2 = mock.COLLECT_INTERFACE_ADMIN_STATE_PAIR_2
        self.assertEqual(parser.collect_interface_admin_state(pair1[0][0], routing_interface=pair1[0][1]), pair1[1])
        self.assertEqual(parser.collect_interface_admin_state(pair2[0][0], routing_interface=pair2[0][1]), pair2[1])

    def test_collect_interface_errors(self):
        pair1 = mock.COLLECT_INTERFACE_ERRORS_PAIR_1
        pair2 = mock.COLLECT_INTERFACE_ERRORS_PAIR_2
        self.assertEqual(parser.collect_interface_errors(pair1[0]), pair1[1])
        self.assertEqual(parser.collect_interface_errors(pair2[0]), pair2[1])


if __name__ == '__main__':
    unittest.main()
