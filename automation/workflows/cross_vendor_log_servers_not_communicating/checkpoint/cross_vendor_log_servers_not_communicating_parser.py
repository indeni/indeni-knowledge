import re
import time
from typing import List


def check_variant_matches(**kwargs) -> bool:
    # This should work on all gateway devices with one of the below versions
    tags = kwargs.get('my_device_tags', None)
    if tags is None:
        return False
    versions = [x.lower() for x in ['R77.30', 'R80.10', 'R80.20', 'R80.30', 'R76SP.30', 'R76SP.40', 'R76SP.50', 'R80.20SP', 'R80.40']]
    if tags.get('os.version', '').lower() not in versions:
        return False
    if tags.get('role-firewall') != 'true':
        return False
    return True


def collect_logging_communication_status(raw_data: str, **kwargs):
    log_server = kwargs.get('log_server_item', '')
    log_servers_status = {'log_established': [], 'sic_established': []}
    lines = raw_data.split('\n')
    for line in lines:
        log_state_regex = re.compile(r'{}:257\s+(\S+)'.format(log_server))
        log_state_regex_result = log_state_regex.search(line)
        log_state = log_state_regex_result and log_state_regex_result.group(1)
        if log_state == 'ESTABLISHED':
            log_servers_status['log_established'].append(log_server)

        sic_state_regex = re.compile(r':18192\s+{}.\d+\s+(\S+)'.format(log_server))
        sic_state_regex_result = sic_state_regex.search(line)
        sic_state = sic_state_regex_result and sic_state_regex_result.group(1)
        if sic_state == 'ESTABLISHED':
            log_servers_status['sic_established'].append(log_server)
    return log_servers_status


def collect_log_servers_configured(raw_data: str):
    servers = []
    lines = raw_data.split('\n')
    log_section_found = False
    for line in lines:
        if line == '[Log]':
            log_section_found = True
            continue
        elif line.startswith('['):
            log_section_found = False
            continue
        elif log_section_found and line.strip():
            servers.append(line)
    return servers


def collect_fwd_process_status(raw_data: str):
    lines = raw_data.split('\n')
    values = lines[1].split()
    return {'cpu_usage': float(values[0]), 'memory_usage': float(values[1])}


def get_server_to_investigate(**kwargs):
    disconnected_log_server = kwargs.get('log_server_item', '')
    return disconnected_log_server


def collect_routing_interface(raw_data: str, **kwargs):
    server_ip = kwargs.get('server_ip', '')
    regex = re.compile(r'{}\s.*dev\s+(\S+)'.format(server_ip))
    interface = regex.search(raw_data)
    route = interface and interface.group(1).strip()
    if route:
        return route
    return None


def collect_connection_status(raw_data: str):
    regex = re.compile(r'\s+(\d+)\s+received')
    result = regex.search(raw_data)
    packets_received = result and result.group(1).strip()
    if packets_received and packets_received.isdigit():
        return int(packets_received)
    return 0


def collect_interface_admin_state(raw_data: str, **kwargs):
    routing_interface = kwargs.get('routing_interface', '')
    regex = re.compile(r'{}.*,UP'.format(routing_interface))
    admin_is_enabled = bool(regex.search(raw_data))
    regex = re.compile(r'{}.*,UP.*DOWN'.format(routing_interface))
    admin_is_down = bool(regex.search(raw_data))
    return {'admin_is_enabled': admin_is_enabled, 'admin_is_down': admin_is_down}


def __process_errors(raw_data: str) -> dict:
    counter = 0
    lines = raw_data.split('\n')
    for line in lines:
        if ':' in line:
            values = line.split(':')
            counter = counter + int(values[1])
    return counter


def collect_interface_errors(raw_data: str):
    return __process_errors(raw_data)

