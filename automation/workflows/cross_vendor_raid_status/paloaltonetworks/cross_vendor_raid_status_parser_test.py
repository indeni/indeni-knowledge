import unittest
import automation.workflows.cross_vendor_raid_status.paloaltonetworks.cross_vendor_raid_status_mock_data as mock
import automation.workflows.cross_vendor_raid_status.paloaltonetworks.cross_vendor_raid_status_parser as parser


class CrossVendorRAIDStatusTests(unittest.TestCase):

    def test_parse_raid_status_1_disk_fails(self):
        pair1 = mock.SHOW_SYSTEM_RAID_PAIR_1
        pair2 = mock.SHOW_SYSTEM_RAID_PAIR_2
        pair3 = mock.SHOW_SYSTEM_RAID_PAIR_3
        self.assertEqual(parser.parse_raid_status(pair1[0]), pair1[1])
        self.assertEqual(parser.parse_raid_status(pair2[0]), pair2[1])
        self.assertEqual(parser.parse_raid_status(pair3[0]), pair3[1])

    def test_parse_serial_number(self):
        pair1 = mock.SHOW_SYSTEM_INFO_MATCH_SERIAL_PAIR_1
        self.assertEqual(parser.parse_serial_number(pair1[0]), pair1[1])

    def test_parse_current_date(self):
        pair1 = mock.SHOW_CLOCK_PAIR_1
        pair2 = mock.SHOW_CLOCK_PAIR_2
        self.assertEqual(parser.parse_current_date(pair1[0]), pair1[1])
        self.assertEqual(parser.parse_current_date(pair2[0]), pair2[1])

    def test_check_support_date(self):
        pair1 = mock.REQUEST_SUPPORT_INFO_PAIR_1
        pair2 = mock.REQUEST_SUPPORT_INFO_PAIR_2
        self.assertEqual(parser.check_support_date(pair1[0][0], pair1[0][1]), pair1[1])
        self.assertEqual(parser.check_support_date(pair2[0][0], pair2[0][1]), pair2[1])


if __name__ == '__main__':
    unittest.main()
