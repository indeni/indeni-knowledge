SHOW_SYSTEM_RAID_OUT_1 = """
<response status="success"><result>
  <drive_status>
    <disk_id_1>Present</disk_id_1>
    <disk_id_2>Failed</disk_id_2>
  </drive_status>
  <overall_raid_status>degraded</overall_raid_status>
</result></response>"""

SHOW_SYSTEM_RAID_OUT_2 = """
<response status="success"><result>
  <drive_status>
    <disk_id_1>Present</disk_id_1>
    <disk_id_2>Present</disk_id_2>
  </drive_status>
  <overall_raid_status>Good</overall_raid_status>
</result></response>"""

SHOW_SYSTEM_RAID_OUT_3 = """
<response status="success"><result>
  <drive_status>
    <disk_id_1>Present</disk_id_1>
    <disk_id_2>Missing</disk_id_2>
  </drive_status>
  <overall_raid_status>Good</overall_raid_status>
</result></response>"""

SHOW_SYSTEM_RAID_PAIR_1 = (SHOW_SYSTEM_RAID_OUT_1, {'failed_drives': ['disk_id_2'], 'status': False})

SHOW_SYSTEM_RAID_PAIR_2 = (SHOW_SYSTEM_RAID_OUT_2, {'failed_drives': [], 'status': True})

SHOW_SYSTEM_RAID_PAIR_3 = (SHOW_SYSTEM_RAID_OUT_3, {'failed_drives': [], 'status': True})

SHOW_SYSTEM_INFO_MATCH_SERIAL_OUT_1 ="""<response status="success">
    <result>
        <system>
            <hostname>PA-5060</hostname>
            <ip-address>172.16.20.81</ip-address>
            <public-ip-address>unknown</public-ip-address>
            <netmask>255.255.255.0</netmask>
            <default-gateway>172.16.20.254</default-gateway>
            <is-dhcp>no</is-dhcp>
            <ipv6-address>unknown</ipv6-address>
            <ipv6-link-local-address>fe80::290:bff:fe2c:71a0/64</ipv6-link-local-address>
            <ipv6-default-gateway/>
            <mac-address>00:90:0b:2c:71:a0</mac-address>
            <time>Thu Jul 2 11:23:32 2020</time>
            <uptime>30 days, 21:01:55</uptime>
            <devicename>PA-5060</devicename>
            <family>5000</family>
            <model/>
            <serial>0008C101207</serial>
            <cloud-mode>non-cloud</cloud-mode>
            <sw-version>8.1.0</sw-version>
            <global-protect-client-package-version>4.1.9</global-protect-client-package-version>
            <app-version>8173-5563</app-version>
            <app-release-date>2019/07/18 12:39:45 PDT</app-release-date>
            <av-version>2932-3442</av-version>
            <av-release-date>2019/03/29 04:00:17 PDT</av-release-date>
            <threat-version>0</threat-version>
            <threat-release-date>2019/07/18 12:39:45 PDT</threat-release-date>
            <wf-private-version>0</wf-private-version>
            <wf-private-release-date>unknown</wf-private-release-date>
            <url-db>paloaltonetworks</url-db>
            <wildfire-version>321218-323890</wildfire-version>
            <wildfire-release-date>2019/02/07 17:29:59 PST</wildfire-release-date>
            <url-filtering-version>0000.00.00.000</url-filtering-version>
            <global-protect-datafile-version>unknown</global-protect-datafile-version>
            <global-protect-datafile-release-date>unknown</global-protect-datafile-release-date>
            <global-protect-clientless-vpn-version>74-141</global-protect-clientless-vpn-version>
            <global-protect-clientless-vpn-release-date>2018/12/12 15:23:18 PST
            </global-protect-clientless-vpn-release-date>
            <logdb-version>8.1.8</logdb-version>
            <platform-family/>
            <vpn-disable-mode>off</vpn-disable-mode>
            <multi-vsys>on</multi-vsys>
            <operational-mode>normal</operational-mode>
        </system>
    </result>
</response>"""

SHOW_SYSTEM_INFO_MATCH_SERIAL_PAIR_1 = (SHOW_SYSTEM_INFO_MATCH_SERIAL_OUT_1, "0008C101207")

SHOW_CLOCK_OUT_1 = """<response status="success">
    <result>Thu Jul 2 11:36:16 PDT 2020 </result>
</response>"""

SHOW_CLOCK_OUT_2 = """<response status="success">
    <result>Thu Jan 23 02:00:04 PST 2000 </result>
</response>"""

SHOW_CLOCK_PAIR_1 = (SHOW_CLOCK_OUT_1, "Jul 02 2020")

SHOW_CLOCK_PAIR_2 = (SHOW_CLOCK_OUT_2, "Jan 23 2000")

REQUEST_SUPPORT_INFO_OUT_1 = """<response status="success">
    <result>
        <SupportInfoResponse>
            <Links>
                <Link>
                    <Title>Contact Us</Title>
                    <Url>https://www.paloaltonetworks.com/company/contact-us.html</Url>
                </Link>
                <Link>
                    <Title>Support Home</Title>
                    <Url>https://www.paloaltonetworks.com/support/tabs/overview.html</Url>
                </Link>
            </Links>
            <Support>
                <Contact>
                    <Contact>Click the contact link at right.</Contact>
                </Contact>
                <ExpiryDate>June 20, 2020</ExpiryDate>
                <SupportLevel>Standard</SupportLevel>
                <SupportDescription>10 x 5 phone support; repair and replace hardware service</SupportDescription>
            </Support>
        </SupportInfoResponse>
    </result>
</response>"""

REQUEST_SUPPORT_INFO_PAIR_1 = ((REQUEST_SUPPORT_INFO_OUT_1, "Dec 02 2020"), False)

REQUEST_SUPPORT_INFO_PAIR_2 = ((REQUEST_SUPPORT_INFO_OUT_1, "Jan 23 2000"), True)