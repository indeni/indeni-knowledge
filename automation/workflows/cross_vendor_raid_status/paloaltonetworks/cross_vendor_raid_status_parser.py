from datetime import datetime
from parser_service.public import helper_methods


def check_variant_matches(**kwargs) -> bool:
    # This should work on all devices
    tags = kwargs.get('my_device_tags', None)
    if tags is None:
        return False
    return True


def parse_raid_status(raw_data: str) -> dict:
    if raw_data:
        xml_data = helper_methods.parse_data_as_xml(raw_data)
        failed_drives = []
        status_result = True

        if xml_data and xml_data['response']['@status'] == 'success':
            response = xml_data['response']['result']
            drive_status = response.get('drive_status')
            if drive_status:
                for disk_id, status in drive_status.items():
                    if status == 'Failed':
                        failed_drives.append(disk_id)
                        status_result = False
            dic = {'status': status_result, 'failed_drives': failed_drives}
            return dic


def parse_serial_number(raw_data: str) -> str:
    if raw_data:
        xml_data = helper_methods.parse_data_as_xml(raw_data)
        if xml_data and xml_data['response']['@status'] == 'success':
            response = xml_data['response']['result']
            system = response.get('system')
            if system:
                serial = system.get('serial')
                if serial:
                    return serial


def parse_current_date(raw_data: str) -> str:
    if raw_data:
        xml_data = helper_methods.parse_data_as_xml(raw_data)
        if xml_data and xml_data['response']['@status'] == 'success':
            response = xml_data['response']['result']
            split_date = response.split(' ')
            if len(split_date[2]) == 1:
                padded_day = '0' + split_date[2]
            else:
                padded_day = split_date[2]

            return split_date[1] + ' ' + padded_day + ' ' + split_date[5]


def check_support_date(raw_data: str, current_date: str) -> bool:
    if raw_data:
        xml_data = helper_methods.parse_data_as_xml(raw_data)
        if xml_data and xml_data['response']['@status'] == 'success':
            response = xml_data['response']['result']
            support_info_response = response.get('SupportInfoResponse')
            if support_info_response:
                support = support_info_response.get('Support')
                if support:
                    expiry_date = support.get('ExpiryDate')
                    support_datetime = datetime.strptime(expiry_date, '%B %d, %Y')
                    current_datetime = datetime.strptime(current_date, '%b %d %Y')
                    return support_datetime > current_datetime
