#SSH command: show routing summary | match total
SHOW_ROUTING_SUMMARY_OUT_1 = """
<response status="success">
<result>
<entry>
<All-Routes>
<total>14</total>
<limit>5000</limit>
<active>14</active>
</All-Routes>
<All-IPv4>
<total>14</total>
<limit>5000</limit>
</All-IPv4>
<All-IPv6>
<total>0</total>
<limit>5000</limit>
</All-IPv6>
<Static-Routes>
<total>6</total>
</Static-Routes>
<Connect-Routes>
<total>8</total>
</Connect-Routes>
<BGP-Routes>
<total>0</total>
</BGP-Routes>
<OSPF-Routes>
<total>0</total>
</OSPF-Routes>
<RIP-Routes>
<total>0</total>
</RIP-Routes>
</entry>
<entry name="default"/>
<entry name="for bgp">
<bgp>
<peer-group-count>1</peer-group-count>
<peer-count>1</peer-count>
<local-rib-prefix-count>1</local-rib-prefix-count>
<mp-bgp-enable>yes</mp-bgp-enable>
<afi-safi-ipv4-unicast>yes</afi-safi-ipv4-unicast>
</bgp>
</entry>
</result>
</response>
"""

PANOS_PARSE_SHOW_ROUTING_SUMMARY_PAIR_1 = (SHOW_ROUTING_SUMMARY_OUT_1,
{'All-Routes_Now': '14',
'All-Routes_Limit': '5000',
'All-IPv4_Now': '14',
'All-IPv4_Limit': '5000',
'All-IPv6_Now': '0',
'All-IPv6_Limit': '5000',
'Static-Routes_Now': '6',
'Connect-Routes_Now': '8',
'BGP-Routes_Now': '0',
'OSPF-Routes_Now': '0',
'RIP-Routes_Now': '0'}
)
