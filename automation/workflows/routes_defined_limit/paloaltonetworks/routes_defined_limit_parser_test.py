import unittest
import automation.workflows.routes_defined_limit.paloaltonetworks.routes_defined_limit_mock_data as mock
import automation.workflows.routes_defined_limit.paloaltonetworks.routes_defined_limit_parser as parser


class RoutesDefinedLimitParserTests(unittest.TestCase):


    def test_panos_parse_show_routing_summary(self):
        pair1 = mock.PANOS_PARSE_SHOW_ROUTING_SUMMARY_PAIR_1
        self.assertEqual(pair1[1], parser.panos_parse_show_routing_summary(pair1[0]))

if __name__ == '__main__':
    unittest.main()