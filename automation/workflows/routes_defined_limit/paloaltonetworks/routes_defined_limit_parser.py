from parser_service.public.helper_methods import *


def check_variant_matches(**kwargs) -> bool:
    # This should work on all gateways
    tags = kwargs.get('my_device_tags', None)
    if tags is None:
        return False
    if tags.get('product') != 'firewall':
        return False
    return True


def panos_parse_show_routing_summary(raw_data: str) -> list:
    raw_data_parsed = parse_data_as_xml(raw_data)
    routing_summary = {}
    if raw_data_parsed['response']['@status'] == 'success':
        for route_type in raw_data_parsed['response']['result']['entry'][0].keys():
            routing_summary['{}{}'.format(route_type, '_Now')] = raw_data_parsed['response']['result']['entry'][0][route_type]['total']
            if raw_data_parsed['response']['result']['entry'][0][route_type].get('limit'):
                routing_summary['{}{}'.format(route_type, '_Limit')] = raw_data_parsed['response']['result']['entry'][0][route_type]['limit']
        return routing_summary
