import automation.workflows.cross_vendor_network_interface_tx_rx_util_percentage.checkpoint.cross_vendor_network_interface_tx_rx_util_percentage_mock_data as mock
import automation.workflows.cross_vendor_network_interface_tx_rx_util_percentage.checkpoint.cross_vendor_network_interface_tx_rx_util_percentage_parser as parser
import unittest



class CcrossVendorNetworkInterfaceTxUtilPercentage(unittest.TestCase):

    def test_strip_interface_name(self):
        pair1 = mock.INTERFACE_NAME_PAIR_1
        self.assertEqual(pair1[1], parser.strip_interface_name(pair1[0]))
        pair2 = mock.INTERFACE_NAME_PAIR_2
        self.assertEqual(pair2[1], parser.strip_interface_name(pair2[0]))
        pair3 = mock.INTERFACE_NAME_PAIR_3
        self.assertEqual(pair3[1], parser.strip_interface_name(pair3[0]))

    def test_parse_mpstat_p_all(self):
        pair1 = mock.MPSTAT_P_ALL_PAIR1
        self.assertEqual(pair1[1], parser.parse_mpstat_p_all(pair1[0]))

    def test_get_tcpdump_interfaces(self):
        pair1 = mock.TCPDUMP_D_PAIR1
        self.assertEqual(pair1[1], parser.get_tcpdump_interfaces(pair1[0]))

    def test_get_cpmonitor_top_talkers(self):
        pair1 = mock.CPMONITOR_PAIR1
        self.assertListEqual(pair1[1], parser.get_cpmonitor_top_talkers(pair1[0]))

if __name__ == '__main__':
    unittest.main()