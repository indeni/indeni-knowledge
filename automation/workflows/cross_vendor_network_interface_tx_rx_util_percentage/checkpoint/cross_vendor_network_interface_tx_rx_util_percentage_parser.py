from parser_service.public import helper_methods
import yaml
import pathlib

def check_variant_matches(**kwargs) -> tuple:
    # This should work on all devices with one of the below versions
    tags = kwargs.get('my_device_tags', None)
    current_path = pathlib.Path(__file__).parent.absolute()
    ate_name = pathlib.Path(__file__).name.replace('_parser.py','')
    full_template_path = '{}/{}_workflow_requirements.yaml'.format(current_path, ate_name)
    tags_check = False
    try:
        with open(full_template_path, 'r') as requirements_file:
            try:
                data_loaded = yaml.safe_load(requirements_file)
                if 'ate_disabled'  in data_loaded.keys():
                    disabled_conclusion = "This workflow is currently disabled by configuration due the risk could imply."
                    disabled_remediation = "Contact Indeni if you consider relevant  to enable this workflow in your environment."
                    if str(data_loaded['ate_disabled']).lower() == 'true':
                        return (False, [disabled_conclusion, disabled_remediation ])
                if 'requirements' in data_loaded.keys():
                    requirements_conclusion = "This workflow is incompatible with the device."
                    requirements_remediation = "Contact Indeni and request that compatibility be added."
                    for key,value in data_loaded['requirements'].items():
                        device_tag = tags.get(key)
                        if value is not None:
                            if type(value) in [bool, str] :
                                if str(value).lower() == str(device_tag).lower():
                                    tags_check = True
                                elif (str(value).lower() == 'false') and (device_tag is None):
                                    tags_check = True
                                else:
                                    return (False, [requirements_conclusion, requirements_remediation ])
                            else:
                                if 'except' in value.keys():
                                    if device_tag in value['except'] or 'all' in value['except'] :
                                        return (False, [requirements_conclusion, requirements_remediation ])
                                if 'allow' in value.keys():
                                    if device_tag in value['allow'] or 'all' in value['allow'] :
                                        tags_check = True
                                    else:
                                        return (False, [requirements_conclusion, requirements_remediation ])
                                if 'gt' in value.keys():
                                    if float(device_tag) < float(value['gt']):
                                        return (False, [requirements_conclusion, requirements_remediation ])
                                    else:
                                        tags_check = True
                                if 'lt' in value.keys():
                                    if float(device_tag) > float(value['lt']):
                                        return (False, [requirements_conclusion, requirements_remediation ])
                                    else:
                                        tags_check = True
            except:
                return (False, [requirements_conclusion, requirements_remediation ])
    except FileNotFoundError:
        if tags is None:
            return (False, [requirements_conclusion, requirements_remediation ])
        return (True, ["",""])
    if tags is None:
        return (False, [requirements_conclusion, requirements_remediation ])
    return (tags_check, ["",""])

def strip_interface_name(interface: str) -> dict:
    my_interface = {}
    if interface.count('_') == 2 :
        my_interface['vsx'] = 'true'
        vs_data = interface.split('_')[0]
        my_interface['vs_id'] = vs_data.split(':')[1]
        my_interface['name'] = interface.split('_')[1]
    else:
        my_interface['vsx'] = 'false'
        my_interface['vs_id'] = "0"
        my_interface['name'] = interface.split('_')[0]
    return my_interface

def parse_mpstat_p_all(raw_data: str) -> int:

    if raw_data:
        data = helper_methods.parse_data_as_list(raw_data, 'checkpoint_mpstat.textfsm')
        if data:
            output = [idx for idx, element in enumerate(data) if element['cpuid'] == 'all']
            return round( 100 - float(data[output[0]]['idle'] ))

def get_tcpdump_interfaces(raw_data: str) -> list:
    if raw_data:
        data = helper_methods.parse_data_as_list(raw_data, 'checkpoint_tcpdump_d.textfsm')
        if data:
            return data[0]['interface']

def searchKey(e):
    return e['throughput_percentage']

def get_cpmonitor_top_talkers(raw_data: str) -> list:
    if raw_data:
        data = helper_methods.parse_data_as_list(raw_data, 'checkpoint_cpmonitor_file.textfsm')
        if data:
            data.sort(reverse=True, key=searchKey)
            data_returned = []
            for i in range (0, 5):
                talker_item = []
                talker_item = [data[i]['throughput_percentage'], data[i]['src_ip'],data[i]['dst_ip'],data[i]['proto'],data[i]['dst_port']]
                data_returned.append(talker_item)
            return data_returned