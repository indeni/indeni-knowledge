import re
from parser_service.public.helper_methods import *


def check_variant_matches(**kwargs) -> bool:
    # This should work on all gateways
    tags = kwargs.get('my_device_tags', None)
    if tags is None:
        return False
    if tags.get('product') != 'firewall':
        return False
    return True


def get_arp_usage_percent(raw_data: str) -> int:
    data = parse_data_as_xml(raw_data)
    if data:
        max_entries = data['response']['result']['max']
        total_entries = data['response']['result']['total']
        return (int(total_entries) / int(max_entries)) * 100
    return 0


def get_session_info(raw_data: str) -> int:
    data = parse_data_as_xml(raw_data)
    if data:
        num_max = float(data['response']['result']['num-max'])
        num_active = float(data['response']['result']['num-active'])
        return round((float(num_active) / float(num_max) * 100), 2)
    return 0
