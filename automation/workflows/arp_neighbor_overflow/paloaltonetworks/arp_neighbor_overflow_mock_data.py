GET_ARP_USAGE_PERCENT_PAIR_1 = ("""
<response status="success">
   <result>
      <max>2500</max>
      <total>0</total>
      <timeout>1800</timeout>
      <dp>dp0</dp>
      <entries />
   </result>
</response>
""", 0)

GET_ARP_USAGE_PERCENT_PAIR_2 = ("""
<response status="success">
   <result>
      <max>2500</max>
      <total>1</total>
      <timeout>1800</timeout>
      <dp>dp0</dp>
      <entries>
         <entry>
            <status>c</status>
            <ip>172.30.250.2</ip>
            <mac>00:50:56:ac:2a:6b</mac>
            <ttl>1665</ttl>
            <interface>ethernet1/1</interface>
            <port>ethernet1/1</port>
         </entry>
      </entries>
   </result>
</response>
""", 0.04)

GET_SESSION_INFO_PAIR_1 = ("""
<response status="success">
   <result>
      <tmo-sctpshutdown>60</tmo-sctpshutdown>
      <tcp-nonsyn-rej>True</tcp-nonsyn-rej>
      <tmo-tcpinit>5</tmo-tcpinit>
      <tmo-tcp>3600</tmo-tcp>
      <pps>1124</pps>
      <tmo-tcp-delayed-ack>250</tmo-tcp-delayed-ack>
      <num-max>256000</num-max>
      <age-scan-thresh>80</age-scan-thresh>
      <tmo-tcphalfclosed>120</tmo-tcphalfclosed>
      <num-active>283</num-active>
      <tmo-sctp>3600</tmo-sctp>
      <dis-def>60</dis-def>
      <num-mcast>0</num-mcast>
      <icmp-unreachable-rate>200</icmp-unreachable-rate>
      <tmo-tcptimewait>15</tmo-tcptimewait>
      <age-scan-ssf>8</age-scan-ssf>
      <tmo-udp>30</tmo-udp>
      <vardata-rate>10485760</vardata-rate>
      <age-scan-tmo>10</age-scan-tmo>
      <dis-sctp>30</dis-sctp>
      <dp>*.dp0</dp>
      <dis-tcp>90</dis-tcp>
      <tcp-reject-siw-thresh>4</tcp-reject-siw-thresh>
      <num-udp>58</num-udp>
      <tmo-sctpcookie>60</tmo-sctpcookie>
      <tmo-icmp>6</tmo-icmp>
      <max-pending-mcast>0</max-pending-mcast>
      <age-accel-thresh>80</age-accel-thresh>
      <num-gtpc>0</num-gtpc>
      <oor-action>drop</oor-action>
      <tmo-def>30</tmo-def>
      <num-predict>0</num-predict>
      <age-accel-en>True</age-accel-en>
      <age-accel-tsf>2</age-accel-tsf>
      <hw-offload>True</hw-offload>
      <num-icmp>23</num-icmp>
      <num-gtpu-active>0</num-gtpu-active>
      <tmo-cp>30</tmo-cp>
      <tcp-strict-rst>True</tcp-strict-rst>
      <tmo-sctpinit>5</tmo-sctpinit>
      <strict-checksum>True</strict-checksum>
      <tmo-tcp-unverif-rst>30</tmo-tcp-unverif-rst>
      <num-bcast>0</num-bcast>
      <ipv6-fw>True</ipv6-fw>
      <cps>8</cps>
      <num-installed>13080902</num-installed>
      <num-tcp>196</num-tcp>
      <dis-udp>60</dis-udp>
      <num-sctp-assoc>0</num-sctp-assoc>
      <num-sctp-sess>0</num-sctp-sess>
      <tcp-reject-siw-enable>False</tcp-reject-siw-enable>
      <tmo-tcphandshake>10</tmo-tcphandshake>
      <hw-udp-offload>True</hw-udp-offload>
      <kbps>2551</kbps>
      <num-gtpu-pending>0</num-gtpu-pending>
   </result>
</response>
""", 0.11)

GET_SESSION_INFO_PAIR_2 = ("""
<response status="success">
   <result>
      <tmo-sctpshutdown>60</tmo-sctpshutdown>
      <tcp-nonsyn-rej>True</tcp-nonsyn-rej>
      <tmo-tcpinit>5</tmo-tcpinit>
      <tmo-tcp>3600</tmo-tcp>
      <pps>1124</pps>
      <tmo-tcp-delayed-ack>250</tmo-tcp-delayed-ack>
      <num-max>256000</num-max>
      <age-scan-thresh>80</age-scan-thresh>
      <tmo-tcphalfclosed>120</tmo-tcphalfclosed>
      <num-active>250000</num-active>
      <tmo-sctp>3600</tmo-sctp>
      <dis-def>60</dis-def>
      <num-mcast>0</num-mcast>
      <icmp-unreachable-rate>200</icmp-unreachable-rate>
      <tmo-tcptimewait>15</tmo-tcptimewait>
      <age-scan-ssf>8</age-scan-ssf>
      <tmo-udp>30</tmo-udp>
      <vardata-rate>10485760</vardata-rate>
      <age-scan-tmo>10</age-scan-tmo>
      <dis-sctp>30</dis-sctp>
      <dp>*.dp0</dp>
      <dis-tcp>90</dis-tcp>
      <tcp-reject-siw-thresh>4</tcp-reject-siw-thresh>
      <num-udp>58</num-udp>
      <tmo-sctpcookie>60</tmo-sctpcookie>
      <tmo-icmp>6</tmo-icmp>
      <max-pending-mcast>0</max-pending-mcast>
      <age-accel-thresh>80</age-accel-thresh>
      <num-gtpc>0</num-gtpc>
      <oor-action>drop</oor-action>
      <tmo-def>30</tmo-def>
      <num-predict>0</num-predict>
      <age-accel-en>True</age-accel-en>
      <age-accel-tsf>2</age-accel-tsf>
      <hw-offload>True</hw-offload>
      <num-icmp>23</num-icmp>
      <num-gtpu-active>0</num-gtpu-active>
      <tmo-cp>30</tmo-cp>
      <tcp-strict-rst>True</tcp-strict-rst>
      <tmo-sctpinit>5</tmo-sctpinit>
      <strict-checksum>True</strict-checksum>
      <tmo-tcp-unverif-rst>30</tmo-tcp-unverif-rst>
      <num-bcast>0</num-bcast>
      <ipv6-fw>True</ipv6-fw>
      <cps>8</cps>
      <num-installed>13080902</num-installed>
      <num-tcp>196</num-tcp>
      <dis-udp>60</dis-udp>
      <num-sctp-assoc>0</num-sctp-assoc>
      <num-sctp-sess>0</num-sctp-sess>
      <tcp-reject-siw-enable>False</tcp-reject-siw-enable>
      <tmo-tcphandshake>10</tmo-tcphandshake>
      <hw-udp-offload>True</hw-udp-offload>
      <kbps>2551</kbps>
      <num-gtpu-pending>0</num-gtpu-pending>
   </result>
</response>
""", 97.66)
