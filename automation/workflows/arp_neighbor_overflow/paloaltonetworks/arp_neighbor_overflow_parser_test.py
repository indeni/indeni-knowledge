import unittest
import automation.workflows.arp_neighbor_overflow.paloaltonetworks.arp_neighbor_overflow_parser as parser
import automation.workflows.arp_neighbor_overflow.paloaltonetworks.arp_neighbor_overflow_mock_data as mock


class HighARPCacheUsageTests(unittest.TestCase):


    def test_get_arp_usage_percent(self):
        pair1 = mock.GET_ARP_USAGE_PERCENT_PAIR_1
        pair2 = mock.GET_ARP_USAGE_PERCENT_PAIR_2
        self.assertEqual(pair1[1], parser.get_arp_usage_percent(pair1[0]))
        self.assertEqual(pair2[1], parser.get_arp_usage_percent(pair2[0]))

    def test_get_session_info(self):
        pair1 = mock.GET_SESSION_INFO_PAIR_1
        pair2 = mock.GET_SESSION_INFO_PAIR_2
        self.assertEqual(pair1[1], parser.get_session_info(pair1[0]))
        self.assertEqual(pair2[1], parser.get_session_info(pair2[0]))


if __name__ == '__main__':
    unittest.main()
