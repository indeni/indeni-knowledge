PARSE_GET_ARP_ENTRIES_PER_INTERFACE_PAIR_1 = ("""
? (10.11.94.46) at 00:50:56:AC:8B:DF [ether] on eth0
? (10.11.94.182) at 00:50:56:91:2C:42 [ether] on eth0
? (3.3.3.1) at 00:50:56:91:EA:2F [ether] on eth1
? (10.11.94.73) at 00:50:56:AC:04:50 [ether] on eth0
""", {'eth0': {'00:50:56:AC:8B:DF': '10.11.94.46', '00:50:56:91:2C:42': '10.11.94.182', '00:50:56:AC:04:50': '10.11.94.73'}, 'eth1': {'00:50:56:91:EA:2F': '3.3.3.1'}}
)

PARSE_GET_NUMBER_ARP_ENTRIES_PAIR_1 = ("""
12
""",12)

PARSE_GET_ARP_SIZE_PAIR_1 = ("""
4096
""",4096)

PARSE_GET_FULL_ARP_CONFIGURATION_PAIR_1 = ("""
set arp table cache-size 4096
set arp table validity-timeout 60
set arp announce 2
""", False)

PARSE_GET_FULL_ARP_CONFIGURATION_PAIR_2 = ("""
set arp table cache-size 6000
set arp table validity-timeout 20
set arp announce 2
""", False)

PARSE_GET_FULL_ARP_CONFIGURATION_PAIR_3 = ("""
set arp table cache-size 1024
set arp table validity-timeout 120
set arp announce 2
""", True)

PARSE_GET_MAX_IPS_PER_INTERFACE_PAIR_1 = ("""
bond1       Link encap:Ethernet  HWaddr 00:50:56:91:29:FC  
            UP BROADCAST RUNNING MASTER MULTICAST  MTU:1500  Metric:1
            RX packets:893606996 errors:0 dropped:0 overruns:0 frame:0
            TX packets:245497109 errors:0 dropped:0 overruns:0 carrier:0
            collisions:0 txqueuelen:0 
            RX bytes:102729861050 (95.6 GiB)  TX bytes:13267043412 (12.3 GiB)

bond1.33    Link encap:Ethernet  HWaddr 00:50:56:91:29:FC  
            inet addr:33.33.33.4  Bcast:33.33.33.255  Mask:255.255.255.0
            UP BROADCAST RUNNING MASTER MULTICAST  MTU:1500  Metric:1
            RX packets:0 errors:0 dropped:0 overruns:0 frame:0
            TX packets:245057254 errors:0 dropped:0 overruns:0 carrier:0
            collisions:0 txqueuelen:0 
            RX bytes:0 (0.0 b)  TX bytes:13221699102 (12.3 GiB)

bond1.33:1  Link encap:Ethernet  HWaddr 00:50:56:91:29:FC  
            inet addr:44.44.44.4  Bcast:44.44.44.255  Mask:255.255.255.0
            UP BROADCAST RUNNING MASTER MULTICAST  MTU:1500  Metric:1

eth0        Link encap:Ethernet  HWaddr 00:50:56:91:D9:C0  
            inet addr:10.11.94.174  Bcast:10.11.94.255  Mask:255.255.255.0
            UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
            RX packets:4432736960 errors:0 dropped:0 overruns:0 frame:0
            TX packets:553127964 errors:0 dropped:0 overruns:0 carrier:0
            collisions:0 txqueuelen:1000 
            RX bytes:1107044580745 (1.0 TiB)  TX bytes:131600927508 (122.5 GiB)
            Interrupt:67 Memory:fd5a0000-fd5c0000

eth1        Link encap:Ethernet  HWaddr 00:50:56:91:07:09  
            inet addr:3.3.3.2  Bcast:3.3.3.3  Mask:255.255.255.252
            UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
            RX packets:793070226 errors:0 dropped:0 overruns:0 frame:0
            TX packets:292669582 errors:0 dropped:0 overruns:0 carrier:0
            collisions:0 txqueuelen:1000 
            RX bytes:87016036934 (81.0 GiB)  TX bytes:31954853238 (29.7 GiB)
            Interrupt:91 Memory:fd4a0000-fd4c0000 

eth2        Link encap:Ethernet  HWaddr 00:50:56:91:37:69  
            inet addr:22.22.22.4  Bcast:22.22.22.255  Mask:255.255.255.0
            UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
            RX packets:734018210 errors:0 dropped:0 overruns:0 frame:0
            TX packets:30075267 errors:0 dropped:0 overruns:0 carrier:0
            collisions:0 txqueuelen:1000 
            RX bytes:87395263166 (81.3 GiB)  TX bytes:2627367952 (2.4 GiB)
            Interrupt:107 Memory:fd3a0000-fd3c0000 

eth3        Link encap:Ethernet  HWaddr 00:50:56:91:29:FC  
            UP BROADCAST RUNNING SLAVE MULTICAST  MTU:1500  Metric:1
            RX packets:446830837 errors:0 dropped:0 overruns:0 frame:0
            TX packets:122753014 errors:0 dropped:0 overruns:0 carrier:0
            collisions:0 txqueuelen:1000 
            RX bytes:51366429118 (47.8 GiB)  TX bytes:6633676112 (6.1 GiB)
            Interrupt:59 Memory:fd2a0000-fd2c0000 

eth4        Link encap:Ethernet  HWaddr 00:50:56:91:29:FC  
            UP BROADCAST RUNNING SLAVE MULTICAST  MTU:1500  Metric:1
            RX packets:446776159 errors:0 dropped:0 overruns:0 frame:0
            TX packets:122744095 errors:0 dropped:0 overruns:0 carrier:0
            collisions:0 txqueuelen:1000 
            RX bytes:51363431932 (47.8 GiB)  TX bytes:6633367300 (6.1 GiB)
            Interrupt:67 Memory:fd1a0000-fd1c0000 

lo          Link encap:Local Loopback  
            inet addr:127.0.0.1  Mask:255.0.0.0
            UP LOOPBACK RUNNING  MTU:16436  Metric:1
            RX packets:349827758 errors:0 dropped:0 overruns:0 frame:0
            TX packets:349827758 errors:0 dropped:0 overruns:0 carrier:0
            collisions:0 txqueuelen:0 
            RX bytes:51684443869 (48.1 GiB)  TX bytes:51684443869 (48.1 GiB)
""", {'bond1.33': 256, 'bond1.33:1': 256, 'eth0': 256, 'eth1': 4, 'eth2': 256, 'eth4': 16777216})


PARSE_CHECK_SUBNETMASK_TOO_WIDE_PAIR_1 = (PARSE_GET_ARP_ENTRIES_PER_INTERFACE_PAIR_1[1], PARSE_GET_MAX_IPS_PER_INTERFACE_PAIR_1[1], False)

PARSE_GET_INTERFACE_AND_ENTRY_FOR_ARPING_PAIR_1 = (PARSE_GET_ARP_ENTRIES_PER_INTERFACE_PAIR_1[1], {'interface': 'eth0', 'ip_address': '10.11.94.46'})

PARSE_RUN_ARPING_PAIR_1 = ("""
ARPING 10.11.94.73 from 10.11.94.174 eth0
Unicast reply from 10.11.94.73 [00:50:56:AC:04:50]  64.727ms
Unicast reply from 10.11.94.73 [00:50:56:AC:04:50]  88.855ms
Unicast reply from 10.11.94.73 [00:50:56:AC:04:50]  0.951ms
Sent 3 probes (1 broadcast(s))
Received 3 response(s)
""", True)

PARSE_VALIDATE_JHF_PAIR_1 = ("""

This is Check Point CPinfo Build 914000190 for GAIA
[IDA]
	No hotfixes..

[MGMT]
	No hotfixes..

[CPFC]
	No hotfixes..

[FW1]
	No hotfixes..

FW1 build number:
This is Check Point's software version R80.30 - Build 095
kernel: R80.30 - Build 064

[SecurePlatform]
	No hotfixes..

[PPACK]
	No hotfixes..

[CPinfo]
	No hotfixes..

[DIAG]
	No hotfixes..

[CVPN]
	No hotfixes..


**  ************************************************************************* **
**                                 Hotfixes                                   **
**  ************************************************************************* **
Display name                                      Type                      
Check Point CPinfo build 202 for R80.30           Hotfix                    
R80.30 Gaia 3.10 Jumbo Hotfix Accumulator                                   
    General Availability  for Security Gateway    Hotfix                    
**  ************************************************************************* **
**                                   HFAs                                     **
**  ************************************************************************* **
Display name                                      Type                      
Check_Point_SmartConsole_R80_30_jumbo_HF_B62_W                              
   in                                             HFA             
""", True)


PARSE_VALIDATE_JHF_PAIR_2 = ("""This is Check Point CPinfo Build 914000182 for GAIA
[IDA] 
   No hotfixes..

[MGMT] 
   No hotfixes..

[CPFC] 
   No hotfixes..

[FW1] 
   No hotfixes..

FW1 build number:
This is Check Point's software version R80.20 - Build 255
kernel: R80.20 - Build 258

[SecurePlatform] 
   No hotfixes..

[CPinfo] 
   No hotfixes..

[DIAG] 
   No hotfixes..

[PPACK] 
   No hotfixes..

[CVPN] 
   No hotfixes..

[rtm] 
   No hotfixes..

**  ************************************************************************* **
**                                   HFAs                                     **
**  ************************************************************************* **
Display name                                      Type                      
Check Point CPinfo build 202 for R80.20           HFA                       
R80.20 Gaia 2.6.18 Jumbo Hotfix Accumulator                                 
    for Security Gateway and Standalone Genera    HFA                       
R80.20 Gaia 3.10 Jumbo Hotfix Accumulator for                               
    Security Management General Availability (    HFA                       
R80.20 Mail Transfer Agent (MTA) Update (Take                               
    57)                                           HFA                       
R80.20 SmartConsole Build 100                     HFA                       
""", True) 

PARSE_VALIDATE_JHF_PAIR_3 = ("""
This is Check Point CPinfo Build 914000190 for GAIA
[IDA]
	No hotfixes..

[MGMT]
	No hotfixes..

[CPFC]
	No hotfixes..

[FW1]
	No hotfixes..

FW1 build number:
This is Check Point's software version R80.30 - Build 484
kernel: R80.30 - Build 478

[SecurePlatform]
	No hotfixes..

[CPinfo]
	No hotfixes..

[DIAG]
	No hotfixes..

[PPACK]
	No hotfixes..

[CVPN]
	No hotfixes..


**             ************************************************************************* **
**                         Connection error. Packages list might be incomplete           **
**             ************************************************************************* **
Show packages: no packages to display
""", False) 