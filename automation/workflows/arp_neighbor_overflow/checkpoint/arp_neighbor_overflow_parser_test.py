import unittest
import automation.workflows.arp_neighbor_overflow.checkpoint.arp_neighbor_overflow_parser as parser
import automation.workflows.arp_neighbor_overflow.checkpoint.arp_neighbor_overflow_mock_data as mock


class HighARPCacheUsageTests(unittest.TestCase):


    def test_parse_get_number_arp_entries(self):
        data = mock.PARSE_GET_NUMBER_ARP_ENTRIES_PAIR_1
        self.assertEqual(parser.parse_get_number_arp_entries(data[0]), data[1])

    def test_parse_get_arp_size(self):
        data = mock.PARSE_GET_ARP_SIZE_PAIR_1
        self.assertEqual(parser.parse_get_arp_size(data[0]), data[1])

    def test_parse_get_full_arp_configuration(self):
        data = mock.PARSE_GET_FULL_ARP_CONFIGURATION_PAIR_1
        self.assertEqual(parser.parse_get_full_arp_configuration(data[0]), data[1])
        data = mock.PARSE_GET_FULL_ARP_CONFIGURATION_PAIR_2
        self.assertEqual(parser.parse_get_full_arp_configuration(data[0]), data[1])
        data = mock.PARSE_GET_FULL_ARP_CONFIGURATION_PAIR_3
        self.assertEqual(parser.parse_get_full_arp_configuration(data[0]), data[1])

    def test_parse_get_arp_entries_per_interface(self):
        data = mock.PARSE_GET_ARP_ENTRIES_PER_INTERFACE_PAIR_1
        self.assertEqual(parser.parse_get_arp_entries_per_interface(data[0]), data[1])

    def test_parse_get_max_ips_per_interface(self):
        data = mock.PARSE_GET_MAX_IPS_PER_INTERFACE_PAIR_1
        self.assertEqual(parser.parse_get_max_ips_per_interface(data[0]), data[1])

    def test_parse_check_subnetmask_too_wide(self):
        data = mock.PARSE_CHECK_SUBNETMASK_TOO_WIDE_PAIR_1
        self.assertEqual(parser.parse_check_subnetmask_too_wide(data[0], data[1]), data[2])

    def test_parse_get_interface_and_entry_for_arping(self):
        data = mock.PARSE_GET_INTERFACE_AND_ENTRY_FOR_ARPING_PAIR_1
        self.assertEqual(parser.parse_get_interface_and_entry_for_arping(data[0]), data[1])

    def test_parse_run_arping(self):
        data = mock.PARSE_RUN_ARPING_PAIR_1
        self.assertEqual(parser.parse_run_arping(data[0]), data[1])

    def test_parse_validate_jhf(self):
        data = mock.PARSE_VALIDATE_JHF_PAIR_1
        self.assertEqual(parser.parse_validate_jhf(data[0]), data[1])
        data = mock.PARSE_VALIDATE_JHF_PAIR_2
        self.assertEqual(parser.parse_validate_jhf(data[0]), data[1])
        data = mock.PARSE_VALIDATE_JHF_PAIR_3
        self.assertEqual(parser.parse_validate_jhf(data[0]), data[1])


if __name__ == '__main__':
    unittest.main()
