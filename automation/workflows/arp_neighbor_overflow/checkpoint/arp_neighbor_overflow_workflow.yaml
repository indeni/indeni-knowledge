id: high_arp_cache_usage_checkpoint
friendly_name: High ARP cache usage
start_block: get_device_tags

blocks:
  get_device_tags:
    type: device_tags
    name: Get device tags
    register_to: my_device_tags
    go_to: make_variant_matches

  make_variant_matches:
    type: logic
    name: Match device against variant
    args: [my_device_tags]
    method: check_variant_matches
    register_to: bool_variant_matches
    go_to: check_if_variant_matches

  check_if_variant_matches:
    type: if
    name: Check if device matches requirements
    condition: bool_variant_matches
    then_go_to: get_number_arp_entries
    else_go_to: variant_conclusion

  variant_conclusion:
    type: conclusion
    name: Incompatible workflow
    triage_conclusion: |
      This workflow is incompatible with the device.
    triage_remediation_steps: |
      Contact Indeni and request that compatibility be added.

  get_number_arp_entries:
    type: device_task
    name: Get number of ARP entries
    runner:
      type: SSH
      command: arp -an | wc -l
    parser:
      args: []
      method: parse_get_number_arp_entries
    register_to: number_arp_entries
    go_to: get_arp_size

  get_arp_size:
    type: device_task
    name: Get ARP table cache size
    runner:
      type: SSH
      command: dbget ip:arp:cache_size
    parser:
      args: []
      method: parse_get_arp_size
    register_to: arp_table_cache_size
    go_to: check_if_arp_usage_80_percent_or_more

  check_if_arp_usage_80_percent_or_more:
    type: if
    name: Check if ARP usage is higher than 80%
    condition: (number_arp_entries/arp_table_cache_size) * 100 >= 80.0
    then_go_to: get_full_arp_configuration
    else_go_to: conclusion_arp_utilization_below_mark

  conclusion_arp_utilization_below_mark:
    type: conclusion
    name: Current ARP utilization is below the high mark
    triage_conclusion: Current ARP utilization is below the high mark.
    triage_remediation_steps: Issue is no longer valid.

  get_full_arp_configuration:
    type: device_task
    name: Get ARP configuration and parameters
    runner:
      type: SSH
      command: clish -c "show configuration arp"
    parser:
      args: []
      method: parse_get_full_arp_configuration
    register_to: arp_paremeters_result
    go_to: check_arp_paremeters

  check_arp_paremeters:
    type: if
    name: Check if ARP table is too low and ARP table validity-timeout is too high
    condition: arp_paremeters_result
    then_go_to: conclusion_arp_configuration_change_needed
    else_go_to: get_arp_entries_per_interface

  conclusion_arp_configuration_change_needed:
    type: conclusion
    name: Change of ARP configuration is needed
    triage_conclusion: ARP table too low for the environment or very dynamic network
    triage_remediation_steps: |
      To change ARP configuration, following CLI commands should be used:
      - set arp table cache-size VALUE
      - set arp table validity-timeout VALUE

  get_arp_entries_per_interface:
    type: device_task
    name: Get ARP entries per interface
    runner:
      type: SSH
      command: arp -an
    parser:
      args: []
      method: parse_get_arp_entries_per_interface
    register_to: arp_entries_per_interface
    go_to: get_max_ips_per_interface

  get_max_ips_per_interface:
    type: device_task
    name: Get maximum number of IP addresses per interface
    runner:
      type: SSH
      command: ifconfig
    parser:
      args: []
      method: parse_get_max_ips_per_interface
    register_to: max_ips_per_interface
    go_to: check_subnetmask_too_wide

  check_subnetmask_too_wide:
    type: logic
    name: Check if subnetmask too wide and arp table is full per interface
    args: [arp_entries_per_interface,max_ips_per_interface]
    method: parse_check_subnetmask_too_wide
    register_to: result_subnetmask_too_wide
    go_to: check_result_subnetmask_too_wide

  check_result_subnetmask_too_wide:
    type: if
    name: Evaluate if subnetmask too wide and arp table is full per interface
    condition: result_subnetmask_too_wide
    then_go_to: conclusion_segmentation_required_or_increase_arp_table
    else_go_to: get_interface_and_entry_for_arping

  
  conclusion_segmentation_required_or_increase_arp_table:
    type: conclusion
    name: Segmentation required and/or increase in ARP table
    triage_conclusion: Segmentation required and/or increase in ARP table
    triage_remediation_steps: |
      - Configure a higher ARP limit: 
          set arp table cache-size VALUE
      - Reduce subnet mask on the firewall
      - Contact network architect and segment the network

  get_interface_and_entry_for_arping:
    type: logic
    name: Get interface and ARP entry that will be used for arping
    args: [arp_entries_per_interface]
    method: parse_get_interface_and_entry_for_arping
    register_to: interface_and_entry_for_ping
    go_to: run_arping

  run_arping:
    type: device_task
    name: Check if arp entry is valid and firewall not under DDoS attack
    runner:
      type: SSH
      command: arping -c 3 -I {{interface_and_entry_for_ping['interface']}} {{interface_and_entry_for_ping['ip_address']}}
    parser:
      args: []
      method: parse_run_arping
    register_to: run_arping_result
    go_to: check_run_arping_result

  check_run_arping_result:
    type: if
    name: Evaluate if ARP entry is valid and respond to ping
    condition: run_arping_result
    then_go_to: validate_jhf
    else_go_to: conclusion_arp_table_not_valid


  conclusion_arp_table_not_valid:
    type: conclusion
    name: ARP table not valid
    triage_conclusion: Firewall under ARP/DDoS attack or environment extremely dynamic and validity-timeout should be reduced to minimum
    triage_remediation_steps: |
      - Investigate if device is under ARP/DDoS attack
      - Reduce validity-timeout:
          set arp table validity-timeout VALUE

  validate_jhf:
    type: device_task
    name: Identify if the firewall is running latest JHF
    runner:
      type: SSH
      command: cpinfo -y all
    parser:
      args: []
      method: parse_validate_jhf
    register_to: jhf_result
    go_to: check_jhf_result

  check_jhf_result:
    type: if
    name: Evaluate if the firewall is running with any Jumbo installed
    condition: jhf_result
    then_go_to: conclusion_unable_to_identify_cause
    else_go_to: conclusion_latest_jhf_required

  conclusion_latest_jhf_required:
    type: conclusion
    name: Latest JHF may be required 
    triage_conclusion: Latest JHF may be required.
    triage_remediation_steps: |
      - Check for bugs related to current software version and ARP bugs.
      - Confirm with Check Point support if there are known issues with ARP on current software version.
      - Install latests JHF if issue is related to ARP bugs.

  conclusion_unable_to_identify_cause:
    type: conclusion
    name: Unable to identify cause
    triage_conclusion: Unable to identify the cause. Further investigation needed.
    triage_remediation_steps: |
      - Make sure you are running with the latest Jumbo HF installed.
      - Obtain packet captures (tcpdump/fwmonitor).
      - Further troubleshooting by firewall administrator.
      - Engage Check Point support.


