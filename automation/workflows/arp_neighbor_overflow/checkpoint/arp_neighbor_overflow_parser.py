import re
import ipaddress
from parser_service.public import helper_methods


def check_variant_matches(**kwargs) -> bool:
    # This should work on all devices with one of the below versions
    tags = kwargs.get('my_device_tags', None)
    if tags is None:
        return False
    versions = [x.lower() for x in ['R77.30', 'R80.10', 'R80.20', 'R80.30', 'R76SP.30', 'R76SP.40', 'R76SP.50', 'R80.20SP', 'R80.40']]
    if tags.get('os.version', '').lower() not in versions:
        return False
    return True


def parse_get_number_arp_entries(raw_data: str) -> int:
    return int(raw_data)


def parse_get_arp_size(raw_data: str) -> int:
    return int(raw_data)


def parse_get_full_arp_configuration(raw_data: str) -> bool:
    default_cache_size = 4096
    default_validity_timeout = 60
    data = helper_methods.parse_data_as_object(raw_data, 'checkpoint_gaia_arp_configuration.textfsm')
    return int(data['cache_size']) < default_cache_size or int(data['validity_timeout']) > default_validity_timeout


def parse_get_arp_entries_per_interface(raw_data: str) -> dict:
    data = helper_methods.parse_data_as_list(raw_data, 'checkpoint_gaia_arp_entries.textfsm')
    arp_entries = {}

    for arp_entry in data:
        interface = arp_entry['interface']
        mac_address = arp_entry['mac_address']
        ip_address = arp_entry['ip_address']
        if interface not in arp_entries:
            arp_entries[interface] = {mac_address: ip_address}
        else:
            arp_entries[interface].update({mac_address: ip_address})
    return arp_entries


def parse_get_max_ips_per_interface(raw_data: str) -> dict:
    data = helper_methods.parse_data_as_list(raw_data, 'checkpoint_gaia_ips_per_interface.textfsm')
    interfaces = {}

    for interface_entry in data:
        network = ipaddress.IPv4Network((interface_entry['ip_address'], interface_entry['mask']), False)
        interfaces[interface_entry['interface']] = network.num_addresses

    return interfaces


def parse_check_subnetmask_too_wide(arp_entries_per_interface: dict, max_ips_per_interface: dict) -> bool:
    for interface in arp_entries_per_interface:
        number_of_entries = len(arp_entries_per_interface[interface])
        hosts_per_network = max_ips_per_interface[interface]
        if (number_of_entries / hosts_per_network) * 100 > 80.0:
            return True

    return False


def parse_run_arping(raw_data: str) -> bool:
    return 'Unicast reply from' in raw_data


def parse_validate_jhf(raw_data: str) -> bool:
    return "JUMBO" in raw_data or "JHF" in raw_data


def parse_get_interface_and_entry_for_arping(arp_entries_per_interface: dict) -> dict:
    interface_with_most_entries = ''
    number_of_entries = 0
    ip_address_to_arping = ''

    for interface in arp_entries_per_interface:
        if number_of_entries < len(arp_entries_per_interface[interface]):
            interface_with_most_entries = interface
            number_of_entries = len(arp_entries_per_interface[interface])
            ip_address_to_arping = list(arp_entries_per_interface[interface].values())[0]

    return {'interface': interface_with_most_entries, 'ip_address': ip_address_to_arping}
