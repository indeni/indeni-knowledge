IS_LOG_FILE_GROWING_NO = ("""10154 /opt/CPmds-R80/log/fw.log
10154 /opt/CPmds-R80/log/fw.log
""", False)

IS_LOG_FILE_GROWING_YES = ("""10154 /opt/CPmds-R80/log/fw.log
10155 /opt/CPmds-R80/log/fw.log
""", True)

COLLECT_MANAGEMENT_TYPE_MDS = ("""+-----------------------------------------------------------------------------------------------------+
|                                      Processes status checking                                      |
+------+--------------------+-----------------+-------------+-------------+-------------+-------------+
| Type | Name               | IP address      | FWM         | FWD         | CPD         | CPCA        |
+------+--------------------+-----------------+-------------+-------------+-------------+-------------+
| MDS  |          -         | 10.11.94.33     | up 16773    | up 10810    | up 4813     | up 12225    |
+------+--------------------+-----------------+-------------+-------------+-------------+-------------+
| CMA  | CP-R80.10-MDS2-... | 10.11.94.34     | up 8523     | up 8504     | up 7977     | up 12262    |
| CMA  | CP-R80.10-MDS2-... | 10.11.94.35     | up 8535     | up 8425     | up 7888     | up 12220    |
+------+--------------------+-----------------+-------------+-------------+-------------+-------------+
| Total Domain Management Servers checked: 2     2 up   0 down                                        |
| Tip: Run mdsstat -h for legend                                                                      |
+-----------------------------------------------------------------------------------------------------+
""", True)

COLLECT_MANAGEMENT_TYPE_NO_MDS = ("mdsstat: command not found", False)

DEFAULT_DIR_CHECK_YES = ("""
-rw-rw-r-- 1 admin root   8.2K Jan  6 00:00 fw.log
-rw-rw---- 1 admin root   8.0K Jan  6 00:00 fw.logLuuidDB
-rw-rw-r-- 1 admin root     80 Jan  6 00:00 fw.logaccount_ptr
-rw-rw-r-- 1 admin root     80 Jan  6 00:00 fw.loginitial_ptr
-rw-rw-r-- 1 admin root     80 Jan  6 00:00 fw.logptr
""", True)

DEFAULT_DIR_CHECK_NO = ("""ls: /opt/CPsuite-R80/fw1/log/: No such file or directory""", False)

LOG_PARTITION_LOWER_THAN_90 = ("""Filesystem                         Size  Used Avail Use% Mounted on
                                    /dev/mapper/vg_splat-lv_current  32G  9.1G   23G  29% /
                                    /dev/sda1                        291M   25M  251M   9% /boot
                                    tmpfs                             16G  4.0K   16G   1% /dev/shm
                                    /dev/mapper/vg_splat-lv_log       32G  4.0G   29G  13% /var/log""", False)


LOG_PARTITION_HIGHER_THAN_90 = ("""Filesystem                         Size  Used Avail Use% Mounted on
                                    /dev/mapper/vg_splat-lv_current  32G  9.1G   23G  29% /
                                    /dev/sda1                        291M   25M  251M   9% /boot
                                    tmpfs                             16G  4.0K   16G   1% /dev/shm
                                    /dev/mapper/vg_splat-lv_log       32G  4.0G   29G  95% /var/log""", True)




IFCONFIG = ("""
bond1       Link encap:Ethernet  HWaddr 00:50:56:AC:EC:87  
            inet addr:14.1.1.2  Bcast:14.1.1.255  Mask:255.255.255.0
            UP BROADCAST RUNNING MASTER MULTICAST  MTU:1500  Metric:1
            RX packets:670664530 errors:0 dropped:0 overruns:0 frame:0
            TX packets:398401474 errors:0 dropped:0 overruns:0 carrier:0
            collisions:0 txqueuelen:0 
            RX bytes:110839022 (105.7 MiB)  TX bytes:3792321866 (3.5 GiB)

eth0        Link encap:Ethernet  HWaddr 00:50:56:AC:2B:0A  
            inet addr:10.11.94.12  Bcast:10.11.94.255  Mask:255.255.255.0
            UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
            RX packets:1190221688 errors:0 dropped:0 overruns:0 frame:0
            TX packets:500497120 errors:0 dropped:0 overruns:0 carrier:0
            collisions:0 txqueuelen:1000 
            RX bytes:2760675943 (2.5 GiB)  TX bytes:2454895049 (2.2 GiB)

eth1        Link encap:Ethernet  HWaddr 00:50:56:AC:EC:87  
            UP BROADCAST RUNNING SLAVE MULTICAST  MTU:1500  Metric:1
            RX packets:670664530 errors:0 dropped:0 overruns:0 frame:0
            TX packets:398401474 errors:0 dropped:0 overruns:0 carrier:0
            collisions:0 txqueuelen:1000 
            RX bytes:110839022 (105.7 MiB)  TX bytes:3792321866 (3.5 GiB)

eth2        Link encap:Ethernet  HWaddr 00:50:56:AC:3B:BE  
            inet addr:13.1.1.2  Bcast:13.1.1.255  Mask:255.255.255.0
            UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
            RX packets:707524414 errors:0 dropped:0 overruns:0 frame:0
            TX packets:398486689 errors:0 dropped:0 overruns:0 carrier:0
            collisions:0 txqueuelen:1000 
            RX bytes:416054196 (396.7 MiB)  TX bytes:3810595701 (3.5 GiB)

eth2.5      Link encap:Ethernet  HWaddr 00:50:56:AC:3B:BE  
            inet addr:55.55.55.12  Bcast:55.55.55.255  Mask:255.255.255.0
            UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
            RX packets:0 errors:0 dropped:0 overruns:0 frame:0
            TX packets:398401572 errors:0 dropped:0 overruns:0 carrier:0
            collisions:0 txqueuelen:0 
            RX bytes:0 (0.0 b)  TX bytes:3807019418 (3.5 GiB)

eth2.5:1    Link encap:Ethernet  HWaddr 00:50:56:AC:3B:BE  
            inet addr:66.66.66.12  Bcast:66.66.66.255  Mask:255.255.255.0
            UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1

lo          Link encap:Local Loopback  
            inet addr:127.0.0.1  Mask:255.0.0.0
            UP LOOPBACK RUNNING  MTU:16436  Metric:1
            RX packets:53288163 errors:0 dropped:0 overruns:0 frame:0
            TX packets:53288163 errors:0 dropped:0 overruns:0 carrier:0
            collisions:0 txqueuelen:0 
            RX bytes:2154059684 (2.0 GiB)  TX bytes:2154059684 (2.0 GiB)

loop00      Link encap:Local Loopback  
            inet addr:10.11.95.188  Bcast:10.11.95.188  Mask:255.255.255.255
            UP BROADCAST RUNNING MULTICAST  MTU:16436  Metric:1
            RX packets:2 errors:0 dropped:0 overruns:0 frame:0
            TX packets:2 errors:0 dropped:0 overruns:0 carrier:0
            collisions:0 txqueuelen:0 
            RX bytes:56 (56.0 b)  TX bytes:56 (56.0 b)
            """, ['14.1.1.2', '10.11.94.12', '13.1.1.2', '55.55.55.12', '66.66.66.12', '127.0.0.1', '10.11.95.188']
)


CPLIC_PRINT = ("""
Host             Expiration  Features            
10.11.94.12      21Mar2020   CPSG-C-8-U CPSB-FW CPSB-VPN CPSB-IPSA CPSB-DLP CPSB-SSLVPN-U CPSB-IA CPSB-ADNC CPSG-VSX-25S CPSB-SWB CPSB-IPS CPSB-AV CPSB-URLF CPSB-ASPM CPSB-APCL CPSB-ABOT CPSB-CTNT CK-646174C03100
10.11.94.31      25Jul2019   CPSG-C-8-U CPSB-FW CPSB-VPN CPSB-IPSA CPSB-DLP CPSB-SSLVPN-U CPSB-IA CPSB-ADNC CPSG-VSX-25S CPSB-SWB CPSB-IPS CPSB-AV CPSB-URLF CPSB-ASPM CPSB-APCL CPSB-ABOT CPSB-CTNT CK-82A2378E8569

Contract Coverage:

 #   ID          Expiration   SKU                 
===+===========+============+====================
1  | 3P3A162   | 20Apr2020  | CPSB-TEX-EVAL
   +-----------+------------+--------------------
   |Covers:     CPSG-C-8-U CPSB-FW CPSB-VPN CPSB-IPSA CPSB-DLP CPSB-SSLVPN-U CPSB-IA CPSB-ADNC CPSG-VSX-25S CPSB-SWB CPSB-IPS CPSB-AV CPSB-URLF CPSB-ASPM CPSB-APCL CPSB-ABOT CPSB-CTNT CK-646174C03100
===+===========+============+====================
2  | 3HFYA3Y   | 20Apr2020  | CPSB-CTNT-EVAL
   +-----------+------------+--------------------
   |Covers:     CPSG-C-8-U CPSB-FW CPSB-VPN CPSB-IPSA CPSB-DLP CPSB-SSLVPN-U CPSB-IA CPSB-ADNC CPSG-VSX-25S CPSB-SWB CPSB-IPS CPSB-AV CPSB-URLF CPSB-ASPM CPSB-APCL CPSB-ABOT CPSB-CTNT CK-646174C03100
===+===========+============+====================
3  | 48838TI   | 20Apr2020  | CPSB-IPS-EVAL
   +-----------+------------+--------------------
   |Covers:     CPSG-C-8-U CPSB-FW CPSB-VPN CPSB-IPSA CPSB-DLP CPSB-SSLVPN-U CPSB-IA CPSB-ADNC CPSG-VSX-25S CPSB-SWB CPSB-IPS CPSB-AV CPSB-URLF CPSB-ASPM CPSB-APCL CPSB-ABOT CPSB-CTNT CK-646174C03100
===+===========+============+====================
4  | 6D7U3Y5   | 20Apr2020  | CPSB-TE-EVAL
   +-----------+------------+--------------------
   |Covers:     CPSG-C-8-U CPSB-FW CPSB-VPN CPSB-IPSA CPSB-DLP CPSB-SSLVPN-U CPSB-IA CPSB-ADNC CPSG-VSX-25S CPSB-SWB CPSB-IPS CPSB-AV CPSB-URLF CPSB-ASPM CPSB-APCL CPSB-ABOT CPSB-CTNT CK-646174C03100
===+===========+============+====================
""",['10.11.94.12', '10.11.94.31'])

CAT_CPDIR_WORKED = """
                                :ICAip (10.11.94.30)
"""

MANAGEMENT_SERVER_IP_ADDRESS_FROM_GW = (CAT_CPDIR_WORKED, '10.11.94.30', '10.11.94.30')


PING_OK = ("""PING 10.11.94.31 (10.11.94.31) 56(84) bytes of data.
64 bytes from 10.11.94.31: icmp_seq=1 ttl=64 time=9.51 ms
64 bytes from 10.11.94.31: icmp_seq=2 ttl=64 time=0.335 ms
64 bytes from 10.11.94.31: icmp_seq=3 ttl=64 time=0.320 ms

--- 10.11.94.31 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2000ms
rtt min/avg/max/mdev = 0.320/3.390/9.515/4.331 ms
""", True)

PING_FAILED = ("""PING 10.11.94.31(10.11.94.31) 56(84) bytes of data.

--- 10.11.94.31 ping statistics ---
3 packets transmitted, 0 received, 100% packet loss, time 2000ms
""", False)

CONNECTION_ESTABLISHED_PORT_257 = ("""
tcp        0      0 0.0.0.0:257                 0.0.0.0:*                   LISTEN      6009/fwd            
tcp        0      0 10.11.94.12:37331           10.11.94.31:257             ESTABLISHED 6009/fwd   """, '10.11.94.31', '257', True)


CONNECTION_NOT_ESTABLISHED_PORT_257 = (""" """,  '10.11.94.31', '257', False)