import unittest
import automation.workflows.CrossVendorDeviceIsLoggingLocallyRule.checkpoint.firewall_logging_locally_parser as parser
import automation.workflows.CrossVendorDeviceIsLoggingLocallyRule.checkpoint.firewall_logging_locally_mock_data as mock

class LoggingLocallyTests(unittest.TestCase):
    def test_parse_firewall_log_filesize_check(self):
        self.assertEqual(parser.parse_firewall_log_filesize_check(mock.IS_LOG_FILE_GROWING_NO[0], path="automation/workflows/CrossVendorDeviceIsLoggingLocallyRule/checkpoint/checkpoint_gaia_filesize.textfsm"), mock.IS_LOG_FILE_GROWING_NO[1])
        self.assertEqual(parser.parse_firewall_log_filesize_check(mock.IS_LOG_FILE_GROWING_YES[0], path="automation/workflows/CrossVendorDeviceIsLoggingLocallyRule/checkpoint/checkpoint_gaia_filesize.textfsm"), mock.IS_LOG_FILE_GROWING_YES[1])

    def test_default_dir_check(self):
        self.assertEqual(parser.default_dir_check(mock.DEFAULT_DIR_CHECK_YES[0]), mock.DEFAULT_DIR_CHECK_YES[1])
        self.assertEqual(parser.default_dir_check(mock.DEFAULT_DIR_CHECK_NO[0]), mock.DEFAULT_DIR_CHECK_NO[1])

   
    def test_parse_get_management_server_ip_address(self):
        self.assertEqual(parser.parse_get_management_server_ip_address(mock.MANAGEMENT_SERVER_IP_ADDRESS_FROM_GW[0]), mock.MANAGEMENT_SERVER_IP_ADDRESS_FROM_GW[2])

    def test_parse_ping_management_ip_address(self):
        self.assertEqual(parser.parse_ping_management_ip_address(mock.PING_OK[0]),mock.PING_OK[1])
        self.assertEqual(parser.parse_ping_management_ip_address(mock.PING_FAILED[0]),mock.PING_FAILED[1])

    def test_parse_connection_established_check_port_257(self):
        data = mock.CONNECTION_ESTABLISHED_PORT_257
        self.assertEqual(parser.parse_connection_established_check_port_257(data[0], management_ip_address=data[1]), data[3])
        data = mock.CONNECTION_NOT_ESTABLISHED_PORT_257
        self.assertEqual(parser.parse_connection_established_check_port_257(data[0], management_ip_address=data[1]), data[3])

if __name__ == '__main__':
    unittest.main()
