import re
from typing import List
from dateutil import parser
from datetime import datetime, timedelta
from parser_service.public.helper_methods import *


def check_variant_matches(**kwargs) -> bool:
    # This should work on all devices with one of the below versions
    tags = kwargs.get('my_device_tags', None)
    if tags is None:
        return False
    versions = [x.lower() for x in ['R77.30', 'R80.10', 'R80.20', 'R80.30', 'R76SP.30', 'R76SP.40', 'R76SP.50', 'R80.20SP', 'R80.40']]
    if tags.get('os.version', '').lower() not in versions:
        return False
    return True


def ip_address_list_dict_to_list(ip_addresses_list_dict: list) -> list:
    ip_addresses_list = []
    for ip_address in ip_addresses_list_dict:
        ip_addresses_list.append(ip_address['ip_address'])
    return ip_addresses_list

def parse_firewall_log_filesize_check(raw_data: str, path='checkpoint_gaia_filesize.textfsm') -> bool:
    filesizes = parse_data_as_list(raw_data, path)
    if filesizes:
        return (filesizes.count(filesizes[0]) != len(filesizes))
    return False

def default_dir_check(raw_data: str) -> bool:
    return 'No such file or directory' not in raw_data

def parse_get_management_server_ip_address(raw_data: str) -> str:
    return raw_data.strip().strip(":ICAip (").strip(")")

def parse_ping_management_ip_address(raw_data: str) -> bool:
    return 'received, 0% packet loss' in raw_data

def parse_connection_established_check_port_257(raw_data: str, **kwargs) -> bool:
    management_ip_address = kwargs.get("management_ip_address")
    if management_ip_address is None:
        return False
    socket = management_ip_address + ':257'
    return socket in raw_data
