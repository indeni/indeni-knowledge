import unittest
import re
from indeni_workflow.remote.device_credentials import SshCredentials, MockCredentials
from indeni_workflow.remote.device_data import DeviceData
from indeni_workflow.remote.remote_type import RemoteType
from indeni_workflow.workflow import Workflow
from vpn_tunnel_down_parser import parse_single_vpn_tunnel, parse_ipsec_sa_status, parse_ike_gw_name, parse_ike_sa_status, parse_system_logs_ike, parse_system_logs_ipsec
import vpn_tunnel_down_mock_data as mock

ATE = 'vpn_tunnel_down_workflow.yaml'


class DeviceTests(unittest.TestCase):

    def test_parse_single_vpn_tunnel(self):
        self.assertEqual(parse_single_vpn_tunnel(mock.SHOW_RUNNING_TUNNEL[0]), mock.SHOW_RUNNING_TUNNEL[1])

    def test_parse_ipsec_sa_status(self):
        self.assertEqual(parse_ipsec_sa_status(mock.SHOW_IPSEC_SA_STATUS[0]), mock.SHOW_IPSEC_SA_STATUS[1])

    def test_parse_ike_gw_name(self):
        self.assertEqual(parse_ike_gw_name(mock.SHOW_TUNNEL_NAME_GW[0]), mock.SHOW_TUNNEL_NAME_GW[1])

    def test_parse_ike_sa_status(self):
        self.assertEqual(parse_ike_sa_status(mock.SHOW_IPSEC_SA_STATUS[0]), mock.SHOW_IPSEC_SA_STATUS[1])

    def test_parse_system_logs_ike(self):
        self.assertEqual(parse_system_logs_ike(mock.SHOW_LOG_SYSTEM_PHASEI[0]), mock.SHOW_LOG_SYSTEM_PHASEI[1])

    def test_parse_system_logs_ipsec(self):
        self.assertEqual(parse_system_logs_ipsec(mock.SHOW_LOG_SYSTEM_PHASEII[0]), mock.SHOW_LOG_SYSTEM_PHASEII[1])


if __name__ == '__main__':
    unittest.main()
