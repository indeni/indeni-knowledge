
SHOW_RUNNING_TUNNEL = ("""
tunnel  IKE_GW
        id:                     4
        type:                   IPSec
        gateway id:             1
        local ip:               172.30.250.1
        peer ip:                100.100.100.1
        inner interface:        tunnel.10 
        outer interface:        ethernet1/1
        state:                  init
        session:                162333
        tunnel mtu:             1448
        soft lifetime:          N/A
        hard lifetime:          N/A
        lifetime remain:        N/A
        lifesize remain:        N/A
        monitor:                off
          monitor packets seen: 0
          monitor packets reply:0
        en/decap context:       1       
        local spi:              00000000
        remote spi:             00000000
        key type:               auto key
        protocol:               ESP
        auth algorithm:         NOT ESTABLISHED
        enc  algorithm:         NOT ESTABLISHED
        anti replay check:      yes
        copy tos:               no
        enable gre encap:       no
        authentication errors:  0
        decryption errors:      0
        inner packet warnings:  0
        replay packets:         0
        packets received 
          when lifetime expired:0
          when lifesize expired:0
        sending sequence:       0
        receive sequence:       0
        encap packets:          0
        decap packets:          0
        encap bytes:            0
        decap bytes:            0
        key acquire requests:   0
        owner state:            0
        owner cpuid:            s1dp0
        ownership:              1
        """, 'init')

SHOW_IPSEC_SA_STATUS = ("""
IPSec SA for tunnel IKE_GW not found.
""", True)

SHOW_TUNNEL_NAME_GW = ("""
4      IKE_GW                         IKE_GW               0.0.0.0/0            0:0        0.0.0.0/0            0:0        ESP tunl [DH2][AES128,3DES][SHA1] 3600-sec 0-kb
""", 'IKE_GW')

SHOW_IKE_SA_STATUS = ("""
IKE SA for gateway ID 1 not found.
""", True)

SHOW_LOG_SYSTEM_PHASEI = ("""
2019/09/29 23:38:14 info vpn IKE_GW ike-neg 0 IKE phase-1 negotiation is failed. no suitable proposal found in peer's SA payload.
2019/09/29 23:37:25 info vpn IKE_GW ike-neg 0 IKE phase-1 negotiation is failed as initiator, main mode. Failed SA: 10.10.20.100[500]-10.10.2.250[500] cookie:b8efac1282141d87:0000000000000000. Due to timeout.
""", True)

SHOW_LOG_SYSTEM_PHASEII = ("""
2019/10/02 22:19:07 info vpn IKE_GW ike-neg 0 IKE phase-2 negotiation failed when processing SA payload. no suitable proposal found in peer's SA payload
""", True)
