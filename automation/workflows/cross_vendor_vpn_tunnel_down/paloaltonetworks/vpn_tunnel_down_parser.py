import re
from typing import List
from dateutil import parser
from datetime import datetime, timedelta
from parser_service.public.helper_methods import *


def check_variant_matches(**kwargs) -> bool:
    # This should work on all gateways
    tags = kwargs.get('my_device_tags', None)
    if tags is None:
        return False
    if tags.get('product') != 'firewall':
        return False
    return True

def extract_ip(**kwargs) -> str:
    vpn_tunnel = kwargs.get('vpn_tunnel')
    return vpn_tunnel.strip().split(' ')[0]

def parse_single_vpn_tunnel(raw_data: str) -> str:
#    tunnel_status = parse_data_as_list(raw_data, 'vpn_tunnel_down_parse_single_vpn_tunnel.textfsm')
#    return tunnel_status[0]['tunnel_state']
    tunnel_line = ' '.join(raw_data.split())
    return tunnel_line.split()[2]

def parse_ipsec_sa_status(raw_data: str) -> bool:
    return 'not found' in raw_data


def parse_ike_tunnel_name(raw_data: str) -> str:
    gw_name = raw_data.strip().split()[1]
    if len(gw_name) > 0:
        return gw_name
    else:
        return ''

def parse_ike_gw_name(raw_data: str) -> str:
    if raw_data:
        gw_name = raw_data.strip().split()[2]
        return gw_name
    else:
        return ''

def parse_ike_sa_status(raw_data: str) -> bool:
    return 'not found' in raw_data

