#####################################################################################
#	Before running this script, make sure you have 
#	read the article on how to set up a demo collector:
#	https://indeni.atlassian.net/wiki/spaces/IKP/pages/88047672/Demo+Simulations
#####################################################################################

# Make sure we don't have indeni-collector installed
sudo apt-get remove -y indeni-collector

# Kill any running demo collectors
kill `ps aux | grep demo-collector | grep java | awk '{print $2}'`

# Get the collector JAR
collector_jar=/tmp/demo-collector.jar
if [ ! -f $collector_jar ]; then
	echo "Couldn't find the collector JAR at $collector_jar"
	exit 1
fi

# Add all the demo devices automatically (if they are already there, we'll fail, but that's fine)
find . -name "device_*.json" -exec grep -H ip {} \; | sed 's/.\/data\//curl -H '"'Content-Type: application\/json'"'  -u '"'admin:admin123!'"' -k https:\/\/localhost:9009\/api\/v1\/devices -X POST -d '\''{\"name\": \"/' | sed 's/\/device_data.json:/\",/' | sed 's/"ip"/"ip_address"/' | sed 's/,\s*$/}'\''/' > /tmp/add_demo_devices.sh
bash /tmp/add_demo_devices.sh

# Start the demo collector
demodir=`pwd`
parsersdir=`echo $demodir | sed 's/demo$/parsers/'`
java -Dindeni.rest-server.ssl.truststore=/tmp/indenicatrust.jks -Dcollector.parsers.directory=$parsersdir -Dindeni.rest-server.ssl.password=changeit -jar $collector_jar
