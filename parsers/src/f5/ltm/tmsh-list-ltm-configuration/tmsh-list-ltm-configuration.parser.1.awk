BEGIN {
    iLTMConf = 0
}

#Node configuration: ltm node Common/test { address 1.1.1.1 }
#Pool configuration:
#Virtual-server configuration:
#Virtual-address configuration:
#NAT configuration:
#SNAT configuration:
/^[a-zA-Z-]+?\sconfiguration:/{

    if(NF > 2){

        iLTMConf++
        configurationObjectArray[iLTMConf, "type"] = $1
    
    }

}

END {

    writeComplexMetricObjectArray("f5-vcmp-host-existing-ltm-configuration", null, configurationObjectArray, "false")  # Converted to new syntax by change_ind_scripts.py script

}