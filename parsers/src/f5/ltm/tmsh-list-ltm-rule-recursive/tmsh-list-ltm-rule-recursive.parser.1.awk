#ltm rule Common/samplematchclassrule {
/^ltm\srule/{

    ruleTags["name"] = "/" $3
    matchClassFound = 0
}

/^}$/{
    if(matchClassFound == 1){
        writeComplexMetricString("f5-matchclass-used", ruleTags, "true", "false")  # Converted to new syntax by change_ind_scripts.py script
    } else {
        writeComplexMetricString("f5-matchclass-used", ruleTags, "false", "false")  # Converted to new syntax by change_ind_scripts.py script
    }
}

{
    #Match all except the rule name row and the end of a rule definition
    if(!match($0, /^ltm\srule/ && !match($0, /^}$/))){
        if(match($0, /[^\#]*matchclass/)){
            matchClassFound = 1
        }
    }
}