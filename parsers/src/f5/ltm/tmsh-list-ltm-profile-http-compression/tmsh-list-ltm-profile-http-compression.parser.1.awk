BEGIN {

    precompressedContentTypeArray["video"] = /.*video.*/
    precompressedContentTypeArray["audio"] = /.*audio.*/
    precompressedContentTypeArray["x-tar"] = /.*x-tar.*/
    precompressedContentTypeArray["zip"] = /.*zip.*/
    precompressedContentTypeArray["compress"] = /.*compress.*/
    precompressedContentTypeArray["jpeg"] = /.*jpeg.*/
    precompressedContentTypeArray["x-m4v"] = /.x-m4v.*/
    precompressedContentTypeArray["ogg"] = /.*\/ogg.*/
    precompressedContentTypeArray["png"] = /.*png.*/
    precompressedContentTypeArray["gif"] = /.*\/gif\".*/

}

#ltm profile http-compression
/^ltm profile http-compression/ {
    
    delete compressionArray
    compressionTags["name"] = "/" $4

}

#    content-type-include {
/^\s+content-type-include \{/ {

    for(description in precompressedContentTypeArray){
        if(match($0, precompressedContentTypeArray[description])) {
            iCompr++
            compressionArray[iCompr, "filetype"] = description
        }
    }
    
}

#}
/^\}$/ {
    
    writeComplexMetricObjectArray("f5-compression-profile-precompressed-content-types", compressionTags, compressionArray, "false")  # Converted to new syntax by change_ind_scripts.py script

}