#64 bytes from 192.168.197.16: icmp_seq=1 ttl=64 time=0.365 ms
/time=.+\sms$/{

    nodeIP = $4
    sub(/:/,"",nodeIP)
    
    nodeTags["name"] = nodeIP

    split($7,timeArr,/=/)
    responseTime = timeArr[2]
    
    writeDoubleMetric("lb-server-ping-response", nodeTags, "gauge", responseTime, "false")  # Converted to new syntax by change_ind_scripts.py script
}