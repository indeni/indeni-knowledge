name: f5-rest-mgmt-tm-ltm-pool
description: Determine pool member state, availability, capacity and action on service
    down
type: monitoring
monitoring_interval: 5 minutes
requires:
    vendor: f5
    product: load-balancer
    rest-api: 'true'
comments:
    lb-pool-member-availability:
        why: |
            A member marked as down by a monitor results in reduced pool capacity or in worst case, downtime. This metric would warn administrators when a member is marked as down.
        how: |
            This alert uses the iControl REST interface to extract the member statuses on the device.
        can-with-snmp: true
        can-with-syslog: false
    lb-pool-member-state:
        why: |
            A node disabled by an administrator results in reduced pool capacity or in worst case, downtime. Disabling nodes is common during ie. a maintenance but it is easily forgotten. This metric would warn administrators when a node is not ready to accept traffic.
        how: |
            This alert uses the iControl REST interface to extract the node states on the device.
        can-with-snmp: true
        can-with-syslog: false
    lb-pool-capacity:
        why: |
            A pool that is not running with full capacity could cause slowness in the application, service disruption, or in worst case downtime. indeni tracks this by measuring the available members of the pool in percent.
        how: |
            This alert uses the iControl REST interface to extract the members available to process traffic compared to the total members of the pool.
    f5-default-action-on-service-down:
        why: |
            The default option is "None", which maintains connections to pool member even when the monitor fails, but does not create new connections. The better option in most cases however, is "Reject" which instead resets the existing connection and forces the client to establish a new one. This, coupled with good monitors ensures that the client has an optimal chance of connecting to a functioning pool member.
        how: |
            This alert uses the iControl REST interface to extract the option "Action On Service Down" for all configured pools.
steps:
-   run:
        type: HTTP
        command: /mgmt/tm/ltm/pool?expandSubcollections=true&$select=fullPath,serviceDownAction,membersReference/items/fullPath,membersReference/items/selfLink,membersReference/items/state,membersReference/items/session
    parse:
        type: JSON
        file: rest-mgmt-tm-ltm-pool.parser.1.json.yaml
