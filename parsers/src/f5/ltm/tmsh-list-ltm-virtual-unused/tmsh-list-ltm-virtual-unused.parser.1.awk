#This script will identify Virtual Servers which have no objects associated to them

#ltm virtual compression-level-6-vip-443 {
/^ltm virtual/{
    virtualName = $3
    virtual_has_objects[virtualName] = 0
}


#we check the resources (policies, pool, rules) associated to the VS, if they are != to none, then there is a resource associated
#if VS has policies, then is in use
/ policies / { if ($2 != "none") virtual_has_objects[virtualName]= 1 }
#if VS has a pool, then is in use
/ pool / { if ($2 != "none") virtual_has_objects[virtualName]= 1 }
#if VS has iRules, then is in use
/ rules / { if ($2 != "none") virtual_has_objects[virtualName]= 1 }
#if VS is forwarding or reject type then we need to ignore
/ l2-forward$/ {virtual_has_objects[virtualName]= 1 }
/ ip-forward$/ {virtual_has_objects[virtualName]= 1 }
/ reject$/ {virtual_has_objects[virtualName]= 1 }

#GTM listeners are actually VS without pool so we need to ignore them, we identify them by filtering by port 53
/ destination [0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}:(domain|53)$/ {virtual_has_objects[virtualName]= 1 }

END {
	for (vs in virtual_has_objects)
		if(virtual_has_objects[vs] ~ 0)
		{
			virtualTags["vs-name"] = "Virtual Server: " vs
      writeDoubleMetric("f5-virtualserver-unused", virtualTags, "gauge", 0, "false")
    }
  }
