#ltm profile tcp mptcp-mobile-optimized {
/^ltm profile tcp/{

    section = "tcpProfile"
    tcpProfileName = $4

}

#    idle-timeout 300
/^\s+idle-timeout\s[0-9]+$/{

    idleTimeout = $2
    tcpProfileIdleTimeouts[tcpProfileName] = idleTimeout

}

#ltm virtual compression-level-6-vip-443 {
/^ltm virtual/{

    section = "virtualserver"
    virtualName = $3

}

#        wam-tcp-lan-optimized {
/^\s+[^\s]+\s\{$/{

    #Filter out only the profile names
    if(section == "virtualserver" && $1 in tcpProfileIdleTimeouts){
        virtualTags["name"] = "Virtual Server: " virtualName " TCP Profile: " $1
        writeComplexMetricString("f5-virtualserver-tcp-profile-idle-timeout", virtualTags, tcpProfileIdleTimeouts[$1], "false")  # Converted to new syntax by change_ind_scripts.py script
    }

}