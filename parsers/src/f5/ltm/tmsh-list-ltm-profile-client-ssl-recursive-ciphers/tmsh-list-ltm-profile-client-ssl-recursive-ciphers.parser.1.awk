#Cipher strings in an F5 can be comprised of a list of ciphers, or internal keywords such as DEFAULT, STRONG, WEAK etc
#Since the meaning of keywords changes between versions it's a bit of a trouble to write a script that matches all versions
#Instead, this script utilized F5's built-in command "tmm --clientciphers", which translates a cipher string to the actual
#ciphers behind the keywords

#Since the command results in a lot of lines if there is many ssl profiles I have chosen to only pick the properties I need at the moment
#This means that we need:
#Profile name - For tagging
#Ciphers - To be used as parameter to the tmm command
#Renegotiation - For Sweet32 
#Renegotiate-size - For Sweet32

#At the moment Renegotiation is the last one at the moment and thus where we write the metrics, but this could change if another property is added.
#Since the F5 presents the attributes in alphabetical order, be careful when adding a property

#Knowledge base articles
#POODLE         - https://support.f5.com/csp/#/article/K15702
#SWEET32        - https://support.f5.com/csp/#/article/K13167034 (or my document)
#DROWN          - https://support.f5.com/csp/#/article/K23196136
#Bar Mitzvah    - https://support.f5.com/csp/#/article/K16864

BEGIN {
    #Let's establish a list of the default SSL profiles in order to exclude them
    #They should never be used anyway and would cause a lot of false alarms
    profileExceptionList["/Common/clientssl-insecure-compatible"]
    profileExceptionList["/Common/clientssl-insecure-compatible"]
    profileExceptionList["/Common/clientssl"]
    profileExceptionList["/Common/clientssl-insecure-compatible"]
    profileExceptionList["/Common/wom-default-clientssl"]
}

/^ProfileName:/ {
    profileName = $2

    #Reset the variables
    SSLv2 = 0
    SSLv3 = 0
    RC4 = 0
    sweet32Ciphers = 0
}

#12:    22  DHE-RSA-DES-CBC3-SHA             168  TLS1    Native  DES       SHA     EDH/RSA
/DES-CBC3/{
    #Ciphers vulnerable to Sweet32 detected
    sweet32Ciphers = 1
}

/SSL3/{
    #Make sure we're in the table by counting number of fields
    if($NF > 2){
        SSLv3 = 1
    }
}

/SSL2/{
    #Make sure we're in the table by counting number of fields
    if($NF > 2){
        SSLv2 = 1
    }
}

/RC4/{
    #Make sure we're in the table by counting number of fields
    if($NF > 2){
        RC4 = 1
    }
}

/^Renegotiation-Size:/{
    renegotiateSize = $2
    
    #Renegotiation-size: indefinite
    if(renegotiateSize == "indefinite"){
        #Renegotiation size has been set to indefinite
        #Setting it to 0 in order to be able to compare integers
        renegotiateSize = 0
    }
}

/Renegotiation:/{

    if(!(profileName in profileExceptionList)){
    
        #Sweet32 uses long lived connections with enabled renegotiations
        #To mititgate Sweet32 renegotiations must be disabled, or either:
        #Rrenegotiation size set to less than 1000
        #The affected ciphers not present
        
        if($2 == "enabled" && renegotiateSize <= 1000 && sweet32Ciphers == 1){
            sweet32Tags["name"] = profileName
            sweet32Tags["type"] = "Client SSL profile"
            sweet32Tags["cipher"] = "DES-CBC3"
            sweet32Tags["vulnerability"] = "SWEET32"
            sweet32Tags["im.dstype.displaytype"] = "state"
            writeDoubleMetric("ssl-weak-cipher", sweet32Tags, "gauge", 1, "false")  # Converted to new syntax by change_ind_scripts.py script
        }
        
        #SSLv3 is consider not only weak, but more or less compromised (ie. Poodle)
        if(SSLv3 == 1){
            sslv3Tags["name"] = profileName
            sslv3Tags["type"] = "Client SSL profile"
            sslv3Tags["protocol"] = "SSLv3"
            sslv3Tags["vulnerability"] = "POODLE"
            sslv3Tags["im.dstype.displaytype"] = "state"
            writeDoubleMetric("ssl-weak-protocol", sslv3Tags, "gauge", 1, "false")  # Converted to new syntax by change_ind_scripts.py script
        }
        
        #SSLv2 is considered compromised (Drown)
        if(SSLv2 == 1){
            sslv2Tags["name"] = profileName
            sslv2Tags["type"] = "Client SSL profile"
            sslv2Tags["protocol"] = "SSLv2"
            sslv2Tags["vulnerability"] = "DROWN"
            sslv2Tags["im.dstype.displaytype"] = "state"
            writeDoubleMetric("ssl-weak-protocol", sslv2Tags, "gauge", 1, "false")  # Converted to new syntax by change_ind_scripts.py script
        }
        
        #RC4 is considered a weak cipher and there has been indications that is has
        #been compromised by certain government agencies
        if(RC4 == 1){
            rc4Tags["name"] = profileName
            rc4Tags["type"] = "Client SSL profile"
            rc4Tags["cipher"] = "RC4"
            rc4Tags["im.dstype.displaytype"] = "state"
            rc4Tags["vulnerability"] = "Bar Mitzvah"
            writeDoubleMetric("ssl-weak-cipher", rc4Tags, "gauge", 1, "false")  # Converted to new syntax by change_ind_scripts.py script
        }
    }
}