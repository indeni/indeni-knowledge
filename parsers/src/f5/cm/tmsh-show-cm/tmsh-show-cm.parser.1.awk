BEGIN {
        cluster_state = 0
        iKnown_devices = 0
}

#Detect when we're in the "failover_status" section
#CM::Failover Status
/CM::Failover Status/{
        section = "failover_status"
}

#Detect when we're in the "sync_status" section
#CM::Sync Status
/CM::Sync Status/{
        section = "sync_status"
}

#Detect when we're in the "device_status" section
#CentMgmt::Device: device1.mydomain.local
/CentMgmt::Device:/ {
        section = "device_status"
        known_devicename = $2
}

#Detect when we're in the "traffic_groups" section
#CM::Traffic-Group
/::Traffic-Group/ {
    if (/CM::Traffic-Group/) {
        section = "traffic_groups"
    } else {
        section = "traffic_groups"
   }

}

#Match on non-empty lines
/^.+$/ {

    #-------------------------------------------
    #CM::Failover Status
    #-------------------------------------------
    #Color    green
    #Status   ACTIVE
    #Summary  1/1 active
    #Details
    # active for /Common/traffic-group-1
    # active for /Common/traffic-group-2

    if (section == "failover_status" && $1 == "active") {

        #Split the traffic group name on "/" and save the number of elements to "n"
        n=split($NF, trafficGroupArr, "/");

        #Get the traffic group name excluding the partition
        trafficGroup = trafficGroupArr[3];


    }

    #------------------------------------------------------------------------------------------
    #CM::Sync Status
    #------------------------------------------------------------------------------------------
    #Color    green
    #Status   In Sync
    #Summary  All devices in the device group are in sync
    #Details
    #       /Common/device1.mydomain.local: connected (for 3568561 seconds)
    #       /Common/DeviceGroupName (In Sync): All devices in the device group are in sync
    #       /Common/device_trust_group (In Sync): All devices in the device group are in sync

    #Write metrics for sync_status
    if (section == "sync_status" && $1 == "Status") {

        if (($2 " " $3) == "In Sync") {
                writeDoubleMetric("cluster-config-synced", null, "gauge", 1, "false")  # Converted to new syntax by change_ind_scripts.py script
        } else {
                writeDoubleMetric("cluster-config-synced", null, "gauge", 0, "false")  # Converted to new syntax by change_ind_scripts.py script
        }
    }

    #------------------------------------------------------------------------------------------
    #CentMgmt::Device: device1.mydomain.local
    #------------------------------------------------------------------------------------------
    #Mgmt Ip               10.20.11.125
    #Configsync Ip         10.253.251.1
    #Hostname              device1.mydomain.local
    #Base Mac              00:01:d7:ef:79:03
    #Mirror Ip             10.253.251.1
    #Mirror Secondary Ip   ::
    #Multicast Interface
    #Multicast IP          0.0.0.0
    #Multicast Port        0
    #Version               11.5.4
    #Product               BIG-IP
    #Edition               Hotfix HF2
    #Build                 2.0.291
    #Marketing Name        BIG-IP vCMP Guest
    #Platform Id           Z101
    #Chassis Id            chs400354s
    #...
    #Location
    #Contact
    #Description
    #Comment
    #Time zone             CEST
    #Self device           1
    #Failover State        active

    #Write metrics for device status
    if (section == "device_status") {

        #Version 11 uses "Failover State", Version 12 uses "Device HA State"
        if (match($0, /((Failover State)|(Device HA State))\s+active/)) {
                cluster_state = 1
        }

        #Get the management IP of the device
        #Mgmt Ip               10.20.11.125
        if (match($0, /Mgmt Ip/)) {
                knownDeviceManagementIP = $NF
        }

    }

    if (section == "traffic_groups") {
        #--------------------------------------------------------------------------
        #CentMgmt::Traffic-Group
        #Name                      Device         Status   Next    Previous  Active
        #                                                  Active  Active    Reason
        #--------------------------------------------------------------------------
        #traffic-group-1           device1.local  standby  true    true      -
        #traffic-group-1           device2.local  active   false   false     -
        #traffic-group-local-only  -              -        -       -         -

        if (match($0, /(true|false)/)) {
                traffic_group_name = $1
                device_name = $2 
                status = $3
                memberStateTags["name"] = device_name
                member_health = 1

                #we need to check at least 2 lines for each traffic-group
                #we will score them based in status

                if(status == "active") {
                    health_counter = 1 
                    member_health = 1                             
                }
            
                else if (status =="standby") {
                    health_counter = 1 
                    member_health = 0                             

                }
                else {
                    health_counter = 0
                    member_health = 0                             
                }
                
                writeDoubleMetric("cluster-member-active", memberStateTags, "gauge", member_health, "false")

                traffic_groups_health_array[traffic_group_name]= traffic_groups_health_array[traffic_group_name] + health_counter
                traffic_groups_health_total_devices[traffic_group_name]=traffic_groups_health_total_devices[traffic_group_name] + 1
            }
    }

}

#Empty lines signifies the end of a section
/^$|^\s$/{

    #Check which section ended and write metrics if applicable
    if (section == "device_status") {
            iKnown_devices++
            known_devices[iKnown_devices, "name"] = known_devicename
            known_devices[iKnown_devices, "ip"] = knownDeviceManagementIP
    }

    #Reset the section variable
    section = ""
}

END {

    for (traffic_group_name in traffic_groups_health_array)
        {
            cluster_health = 0
            final_cluster_health = 1

            #We are using active and standby as scoring values, should be same as number of devices for healthy cluster

            cluster_health = traffic_groups_health_array[traffic_group_name]
            total_number_devices = traffic_groups_health_total_devices[traffic_group_name]
            if (cluster_health != total_number_devices)     
                final_cluster_health = 0             
            
            trafficGroupTags["name"]=traffic_group_name
            writeDoubleMetric("cluster-state", trafficGroupTags, "gauge", final_cluster_health, "false")
        }

    writeComplexMetricObjectArray("known-devices", null, known_devices, "false")
}
