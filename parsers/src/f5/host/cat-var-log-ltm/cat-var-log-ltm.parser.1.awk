BEGIN{
    iaddress_conflict  = 0
    snatexhaust = 0
}

#Jun  6 14:21:14 F5-LB1 warning tmm[18497]: 01190004:4: address conflict detected for 10.245.0.253 (00:50:56:90:01:22) on vlan 31
/address conflict detected for/ {

    log_line = $0

    #Remove parts that we are no interested in
    sub(/^.*address conflict detected for /, "", log_line)

    #10.245.0.253 (00:50:56:90:01:22) on vlan 31
    split(log_line, arr_description, /\s/)

    ip_address = arr_description[1]
    mac_address = arr_description[2]

    #(00:50:56:90:01:22)
    sub(/(\(|\))/, "", mac_address)

    #There has been examples on devcentral where vlan id was not there.
    #Might be older versions, or the IP residing on an untagged VLAN, but I'm adding this check here all the same.
    if ($(NF-1) == "vlan") {
        vlan_id = $NF
    } else {
        vlan_id = "n/a"
    }

    iaddress_conflict ++
    address_conflict_obj[iaddress_conflict , "ip"] = ip_address
    address_conflict_obj[iaddress_conflict , "mac-address"] = mac_address
    address_conflict_obj[iaddress_conflict , "vlan-id"] = vlan_id

}
#Jun  6 14:21:25 F5-LB1 crit tmm1[18497]: 01010201:2: Inet port exhaustion on 192.168.10.100 to 172.28.21.11:80 (proto 6)
/Inet port exhaustion/ {
    # If we see this in the log, we will set snatexhaust to true
    snatexhaust = 1
}

END {
    writeComplexMetricObjectArray("ip-address-conflict", null, address_conflict_obj, "false")  
    writeDoubleMetric("f5-snat-exhaustion-warning", null, "gauge", snatexhaust, "false") 
}