BEGIN{

    iFile = 0

}

#lrwxrwxrwx 1 root root 25 2017-03-17 08:16 test.core.gz -> /shared/core/test.core.gz

/\.core\.gz$/{

    dateCreated = $6
    
    #2016-12-08
    split(dateCreated, dateArr, /-/)
    
    year = dateArr[1]
    month = dateArr[2]
    day = dateArr[3]
    
    timeCreated = $7
    
    #08:16
    split(timeCreated, timeArr, /:/)
    
    hour = timeArr[1]
    minute = timeArr[2]
    second = 0
    
    secondsSinceEpoch = datetime(year, month, day, hour, minute, second)
    
    iFile++
    coreFiles[iFile, "path"] = "/var/core" $8
    coreFiles[iFile, "created"] = secondsSinceEpoch

}

END {

    writeComplexMetricObjectArray("core-dumps", null, coreFiles, "false")  # Converted to new syntax by change_ind_scripts.py script

}