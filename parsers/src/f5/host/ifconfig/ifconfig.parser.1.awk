BEGIN {

    netMaskToCIDR["0.0.0.0"] = 0
    netMaskToCIDR["128.0.0.0"] = 1
    netMaskToCIDR["192.0.0.0"] = 2
    netMaskToCIDR["224.0.0.0"] = 3
    netMaskToCIDR["240.0.0.0"] = 4
    netMaskToCIDR["248.0.0.0"] = 5
    netMaskToCIDR["252.0.0.0"] = 6
    netMaskToCIDR["254.0.0.0"] = 7
    netMaskToCIDR["255.0.0.0"] = 8
    netMaskToCIDR["255.128.0.0"] = 9
    netMaskToCIDR["255.192.0.0"] = 10
    netMaskToCIDR["255.224.0.0"] = 11
    netMaskToCIDR["255.240.0.0"] = 12
    netMaskToCIDR["255.248.0.0"] = 13
    netMaskToCIDR["255.252.0.0"] = 14
    netMaskToCIDR["255.254.0.0"] = 15
    netMaskToCIDR["255.255.0.0"] = 16
    netMaskToCIDR["255.255.128.0"] = 17
    netMaskToCIDR["255.255.192.0"] = 18
    netMaskToCIDR["255.255.224.0"] = 19
    netMaskToCIDR["255.255.240.0"] = 20
    netMaskToCIDR["255.255.248.0"] = 21
    netMaskToCIDR["255.255.252.0"] = 22
    netMaskToCIDR["255.255.254.0"] = 23
    netMaskToCIDR["255.255.255.0"] = 24
    netMaskToCIDR["255.255.255.128"] = 25
    netMaskToCIDR["255.255.255.192"] = 26
    netMaskToCIDR["255.255.255.224"] = 27
    netMaskToCIDR["255.255.255.240"] = 28
    netMaskToCIDR["255.255.255.248"] = 29
    netMaskToCIDR["255.255.255.252"] = 30
    netMaskToCIDR["255.255.255.254"] = 31
    netMaskToCIDR["255.255.255.255"] = 32
}

#Lines not starting with whitespace
#VLAN0152  Link encap:Ethernet  HWaddr F4:15:63:1D:E4:10
/^[^\s].*Link/{

    interfaceTags["name"] = "if-" $1

    #Match for a mac address.
    if (match($5, /[A-F0-9]{2}:[A-F0-9]{2}:[A-F0-9]{2}:[A-F0-9]{2}:[A-F0-9]{2}:[A-F0-9]{2}/)) {
        writeComplexMetricString("network-interface-mac", interfaceTags, $5, "false")  # Converted to new syntax by change_ind_scripts.py script
    }

    #Add interface state as always up as there is no way to determine the VLAN interface states
    writeDoubleMetric("network-interface-state", interfaceTags, "gauge", 1, "false")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("network-interface-admin-state", interfaceTags, "gauge", 1, "false")  # Converted to new syntax by change_ind_scripts.py script
    next
}

#          inet addr:10.100.13.11  Bcast:10.100.13.255  Mask:255.255.255.0
/inet addr:/{

    ipAddr = $2

    #Remove "addr:" to get the value
    sub(/addr:/, "", ipAddr)
    writeComplexMetricString("network-interface-ipv4-address", interfaceTags, ipAddr, "false","name")  # Converted to new syntax by change_ind_scripts.py script

    netMask = $NF
    #Remove "Mask:" to get the value
    sub(/Mask:/, "", netMask)

    #Write the metric with the value from the netMaskToCIDR array
    CIDRMask = netMaskToCIDR[netMask]
    writeComplexMetricString("network-interface-ipv4-subnet", interfaceTags, CIDRMask, "false","name")  # Converted to new syntax by change_ind_scripts.py script
    next
}

#          UP BROADCAST RUNNING MULTICAST  MTU:9282  Metric:1
/\s+MTU:.+\s+Metric/{

    MTU = $(NF-1)

    #Remove "MTU:" to get the value
    sub(/MTU:/, "", MTU)
    writeComplexMetricString("network-interface-mtu", interfaceTags, MTU, "false")  # Converted to new syntax by change_ind_scripts.py script
    next
}

#          RX packets:9075120 errors:0 dropped:0 overruns:0 frame:0
/^\s+RX\s+packets:/{

    packetsCount = $2

    #Remove "packets:" to get the value
    sub(/packets:/, "", packetsCount)
    writeDoubleMetric("network-interface-rx-packets", interfaceTags, "counter", packetsCount, "false")  # Converted to new syntax by change_ind_scripts.py script

    errorsCount = $3

    #Remove "errors:" to get the value
    sub(/errors:/, "", errorsCount)
    writeDoubleMetric("network-interface-rx-errors", interfaceTags, "gauge", errorsCount, "false")  # Converted to new syntax by change_ind_scripts.py script

    droppedCount = $4

    #Remove "dropped:" to get the value
    sub(/dropped:/, "", droppedCount)
    writeDoubleMetric("network-interface-rx-dropped", interfaceTags, "counter", droppedCount, "false")  # Converted to new syntax by change_ind_scripts.py script

    overrunsCount = $5

    #Remove "overruns:" to get the value
    sub(/overruns:/, "", overrunsCount)
    writeDoubleMetric("network-interface-rx-overruns", interfaceTags, "gauge", overrunsCount, "false")  # Converted to new syntax by change_ind_scripts.py script

    frameCount = $6

    #Remove "frame:" to get the value
    sub(/frame:/, "", frameCount)
    writeDoubleMetric("network-interface-rx-frame", interfaceTags, "gauge", frameCount, "false")  # Converted to new syntax by change_ind_scripts.py script
    next
}

#          TX packets:227082071 errors:0 dropped:0 overruns:0 carrier:0
/^\s+TX\s+packets:/{

    packetsCount = $2

    #Remove "packets:" to get the value
    sub(/packets:/, "", packetsCount)
    writeDoubleMetric("network-interface-tx-packets", interfaceTags, "gauge", packetsCount, "false")  # Converted to new syntax by change_ind_scripts.py script

    errorsCount = $3

    #Remove "errors:" to get the value
    sub(/errors:/, "", errorsCount)
    writeDoubleMetric("network-interface-tx-errors", interfaceTags, "gauge", errorsCount, "false")  # Converted to new syntax by change_ind_scripts.py script

    droppedCount = $4

    #Remove "dropped:" to get the value
    sub(/dropped:/, "", droppedCount)
    writeDoubleMetric("network-interface-tx-dropped", interfaceTags, "gauge", droppedCount, "false")  # Converted to new syntax by change_ind_scripts.py script

    overrunsCount = $5

    #Remove "overruns:" to get the value
    sub(/overruns:/, "", overrunsCount)
    writeDoubleMetric("network-interface-tx-overruns", interfaceTags, "gauge", overrunsCount, "false")  # Converted to new syntax by change_ind_scripts.py script

    carrierCount = $6

    #Remove "carrier:" to get the value
    sub(/carrier:/, "", carrierCount)
    writeDoubleMetric("network-interface-tx-carrier", interfaceTags, "gauge", carrierCount, "false")  # Converted to new syntax by change_ind_scripts.py script
    next
}

#          RX bytes:165055284655 (153.7 GiB)  TX bytes:406066635367 (378.1 GiB)
/^\s+RX\s+bytes:/{

    rxBytesCount = $2

    #Remove "bytes:" to get the value
    sub(/bytes:/, "", rxBytesCount)
    writeDoubleMetric("network-interface-rx-bits", interfaceTags, "counter", rxBytesCount * 8, "true", "Network Interfaces - RX Bits", "bits","name")  # Converted to new syntax by change_ind_scripts.py script

    txBytesCount = $6

    #Remove "bytes:" to get the value
    sub(/bytes:/, "", txBytesCount)
    writeDoubleMetric("network-interface-tx-bits", interfaceTags, "counter", txBytesCount * 8, "true", "Network Interfaces - TX Bits", "bits","name")  # Converted to new syntax by change_ind_scripts.py script
    next
}

# support for new ifconfig format without ":"

#VLAN_HA: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
/^[^\s]/{
    # remove last ":"
    interfaceTags["name"] = "if-" substr($1, 1, length($1)-1)

    # admin state: UP/DOWN
    admin_state = 0
    if ($0 ~ "<UP,")
        admin_state = 1
    writeDoubleMetric("network-interface-admin-state", interfaceTags, "gauge", admin_state, "false")

    writeComplexMetricString("network-interface-mtu", interfaceTags, $(NF), "false")
    next
}

#        inet 10.11.171.171  netmask 255.255.255.0  broadcast 10.11.171.255
/^\s.*inet\s/ {
    writeComplexMetricString("network-interface-ipv4-address", interfaceTags, $2, "false")

    netMask = $4
    CIDRMask = netMaskToCIDR[netMask]
    writeComplexMetricString("network-interface-ipv4-subnet", interfaceTags, CIDRMask, "false")  # Converted to new syntax by change_ind_scripts.py script
    next
}

#        ether 00:50:56:91:1b:25  txqueuelen 1000  (Ethernet)
/^\s.*ether\s/ {
    if (match($2, /[A-F0-9]{2}:[A-F0-9]{2}:[A-F0-9]{2}:[A-F0-9]{2}:[A-F0-9]{2}:[A-F0-9]{2}/)) {
        writeComplexMetricString("network-interface-mac", interfaceTags, $2, "false")
    }
}

#        RX packets 81647  bytes 5961875 (5.6 MiB)
/^\s.*RX packets\s/ {
    writeDoubleMetric("network-interface-rx-packets", interfaceTags, "counter", $3, "false")
    writeDoubleMetric("network-interface-rx-bits", interfaceTags, "counter", $5 * 8, "true", "Network Interfaces - " $1 " Bits", "bits","name")
    next
}

#        TX packets 81722  bytes 8212883 (7.8 MiB)
/^\s.*TX packets\s/ {
    writeDoubleMetric("network-interface-tx-packets", interfaceTags, "counter", $3, "false")
    writeDoubleMetric("network-interface-tx-bits", interfaceTags, "counter", $5 * 8, "true", "Network Interfaces - " $1 " Bits", "bits","name")
    next
}

#        RX errors 0  dropped 0  overruns 0  frame 0
/^\s.*RX errors\s/ {
    writeDoubleMetric("network-interface-rx-errors", interfaceTags, "gauge", $3, "false")
    writeDoubleMetric("network-interface-rx-dropped", interfaceTags, "counter", $5, "false")
    writeDoubleMetric("network-interface-rx-overruns", interfaceTags, "gauge", $7, "false")
    writeDoubleMetric("network-interface-rx-frame", interfaceTags, "gauge", $9, "false")
    next
}

#        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
/^\s.*TX errors\s/ {
    writeDoubleMetric("network-interface-tx-errors", interfaceTags, "gauge", $3, "false")
    writeDoubleMetric("network-interface-tx-dropped", interfaceTags, "counter", $5, "false")
    writeDoubleMetric("network-interface-tx-overruns", interfaceTags, "gauge", $7, "false")
    writeDoubleMetric("network-interface-tx-carrier", interfaceTags, "gauge", $9, "false")
    writeDoubleMetric("network-interface-tx-collisions", interfaceTags, "gauge", $11, "false")
    next
}