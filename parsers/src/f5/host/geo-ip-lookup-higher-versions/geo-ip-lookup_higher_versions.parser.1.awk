#The GeoIP database is used to take decisions based on client geographical location, ISP etc.
#This data is not automatically updated and requires manual updates using packages from download.f5.com.

#A failed database would cause problems when using lookups.
#An outdated database could place a customer in a country where they are not situated and cause problems in ie. irules.

#Remediation:
#Re-install/update the database
#https://support.f5.com/csp/#/article/K11176

#Updates are released, but not at a set interval so I would count a database being 3 months old too old.

#---BEGINRECORD---
/---BEGINRECORD---/{
    #Resetting tags and variables for geoip database version
    geoIPDatabaseVersionTags["database"] = ""
    geoIPDatabaseVersionTags["im.dstype.displayType"] = "date"
    versiondate = ""
    
    #Resetting tags and variables for geoip database state
    geoIPDatabaseStateTags["im.dstype.displayType"] = "state"
    result = 0
}

#GeoIPDatabase: /shared/GeoIP/F5GeoIP.dat
/^GeoIPDatabase:/{
    geoIPDatabaseVersionTags["database"] = $2
    geoIPDatabaseStateTags["database"] = $2
}

#country_name = United States
#name = google inc.
/(^country_name|^name =|version)/{
    #Got a result
    result = 1
}

#size of geoip database = 2593066, segments = 420692, version = Copyright (c) F5 Networks Inc, All Rights Reserved GEOIP2 v1, 20181112
/version/{
    
    if(NF > 12){
    
        versiondate = $22
        
        if(match(versiondate,/^[\d]{8}$/)){
            #20161206
            year = versiondate
            month = versiondate
            day = versiondate
            
            #Remove the last 4 digits to get the year
            sub(/[\d]{4}$/,"",year)
            
            #Remove the first 4 digits to get month and day
            sub(/^[\d]{4}/,"",month)
            
            #Remove the last two to get the month
            #1206
            sub(/[\d]{2}$/,"",month)
            
            #Remove the first 6 digits to get the day
            sub(/^[\d]{6}/,"",day)
            
            versiondate = date(year,month,day)
        }
    }
}

#---ENDRECORD---
/---ENDRECORD---/{
    #Verifying that we have a date and writing metric if we do
    if(versiondate != ""){
        #Writing metric for when the database was alst updated
        writeDoubleMetric("geoip-database-version", geoIPDatabaseVersionTags, "gauge", versiondate, "false")  # Converted to new syntax by change_ind_scripts.py script
    }
    
    #Verifying that a result was detected
    if(result == 1){
        writeDoubleMetric("geoip-database-state", geoIPDatabaseStateTags, "gauge", 1, "false")  # Converted to new syntax by change_ind_scripts.py script
    } else {
        writeDoubleMetric("geoip-database-state", geoIPDatabaseStateTags, "gauge", 0, "false")  # Converted to new syntax by change_ind_scripts.py script
    }
    
    
}