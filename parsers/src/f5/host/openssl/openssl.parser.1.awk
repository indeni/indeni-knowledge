#        Issuer: C=--, ST=WA, L=Seattle, O=MyCompany, OU=MyOrg, CN=localhost.localdomain/emailAddress=root@localhost.localdomain
/^\s+Issuer:/{

    if(match($0, /.+?CN=.*localhost.*$/)){
        writeComplexMetricString("default-management-certificate-used", null, "true", "false")  # Converted to new syntax by change_ind_scripts.py script
    } else {
        writeComplexMetricString("default-management-certificate-used", null, "false", "false")  # Converted to new syntax by change_ind_scripts.py script
    }
    
}