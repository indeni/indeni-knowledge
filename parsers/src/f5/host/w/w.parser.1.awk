# 18:37:53 up 18 days,  8:54,  2 users,  load average: 0.35, 0.28, 0.20
#USER     TTY      FROM              LOGIN@   IDLE   JCPU   PCPU WHAT
#user1    pts/0    10.10.10.11       Thu17    1.00s  1.96s  0.06s w
#user2    pts/1    10.10.10.12       Wed09    3days  0.26s  0.26s -bash

/^(?!\s|USER\s+TTY\s+FROM\s+LOGIN@\s+IDLE\s+JCPU\s+PCPU\s+WHAT)/ {

	username=$1
	from=$3
	idle=$5
	tty=$2	
	iuser++
	users[iuser, "username"]=username
	users[iuser, "from"]=from
	if ( from == "-" ) {
		users[iuser, "from"]="console"
	}
	# Here we count number of seconds of idle time
	# w uses several time formats
	# DDdays, HH:MMm, MM:SS, SS.CCs
	
	# 2days
	if ( idle ~ /days/ ) {
		
		split (idle,idleArr,"days")
		secondsIdle=idleArr[1] * 86400
		#secondsIdle = 501	

	# 7:13m
	} else if ( idle ~ /m/ ) {
		split (idle,idleArr,"m")
		split (idleArr[1],idleArr,":")
		secondsIdle = idleArr[1] * 3600 + idleArr[2] * 60
		#secondsIdle = 502

	# 11.00s
	} else if ( idle ~ /s/ ) {
		split (idle,idleArr,"\\.")
		secondsIdle = idleArr[1]
		#secondsIdle = 503		

	} else if ( idle ~ /:/ ) {
		split (idle,idleArr,":")
		secondsIdle = idleArr[1] * 60 + idleArr[2]
		#secondsIdle = 504
	}

	users[iuser, "idle"]=secondsIdle
}

#{
#  "username" : "user1",
#  "idle" : "1",
#  "from" : "10.10.10.11"
#}, {
#  "idle" : "259200",
#  "from" : "10.10.10.12",
#  "username" : "user2"
#} ]

END {
   writeComplexMetricObjectArray("logged-in-users", null, users, "false")  # Converted to new syntax by change_ind_scripts.py script
}