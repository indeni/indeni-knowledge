BEGIN {
    f5_out_of_memory_warning = 1
    # We set variable to 1 (no alert as default), /var/log/kern.log file rotates every day
    # if not error message is found in the logs, then alert will clear
}


# warning kernel: httpd invoked oom-killer: gfp_mask=0x201da, order=0, oom_adj=0, oom_score_adj=0
/invoked oom-killer/ {
    f5_out_of_memory_warning = 0
    exit
}

# err kernel: Out of memory: Kill process 13579 (java) score 90 or sacrifice child
/Out of memory/ {
    f5_out_of_memory_warning = 0
    exit
}

END {
    writeDoubleMetric("f5-out-of-memory-warning", null, "gauge", f5_out_of_memory_warning, "false") 
}