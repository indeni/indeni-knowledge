#Source article about the mcdp reload flag
#https://support.f5.com/csp/article/K13030

BEGIN{
state = 0
}

#/service/mcpd/forceload
/^\/service\/mcpd\/forceload$/{
    state = 1
}

END{
	debugTags["name"] = "mcpd-force-reload"
    writeDoubleMetric("debug-status", debugTags, "gauge", state, "false")  # Converted to new syntax by change_ind_scripts.py script
}