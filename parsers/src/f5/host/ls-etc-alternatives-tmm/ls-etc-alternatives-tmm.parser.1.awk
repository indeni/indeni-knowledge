#Source article about enabling/disabling this debug process
#https://support.f5.com/kb/en-us/solutions/public/11000/400/sol11490.html

#lrwxrwxrwx. 1 root webusers 18 2016-11-06 12:52 /etc/alternatives/tmm -> /usr/bin/tmm.debug
/tmm/{
	
	debugTags["name"] = "tmm-process"
	
	if($NF == "/usr/bin/tmm.debug"){	
		status = 1
	} else {
		status = 0
	}
	writeDoubleMetric("debug-status", debugTags, "gauge", status, "true", "TMM Debug", "state", "")  # Converted to new syntax by change_ind_scripts.py script
}