#Vcmp::Global
/^Vcmp::Global$/{
    section = "global"
}

#Vcmp::VirtualDisk
/^Vcmp::VirtualDisk$/{
    section="virtualDisk"
}

#262601088.0
/^[\d\.]+$/{
    if(section == "global"){
        totalFreeDiskSpace = $1
    }
}

#mylb1.domain.local.img                tmos  in-use        12962156.0
/^.+[\d\.]+$/{
    if(section == "virtualDisk"){
    
        guestsArray[$1] = $NF
        
        allocatedDiskTags["name"] = $1
        
        #Write metric with disk space in kilobytes as value
        writeDoubleMetric("f5-vcmp-guest-used-disk-kbytes", allocatedDiskTags, "gauge", $NF, "true", "vCMP guest disk use", "kilobytes", "name")  # Converted to new syntax by change_ind_scripts.py script
    
    }
}

END {
    
    totalDiskSpace = totalFreeDiskSpace
    
    #Add used disk space to get the total available disk space
    for(guest in guestsArray){
        totalDiskSpace += guestsArray[guest]
    }
    
    usedDiskSpace = totalDiskSpace - totalFreeDiskSpace
    
    diskUsage = usedDiskSpace/(totalDiskSpace)*100

    writeDoubleMetric("f5-vcmp-host-disk-used-kbytes", null, "gauge", usedDiskSpace, "false")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("f5-vcmp-host-disk-total-kbytes", null, "gauge", totalDiskSpace, "false")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("f5-vcmp-host-disk-usage-percentage", null, "gauge", diskUsage, "true", "vCMP host disk usage", "percentage")  # Converted to new syntax by change_ind_scripts.py script

}