name: f5-rest-net-interface-stats
description: Determine network interface metrics
type: monitoring
monitoring_interval: 1 minute
requires:
    vendor: f5
    product: load-balancer
    rest-api: 'true'
comments:
    network-interface-state:
        why: |
            Interfaces in the "down" state could result in downtime or reduced redundancy.
        how: |
            The state of the interface is retrieved via the F5 iControl REST API.
        can-with-snmp: true
        can-with-syslog: false
    network-interface-admin-state:
        why: |
            An administrator might set a network interface to be disabled for troubleshooting, but. Should he he forget about doing this network trunks might be running at reduced capacity.
        how: |
            This alert uses the F5 iControl REST API to retrieve the status of all physical network interfaces. In that output, it looks for interfaces that are set to be up, but are actually down.
        can-with-snmp: true
        can-with-syslog: false
    network-interface-mac:
        why: |
            To be able to search for MAC addresses in indeni, this data needs to be stored.
        how: |
            This alert uses the F5 iControl REST API to retrieve the MAC addresses of all physical network interfaces.
        can-with-snmp: true
        can-with-syslog: false
    network-interface-mtu:
        why: |
            The MTU sometimes needs to be adjusted. Storing this gives an administrator an easy way to view the MTU from a large number of devices, as well as identifying incorrectly set MTU.
        how: |
            This alert uses the F5 iControl REST API to retrieve the MTU from all physical interfaces.
        can-with-snmp: true
        can-with-syslog: false
    network-interface-rx-packets:
        why: |
            Tracking the number of packets flowing through each network interface is important to identify potential issues, spikes in traffic, etc.
        how: |
            This alert logs into the F5 unit through the F5 iControl REST API and retrieves the metrics from all network interfaces. In that output, it looks for the number of packets received through the interface.
        can-with-snmp: true
        can-with-syslog: false
    network-interface-tx-packets:
        why: |
            Tracking the number of packets flowing through each VLAN interface is important to identify potential issues, spikes in traffic, etc.
        how: |
            This alert logs into the F5 unit through the F5 iControl REST API and retrieves the metrics from all network interfaces. In that output, it looks for the number of packets transmitted through the interface.
        can-with-snmp: true
        can-with-syslog: false
    network-interface-rx-dropped:
        why: |
            If incoming packets are being dropped on a network interface, it is important to be aware of it. This may be due to a high load on the unit, or another capacity issue.
        how: |
            This alert logs into the F5 unit through the F5 iControl REST API and retrieves the metrics from all network interfaces. In that output, it looks for the number of packets dropped on an interface.
        can-with-snmp: true
        can-with-syslog: true
    network-interface-tx-dropped:
        why: |
            If outgoing packets are being dropped on a network interface, it is important to be aware of it. This may be due to a high load on the unit, or another capacity issue.
        how: |
            This alert logs into the F5 unit through the F5 iControl REST API and retrieves the metrics from all network interfaces. In that output, it looks for the number of packets dropped on an interface.
        can-with-snmp: true
        can-with-syslog: false
    network-interface-rx-bits:
        why: |
            Tracking the amount of data flowing through each network interface is important to identify potential issues, spikes in traffic, etc.
        how: |
            This alert logs into the F5 unit through the F5 iControl REST API and retrieves the metrics from all network interfaces. In that output, it looks for the number of bits received through the interface.
        can-with-snmp: true
        can-with-syslog: false
    network-interface-tx-bits:
        why: |
            Tracking the amount of data flowing through each network interface is important to identify potential issues, spikes in traffic, etc.
        how: |
            This alert logs into the F5 unit through the F5 iControl REST API and retrieves the metrics from all network interfaces. In that output, it looks for the number of bits transmitted through the interface.
        can-with-snmp: true
        can-with-syslog: false
    network-interface-tx-errors:
        why: |
            Transmit errors on an interface could indicate a problem.
        how: |
            This alert logs into the F5 unit through the F5 iControl REST API and retrieves the metrics from all network interfaces. In that output, it looks for the number of transmit errors on each interface.
        can-with-snmp: true
        can-with-syslog: false
    network-interface-rx-errors:
        why: |
            Receive errors on an interface could indicate a problem.
        how: |
            This alert logs into the F5 unit through the F5 iControl REST API and retrieves the metrics from all network interfaces. In that output, it looks for the number of receive errors on each interface.
        can-with-snmp: true
        can-with-syslog: false
    network-interface-speed:
        why: |
            Generally, these days network interfaces are set at 1Gbps or more. Sometimes, due to a configuration or device issue, an interface can be set below that (to 100mbps or even 10mbps). As that is usually _not_ the intended behavior, it is important to track the speed of all network interfaces.
        how: |
            This alert logs into the F5 unit through the iControl REST API and retrieves the status of all network interfaces. In that output, it looks for the actual runtime speed of each interface.
        can-with-snmp: true
        can-with-syslog: false
    network-interface-duplex:
        why: |
            Generally, these days network interfaces are set at full duplex. Sometimes, due to a configuration or device issue, an interface can be set to half duplex. As that is usually _not_ the intended behavior, it is important to track the duplex setting of all network interfaces.
        how: |
            This alert logs into the F5 unit through the iControl REST API and retrieves the status of all network interfaces. In that output, it looks for the actual runtime duplex of each interface.
        can-with-snmp: true
        can-with-syslog: false
    network-interface-type:
        why: |
            The type of interface can be useful for administrators.
        how: |
            This alert logs into the F5 unit through the iControl REST API and retrieves the status of all network interfaces. In that output, it looks for the actual runtime duplex of each interface.
        can-with-snmp: true
        can-with-syslog: false
steps:
-   run:
        type: HTTP
        command: /mgmt/tm/net/interface/stats?options=all-properties&$select=tmName,status,macAddress,mtu,counters.bitsIn,counters.bitsOut,counters.pktsIn,counters.pktsOut,counters.dropsIn,counters.dropsOut,counters.errorsIn,counters.errorsOut,mediaActive
    parse:
        type: JSON
        file: rest-mgmt-tm-net-interface.parser.1.json.yaml
