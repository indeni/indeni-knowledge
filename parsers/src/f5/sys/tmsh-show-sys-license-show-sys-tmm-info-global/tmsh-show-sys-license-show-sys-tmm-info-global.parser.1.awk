#perf_http_compression_Mbps
/^perf_http_compression_Mbps \[/ {
	
    maxCompressionThroughput = $2
    
	#Remove the brackets
	sub(/\[/,"", maxCompressionThroughput)
	sub(/\]/,"", maxCompressionThroughput)
	
    if(match(maxCompressionThroughput, /^\d+$/)){
    
        licenseElementsTag["name"] = "Maximum Compression Throughput (Mbps)"
        writeDoubleMetric("license-elements-limit", licenseElementsTag, "gauge", maxCompressionThroughput, "true", "License Limitation", "number", "name")  # Converted to new syntax by change_ind_scripts.py script
        
    }
    
}

#perf_VE_throughput_Mbps [10]
/^perf_VE_throughput_Mbps \[/ {
	
    maxThroughput = $2
    
	#Remove the brackets
	sub(/\[/,"", maxThroughput)
	sub(/\]/,"", maxThroughput)
    
    if(match(maxThroughput, /^\d+$/)){
    
        if(maxThroughput != 1){
        
            licenseElementsTag["name"] = "VE Edition Maximum Throughput (Mbps)"
            writeDoubleMetric("license-elements-limit", licenseElementsTag, "gauge", maxThroughput, "true", "License Limitation", "number", "name")  # Converted to new syntax by change_ind_scripts.py script
            
        }
        
    }
    
}

#perf_SSL_total_TPS [500]
/^perf_SSL_total_TPS \[/ {
	
    tpsPerCore = $2
    
	#Remove the brackets
	sub(/\[/,"", tpsPerCore)
	sub(/\]/,"", tpsPerCore)
    
}

#  TMM Count               2
/TMM Count/ {

    if(match(tpsPerCore, /^\d+$/)){
    
        licenseElementsTag["name"] = "SSL TPS"
        
        #License limit is multiplied by the tmm cores
        #  TMM Count               2
        
        tpsLimit = tpsPerCore * $3
        writeDoubleMetric("license-elements-limit", licenseElementsTag, "gauge", tpsLimit, "true", "License Limitation", "number", "name")  # Converted to new syntax by change_ind_scripts.py script
        
    }
    
}

#Service Check Date       2016/09/19
/Service Check Date/ {
    
    #Latest version service activation date
    #Found here: https://support.f5.com/csp/article/K7727
    
    latestServiceCheckDate = date("2019", "05", "03")
        
	#Split the date into an array
	split($4,serviceCheckDateArray, "/")
	
	year = serviceCheckDateArray[1]
	month = serviceCheckDateArray[2]
	day = serviceCheckDateArray[3]
    
    unitServiceCheckDatedate = date(year, month, day)
    
    #Check if the latest versions service check date is later than the units is
    if(latestServiceCheckDate > unitServiceCheckDatedate){
    
        status = 0
        
    } else {
    
        status = 1
    
    }
	
	writeDoubleMetric("f5-service-check", null, "gauge", status, "false")  # Converted to new syntax by change_ind_scripts.py script
    
}