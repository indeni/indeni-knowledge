BEGIN {
    iHA = 0
}

#Sys::HA Group: trunk-health
/Sys::HA Group: /{
    iPool = 0
    iTrunk = 0
    
    #Save the HA-group name for later use
    haGroupName = $NF
}

#State         disabled
/^State\s+(disabled|enabled)$/{

    state = $2
    
    #If the state is enabled we will go ahead and increase the iterator and set the name
    if(state == "enabled"){
        iHA++
        haGroupArray[iHA, "name"] = haGroupName
    }
}

#Active Bonus      100
/^Active Bonus\s+[0-9]+$/{
    activeBonus = $NF
}

#  | Sys::HA Group Trunk: trunk-health:External
/^\s+\| Sys::HA Group Trunk:/{

    iTrunk++
    
    trunkName = $NF
    
    #  | Sys::HA Group Trunk: trunk-health:External
    sub(/.*:/,"",trunkName)
    
    arrayKey = "Trunk " iTrunk
    arrayValue = "Trunk name: " trunkName

}

#  | Sys::HA Group Pool: trunk-health:
/^\s+\| Sys::HA Group Pool: trunk-health:/{

    iPool++
    
    poolName = $NF
    sub(/.*:/,"",poolName)
    
    arrayKey = "Pool " iPool
    arrayValue = poolName
    
}

#  | Threshold             0
/\s+\|\s+Threshold\s+[0-9]+$/{
    arrayValue = arrayValue " - Threshold: " $NF
}

#  | Percent Up          100
/\s+\|\s+Percent Up\s+[0-9]+$/{
    arrayValue = arrayValue " - Percent up: " $NF
}

#  | Weight               25
/\s+\|\s+Weight\s+[0-9]+$/{
    
    #There is no need to write disabled ha-groups
    if(state == "enabled"){
        arrayValue = arrayValue " - Weight: " $NF
        haGroupArray[iHA, arrayKey] = arrayValue
        
        #Add the active bonus extracted earlier
        haGroupArray[iHA, "active-bonus"] = activeBonus
    }
}

END{
    writeComplexMetricObjectArray("f5-ha-group", null, haGroupArray, "false")  # Converted to new syntax by change_ind_scripts.py script
}