#Cipher strings in an F5 can be comprised of a list of ciphers, or internal keywords such as DEFAULT, STRONG, WEAK etc
#Since the meaning of keywords changes between versions it's a bit of a trouble to write a script that matches all versions
#Instead, this script utilized F5's built-in command "tmm --clientciphers", which translates a cipher string to the actual
#ciphers behind the keywords

#12:    22  DHE-RSA-DES-CBC3-SHA             168  TLS1    Native  DES       SHA     EDH/RSA
/DES-CBC3/{
    #Ciphers vulnerable to Sweet32 detected
    sweet32Ciphers = 1
}

/SSL3/{
    #Make sure we're in the table by counting number of fields
    if($NF > 2){
        SSLv3 = 1
    }
}

/SSL2/{
    #Make sure we're in the table by counting number of fields
    if($NF > 2){
        SSLv2 = 1
    }
}

/RC4/{
    #Make sure we're in the table by counting number of fields
    if($NF > 2){
        RC4 = 1
    }
}

END {
        
    #Sweet32 uses long lived connections with enabled renegotiations
    #To mititgate Sweet32 renegotiations must be disabled, or either:
    #Rrenegotiation size set to less than 1000
    #The affected ciphers not present
    
    #Judging from F5's article about the matter renegotiation is always on on the management interface.
    #I have found no way of turning it off though, nor regulate the size

    if(sweet32Ciphers == 1){
        sweet32Tags["name"] = "httpd"
        sweet32Tags["type"] = "Management interface"
        sweet32Tags["vulnerability"] = "SWEET32"
        sweet32Tags["cipher"] = "DES-CBC3"
        writeDoubleMetric("ssl-weak-cipher", sweet32Tags, "gauge", 1, "false")  # Converted to new syntax by change_ind_scripts.py script
    }
    
    #SSLv3 is consider not only weak, but more or less compromised (ie. Poodle)
    if(SSLv3 == 1){
        sslv3Tags["name"] = "httpd"
        sslv3Tags["type"] = "Management interface"
        sslv3Tags["protocol"] = "SSLv3"
        sslv3Tags["vulnerability"] = "POODLE"
        writeDoubleMetric("ssl-weak-protocol", sslv3Tags, "gauge", 1, "false")  # Converted to new syntax by change_ind_scripts.py script
    }
    
    #SSLv2 is considered compromised (Drown)
    if(SSLv2 == 1){
        sslv2Tags["name"] = "httpd"
        sslv2Tags["type"] = "Management interface"
        sslv2Tags["protocol"] = "SSLv2"
        sslv2Tags["vulnerability"] = "DROWN"
        writeDoubleMetric("ssl-weak-protocol", sslv2Tags, "gauge", 1, "false")  # Converted to new syntax by change_ind_scripts.py script
    }
    
    #RC4 is considered a weak cipher and there has been indications that is has
    #been compromised by certain government agencies
    if(RC4 == 1){
        rc4Tags["name"] = "httpd"
        rc4Tags["type"] = "Management interface"
        rc4Tags["cipher"] = "RC4"
        rc4Tags["vulnerability"] = "Bar Mitzvah"
        writeDoubleMetric("ssl-weak-cipher", rc4Tags, "gauge", 1, "false")  # Converted to new syntax by change_ind_scripts.py script
    }
}