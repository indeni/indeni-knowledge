#Client Bits In                        132557004  129647133                     183154812
/Client Bits In/{
    clientBitsIn = $4
}

#Client Bits Out                       603210992  488761174                     876960406
/Client Bits Out/{
    clientBitsOut = $4
}

#Server Bits In                        133686411  130672797                     184317636
/Server Bits In/{
    serverBitsIn = $4
}

#Server Bits Out                       595569427  487756939                     871842236
/Server Bits Out/{
    serverBitsOut = $4
}


#SSL TPS                              546        512                           596
/^SSL TPS/ {
    
    section="ssltransactions"
    
	licenseElementsTag["name"] = "SSL TPS"
	writeDoubleMetric("license-elements-used", licenseElementsTag, "gauge", $3, "true", "License Utilization", "number", "name")  # Converted to new syntax by change_ind_scripts.py script
}

END {
    
    # License limits are enforced by the larger of the two values measured from
    # combining Client Bits In plus Server Bits Out or combining Client Bits Out
    # plus Server Bits In.
    
    clientToServerSide = clientBitsIn + serverBitsOut
    serverToClientSide = clientBitsOut + serverBitsIn
    
    licenseElementsTag["name"] = "VE Edition Maximum Throughput (Mbps)"
    
    if(clientToServerSide>serverToClientSide){
        usageValue = clientToServerSide/1000000
    } else {
        usageValue = serverToClientSide/1000000
    }
    
    writeDoubleMetric("license-elements-used", licenseElementsTag, "gauge", usageValue, "true", "License Utilization", "number", "name")  # Converted to new syntax by change_ind_scripts.py script
    
}