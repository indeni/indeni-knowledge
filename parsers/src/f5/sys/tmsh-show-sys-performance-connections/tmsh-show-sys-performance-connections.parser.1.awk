BEGIN{
    connsPerSec = 0
}

/^Connections/{
    writeDoubleMetric("concurrent-connections", null, "number", $2, "false")  # Converted to new syntax by change_ind_scripts.py script
}

/^(Client|Server) Connections/{
    connsPerSec += $3
}

END{
    writeDoubleMetric("connections-per-second", null, "number", connsPerSec, "false")  # Converted to new syntax by change_ind_scripts.py script
}