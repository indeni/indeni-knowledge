#!/bin/bash
tmsh -q -c "list sys syslog" | grep -P "(include|host)" | grep -Po "[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}"