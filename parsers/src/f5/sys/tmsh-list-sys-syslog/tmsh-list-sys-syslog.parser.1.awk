BEGIN {
    iSyslog = 0
}

#192.168.1.3
/^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$/{
    iSyslog++
    syslogArray[iSyslog, "host"] = $1
}

END {
    writeComplexMetricObjectArray("syslog-servers", null, syslogArray, "false")  # Converted to new syntax by change_ind_scripts.py script
}