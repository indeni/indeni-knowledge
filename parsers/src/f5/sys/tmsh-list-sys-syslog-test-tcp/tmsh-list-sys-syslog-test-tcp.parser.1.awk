#Explanation of the command
#1. First get the output of list sys sylog
#2. Send it to grep which would return "10.0.0.10\" port (514)"
#3. Remove '\" port (' and ')"' using sed
#4. Test the syslog server using netcat

{
    writeDebug($0)
}

#nc: connect to 10.0.0.10 port 514 (tcp) timed out: Operation now in progress
/timed out/{

    ip = $4
    port = $6
    ipPort = ip ":" port
    
    syslogTags["name"] = ipPort
    
    writeDoubleMetric("tcp-syslog-state", syslogTags, "gauge", 0, "false")  # Converted to new syntax by change_ind_scripts.py script
    
}

#nc: connect to 192.168.197.16 port 95 (tcp) failed: Connection refused
/Connection refused/{

    ip = $4
    port = $6
    ipPort = ip ":" port
    
    syslogTags["name"] = ipPort
    
    writeDoubleMetric("tcp-syslog-state", syslogTags, "gauge", 0, "false")  # Converted to new syntax by change_ind_scripts.py script
    
}

#Connection to 192.168.197.16 80 port [tcp/http] succeeded!
/succeeded!/{

    ip = $3
    port = $4
    ipPort = ip ":" port
    
    syslogTags["name"] = ipPort
    
    writeDoubleMetric("tcp-syslog-state", syslogTags, "gauge", 1, "false")  # Converted to new syntax by change_ind_scripts.py script
}