BEGIN {
    timeZone = ""
}

#    servers { 192.168.70.1 pool.ntp.org }
/^\s+servers/ {

    configurationLine = $0

    #    servers { 192.168.70.1 pool.ntp.org }
    sub(/^\s+servers\s\{\s/, "", configurationLine )
    
    #192.168.70.1 pool.ntp.org }
    sub(/\s\}/, "", configurationLine)
    
    #192.168.70.1 pool.ntp.org
    split(configurationLine, ntpServerArr, /\s/)

    #["192.168.70.1", "pool.ntp.org"]
    for (iNTP in ntpServerArr) {
        ntpServers[iNTP, "ipaddress"] = ntpServerArr[iNTP]
        ntpServers[iNTP, "version"] = 4
        ntpServers[iNTP, "type"] = "N/A"
    }

}

#    timezone Europe/Stockholm
/^\s+timezone/ {

    timeZone = $2

}

END {

    #If America/Los Angeles is specified as time zone the tmsh command would not list any timezone.
    #Thus we need to set this manually if the timeZone variable is empty.
    if (timeZone == "") {
        timeZone = "America/Los Angeles"
    }
    
    writeComplexMetricString("timezone", null, timeZone, "true", "Timezone")  # Converted to new syntax by change_ind_scripts.py script
    writeComplexMetricObjectArray("ntp-servers", null, ntpServers, "true", "NTP Servers Defined")  # Converted to new syntax by change_ind_scripts.py script
    
}