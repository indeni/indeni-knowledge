# Example:
# Sys::Raid::Array:
#1 WD-WCAT18586780 HD2 yes failed
#2 WD-WCAT1E733419 HD1 yes ok
# or if theres a RAID with only 1 drive active:
#1 WD-WCAT18586780 HD2 yes ok

BEGIN {
    sys_raid_response = -1
    raid_status = -1
}

/Sys::Raid::Array:/ {
    sys_raid_response = 1
}

/^\d{0,2}\s/ {
    # This will check if Array member is set to yes, if so it will add it to a counter that we use afterwards
    if ($4 == "yes") {
        array_member++
    }
    if ($NF != "ok" || raid_status == 0) {
        raid_status = 0
    } else {
        raid_status = 1
    }
}

END {
    # If there is a raid configured, and one drive is missing/dead and tmsh still says status is ok, it will alert
    if ( (array_member == 1) && (raid_status == 1)) {
        raid_status = 0
    }

    if ((sys_raid_response == 1) && (raid_status != -1)) {
        raidTags["name"] = "RAID"
        writeDoubleMetric("hardware-element-status", raidTags, "state", raid_status, "false")  # Converted to new syntax by change_ind_scripts.py script
    }
}