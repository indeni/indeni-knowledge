BEGIN {
    f5_apm_no_response = 1
    f5_apm_wrong_psk = 1
}

#Nov 19 11:17:33 bigip err apmd[7909]: 01490108:3: /Common/VPN:Common:b6e870d4: RADIUS module: authentication with 'testuser' failed: no response from server  (0)
/failed: no response from server/ {
    #The authentication server is not responding
    f5_apm_no_response = 0
}

#Nov 19 11:41:42 bigip err apmd[7909]: 01490108:3: /Common/VPN:Common:9a937735: RADIUS module: authentication with 'testuser' failed: packet verification failed, most likely the shared secret is not correct  (-1)
/packet verification failed, most likely the shared secret is not correct/ {
    #The pre shared key is not correct
    f5_apm_wrong_psk = 0
}

END {
    tags["im.dstype.displayType"] = "state"
    tags["name"] = "AAA"
    writeDoubleMetric("identity-integration-connection-state", tags, "gauge", f5_apm_no_response, "false")
    writeDoubleMetric("f5-apm-wrong-psk", null, "gauge", f5_apm_wrong_psk, "false") 
}