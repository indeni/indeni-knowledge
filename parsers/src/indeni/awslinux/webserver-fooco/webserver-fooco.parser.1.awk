/,/ {
    split($0, parts, ",")
    tags["name"] = parts[1]
    writeDoubleMetric("fooco-a", tags, "gauge", parts[2], "false")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("fooco-b", tags, "gauge", parts[2], "false")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("fooco-c", tags, "gauge", parts[2], "false")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("fooco-d", tags, "gauge", parts[2], "false")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("fooco-e", tags, "gauge", parts[2], "false")  # Converted to new syntax by change_ind_scripts.py script
}