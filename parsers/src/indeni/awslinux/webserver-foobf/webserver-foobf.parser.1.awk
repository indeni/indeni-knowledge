/,/ {
    split($0, parts, ",")
    tags["name"] = parts[1]
    writeDoubleMetric("foobf-a", tags, "gauge", parts[2], "false")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("foobf-b", tags, "gauge", parts[2], "false")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("foobf-c", tags, "gauge", parts[2], "false")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("foobf-d", tags, "gauge", parts[2], "false")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("foobf-e", tags, "gauge", parts[2], "false")  # Converted to new syntax by change_ind_scripts.py script
}