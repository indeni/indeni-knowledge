/,/ {
    split($0, parts, ",")
    tags["name"] = parts[1]
    writeDoubleMetric("fooar-a", tags, "gauge", parts[2], "false")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("fooar-b", tags, "gauge", parts[2], "false")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("fooar-c", tags, "gauge", parts[2], "false")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("fooar-d", tags, "gauge", parts[2], "false")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("fooar-e", tags, "gauge", parts[2], "false")  # Converted to new syntax by change_ind_scripts.py script
}