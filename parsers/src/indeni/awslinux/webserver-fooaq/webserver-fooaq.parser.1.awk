/,/ {
    split($0, parts, ",")
    tags["name"] = parts[1]
    writeDoubleMetric("fooaq-a", tags, "gauge", parts[2], "false")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("fooaq-b", tags, "gauge", parts[2], "false")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("fooaq-c", tags, "gauge", parts[2], "false")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("fooaq-d", tags, "gauge", parts[2], "false")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("fooaq-e", tags, "gauge", parts[2], "false")  # Converted to new syntax by change_ind_scripts.py script
}