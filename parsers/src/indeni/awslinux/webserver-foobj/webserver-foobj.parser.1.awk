/,/ {
    split($0, parts, ",")
    tags["name"] = parts[1]
    writeDoubleMetric("foobj-a", tags, "gauge", parts[2], "false")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("foobj-b", tags, "gauge", parts[2], "false")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("foobj-c", tags, "gauge", parts[2], "false")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("foobj-d", tags, "gauge", parts[2], "false")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("foobj-e", tags, "gauge", parts[2], "false")  # Converted to new syntax by change_ind_scripts.py script
}