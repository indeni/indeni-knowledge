/,/ {
    split($0, parts, ",")
    tags["name"] = parts[1]
    writeDoubleMetric("foobs-a", tags, "gauge", parts[2], "false")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("foobs-b", tags, "gauge", parts[2], "false")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("foobs-c", tags, "gauge", parts[2], "false")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("foobs-d", tags, "gauge", parts[2], "false")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("foobs-e", tags, "gauge", parts[2], "false")  # Converted to new syntax by change_ind_scripts.py script
}