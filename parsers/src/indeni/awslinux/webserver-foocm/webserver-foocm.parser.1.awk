/,/ {
    split($0, parts, ",")
    tags["name"] = parts[1]
    writeDoubleMetric("foocm-a", tags, "gauge", parts[2], "false")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("foocm-b", tags, "gauge", parts[2], "false")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("foocm-c", tags, "gauge", parts[2], "false")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("foocm-d", tags, "gauge", parts[2], "false")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("foocm-e", tags, "gauge", parts[2], "false")  # Converted to new syntax by change_ind_scripts.py script
}