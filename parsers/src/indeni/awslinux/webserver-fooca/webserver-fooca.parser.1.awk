/,/ {
    split($0, parts, ",")
    tags["name"] = parts[1]
    writeDoubleMetric("fooca-a", tags, "gauge", parts[2], "false")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("fooca-b", tags, "gauge", parts[2], "false")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("fooca-c", tags, "gauge", parts[2], "false")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("fooca-d", tags, "gauge", parts[2], "false")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("fooca-e", tags, "gauge", parts[2], "false")  # Converted to new syntax by change_ind_scripts.py script
}