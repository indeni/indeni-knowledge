/,/ {
    split($0, parts, ",")
    tags["name"] = parts[1]
    writeDoubleMetric("fooaw-a", tags, "gauge", parts[2], "false")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("fooaw-b", tags, "gauge", parts[2], "false")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("fooaw-c", tags, "gauge", parts[2], "false")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("fooaw-d", tags, "gauge", parts[2], "false")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("fooaw-e", tags, "gauge", parts[2], "false")  # Converted to new syntax by change_ind_scripts.py script
}