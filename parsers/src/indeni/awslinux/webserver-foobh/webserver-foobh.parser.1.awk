/,/ {
    split($0, parts, ",")
    tags["name"] = parts[1]
    writeDoubleMetric("foobh-a", tags, "gauge", parts[2], "false")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("foobh-b", tags, "gauge", parts[2], "false")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("foobh-c", tags, "gauge", parts[2], "false")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("foobh-d", tags, "gauge", parts[2], "false")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("foobh-e", tags, "gauge", parts[2], "false")  # Converted to new syntax by change_ind_scripts.py script
}