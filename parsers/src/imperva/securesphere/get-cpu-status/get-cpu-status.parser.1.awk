#CPU    %usr   %nice    %sys %iowait    %irq   %soft  %steal  %guest  %gnice   %idle
/CPU|%usr|%user|%nice|%sys|%iowait|%irq|%soft|%steal|%guest|%gnice|%idle/ {
    # Parse the line into a column array. The second parameter is the separator between column names.
    # The last parameter is the array to save the data into.
    getColumns(trim($0), "[ ]+", columns)
}

#all    0.50    0.00    0.00    0.00    0.00    0.00    0.00    0.00   99.50
#0    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00  100.00
#1    1.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00   99.00
#2    0.99    0.00    0.99    0.00    0.00    0.00    0.00    0.00   98.02
#3    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00  100.00
/^([0-9]|all)/ {

    # Use getColData to parse out the data for the specific column from the current line. The current line will be
    # split according to the same separator we've passed in the getColumns function (it's stored in the "columns" variable).
    # If the column cannot be found, the result of getColData is null (not "null").

    cpu_row["usr"] = getColData(trim($0), columns, "%usr")
    if (cpu_row["usr"] == "") {
        cpu_row["usr"] = getColData(trim($0), columns, "%user")
    }

    cpu_row["nice"] = getColData(trim($0), columns, "%nice")
    cpu_row["sys"] = getColData(trim($0), columns, "%sys")
    cpu_row["iowait"] = getColData(trim($0), columns, "%iowait")
    cpu_row["irq"] = getColData(trim($0), columns, "%irq")
    cpu_row["soft"] = getColData(trim($0), columns, "%soft")
    cpu_row["steal"] = getColData(trim($0), columns, "%steal")
    cpu_row["guest"] = getColData(trim($0), columns, "%guest")
    cpu_row["gnice"] = getColData(trim($0), columns, "%gnice")

    cpu_id = $1
    usage = 0
    # Adding together the total usage
    for (id in cpu_row) {
        usage = usage + cpu_row[id]
    }

    # Sometimes idle can be 0% but usage is also 0%. So we will not use the idle column for measurements as we see it as possible faulty.

    cpu_tags["cpu-id"] = cpu_id
    if (cpu_id == "all") {
        cpu_tags["cpu-is-avg"] = "true"
        cpu_tags["cpu-id"] = "all-average"
        cpu_tags["resource-metric"] = "true"
    } else {
        cpu_tags["cpu-is-avg"] = "false"
        cpu_tags["resource-metric"] = "false"
    }
    
    writeDoubleMetric("cpu-usage", cpu_tags, "gauge", usage , "true", "CPU", "percentage", "cpu-id")

}