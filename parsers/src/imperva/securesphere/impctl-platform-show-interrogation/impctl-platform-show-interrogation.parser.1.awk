#vendor                         Imperva
 /^vendor/ {
    vendor = $2
}

#environment                    VMWare
 /^environment/ {
    environment = $2
}

#model                          VM150
/^model/ {
    model = $2
}

#version                        13.2.0.20_0
/^version/ {
    version = $2
}

END {

    if (vendor != "") {
        writeTag("vendor", "imperva")
        writeTag("os.name", "centos")
    }

    if (environment != "") {
        writeTag("environment", environment)
    }

    if (model != "") {
        writeTag("model", model)
    }

    if (version != "") {
        writeTag("version", version)
    }

    if (environment != "") {
        writeTag("environment", environment)
    }
}