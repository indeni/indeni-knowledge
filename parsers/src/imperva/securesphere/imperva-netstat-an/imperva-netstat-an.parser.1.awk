#177
/^[0-9]+$/ {
    writeDoubleMetric("concurrent-connections", null, "gauge", $0, "true", "Connections - Current", "number", "")  # Converted to new syntax by change_ind_scripts.py script
}