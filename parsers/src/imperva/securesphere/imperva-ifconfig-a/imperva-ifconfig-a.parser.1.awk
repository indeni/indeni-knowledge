#eth0        Link encap:Ethernet  HWaddr 00:0C:29:FF:5B:0C
/Link encap:/ {
    interface_name = $1
    tags["name"] = interface_name
    next
}

#          RX packets:46987 errors:0 dropped:0 overruns:0 frame:0
/RX packets:/ {
    split($2, rx_packets_array, ":")
    rx_packets = rx_packets_array[2]
    writeDoubleMetric("network-interface-rx-packets", tags, "counter", rx_packets, "true", "Network Interfaces - RX Packets", "number", "name")  # Converted to new syntax by change_ind_scripts.py script

    split($3, rx_errors_array, ":")
    rx_errors = rx_errors_array[2]
    writeDoubleMetric("network-interface-rx-errors", tags, "gauge", rx_errors, "true", "Network Interfaces - RX Errors", "number", "name")  # Converted to new syntax by change_ind_scripts.py script

    split($4, rx_dropped_array, ":")
    rx_dropped = rx_dropped_array[2]
    writeDoubleMetric("network-interface-rx-dropped", tags, "counter", rx_dropped, "true", "Network Interfaces - RX Dropped", "number", "name")  # Converted to new syntax by change_ind_scripts.py script

    next
}

#          TX packets:14851 errors:0 dropped:0 overruns:0 carrier:0
/TX packets:/ {
    split($2, tx_packets_array, ":")
    tx_packets = tx_packets_array[2]
    writeDoubleMetric("network-interface-tx-packets", tags, "gauge", tx_packets, "true", "Network Interfaces - TX Packets", "number", "name")  # Converted to new syntax by change_ind_scripts.py script

    split($3, tx_errors_array, ":")
    tx_errors = tx_errors_array[2]
    writeDoubleMetric("network-interface-tx-errors", tags, "gauge", tx_errors, "true", "Network Interfaces - TX Errors", "number", "name")  # Converted to new syntax by change_ind_scripts.py script

    split($4, tx_dropped_array, ":")
    tx_dropped = tx_dropped_array[2]
    writeDoubleMetric("network-interface-tx-dropped", tags, "gauge", tx_dropped, "true", "Network Interfaces - TX Dropped", "number", "name")  # Converted to new syntax by change_ind_scripts.py script

    next
}

#          RX bytes:5935525 (5.6 MiB)  TX bytes:2264200 (2.1 MiB)
/RX bytes:/ {
    split($2, rx_bytes_array, ":")
    rx_bits = rx_bytes_array[2] * 8
    writeDoubleMetric("network-interface-rx-bits", tags, "gauge", rx_bits, "true", "Network Interfaces - RX Bits", "bits", "name")  # Converted to new syntax by change_ind_scripts.py script

    split($6, tx_bytes_array, ":")
    tx_bits = tx_bytes_array[2] * 8
    writeDoubleMetric("network-interface-tx-bits", tags, "gauge", tx_bits, "true", "Network Interfaces - TX Bits", "bits", "name")  # Converted to new syntax by change_ind_scripts.py script

    next
}