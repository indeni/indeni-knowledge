#role is server example:
#role                           server

#role is gateway example:
#role                           gateway

/^role\s/ {
    role = $2
    if (role == "server") {
        product = "management"
    } else if (role == "gateway") {
        product = "firewall"
    }
    writeTag("product", product)
}