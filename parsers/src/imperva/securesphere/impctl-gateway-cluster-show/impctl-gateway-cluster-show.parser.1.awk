BEGIN {
    state = 0
}

#interface: eth0 ip: 10.100.86.121/24 port: 22222
/interface:/ {
    state = 1
}

END {
    writeDoubleMetric("imperva-gateway-cluster-state", null, "gauge", state, "true", "Gateway Cluster State", "state", "")
}