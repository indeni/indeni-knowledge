BEGIN {
    FS = ","
}

# Parse connection status (registered | not-registered)
#not-registered,not-running
#registered,not-running 
/^(registered|not\-registered),/ {
    connection_status = $1
    # Compare the connection_status set 1 if is 'registered'
    if (connection_status == "registered") {
        is_connected = 1
    } else {
        is_connected = 0
    }
}

END {
    writeDoubleMetric("device-to-mgmt-server-connection-state", null, "gauge", is_connected, "true", "Imperva Management", "state", "")
}