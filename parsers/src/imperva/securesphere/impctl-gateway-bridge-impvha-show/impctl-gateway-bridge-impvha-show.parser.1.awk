BEGIN {
    configured = 0
    number_ha_true = 0
}

#br0                            devices=eth2 eth3,high-availability=false,mtu=1500
/^br/ {
    configured = 1
    idx++
    bridges[idx, "name"] = $1
    config = $0
    gsub($1, "", config)
    config = trim(config)
    split(config, config_arrays, ",")
    if (config_arrays[2] != "high-availability=false") {
        number_ha_true++
    }
    for (i in config_arrays) {
        split(config_arrays[i], key_value, "=")
        bridges[idx, key_value[1]] = key_value[2]
    }

}

END {
    if (configured == 1) {
        if (number_ha_true < 2) {
            state = 0
        } else {
            state = 1
        }
        writeDoubleMetric("imperva-bridge-ha-state", null, "gauge", state, "true", "Bridge HA State", "state", "")
        writeComplexMetricObjectArray("imperva-bridge-ha-config", null, bridges, "true", "Bridge HA Configuration")
    }
}