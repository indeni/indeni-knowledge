BEGIN {
  bluecat="false"
}

/^Distributor ID:/ {
  dist=$NF
}
/^Release:/ {
 release=$NF
}

/.*BlueCat/ {
  bluecat="true"
}


END {
 if (dist != "" && release != "" && bluecat =="false") {
   writeTag("vendor", "Canonical")
   writeTag("os.name", dist)
   writeTag("os.version", release)
 }
}