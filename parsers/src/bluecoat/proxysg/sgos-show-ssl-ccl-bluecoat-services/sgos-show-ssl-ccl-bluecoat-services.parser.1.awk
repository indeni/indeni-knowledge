#Certificate ID:           DigiCert_Assured_ID_Root_G2
/^Certificate ID/ {
    cert_tags["name"] = $NF
}

#Valid to:                 Jan 15 12:00:00 2038 GMT
/^Valid to/ {
    time_string = $5
    split(time_string, time_array, /:/)

    month = parseMonthThreeLetter($3)
    day = $4
    year = $6
    hour = time_array[1]
    minute = time_array[2]
    second = time_array[3]
    cert_expiration = datetime(year, month, day, hour, minute, second)
    writeDoubleMetric("certificate-expiration", cert_tags, "gauge", cert_expiration, "true", "Certificate Expiration", "date", "name")  # Converted to new syntax by change_ind_scripts.py script
}