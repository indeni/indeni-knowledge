# This script collects metrics for the configured and enabled dns servers.

# DNS servers can be in an unknown state until the tests has finished.
# In these cases the state of the server is still used to avoid flapping alerts.

# Examples of the states that the script handles:
#    Enabled     Unknown         UP
#    Enabled     OK      UP
#    Enabled     Check failed    DOWN

# Disabled DNS servers are skipped.

#Authentication
#DNS Server
#External Services
#Content analysis services
#Forwarding
/^[^\s]/{
    check_type = $0
}

#  dns.1.1.1.1
/\s+dns\./ && check_type == "DNS Server" {

    ip = $1
    sub(/^.+?\./, "", ip)

    # This variable is used to indicate if the DNS server in question is in use
    # If it isn't the script won't write response time metrics to avoid false positives.
    dns_server_status = 0

    next

}


#DNS Server
/^\s+Enabled.+(DOWN|UP)$/ && check_type == "DNS Server" {


    if ($NF == "UP") {
        dns_server_status = 1
    }

    state_tags["name"] = ip
    writeDoubleMetric("dns-server-state", state_tags, "gauge", dns_server_status, "true", "DNS Servers - State", "state", "name")  # Converted to new syntax by change_ind_scripts.py script

    next

}

#    Last response time: 150256 ms       Average response time: 150252 ms
/^\s+Last response time: [0-9]+ ms/ && check_type == "DNS Server" {

    if (dns_server_status == 1) {
        dns_response_time_tags["dns-server"] = ip
        writeDoubleMetric("dns-response-time", dns_response_time_tags, "gauge", $4, "false")  # Converted to new syntax by change_ind_scripts.py script
        writeDoubleMetric("dns-average-response-time", dns_response_time_tags, "gauge", $4, "true", "DNS Servers - Average Response Time", "number", "dns-server")  # Converted to new syntax by change_ind_scripts.py script
    }

    next

}



#Checks the status of the ICAP services:
#  icap.proxyav
#    Enabled     OK      UP

/^\s+icap\.[^\s]+$/ && check_type == "Content analysis services" {

    icap_name = $1
    sub(/icap\./, "", icap_name)
    icap_tags["name"] = icap_name
    icap_tags["process-name"] = "icap-service"
    icap_tags["command"] = "show health-checks statistics"
    icap_tags["description"] = "ICAP Traffic might drop or not be fully monitored"

    next
}

#    Enabled     OK      UP
/^\s+(Enabled|Disabled)\s+/ && check_type == "Content analysis services" {
    status = $2

    if (status == "OK") {
        icap_service_status = 1
    } else {
        icap_service_status = 0
    }
    writeDoubleMetric("bluecoat-icap-state", icap_tags, "gauge", icap_service_status, "false")
    writeDoubleMetric("bluecoat-process-state", icap_tags, "gauge", icap_service_status, "true", "ICAP Services - State", "state", "name")  # Converted to new syntax by change_ind_scripts.py script
    next
}


#Checks the status of the drtr service:
#External Services
#  drtr.rating_service
/^\s+drtr\.[^\s]+$/ && check_type == "External Services" {

    extsrv_name = $1
    extsrv_tags["process-name"] = "external-rating-service"
    extsrv_tags["command"] = "show health-checks statistics"
    extsrv_tags["description"] = "The Dynamic Real Time Rating might not work"

    next
}

#      IP address: 185.2.196.215                 Enabled         Check failed    DOWN
#      IP address: 185.2.196.215           Enabled         OK      UP
/^\s+(IP)\s+address:\s+/ && check_type == "External Services" {
    status = $5
    extsrv_ipaddr = $3
    extsrv_tags["ip"] = extsrv_ipaddr
    extsrv_tags["name"] = extsrv_name " " extsrv_ipaddr
    if (status == "OK") {
        extsrv_service_status = 1
    } else {
        extsrv_service_status = 0
    }
    writeDoubleMetric("bluecoat-external-rating-service-state", extsrv_tags, "gauge", extsrv_service_status, "false")
    writeDoubleMetric("bluecoat-process-state", extsrv_tags, "gauge", extsrv_service_status, "true", "External Services - State", "state", "name")  # Converted to new syntax by change_ind_scripts.py script
    next
}


#Authentication
#  auth.auth

/^\s+auth\.[^\s]+$/ && check_type == "Authentication" {

    auth_name = $1
    sub(/auth\./, "", auth_name)
    auth_tags["name"] = auth_name

    next
}

#    Enabled     OK      UP
/^\s+(Enabled|Disabled)\s+/ && check_type == "Authentication" {
    status = $2

    if (status == "OK") {
        auth_service_status = 1
    } else {
        auth_service_status = 0
    }

    writeDoubleMetric("bc-identity-integration-connection-state", auth_tags, "gauge", auth_service_status, "true", "Authentication Server - State", "state", "name")  # Converted to new syntax by change_ind_scripts.py script
    next

}

#    Last response time: 40 ms   Average response time: 53 ms
/^\s+Last response time: [0-9]+ ms/ && check_type == "Authentication" {
    auth_response_time_tags["auth-server"] = auth_name
    auth_response_time_tags["im.identity-tags"] = "auth-server"
    writeDoubleMetric("auth-response-time", auth_response_time_tags, "gauge", $4, "false", "name")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("auth-average-response-time", auth_response_time_tags, "gauge", $4, "true", "Authentication Servers - Average Response Time", "number")  # Converted to new syntax by change_ind_scripts.py script
}