#SPS51               1
/^SPS51/ {
    SPS51 = $2
}

#SPS61               1
/^SPS61/ {
    SPS61 = $2
}

END {
    #remove ',' in number
    gsub(",", "", SPS51)
    gsub(",", "", SPS61)
    percent_ssl = (SPS51 / (SPS51 + SPS61)) * 100
    if ((SPS51 + SPS61) != 0 ) {
        writeDoubleMetric("bluecoat-certificate-cache-ratio", null, "gauge", percent_ssl, "false")  # Converted to new syntax by change_ind_scripts.py script
    }
}