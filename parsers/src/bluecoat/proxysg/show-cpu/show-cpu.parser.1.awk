#CPU  0:    0.8    0.9    1.0    1.0
/^CPU\s+/{

    cpuId = $2
    
    #0:
    sub(/:$/, "", cpuId)

    tags["cpu-id"] = "cpu" cpuId
    tags["cpu-is-avg"] = false
    tags["resource-metric"] = "true"

    writeDoubleMetric("cpu-usage", tags, "gauge", $3, "true", "CPU", "percentage", "cpu-id")  # Converted to new syntax by change_ind_scripts.py script

}