from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class BluecoatSendingSyslogsToSyslogServerStep1(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            parsed_data = helper_methods.parse_data_as_list(raw_data, 'bluecoat_sending_syslogs_to_syslog_server_step_1.textfsm')
            for data in parsed_data:
                syslog_name = data['name']
                self.write_dynamic_variable('log_name', syslog_name)
        return self.output