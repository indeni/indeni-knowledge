from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class BluecoatSendingSyslogsToSyslogServerStep2(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            parsed_data = helper_methods.parse_data_as_list(raw_data, 'bluecoat_sending_syslogs_to_syslog_server_step_2.textfsm')
            tags = {}
            tags['name'] = dynamic_var['log_name']
            self.write_complex_metric_string('bluecoat-upload-client-status', tags, parsed_data[0]['upload_client_status'], False)
            self.write_double_metric('bluecoat-log-manager-status', tags, 'gauge', 1 if parsed_data[0]['log_manager_status'] == 'enabled and running' else 0, False)
            if parsed_data[0]['current_bandwidth'] != '':
                self.write_double_metric('bluecoat-current-bandwidth-used', tags, 'gauge', float(parsed_data[0]['current_bandwidth']), True, 'Logging - Bandwidth usage')
        return self.output