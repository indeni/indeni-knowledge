from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class BluecoatSendingSyslogsToSyslogServerAccessLogging(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            parsed_data = helper_methods.parse_data_as_list(raw_data, 'bluecoat_sending_syslogs_to_syslog_server_access_logging.textfsm')
            self.write_double_metric('bluecoat-access-log-status', {}, 'gauge', 1 if parsed_data[0]['log_status'] == 'enabled' else 0, False)
        return self.output