import os
import unittest
from bluecoat.proxysg.bluecoat_sending_syslogs_to_syslog_server.bluecoat_sending_syslogs_to_syslog_server_step_1 import BluecoatSendingSyslogsToSyslogServerStep1
from parser_service.public.action import *

class TestBluecoatSendingSyslogsToSyslogServerStep1(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = BluecoatSendingSyslogsToSyslogServerStep1()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_bluecoat_show_access_log_statistics(self):
        result = self.parser.parse_file(self.current_dir + '/bluecoat_show_access_log_statistics_step_1.input', {}, {})
        self.assertEqual(2, len(result))
        self.assertEqual(result[0].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[0].key, 'log_name')
        self.assertEqual(result[0].value, 'IND-198')
        self.assertEqual(result[1].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[1].key, 'log_name')
        self.assertEqual(result[1].value, 'SYSLOG198')

if __name__ == '__main__':
    unittest.main()