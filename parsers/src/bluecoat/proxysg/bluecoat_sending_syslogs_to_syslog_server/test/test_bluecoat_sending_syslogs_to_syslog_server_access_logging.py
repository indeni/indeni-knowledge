import os
import unittest
from bluecoat.proxysg.bluecoat_sending_syslogs_to_syslog_server.bluecoat_sending_syslogs_to_syslog_server_access_logging import BluecoatSendingSyslogsToSyslogServerAccessLogging
from parser_service.public.action import *

class TestBluecoatSendingSyslogsToSyslogServerAccessLogging(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = BluecoatSendingSyslogsToSyslogServerAccessLogging()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_bluecoat_show_access_log_enabled(self):
        result = self.parser.parse_file(self.current_dir + '/bluecoat_show_access_log_enabled.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'bluecoat-access-log-status')
        self.assertEqual(result[0].value, 1)

    def test_bluecoat_show_access_log_disabled(self):
        result = self.parser.parse_file(self.current_dir + '/bluecoat_show_access_log_disabled.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'bluecoat-access-log-status')
        self.assertEqual(result[0].value, 0)

if __name__ == '__main__':
    unittest.main()