import os
import unittest
from bluecoat.proxysg.bluecoat_sending_syslogs_to_syslog_server.bluecoat_sending_syslogs_to_syslog_server_step_2 import BluecoatSendingSyslogsToSyslogServerStep2
from parser_service.public.action import *

class TestBluecoatSendingSyslogsToSyslogServerStep2(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = BluecoatSendingSyslogsToSyslogServerStep2()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_bluecoat_show_access_log_statistics_not_connected(self):
        result = self.parser.parse_file(self.current_dir + '/bluecoat_show_access_log_statistics_not_connected_step_2.input', {'log_name': 'IND-198'}, {})
        self.assertEqual(2, len(result))
        self.assertEqual(result[0].action_type, 'WriteComplexMetric')
        self.assertEqual(result[0].name, 'bluecoat-upload-client-status')
        self.assertEqual(result[0].tags['name'], 'IND-198')
        self.assertEqual(result[0].value['value'], 'not connected')
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].name, 'bluecoat-log-manager-status')
        self.assertEqual(result[1].tags['name'], 'IND-198')
        self.assertEqual(result[1].value, 1)

    def test_bluecoat_show_access_log_statistics_not_working(self):
        result = self.parser.parse_file(self.current_dir + '/bluecoat_show_access_log_statistics_not_working_step_2.input', {'log_name': 'IND-198'}, {})
        self.assertEqual(3, len(result))
        self.assertEqual(result[0].action_type, 'WriteComplexMetric')
        self.assertEqual(result[0].name, 'bluecoat-upload-client-status')
        self.assertEqual(result[0].tags['name'], 'IND-198')
        self.assertEqual(result[0].value['value'], 'sending')
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].name, 'bluecoat-log-manager-status')
        self.assertEqual(result[1].tags['name'], 'IND-198')
        self.assertEqual(result[1].value, 0)
        self.assertEqual(result[2].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[2].name, 'bluecoat-current-bandwidth-used')
        self.assertEqual(result[2].tags['name'], 'IND-198')
        self.assertEqual(result[2].tags['live-config'], 'true')
        self.assertEqual(result[2].tags['display-name'], 'Logging - Bandwidth usage')
        self.assertEqual(result[2].value, 0.0)

    def test_bluecoat_show_access_log_statistics_running_continuous_upload(self):
        result = self.parser.parse_file(self.current_dir + '/bluecoat_show_access_log_statistics_running_continuous_upload_step_2.input', {'log_name': 'IND-198'}, {})
        self.assertEqual(2, len(result))
        self.assertEqual(result[0].action_type, 'WriteComplexMetric')
        self.assertEqual(result[0].name, 'bluecoat-upload-client-status')
        self.assertEqual(result[0].tags['name'], 'IND-198')
        self.assertEqual(result[0].value['value'], 'not connected')
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].name, 'bluecoat-log-manager-status')
        self.assertEqual(result[1].tags['name'], 'IND-198')
        self.assertEqual(result[1].value, 1)

    def test_bluecoat_show_access_log_statistics_running_not_connected(self):
        result = self.parser.parse_file(self.current_dir + '/bluecoat_show_access_log_statistics_running_not_connected_step_2.input', {'log_name': 'IND-198'}, {})
        self.assertEqual(2, len(result))
        self.assertEqual(result[0].action_type, 'WriteComplexMetric')
        self.assertEqual(result[0].name, 'bluecoat-upload-client-status')
        self.assertEqual(result[0].tags['name'], 'IND-198')
        self.assertEqual(result[0].value['value'], 'not connected')
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].name, 'bluecoat-log-manager-status')
        self.assertEqual(result[1].tags['name'], 'IND-198')
        self.assertEqual(result[1].value, 1)

    def test_bluecoat_show_access_log_statistics_running_upload_disabled_no_ip(self):
        result = self.parser.parse_file(self.current_dir + '/bluecoat_show_access_log_statistics_running_upload_disabled_no_ip_step_2.input', {'log_name': 'IND-198'}, {})
        self.assertEqual(2, len(result))
        self.assertEqual(result[0].action_type, 'WriteComplexMetric')
        self.assertEqual(result[0].name, 'bluecoat-upload-client-status')
        self.assertEqual(result[0].tags['name'], 'IND-198')
        self.assertEqual(result[0].value['value'], 'disabled')
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].name, 'bluecoat-log-manager-status')
        self.assertEqual(result[1].tags['name'], 'IND-198')
        self.assertEqual(result[1].value, 1)

    def test_bluecoat_show_access_log_statistics_stopped_not_connected(self):
        result = self.parser.parse_file(self.current_dir + '/bluecoat_show_access_log_statistics_stopped_not_connected_step_2.input', {'log_name': 'IND-198'}, {})
        self.assertEqual(2, len(result))
        self.assertEqual(result[0].action_type, 'WriteComplexMetric')
        self.assertEqual(result[0].name, 'bluecoat-upload-client-status')
        self.assertEqual(result[0].tags['name'], 'IND-198')
        self.assertEqual(result[0].value['value'], 'not connected')
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].name, 'bluecoat-log-manager-status')
        self.assertEqual(result[1].tags['name'], 'IND-198')
        self.assertEqual(result[1].value, 0)

    def test_bluecoat_show_access_log_statistics_working(self):
        result = self.parser.parse_file(self.current_dir + '/bluecoat_show_access_log_statistics_working_step_2.input', {'log_name': 'IND-198'}, {})
        self.assertEqual(3, len(result))
        self.assertEqual(result[0].action_type, 'WriteComplexMetric')
        self.assertEqual(result[0].name, 'bluecoat-upload-client-status')
        self.assertEqual(result[0].tags['name'], 'IND-198')
        self.assertEqual(result[0].value['value'], 'connected/ready')
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].name, 'bluecoat-log-manager-status')
        self.assertEqual(result[1].tags['name'], 'IND-198')
        self.assertEqual(result[1].value, 1)
        self.assertEqual(result[2].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[2].name, 'bluecoat-current-bandwidth-used')
        self.assertEqual(result[2].tags['name'], 'IND-198')
        self.assertEqual(result[2].tags['live-config'], 'true')
        self.assertEqual(result[2].tags['display-name'], 'Logging - Bandwidth usage')
        self.assertEqual(result[2].value, 140.0)

    def test_bluecoat_show_access_log_statistics_disabled(self):
        result = self.parser.parse_file(self.current_dir + '/bluecoat_show_access_log_statistics_disabled_step_2.input', {'log_name': 'SYSLOG198'}, {})
        self.assertEqual(2, len(result))
        self.assertEqual(result[0].action_type, 'WriteComplexMetric')
        self.assertEqual(result[0].name, 'bluecoat-upload-client-status')
        self.assertEqual(result[0].tags['name'], 'SYSLOG198')
        self.assertEqual(result[0].value['value'], 'disabled')
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].name, 'bluecoat-log-manager-status')
        self.assertEqual(result[1].tags['name'], 'SYSLOG198')
        self.assertEqual(result[1].value, 0)

if __name__ == '__main__':
    unittest.main()