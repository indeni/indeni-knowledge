#Interface: 0:0
/^Interface:/ {
    int_name = $2
}


#Total number of bytes received 1248629681
/^Total number of bytes received / {
    rx_bits = $6 * 8
    interface_tags["name"] = int_name
    writeDoubleMetric("network-interface-rx-bits", interface_tags, "counter", rx_bits, "true", "Network Interfaces - Rx Bits", "bits", "name")
}

#Total number of bytes sent 1248629681
/^Total number of bytes sent / {
    tx_bits = $6 * 8
    interface_tags["name"] = int_name
    writeDoubleMetric("network-interface-tx-bits", interface_tags, "counter", tx_bits, "true", "Network Interfaces - Tx Bits", "bits", "name")
}

#Dropped on input 0
/^Dropped on input / {
    rx_dropped = $4
    interface_tags["name"] = int_name
    writeDoubleMetric("network-interface-rx-dropped", interface_tags, "counter", rx_dropped, "true", "Network Interfaces - Dropped on input", "number", "name")
}

#Dropped on output 0
/^Dropped on output / {
    tx_dropped = $4
    interface_tags["name"] = int_name
    writeDoubleMetric("network-interface-tx-dropped", interface_tags, "counter", tx_dropped, "true", "Network Interfaces - Dropped on output", "number", "name")
}

#Input errors 0
/^Input errors / {
    rx_errors = $3
    interface_tags["name"] = int_name
    writeDoubleMetric("network-interface-rx-errors", interface_tags, "counter", rx_errors, "true", "Network Interfaces - Input errors", "number", "name")
}

#Output errors 0
/^Output errors / {
    tx_errors = $3
    interface_tags["name"] = int_name
    writeDoubleMetric("network-interface-tx-errors", interface_tags, "counter", tx_errors, "true", "Network Interfaces - Output errors", "number", "name")
}