#CPU Monitor
#If it's not running, we'll see "CPU Monitor is not running. Enable in diagnostics menu"
#If it is, we will see:
#CPU Monitor:
#Configured interval duration:  5 seconds
#Current interval complete in:  2 seconds
#CPU 0                                                1%
#TCPIP                                            1%

/^(CPU Monitor is not running|Configured interval duration)/ {
    if (match($0, /^CPU Monitor is not running/)) {
        cpu_monitor_enabled = "false"
    } else {
        cpu_monitor_enabled = "true"
    }
    writeComplexMetricString("cpu-monitor-enabled", null, cpu_monitor_enabled, "false")  # Converted to new syntax by change_ind_scripts.py script

}

#TCPIP                                            1%
/%$/ {
    process_name = $1
    if (process_name != "CPU") {
        cpu_usage = $NF
        gsub(/%/, "", cpu_usage)
        processtags["name"] = process_name
        processtags["process-name"] = process_name
        processtags["command"] = "show cpu-monitor"
        writeDoubleMetric("process-cpu", processtags, "gauge", cpu_usage, "true", "Top Processes CPU Utilization", "percentage", "process-name")  # Converted to new syntax by change_ind_scripts.py script
   }
}