/^Maximum acceptable concurrent client connections/ || /^Maximum acceptable concurrent client workers/ {
    value = $NF
    writeDoubleMetric("concurrent-connections-limit", null, "gauge", value, "true", "Maximum acceptable concurrent client connections", "number", "")  # Converted to new syntax by change_ind_scripts.py script
}

/^Currently established client connections/ || /^Currently created client workers/ {
    value = $NF
    writeDoubleMetric("concurrent-connections", null, "gauge", value, "true", "Current concurrent client connections", "number", "")  # Converted to new syntax by change_ind_scripts.py script
}