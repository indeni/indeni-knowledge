import os
import unittest
from bluecoat.proxysg.show_content_analysis_statistics.show_content_analysis_statistics import ShowContentAnalysisStatistics
from parser_service.public.action import *

class TestShowContentAnalysisStatistics(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = ShowContentAnalysisStatistics()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_content_analysis(self):
        result = self.parser.parse_file(self.current_dir + '/show_content_analysis_statistics.input', {}, {})
        self.assertEqual(6, len(result))
        self.assertEqual(result[0].tags['name'], 'cas')
        self.assertEqual(result[0].name, 'total-requests')
        self.assertEqual(result[0].value, 0)
        self.assertEqual(result[1].name, 'communication-errors')
        self.assertEqual(result[1].value, 0)
        self.assertEqual(result[2].name, 'max-connections')
        self.assertEqual(result[2].value, 25)
        self.assertEqual(result[3].name, 'version')
        self.assertEqual(result[3].value['value'], 1.0)
        self.assertEqual(result[4].name, 'opp-entity')
        self.assertEqual(result[4].value['value'], 'cas')
        self.assertEqual(result[5].name, 'entity-name')
        self.assertEqual(result[5].value['value'], 'cas')

    def test_content_analysis_multiple(self):
        result = self.parser.parse_file(self.current_dir + '/show_content_analysis_statistics_multiple.input', {}, {})
        self.assertEqual(34, len(result))
        self.assertEqual(result[0].tags['name'], 'test_pa02')
        self.assertEqual(result[0].name, 'total-requests')
        self.assertEqual(result[0].value, 17926807)
        self.assertEqual(result[1].name, 'communication-errors')
        self.assertEqual(result[1].value, 4673)
        self.assertEqual(result[2].name, 'max-connections')
        self.assertEqual(result[2].value, 100)
        self.assertEqual(result[3].name, 'version')
        self.assertEqual(result[3].value['value'], 1.0)
        self.assertEqual(result[4].name, 'opp-entity')
        self.assertEqual(result[4].value['value'], 'test_pa02')
        self.assertEqual(result[5].name, 'entity-name')
        self.assertEqual(result[5].value['value'], 'test_pa02')

        self.assertEqual(result[6].tags['name'], 'cas_cas01')
        self.assertEqual(result[6].name, 'total-requests')
        self.assertEqual(result[6].value, 28197551)
        self.assertEqual(result[7].name, 'communication-errors')
        self.assertEqual(result[7].value, 5)
        self.assertEqual(result[8].name, 'max-connections')
        self.assertEqual(result[8].value, 250)
        self.assertEqual(result[9].name, 'version')
        self.assertEqual(result[9].value['value'], 1.0)
        self.assertEqual(result[10].name, 'opp-entity')
        self.assertEqual(result[10].value['value'], 'cas_cas01')
        self.assertEqual(result[11].name, 'entity-name')
        self.assertEqual(result[11].value['value'], 'cas_cas01')

if __name__ == '__main__':
    unittest.main()