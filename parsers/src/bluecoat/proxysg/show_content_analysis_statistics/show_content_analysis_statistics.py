from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class ShowContentAnalysisStatistics(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            parsed_data = helper_methods.parse_data_as_list(raw_data, 'show_content_analysis_statistics.textfsm')
            if parsed_data:
                tags = {}
                for entry in parsed_data:
                    tags['name'] = str(entry['name'])
                    if len(entry['request']) > 0:
                        self.write_double_metric('total-requests', tags, 'gauge', int(entry['request'].replace(',', '')), True, 'Total Requests', 'number', 'name')
                    if len(entry['errors']) > 0:
                        self.write_double_metric('communication-errors', tags, 'gauge', int(entry['errors'].replace(',', '')), False)
                    if len(entry['max_conns']) > 0:
                        self.write_double_metric('max-connections', tags, 'gauge', int(entry['max_conns'].replace(',', '')), False)
                    self.write_complex_metric_string('version', tags, float(entry['version']), False)
                    self.write_complex_metric_string('opp-entity', tags, str(entry['entity']), False)
                    self.write_complex_metric_string('entity-name', tags, str(entry['name']), False)
        return self.output
