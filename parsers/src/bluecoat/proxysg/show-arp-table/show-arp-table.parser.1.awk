BEGIN {
    i = 0
}

#10.3.3.2         (incomplete)      0:0
#10.3.3.29        00:1b:17:00:3e:11 0:0
/^[0-9]+\./ {

    if($2 == "(incomplete)"){
        success = 0
        mac = "incomplete"
    } else {
        success = 1
        mac = $2
    }

    i++
    arpTable[i, "targetip"] = $1
    arpTable[i, "mac"] = mac
    arpTable[i, "interface"] = $3
    arpTable[i, "success"] = success

}

END {
    writeComplexMetricObjectArray("arp-table", null, arpTable, "false")  # Converted to new syntax by change_ind_scripts.py script
}