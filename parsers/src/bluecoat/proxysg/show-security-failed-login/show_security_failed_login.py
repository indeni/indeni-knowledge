from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

# Default List: local_user_database
# Append users loaded from file to default list: false
# local_user_database
#   Lockout parameters:
#     Max failed attempts: 60
#     Lockout duration:    3600
#     Reset interval:      7200
#   Users:
#       user1
#         Hashed Password:
#         Enabled:         true
#         Groups:
#   Groups:
#       users

# list
#   Lockout parameters:
#     Max failed attempts: 60
#     Lockout duration:    3600
#     Reset interval:      7200
#   Users:
#       test
#         Hashed Password:
#         Enabled:         true
#         Groups:
#   Groups:


class ShowSecurityFailedLogin(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:  # We check input is not emtpy
            lines = raw_data.splitlines()
            for i in range(2, len(lines)):  # we ignore first two lines
                if not lines[i].startswith(' '):  # if doesnt start with empty space then is userlist
                    user_list = lines[i]
                elif 'Max failed attempts:' in lines[i]:
                    max_failed_attemps = int(lines[i].split()[-1])
                    tags = {'name': user_list}
                    if max_failed_attemps == 0 or max_failed_attemps > 5:
                        alert_max_failed_attempts = 0
                    else:
                        alert_max_failed_attempts = 1
                    self.write_double_metric('max-login-failed-attempts', tags, 'gauge', alert_max_failed_attempts, False, 'Login Max failed attempts', 'number', 'name')
        return self.output
