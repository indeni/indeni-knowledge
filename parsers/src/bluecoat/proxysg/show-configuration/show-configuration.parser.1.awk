BEGIN {
    i = 0;
    security_allowed_access = 0
    ip_addresses_index = 0
    password_length = 0
}

/^\s*$/ {
    next
}

{
    configuration_obj[i++, "line"] = $0
    if ($0 ~ /security allowed-access add/) {
    	security_allowed_access = 1
        ip_addresses[ip_addresses_index,"IP Address"] = $(NF-1)"/"$NF
        ip_addresses_index++
    }

    if ($0 ~ /security password-min-len/) {
        if ($NF > 13)
            password_length = 1
    }
}

END {
    if (security_allowed_access == 0)
    	ip_addresses[ip_addresses_index,"IP Address"] = "ANY"
   	
    writeComplexMetricObjectArray("configuration-content", null, configuration_obj, "false")
    writeComplexMetricObjectArray("security-allowed-access", null, ip_addresses, "true","Security Allowed Access")
    writeDoubleMetric("bluecoat-allowed-ips", null, "gauge", security_allowed_access, "false")
    writeDoubleMetric("password-min-length-too-short", null, "gauge", password_length, "false")

}
