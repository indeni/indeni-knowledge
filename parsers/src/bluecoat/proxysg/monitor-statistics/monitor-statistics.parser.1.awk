# Stat: CPU Utilization
# Current State                 : OK
# Last Transition               : Mon, 23 May 2016 14:38:19 UTC
# Current Value                 : 7
# Unit of Measurement           : percent
# Warning Threshold             : 80
# Warning Interval              : 120
# Critical Threshold            : 95
# Critical Interval             : 120
# Notification Method           : log
BEGIN{ FS = ":" } # In the Health Monitor section, we use ":"

/^Stat/ {
    hmname = trim($2)
}
/^Current State/ {
    hmstate = trim($2)
}
/^Notification Method/ {
 # We use this line to flush the metric
    is_ok = 0
    if (hmstate == "OK") {
        is_ok = 1
 }

 if (hmname != "Health Check Status") {
    healthmonitorTags["monitor-name"] = hmname
    healthmonitorTags["monitor-state"] = hmstate

    writeDoubleMetric("bluecoat-health-monitor", healthmonitorTags, "gauge", is_ok, "true", "Health Monitors", "state", "monitor-name|monitor-state")  # Converted to new syntax by change_ind_scripts.py script
 }
}