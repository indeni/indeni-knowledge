BEGIN {
    profile_name = "SSL Client Default"
    rc4_cipher_value = 0
    sweet32_cipher_value = 0
    sslv3_value = 0
    sslv2_value = 0
}

#Cipher suite: ecdhe-rsa-aes256-sha384 ecdhe-rsa-aes128-sha256 ecdhe-rsa-aes256-gcm-sha384 ecdhe-rsa-aes128-gcm-sha256 ecdhe-rsa-aes128-sha ecdhe-rsa-aes256-sha aes128-sha256 aes256-sha256 aes128-gcm-sha256 aes256-gcm-sha384 aes128-sha ae-aes256-gcm-sha384 des-cbc3-md5
/Cipher suite/ {
    if ($0 ~ /rc4/) {
        rc4_cipher_value = 1
    }
    if ($0 ~ /des-cbc3/) {
        sweet32_cipher_value = 1
    }

    #RC4 is considered a weak cipher and there has been indications that is has
    #been compromised by certain government agencies
    rc4_tags["name"] = profile_name
    rc4_tags["cipher"] = "RC4"
    rc4_tags["vulnerability"] = "Bar Mitzvah"
    writeDoubleMetric("ssl-weak-cipher", rc4_tags, "gauge", rc4_cipher_value, "false")  # Converted to new syntax by change_ind_scripts.py script

    sweet32_tags["name"] = profile_name
    sweet32_tags["vulnerability"] = "SWEET32"
    sweet32_tags["cipher"] = "DES-CBC3"
    writeDoubleMetric("ssl-weak-cipher", sweet32_tags, "gauge", sweet32_cipher_value, "false")  # Converted to new syntax by change_ind_scripts.py script
}

#Protocol: sslv2 sslv3 tlsv1 tlsv1.1 tlsv1.2
/Protocol/ {
    if ($0 ~ /sslv3/) {
        sslv3_value = 1
    }
    if ($0 ~ /sslv2/) {
        sslv2_value = 1
    }

    #SSLv3 is consider not only weak, but more or less compromised (ie. Poodle)
    sslv3_tags["name"] = profile_name
    sslv3_tags["vulnerability"] = "POODLE"
    sslv3_tags["protocol"] = "SSLv3"
    writeDoubleMetric("ssl-weak-protocol", sslv3_tags, "gauge", sslv3_value, "false")  # Converted to new syntax by change_ind_scripts.py script

    #SSLv2 is considered compromised (Drown)
    sslv2_tags["name"] = profile_name
    sslv2_tags["vulnerability"] = "DROWN"
    sslv2_tags["protocol"] = "SSLv2"
    writeDoubleMetric("ssl-weak-protocol", sslv2_tags, "gauge", sslv2_value, "false")  # Converted to new syntax by change_ind_scripts.py script
}