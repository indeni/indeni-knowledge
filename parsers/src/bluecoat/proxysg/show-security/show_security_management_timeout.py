from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

# Account:
#   Username:        "admin"
#   Hashed Password: ###########################
#   Hashed Enable Password: ###########################
#   Hashed Front Panel PIN: ###########################
#   Minimum password length: 14
#   Web interface display realm name: ""
#   Web interface session timeout: 15 minutes
#   CLI session timeout: 5 minutes
# Access control is disabled
# Access control list (source, mask):
#   192.168.0.0, 255.255.0.0
#   192.168.1.1, 255.255.255.255
# Legacy relative usernames are disabled
# Force credential forwarding is disabled


class ShowSecurityManagementTimeout(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:  # We check if input is not empty
            alert_web_timeout = 9999999000
            alert_ssh_timeout = 9999999000
            # High thresholds to fire alert if not timeout set

            lines = raw_data.splitlines()
            for line in lines:
                if ('Web interface session timeout: ' in line
                        and 'Never' not in line):
                    alert_web_timeout = int(line.split()[4])
                    alert_web_timeout = alert_web_timeout*60*1000

                if ('CLI session timeout: ' in line
                        and 'Never' not in line):
                    alert_ssh_timeout = int(line.split()[3])
                    alert_ssh_timeout = alert_ssh_timeout*60*1000

            tags = {'name': 'Web Idle Timeout'}
            self.write_double_metric('web-timeout', tags, 'gauge', alert_web_timeout, True, 'Web Idle Timeout', 'duration', 'name')
            tags = {'name': 'SSH Idle Timeout'}
            self.write_double_metric('ssh-timeout', tags, 'gauge', alert_ssh_timeout, True, 'SSH Idle Timeout', 'duration', 'name')

        return self.output
