from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser
 
#Traps and informs are enabled.
#SNMP authentication failure traps are disabled.
#SNMPv1 is enabled. SNMPv2c is enabled. SNMPv3 is enabled.
#Traps and informs are enabled.
#SNMP authentication failure traps are disabled.

class ShowSNMPParser(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        unencrypted_snmp_configured = "false"
        lines = raw_data.splitlines()
        for line in lines:
          if 'SNMPv1 is enabled' in line or 'SNMPv2c is enabled' in line:
            unencrypted_snmp_configured = "true"

        tags = {"name" : "Unencrypted SNMP Configured"}
        self.write_complex_metric_string("unencrypted-snmp-configured",tags, unencrypted_snmp_configured, False)
 
        return self.output