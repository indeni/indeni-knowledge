BEGIN {
    software_eos_array["7.2"] = date(2022, 10, 31)
    software_eos_array["6.7"] = date(2023, 12, 31)
    software_eos_array["6.6"] = date(2019, 06, 30)
    software_eos_array["6.5"] = date(2020, 04, 30)
    software_eos_array["6.4"] = date(2014, 10, 31)
    software_eos_array["6.3"] = date(2014, 04, 30)
    software_eos_array["6.2"] = date(2015, 10, 31)
    software_eos_array["6.1"] = date(2015, 04, 30)
    software_eos_array["5.5"] = date(2014, 08, 31)
    software_eos_array["5.4"] = date(2014, 08, 31)
    software_eos_array["5.3"] = date(2012, 08, 31)
    software_eos_array["5.2"] = date(2010, 12, 30)
    software_eos_array["5.1"] = date(2010, 06, 30)
    software_eos_array["4.3"] = date(2014, 06, 30)
    software_eos_array["4.2"] = date(2010, 12, 30)
    software_eos_array["4.1"] = date(2010, 06, 30)
    software_eos_array["3.2"] = date(2009, 09, 05)
    software_eos_array["3.1"] = date(2004, 12, 15)
    software_eos_array["2.1"] = date(2004, 12, 15)
}

#  Software version:        SG 6.7.2.1
/^\s*Software version:\s+SG/ {
    split($NF, version_array, ".")
    major_version = version_array[1] "." version_array[2]
    if (major_version in software_eos_array) {
        software_eos_date = software_eos_array[major_version]
        writeDoubleMetric("software-eos-date", null, "gauge", software_eos_date, "false")  # Converted to new syntax by change_ind_scripts.py script
    }
}