#The ProxySG Appliance was last software rebooted 24 days, 11 hours, 7 minutes, and 8 seconds ago.
/^The ProxySG Appliance was last software rebooted/{
    years = 0
    days = 0
    hours = 0
    minutes = 0
    seconds = 0
    for (i = 1; i <= NF; i++) {
        element = trim($i)
        if (element ~ /^year/) {
            years = $(i-1)
        }
        else if (element ~ /^day/) {
            days = $(i-1)
        }
        else if (element ~ /^hour/) {
            hours = $(i-1)
        }
        else if (element ~ /^minute/) {
            minutes = $(i-1)
        }
        else if (element ~ /^second/) {
            seconds = $(i-1)
        }
    }
    uptime_in_seconds = years * 31536000 + days * 86400 + hours * 3600 + minutes * 60 + seconds
    writeDoubleMetric("uptime-milliseconds", null, "gauge", (uptime_in_seconds*1000), "true", "Device Uptime", "duration", "")  # Converted to new syntax by change_ind_scripts.py script
}