BEGIN {
    memory_tags["name"] = "RAM"
    disk_tags["name"] = "DISK"
    disk_tags["file-system"] = "proxysg-disk-resources"
}

function convert_to_kb(mem_bytes) {

    #4,105,961,472
    gsub(/,/, "", mem_bytes)
    total_mem_kbytes = mem_bytes/1000

    return total_mem_kbytes
}



#  Total disk installed                        :          107,374,182,400 bytes
/^\s+Total disk installed/{
    total_disk_kbytes = convert_to_kb($(NF-1))
    writeDoubleMetric("disk-total-kbytes", disk_tags, "gauge", total_disk_kbytes, "true", "Disk - Total", "kilobytes", "name")  # Converted to new syntax by change_ind_scripts.py script
}

#  Disk available for cache                    :           95,556,380,304 bytes
/^\s+Disk available for cache/{
    total_disk_kbytes_free = convert_to_kb($(NF-1))
    writeDoubleMetric("disk-free-kbytes", disk_tags, "gauge", total_disk_kbytes_free, "true", "Disk - Free", "kilobytes", "name")  # Converted to new syntax by change_ind_scripts.py script
}

#  Total memory available                      :            4,105,961,472 bytes
/^\s+Total memory available/{
    total_mem_kbytes = convert_to_kb($(NF-1))
    writeDoubleMetric("memory-total-kbytes", memory_tags, "gauge", total_mem_kbytes, "true", "Memory - Total", "kilobytes", "name")  # Converted to new syntax by change_ind_scripts.py script
}

#    Total memory free                         :            2,728,988,672 bytes
/^\s+Total memory free/{
    total_mem_kbytes_free = convert_to_kb($(NF-1))
    writeDoubleMetric("memory-free-kbytes", memory_tags, "gauge", total_mem_kbytes_free, "true", "Memory - Free", "kilobytes", "name")  # Converted to new syntax by change_ind_scripts.py script
}

#    Total memory committed                    :            1,018,867,712 bytes
/^\s+Committed memory:\(%/{
    memory_tags["resource-metric"] = "true"
    memory_usage = $(NF-1)
    total_disk_used_kbytes = total_disk_kbytes - total_disk_kbytes_free
    disk_usage = total_disk_used_kbytes / total_disk_kbytes * 100
    writeDoubleMetric("memory-usage", memory_tags, "gauge", memory_usage, "true", "Memory - Usage", "percentage", "name")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("disk-used-kbytes", disk_tags, "gauge", total_disk_used_kbytes, "true", "Disk - Used", "kilobytes", "name")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("disk-usage-percentage", disk_tags, "gauge", disk_usage, "true", "Disk - Used Percentage", "percentage", "name")  # Converted to new syntax by change_ind_scripts.py script
}