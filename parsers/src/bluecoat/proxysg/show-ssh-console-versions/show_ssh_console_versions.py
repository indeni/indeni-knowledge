from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class ShowSshVersions(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        sshv1_enabled = 0
        lines = raw_data.splitlines()
        for line in lines:
            if 'SSHv1 is enabled.' in line:
                sshv1_enabled = 1
        tags = {'name': 'SSH Version'}
        self.write_double_metric('ssh-version-1-enabled', tags, 'gauge', sshv1_enabled, False, 'SSH Version 1 Enabled', 'state', 'name')
        return self.output
