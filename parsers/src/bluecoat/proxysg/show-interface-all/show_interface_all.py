from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser
from ipaddress import IPv4Network, NetmaskValueError


class ShowInterfaceAll(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        data = helper_methods.parse_data_as_list(raw_data, 'show_interface_all.textfsm')
        for interface in data:
            tags = {'name': interface['name']}
            self.write_double_metric('network-interface-admin-state', tags, 'gauge', 1 if interface['status'] == 'enabled' else 0, True, 'Network Interfaces - Enabled/Disabed', 'state', 'name')

            if interface['ip']:
                self.write_complex_metric_string('network-interface-ipv4-address', tags, interface['ip'], True, 'Network Interfaces - IPv4 Address')

            if interface['netmask']:
                try:
                    self.write_complex_metric_string('network-interface-ipv4-subnet', tags, str(IPv4Network('0.0.0.0/'+interface['netmask']).prefixlen), False)
                except NetmaskValueError:
                    self.write_complex_metric_string('network-interface-ipv4-subnet', tags, 'Bad Netmask: '+interface['netmask'], False)
            
            if interface['MTU']:
                self.write_complex_metric_string('network-interface-mtu', tags, interface['MTU'], False)

            if 'no link' in interface['linkstatus']:
                self.write_double_metric('network-interface-state', tags, 'gauge', 0, False)
            else:
                self.write_double_metric('network-interface-state', tags, 'gauge', 1, False)
                if 'full duplex' in interface['linkstatus']:
                    self.write_complex_metric_string('network-interface-duplex', tags, 'full', False)
                elif 'half duplex':
                    self.write_complex_metric_string('network-interface-state', tags, 'half', False)

            if 'virtual network' in interface['linkstatus']:
                self.write_complex_metric_string('network-interface-type', tags, 'virtual', False)

        return self.output
