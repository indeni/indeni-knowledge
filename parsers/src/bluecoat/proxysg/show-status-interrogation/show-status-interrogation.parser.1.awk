#  Software version:        SG 6.7.2.1
/^\s*Software version:\s+SG/{
    writeTag("vendor", "bluecoat")
    writeTag("os.name", "sgos")
    writeTag("os.version", $NF)
}