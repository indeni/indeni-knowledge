#External content analysis services:
#   Secure-ICAP-enabled: no

/\s*Secure-ICAP-enabled:/ {
    if ($NF == "no")
    	secure_icap_enabled = 0
    else
    	secure_icap_enabled = 1
  
    writeDoubleMetric("secure-icap-enabled", tags, "gauge", secure_icap_enabled, "true", "Secure-ICAP-enabled","state", "name")
}
