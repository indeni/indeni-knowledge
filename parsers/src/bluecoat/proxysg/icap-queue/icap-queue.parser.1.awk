/stats:queued_transaction_count/{
    last_min_queue = $67
    writeDoubleMetric("bluecoat-icap-queue", null, "gauge", last_min_queue, "true", "ICAP Service - Last Minute Queue Count ", "number", "name")  # Converted to new syntax by change_ind_scripts.py script
}