import re
from parser_service.public.base_parser import BaseParser


class AsaUptime(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):

        seconds = -1

        # The are three variants for the input string
        # 81 days, 5 hours, 30 minutes, 47 seconds.
        # 3087 hours 34 minutes 36 seconds (1111527600)
        result = re.search(
            '((?P<days>[0-9]+)\s+days,?\s+)?(?P<hours>[0-9]+)\s+hours,?\s+(?P<minutes>[0-9]+)\s+minutes,?\s+(?P<seconds>[0-9]+)\s+seconds',
            raw_data)

        if result is None:
            # 2 days, 21:09:40.00
            result = re.search(
                '((?P<days>[0-9]+)\s+days,?\s+)?(?P<hours>[0-9]+):(?P<minutes>[0-9]+):(?P<seconds>[0-9]+)', raw_data)

        if result is not None:
            # Days is optional
            days = 0
            if result.group('days') is not None:
                days = int(result.group('days'))
            hours = int(result.group('hours'))
            minutes = int(result.group('minutes'))
            seconds = int(result.group('seconds'))
            seconds += (days * 24 * 60 * 60) + (hours * 60 * 60) + (minutes * 60)

        if seconds != -1:
            self.write_double_metric('uptime-milliseconds', {}, 'gauge', (seconds * 1000), True, 'System Uptime',
                                     'duration', None)

        return self.output
