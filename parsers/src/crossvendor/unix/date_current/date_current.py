from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class UnixDateCurrent(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            parsed_data = helper_methods.parse_data_as_list(raw_data, 'date_current.textfsm')
            if parsed_data:
                self.write_double_metric('current-datetime', {}, 'gauge', int(parsed_data[0]['date_sec']), True, 'Current Date/Time', 'date', '')
                timezone_list = []
                timezone = {}
                timezone['current'] = parsed_data[0]['timezone']
                timezone_list.append(timezone)
                self.write_complex_metric_object_array('current-timezone', {}, timezone_list, True, "Timezone code")
            return self.output

