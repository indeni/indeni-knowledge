import os
import unittest
from crossvendor.unix.date_current.date_current import UnixDateCurrent
from parser_service.public.action import *

class TestUnixDateCurrent(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = UnixDateCurrent()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_timezone_CEST(self):
        result = self.parser.parse_file(self.current_dir + '/timezone_CEST.input', {}, {})
        self.assertEqual(2, len(result))
        self.assertEqual(1574014515, result[0].value)
        self.assertEqual('CEST', result[1].value[0]['current'])
        self.assertEqual('current-datetime',result[0].name)
        self.assertEqual('current-timezone',result[1].name)

if __name__ == '__main__':
    unittest.main()