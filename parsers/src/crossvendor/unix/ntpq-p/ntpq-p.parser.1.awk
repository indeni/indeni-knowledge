#Match lines that contains digits
#*10.10.10.144  193.204.114.233  2 u   42   64   37    0.193   -7.375   9.959
/\d/{
	serverIP = $1
	
	#Remove the first character if it's not a number
	sub(/^[^\d]/, "", serverIP)
	
	#Extract the ipaddress
	ntpServerTags["name"] = serverIP
	
	#Rows that starts with the following contains failed ntp servers
	#" "	non-communicating remote machines,
	#	"LOCAL" for this local host,
	#	(unutilised) high stratum servers,
	#	remote machines that are themselves using this host as their synchronisation reference;
		
	#Rows that starts with the following contains functioning ntp servers
	#"#" 	Good remote peer or server but not utilised (not among the first six peers sorted by synchronization distance, ready as a backup source);
	#"+"	Good and a preferred remote peer or server (included by the combine algorithm);
	#"*"	The remote peer or server presently used as the primary reference;
	#"o"	PPS peer (when the prefer peer is valid). The actual system synchronization is derived from a pulse-per-second (PPS) signal, either indirectly via the PPS reference clock driver or directly via kernel interface.
	
	#Source:
	#http://nlug.ml1.co.uk/2012/01/ntpq-p-output/831
	
	if(match($0, /^[\#\+\*o]/)){
		writeDoubleMetric("ntp-server-state", ntpServerTags, "gauge", 1, "true", "NTP Servers", "state", "name")  # Converted to new syntax by change_ind_scripts.py script
	} else {
		writeDoubleMetric("ntp-server-state", ntpServerTags, "gauge", 0, "true", "NTP Servers", "state", "name")  # Converted to new syntax by change_ind_scripts.py script
	}
    
}