############
# Script explanation: Decrease timeout (+time=1), otherwise the script times out if there are too many servers that are not reachable. One second means that for each DNS server it will make three attempts and wait 1 second for each.
###########

# DNSserver 8.8.8.8
/DNSserver/ {
    server=$NF
    serverStatus[server] = 0
    idns++
    dns[idns, "ipaddress"] = server
}

# ;; Query time: 12 msec
/Query time/ {
    querytime=$(NF-1)
    serverStatus[server] = 1
    response[server] = querytime
}


END {
    for (id in serverStatus) {
        dnstags["dns-server"] = id
        t["name"] = id
        if (serverStatus[id] == 1) {
            writeDoubleMetric("dns-response-time", dnstags, "gauge", response[id], "true", "DNS Response Time (Average)", "number", "dns-server")  # Converted to new syntax by change_ind_scripts.py script
        }
        writeDoubleMetric("dns-server-state", t, "gauge", serverStatus[id], "true", "DNS Servers", "state", "name")  # Converted to new syntax by change_ind_scripts.py script
        writeComplexMetricObjectArray("dns-servers", null, dns, "true", "DNS Servers")  # Converted to new syntax by change_ind_scripts.py script
    }
}