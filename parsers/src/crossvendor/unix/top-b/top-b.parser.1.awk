BEGIN {
    no_of_cores = 0
}

#load average: 2.43, 3.16, 3.12
#last pid:  3812;  load averages:  1.23,  2.34,  3.45  up 1+01:19:04    11:54:12
#top - 14:12:03 up 4 days, 8 min, 23 users,  load average: 0.28, 0.30, 0.31
/load average/ {
    line = $0
    sub(/.*load average[s]*:\s+/, "", line)
    split(line, loads, ",")

    # the third one might have a "tail" of data, if it's in FreeBSD
    loads[3] = trim(loads[3])
    sub(/\s+.*/, "", loads[3])
    next
}

#Tasks: 217 total,   3 running, 214 sleeping,   0 stopped,   17 zombie
/^Tasks:/ {
    zombies = $(NF-1)
    writeDoubleMetric("tasks-zombies", null, "gauge", zombies, "false")  # Converted to new syntax by change_ind_scripts.py script
    next
}

#4
/^[0-9]+/{
    no_of_cores = trim($NF)
}

END {
    one_min_average = trim(loads[1])
    five_min_average = trim(loads[2])
    fifteen_min_average = trim(loads[3])

    writeDoubleMetric("load-average-one-minute-live-config", null, "gauge", one_min_average, "true", "Load Average (1 Minute)", "number", "")
    writeDoubleMetric("load-average-five-minutes-live-config", null, "gauge", five_min_average, "true", "Load Average (5 Minutes)", "number", "")
    writeDoubleMetric("load-average-fifteen-minutes-live-config", null, "gauge", fifteen_min_average, "true", "Load Average (15 Minutes)", "number", "")

    if (no_of_cores > 0) {
        one_min_normalized = one_min_average / no_of_cores
        five_min_normalized = five_min_average / no_of_cores
        fifteen_min_normalized = fifteen_min_average / no_of_cores
    } else {
        one_min_normalized = one_min_average
        five_min_normalized = five_min_average
        fifteen_min_normalized = fifteen_min_average
    }

    writeDoubleMetric("load-average-one-minute", null, "gauge", one_min_normalized, "false")
    writeDoubleMetric("load-average-five-minutes", null, "gauge", five_min_normalized, "false")
    writeDoubleMetric("load-average-fifteen-minutes", null, "gauge", fifteen_min_normalized, "false")
}