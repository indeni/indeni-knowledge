# admin        1  0.0  0.0   2044   632 ?        Ss   Mar03   0:44 init [3]
/[0-9]/ {
    cpu = $3
    memory = $4
    pid = $2
    processname = $11
    command = $11
    for (i = 12; i < NF; i++) {
        command = command " " $i
    }

    gsub(/,/,"", command)
    command = substr(command,1,100)
    pstags["process-name"] = processname

    writeDoubleMetric("process-cpu", pstags, "gauge", cpu, "false")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("process-memory", pstags, "gauge", memory, "false")  # Converted to new syntax by change_ind_scripts.py script
}