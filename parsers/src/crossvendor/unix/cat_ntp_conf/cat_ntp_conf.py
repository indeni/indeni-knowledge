from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class UnixCatNtpConf(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            parsed_data = helper_methods.parse_data_as_list(raw_data, 'cat_etc_ntp_conf.textfsm')
            if parsed_data:
                ntp_servers = []
                for entry in parsed_data :
                    line = {'ipaddress' :  entry['ntp_server']}
                    ntp_servers.append(line)
                    sorted_ntp_servers = sorted(ntp_servers, key=lambda d: d['ipaddress'])
                self.write_complex_metric_object_array('ntp-servers', {}, sorted_ntp_servers, False)
        return self.output