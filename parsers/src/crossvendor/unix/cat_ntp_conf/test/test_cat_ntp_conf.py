import os
import unittest

from crossvendor.unix.cat_ntp_conf.cat_ntp_conf import UnixCatNtpConf



class TestUnixCatNtpConf(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = UnixCatNtpConf()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_f5_default_config(self):
        result = self.parser.parse_file(self.current_dir + '/f5_default_config.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].name,'ntp-servers')
        self.assertEqual(result[0].action_type,'WriteComplexMetric')
        self.assertEqual(1, len(result[0].value))
        self.assertEqual(result[0].value[0]['ipaddress'],'127.127.1.0')


    def test_multiple_servers_configured(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_multiple_servers_configured.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].name,'ntp-servers')
        self.assertEqual(result[0].action_type,'WriteComplexMetric')
        self.assertEqual(2, len(result[0].value))
        self.assertEqual(result[0].value[0]['ipaddress'],'10.11.80.50')
        self.assertEqual(result[0].value[1]['ipaddress'],'8.8.8.8')

    def test_single_server_configured(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_single_server_configured.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].name,'ntp-servers')
        self.assertEqual(result[0].action_type,'WriteComplexMetric')
        self.assertEqual(1, len(result[0].value))
        self.assertEqual(result[0].value[0]['ipaddress'],'10.11.80.50')



if __name__ == '__main__':
    unittest.main()