#Filesystem            Inodes   IUsed   IFree IUse% Mounted on
#/dev/mapper/vg_splat-lv_current
#                     1179648  121647 1058001   11% /
#proc                       0       0       0    -  /proc
#sysfs                      0       0       0    -  /sys
#devpts                     0       0       0    -  /dev/pts
#/dev/sda1              76304      53   76251    1% /boot
#tmpfs                 450304       1  450303    1% /dev/shm
#/dev/mapper/vg_splat-lv_log
#                     1835008   11867 1823141    1% /var/log
#none                       0       0       0    -  /proc/sys/fs/binfmt_misc

/\s+[0-9]+\s+[0-9]+\s/ {
    if ($1 ~ /^[0-9]/) {
        used = $2
        total = $1
        tags["partition"] = $5
    } else {
        used = $3
        total = $2
        tags["partition"] = $6
    }
    if (total != 0) {
        writeDoubleMetric("inode-total-count", tags, "gauge", total, "true", "Inode - Total", "number", "partition")  # Converted to new syntax by change_ind_scripts.py script
        writeDoubleMetric("inode-used-count", tags, "gauge", used, "true", "Inode - Used", "number", "partition")  # Converted to new syntax by change_ind_scripts.py script
    }
}