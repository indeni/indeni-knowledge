###############
# The -n is to not resolve the IPs with DNS.
###############

# ? (10.3.3.62) at 00:50:56:80:79:BA [ether] on eth0
# ? (10.3.3.211) at <incomplete> on eth0
# ? (10.3.3.211) at (incomplete)
/at/ {
    totalArps++
    targetip = $2
    gsub(/[\(\)]/, "", targetip)
    mac = $4

    iarp++

    arps[iarp, "targetip"]=targetip
    if (mac !~ /.*incomplete.*/) {
        arps[iarp, "mac"] = mac
        arps[iarp, "success"] = "1"
    } else {
        arps[iarp, "success"] = "0"
    }

    if ($0 ~ / on /) {
        interface = $NF
        arps[iarp, "interface"]=interface
    }
}

# ? (10.10.6.254) at 00:12:12:12:12:12 [ether] PERM on eth2
# ? (172.16.20.231) at 0:c:29:c0:94:bf permanent
/(PERM on|permanent)/ {
    iarp++
    targetip = $2
    gsub(/[\(\)]/, "", targetip)
    mac = $4
	
    staticArp[iarp, "ip-address"] = targetip
    staticArp[iarp, "mac-address"] = mac
}

END {
    writeDoubleMetric("arp-total-entries", null, "gauge", totalArps, "false")  # Converted to new syntax by change_ind_scripts.py script
    writeComplexMetricObjectArray("arp-table", null, arps, "false")  # Converted to new syntax by change_ind_scripts.py script
    writeComplexMetricObjectArray("static-arp", null, staticArp, "false")  # Converted to new syntax by change_ind_scripts.py script
}