############
# Script explanation: 
# The mount point info could be on a single line, or two, depending on the filesystem's name:
# But we don't care about the filesystem, so we can ignore that.
############


# 10157368   6384800   3248280  67% /
# /dev/sda1  295561     24017    256284   9% /boot
/(\d+)%/ {
    mount = $NF
    usage = $(NF-1)
    sub(/%/, "", usage)
    available = $(NF-2)
    used = $(NF-3)
	total = $(NF-4)

    mounttags["file-system"] = mount

    writeDoubleMetric("disk-usage-percentage", mounttags, "gauge", usage, "true", "Mount Points - Usage", "percentage", "file-system")  # Converted to new syntax by change_ind_scripts.py script
	writeDoubleMetric("disk-used-kbytes", mounttags, "gauge", used, "true", "Mount Points - Used", "kbytes", "file-system")  # Converted to new syntax by change_ind_scripts.py script
	writeDoubleMetric("disk-total-kbytes", mounttags, "gauge", total, "true", "Mount Points - Total", "kbytes", "file-system")  # Converted to new syntax by change_ind_scripts.py script
}