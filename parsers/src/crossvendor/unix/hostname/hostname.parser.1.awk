# 2/20/19: reduced monitoring interval to 50 minutes. We have noticed that the 60 minute interval causes problems.
# The indeni-collector was trying to schedule the script to run every 60 mins, but was sometimes unable
# to hit the mark (e.g., if other scripts in the pipe delayed it). Since the timeseries database currently
# retains only the last 60 minutes of data, sometimes the database would end up with no data.

#lab-CP-GW1
/^[a-zA-Z0-9]/ {
    hostname = $1
}

END {
    writeComplexMetricString("hostname", null, hostname, "true", "Host Name")  # Converted to new syntax by change_ind_scripts.py script
}