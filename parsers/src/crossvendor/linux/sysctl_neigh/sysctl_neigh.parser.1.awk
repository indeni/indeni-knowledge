# net.ipv4.neigh.default.gc_thresh3 = 4096
/gc_thresh3/ {
    writeDoubleMetric("arp-limit", null, "gauge", $NF, "true", "ARP Cache - Limit", "number", "")  # Converted to new syntax by change_ind_scripts.py script
}