###### NOTES #########
# The script identifies when the kernel loops in kernel mode for more than 20 seconds, not allowing a CPU to be used for other resources.
# The script looks at the /var/log/messages file to find any entries over the last 20 minutes with parameter "soft lockup"
# If more than 1 entry is found, we write identify that a lock up has occured
# Due to the behavior being erratic and unpredictable, the frequency is set to 2 minutes

# Here, we establish a baseline, in case that no log entries are found.

BEGIN {
    log_entries = 0
}


#An example of a lock up below
#Jun 20 13:02:18 [remote host] kernel: BUG: soft lockup - CPU#3 stuck for 10s! [cphaconf:12533]

#Ignoring grep to exclude false positives for the following line
#Sep 12 12:33:57 2018 newf2ext shell: cmd by srvindeni: awk '$0>=from' from="$(date +%b" "%e" "%H:%M:%S -d -10min)" /var/log/messages | grep "soft lockup"
/soft lockup/ && !/grep/ {
    log_entries++
}

END {
    if (log_entries > 0) {
        cores_locked = 1

    } else {
        cores_locked = 0
    }
    writeDoubleMetric("cores-locked", null, "gauge", cores_locked, "false")  # Converted to new syntax by change_ind_scripts.py script
}