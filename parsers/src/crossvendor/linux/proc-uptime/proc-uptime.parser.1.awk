############
# Script explanation: /proc/uptime is used instead of the command "uptime" since /proc/uptime has a higher granularity, since it shows uptime in seconds.
############

#896218.37 3217298.93
/^[0-9]/ {
	uptime = $1 * 1000
	writeDoubleMetric("uptime-milliseconds", null, "gauge", uptime, "true", "Uptime", "duration", "")  # Converted to new syntax by change_ind_scripts.py script
}