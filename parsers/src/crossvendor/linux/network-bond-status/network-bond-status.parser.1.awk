# bond48
/bond/ {
	bond=$1
}

# Slave Interface: eth1-02
/Slave Interface/ {
	interfaceName=$3
}

# Aggregator ID: 1
/^Aggregator ID/ {
	aggregatorId=$3	

	nictags["interface-name"] = interfaceName
	nictags["bond-name"] = bond
	writeComplexMetricString("interface-aggregator-id", nictags, aggregatorId, "false")  # Converted to new syntax by change_ind_scripts.py script
}