#CPU    %usr   %nice    %sys %iowait    %irq   %soft  %steal  %guest  %gnice   %idle
#CPU   %user   %nice    %sys %iowait    %irq   %soft  %steal   %idle    intr/s
/(CPU|%usr|%nice)/ {
	# Parse the line into a column array. The second parameter is the separator between column names.
	# The last parameter is the array to save the data into.
	#gsub(/%/,"",$0)
	getColumns(trim($0), "[ ]+", columns)
}

#all    0.98    0.00    0.98    0.00    0.00    0.00    0.00    0.00   98.04
#0    0.98    0.00    0.98    0.00    0.00    0.00    0.00    0.00   98.04
#1    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00
/[0-9]/ {

	# Use getColData to parse out the data for the specific column from the current line. The current line will be
	# split according to the same separator we've passed in the getColumns function (it's stored in the "columns" variable).
	# If the column cannot be found, the result of getColData is null (not "null").
	cpu["usr"] = getColData(trim($0), columns, "%usr")
	if (cpu["usr"] == "") {
		# The "usr" column name could sometimes be "user"
		cpu["usr"] = getColData(trim($0), columns, "%user")
	}
	cpu["nice"] = getColData(trim($0), columns, "%nice")
	cpu["sys"] = getColData(trim($0), columns, "%sys")
	cpu["iowait"] = getColData(trim($0), columns, "%iowait")
	cpu["irq"] = getColData(trim($0), columns, "%irq")
	cpu["soft"] = getColData(trim($0), columns, "%soft")
	cpu["steal"] = getColData(trim($0), columns, "%steal")
    cpu["guest"] = getColData(trim($0), columns, "%guest")
	cpu["gnice"] = getColData(trim($0), columns, "%gnice")
	
	cpuId = $1
	
	
	# Adding together the total usage
	for (id in cpu) {
		usage = usage + cpu[id]
	}
	
	# Sometimes idle can be 0% but usage is also 0%. So we will not use the idle column for measurements as we see it as possible faulty.
	#usage = $2 + $3 + $4 + $5 + $6 + $7 + $8
	idle = 100 - usage

    cputags["cpu-id"] = cpuId
    if (cpuId == "all") {
        cputags["cpu-is-avg"] = "true"
		cputags["cpu-id"] = "all-average"
		cputags["resource-metric"] = "true"
    } else {
        cputags["cpu-is-avg"] = "false"
        cputags["resource-metric"] = "false"
    }

    writeDoubleMetric("cpu-usage", cputags, "gauge", usage, "true", "CPU", "percentage", "cpu-id")  # Converted to new syntax by change_ind_scripts.py script
	
	delete cpu
	usage = ""
}