# Function to calculate number of binary 1s in a decimal number
function count1s(N) {
	r = ""                    # initialize result to empty (not 0)
	while(N != 0){            # as long as number still has a value
		r = ((N%2)?"1":"0") r   # prepend the modulos2 to the result
		N = int(N/2)            # shift right (integer division by 2)
	}

	# count number of 1s
	r = gsub(/1/,"",r)
	# Return result
	return r
}


# Function to convert a subnetmask (example: 255.255.255.0) to subnet prefix (example: 24)
function subnetmaskToPrefix(subnetmask) {
	split(subnetmask, v, "\\.")
	prefix = count1s(v[1]) + count1s(v[2]) + count1s(v[3]) + count1s(v[4])
	return prefix
}


# 10.11.2.0       0.0.0.0         255.255.255.0   U         0 0          0 eth1
/^(?:[0-9]{1,3}\.){3}[0-9]{1,3}/ {
	destination = $1
	mask = $3
	subnetprefix = subnetmaskToPrefix(mask)
	flags = $4
	gateway = $2
	
	# If its a direct connected network route
	if (gateway == "0.0.0.0") {
		iDirectRoute++
		
		directRoutes[iDirectRoute, "network"] = destination
		directRoutes[iDirectRoute, "mask"] = subnetprefix
	}

	# If its not a directly connected network
	if (gateway != "0.0.0.0") {
		iStaticRoute++
		
		staticRoutes[iStaticRoute, "network"] = destination
		staticRoutes[iStaticRoute, "mask"] = subnetprefix
		staticRoutes[iStaticRoute, "next-hop"] = gateway
	}
}

END {
	writeComplexMetricObjectArray("static-routing-table", null, staticRoutes, "true", "Static routes")  # Converted to new syntax by change_ind_scripts.py script
	writeComplexMetricObjectArray("connected-networks-table", null, directRoutes, "true", "Directly Connected Networks")  # Converted to new syntax by change_ind_scripts.py script
}