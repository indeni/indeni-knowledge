import os
import unittest

from crossvendor.linux.etc_os_release.etc_os_release import EtcOsRelease
from parser_service.public.action import WriteDoubleMetric


class TestEtcOsRelease(unittest.TestCase):
    def setUp(self):
        self.parser = EtcOsRelease()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_f5_v17_1_0(self):
        result = self.parser.parse_file(self.current_dir + '/f5_v17.1.0.input', {}, {})
        self.assertEqual(3, len(result))
        self.assertEqual(result[0].key, 'vendor')
        self.assertEqual(result[0].value, 'f5')
        self.assertEqual(result[1].key, 'product')
        self.assertEqual(result[1].value, 'load-balancer')
        self.assertEqual(result[2].key, 'shell')
        self.assertEqual(result[2].value, 'bash')

    def test_rhel_v7_9(self):
        result = self.parser.parse_file(self.current_dir + '/rhel_v7_9.input', {}, {})
        self.assertEqual(4, len(result))
        self.assertEqual(result[0].key, 'os.name')
        self.assertEqual(result[0].value, 'rhel')
        self.assertEqual(result[1].key, 'os.version')
        self.assertEqual(result[1].value, 'Red Hat Enterprise Linux 7.9')
        self.assertEqual(result[2].key, 'vendor')
        self.assertEqual(result[2].value, 'zscaler')
        self.assertEqual(result[3].key, 'nice-path')
        self.assertEqual(result[3].value, '/usr/bin/nice')

    def test_rhel_v8_4(self):
        result = self.parser.parse_file(self.current_dir + '/rhel_v8_4.input', {}, {})
        self.assertEqual(4, len(result))
        self.assertEqual(result[0].key, 'os.name')
        self.assertEqual(result[0].value, 'rhel')
        self.assertEqual(result[1].key, 'os.version')
        self.assertEqual(result[1].value, 'Red Hat Enterprise Linux 8.4 (Ootpa) 8.4')
        self.assertEqual(result[2].key, 'vendor')
        self.assertEqual(result[2].value, 'zscaler')
        self.assertEqual(result[3].key, 'nice-path')
        self.assertEqual(result[3].value, '/usr/bin/nice')


if __name__ == '__main__':
    unittest.main()