from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

flavours_nice = {
    'ubuntu': '/bin/nice',
    'rhel': '/usr/bin/nice'
}

class EtcOsRelease(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        # Step 1 : Data Extraction
        if raw_data:
            services_data = helper_methods.parse_data_as_list(raw_data, 'system_ctl_list_units.textfsm')
            parsed_data = helper_methods.parse_data_as_list(raw_data, 'linux_etc_os_release.textfsm')
            tmsh_data = helper_methods.parse_data_as_list(raw_data, 'tmsh_show_sys_version.textfsm')
            services = [x['name'] for x in services_data]
            if parsed_data:
                bluecat_service_descriptions = [x['description'] for x in services_data if 'bluecat' in x['description'].lower()]
                if tmsh_data and tmsh_data[0]['product'] == 'BIG-IP':
                    self.write_tag('vendor', 'f5')
                    self.write_tag('product', 'load-balancer')
                    self.write_tag('shell', 'bash')
                elif bluecat_service_descriptions :
                    pass
                else:
                    if device_tags.get('os.name') is None:
                        if 'zpa-connector' in services:
                            self.write_tag('os.name', parsed_data[0]['os_flavour'])
                        else:
                            self.write_tag('os.name', parsed_data[0]['os_name'])
                    if device_tags.get('os.version') is None:
                        if 'zpa-connector' in services:
                            self.write_tag('os.version', '{} {}'.format(parsed_data[0]['os_name'],parsed_data[0]['os_version']))
                        else:
                            self.write_tag('os.version', parsed_data[0]['os_version'])
                    if device_tags.get('vendor') is None:
                        if 'zpa-connector' in services:
                            self.write_tag('vendor', 'zscaler')
                        else:
                            self.write_tag('vendor', parsed_data[0]['os_flavour'])
                    if parsed_data[0]['os_flavour'] in flavours_nice.keys():
                        self.write_tag('nice-path', flavours_nice[parsed_data[0]['os_flavour']])
                    else:
                        self.write_tag('nice-path', '/bin/nice')
            return self.output