#This check is not executed on F5 devices as their architecture with tmos gives a skewed picture.
#Instead we monitor the memory usage via the rest interface.

/total.*used.*available/ {
    # https://askubuntu.com/questions/770108/what-do-the-changes-in-free-output-from-14-04-to-16-04-mean
    new_mode = 1
}

#Mem:          1959       1921         37          0        258       1063
/Mem:/ {
    total_ram = $2

    if (new_mode == 1) {
        free_ram = $7
        used_ram = (total_ram - free_ram)
    }
}


#-/+ buffers/cache:     613980    1392056
/buffers\/cache/ {
    free_ram = $4
    used_ram = (total_ram - free_ram)
}



#Swap:      4225084        108    4224976
/Swap/ {
    total_swap = $2
    used_swap = $3
    free_swap = $4

    # if swap is 0 then the calculation will fail since the result will be "NaN"
    if (total_swap != 0) {
        swaptag["name"] = "swap"
        usage_swap = (used_swap / total_swap) * 100
        writeDoubleMetric("memory-free-kbytes", swaptag, "gauge", free_swap, "true", "Memory - Free", "kbytes", "name")  # Converted to new syntax by change_ind_scripts.py script
        writeDoubleMetric("memory-total-kbytes", swaptag, "gauge", total_swap, "true", "Memory - Total", "kbytes", "name")  # Converted to new syntax by change_ind_scripts.py script

        swaptag_memory_usage["name"] = "swap"
        swaptag_memory_usage["resource-metric"] = "true"
        writeDoubleMetric("memory-usage", swaptag_memory_usage, "gauge", usage_swap, "true", "Memory - Usage", "percentage", "name")  # Converted to new syntax by change_ind_scripts.py script
    }
}

END {
    if (free_ram && used_ram) {
        ramtag["name"] = "RAM"
        usage_ram = (used_ram / total_ram) * 100
        writeDoubleMetric("memory-free-kbytes", ramtag, "gauge", free_ram, "true", "Memory - Free", "kbytes", "name")  # Converted to new syntax by change_ind_scripts.py script
        writeDoubleMetric("memory-total-kbytes", ramtag, "gauge", total_ram, "true", "Memory - Total", "kbytes", "name")  # Converted to new syntax by change_ind_scripts.py script

        ramtag_memory_usage["name"] = "RAM"
        ramtag_memory_usage["resource-metric"] = "true"
        writeDoubleMetric("memory-usage", ramtag_memory_usage, "gauge", usage_ram, "true", "Memory - Usage", "percentage", "name")  # Converted to new syntax by change_ind_scripts.py script
    }
}