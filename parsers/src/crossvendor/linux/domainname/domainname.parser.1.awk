#test.local
/^[a-zA-Z0-9]/ {
	domainname = $1
}

END {
        if (domainname != "") {
	    writeComplexMetricString("domain", null, domainname, "true", "Domain Name")  # Converted to new syntax by change_ind_scripts.py script
        }
}