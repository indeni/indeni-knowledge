# USER                             TTY      FROM              LOGIN@  IDLE WHAT
/USER.*TTY/ {
    foundColumns = "true"
    getColumns(trim($0), "[ \t]+", columns)
}

# indeni                           d0       -                10:36AM 11:17 -csh (csh)
# indeni                           p0       192.168.201.8     9:43PM     - w
/[0-9]/ {
    # Make sure we already initialized the columns for the table. There may be lines before
    # the table, so this is a way to ignore them
    if (foundColumns == "true") {
        iuser++

        username = getColData(trim($0), columns, "USER")
        from = getColData(trim($0), columns, "FROM")

        if (from == "-") {
            from = "console"
        }

        users[iuser, "username"] = username
        users[iuser, "from"] = from

        idle = getColData(trim($0), columns, "IDLE")

        # The logic below was copied from the linux version of this script.

        # Here we count number of seconds of idle time
        # w uses several time formats
        # DDdays, HH:MMm, MM:SS, SS.CCs
        
        # 2days
        secondsIdle = 0
        if (idle ~ /days/) {
            
            split(idle, idleArrDays, "days")
            secondsIdle = idleArrDays[1] * 86400    

        # 7:13m
        } else if (idle ~ /m/) {
            split(idle, idleArrMin, "m")
            split(idleArrMin[1], idleArrSplit, ":")
            secondsIdle = idleArrSplit[1] * 3600 + idleArrSplit[2] * 60

        # 11.00s
        } else if (idle ~ /s/) {
            split(idle, idleArrSec, "\\.")
            secondsIdle = idleArrSec[1] 

        } else if (idle ~ /:/) {
            split(idle, idleArr, ":")
            secondsIdle = idleArr[1] * 60 + idleArr[2]
        }

        users[iuser, "idle"] = secondsIdle
    }
}

END {
   writeComplexMetricObjectArray("logged-in-users", null, users, "false")  # Converted to new syntax by change_ind_scripts.py script
}