# Device          1K-blocks     Used    Avail Capacity
/Device/ {
	getColumns(trim($0), "[ \t]+", columns)	
}

# /dev/mirror/gmroots1b   8388608        0  8388608     0%
/[0-9]%/ {
	swaptag["name"] = "swap"
	totalSwap = getColData(trim($0), columns, "1K-blocks")
	usedSwap = getColData(trim($0), columns, "Used")
	freeSwap = getColData(trim($0), columns, "Avail")
	
	# if swap is 0 then the calculation will fail since the result will be "NaN"
	if (totalSwap != 0) {
		usageSwap = (usedSwap / totalSwap) * 100
		writeDoubleMetric("memory-usage", swaptag, "gauge", usageSwap, "true", "Memory - Usage", "kbytes", "name")  # Converted to new syntax by change_ind_scripts.py script
		writeDoubleMetric("memory-free-kbytes", swaptag, "gauge", freeSwap, "true", "Memory - Free", "kbytes", "name")  # Converted to new syntax by change_ind_scripts.py script
		writeDoubleMetric("memory-total-kbytes", swaptag, "gauge", totalSwap, "true", "Memory - Total", "kbytes", "name")  # Converted to new syntax by change_ind_scripts.py script
	}
}