#curdate: 1503135122
/^curdate:\s/ {
    curDate = $NF
}

#boottime: 1503077738
/^boottime:\s/ {
    bootTime = $NF
}

END {
    if (curDate && bootTime) {
        uptime = (curDate - bootTime) * 1000
        writeDoubleMetric("uptime-milliseconds", null, "gauge", uptime, "true", "Uptime", "duration", "")  # Converted to new syntax by change_ind_scripts.py script
    }
}