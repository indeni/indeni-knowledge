# Destination        Type  Ref NextHop            Type  Ref Index    Interface
/Destination.*Type/ {
	# The list of columns has "Type" twice. So we cheat and replace the first one.
	line = $0
	sub(/Type/, "DestType", line) # Only replace the first instance of Type
	getColumns(trim(line), "[ \t]+", columns)
}

#################################
# The DestType's:
#     user  Installed by routing protocol process
#     kern  Installed by kernel
#     perm  Installed by kernel when routing table is initialized
#     dest  Directly reachable through an interface
#     clon  A clone route
#     cach  A cache maker route
#     unkn  Route unknown
#
# The type of the nexthop:
#     dscd  Causes packets to be dropped rather than forward but network
#           unreachable not generated
#     rjct  Causes packets to be dropped rather than forward and network
#           unreachable are sent to the packet originators
#     locl  Nexthop is for local addresses. Packets are sent to the local
#           protocl stack.
#     dest  Nexthop address corresponds to a directly connected neighbour.
#     recv  Nexthop type is currently unused
#     mcst  Nexthop is a multicast packet distribution
#     mgrp  Nexthop for packets sent to multicast groups
#     ulst  Nexthop is a set of unicast next-hops
#     rslv  Nexthop is resolving or is resolved in user space
#     tunl  Nexthop is a tunnel overlay
#     hold  Nexthop is waiting to be resolved into unicast or multicast type
#     intf  Nexthop directly to interface
#################################

# default            user    0 172.16.20.1        dest    3    65        eth2c0
# 0.0.0.0            perm    0                    locl    2    41
# 172.16.16/24       dest    0                    rslv    1    61        eth1c0
# 172.16.16.30       dest    0 172.16.16.30       locl    1    62        eth1c0
# 224/4              perm    0                    rjct    3    44
/(user|kern|perm|dest|clon|cach|unkn)/ {
	shorthandDestination = getColData(trim($0), columns, "Destination")

	if (shorthandDestination ~ /\//) {
		split(shorthandDestination, destinationParts, "/")
		destination = destinationParts[1]
		subnetprefix = destinationParts[2]

		# FreeBSD's netstat shortens the destination, we need to pad it:
		if (subnetprefix <= 8) {
			destination = destination ".0.0.0"
		} else if (subnetprefix <= 16) {
			destination = destination ".0.0"
		} else if (subnetprefix <= 24) {
			destination = destination ".0"
		}
	} else if (shorthandDestination == "default") {
		destination = "0.0.0.0"
		subnetprefix = 0
	} else {
		# No subnet in destination means it's /32
		destination = shorthandDestination
		subnetprefix = 32
	}

	destType = getColData(trim($0), columns, "DestType")
	nexthopType = getColData(trim($0), columns, "Type")

	# If its a direct connected network route
	if (destType == "dest") {
		iDirectRoute++
		
		directRoutes[iDirectRoute, "network"] = destination
		directRoutes[iDirectRoute, "mask"] = subnetprefix
	}

	# If its not a directly connected network
	if (destType != "dest") {
		iStaticRoute++

		gateway = getColData(trim($0), columns, "NextHop")
		
		# The gateway should be an IP address (v4 or v6). If it's not, it means the NextHop is empty and we
		# actually grabbed a different column.
		if ((gateway ~ /\./ || gateway ~ /\:/) && (subnetprefix < 32)) {
			staticRoutes[iStaticRoute, "network"] = destination
			staticRoutes[iStaticRoute, "mask"] = subnetprefix
			staticRoutes[iStaticRoute, "next-hop"] = gateway
		}
	}
}

END {
	writeComplexMetricObjectArray("static-routing-table", null, staticRoutes, "true", "Static routes")  # Converted to new syntax by change_ind_scripts.py script
	writeComplexMetricObjectArray("connected-networks-table", null, directRoutes, "true", "Directly Connected Networks")  # Converted to new syntax by change_ind_scripts.py script
}