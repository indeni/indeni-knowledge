import os
import ipaddress
import re
from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class BluecatBddsProbeDnsStep1(BaseParser):
    """
    Parser of:
        ACTIVE NODE
            node get-notify state=NODE_HA_ACTIVE
            node get-notify dns-enable=1
            node get-cmplt retcode=ok
        PASSIVE NODE
            node get-notify state=NODE_HA_PASSIVE
            node get-notify dns-enable=1
            node get-cmplt retcode=ok
        STANDALONE
            node get-notify state=NODE_SA_ACTIVE
            node get-notify dns-enable=1
            node get-cmplt retcode=ok
    """


    
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        TAGS_WITH_DASH = ['ip-address', 'role-dns', 'role-dhcp', 'cluster-id', 'high-availability', 'linux-based', 'server-id', 'nice-path'] # This list of modified tags could be extended
        TAGS_WITH_DASH_CHANGED = [tag.replace('-','_') for tag in TAGS_WITH_DASH]  
        if raw_data:
            data = helper_methods.parse_data_as_list(raw_data, 'bluecat_psclient_node_get.textfsm')
            if data:
                if data[0]['dns_enable'] == '1':
                    config_file = os.path.dirname(os.path.realpath(__file__)) + '/dns_probe.config.yaml'
                    if not os.path.isfile(config_file):
                        pass
                    else:
                        with open(config_file) as f:
                            content = f.read()
                            for tag_to_replace in TAGS_WITH_DASH:   # Replace temporaly hte dash in the tags as are not allowed as key in YAML
                                content = content.replace(tag_to_replace,tag_to_replace.replace('-','_')) 
                            config_yaml = helper_methods.parse_data_as_yaml(content)
                            serialized_config_yaml = str(config_yaml)
                            for tag_to_replace_back in TAGS_WITH_DASH_CHANGED:
                                serialized_config_yaml = serialized_config_yaml.replace(tag_to_replace_back,tag_to_replace_back.replace('_','-'))
                            config_yaml = eval(serialized_config_yaml)
                            this_device_probes = []
                            for test in config_yaml:
                                if self.requirements_all_check(test['requirements'], device_tags):
                                    for dns_test in test['probes']['dns']:
                                        this_device_probes.append(dns_test)
                            if this_device_probes != []:
                                for entry in this_device_probes:
                                    if entry.get('type'):
                                        self.write_dynamic_variable('command_parameters', f"dig +short @localhost {entry['url']} {entry['type']}")
                                    else:
                                        self.write_dynamic_variable('command_parameters', f"dig +short @localhost {entry['url']}")
                            else:
                                self.write_dynamic_variable('command_parameters', "sleep 1")
                                    
        return self.output


    def requirement_single_check (self,requirements:dict, device_tags:dict ):
        for tag in requirements.keys():
            if isinstance(requirements[tag], dict) and requirements[tag].get('exists') is not None:
                if device_tags.get(tag):
                    if str(requirements[tag].get('exists')).lower() == 'true':
                        return True
                    return False
                if str(requirements[tag].get('exists')).lower() == 'false':
                    return True
                return False
            if isinstance(requirements[tag], dict) and requirements[tag].get('compare-type') is not None:
                if requirements[tag].get('regex') is not None and device_tags.get(tag) is not None:
                    return bool(re.search(requirements[tag].get('regex'), device_tags.get(tag)))
                if requirements[tag].get('compare-type') is not None and device_tags.get(tag) is not None:
                    return eval(f"{device_tags.get(tag)} {requirements[tag].get('op')} {requirements[tag].get('value')}")
                return False
            if tag == 'ip-address':
                if isinstance(requirements[tag], dict) and requirements[tag].get('neq') is not None:
                    if '/' in requirements[tag]['neq']:
                        return ipaddress.IPv4Address(device_tags['ip-address']) not in ipaddress.IPv4Network(requirements[tag])
                    return not device_tags.get('ip-address') == requirements[tag]
                if '/' in requirements[tag]:
                    return ipaddress.IPv4Address(device_tags['ip-address']) in ipaddress.IPv4Network(requirements[tag])
                return device_tags.get('ip-address') == requirements[tag]
            if isinstance(requirements[tag], dict) and requirements[tag].get('neq') is not None:
                return not str(device_tags.get(tag)).lower() == str(requirements[tag]['neq']).lower()
            return str(device_tags.get(tag)).lower() == str(requirements[tag]).lower()

    def requirement_or_check (self, requirements:dict, device_tags:dict ):
        eval_all_subrequirements = [self.requirement_or_check(item['or'],device_tags) if 'or' in item.keys() 
                                    else self.requirement_and_check(item['and'],device_tags) if 'and' in item.keys()
                                    else self.requirement_single_check(item,device_tags) for item in requirements]
        return True in eval_all_subrequirements 

    def requirement_and_check (self, requirements:dict, device_tags:dict ):
        eval_all_subrequirements = [self.requirement_or_check(item['or'],device_tags) if 'or' in item.keys()
                                    else self.requirement_and_check(item['and'],device_tags) if 'and' in item.keys()
                                    else self.requirement_single_check(item,device_tags) for item in requirements]
        return  all(i == True for i in eval_all_subrequirements)

    def requirements_all_check(self, requirements:dict, device_tags:dict):
        requirements_validation = []
        for tag in requirements.keys():
            if tag == 'or':
                or_validation = self.requirement_or_check (requirements[tag], device_tags)
                requirements_validation.append(or_validation)
            elif tag == 'and':
                and_validation = self.requirement_and_check (requirements[tag], device_tags)
                requirements_validation.append(and_validation)
            else:
                single_validation = self.requirement_single_check({tag: requirements[tag]}, device_tags)
                requirements_validation.append(single_validation)
            if not all(i == True for i in requirements_validation):
                break
        return all(i == True for i in requirements_validation)

