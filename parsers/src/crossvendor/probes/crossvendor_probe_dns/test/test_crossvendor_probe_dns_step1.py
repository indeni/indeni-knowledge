import os
import unittest
from crossvendor.probes.crossvendor_probe_dns.crossvendor_probe_dns_step1 import BluecatBddsProbeDnsStep1
from parser_service.public.action import *

class TestBluecatDnsServiceStatus(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = BluecatBddsProbeDnsStep1()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))    
    
    def test_crossvendor_probe_dns_step1_active_node_get(self):
        result = self.parser.parse_file(self.current_dir + '/crossvendor_probe_dns_step1_active_node_get.input', 
                                        {}, 
                                        {
                                            'os.name': 'bdds',
                                            'device-ip': '257.278.25.25',
                                            'role-dns' : 'true', 
                                            'hostname' : 'bc0a86405n', 
                                            'ssh' : 'true', 
                                            'cluster-id' : '192.168.100.3', 
                                            'linux-based' : 'true', 
                                            'xha' : 'true', 
                                            'os.version' : '9.5.1', 
                                            'high-availability' : 'true', 
                                            'role-dhcp' : 'true', 
                                            'copy' : 'true', 
                                            'ip-address' : '10.244.29.73', 
                                            'vendor' : 'bluecat', 
                                            'failover' : 'true', 
                                            'os.name' : 'bdds', 
                                            'server-id' : '42289655-81a0-f05b-edcf-2720cd420586', 
                                            'product' : 'integrity', 
                                            'nice-path' :'/bin/nice',
                                         }
                                        )
        self.assertEqual(4,len(result))
        self.assertEqual(result[0].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[0].value, 'dig +short @localhost indeni.com')
        self.assertEqual(result[1].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[1].value, 'dig +short @localhost www.f5.com SRV')
        self.assertEqual(result[2].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[2].value, 'dig +short @localhost www.checkpoint.com TXT')
        self.assertEqual(result[3].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[3].value, 'dig +short @localhost bluecatnetworks.corp TXT')

    def test_crossvendor_probe_dns_step1_active_node_get_wrong_req(self):
        result = self.parser.parse_file(self.current_dir + '/crossvendor_probe_dns_step1_active_node_get.input', {}, {'device-ip' : '257.278.25.25'})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[0].value, 'sleep 1')


    def test_crossvendor_probe_dns_step1_passive_node_get(self):
        result = self.parser.parse_file(self.current_dir + '/crossvendor_probe_dns_step1_passive_node_get.input', {}, {'device-ip' : '257.278.25.25'})
        self.assertEqual(1,len(result))


    def test_crossvendor_probe_dns_step1_standalone_node_get(self):
        result = self.parser.parse_file(self.current_dir + '/crossvendor_probe_dns_step1_passive_node_get.input', {}, {'device-ip' : '257.278.25.25'})
        self.assertEqual(1,len(result))


if __name__ == '__main__':
    unittest.main()