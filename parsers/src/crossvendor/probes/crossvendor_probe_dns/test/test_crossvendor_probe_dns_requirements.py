import os
import unittest
from crossvendor.probes.crossvendor_probe_dns.crossvendor_probe_dns_step1 import BluecatBddsProbeDnsStep1
from parser_service.public.action import *
from parser_service.public import helper_methods


class TestRequirements(unittest.TestCase):   
    
    def setUp(self):
        # Arrange
        self.parser = BluecatBddsProbeDnsStep1()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))    

    def test_requirements_with_single_requirement_string_single_tag_true(self):
        requirements_yaml = """
        - requirements:
            os.name: bdds"""
        device_tags = {
            'os.name': 'bdds'
        }
        requirements_parsed = helper_methods.parse_data_as_yaml(requirements_yaml)
        result = self.parser.requirements_all_check(requirements_parsed[0]['requirements'],device_tags)
        self.assertEqual(True,result)

    def test_requirements_with_single_neq_requirement_string_single_tag_true(self):
        requirements_yaml = """
        - requirements:
            os.name: 
                neq: checkpoint"""
        device_tags = {
            'os.name': 'bluecat'
        }
        requirements_parsed = helper_methods.parse_data_as_yaml(requirements_yaml)
        result = self.parser.requirements_all_check(requirements_parsed[0]['requirements'],device_tags)
        self.assertEqual(True,result)

    def test_requirements_with_single_neq_requirement_single_tag_false(self):
        requirements_yaml = """
        - requirements:
            os.name: 
                neq: bdds"""
        device_tags = {
            'os.name': 'bdds'
        }
        requirements_parsed = helper_methods.parse_data_as_yaml(requirements_yaml)
        result = self.parser.requirements_all_check(requirements_parsed[0]['requirements'],device_tags)
        self.assertEqual(False,result)

    def test_requirements_with_single_requirement_bool_single_tag_true_1(self):
        requirements_yaml = """
        - requirements:
            high-availability: True"""
        device_tags = {
            'high-availability': 'True'
        }
        requirements_parsed = helper_methods.parse_data_as_yaml(requirements_yaml)
        result = self.parser.requirements_all_check(requirements_parsed[0]['requirements'],device_tags)
        self.assertEqual(True,result)

    def test_requirements_with_single_requirement_bool_single_tag_true_2(self):
        requirements_yaml = """
        - requirements:
            high-availability: True"""
        device_tags = {
            'high-availability': 'true'
        }
        requirements_parsed = helper_methods.parse_data_as_yaml(requirements_yaml)
        result = self.parser.requirements_all_check(requirements_parsed[0]['requirements'],device_tags)
        self.assertEqual(True,result)

    def test_requirements_with_single_requirement_bool_single_tag_true_3(self):
        requirements_yaml = """
        - requirements:
            high-availability: true"""
        device_tags = {
            'high-availability': 'True'
        }
        requirements_parsed = helper_methods.parse_data_as_yaml(requirements_yaml)
        result = self.parser.requirements_all_check(requirements_parsed[0]['requirements'],device_tags)
        self.assertEqual(True,result)

    def test_requirements_with_single_requirement_bool_single_tag_true_4(self):
        requirements_yaml = """
        - requirements:
            high-availability: true"""
        device_tags = {
            'high-availability': 'true'
        }
        requirements_parsed = helper_methods.parse_data_as_yaml(requirements_yaml)
        result = self.parser.requirements_all_check(requirements_parsed[0]['requirements'],device_tags)
        self.assertEqual(True,result)
        
    def test_requirements_with_single_neq_requirement_bool_single_tag_true_1(self):
        requirements_yaml = """
        - requirements:
            high-availability: 
                neq: True"""
        device_tags = {
            'high-availability': 'True'
        }
        requirements_parsed = helper_methods.parse_data_as_yaml(requirements_yaml)
        result = self.parser.requirements_all_check(requirements_parsed[0]['requirements'],device_tags)
        self.assertEqual(False,result)

    def test_requirements_with_single_neq_requirement_bool_single_tag_true_2(self):
        requirements_yaml = """
        - requirements:
            high-availability: 
                neq: True"""
        device_tags = {
            'high-availability': 'true'
        }
        requirements_parsed = helper_methods.parse_data_as_yaml(requirements_yaml)
        result = self.parser.requirements_all_check(requirements_parsed[0]['requirements'],device_tags)
        self.assertEqual(False,result)

    def test_requirements_with_single_neq_requirement_bool_single_tag_true_3(self):
        requirements_yaml = """
        - requirements:
            high-availability: 
                neq: true"""
        device_tags = {
            'high-availability': 'True'
        }
        requirements_parsed = helper_methods.parse_data_as_yaml(requirements_yaml)
        result = self.parser.requirements_all_check(requirements_parsed[0]['requirements'],device_tags)
        self.assertEqual(False,result) 

    def test_requirements_with_single_neq_requirement_bool_single_tag_true_4(self):
        requirements_yaml = """
        - requirements:
            high-availability: 
                neq: true"""
        device_tags = {
            'high-availability': 'true'
        }
        requirements_parsed = helper_methods.parse_data_as_yaml(requirements_yaml)
        result = self.parser.requirements_all_check(requirements_parsed[0]['requirements'],device_tags)
        self.assertEqual(False,result) 

    def test_requirements_with_single_requirement_multiple_tags_true(self):
        requirements_yaml = """
        - requirements:
            os.name: bdds"""
        device_tags = {
            'os.name': 'bdds',
            'ip-address': '257.278.25.25'
        }
        requirements_parsed = helper_methods.parse_data_as_yaml(requirements_yaml)
        result = self.parser.requirements_all_check(requirements_parsed[0]['requirements'],device_tags)
        self.assertEqual(True,result)

    def test_requirements_with_multiple_requirement_multiple_tags_true(self):
        requirements_yaml = """
        - requirements:
            os.name: bdds
            ip-address: 257.278.25.25"""
        device_tags = {
            'os.name': 'bdds',
            'ip-address': '257.278.25.25'
        }
        requirements_parsed = helper_methods.parse_data_as_yaml(requirements_yaml)
        result = self.parser.requirements_all_check(requirements_parsed[0]['requirements'],device_tags)
        self.assertEqual(True,result)

    def test_requirements_with_or_single_true(self):
        requirements_yaml = """
        - requirements:
            os.name: bdds
            or:
              - ip-address : 257.278.25.25"""
        device_tags = {
            'os.name': 'bdds',
            'ip-address': '257.278.25.25'
        }
        requirements_parsed = helper_methods.parse_data_as_yaml(requirements_yaml)
        result = self.parser.requirements_all_check(requirements_parsed[0]['requirements'],device_tags)
        self.assertEqual(True,result)

    def test_requirements_with_or_single_false(self):
        requirements_yaml = """
        - requirements:
            os.name: bdds'
            or:
              - ip-address : 257.278.25.25"""
        device_tags = {
            'os.name': 'bdds',
            'ip-address': '257.278.25.23'
        }
        requirements_parsed = helper_methods.parse_data_as_yaml(requirements_yaml)
        result = self.parser.requirements_all_check(requirements_parsed[0]['requirements'],device_tags)
        self.assertEqual(False,result)

    def test_requirements_with_or_multiple_true_1(self):
        requirements_yaml = """
        - requirements:
            os.name: bdds
            or:
              - ip-address : 10.244.29.73
              - ip-address : 257.278.25.23"""
        device_tags = {
            'os.name': 'bdds',
            'role-dns' : 'true', 
            'hostname' : 'bc0a86405n', 
            'ssh' : 'true', 
            'cluster-id' : '192.168.100.3', 
            'linux-based' : 'true', 
            'xha' : 'true', 
            'os.version' : '9.5.1', 
            'high-availability' : 'true', 
            'role-dhcp' : 'true', 
            'copy' : 'true', 
            'ip-address' : '10.244.29.73', 
            'vendor' : 'bluecat', 
            'failover' : 'true', 
            'os.name' : 'bdds', 
            'server-id' : '42289655-81a0-f05b-edcf-2720cd420586', 
            'product' : 'integrity', 
            'nice-path' :'/bin/nice',
        }
        requirements_parsed = helper_methods.parse_data_as_yaml(requirements_yaml)
        result = self.parser.requirements_all_check(requirements_parsed[0]['requirements'],device_tags)
        self.assertEqual(True,result)

    def test_requirements_with_or_multiple_true_2(self):
        requirements_yaml = """
        - requirements:
            os.name: bdds
            or:
              - ip-address : 257.278.25.25
              - high-availability : true"""
        device_tags = {
            'os.name': 'bdds',
            'ip-address': '257.278.25.25'
        }
        requirements_parsed = helper_methods.parse_data_as_yaml(requirements_yaml)
        result = self.parser.requirements_all_check(requirements_parsed[0]['requirements'],device_tags)
        self.assertEqual(True,result)

    def test_requirements_with_or_multiple_false(self):
        requirements_yaml = """
        - requirements:
            os.name: bdds
            or:
              - ip-address : 257.278.25.27
              - ip-address : 257.278.25.23"""
        device_tags = {
            'os.name': 'bdds',
            'ip-address': '257.278.25.25'
        }
        requirements_parsed = helper_methods.parse_data_as_yaml(requirements_yaml)
        result = self.parser.requirements_all_check(requirements_parsed[0]['requirements'],device_tags)
        self.assertEqual(False,result)

    def test_requirements_with_and_multiple_true_1(self):
        requirements_yaml = """
        - requirements:
            os.name: bdds
            and:
              - ip-address : 257.278.25.25
              - high-availability : True"""
        device_tags = {
            'os.name': 'bdds',
            'ip-address': '257.278.25.25',
            'high-availability': 'True',
        }
        requirements_parsed = helper_methods.parse_data_as_yaml(requirements_yaml)
        result = self.parser.requirements_all_check(requirements_parsed[0]['requirements'],device_tags)
        self.assertEqual(True,result)

    def test_requirements_with_and_multiple_true_2(self):
        requirements_yaml = """
        - requirements:
            os.name: bdds
            and:
              - ip-address : 257.278.25.25
              - high-availability : True"""
        device_tags = {
            'os.name': 'bdds',
            'ip-address': '257.278.25.25',
            'high-availability': 'true',
        }
        requirements_parsed = helper_methods.parse_data_as_yaml(requirements_yaml)
        result = self.parser.requirements_all_check(requirements_parsed[0]['requirements'],device_tags)
        self.assertEqual(True,result)

    def test_requirements_with_and_multiple_true_3(self):
        requirements_yaml = """
        - requirements:
            os.name: bdds
            and:
              - ip-address : 257.278.25.25
              - high-availability : true"""
        device_tags = {
            'os.name': 'bdds',
            'ip-address': '257.278.25.25',
            'high-availability': 'True',
        }
        requirements_parsed = helper_methods.parse_data_as_yaml(requirements_yaml)
        result = self.parser.requirements_all_check(requirements_parsed[0]['requirements'],device_tags)
        self.assertEqual(True,result)

    def test_requirements_with_and_multiple_false_1(self):
        requirements_yaml = """
        - requirements:
            os.name: bdds
            and:
              - ip-address : 257.278.25.26
              - high-availability : True"""
        device_tags = {
            'os.name': 'bdds',
            'ip-address': '257.278.25.25',
            'high-availability': 'True',
        }
        requirements_parsed = helper_methods.parse_data_as_yaml(requirements_yaml)
        result = self.parser.requirements_all_check(requirements_parsed[0]['requirements'],device_tags)
        self.assertEqual(False,result)

    def test_requirements_with_and_multiple_false_2(self):
        requirements_yaml = """
        - requirements:
            os.name: bdds
            and:
              - ip-address : 257.278.25.26
              - high-availability : True"""
        device_tags = {
            'os.name': 'bdds',
            'ip-address': '257.278.25.25',
            'high-availability': 'True',
        }
        requirements_parsed = helper_methods.parse_data_as_yaml(requirements_yaml)
        result = self.parser.requirements_all_check(requirements_parsed[0]['requirements'],device_tags)
        self.assertEqual(False,result)

    def test_requirements_with_and_multiple_false_3(self):
        requirements_yaml = """
        - requirements:
            os.name: bdds
            and:
              - ip-address : 257.278.25.25
              - high-availability : True"""
        device_tags = {
            'os.name': 'bdds',
            'ip-address': '257.278.25.25',
            'high-availability': 'False',
        }
        requirements_parsed = helper_methods.parse_data_as_yaml(requirements_yaml)
        result = self.parser.requirements_all_check(requirements_parsed[0]['requirements'],device_tags)
        self.assertEqual(False,result)

    def test_requirements_with_and_multiple_false_4(self):
        requirements_yaml = """
        - requirements:
            os.name: checkpoint
            and:
              - ip-address : 257.278.25.25
              - high-availability : True"""
        device_tags = {
            'os.name': 'bdds',
            'ip-address': '257.278.25.25',
            'high-availability': 'False',
        }
        requirements_parsed = helper_methods.parse_data_as_yaml(requirements_yaml)
        result = self.parser.requirements_all_check(requirements_parsed[0]['requirements'],device_tags)
        self.assertEqual(False,result)
        
        
    def test_requirements_complex_1(self):
        requirements_yaml = """
        - requirements:
            or:
                - and:
                    - vendor: checkpoint
                    - os.name: gaia
                - vendor: f5
                - and:
                    - ip-address : '10.255.253.128/25'
                    - vendor: bluecat
                    - os.name: bdds"""
        device_tags = {
            'vendor': 'bluecat',
            'ip-address': '10.255.253.200',
            'os.name': 'bdds',
        }
        requirements_parsed = helper_methods.parse_data_as_yaml(requirements_yaml)
        result = self.parser.requirements_all_check(requirements_parsed[0]['requirements'],device_tags)
        self.assertEqual(True,result)

    def test_requirements_complex_2(self):
        requirements_yaml = """
        - requirements:
            or:
                - and:
                    - vendor: checkpoint
                    - os.name: gaia
                - vendor: f5
                - and:
                    - ip-address : '10.255.253.128/25'
                    - vendor: bluecat
                    - os.name: bdds"""
        device_tags = {
            'vendor': 'bluecat',
            'ip-address': '10.255.253.90',
            'os.name': 'bdds',
        }
        requirements_parsed = helper_methods.parse_data_as_yaml(requirements_yaml)
        result = self.parser.requirements_all_check(requirements_parsed[0]['requirements'],device_tags)
        self.assertEqual(False,result)

        
    def test_requirements_complex_3(self):
        requirements_yaml = """
        - requirements:
            or:
                - and:
                    - vendor: checkpoint
                    - os.name: gaia
                - vendor: f5
                - and:
                    - ip-address : '10.255.253.128/25'
                    - vendor: bluecat
                    - os.name: bdds
            os.version.num:
                compare-type: version-compare
                op: '<'
                value: '80.20'
                """
        device_tags = {
            'vendor': 'checkpoint',
            'ip-address': '10.255.253.200',
            'os.name': 'gaia',
            'os.version.num': '80.20',
        }
        requirements_parsed = helper_methods.parse_data_as_yaml(requirements_yaml)
        result = self.parser.requirements_all_check(requirements_parsed[0]['requirements'],device_tags)
        self.assertEqual(False,result)

    def test_requirements_complex_4(self):
        requirements_yaml = """
        - requirements:
            or:
                - and:
                    - vendor: checkpoint
                    - os.name: gaia
                - vendor: f5
                - and:
                    - ip-address : '10.255.253.128/25'
                    - vendor: bluecat
                    - os.name: bdds
            os.version.num:
                compare-type: version-compare
                op: '<'
                value: '80.20'
                """
        device_tags = {
            'vendor': 'checkpoint',
            'ip-address': '10.255.253.200',
            'os.name': 'gaia',
            'os.version.num': '80.10',
        }
        requirements_parsed = helper_methods.parse_data_as_yaml(requirements_yaml)
        result = self.parser.requirements_all_check(requirements_parsed[0]['requirements'],device_tags)
        self.assertEqual(True,result)


    def test_requirements_complex_5(self):
        requirements_yaml = """
        - requirements:
            or:
                - and:
                    - vendor: checkpoint
                    - os.name: gaia
                - vendor: f5
                - and:
                    - ip-address : '10.255.253.128/25'
                    - vendor: bluecat
                    - os.name: bdds
            os.version.num:
                compare-type: version-compare
                op: '=='
                value: '80.20'
                """
        device_tags = {
            'vendor': 'checkpoint',
            'ip-address': '10.255.253.200',
            'os.name': 'gaia',
            'os.version.num': '80.20',
        }
        requirements_parsed = helper_methods.parse_data_as_yaml(requirements_yaml)
        result = self.parser.requirements_all_check(requirements_parsed[0]['requirements'],device_tags)
        self.assertEqual(True,result)


    def test_requirements_complex_6(self):
        requirements_yaml = """
        - requirements:
            or:
                - and:
                    - vendor: checkpoint
                    - os.name: gaia
                - vendor: f5
                - and:
                    - ip-address : '10.255.253.128/25'
                    - vendor: bluecat
                    - os.name: bdds
            os.version.num:
                compare-type: version-compare
                op: '>='
                value: '80.20'
                """
        device_tags = {
            'vendor': 'checkpoint',
            'ip-address': '10.255.253.200',
            'os.name': 'gaia',
            'os.version.num': '80.20',
        }
        requirements_parsed = helper_methods.parse_data_as_yaml(requirements_yaml)
        result = self.parser.requirements_all_check(requirements_parsed[0]['requirements'],device_tags)
        self.assertEqual(True,result)

    def test_requirements_complex_7(self):
        requirements_yaml = """
        - requirements:
            or:
                - and:
                    - vendor: checkpoint
                    - os.name: gaia
                - vendor: f5
                - and:
                    - ip-address : '10.255.253.128/25'
                    - vendor: bluecat
                    - os.name: bdds
            os.version.num:
                compare-type: version-compare
                op: '>'
                value: '80.20'
                """
        device_tags = {
            'vendor': 'checkpoint',
            'ip-address': '10.255.253.200',
            'os.name': 'gaia',
            'os.version.num': '80.20',
        }
        requirements_parsed = helper_methods.parse_data_as_yaml(requirements_yaml)
        result = self.parser.requirements_all_check(requirements_parsed[0]['requirements'],device_tags)
        self.assertEqual(False,result)


if __name__ == '__main__':
    unittest.main()