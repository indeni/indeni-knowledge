import os
import unittest
from crossvendor.probes.crossvendor_probe_dns.crossvendor_probe_dns_step2 import BluecatBddsProbeDnsStep2
from parser_service.public.action import *

class TestBluecatDnsServiceStatus(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = BluecatBddsProbeDnsStep2()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))    
    
    def test_crossvendor_probe_dns_step2_no_output(self):
        result = self.parser.parse_file(self.current_dir + '/crossvendor_probe_dns_step2_no_output.input', {}, {})
        self.assertEqual(0,len(result))


    def test_crossvendor_probe_dns_step2_single_line(self):
        result = self.parser.parse_file(self.current_dir + '/crossvendor_probe_dns_step2_single_line.input', {'command_parameters': 'dig +short @localhost indeni.com'}, {})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].value, 1)
        self.assertEqual(result[0].name, 'dns-probe-status')
        self.assertEqual(result[0].tags['name'], 'indeni.com')


    def test_crossvendor_probe_dns_step2_multiline(self):
        result = self.parser.parse_file(self.current_dir + '/crossvendor_probe_dns_step2_multiline.input', {'command_parameters': 'dig +short @localhost indeni.com'}, {})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].value, 1)
        self.assertEqual(result[0].name, 'dns-probe-status')
        self.assertEqual(result[0].tags['name'], 'indeni.com')


if __name__ == '__main__':
    unittest.main()