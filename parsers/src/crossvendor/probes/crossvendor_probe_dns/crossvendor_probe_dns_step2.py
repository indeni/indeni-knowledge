from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class BluecatBddsProbeDnsStep2(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if dynamic_var.get('command_parameters'):
            if dynamic_var.get('command_parameters') != 'sleep 1':
                self.write_double_metric('dns-probe-status',
                                         {'name' : dynamic_var.get('command_parameters').replace('dig +short @localhost ','')} ,
                                         'gauge',
                                         1 if raw_data else 0,
                                         True,
                                         'User defined DNS probes (name resolution)',
                                         'state',
                                         'name' )
            else:
                pass                            
        return self.output
