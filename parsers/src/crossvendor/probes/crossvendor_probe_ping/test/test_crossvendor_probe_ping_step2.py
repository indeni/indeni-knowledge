import os
import unittest
from crossvendor.probes.crossvendor_probe_ping.crossvendor_probe_ping_step2 import CrossvendorProbePingStep2
from parser_service.public.action import *

class TestBluecatDnsServiceStatus(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = CrossvendorProbePingStep2()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))    

    def test_ping_c_5_reachable(self):
        result = self.parser.parse_file(self.current_dir + '/ping_c_5_reachable.input', 
                                        {'command_parameters': '/usr/bin/ping -c 5 10.244.29.1'}, {})
        self.assertEqual(5,len(result))
        self.assertEqual(result[0].value, 0)
        self.assertEqual(result[0].name, 'ping-probe-pkt-loss')
        self.assertEqual(result[0].tags['name'], 'ping -c 5 10.244.29.1')

    def test_ping_c_5_not_reachable(self):
        result = self.parser.parse_file(self.current_dir + '/ping_c_5_not_reachable.input', 
                                        {'command_parameters': '/usr/bin/ping -c 5 10.244.29.2'}, {})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].value, 100)
        self.assertEqual(result[0].name, 'ping-probe-pkt-loss')
        self.assertEqual(result[0].tags['name'], 'ping -c 5 10.244.29.2')


if __name__ == '__main__':
    unittest.main()