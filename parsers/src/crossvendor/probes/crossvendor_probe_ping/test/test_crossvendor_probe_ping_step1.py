import os
import unittest
from crossvendor.probes.crossvendor_probe_ping.crossvendor_probe_ping_step1 import CrossvendorProbePingStep1
from parser_service.public.action import *

class TestBluecatDnsServiceStatus(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = CrossvendorProbePingStep1()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))    
    
    def test_whereis_ping_single_location(self):
        result = self.parser.parse_file(self.current_dir + '/whereis_ping_single_location.input', 
                                        {}, 
                                        {
                                            'os.name': 'gaia',
                                            'ip-address': '10.244.29.351',
                                            'hostname' : 'CHKP-R81.20-MDS01', 
                                            'ssh' : 'true', 
                                            'cluster-id' : '192.168.100.3', 
                                            'linux-based' : 'true', 
                                         }
                                        )
        self.assertEqual(3,len(result))
        self.assertEqual(result[0].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[0].value, '/usr/bin/ping -c 5 10.244.29.1')
        self.assertEqual(result[1].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[1].value, '/usr/bin/ping -c 5 10.244.29.2')
        self.assertEqual(result[2].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[2].value, '/usr/bin/ping -c 10 10.244.29.254')
  
    def test_whereis_ping_single_location(self):
        result = self.parser.parse_file(self.current_dir + '/whereis_ping_single_location.input', 
                                        {}, 
                                        {
                                            'os.name': 'bam',
                                            'ip-address': '10.244.29.80',
                                            'hostname' : 'dgs', 
                                            'ssh' : 'true', 
                                            'cluster-id' : '192.168.100.3', 
                                            'linux-based' : 'true', 
                                         }
                                        )
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[0].value, 'sleep 1')
     
        
    def test_whereis_ping_multiple_location(self):
        result = self.parser.parse_file(self.current_dir + '/whereis_ping_multiple_location.input', 
                                        {}, 
                                        {
                                            'os.name': 'gaia',
                                            'ip-address': '10.244.29.362',
                                            'hostname' : 'CHKP-R81.20-GW01', 
                                            'ssh' : 'true', 
                                            'cluster-id' : '192.168.100.3', 
                                            'linux-based' : 'true', 
                                         }
                                        )
        self.assertEqual(3,len(result))
        self.assertEqual(result[0].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[0].value, '/usr/bin/ping -c 5 10.244.29.80')
        self.assertEqual(result[1].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[1].value, '/usr/bin/ping -c 5 10.244.29.36')
        self.assertEqual(result[2].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[2].value, '/usr/bin/ping -c 10 10.244.29.152')

if __name__ == '__main__':
    unittest.main()