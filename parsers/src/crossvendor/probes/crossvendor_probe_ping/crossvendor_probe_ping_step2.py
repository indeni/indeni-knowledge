from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class CrossvendorProbePingStep2(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if dynamic_var.get('command_parameters'):
            if dynamic_var.get('command_parameters') != 'sleep 1':
                parsed_data = helper_methods.parse_data_as_list(raw_data, 'checkpoint_ping.textfsm')
                self.write_double_metric('ping-probe-pkt-loss',
                                         {'name' : dynamic_var.get('command_parameters')[dynamic_var.get('command_parameters').find('ping'):]},
                                         'gauge',
                                         float(parsed_data[0]['pkt_loss']),
                                         True,
                                         'User defined PING probes (packet loss)',
                                         'percentage',
                                         'name')
                if int(parsed_data[0]['pkt_loss']) != 100:
                    self.write_double_metric('ping-probe-rtt-min',
                                            {'name' : dynamic_var.get('command_parameters')[dynamic_var.get('command_parameters').find('ping'):]} ,
                                            'gauge',
                                            float(parsed_data[0]['rtt_min']),
                                            True,
                                            'User defined PING probes (min RTT milliseconds)',
                                            'number',
                                            'name')
                    self.write_double_metric('ping-probe-rtt-max',
                                            {'name' : dynamic_var.get('command_parameters')[dynamic_var.get('command_parameters').find('ping'):]} ,
                                            'gauge',
                                            float(parsed_data[0]['rtt_max']),
                                            True,
                                            'User defined PING probes (max RTT milliseconds)',
                                            'number',
                                            'name')
                    self.write_double_metric('ping-probe-rtt-avg',
                                            {'name' : dynamic_var.get('command_parameters')[dynamic_var.get('command_parameters').find('ping'):]} ,
                                            'gauge',
                                            float(parsed_data[0]['rtt_avg']),
                                            True,
                                            'User defined PING probes (avg RTT milliseconds)',
                                            'number',
                                            'name')          
                    self.write_double_metric('ping-probe-rtt-mdev',
                                            {'name' : dynamic_var.get('command_parameters')[dynamic_var.get('command_parameters').find('ping'):]} ,
                                            'gauge',
                                            float(parsed_data[0]['rtt_mdev']),
                                            True,
                                            'User defined PING probes (mdev RTT milliseconds)',
                                            'number',
                                            'name')            
            else:
                pass                            
        return self.output
