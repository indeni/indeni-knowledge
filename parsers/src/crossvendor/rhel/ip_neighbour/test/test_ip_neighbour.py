import os
import unittest

from crossvendor.rhel.ip_neighbour.ip_neighbour import IpNeighbour
from parser_service.public.action import *

class TestIpNeighbour(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = IpNeighbour()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_3_arp_entries(self):
        result = self.parser.parse_file(self.current_dir + '/3_arp_entries.input', {}, {})
        self.assertEqual(3, len(result))
        self.assertEqual(result[0].name, 'arp-total-entries')
        self.assertEqual(result[0].value, 3)
        self.assertEqual(result[1].name, 'static-arp')
        self.assertEqual(len(result[1].value), 0)
        self.assertEqual(result[2].name, 'arp-table')
        self.assertEqual(len(result[2].value), 3)

    def test_static_entry(self):
        result = self.parser.parse_file(self.current_dir + '/static_entry.input', {}, {})
        self.assertEqual(3, len(result))
        self.assertEqual(result[0].name, 'arp-total-entries')
        self.assertEqual(result[0].value, 4)
        self.assertEqual(result[1].name, 'static-arp')
        self.assertEqual(len(result[1].value), 1)
        self.assertEqual(result[2].name, 'arp-table')
        self.assertEqual(len(result[2].value), 4)

if __name__ == '__main__':
    unittest.main()