from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class IpNeighbour(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            arp_table = helper_methods.parse_data_as_list(raw_data, 'linux_ip_neighbour.textfsm')
            static_arp_list = []
            arp_table_list = []
            for arp_entry in arp_table:
                new_entry = {}
                new_entry['targetip'] = arp_entry['ip_address']
                new_entry['mac'] = arp_entry['mac_address']
                if arp_entry['state'].lower() == 'permanent':
                    new_static_arp = {}
                    new_static_arp['ip-address'] = arp_entry['ip_address']
                    new_static_arp['mac-address'] = arp_entry['mac_address']
                    static_arp_list.append(new_static_arp)
                new_entry['interface'] = arp_entry['interface']
                new_entry['success'] = '0' if arp_entry['state'].lower() == 'incomplete' else '1'
                arp_table_list.append(new_entry)

            self.write_double_metric('arp-total-entries', {}, 'gauge', len(arp_table), False)
            self.write_complex_metric_object_array('static-arp', {}, static_arp_list, False)
            self.write_complex_metric_object_array('arp-table', {}, arp_table_list, False)

        return self.output
