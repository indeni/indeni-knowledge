from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class IpRoute(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            static_routes = helper_methods.parse_data_as_list(raw_data, 'linux_ip_route_static.textfsm')
            directly_connect_networks = helper_methods.parse_data_as_list(raw_data, 'linux_ip_route_connected.textfsm')
            static_route_list = []
            directly_connect_list = []
            for route in static_routes:
                new_static_route = {}
                if route['network'] == 'default':
                    new_static_route['network'] = '0.0.0.0'
                    new_static_route['mask'] = '0'
                else:
                    new_static_route['network'] = route['network']
                    new_static_route['mask'] = route['mask']
                new_static_route['next-hop'] = route['next_hop']
                static_route_list.append(new_static_route)
            for network in directly_connect_networks:
                new_direct_network = {}
                new_direct_network['network'] = network['network']
                new_direct_network['mask'] = network['mask']
                directly_connect_list.append(new_direct_network)

            self.write_complex_metric_object_array('static-routing-table', {}, static_route_list, True, 'Static routes')
            self.write_complex_metric_object_array('connected-networks-table', {}, directly_connect_list, True, 'Directly Connected Networks')

        return self.output
