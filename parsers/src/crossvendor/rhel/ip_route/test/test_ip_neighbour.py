import os
import unittest

from crossvendor.rhel.ip_route.ip_route import IpRoute
from parser_service.public.action import *

class TestIpRoute(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = IpRoute()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_2_static_2_directly_connected(self):
        result = self.parser.parse_file(self.current_dir + '/2_static_2_directly_connected.input', {}, {})
        self.assertEqual(2, len(result))
        self.assertEqual(result[0].name, 'static-routing-table')
        self.assertEqual(len(result[0].value), 3)
        self.assertEqual(result[1].name, 'connected-networks-table')
        self.assertEqual(len(result[1].value), 2)

if __name__ == '__main__':
    unittest.main()