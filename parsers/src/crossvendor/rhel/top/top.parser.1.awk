#%Cpu(s):  1.6 us,  1.6 sy,  0.0 ni, 96.9 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
/^%Cpu\(s\):/ {

	user = $2
	sytem = $4
	nice = $6
	idle = $8
	wait = $10
	hwint = $12
	swint = $14
	steal = $16

	usage = 100 - idle

    cputags["cpu-id"] = 0
    cputags["cpu-is-avg"] = "false"
    cputags["resource-metric"] = "false"

    writeDoubleMetric("cpu-usage", cputags, "gauge", usage, "true", "CPU", "percentage", "cpu-id")


    cputags["cpu-id"] = "all"
    cputags["cpu-is-avg"] = "true"
	cputags["cpu-id"] = "all-average"
	cputags["resource-metric"] = "true"

    writeDoubleMetric("cpu-usage", cputags, "gauge", usage, "true", "CPU", "percentage", "cpu-id")
}