from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser
from ipaddress import IPv4Network

class ThroughputAlert(BaseParser):
    def cidr_to_netmask(self, cidr):
        netmask_list = ['0.0.0.0', '128.0.0.0', '192.0.0.0', '224.0.0.0', '240.0.0.0', '248.0.0.0', '252.0.0.0', '254.0.0.0', '255.0.0.0',
                                '255.128.0.0', '255.192.0.0', '255.224.0.0', '255.240.0.0', '255.248.0.0', '255.252.0.0', '255.254.0.0', '255.255.0.0',
                                '255.255.128.0', '255.255.192.0', '255.255.224.0', '255.255.240.0', '255.255.248.0', '255.255.252.0', '255.255.254.0', '255.255.255.0',
                                '255.255.255.128', '255.255.255.192', '255.255.255.224', '255.255.255.240', '255.255.255.248', '255.255.255.252', '255.255.255.254', '255.255.255.255']
        return netmask_list[cidr]

    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):

        speed_units = {
            "Mb/s": 1000000,
            "Gb/s": 1000000000
        }
        # Step 1 : Data Extraction
        if device_tags.get('os.name') in ['bam', 'bdds'] and device_tags.get('model') in ['AWS', 'Azure', 'Google Cloud']:
            data = helper_methods.parse_data_as_list(raw_data, 'throughput_alert_bluecat_public_cloud.textfsm') # ethtool not available
        else:
            data = helper_methods.parse_data_as_list(raw_data, 'throughput_alert.textfsm')

        # Step 2 : Data Processing and Reporting
        if data:
            interfaces_list=list(dict.fromkeys([entry['int_name'] for entry in data]))
            if len(interfaces_list) > 0:
                for interface in interfaces_list:
                    if not (device_tags.get('vendor') == "bluecat" and 'tun' in  interface):
                        if device_tags.get('os.name') not in ['bam', 'bdds'] and device_tags.get('model') not in ['AWS', 'Azure', 'Google Cloud']:
                            interface_stats_tx=[entry['tx_bytes'] for entry in data if entry['int_name']==interface]
                            interface_stats_rx=[entry['rx_bytes'] for entry in data if entry['int_name']==interface]
                            interface_speed_value=int([entry['int_speed_value'] for entry in data if entry['int_name']==interface][0])
                            interface_speed_unit=str([entry['int_speed_unit'] for entry in data if entry['int_name']==interface][0])
                            tags = {
                                'type': 'Bandwith utilization'
                            }
                            tags['name'] = '{}_{}{}'.format(str(interface), interface_speed_value, interface_speed_unit.rstrip('/s'))
                            percentage_rx = percentage_tx = 0
                            if len(interface_stats_tx) == 2 and len(interface_stats_rx) == 2:
                                percentage_tx = (abs(int(interface_stats_tx[0])-int(interface_stats_tx[1]))*8/10)/(interface_speed_value*speed_units[interface_speed_unit])*100
                                if percentage_tx < 120:
                                    self.write_double_metric('network-interface-tx-util-percentage',tags,'gauge', percentage_tx, True, 'Network Interfaces - Throughput Transmit', 'percentage', 'name|type')
                                percentage_rx = (abs(int(interface_stats_rx[0])-int(interface_stats_rx[1]))*8/10)/(interface_speed_value*speed_units[interface_speed_unit])*100
                                if percentage_rx < 120:
                                        self.write_double_metric('network-interface-rx-util-percentage',tags,'gauge', percentage_rx, True, 'Network Interfaces - Throughput Receive', 'percentage', 'name|type')
                            interface_stats_packets_tx=[entry['tx_packets'] for entry in data if entry['int_name']==interface]
                            interface_stats_packets_rx=[entry['rx_packets'] for entry in data if entry['int_name']==interface]
                            if len(interface_stats_packets_tx) == 2 and len(interface_stats_packets_rx) == 2:
                                interface_stats_errors_tx=[entry['tx_errors'] for entry in data if entry['int_name']==interface]
                                interface_stats_errors_rx=[entry['rx_errors'] for entry in data if entry['int_name']==interface]
                                if len(interface_stats_errors_tx) == 2 and len(interface_stats_errors_rx) == 2:
                                    tags['type'] = 'Errors percentage'
                                    if abs(int(interface_stats_packets_tx[0])-int(interface_stats_packets_tx[1])) != 0 and percentage_tx > 0.1:
                                        percentage_errors_tx = (abs(int(interface_stats_errors_tx[0])-int(interface_stats_errors_tx[1])))/(abs(int(interface_stats_packets_tx[0])-int(interface_stats_packets_tx[1])))*100
                                    else:
                                        percentage_errors_tx = 0
                                    self.write_double_metric('network-interface-tx-error-percentage',tags,'gauge', percentage_errors_tx, True, 'Network Interfaces - Throughput Transmit', 'percentage', 'name|type')
                                    if abs(int(interface_stats_packets_rx[0])-int(interface_stats_packets_rx[1])) != 0 and percentage_rx > 0.1:
                                        percentage_errors_rx = (abs(int(interface_stats_errors_rx[0])-int(interface_stats_errors_rx[1])))/(abs(int(interface_stats_packets_rx[0])-int(interface_stats_packets_rx[1])))*100
                                    else:
                                        percentage_errors_rx = 0
                                    self.write_double_metric('network-interface-rx-error-percentage',tags,'gauge', percentage_errors_rx, True, 'Network Interfaces - Throughput Receive', 'percentage', 'name|type')

                                    interface_stats_dropped_tx=[entry['tx_dropped'] for entry in data if entry['int_name']==interface]
                                    interface_stats_dropped_rx=[entry['rx_dropped'] for entry in data if entry['int_name']==interface]
                                    if len(interface_stats_dropped_tx) == 2 and len(interface_stats_dropped_rx) == 2:
                                        tags ['type'] = 'Dropped percentage'
                                        if abs(int(interface_stats_packets_tx[0])-int(interface_stats_packets_tx[1])) != 0 and percentage_tx > 0.1:
                                            percentage_dropped_tx = (abs(int(interface_stats_dropped_tx[0])-int(interface_stats_dropped_tx[1])))/(abs(int(interface_stats_packets_tx[0])-int(interface_stats_packets_tx[1])))*100
                                        else:
                                            percentage_dropped_tx = 0
                                        self.write_double_metric('network-interface-tx-dropped-percentage',tags,'gauge', percentage_dropped_tx, True, 'Network Interfaces - Throughput Transmit', 'percentage', 'name|type')
                                        if abs(int(interface_stats_packets_rx[0])-int(interface_stats_packets_rx[1])) != 0 and percentage_rx > 0.1:
                                            percentage_dropped_rx = (abs(int(interface_stats_dropped_rx[0])-int(interface_stats_dropped_rx[1])))/(abs(int(interface_stats_packets_rx[0])-int(interface_stats_packets_rx[1])))*100
                                        else:
                                            percentage_dropped_rx = 0
                                        self.write_double_metric('network-interface-rx-dropped-percentage',tags,'gauge', percentage_dropped_rx, True, 'Network Interfaces - Throughput Receive', 'percentage', 'name|type')

                        tags = {}
                        tags['name'] = '{}'.format(str(interface))
                        tags_mac_address = {'name': '{}'.format(str(interface)) , 'im.identity-tags': 'name', 'im.dstype.displaytype': 'string'}

                        interface_data = [entry for entry in data if entry['int_name']==interface]
                        interface_data.pop(1) if interface_data[0]['rx_bytes'] > interface_data[1]['rx_bytes'] else interface_data.pop(0)

                        self.write_complex_metric_string('network-interface-mac', tags_mac_address, interface_data[0]['int_mac_address'], True, 'Network Interfaces - MAC Address')
                        if interface_data[0].get('int_ip_address'):
                            self.write_complex_metric_string('network-interface-ipv4-address', tags, interface_data[0]['int_ip_address'], True, 'Network Interfaces - IPv4 Address')
                            self.write_complex_metric_string('network-interface-ipv4-subnet', tags, self.cidr_to_netmask(int(interface_data[0]['int_mask_cidr'])),
                                                                                    True, 'Network Interfaces - IPv4 Netmask')
                        self.write_complex_metric_string('network-interface-mtu', tags, interface_data[0]['mtu'], True, 'Network Interfaces - MTU')
                        self.write_complex_metric_string('network-interface-duplex', tags, interface_data[0]['duplex'], True, 'Network Interfaces - duplex settings')
                        flags = interface_data[0]['int_flags'].split(',')
                        self.write_double_metric('network-interface-admin-state', tags, 'gauge', 1 if 'UP' in flags else 0, True,
                                                                'Network Interfaces - Enabled/Disabed', 'state', 'name')
                        self.write_double_metric('network-interface-state', tags, 'gauge', 1 if 'LOWER_UP' in flags else 0, True,
                                                                'Network Interfaces - Up/Down', 'state', 'name')

                        statistics_list = [x for x in list(dict.fromkeys(interface_data[0])) if  x.startswith('rx_') or x.startswith('tx_')]
                        for stat in statistics_list:
                            if len(interface_data[0][stat]) > 0:
                                self.write_double_metric('network-interface-'+'{}-{}'.format(stat.split('_')[0], stat.split('_')[1]), tags, 'gauge', int(interface_data[0][stat]),
                                    True, '{}{} {}'.format('Network Interfaces - ', stat.split('_')[0], stat.split('_')[1]), 'number', 'name')

                        tags['im.identity-tags'] = 'name'
                        self.write_complex_metric_string('network-interface-speed', tags, '{} {}'.format(interface_data[0]['int_speed_value'], interface_data[0]['int_speed_unit']), True, 'Network Interfaces - speed')


        return self.output
