import os
import unittest

from crossvendor.rhel.throughput_alert.throughput_alert import ThroughputAlert


class TestThroughputAlert(unittest.TestCase):

    def setUp(self):
        self.parser = ThroughputAlert()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_8int(self):

        expected_results =[{'metric': 'network-interface-tx-util-percentage', 'tags': {'type': 'Bandwith utilization', 'name': 'ens192_10000Mb', 'live-config': 'true', 'display-name': 'Network Interfaces - Throughput Transmit', 'im.dstype.displayType': 'percentage', 'im.identity-tags': 'name|type'}, 'value': 8.531200000000001e-05},
            {'metric': 'network-interface-rx-util-percentage', 'tags': {'type': 'Bandwith utilization', 'name': 'ens192_10000Mb', 'live-config': 'true', 'display-name': 'Network Interfaces - Throughput Receive', 'im.dstype.displayType': 'percentage', 'im.identity-tags': 'name|type'}, 'value': 1.4767999999999998e-05},
            {'metric': 'network-interface-tx-error-percentage', 'tags': {'type': 'Errors percentage', 'name': 'ens192_10000Mb', 'live-config': 'true', 'display-name': 'Network Interfaces - Throughput Transmit', 'im.dstype.displayType': 'percentage', 'im.identity-tags': 'name|type'}, 'value': 0.0},
            {'metric': 'network-interface-rx-error-percentage', 'tags': {'type': 'Errors percentage', 'name': 'ens192_10000Mb', 'live-config': 'true', 'display-name': 'Network Interfaces - Throughput Receive', 'im.dstype.displayType': 'percentage', 'im.identity-tags': 'name|type'}, 'value': 0.0},
            {'metric': 'network-interface-tx-dropped-percentage', 'tags': {'type': 'Dropped percentage', 'name': 'ens192_10000Mb', 'live-config': 'true', 'display-name': 'Network Interfaces - Throughput Transmit', 'im.dstype.displayType': 'percentage', 'im.identity-tags': 'name|type'}, 'value': 0.0},
            {'metric': 'network-interface-rx-dropped-percentage', 'tags': {'type': 'Dropped percentage', 'name': 'ens192_10000Mb', 'live-config': 'true', 'display-name': 'Network Interfaces - Throughput Receive', 'im.dstype.displayType': 'percentage', 'im.identity-tags': 'name|type'}, 'value': 0.0},
            {'metric': 'network-interface-mac', 'tags': {'name': 'ens192', 'im.dstype.displaytype': 'string', 'im.identity-tags': 'name', 'live-config': 'true', 'display-name': 'Network Interfaces - MAC Address'}, 'value': {'value': '00:50:56:91:90:bb'}},
            {'metric': 'network-interface-ipv4-address', 'tags': {'name': 'ens192', 'live-config': 'true', 'display-name': 'Network Interfaces - IPv4 Address'}, 'value': {'value': '10.11.80.40'}},
            {'metric': 'network-interface-ipv4-subnet', 'tags': {'name': 'ens192', 'live-config': 'true', 'display-name': 'Network Interfaces - IPv4 Netmask'}, 'value': {'value': '255.255.255.0'}},
            {'metric': 'network-interface-mtu', 'tags': {'name': 'ens192', 'live-config': 'true', 'display-name': 'Network Interfaces - MTU'}, 'value': {'value': '1500'}},
            {'metric': 'network-interface-duplex', 'tags': {'name': 'ens192', 'live-config': 'true', 'display-name': 'Network Interfaces - duplex settings'}, 'value': {'value': 'Full'}},
            {'metric': 'network-interface-admin-state', 'tags': {'name': 'ens192', 'live-config': 'true', 'display-name': 'Network Interfaces - Enabled/Disabed', 'im.dstype.displayType': 'state', 'im.identity-tags': 'name'}, 'value': 1},
            {'metric': 'network-interface-state', 'tags': {'name': 'ens192', 'live-config': 'true', 'display-name': 'Network Interfaces - Up/Down', 'im.dstype.displayType': 'state', 'im.identity-tags': 'name'}, 'value': 1},
            {'metric': 'network-interface-rx-bytes', 'tags': {'name': 'ens192', 'live-config': 'true', 'display-name': 'Network Interfaces - rx bytes', 'im.dstype.displayType': 'number', 'im.identity-tags': 'name'}, 'value': 1678072929},
            {'metric': 'network-interface-tx-bytes', 'tags': {'name': 'ens192', 'live-config': 'true', 'display-name': 'Network Interfaces - tx bytes', 'im.dstype.displayType': 'number', 'im.identity-tags': 'name'}, 'value': 2339336275},
            {'metric': 'network-interface-rx-packets', 'tags': {'name': 'ens192', 'live-config': 'true', 'display-name': 'Network Interfaces - rx packets', 'im.dstype.displayType': 'number', 'im.identity-tags': 'name'}, 'value': 18594668},
            {'metric': 'network-interface-tx-packets', 'tags': {'name': 'ens192', 'live-config': 'true', 'display-name': 'Network Interfaces - tx packets', 'im.dstype.displayType': 'number', 'im.identity-tags': 'name'}, 'value': 8645984},
            {'metric': 'network-interface-rx-errors', 'tags': {'name': 'ens192', 'live-config': 'true', 'display-name': 'Network Interfaces - rx errors', 'im.dstype.displayType': 'number', 'im.identity-tags': 'name'}, 'value': 0},
            {'metric': 'network-interface-tx-errors', 'tags': {'name': 'ens192', 'live-config': 'true', 'display-name': 'Network Interfaces - tx errors', 'im.dstype.displayType': 'number', 'im.identity-tags': 'name'}, 'value': 0},
            {'metric': 'network-interface-rx-dropped', 'tags': {'name': 'ens192', 'live-config': 'true', 'display-name': 'Network Interfaces - rx dropped', 'im.dstype.displayType': 'number', 'im.identity-tags': 'name'}, 'value': 61},
            {'metric': 'network-interface-tx-dropped', 'tags': {'name': 'ens192', 'live-config': 'true', 'display-name': 'Network Interfaces - tx dropped', 'im.dstype.displayType': 'number', 'im.identity-tags': 'name'}, 'value': 0},
            {'metric': 'network-interface-rx-frame', 'tags': {'name': 'ens192', 'live-config': 'true', 'display-name': 'Network Interfaces - rx frame', 'im.dstype.displayType': 'number', 'im.identity-tags': 'name'}, 'value': 0},
            {'metric': 'network-interface-rx-overruns', 'tags': {'name': 'ens192', 'live-config': 'true', 'display-name': 'Network Interfaces - rx overruns', 'im.dstype.displayType': 'number', 'im.identity-tags': 'name'}, 'value': 0},
            {'metric': 'network-interface-speed', 'tags': {'name': 'ens192', 'im.identity-tags': 'name', 'live-config': 'true', 'display-name': 'Network Interfaces - speed'}, 'value': {'value': '10000 Mb/s'}},
            {'metric': 'network-interface-tx-util-percentage', 'tags': {'type': 'Bandwith utilization', 'name': 'virbr0-nic_10Mb', 'live-config': 'true', 'display-name': 'Network Interfaces - Throughput Transmit', 'im.dstype.displayType': 'percentage', 'im.identity-tags': 'name|type'}, 'value': 0.0},
            {'metric': 'network-interface-rx-util-percentage', 'tags': {'type': 'Bandwith utilization', 'name': 'virbr0-nic_10Mb', 'live-config': 'true', 'display-name': 'Network Interfaces - Throughput Receive', 'im.dstype.displayType': 'percentage', 'im.identity-tags': 'name|type'}, 'value': 0.0},
            {'metric': 'network-interface-tx-error-percentage', 'tags': {'type': 'Errors percentage', 'name': 'virbr0-nic_10Mb', 'live-config': 'true', 'display-name': 'Network Interfaces - Throughput Transmit', 'im.dstype.displayType': 'percentage', 'im.identity-tags': 'name|type'}, 'value': 0},
            {'metric': 'network-interface-rx-error-percentage', 'tags': {'type': 'Errors percentage', 'name': 'virbr0-nic_10Mb', 'live-config': 'true', 'display-name': 'Network Interfaces - Throughput Receive', 'im.dstype.displayType': 'percentage', 'im.identity-tags': 'name|type'}, 'value': 0},
            {'metric': 'network-interface-tx-dropped-percentage', 'tags': {'type': 'Dropped percentage', 'name': 'virbr0-nic_10Mb', 'live-config': 'true', 'display-name': 'Network Interfaces - Throughput Transmit', 'im.dstype.displayType': 'percentage', 'im.identity-tags': 'name|type'}, 'value': 0},
            {'metric': 'network-interface-rx-dropped-percentage', 'tags': {'type': 'Dropped percentage', 'name': 'virbr0-nic_10Mb', 'live-config': 'true', 'display-name': 'Network Interfaces - Throughput Receive', 'im.dstype.displayType': 'percentage', 'im.identity-tags': 'name|type'}, 'value': 0},
            {'metric': 'network-interface-mac', 'tags': {'name': 'virbr0-nic', 'im.dstype.displaytype': 'string', 'im.identity-tags': 'name', 'live-config': 'true', 'display-name': 'Network Interfaces - MAC Address'}, 'value': {'value': '52:54:00:ba:1e:b9'}},
            {'metric': 'network-interface-mtu', 'tags': {'name': 'virbr0-nic', 'live-config': 'true', 'display-name': 'Network Interfaces - MTU'}, 'value': {'value': '1500'}},
            {'metric': 'network-interface-duplex', 'tags': {'name': 'virbr0-nic', 'live-config': 'true', 'display-name': 'Network Interfaces - duplex settings'}, 'value': {'value': 'Full'}},
            {'metric': 'network-interface-admin-state', 'tags': {'name': 'virbr0-nic', 'live-config': 'true', 'display-name': 'Network Interfaces - Enabled/Disabed', 'im.dstype.displayType': 'state', 'im.identity-tags': 'name'}, 'value': 0},
            {'metric': 'network-interface-state', 'tags': {'name': 'virbr0-nic', 'live-config': 'true', 'display-name': 'Network Interfaces - Up/Down', 'im.dstype.displayType': 'state', 'im.identity-tags': 'name'}, 'value': 0},
            {'metric': 'network-interface-rx-bytes', 'tags': {'name': 'virbr0-nic', 'live-config': 'true', 'display-name': 'Network Interfaces - rx bytes', 'im.dstype.displayType': 'number', 'im.identity-tags': 'name'}, 'value': 0},
            {'metric': 'network-interface-tx-bytes', 'tags': {'name': 'virbr0-nic', 'live-config': 'true', 'display-name': 'Network Interfaces - tx bytes', 'im.dstype.displayType': 'number', 'im.identity-tags': 'name'}, 'value': 0},
            {'metric': 'network-interface-rx-packets', 'tags': {'name': 'virbr0-nic', 'live-config': 'true', 'display-name': 'Network Interfaces - rx packets', 'im.dstype.displayType': 'number', 'im.identity-tags': 'name'}, 'value': 0},
            {'metric': 'network-interface-tx-packets', 'tags': {'name': 'virbr0-nic', 'live-config': 'true', 'display-name': 'Network Interfaces - tx packets', 'im.dstype.displayType': 'number', 'im.identity-tags': 'name'}, 'value': 0},
            {'metric': 'network-interface-rx-errors', 'tags': {'name': 'virbr0-nic', 'live-config': 'true', 'display-name': 'Network Interfaces - rx errors', 'im.dstype.displayType': 'number', 'im.identity-tags': 'name'}, 'value': 0},
            {'metric': 'network-interface-tx-errors', 'tags': {'name': 'virbr0-nic', 'live-config': 'true', 'display-name': 'Network Interfaces - tx errors', 'im.dstype.displayType': 'number', 'im.identity-tags': 'name'}, 'value': 0},
            {'metric': 'network-interface-rx-dropped', 'tags': {'name': 'virbr0-nic', 'live-config': 'true', 'display-name': 'Network Interfaces - rx dropped', 'im.dstype.displayType': 'number', 'im.identity-tags': 'name'}, 'value': 0},
            {'metric': 'network-interface-tx-dropped', 'tags': {'name': 'virbr0-nic', 'live-config': 'true', 'display-name': 'Network Interfaces - tx dropped', 'im.dstype.displayType': 'number', 'im.identity-tags': 'name'}, 'value': 0},
            {'metric': 'network-interface-rx-frame', 'tags': {'name': 'virbr0-nic', 'live-config': 'true', 'display-name': 'Network Interfaces - rx frame', 'im.dstype.displayType': 'number', 'im.identity-tags': 'name'}, 'value': 0},
            {'metric': 'network-interface-rx-overruns', 'tags': {'name': 'virbr0-nic', 'live-config': 'true', 'display-name': 'Network Interfaces - rx overruns', 'im.dstype.displayType': 'number', 'im.identity-tags': 'name'}, 'value': 0},
            {'metric': 'network-interface-speed', 'tags': {'name': 'virbr0-nic', 'im.identity-tags': 'name', 'live-config': 'true', 'display-name': 'Network Interfaces - speed'}, 'value': {'value': '10 Mb/s'}}]

        result = self.parser.parse_file(self.current_dir + '/1_ethernet.input', {}, {'vendor' : 'checkpoint'})
        self.assertEqual(46, len(result))

        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i]['tags'], result[i].tags)
            self.assertEqual(expected_results[i]['metric'], result[i].name)
            self.assertEqual(expected_results[i]['value'], result[i].value)

    def test_8int(self):
        result = self.parser.parse_file(self.current_dir + '/bam_aws_cloud.input', {}, {'vendor' : 'bluecat', 'os.name' : 'bam' , 'model' : 'AWS'})
        self.assertEqual(18, len(result))

if __name__ == '__main__':
    unittest.main()