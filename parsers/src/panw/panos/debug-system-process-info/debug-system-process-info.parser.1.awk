BEGIN {
    crit_processes["mgmtsrvr"] = "Management Plane server for configuring policy device settings etc."
    crit_processes["authd"] = "Handles authentication functionality"
    crit_processes["crypto"] = "N/A"
    crit_processes["dhcp"] = "DHCP service"
    crit_processes["ikemgr"] = "IKE Manager for IPsec tunnels"
    crit_processes["keymgr"] = "Key Manager for IPsec tunnels"
    crit_processes["l3svc"] = "Captive portal server"
    crit_processes["logrcvr"] = "Log receiver"
    crit_processes["masterd"] = "Responsible for the health of other critical processes"  
    crit_processes["routed"] = "Dynamic routing daemon"
    crit_processes["snmpd"] = "SNMP trap sender and agent"
    crit_processes["sshd"] = "SSH server"
    crit_processes["sslmgr"] = "SSL certificate manager"
    crit_processes["sslvpn"] = "SSL VPN service"
    crit_processes["websrvr"] = "Web interface server"
    # Initialize the found_process array
    for (key in crit_processes) { found_process[key] = "0.0" }
}

#Name                   PID      CPU%  FDs Open   Virt Mem     Res Mem      State
#websrvr                3019     0     10         107136       436          S

/ [RS]\s*$/ {
    command_run = 1
    if ($1 in found_process) {
        found_process[$1] = "1.0"
    }
    process_name = $1
    pid = $2
    cpu = $3
    virtmem = $5
    resmem = $6
    state = $NF

    ps_tags["process_pid"] = pid
    ps_tags["process_name"] = process_name

    writeDoubleMetric("process-cpu", ps_tags, "gauge", cpu, "true", "Processes (CPU)", "percentage", "process_pid|process_name")  # Converted to new syntax by change_ind_scripts.py script
}
END {
    if (command_run) {
        for (process in found_process) {
            crit_ps_tags["process-name"] = process
            crit_ps_tags["description"] = crit_processes[process]
            writeDoubleMetric("process-state", crit_ps_tags, "gauge", found_process[process], "false")  # Converted to new syntax by change_ind_scripts.py script
        }
    }
}