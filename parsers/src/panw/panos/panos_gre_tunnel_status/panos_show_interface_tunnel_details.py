from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class PanosShowInterfaceTunnelDetails(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            tunnels_data = helper_methods.parse_data_as_list(raw_data, 'panos_show_interface_tunnel_details.textfsm')
            tunnels_data = [x for x in tunnels_data if len(x['tunnel_name'])>0]
            for tunnel in tunnels_data :
                tags = {}
                tags['name'] = tunnel['tunnel_name']
                tags['local_ip'] = tunnel['tunnel_local_ip']
                tags['peer_ip'] = tunnel['tunnel_peer_ip']

                bytes_rx = int(tunnel['bytes_rx'])
                bytes_tx = int(tunnel['bytes_tx'])
                packets_rx = int(tunnel['packets_rx'])
                packets_tx = int(tunnel['packets_tx'])
                errors = int(tunnel['errors'])
                packet_drop = int(tunnel['packet_drop'])

                self.write_double_metric('gre-tunnel-enabled', tags, 'gauge', 1 if 'False' in tunnel['tunnel_disabled'] else 0, True, 'GRE Tunnels - Enabled','state','name')
                self.write_double_metric('gre-tunnel-status', tags, 'gauge', 1 if 'Up' in tunnel['tunnel_state'] else 0, True, 'GRE Tunnels - State','state','name')

                self.write_double_metric('gre-tunnel-bytes-rx', tags, 'gauge', bytes_rx, True, 'GRE Tunnels Stats - Bytes RX','number','name')
                self.write_double_metric('gre-tunnel-bytes-tx', tags, 'gauge', bytes_tx, True, 'GRE Tunnels Stats - Bytes TX','number','name')

                self.write_double_metric('gre-tunnel-packets-rx', tags, 'gauge', packets_rx, True, 'GRE Tunnels Stats - Packets RX','number','name')
                self.write_double_metric('gre-tunnel-packets-tx', tags, 'gauge', packets_tx, True, 'GRE Tunnels Stats - Packets TX','number','name')

                self.write_double_metric('gre-tunnel-errors', tags, 'gauge', errors, True, 'GRE Tunnels Stats - Errors','number','name')
                self.write_double_metric('gre-tunnel-packets-drop', tags, 'gauge', packet_drop, True, 'GRE Tunnels Stats - Packets Dropped','number','name')

        return self.output
