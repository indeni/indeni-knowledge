import os
import unittest

from panw.panos.panos_gre_tunnel_status.panos_show_interface_tunnel import PanosShowInterfaceTunnel
from parser_service.public.action import WriteDynamicVariable

class TestPanosShowInterfaceTunnel(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = PanosShowInterfaceTunnel()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_one_gre_tunnel(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/one_gre_tunnel.input', {}, {})

        # Assert
        self.assertEqual(1, len(result))
        self.assertTrue(isinstance(result[0], WriteDynamicVariable))
        self.assertEqual('tunnel.2', result[0].value)

    def test_multiple_gre_tunnels(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/multiple_gre_tunnels.input', {}, {})

        # Assert
        self.assertEqual(4, len(result))
        self.assertTrue(isinstance(result[0], WriteDynamicVariable))
        self.assertEqual('tunnel.2', result[0].value)
        self.assertTrue(isinstance(result[1], WriteDynamicVariable))
        self.assertEqual('tunnel.3', result[1].value)
        self.assertTrue(isinstance(result[2], WriteDynamicVariable))
        self.assertEqual('tunnel.4', result[2].value)
        self.assertTrue(isinstance(result[3], WriteDynamicVariable))
        self.assertEqual('tunnel.5', result[3].value)

if __name__ == '__main__':
    unittest.main()
