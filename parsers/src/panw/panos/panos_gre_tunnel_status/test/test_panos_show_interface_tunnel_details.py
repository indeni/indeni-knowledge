import os
import unittest

from panw.panos.panos_gre_tunnel_status.panos_show_interface_tunnel_details import PanosShowInterfaceTunnelDetails
from parser_service.public.action import *

class TestPanosShowInterfaceTunnelDetails(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = PanosShowInterfaceTunnelDetails()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_gre_tunnel_up(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/gre_tunnel_up.input', {}, {})

        # Assert
        self.assertEqual(8, len(result))

        self.assertTrue(isinstance(result[0], WriteDoubleMetric))
        self.assertEqual('gre-tunnel-enabled', result[0].name)
        self.assertEqual('gre-tunnel', result[0].tags['name'])
        self.assertEqual('10.11.80.229', result[0].tags['local_ip'])
        self.assertEqual('10.11.80.228', result[0].tags['peer_ip'])
        self.assertEqual('GRE Tunnels - Enabled', result[0].tags['display-name'])
        self.assertEqual(1, result[0].value)

        self.assertTrue(isinstance(result[1], WriteDoubleMetric))
        self.assertEqual('gre-tunnel-status', result[1].name)
        self.assertEqual('gre-tunnel', result[1].tags['name'])
        self.assertEqual('10.11.80.229', result[1].tags['local_ip'])
        self.assertEqual('10.11.80.228', result[1].tags['peer_ip'])
        self.assertEqual('GRE Tunnels - State', result[1].tags['display-name'])
        self.assertEqual(1, result[1].value)

    def test_gre_tunnel_down(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/gre_tunnel_down.input', {}, {})

        # Assert
        self.assertEqual(8, len(result))

        self.assertTrue(isinstance(result[0], WriteDoubleMetric))
        self.assertEqual('gre-tunnel-enabled', result[0].name)
        self.assertEqual('gre-tunnel', result[0].tags['name'])
        self.assertEqual('10.11.80.229', result[0].tags['local_ip'])
        self.assertEqual('10.11.80.228', result[0].tags['peer_ip'])
        self.assertEqual('GRE Tunnels - Enabled', result[0].tags['display-name'])
        self.assertEqual(1, result[0].value)

        self.assertTrue(isinstance(result[1], WriteDoubleMetric))
        self.assertEqual('gre-tunnel-status', result[1].name)
        self.assertEqual('gre-tunnel', result[1].tags['name'])
        self.assertEqual('10.11.80.229', result[1].tags['local_ip'])
        self.assertEqual('10.11.80.228', result[1].tags['peer_ip'])
        self.assertEqual('GRE Tunnels - State', result[1].tags['display-name'])
        self.assertEqual(0, result[1].value)

    def test_gre_tunnel_disabled(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/gre_tunnel_disabled.input', {}, {})

        # Assert
        self.assertEqual(8, len(result))

        self.assertTrue(isinstance(result[0], WriteDoubleMetric))
        self.assertEqual('gre-tunnel-enabled', result[0].name)
        self.assertEqual('gre-tunnel', result[0].tags['name'])
        self.assertEqual('10.11.80.229', result[0].tags['local_ip'])
        self.assertEqual('10.11.80.228', result[0].tags['peer_ip'])
        self.assertEqual('GRE Tunnels - Enabled', result[0].tags['display-name'])
        self.assertEqual(0, result[0].value)

        self.assertTrue(isinstance(result[1], WriteDoubleMetric))
        self.assertEqual('gre-tunnel-status', result[1].name)
        self.assertEqual('gre-tunnel', result[1].tags['name'])
        self.assertEqual('10.11.80.229', result[1].tags['local_ip'])
        self.assertEqual('10.11.80.228', result[1].tags['peer_ip'])
        self.assertEqual('GRE Tunnels - State', result[1].tags['display-name'])
        self.assertEqual(1, result[1].value)


if __name__ == '__main__':
    unittest.main()
