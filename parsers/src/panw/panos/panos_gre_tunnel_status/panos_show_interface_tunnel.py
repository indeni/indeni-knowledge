from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class PanosShowInterfaceTunnel(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            interfaces_list = helper_methods.parse_data_as_list(raw_data, 'panos_show_interface_tunnel.textfsm')
            for tunnel in interfaces_list[0]['int_name']:
                self.write_dynamic_variable('int_name', tunnel)
        return self.output
