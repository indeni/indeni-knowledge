BEGIN {
    first = 1
    raid_status = 1
    missing_disk = 0
    overall_raid_status_found = 0

}

#Disk Pair A
/^Disk Pair/{

    if (!first) {
        raid_states[raid_name] = raid_status
    }

    # Reset variables
    raid_status = 1
    raid_name = "Disk Pair " $3
    first = 0
}

# This catches the instances where disk pairs is not used and sets the name to "raid"
#Overall RAID status                        Good
/^Overall RAID status/ {
    overall_raid_status_found = 1
    raid_name = "Single RAID"
}


#   Disk id 1                            Present
#   Disk id 2                            Missing
/^\s+Disk id/ {

    if ($NF != "Missing" && $NF != "Present") {
        raid_status = 0
    }

    # Begin test for raid not enabled. See examples above for disk and raid status input.

    if ($NF == "Missing" && overall_raid_status_found) {
        missing_disk++
    }

    if (!disk_states[raid_name]) {
        disk_states[raid_name] = $3 ":" $NF
    } else {
        disk_states[raid_name] = disk_states[raid_name] ", " $3 ": " $NF
    }

    next

}

END {

    raid_states[raid_name] = raid_status

    for(name in raid_states) {
        tags["raid"] = name
        tags["diskid-states"] = disk_states[name]
        writeDoubleMetric("raid-status", tags, "gauge", raid_states[name], "false")  # Converted to new syntax by change_ind_scripts.py script
    }
    if (overall_raid_status_found) {
        if (missing_disk > 0) {
            raid_configured = 0
        } else {
            raid_configured = 1
        }
        writeDoubleMetric("panw-raid-configured", tags, "gauge", raid_configured, "false")  # Converted to new syntax by change_ind_scripts.py script
    }
}