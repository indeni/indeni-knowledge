# Status will look like this:
#debug:on level:debug

/debug:/ {
    debugtags["name"] = "log-receiver"

    state = 1
    #The default state for this debug level is info so info will set the state to 0 or not in debug.
    # Added || debug:off so as not to alert if the user forcibly changed it to off.
    if ($NF == "level:info" || $NF == "debug:off") {
        state = 0
    }
    writeDoubleMetric("debug-status", debugtags, "gauge", state, "false")  # Converted to new syntax by change_ind_scripts.py script
}