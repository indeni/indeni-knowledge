from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class PanosUrlFilteringCredentialTheftParser(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        # This script will write the blocked state in PANOS url filtering profiles
        # It will write state metrics for the following members specifically to keep track that they are blocked
        # command-and-control
        # copyright-infringement
        # dynamic-dns
        # extremism
        # malware
        # parked
        # phishing
        # proxy-avoidance-and-anonymizers
        # unknown

        # Check if input is empty to avoid some errors
        if raw_data.strip() != '':
            # Parse the raw data as xml
            xml_data = helper_methods.parse_data_as_xml(raw_data)
            # Wrap the path in try/except to keep the script simpler(otherwise a check will be necessary for each section)
            if xml_data and xml_data['response']['result'] is not None:
                if xml_data and xml_data['response']['result']['url-filtering'] is not None:
                    try:
                        for entry in helper_methods.to_list(xml_data['response']['result']['url-filtering']['entry']):
                            self.write_entry(entry)
                            pass
                    except KeyError:
                        pass

        return self.output

    def write_entry(self, entry):
        # Here we define the members we want to report metrics on
        # Each member will get a default value of 0.0
        members_to_report = {}
        members_to_report['command-and-control'] = float(0.0)
        members_to_report['copyright-infringement'] = float(0.0)
        members_to_report['dynamic-dns'] = float(0.0)
        members_to_report['extremism'] = float(0.0)
        members_to_report['malware'] = float(0.0)
        members_to_report['parked'] = float(0.0)
        members_to_report['phishing'] = float(0.0)
        members_to_report['proxy-avoidance-and-anonymizers'] = float(0.0)
        members_to_report['unknown'] = float(0.0)

        # Try to get the credential_enforcement entry, if that does not exist no metrics will be written
        credential_enforcement = entry.get('credential-enforcement', None)
        if credential_enforcement is None:
            return  # Credential enforcement section does not exist, write no metrics

        # Get the entry name
        entry_name = entry.get('@name', None)

        # Get the entry mode
        mode = credential_enforcement.get('mode')
        disabled = False
        for key in mode:
            if key == 'disabled':
                disabled = True

        # If credential enforcement is not disabled we will figure out which members are blocked
        if not disabled:
            try:
                # Loop through the block section. If the members we want to report on are there, they are in a good state and a 1.0 value will be written.
                # If the node has no attributes the value will be returned as a pure string
                # Otherwise the value will be in the dictionary item #text
                # This loop will check if a string is returned or if #text will need to be resolved
                for member in helper_methods.to_list(credential_enforcement['block']['member']):
                    if (isinstance(member, str)):
                        member_text = member
                    else:
                        member_text = member.get('#text', None)
                    if member_text is None:
                        continue
                    if members_to_report.get(member_text, None) is not None:
                        members_to_report[member_text] = float(1.0)
            except KeyError:
                pass

        # Write the mode as a metric
        self.write_double_metric('url-filtering-entry-mode-disabled', {'category': entry_name}, 'gauge', float(1.0) if disabled else float(0.0), False)
        # Write the metrics for this section
        for member, value in members_to_report.items():
            self.write_double_metric('url-filtering-profile-entry-status', {'category': entry_name, 'member': member}, 'gauge', value, False)
