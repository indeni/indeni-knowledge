import os
import unittest

from panw.panos.show_counter_global_filter_severity_drop.show_counter_global_filter_severity_drop import ShowCounterGlobalFilterSeverity
from parser_service.public.action import WriteDoubleMetric

class TestShowCounterGlobalFilterSeverity(unittest.TestCase):
    def setUp(self):
        self.parser = ShowCounterGlobalFilterSeverity()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_multiple_counters(self):
        expected_results = {
            'flow_xmt_platform_encap_err:': 507,
            'flow_no_interface:': 15893,
            'flow_host_decap_err:':163,
            'flow_host_service_unknown:': 14333,
            'flow_rcv_dot1q_tag_err:': 15893,
            'url_request_pkt_drop:': 58,
            'flow_policy_nat_land:': 696,
            'flow_fwd_l3_mcast_drop:': 104140,
            'flow_parse_ip_ver:':1,
            'flow_parse_l4_cksm:': 1,
            'flow_parse_l4_tcpsynfin:': 16,
            'flow_fwd_l3_noarp:': 640,
            'flow_fwd_l3_noroute:': 8579,
            'flow_fwd_l3_ttl_zero:': 212,
            'flow_policy_nat:': 2,
            'flow_parse_l4_tcpfin:': 43,
            'flow_arp_rcv_err:': 3,
            'flow_icmp_err_not_passing_thru:': 206,
            'flow_fwd_l3_bcast_drop:': 2769,
            'flow_host_service_deny:': 8691,
            'flow_rcv_err:': 61,
            'flow_ipv6_disabled:': 316857,
            'flow_policy_nofwd:': 44465,
            'flow_tcp_non_syn_drop:':67984
        }

        # Act
        result = self.parser.parse_file(self.current_dir + '/multiple_counters.input', {}, {})
        self.assertEqual(24, len(result))

        for single_result in result:
            for expected_entry in expected_results:
                if expected_entry in single_result.tags['name']:
                    self.assertEqual(expected_results[expected_entry], single_result.value)

    def test_no_counters(self):
        result = self.parser.parse_file(self.current_dir + '/no_counters.input', {}, {})
        self.assertIsInstance(result, list)
        self.assertEqual(len(result), 0)

    def test_result_failed(self):
        result = self.parser.parse_file(self.current_dir + '/result_failed.input', {}, {})
        self.assertIsInstance(result, list)
        self.assertEqual(len(result), 0)

if __name__ == '__main__':
    unittest.main()