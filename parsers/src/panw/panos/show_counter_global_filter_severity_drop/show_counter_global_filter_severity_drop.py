from parser_service.public.base_parser import BaseParser
from parser_service.public.helper_methods import *


class ShowCounterGlobalFilterSeverity(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data = parse_data_as_xml(raw_data)
            if data and 'result' in data['response']:
                if data['response']['@status'] == "success":
                    if data['response']['result']['global']['counters'] is not None:
                        entries = data['response']['result']['global']['counters']['entry']
                        if type(entries) != list:
                            entries = [entries]
                        for entry in entries:
                            tags = {'name': '{}'.format(entry['name'])}
                            value = float(entry['value'])
                            self.write_double_metric("packet-drop-counter", tags, 'counter', value, True, "Packet Drop Counters", 'number', 'name')

        return self.output
