BEGIN {
    metric_tags["live-config"] = "true"
    metric_tags["display-name"] = "Service"
    metric_tags["im.identity-tags"] = "name"
}

# SAMPLE Output
#admin@PA-220> show system services
#HTTP       : Enabled
#HTTPS      : Enabled
#Telnet     : Enabled
#SSH        : Enabled
#Ping       : Enabled
#SNMP       : Enabled
/^HTTP[^S]/ {
    # The service name can be retreived using $1 and its status using $3
    service = $1
    status = $3
    # An alarm will be triggered if the service is enabled "true"
    if (status == "Enabled") {
        service_state = "true"
    } else {
        service_state = "false"
    }
    metric_tags["name"] = service
    writeComplexMetricString("http-server-enabled", null, service_state, "false")  # Converted to new syntax by change_ind_scripts.py script
}

/^Telnet/ {
    # The service name can be retreived using $1 and its status using $3
    service = $1
    status = $3
    # An alarm will be triggered if the service is enabled "true"
    if (status == "Enabled") {
        service_state = "true"
    } else {
        service_state = "false"
    }
    metric_tags["name"] = service
    writeComplexMetricString("telnet-enabled", null, service_state, "false")  # Converted to new syntax by change_ind_scripts.py script
}