import os
import unittest
from panw.panos.panos_show_snmp_community.panos_show_snmp_community import PanosShowSnmpCommunity
from parser_service.public.action import *

class TestPanosShowSnmpTrapCommunity(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = PanosShowSnmpCommunity()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_snmp_community_v2c(self):
        data = self.parser.parse_file(self.current_dir + '/snmp_community_v2c.input', {}, {})
        self.assertEqual(1, len(data))
        self.assertEqual(isinstance(data,list), True)
        self.assertEqual([{'community': 'No_Trap_SNMP'}], data[0].value)

    def test_snmp_community_v3(self):
        data = self.parser.parse_file(self.current_dir + '/snmp_community_v3.input', {}, {})
        self.assertEqual(isinstance(data,list), True)

    def test_no_snmp_community(self):
        data = self.parser.parse_file(self.current_dir + '/no_snmp_community.input', {}, {})
        self.assertEqual(0, len(data))
        self.assertEqual(isinstance(data,list), True)

    def test_snmp_community_empty_result(self):
        data = self.parser.parse_file(self.current_dir + '/snmp_community_empty_result.input', {}, {})
        self.assertEqual(0, len(data))
        self.assertEqual(isinstance(data,list), True)

    def test_snmp_community_empty_community(self):
        data = self.parser.parse_file(self.current_dir + '/snmp_community_empty_community.input', {}, {})
        self.assertEqual(0, len(data))
        self.assertEqual(isinstance(data,list), True)

    def test_snmp_community_traps(self):
        data = self.parser.parse_file(self.current_dir + '/snmp_community_traps.input', {}, {})
        self.assertEqual(0, len(data))
        self.assertEqual(isinstance(data,list), True)

if __name__ == '__main__':
    unittest.main()