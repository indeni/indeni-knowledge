from parser_service.public.base_parser import BaseParser
from parser_service.public import helper_methods

class PanosShowSnmpCommunity(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data = helper_methods.parse_data_as_xml(raw_data)
            try:
                entry = {}
                list = []
                entry['community'] = data['response']['result']['access-setting']['version']['v2c']['snmp-community-string']
                list.append(entry)
                self.write_complex_metric_object_array('snmp-communities',{},list,True,'SNMP-Community')
            except:
                    pass

        return self.output