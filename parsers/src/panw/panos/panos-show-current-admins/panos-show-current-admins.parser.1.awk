#  Admin                           From   Client Session-start   Idle-for    Session-expiry
#---------------------------------------------------------------------------------------------
#  indeni                  10.10.10.131      Web 05/08 04:20:39  00:00:02s  06/07 04:20:39
#* indeni                  10.10.10.110      CLI 05/08 05:49:10  00:00:00s  06/07 05:49:10

BEGIN {
    current = ""
}

/Web|CLI/ {
    if ($1 == "*") {
        list[$2] = "current"
        current_admin = $2
    }
    else
        list[$1] = "global"
}

END {
    if (current_admin != "") {
        adminusers[1, "adminname"] = current_admin
    }
    else {
        count = 0
        for (u in list) {
            count++
            adminusers[count, "adminname"] = u
        }
    }
    writeComplexMetricObjectArray("admins_in_use", null, adminusers, "false")
}
