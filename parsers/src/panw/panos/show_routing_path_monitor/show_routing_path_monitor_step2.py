from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser
import re

class PanosShowRoutingPathMonitor(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            xml_data = helper_methods.parse_data_as_xml(raw_data)

            if xml_data and xml_data['response']['@status'] == 'success':
                response = xml_data['response']['result']
                # print(response)
                if response and 'entry' in response:
                    path_monitor_entries = response['entry']
                    if type(response['entry']) != list:
                        path_monitor_entries = [response['entry']]
                    default_route_exist = False
                    default_routes_up = 0
                    for entry in path_monitor_entries:
                        if entry.get('monitordst-0') is not None:
                            tags = {}
                            tags['virtual-router'] = dynamic_var['virtual-router']
                            tags['destination'] = entry['destination']
                            tags['nexthop'] = entry['nexthop']
                            tags['interface'] = entry['interface']
                            tags['monitor_destination'] = entry['monitordst-0']
                            tags['name'] = f"virtual-router: {dynamic_var['virtual-router']} destination: {entry['destination']} nexthop: {entry['nexthop']} monitor_dst: {entry['monitordst-0']}"
                            self.write_double_metric('static-route-path-monitor-status', tags, 'gauge', 1 if len(re.findall('Down', entry['pathmonitor-status']))==0 else 0, True, 'Path-Monitor Status', 'state', 'name' )
                            if entry['destination'] == '0.0.0.0/0':
                                default_route_exist = True
                                if len(re.findall('Down', entry['pathmonitor-status']))==0:
                                    default_routes_up += 1
                        else:
                            if entry.get('destination') == '0.0.0.0/0' and 'A' in list(entry.get('flags')):
                                default_routes_up += 1
                    if default_route_exist:
                        tags = {}
                        tags['virtual-router'] = dynamic_var['virtual-router']
                        self.write_double_metric('static-default-route-path-monitor-status', tags, 'gauge', 1 if default_routes_up > 0 else 0, False )
        return self.output
