import os
import unittest

from panw.panos.show_routing_path_monitor.show_routing_path_monitor_step2 import PanosShowRoutingPathMonitor
from parser_service.public.action import *

class TestPanosShowRoutingPathMonitor(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = PanosShowRoutingPathMonitor()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_path_monitor_configured_disabled(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/path_monitor_configured_disabled.input', {'virtual-router': 'default'}, {})
        # Assert
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].name, 'static-route-path-monitor-status')
        self.assertEqual(result[0].value, 1)
        self.assertEqual(result[0].tags['name'], 'virtual-router: default destination: 172.16.16.16/32 nexthop: 10.11.95.254 monitor_dst: 10.11.95.254')
        self.assertEqual(result[0].tags['display-name'], 'Path-Monitor Status')
        self.assertEqual(result[0].tags['im.identity-tags'],'name')

    def test_path_monitor_multiple_monitors(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/path_monitor_multiple_monitors.input', {'virtual-router': 'default'}, {})
        # Assert
        self.assertEqual(2, len(result))
        self.assertEqual(result[0].name, 'static-route-path-monitor-status')
        self.assertEqual(result[0].value, 1)
        self.assertEqual(result[0].tags['name'], 'virtual-router: default destination: 172.16.16.16/32 nexthop: 10.11.95.254 monitor_dst: 10.11.95.254')
        self.assertEqual(result[0].tags['display-name'], 'Path-Monitor Status')
        self.assertEqual(result[0].tags['im.identity-tags'],'name')
        self.assertEqual(result[1].name, 'static-route-path-monitor-status')
        self.assertEqual(result[1].value, 0)
        self.assertEqual(result[1].tags['name'], 'virtual-router: default destination: 172.16.16.47/32 nexthop: 10.11.95.254 monitor_dst: 10.11.95.254')
        self.assertEqual(result[1].tags['display-name'], 'Path-Monitor Status')
        self.assertEqual(result[1].tags['im.identity-tags'],'name')

    def test_path_monitor_configured_default_route_no_monitor(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/path_monitor_configured_default_route_no_monitor.input', {'virtual-router': 'default'}, {})
        # Assert
        self.assertEqual(4, len(result))
        self.assertEqual(result[0].name, 'static-route-path-monitor-status')
        self.assertEqual(result[0].value, 1)
        self.assertEqual(result[0].tags['name'], 'virtual-router: default destination: 172.16.16.16/32 nexthop: 10.11.95.254 monitor_dst: 10.11.95.254')
        self.assertEqual(result[0].tags['display-name'], 'Path-Monitor Status')
        self.assertEqual(result[0].tags['im.identity-tags'],'name')
        self.assertEqual(result[1].name, 'static-route-path-monitor-status')
        self.assertEqual(result[1].value, 0)
        self.assertEqual(result[1].tags['name'], 'virtual-router: default destination: 0.0.0.0/0 nexthop: 10.11.95.254 monitor_dst: 172.16.16.16')
        self.assertEqual(result[1].tags['display-name'], 'Path-Monitor Status')
        self.assertEqual(result[1].tags['im.identity-tags'],'name')
        self.assertEqual(result[2].name, 'static-route-path-monitor-status')
        self.assertEqual(result[2].value, 0)
        self.assertEqual(result[2].tags['name'], 'virtual-router: default destination: 172.16.16.16/32 nexthop: 10.11.95.254 monitor_dst: 10.11.95.254')
        self.assertEqual(result[2].tags['display-name'], 'Path-Monitor Status')
        self.assertEqual(result[2].tags['im.identity-tags'],'name')
        self.assertEqual(result[3].name, 'static-default-route-path-monitor-status')
        self.assertEqual(result[3].value, 1)
        self.assertEqual(result[3].tags['virtual-router'],'default')

    def test_path_monitor_configured_default_route_monitored_up(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/path_monitor_configured_default_route_monitored_up.input', {'virtual-router': 'default'}, {})
        # Assert
        self.assertEqual(5, len(result))
        self.assertEqual(result[0].name, 'static-route-path-monitor-status')
        self.assertEqual(result[0].value, 1)
        self.assertEqual(result[0].tags['name'], 'virtual-router: default destination: 172.16.16.16/32 nexthop: 10.11.95.254 monitor_dst: 10.11.95.254')
        self.assertEqual(result[0].tags['display-name'], 'Path-Monitor Status')
        self.assertEqual(result[0].tags['im.identity-tags'],'name')
        self.assertEqual(result[1].name, 'static-route-path-monitor-status')
        self.assertEqual(result[1].value, 0)
        self.assertEqual(result[1].tags['name'], 'virtual-router: default destination: 0.0.0.0/0 nexthop: 10.11.95.254 monitor_dst: 172.16.16.16')
        self.assertEqual(result[1].tags['display-name'], 'Path-Monitor Status')
        self.assertEqual(result[1].tags['im.identity-tags'],'name')
        self.assertEqual(result[2].name, 'static-route-path-monitor-status')
        self.assertEqual(result[2].value, 0)
        self.assertEqual(result[2].tags['name'], 'virtual-router: default destination: 172.16.16.16/32 nexthop: 10.11.95.254 monitor_dst: 10.11.95.254')
        self.assertEqual(result[2].tags['display-name'], 'Path-Monitor Status')
        self.assertEqual(result[2].tags['im.identity-tags'],'name')
        self.assertEqual(result[3].name, 'static-route-path-monitor-status')
        self.assertEqual(result[3].value, 1)
        self.assertEqual(result[3].tags['name'], 'virtual-router: default destination: 0.0.0.0/0 nexthop: 10.111.95.215 monitor_dst: 172.16.16.16')
        self.assertEqual(result[3].tags['display-name'], 'Path-Monitor Status')
        self.assertEqual(result[3].tags['im.identity-tags'],'name')
        self.assertEqual(result[4].name, 'static-default-route-path-monitor-status')
        self.assertEqual(result[4].value, 1)
        self.assertEqual(result[4].tags['virtual-router'],'default')

    def test_path_monitor_configured_default_route_monitored_down(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/path_monitor_configured_default_route_monitored_down.input', {'virtual-router': 'default'}, {})
        # Assert
        self.assertEqual(5, len(result))
        self.assertEqual(result[0].name, 'static-route-path-monitor-status')
        self.assertEqual(result[0].value, 1)
        self.assertEqual(result[0].tags['name'], 'virtual-router: default destination: 172.16.16.16/32 nexthop: 10.11.95.254 monitor_dst: 10.11.95.254')
        self.assertEqual(result[0].tags['display-name'], 'Path-Monitor Status')
        self.assertEqual(result[0].tags['im.identity-tags'],'name')
        self.assertEqual(result[1].name, 'static-route-path-monitor-status')
        self.assertEqual(result[1].value, 0)
        self.assertEqual(result[1].tags['name'], 'virtual-router: default destination: 0.0.0.0/0 nexthop: 10.11.95.254 monitor_dst: 172.16.16.16')
        self.assertEqual(result[1].tags['display-name'], 'Path-Monitor Status')
        self.assertEqual(result[1].tags['im.identity-tags'],'name')
        self.assertEqual(result[2].name, 'static-route-path-monitor-status')
        self.assertEqual(result[2].value, 0)
        self.assertEqual(result[2].tags['name'], 'virtual-router: default destination: 172.16.16.16/32 nexthop: 10.11.95.254 monitor_dst: 10.11.95.254')
        self.assertEqual(result[2].tags['display-name'], 'Path-Monitor Status')
        self.assertEqual(result[2].tags['im.identity-tags'],'name')
        self.assertEqual(result[3].name, 'static-route-path-monitor-status')
        self.assertEqual(result[3].value, 0)
        self.assertEqual(result[3].tags['name'], 'virtual-router: default destination: 0.0.0.0/0 nexthop: 10.111.95.215 monitor_dst: 172.16.16.16')
        self.assertEqual(result[3].tags['display-name'], 'Path-Monitor Status')
        self.assertEqual(result[3].tags['im.identity-tags'],'name')
        self.assertEqual(result[4].name, 'static-default-route-path-monitor-status')
        self.assertEqual(result[4].value, 0)
        self.assertEqual(result[4].tags['virtual-router'],'default')
        
if __name__ == '__main__':
    unittest.main()