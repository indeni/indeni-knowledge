import os
import unittest

from panw.panos.show_routing_path_monitor.show_routing_path_monitor_step1 import PanosGetVirtualRouter
from parser_service.public.action import *

class TestShowPanoramaStatus(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = PanosGetVirtualRouter()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_get_virtual_router_two(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/get_virtual_router_two.input', {}, {})
        # Assert
        self.assertEqual(2, len(result))
        self.assertEqual(result[0].key, 'virtual-router')
        self.assertEqual(result[0].value, 'default')
        self.assertEqual(result[1].key, 'virtual-router')
        self.assertEqual(result[1].value, 'External_VR')

if __name__ == '__main__':
    unittest.main()