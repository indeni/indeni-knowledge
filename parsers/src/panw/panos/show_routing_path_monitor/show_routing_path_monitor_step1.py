from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class PanosGetVirtualRouter(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            xml_data = helper_methods.parse_data_as_xml(raw_data)
            if xml_data and xml_data['response']['@status'] == 'success':
                response = xml_data['response']['result']['virtual-router']
                if response and 'entry' in response:
                    virtual_router_entries = response['entry']
                    if type(response['entry']) != list:
                        virtual_router_entries = [response['entry']]
                    for entry in virtual_router_entries:
                        self.write_dynamic_variable('virtual-router', entry['@name'])
        return self.output
