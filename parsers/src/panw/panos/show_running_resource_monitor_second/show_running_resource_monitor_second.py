from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class ShowRunningResourceMonitorParser(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        try:
            if raw_data:
                data = helper_methods.parse_data_as_xml(raw_data)
                data_processors = data['response']['result']['resource-monitor']['data-processors']
                all_cores_info = []
                for data_processor in data_processors:
                    num_for_sum = 0
                    iter_num = 0
                    processor_info_dict = {}
                    processor_info_dict['processor'] = str(data_processor)
                    processor_info_dict['cores_info'] = []
                    for core in data_processors[data_processor]['second']['cpu-load-average']['entry']:
                        core_info_dict = {}
                        core_info_dict['core'] = core['coreid']
                        core['value'] = core['value'].split(",")
                        num_for_sum += int(core['value'][-1])
                        iter_num += 1
                        core_info_dict['high_values'] = [int(x) for x in core['value'] if int(x)>80]
                        core_info_dict['core_avg'] = sum([int(x) for x in core['value']])
                        if core_info_dict['core_avg'] > 0:
                            core_info_dict['core_avg'] = core_info_dict['core_avg']/len(core['value'])
                        processor_info_dict['cores_info'].append(core_info_dict)
                    all_cores_info.append(processor_info_dict)
                    average_value = 0 if not (iter_num > 0) else int(num_for_sum / iter_num)
                    tags = {'cpu-id': str(data_processor).replace('s', 'slot'),'cpu-is-avg': 'false','dp': 'true'}
                    self.write_double_metric('cpu-usage', tags, 'gauge', average_value, True, 'CPU Usage', 'percentage','cpu-id')
                cpu_system_avg = 0
                number_items_avg = 0
                for processor_data in all_cores_info: 
                    for core_data in processor_data['cores_info']:
                        cpu_system_avg += int(core_data['core_avg'])
                        number_items_avg += 1
                if cpu_system_avg > 0:
                    cpu_system_avg = cpu_system_avg/number_items_avg
                cpu_alerted = []
                for processor_data in all_cores_info: 
                    processor_cpu_microburst = 1
                    for core_data in processor_data['cores_info']:
                        tags = {}
                        tags['cpu-id'] = str(processor_data['processor']).replace('s', 'slot')
                        if core_data['high_values']:
                            for core_value in core_data['high_values']:
                                if core_value > 1.5*cpu_system_avg:
                                    processor_cpu_microburst = 0
                    self.write_double_metric('panos-cpu-microburst', tags, 'gauge', processor_cpu_microburst, False)

        except Exception:
            return self.output
        return self.output