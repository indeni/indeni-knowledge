import os
import unittest
from panw.panos.show_running_resource_monitor_second.show_running_resource_monitor_second import ShowRunningResourceMonitorParser
from parser_service.public.action import *

class TestShowRunningResourceMonitorParser(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = ShowRunningResourceMonitorParser()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_empty(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/empty.input', {}, {})
        # Assert
        self.assertEqual(0, len(result))

    def test_empty_response(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/empty_response.input', {}, {})
        # Assert
        self.assertEqual(0, len(result))

    def test_vm_with_4_cores(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/vm_with_4_cores.input', {}, {})
        # Assert
        self.assertEqual(2, len(result))
        self.assertTrue(isinstance(result[0], WriteDoubleMetric))
        self.assertEqual(result[0].name, 'cpu-usage')
        self.assertEqual(result[0].tags['cpu-id'], 'dp0')
        self.assertEqual(result[0].tags['cpu-is-avg'], 'false')
        self.assertEqual(result[0].tags['dp'], 'true')
        self.assertEqual(result[0].value, 0)

    def test_multiple_dp_1_microbust(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/multiple_dp_1_microbust.input', {}, {})
        # Assert
        self.assertEqual(4, len(result))
        self.assertTrue(isinstance(result[0], WriteDoubleMetric))
        self.assertEqual(result[0].name, 'cpu-usage')
        self.assertEqual(result[0].tags['cpu-id'], 'dp0')
        self.assertEqual(result[0].tags['cpu-is-avg'], 'false')
        self.assertEqual(result[0].tags['dp'], 'true')
        self.assertEqual(result[0].value, 44)
        self.assertTrue(isinstance(result[1], WriteDoubleMetric))
        self.assertEqual(result[1].name, 'cpu-usage')
        self.assertEqual(result[1].tags['cpu-id'], 'dp1')
        self.assertEqual(result[1].tags['cpu-is-avg'], 'false')
        self.assertEqual(result[1].tags['dp'], 'true')
        self.assertEqual(result[1].value, 44)
        self.assertTrue(isinstance(result[2], WriteDoubleMetric))
        self.assertEqual(result[2].name, 'panos-cpu-microburst')
        self.assertEqual(result[2].tags['cpu-id'], 'dp0')
        self.assertEqual(result[2].value, 0)
        self.assertTrue(isinstance(result[3], WriteDoubleMetric))
        self.assertEqual(result[3].name, 'panos-cpu-microburst')
        self.assertEqual(result[3].tags['cpu-id'], 'dp1')
        self.assertEqual(result[3].value, 1)

    def test_multiple_slots_multiple_dp(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/multiple_slots_multiple_dp.input', {}, {})
        # Assert
        self.assertEqual(8, len(result))
        self.assertTrue(isinstance(result[0], WriteDoubleMetric))
        self.assertEqual(result[0].name, 'cpu-usage')
        self.assertEqual(result[0].tags['cpu-id'], 'slot5dp1')
        self.assertEqual(result[0].tags['cpu-is-avg'], 'false')
        self.assertEqual(result[0].value, 39)
        self.assertTrue(isinstance(result[1], WriteDoubleMetric))
        self.assertEqual(result[1].name, 'cpu-usage')
        self.assertEqual(result[1].tags['cpu-id'], 'slot5dp0')
        self.assertEqual(result[1].tags['cpu-is-avg'], 'false')
        self.assertEqual(result[1].value, 39)
        self.assertTrue(isinstance(result[2], WriteDoubleMetric))
        self.assertEqual(result[2].name, 'cpu-usage')
        self.assertEqual(result[2].tags['cpu-id'], 'slot2dp0')
        self.assertEqual(result[2].tags['cpu-is-avg'], 'false')
        self.assertEqual(result[2].value, 39)
        self.assertTrue(isinstance(result[3], WriteDoubleMetric))
        self.assertEqual(result[3].name, 'cpu-usage')
        self.assertEqual(result[3].tags['cpu-id'], 'slot2dp1')
        self.assertEqual(result[3].tags['cpu-is-avg'], 'false')
        self.assertEqual(result[3].value, 39)
        self.assertTrue(isinstance(result[4], WriteDoubleMetric))
        self.assertEqual(result[4].name, 'panos-cpu-microburst')
        self.assertEqual(result[4].tags['cpu-id'], 'slot5dp1')
        self.assertEqual(result[4].value, 1)
        self.assertTrue(isinstance(result[5], WriteDoubleMetric))
        self.assertEqual(result[5].name, 'panos-cpu-microburst')
        self.assertEqual(result[5].tags['cpu-id'], 'slot5dp0')
        self.assertEqual(result[5].value, 1)
        self.assertTrue(isinstance(result[6], WriteDoubleMetric))
        self.assertEqual(result[6].name, 'panos-cpu-microburst')
        self.assertEqual(result[6].tags['cpu-id'], 'slot2dp0')
        self.assertEqual(result[6].value, 1)
        self.assertTrue(isinstance(result[7], WriteDoubleMetric))
        self.assertEqual(result[7].name, 'panos-cpu-microburst')
        self.assertEqual(result[7].tags['cpu-id'], 'slot2dp1')
        self.assertEqual(result[7].value, 1)

    def test_multiple_dp_low_use(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/multiple_dp_low_use.input', {}, {})
        # Assert
        self.assertEqual(2, len(result))
        self.assertTrue(isinstance(result[0], WriteDoubleMetric))
        self.assertTrue(isinstance(result[0], WriteDoubleMetric))
        self.assertEqual(result[0].name, 'cpu-usage')
        self.assertEqual(result[0].tags['cpu-id'], 'dp0')
        self.assertEqual(result[0].tags['cpu-is-avg'], 'false')
        self.assertEqual(result[0].value, 0)
        self.assertEqual(result[1].name, 'panos-cpu-microburst')
        self.assertEqual(result[1].tags['cpu-id'], 'dp0')
        self.assertEqual(result[1].value, 1)

if __name__ == '__main__':
    unittest.main()
