BEGIN {
    forward_proxy = 0
    ssl_decrypt = 0
}

#Forward Proxy Ready           : yes
/^Forward Proxy Ready/ {
    if ($5 == "yes") {
        forward_proxy = 1
    }
}

#Inbound Proxy Ready           : yes
/^Inbound Proxy Ready/ {
    if (($5 == "yes") || (forward_proxy == 1)) {
        ssl_decrypt = 1
    }
}

END {
    writeDynamicVariable("decrypt_enabled", ssl_decrypt )
}