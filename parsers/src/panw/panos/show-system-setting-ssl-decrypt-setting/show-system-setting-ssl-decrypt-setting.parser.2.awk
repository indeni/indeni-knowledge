BEGIN {
    ssl_decryption_enabled = dynamic("decrypt_enabled")
    tls13_supported = 1
}

/sw-version/ && ssl_decryption_enabled {

    # This version_found gets set if it finds a version after running the panos command.
    #sw-version: 9.0.0-b11
    #sw-version: 6.1.10
    #sw-version: 7.0.13
    #sw-version: 7.1.10
    #sw-version: 7.1.21
    #sw-version: 8.0.14
    #sw-version: 8.0.13
    #sw-version: 8.1.2
    #sw-version: 8.1.4

    sw_version = $2
    split(sw_version, version_vals, ".")
    major = version_vals[1]
    minor = version_vals[2]
    patch = version_vals[3]

    # See if latest version entry is less than this release's release epoch.
    if (( major < 7 ) || (( major == 7 ) && ( minor == 0 )) || (( major == 7 ) && ( minor == 1 ) && ( patch < 21 )) || (( major == 8 ) && ( minor == 0 ) && ( patch < 14 )) || (( major == 8 ) && ( minor == 1 ) && ( patch < 4 ))) {
        tls13_supported = 0
    }

    decryption_tags["cipher"] = "TLS 1.3"
    writeDoubleMetric("ssl-cipher-not-supported", decryption_tags, "gauge", tls13_supported, "false")  # Converted to new syntax by change_ind_scripts.py script
}