import os
import unittest
from panw.panos.show_counter_global_name_ctd_exceed_queue_limit.show_counter_global_name_ctd_exceed_queue_limit import ShowCounterGlobalNameCtdExceedQueueLimit

class TestShowCounterGlobalNameCtdExceedQueueLimit(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = ShowCounterGlobalNameCtdExceedQueueLimit()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_show_counter_global_name_ctd_exceed_queue_limit(self):
        result = self.parser.parse_file(self.current_dir + '/show_counter_global_name_ctd_exceed_queue_limit.input', {}, {})
        self.assertEqual(2, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].ds_type, 'counter')
        self.assertEqual(result[0].name, 'ctd-exceed-queue-limit')
        self.assertEqual(result[0].value, 0)
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].ds_type, 'gauge')
        self.assertEqual(result[1].name, 'ctd-exceed-queue-limit-webui')
        self.assertEqual(result[1].tags['live-config'], 'true')
        self.assertEqual(result[1].tags['display-name'], 'Global counters - ctd exceed queue limit')
        self.assertEqual(result[1].tags['im.dstype.displayType'], 'number')
        self.assertEqual(result[1].value, 0)

    def test_show_counter_global_name_ctd_exceed_queue_limit_high(self):
        result = self.parser.parse_file(self.current_dir + '/show_counter_global_name_ctd_exceed_queue_limit_high.input', {}, {})
        self.assertEqual(2, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].ds_type, 'counter')
        self.assertEqual(result[0].name, 'ctd-exceed-queue-limit')
        self.assertEqual(result[0].value, 64)
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].ds_type, 'gauge')
        self.assertEqual(result[1].name, 'ctd-exceed-queue-limit-webui')
        self.assertEqual(result[1].tags['live-config'], 'true')
        self.assertEqual(result[1].tags['display-name'], 'Global counters - ctd exceed queue limit')
        self.assertEqual(result[1].tags['im.dstype.displayType'], 'number')
        self.assertEqual(result[1].value, 64)

if __name__ == '__main__':
    unittest.main()