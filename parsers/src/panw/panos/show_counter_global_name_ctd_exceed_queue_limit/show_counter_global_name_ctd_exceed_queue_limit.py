from parser_service.public.base_parser import BaseParser
from parser_service.public.helper_methods import *

class ShowCounterGlobalNameCtdExceedQueueLimit(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data = parse_data_as_xml(raw_data)
            if data and 'result' in data['response']:
                if data['response']['@status'] == "success":
                    try:
                        value = int(data['response']['result']['global']['counters']['entry']['value'])
                        self.write_double_metric("ctd-exceed-queue-limit", {'name':'ctd_exceed_queue_limit'}, 'counter', value, False)
                        self.write_double_metric("ctd-exceed-queue-limit-webui", {'name':'ctd_exceed_queue_limit'}, 'gauge', value, True, "Global counters - ctd exceed queue limit", 'number')
                    except:
                        pass
        return self.output