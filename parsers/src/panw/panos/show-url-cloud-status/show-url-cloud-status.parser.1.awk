BEGIN {
	connected = ""
}

# NOTE:
# The output of this API call may come as a CDATA (older versions) or XML (newer versions)

# Cloud connection :                 not connected
# OR
# Cloud connection :                 connected
/Cloud connection/ {
	if ($(NF-1) == "not") {
		connected = 0
	} else {
		connected = 1
	}
}

# <cloud-connection>connected</cloud-connection>
/cloud-connection/ {
	if ($0 ~ />connected</) {
		connected = 1
	} else {
		connected = 0
	}
}

END {
	if (connected != "") {
		writeDoubleMetric("url-filtering-cloud-connected", null, "gauge", connected, "true", "URL Cloud Connected?", "boolean", "")  # Converted to new syntax by change_ind_scripts.py script
	}
}