from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class PanosManagementPermittedIp(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data = helper_methods.parse_data_as_xml(raw_data)
            if data:
                output = data['response']['result']
                tags = {}
                entries = []
                tempdict = {}
                if output is not None and output['permitted-ip'] is not None:
                    temp = output['permitted-ip']['entry']
                    temp_long = [x['@name'] for x in temp if len(temp) > 1 and raw_data.count('entry name=') > 1]
                    if (len(temp_long) > 1):
                        for i in temp_long:
                            tempdict['ip-address'] = i
                            entries.append(dict(tempdict))
                    else:
                        tempdict['ip-address'] = temp['@name']
                        entries.append(dict(tempdict))
                    self.write_complex_metric_object_array("panos-mgmt-permitted-ip", tags, entries, False)
        return self.output

