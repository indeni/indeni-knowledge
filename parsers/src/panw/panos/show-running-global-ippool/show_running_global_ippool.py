from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class NATMemoryParser(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        data = helper_methods.parse_data_as_list(raw_data, 'show_running_global_ippool.textfsm')
        if data:
            for mementry in data:
                mem_tags = {'name': mementry['name'], 'resource-metric': 'true'}
                self.write_double_metric('memory-usage', mem_tags, 'gauge', float(mementry['value']), True, "Memory Usage", "percentage", "name")

        return self.output
