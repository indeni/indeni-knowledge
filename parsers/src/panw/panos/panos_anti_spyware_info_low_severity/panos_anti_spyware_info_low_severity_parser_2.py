from parser_service.public.base_parser import BaseParser
from parser_service.public.helper_methods import *
from parser_service.public import helper_methods


class LowSevParser2(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data = helper_methods.parse_data_as_xml(raw_data)
            if data and 'result' in data['response'] and data['response']['result']:
                if data['response']['result']['entry'].get('rules'):
                    entries = data['response']['result']['entry']['rules']['entry']

                    if entries:
                        if type(entries) != list:
                            entries = [entries]
                        for entry in entries:
                            tags = {}
                            spyware_severities = to_list(entry['severity'] and entry['severity']['member'])
                            for spyware_severity in spyware_severities:
                                actions = entry['action'].keys()
                                tags['name'] = '{}: {}'.format(dynamic_var['profile'], spyware_severity)

                                if spyware_severity in ['informational', 'low'] and ('default' in actions or 'reset-both' in actions):
                                    value = 1.0
                                elif spyware_severity == 'any' and 'reset-both' in actions:
                                    value = 1.0
                                elif spyware_severity in ['medium', 'high', 'critical']:
                                    value = 1.0
                                else:
                                    value = 0.0
                                self.write_double_metric('spyware-info-low-severity', tags, 'gauge', value, True, 'Anti-Spyware info and low severities', 'state', 'name')
        return self.output
