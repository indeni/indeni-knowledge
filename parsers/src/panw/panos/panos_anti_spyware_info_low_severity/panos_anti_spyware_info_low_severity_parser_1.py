from parser_service.public.base_parser import BaseParser
from parser_service.public import helper_methods

class LowSevParser1(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data = helper_methods.parse_data_as_xml(raw_data)
            if data and 'result' in data['response'] and data['response']['result']:
                if data['response']['result'].get('spyware'):
                    entries = [data['response']['result']['spyware']['entry']]
                    for entry in entries:
                        if type(entry) != list:
                            entry = [entry]
                        for item in entry:
                            profile = item['@name']
                            self.write_dynamic_variable('profile', profile)
        return self.output
