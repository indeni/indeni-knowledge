import os
import unittest

from panw.panos.panos_anti_spyware_info_low_severity.panos_anti_spyware_info_low_severity_parser_2 import LowSevParser2
from parser_service.public.action import WriteDoubleMetric


class TestLowSevParser2(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = LowSevParser2()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_valid_input_2_profiles_2nd_step(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/valid_input_profile1_2nd_step.xml', {'profile': 'Profile1'}, {})

        # Assert
        self.assertEqual(5, len(result))

        self.assertTrue(isinstance(result[0], WriteDoubleMetric))
        self.assertEqual('gauge', result[0].ds_type)
        self.assertEqual('spyware-info-low-severity', result[0].name)
        self.assertEqual('Profile1: informational', result[0].tags['name'])
        self.assertEqual('Profile1: low', result[1].tags['name'])
        self.assertEqual('Profile1: medium', result[2].tags['name'])
        self.assertEqual('Profile1: high', result[3].tags['name'])
        self.assertEqual('Profile1: critical', result[4].tags['name'])


    def test_valid_input_empty_result(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/valid_input_empty_result.xml', {}, {})

        # Assert
        self.assertEqual(0, len(result))

    def test_invalid_input(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/invalid_input.xml', {}, {})
        # Assert
        self.assertEqual(0, len(result))


if __name__ == '__main__':
    unittest.main()
