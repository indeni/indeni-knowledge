import os
import unittest

from panw.panos.panos_anti_spyware_info_low_severity.panos_anti_spyware_info_low_severity_parser_1 import LowSevParser1
from parser_service.public.action import WriteDynamicVariable

class TestLowSevParser1(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = LowSevParser1()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_valid_input_2_profiles_1st_step(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/valid_input_2_profiles_1st_step.xml', {}, {})

        # Assert
        self.assertEqual(2, len(result))

        self.assertTrue(isinstance(result[0], WriteDynamicVariable))
        self.assertEqual('profile', result[0].key)
        self.assertEqual('Profile1', result[0].value)

        self.assertTrue(isinstance(result[1], WriteDynamicVariable))
        self.assertEqual('profile', result[1].key)
        self.assertEqual('Profile2', result[1].value)

    def test_valid_input_empty_result(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/valid_input_empty_result.xml', {}, {})

        # Assert
        self.assertEqual(0, len(result))

    def test_invalid_input(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/invalid_input.xml', {}, {})
        # Assert
        self.assertEqual(0, len(result))

    def test_valid_input_1_profile_1st_step(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/valid_input_1_profile_1st_step.xml', {}, {})
        # Assert
        self.assertEqual(1, len(result))
        self.assertTrue(isinstance(result[0], WriteDynamicVariable))
        self.assertEqual('profile', result[0].key)
        self.assertEqual('Profile1', result[0].value)

if __name__ == '__main__':
    unittest.main()
