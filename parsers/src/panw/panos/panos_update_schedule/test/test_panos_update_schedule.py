import os
import unittest

from panw.panos.panos_update_schedule.panos_update_schedule import UpdateSchedule
from parser_service.public.action import WriteComplexMetric


class TestAvUpdateSchedule(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = UpdateSchedule()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_valid_input_update_schedule_multiple(self):
        result = self.parser.parse_file(self.current_dir + '/valid_input_update_schedule_multiple.xml', {}, {})
        self.assertEqual(5, len(result))
        self.assertTrue(isinstance(result[0], WriteComplexMetric))
        self.assertEqual('Antivirus Update Schedule', result[0].tags['name'])
        self.assertEqual([{'Action': 'download-only', 'Schedule': 'daily', 'Threshold': '2 Hours'}], result[0].value)
        self.assertTrue(isinstance(result[1], WriteComplexMetric))
        self.assertEqual('Application and Threat Update Schedule', result[1].tags['name'])
        self.assertEqual([{'Action': 'download-only', 'Schedule': 'weekly'}], result[1].value)
        self.assertTrue(isinstance(result[2], WriteComplexMetric))
        self.assertEqual('GlobalProtect Clientless VPN Update Schedule', result[2].tags['name'])
        self.assertEqual([{'Action': None, 'Schedule': 'weekly'}], result[2].value)
        self.assertTrue(isinstance(result[3], WriteComplexMetric))
        self.assertEqual('GlobalProtect Data File Update Schedule', result[3].tags['name'])
        self.assertEqual([{'Action': 'download-and-install', 'Schedule': 'daily'}], result[3].value)
        self.assertTrue(isinstance(result[4], WriteComplexMetric))
        self.assertEqual('Wildfire Update Schedule', result[4].tags['name'])
        self.assertEqual([{'Action': 'download-only', 'Schedule': 'every-30-mins'}], result[4].value)

    def test_valid_input_single(self):
        expected_results = {
             'valid_input_av_update_daily_schedule.xml': {'length': 1, 'name': 'Antivirus Update Schedule', 'value': [{'Action': 'download-and-install', 'Schedule': 'daily'}]},
             'valid_input_av_update_download_only.xml': {'length': 1, 'name': 'Antivirus Update Schedule', 'value': [{'Action': 'download-only', 'Schedule': 'hourly'}]},
             'valid_input_av_update_download_only_threshold.xml': {'length': 1, 'name': 'Antivirus Update Schedule', 'value': [{'Action': 'download-only', 'Schedule': 'hourly', 'Threshold': '2 Hours'}]},
             'valid_input_av_update_weekly_schedule.xml': {'length': 1, 'name': 'Antivirus Update Schedule', 'value': [{'Action': 'download-and-install', 'Schedule': 'weekly'}]},
             'valid_input_av_update_passed_case.xml': {'length': 1, 'name': 'Antivirus Update Schedule', 'value': [{'Action': 'download-and-install', 'Schedule': 'hourly'}]},
             'valid_input_content_update_threats_download.xml': {'length': 1, 'name': 'Application and Threat Update Schedule', 'value': [{'Action': 'download-only', 'Schedule': 'every-30-mins', 'Threshold': '2 Hours'}]},
             'valid_input_content_update_threats_no_threshold.xml': {'length': 1, 'name': 'Application and Threat Update Schedule', 'value': [{'Action': 'download-and-install', 'Schedule': 'every-30-mins'}]},
             'valid_input_content_update_threats_passed.xml': {'length': 1, 'name': 'Application and Threat Update Schedule', 'value': [{'Action': 'download-and-install', 'Schedule': 'every-30-mins', 'Threshold': '2 Hours'}]},
             'valid_input_content_update_threats_threshold.xml': {'length': 1, 'name': 'Application and Threat Update Schedule', 'value': [{'Action': 'download-and-install', 'Schedule': 'every-30-mins', 'Threshold': '49 Hours'}]},
             'valid_input_gp_update_daily_schedule.xml': {'length': 1, 'name': 'GlobalProtect Data File Update Schedule', 'value': [{'Action': 'download-and-install', 'Schedule': 'daily'}]},
             'valid_input_gp_update_download_action.xml': {'length': 1, 'name': 'GlobalProtect Data File Update Schedule', 'value': [{'Action': 'download-only', 'Schedule': 'hourly'}]},
             'valid_input_gp_update_schedule_passed.xml': {'length': 1, 'name': 'GlobalProtect Data File Update Schedule', 'value': [{'Action': 'download-and-install', 'Schedule': 'hourly'}]},
             'valid_input_gp_update_weekly_schedule.xml': {'length': 1, 'name': 'GlobalProtect Data File Update Schedule', 'value': [{'Action': 'download-and-install', 'Schedule': 'weekly'}]},
             'valid_input_gpclientless_action.xml': {'length': 1, 'name': 'GlobalProtect Clientless VPN Update Schedule', 'value': [{'Action': 'download-and-install', 'Schedule': 'daily'}]},
             'valid_input_gpclientless_passed.xml': {'length': 1, 'name': 'GlobalProtect Clientless VPN Update Schedule', 'value': [{'Action': 'download-and-install', 'Schedule': 'hourly'}]},
             'valid_input_gpclientless_schedule.xml': {'length': 1, 'name': 'GlobalProtect Clientless VPN Update Schedule', 'value': [{'Action': 'download-and-install', 'Schedule': 'daily'}]},
             'valid_input_wildfire_passed.xml': {'length': 1, 'name': 'Wildfire Update Schedule', 'value': [{'Action': 'download-and-install', 'Schedule': 'every-min'}]},
             'valid_input_wildfire_action.xml': {'length': 1, 'name': 'Wildfire Update Schedule', 'value': [{'Action': 'download-only', 'Schedule': 'every-min'}]},
             'valid_input_wildfire_schedule.xml': {'length': 1, 'name': 'Wildfire Update Schedule', 'value': [{'Action': 'download-and-install', 'Schedule': 'every-15-mins'}]}
        }
        for k, v in expected_results.items():
            self.parser = UpdateSchedule()
            result = self.parser.parse_file(self.current_dir + '/'+k, {}, {})
            self.assertEqual(v['length'], len(result))
            self.assertTrue(isinstance(result[0], WriteComplexMetric))
            self.assertEqual(v['name'], result[0].tags['name'])
            self.assertEqual(v['value'], result[0].value)


    def test_invalid_input_empty(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/invalid_input_empty.xml', {}, {})
        # Assert
        self.assertEqual(0, len(result))

    def test_invalid_input_empty_reponse(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/invalid_input_empty_reponse.xml', {}, {})
        # Assert
        self.assertEqual(0, len(result))

    def test_invalid_input_empty_reponse_sync(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/invalid_input_empty_response_sync.xml', {}, {})
        # Assert
        self.assertEqual(0, len(result))

    def test_invalid_input_partial_reponse(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/invalid_input_partial_reponse.xml', {}, {})
        # Assert
        self.assertEqual(0, len(result))

if __name__ == '__main__':
    unittest.main()
