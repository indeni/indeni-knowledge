from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class UpdateSchedule(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        update_items = {'anti-virus': {'name': 'Antivirus Update Schedule', 'metric': 'av-update-schedule'},
                        'threats': {'name': 'Application and Threat Update Schedule', 'metric': 'content-update-schedule'},
                        'global-protect-clientless-vpn': {'name': 'GlobalProtect Clientless VPN Update Schedule', 'metric': 'panos-gpclientless-update-schedule'},
                        'global-protect-datafile': {'name': 'GlobalProtect Data File Update Schedule', 'metric': 'panos-gp-update-schedule'},
                        'wildfire': {'name': 'Wildfire Update Schedule', 'metric': 'panos-wildfire-update-schedule'}}

        if raw_data:  # We check if input is not empty
            xml_data = helper_methods.parse_data_as_xml(raw_data)
            if xml_data and xml_data['response']['@status'] == 'success':
                if xml_data['response']['result']:
                    if xml_data['response']['result'].get('update-schedule'):
                        for k, v in update_items.items():
                            if k in xml_data['response']['result']['update-schedule']:
                                if 'recurring' in xml_data['response']['result']['update-schedule'][k]:
                                    recurring = xml_data['response']['result']['update-schedule'][k]['recurring']
                                    possible_schedules = {'daily', 'hourly', 'weekly', 'every-min', 'every-15-mins', 'every-30-mins', 'every-hour'}
                                    for elem in recurring.keys():
                                        if elem in possible_schedules:
                                            action = None
                                            schedule = elem
                                            schedule_xml = recurring[schedule]
                                            if 'action' in schedule_xml:
                                                if '#text' in schedule_xml['action']:
                                                    action = schedule_xml['action']['#text']
                                                else:
                                                    action = schedule_xml['action']

                                            if 'threshold' in recurring:
                                                if '#text' in recurring['threshold']:
                                                    threshold = recurring['threshold']['#text']
                                                else:
                                                    threshold = recurring['threshold']
                                                output = [{'Action': action, 'Schedule': schedule, 'Threshold': threshold + ' Hours'}]
                                            else:
                                                output = [{'Action': action, 'Schedule': schedule}]

                                            tags = {'name': v['name'], 'im.identity-tags': 'name', 'im.dstype.displaytype': 'string'}
                                            self.write_complex_metric_object_array(v['metric'], tags, output, True, 'Update Schedules')
        return self.output
