from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

from collections import defaultdict, OrderedDict
import json
from xml.etree import cElementTree as ET
import re

class ShowConfigPushedTemplate(BaseParser):
    def dict_path(self, my_dict, path=None):
        if path is None:
            path = []
        if isinstance(my_dict, str):
            newpath = path + [my_dict]
            pass
        for k,v in my_dict.items():
            if 'entry' in k:
                if isinstance(v, list):
                    for att in v:
                        newpath = path + [att['@name']]
                        for element in self.dict_path(att, newpath):
                            yield element
                elif isinstance(v, dict):
                    newpath = path + [v['@name']]
                    for u in self.dict_path(v, newpath):
                        yield u
                else:
                    yield newpath, v
            elif '@' not in k:
                newpath = path + [k]
                if isinstance(v, dict):
                    for u in self.dict_path(v, newpath):
                        yield u
                else:
                    yield newpath, v
            else:
                pass

    def etree_to_dict(self, t):
        d = {t.tag: {} if t.attrib else None}
        children = list(t)
        if children:
            dd = defaultdict(list)
            for dc in map(self.etree_to_dict, children):
                for k, v in dc.items():
                    dd[k].append(v)
            d = {t.tag: {k:v[0] if len(v) == 1 else v for k, v in dd.items()}}
        if t.attrib:
            d[t.tag].update(('@' + k, v) for k, v in t.attrib.items())
        if t.text:
            text = t.text.strip()
            if children or t.attrib:
                if text:
                    d[t.tag] = text
            else:
                d[t.tag] = text
        return d

    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            xml_data = helper_methods.parse_data_as_xml(raw_data)['response']['@status']
            if xml_data  == 'success':
                current_line = {}
                config = []
                users = []
                for path, value in self.dict_path(self.etree_to_dict(ET.XML(raw_data))):
                    current_line = {}
                    if isinstance(value,list):
                        for n,i in enumerate (value):
                            if isinstance(value[n],dict):
                                value[n] = value[n]['#text']
                    new_line = '{} {}'.format( ' '.join(path),value).replace('response result template config', 'set')

                    current_line['line'] = new_line
                    if len(current_line['line']) < 500:
                        config.append(current_line)
                        m = re.search('set mgt-config users (\S+?) permissions role-based (\S+?) ', current_line['line'])
                        if m:
                            user = {}
                            user['username'] = m.group(1)
                            user_role = m.group(2)
                            if user_role not in ['admin_authentication', 'devicereader', 'superreader']:
                                users.append(user)

                self.write_complex_metric_object_array('configuration-content', {}, config, False)
                self.write_complex_metric_object_array('users', {}, users, False)

        return self.output
