import os
import unittest
from pathlib import Path

from panw.panos.show_config_pushed_template.show_config_pushed_template import ShowConfigPushedTemplate
from parser_service.public.action import WriteComplexMetric

class TestShowConfigPushedTemplate(unittest.TestCase):
    def setUp(self):
        self.parser = ShowConfigPushedTemplate()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_show_config_pushed_template_not_returned(self):
        result = self.parser.parse_file(self.current_dir + '/show_config_pushed_template_not_returned.input', {}, {})
        self.assertIsInstance(result, list)
        self.assertEqual(0, len(result))

    def test_show_config_pushed_template_basic(self):
        expected_output = """response result template name template_stack_test
        set devices localhost.localdomain deviceconfig system ntp-servers primary-ntp-server ntp-server-address new.dummy.org
        set devices localhost.localdomain deviceconfig setting management admin-lockout failed-attempts 6
        set devices localhost.localdomain deviceconfig setting management admin-lockout lockout-time 60
        set devices localhost.localdomain deviceconfig setting management idle-timeout 10
        set mgt-config password-complexity minimum-length 8
        set mgt-config password-complexity minimum-uppercase-letters 1
        set mgt-config password-complexity minimum-lowercase-letters 1
        set mgt-config password-complexity minimum-numeric-letters 1
        set mgt-config password-complexity minimum-special-characters 1
        set mgt-config password-complexity block-username-inclusion yes
        set shared log-settings syslog syslog_profile_test server syslog_server transport UDP
        set shared log-settings syslog syslog_profile_test server syslog_server port 514
        set shared log-settings syslog syslog_profile_test server syslog_server format BSD
        set shared log-settings syslog syslog_profile_test server syslog_server server 10.11.95.101
        set shared log-settings syslog syslog_profile_test server syslog_server facility LOG_USER
        set shared log-settings system match-list log_settings_ send-syslog member syslog_profile_test
        set shared log-settings system match-list log_settings_ filter All Logs
        set shared log-settings system match-list log_settings_ send-to-panorama yes
        response result template ts-ctxt 16
        response result template variable $dummy2 type fqdn new.dummy.org"""
        result = self.parser.parse_file(self.current_dir + '/show_config_pushed_template_basic.input', {}, {})
        self.assertEqual(21, len(result[0].value))
        for line in result[0].value:
            self.assertIn(line['line'], expected_output)

    def test_show_config_pushed_template_with_users(self):
        result = self.parser.parse_file(self.current_dir + '/show_config_pushed_template_with_users.input', {}, {})
        self.assertEqual(7251, len(result[0].value))
        self.assertEqual(2, len(result[1].value))
        for user in result[1].value:
            self.assertIn(user['username'],['templated_user_superuser','templated_user_superuser_ro'])

if __name__ == '__main__':
    unittest.main()