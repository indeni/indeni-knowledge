import os
import unittest
from panw.panos.panos_show_chassis_status.panos_show_chassis_status import PanosShowChassisStatus
from parser_service.public.action import *

class TestPanosShowChassisStatus(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = PanosShowChassisStatus()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_show_chassis_status(self):
        result = self.parser.parse_file(self.current_dir + '/panos_show_chassis_status.input', {}, {})
        self.assertEqual(8, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'line-card-status')
        self.assertEqual(result[0].tags['slot.id'], '1')
        self.assertEqual(result[0].value, 1)
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].name, 'line-card-status')
        self.assertEqual(result[1].tags['slot.id'], '2')
        self.assertEqual(result[1].value, 0)
        self.assertEqual(result[2].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[2].name, 'line-card-status')
        self.assertEqual(result[2].tags['slot.id'], '3')
        self.assertEqual(result[2].value, 1)
        self.assertEqual(result[3].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[3].name, 'line-card-status')
        self.assertEqual(result[3].tags['slot.id'], '4')
        self.assertEqual(result[3].value, 1)
        self.assertEqual(result[4].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[4].name, 'line-card-status')
        self.assertEqual(result[4].tags['slot.id'], '5')
        self.assertEqual(result[4].value, 1)
        self.assertEqual(result[5].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[5].name, 'line-card-status')
        self.assertEqual(result[5].tags['slot.id'], '6')
        self.assertEqual(result[5].value, 1)
        self.assertEqual(result[6].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[6].name, 'line-card-status')
        self.assertEqual(result[6].tags['slot.id'], '7')
        self.assertEqual(result[6].value, 1)
        self.assertEqual(result[7].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[7].name, 'line-card-status')
        self.assertEqual(result[7].tags['slot.id'], '8')
        self.assertEqual(result[7].value, 1)


if __name__ == '__main__':
    unittest.main()