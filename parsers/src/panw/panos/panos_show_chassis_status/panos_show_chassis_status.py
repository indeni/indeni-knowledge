from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class PanosShowChassisStatus(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data = helper_methods.parse_data_as_list(raw_data, 'panos_show_chassis_status.textfsm')
            if data:
                for parsed_data in data:
                    tags = {}
                    tags['slot.id'] = parsed_data['slot_id']
                    self.write_double_metric('line-card-status', tags, 'gauge', 1 if parsed_data['status'] == 'Up' or parsed_data['status'] == '' else 0, True, 'Chassis Line Card - Status', 'state')
        return self.output