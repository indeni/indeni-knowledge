from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class ShowSystemResourcesParser(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        data = helper_methods.parse_data_as_object(raw_data, 'show_system_resources.textfsm')
        if data:
            cputags = {'cpu-id': 'MP', 'cpu-is-avg': 'false', 'resource-metric': 'true', 'dp': 'false'}
            self.write_double_metric('cpu-usage', cputags, 'gauge', 100 - float(data['cpuidle']), True, 'CPU Usage', 'percentage', 'cpu-id')

            mem_tags = {'name': 'PA firewall management plane'}
            memused = int(data['memused']) - int(data['membuffers'])  # In linux we shouldn't count "buffers" as used
            self.write_double_metric('memory-free-kbytes', mem_tags, "gauge", int(data['memtotal']) - memused, True, 'Memory - Free', 'kilobytes', 'name')
            self.write_double_metric('memory-total-kbytes', mem_tags, "gauge", int(data['memtotal']), True, 'Memory - Total', 'kilobytes', 'name')

            # Update: memory-usage = (used - (buffers+cached))
            # https://live.paloaltonetworks.com/t5/Management-Articles/SNMP-Poll-Reports-Different-Memory-Usage-than-quot-show-system/ta-p/53928
            mem_tags = {'name': 'PA firewall management plane', 'resource-metric': 'true'}
            self.write_double_metric('memory-usage', mem_tags, "gauge", (memused - int(data['memcached'])) / int(data['memtotal']) * 100, True, 'Memory Usage', 'percentage', 'name')

        return self.output
