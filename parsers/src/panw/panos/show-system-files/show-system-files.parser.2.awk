BEGIN {
    hour = 0
    minute = 0
    second = 0

    monthNum["Jan"] = 1
    monthNum["Feb"] = 2
    monthNum["Mar"] = 3
    monthNum["Apr"] = 4
    monthNum["May"] = 5
    monthNum["Jun"] = 6
    monthNum["Jul"] = 7
    monthNum["Aug"] = 8
    monthNum["Sep"] = 9
    monthNum["Oct"] = 10
    monthNum["Nov"] = 11
    monthNum["Dec"] = 12

    # Jan 20 2019
    split(dynamic("dyn_var_date"),d," ")
    this_month = monthNum[d[1]]
    this_day   = d[2]
    this_year  = d[3]
}

#/var/cores/:
/\/.*\:$/ {
    path = $1
    sub(/:/, "", path)
    if (path !~ /\/$/) {
        path = path "/"
    }
}

#-rw-rw-rw- 1 root root 3.1K Jun  4  2015 dha_6.1.2_0.info
#drwxr-xr-x 2 root root 4.0K Dec 27 20:09 crashinfo
#-rw-rw-rw- 1 root root 42K Oct 18 19:46 mgmtsrvr_8.0.13_0.info

#/var/cores/:
#total 4.0K
#drwxr-xr-x 2 root root 4.0K Feb 12  2017 crashinfo

#IMPORTANT: We ignore directories (no "d" in the regex below)
/^[rwx\-]+/ && path == "/var/cores/" && ! /cimem$|core$|inuse$|info$/ {
    if (path ~ /core/) {
        ifile++
        month = $(NF-3)
        day = $(NF-2)
        # Timestamps older than 1yr will have the year just before the file name.
        # Like:
        # drwxr-xr-x 2 root root 4.0K Feb 12  2017 crashinfo
        year = $(NF-1)
        if (year !~ /\d\d\d\d/) {
            year = this_year
            if (monthNum[month] > this_month)
                year = year - 1
            else 
                if ((monthNum[month] == this_month) && (day > this_day))
                    year = year - 1
        }

        if ($(NF-1) ~ /:/) {
            hour_minute = $(NF-1)
            split(hour_minute, time_array, /:/)
            hour = time_array[1]
            minute = time_array[2]
        }

        files[ifile, "path"] = path $NF
        files[ifile, "created"] = datetime(year, parseMonthThreeLetter(month), day, hour, minute, second)
    }
}
END {
    # Write complex metric
    writeComplexMetricObjectArray("core-dumps", null, files, "false")  # Converted to new syntax by change_ind_scripts.py script
}
