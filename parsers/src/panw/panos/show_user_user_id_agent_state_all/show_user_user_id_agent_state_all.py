from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser
import re


class ShowUserUserIdAgentState(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        
        # Step 0 : Initial declarations
        tags = {}
        panorama_tags = {}
        panorama_state = 0
        panorama_device_count = 0
        duplicate_data_to_delete = re.compile(r'\([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\)|\(\)')

        # Step 1 : Data Extraction
        user_is_agent_list = helper_methods.parse_data_as_list(raw_data, 'paloalto_panos_show_user_userid_agent_state.textfsm')
        
        # Step 2 : Data Processing
        for agent_entry in user_is_agent_list:
            agent_entry['data'] = re.sub(duplicate_data_to_delete, '', agent_entry['data'])
            if re.search("^conn", agent_entry['status']):
                state = 1
            else:
                state = 0
            tags["name"] = agent_entry['data']
            if re.search("[Pp]anorama", agent_entry['data']):
                panorama_device_count += 1
                if panorama_device_count == 2:
                    self.write_double_metric('userid_agent_state_panorama', panorama_tags, 'gauge', panorama_state, True, 'User-ID Agents', 'state', 'name')
                    self.write_double_metric('userid_agent_state_panorama', tags, 'gauge', state, True, 'User-ID Agents', 'state', 'name')
                    panorama_state += state
                    panorama_tags["name"] = "Panorama HA"
                    self.write_double_metric('userid_agent_state', panorama_tags, 'gauge', 1 if (panorama_state != 0) else 0, False)
                else:
                    panorama_state += state
                    panorama_tags["name"] = tags["name"]
                
            else:
                self.write_double_metric('userid_agent_state', tags, 'gauge', state, True, 'User-ID Agents', 'state', 'name')
        if panorama_device_count == 1:
            self.write_double_metric('userid_agent_state', panorama_tags, 'gauge', panorama_state, False)
        
        # Step 3 : Data Reporting
        return self.output
