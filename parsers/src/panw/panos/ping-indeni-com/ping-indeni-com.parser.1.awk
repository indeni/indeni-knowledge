BEGIN {
	dns_success=0
}

# Let's see if it managed to translate the domain to IP:
# PING indeni.com (109.199.106.156) 56(84) bytes of data.
/PING .*indeni.com \([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\)/ {
	dns_success=1
}

END {
	dnstags["name"] = "Currently configured DNS server(s)"
	writeDoubleMetric("dns-server-state", dnstags, "gauge", dns_success, "true", "DNS Servers - State", "state", "name")  # Converted to new syntax by change_ind_scripts.py script
}