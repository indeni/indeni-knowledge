#Number of sessions that match filter: 0
/Number of sessions/ {
    value = $NF
    writeDoubleMetric("concurrent-ssl-decryption-current", null, "gauge", value, "true", "Current Concurrent SSL Decryption Connections", "number", "name")  # Converted to new syntax by change_ind_scripts.py script
}