BEGIN {
    unsaved = 0
}

#If there's a "@@" or added ("+") or removed ("-") line, then there's a difference.
/^(@@|\+|\-)/ {
    unsaved = 1
}

END {
    writeDoubleMetric("config-unsaved", null, "gauge", unsaved, "true", "Configuration Unsaved?", "boolean", "")  # Converted to new syntax by change_ind_scripts.py script
}