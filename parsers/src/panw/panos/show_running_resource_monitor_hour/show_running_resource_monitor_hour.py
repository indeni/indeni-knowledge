from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class ShowRunningResourceMonitorHourParser(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data = helper_methods.parse_data_as_xml(raw_data)
            if data:
                data_processors = data['response']['result']['resource-monitor']['data-processors']

                if data_processors:
                    for data_processor in data_processors:
                        tags = {'name':data_processor}
                        resource_utilization = data_processors[data_processor]['hour']['resource-utilization']['entry']
                        if resource_utilization:
                            for item in resource_utilization:
                                if item['name'] == 'packet buffer (average)':
                                    self.write_double_metric('packet-buffer-average', tags, 'gauge', float(item['value']), True, 'Packet Buffer (average)', 'percentage', 'name')

                                if item['name'] == 'packet buffer (maximum)':
                                    self.write_double_metric('packet-buffer-maximum', tags, 'gauge', float(item['value']), True, 'Packet Buffer (maximum)', 'percentage', 'name')

                                if item['name'] == 'packet descriptor (on-chip) (average)':
                                    self.write_double_metric('packet-descriptor-on-chip-average', tags, 'gauge',  float(item['value']), True, 'Packet Descriptor (on-chip) (average)', 'percentage', 'name')

                                if item['name'] == 'packet descriptor (on-chip) (maximum)':
                                    self.write_double_metric('packet-descriptor-on-chip-maximum', tags, 'gauge',  float(item['value']), True, 'Packet Descriptor (on-chip) (maximum)', 'percentage', 'name')
        return self.output
