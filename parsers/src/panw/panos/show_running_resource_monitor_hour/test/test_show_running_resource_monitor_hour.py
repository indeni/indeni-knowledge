import os
import unittest

from panw.panos.show_running_resource_monitor_hour.show_running_resource_monitor_hour import ShowRunningResourceMonitorHourParser
from parser_service.public.action import *

class TestShowRunningResourceMonitorHourParser(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = ShowRunningResourceMonitorHourParser()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_empty_input(self):
        expected_results = []

        # Act
        result = self.parser.parse_file(self.current_dir + '/empty.input', {}, {})
        # Assert
        self.assertEqual(0, len(result))

    def test_multi_dp(self):
        expected_results = [
                            {'name': 's5dp1', 'value': 0.0, 'metric': 'packet-buffer-average'},
                            {'name': 's5dp1', 'value': 1.0, 'metric': 'packet-buffer-maximum'},
                            {'name': 's5dp1', 'value': 4.0, 'metric': 'packet-descriptor-on-chip-average'},
                            {'name': 's5dp1', 'value': 51.0, 'metric': 'packet-descriptor-on-chip-maximum'},
                            {'name': 's5dp0', 'value': 0.0, 'metric': 'packet-buffer-average'},
                            {'name': 's5dp0', 'value': 1.0, 'metric': 'packet-buffer-maximum'},
                            {'name': 's5dp0', 'value': 4.0, 'metric': 'packet-descriptor-on-chip-average'},
                            {'name': 's5dp0', 'value': 34.0, 'metric': 'packet-descriptor-on-chip-maximum'},
                            {'name': 's2dp0', 'value': 0.0, 'metric': 'packet-buffer-average'},
                            {'name': 's2dp0', 'value': 3.0, 'metric': 'packet-buffer-maximum'},
                            {'name': 's2dp0', 'value': 4.0, 'metric': 'packet-descriptor-on-chip-average'},
                            {'name': 's2dp0', 'value': 94.0, 'metric': 'packet-descriptor-on-chip-maximum'},
                            {'name': 's2dp1', 'value': 0.0, 'metric': 'packet-buffer-average'},
                            {'name': 's2dp1', 'value': 0.0, 'metric': 'packet-buffer-maximum'},
                            {'name': 's2dp1', 'value': 4.0, 'metric': 'packet-descriptor-on-chip-average'},
                            {'name': 's2dp1', 'value': 10.0, 'metric': 'packet-descriptor-on-chip-maximum'},
                            {'name': 's3dp1', 'value': 0.0, 'metric': 'packet-buffer-average'},
                            {'name': 's3dp1', 'value': 2.0, 'metric': 'packet-buffer-maximum'},
                            {'name': 's3dp1', 'value': 4.0, 'metric': 'packet-descriptor-on-chip-average'},
                            {'name': 's3dp1', 'value': 67.0, 'metric': 'packet-descriptor-on-chip-maximum'},
                            {'name': 's3dp0', 'value': 0.0, 'metric': 'packet-buffer-average'},
                            {'name': 's3dp0', 'value': 2.0, 'metric': 'packet-buffer-maximum'},
                            {'name': 's3dp0', 'value': 4.0, 'metric': 'packet-descriptor-on-chip-average'},
                            {'name': 's3dp0', 'value': 55.0, 'metric': 'packet-descriptor-on-chip-maximum'},
                            {'name': 's6dp0', 'value': 0.0, 'metric': 'packet-buffer-average'},
                            {'name': 's6dp0', 'value': 2.0, 'metric': 'packet-buffer-maximum'},
                            {'name': 's6dp0', 'value': 4.0, 'metric': 'packet-descriptor-on-chip-average'},
                            {'name': 's6dp0', 'value': 59.0, 'metric': 'packet-descriptor-on-chip-maximum'},
                            {'name': 's6dp1', 'value': 0.0, 'metric': 'packet-buffer-average'},
                            {'name': 's6dp1', 'value': 0.0, 'metric': 'packet-buffer-maximum'},
                            {'name': 's6dp1', 'value': 3.0, 'metric': 'packet-descriptor-on-chip-average'},
                            {'name': 's6dp1', 'value': 13.0, 'metric': 'packet-descriptor-on-chip-maximum'},
                            {'name': 's1dp1', 'value': 0.0, 'metric': 'packet-buffer-average'},
                            {'name': 's1dp1', 'value': 1.0, 'metric': 'packet-buffer-maximum'},
                            {'name': 's1dp1', 'value': 4.0, 'metric': 'packet-descriptor-on-chip-average'},
                            {'name': 's1dp1', 'value': 53.0, 'metric': 'packet-descriptor-on-chip-maximum'},
                            {'name': 's1dp0', 'value': 0.0, 'metric': 'packet-buffer-average'},
                            {'name': 's1dp0', 'value': 0.0, 'metric': 'packet-buffer-maximum'},
                            {'name': 's1dp0', 'value': 3.0, 'metric': 'packet-descriptor-on-chip-average'},
                            {'name': 's1dp0', 'value': 16.0, 'metric': 'packet-descriptor-on-chip-maximum'},
                            {'name': 's7dp1', 'value': 0.0, 'metric': 'packet-buffer-average'},
                            {'name': 's7dp1', 'value': 1.0, 'metric': 'packet-buffer-maximum'},
                            {'name': 's7dp1', 'value': 4.0, 'metric': 'packet-descriptor-on-chip-average'},
                            {'name': 's7dp1', 'value': 39.0, 'metric': 'packet-descriptor-on-chip-maximum'},
                            {'name': 's7dp0', 'value': 0.0, 'metric': 'packet-buffer-average'},
                            {'name': 's7dp0', 'value': 1.0, 'metric': 'packet-buffer-maximum'},
                            {'name': 's7dp0', 'value': 4.0, 'metric': 'packet-descriptor-on-chip-average'},
                            {'name': 's7dp0', 'value': 50.0, 'metric': 'packet-descriptor-on-chip-maximum'}]

        # Act
        result = self.parser.parse_file(self.current_dir + '/test_multi_dp.input', {}, {})
        # Assert
        self.assertEqual(48, len(result))

        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i]['name'], result[i].tags['name'])
            self.assertEqual(expected_results[i]['metric'], result[i].name)
            self.assertEqual(expected_results[i]['value'], result[i].value)


    def test_single_dp(self):
        expected_results = [{'name': 's1dp0', 'metric': 'packet-buffer-average', 'value': 0.0},
                            {'name': 's1dp0', 'metric': 'packet-buffer-maximum', 'value': 0.0},
                            {'name': 's1dp0', 'metric': 'packet-descriptor-on-chip-average', 'value': 2.0},
                            {'name': 's1dp0', 'metric': 'packet-descriptor-on-chip-maximum', 'value': 7.0}]

        # Act
        result = self.parser.parse_file(self.current_dir + '/test_single_dp.input', {}, {})
        # Assert
        self.assertEqual(4, len(result))

        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i]['name'], result[i].tags['name'])
            self.assertEqual(expected_results[i]['metric'], result[i].name)
            self.assertEqual(expected_results[i]['value'], result[i].value)


if __name__ == '__main__':
    unittest.main()