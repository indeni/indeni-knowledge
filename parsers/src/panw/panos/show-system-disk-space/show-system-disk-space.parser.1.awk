function sizeToK(size) {
    if (size ~ /T/) {
    	sub(/T/, "", size)
    	size = size * 1024 * 1024 * 1024
    } else if (size ~ /G/) {
    	sub(/G/, "", size)
    	size = size * 1024 * 1024
    } else if (size ~ /M/) {
    	sub(/M/, "", size)
    	size = size * 1024
    } else if (size ~ /K/) {
    	sub(/K/, "", size)
    	size = size
    }
    return size
}

BEGIN {
}

# Filesystem            Size  Used Avail Use% Mounted on
# /dev/sda3             3.8G  2.1G  1.6G  58% /
/(\d+)%/ {
    mount = $NF
    usage = $(NF-1)
    sub(/%/, "", usage)
    available = sizeToK($(NF-2))
    used = sizeToK($(NF-3))

    mounttags["file-system"] = mount
    writeDoubleMetric("disk-used-kbytes", mounttags, "gauge", used, "true", "Mount Points - Used Space", "kilobytes", "file-system")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("disk-total-kbytes", mounttags, "gauge", used+available, "true", "Mount Points - Total Space", "kilobytes", "file-system")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("disk-usage-percentage", mounttags, "gauge", usage, "true", "Mount Points", "percentage", "file-system")  # Converted to new syntax by change_ind_scripts.py script
}
