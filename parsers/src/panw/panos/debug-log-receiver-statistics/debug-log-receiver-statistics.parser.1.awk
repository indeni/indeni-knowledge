#Logs discarded (queue full):   0
/Logs discarded/ {
    writeDoubleMetric("logs-discarded-queue-full", null, "counter", $NF, "false")  # Converted to new syntax by change_ind_scripts.py script
}

#Log Forward discarded (queue full) count: 0
/Log Forward discarded.*queue full/ {
    writeDoubleMetric("log-forward-discarded-queue-full", null, "counter", $NF, "false")  # Converted to new syntax by change_ind_scripts.py script
}

#Log Forward discarded (send error) count: 0
/Log Forward discarded.*send error/ {
    writeDoubleMetric("log-forward-discarded-send-error", null, "counter", $NF, "false")  # Converted to new syntax by change_ind_scripts.py script
}