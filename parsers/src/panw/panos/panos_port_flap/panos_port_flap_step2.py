from parser_service.public.base_parser import BaseParser
from parser_service.public import helper_methods

class PanosShowLogSystemJobId(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data = helper_methods.parse_data_as_xml(raw_data)
            if data and data['response']['@status'] == "success":
                if 'result' in data['response'] and data['response']['result']:
                    self.write_dynamic_variable('int_name_list', dynamic_var['int_name_list'])
                    self.write_dynamic_variable('log_query_job_id', data['response']['result'].get('job'))
            elif data and data['response']['@status'] != "success":
                self.write_dynamic_variable('int_name_list','no-value-due-https-error')
                self.write_dynamic_variable('log_query_job_id', '0')
        return self.output
