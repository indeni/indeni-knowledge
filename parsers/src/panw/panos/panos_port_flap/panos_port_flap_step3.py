from parser_service.public.base_parser import BaseParser
from parser_service.public import helper_methods

class PanosShowSystemLogEntries(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data = helper_methods.parse_data_as_xml(raw_data)
            if data and data['response']['@status'] == "success":
                if 'result' in data['response'] and data['response']['result']:
                    int_name_list = dynamic_var['int_name_list'].split(';')
                    if data['response']['result']['log']['logs'].get('entry'):
                        entries = data['response']['result']['log']['logs']['entry']
                    for interface in int_name_list:
                        if interface != 'no-value-due-https-error':
                            log_entries = 0
                            tags = {}
                            tags['name'] = interface
                            if 'entries' in locals():
                                for entry in entries:
                                    if entry['object'] == interface:
                                        if entry['opaque'].find('MAC UP') == -1:
                                            log_entries += 1
                            self.write_double_metric('interface-flap-status', tags, 'gauge', 1 if log_entries > 1 else 0, False)
        return self.output
