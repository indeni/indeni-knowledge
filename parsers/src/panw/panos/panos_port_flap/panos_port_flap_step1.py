from parser_service.public.base_parser import BaseParser
from parser_service.public import helper_methods

class PanosShowInterfaceAll(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data: 
            data = helper_methods.parse_data_as_xml(raw_data)
            if data and data['response']['@status'] == "success":
                if 'result' in data['response'] and data['response']['result']:
                    int_name_list = []
                    if data['response']['result'].get('hw'):
                        entries = data['response']['result']['hw']['entry']
                        for entry in entries:
                            int_name_list.append(entry['name'])
                        self.write_dynamic_variable('int_name_list', ';'.join(int_name_list))
            elif data and data['response']['@status'] != "success":
                self.write_dynamic_variable('int_name_list','no-value-due-https-error')

        return self.output
