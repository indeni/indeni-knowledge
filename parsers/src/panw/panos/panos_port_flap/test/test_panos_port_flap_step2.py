import os
import unittest

from panw.panos.panos_port_flap.panos_port_flap_step2 import PanosShowLogSystemJobId
from parser_service.public.action import WriteDynamicVariable

class TestPanosShowLogSystemJobId(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = PanosShowLogSystemJobId()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_show_log_system_eventid_last_15min_error(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/show_log_system_eventid_last_15min_error.input', {'int_name_list': 'ethernet1/1;ethernet1/2;ethernet1/3;ethernet1/4;ethernet1/5'}, {})

        # Assert
        self.assertEqual(2, len(result))

        self.assertTrue(isinstance(result[0], WriteDynamicVariable))
        self.assertEqual('int_name_list', result[0].key)
        self.assertEqual('no-value-due-https-error', result[0].value)

        self.assertTrue(isinstance(result[1], WriteDynamicVariable))
        self.assertEqual('log_query_job_id', result[1].key)
        self.assertEqual('0', result[1].value)


    def test_show_log_system_eventid_last_15min(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/show_log_system_eventid_last_15min.input', {'int_name_list': 'ethernet1/1;ethernet1/2;ethernet1/3;ethernet1/4;ethernet1/5'}, {})

        # Assert
        self.assertEqual(2, len(result))

        self.assertTrue(isinstance(result[0], WriteDynamicVariable))
        self.assertEqual('int_name_list', result[0].key)
        self.assertEqual('ethernet1/1;ethernet1/2;ethernet1/3;ethernet1/4;ethernet1/5', result[0].value)

        self.assertTrue(isinstance(result[1], WriteDynamicVariable))
        self.assertEqual('log_query_job_id', result[1].key)
        self.assertEqual('587', result[1].value)


if __name__ == '__main__':
    unittest.main()
