import os
import unittest

from panw.panos.panos_port_flap.panos_port_flap_step1 import PanosShowInterfaceAll
from parser_service.public.action import WriteDynamicVariable

class TestPanosShowInterfaceAll(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = PanosShowInterfaceAll()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_show_interface_all_error(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/show_interface_all_error.input', {}, {})

        # Assert
        self.assertEqual(1, len(result))

        self.assertTrue(isinstance(result[0], WriteDynamicVariable))
        self.assertEqual('int_name_list', result[0].key)
        self.assertEqual('no-value-due-https-error', result[0].value)

    def test_show_interface_all(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/show_interface_all.input', {}, {})

        # Assert
        self.assertEqual(1, len(result))

        self.assertTrue(isinstance(result[0], WriteDynamicVariable))
        self.assertEqual('int_name_list', result[0].key)
        self.assertEqual('ethernet1/1;ethernet1/2;ethernet1/3;ethernet1/4;ethernet1/5', result[0].value)



if __name__ == '__main__':
    unittest.main()
