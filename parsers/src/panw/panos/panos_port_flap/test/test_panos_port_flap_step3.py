import os
import unittest

from panw.panos.panos_port_flap.panos_port_flap_step3 import PanosShowSystemLogEntries
from parser_service.public.action import WriteDoubleMetric

class TestPanosShowSystemLogEntries(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = PanosShowSystemLogEntries()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_show_system_log_entries(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/show_system_log_entries.input', {'int_name_list': 'ethernet1/1;ethernet1/2;ethernet1/3;ethernet1/4;ethernet1/5', 'log_query_job_id': '587' }, {})

        # Assert
        self.assertEqual(5, len(result))

        self.assertTrue(isinstance(result[0], WriteDoubleMetric))
        self.assertEqual(result[0].name, 'interface-flap-status')
        self.assertEqual(result[0].tags['name'], 'ethernet1/1')
        self.assertEqual(result[0].value, 0)

        self.assertTrue(isinstance(result[1], WriteDoubleMetric))
        self.assertEqual(result[1].name, 'interface-flap-status')
        self.assertEqual(result[1].tags['name'], 'ethernet1/2')
        self.assertEqual(result[1].value, 0)

        self.assertTrue(isinstance(result[2], WriteDoubleMetric))
        self.assertEqual(result[2].name, 'interface-flap-status')
        self.assertEqual(result[2].tags['name'], 'ethernet1/3')
        self.assertEqual(result[2].value, 0)

    def test_show_system_log_entries_empty(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/show_system_log_entries_empty.input', {'int_name_list': 'ethernet1/1;ethernet1/2;ethernet1/3;ethernet1/4;ethernet1/5', 'log_query_job_id': '587' }, {})

        # Assert
        self.assertEqual(5, len(result))

        self.assertTrue(isinstance(result[0], WriteDoubleMetric))
        self.assertEqual(result[0].name, 'interface-flap-status')
        self.assertEqual(result[0].tags['name'], 'ethernet1/1')
        self.assertEqual(result[0].value, 0)

        self.assertTrue(isinstance(result[1], WriteDoubleMetric))
        self.assertEqual(result[1].name, 'interface-flap-status')
        self.assertEqual(result[1].tags['name'], 'ethernet1/2')
        self.assertEqual(result[1].value, 0)

        self.assertTrue(isinstance(result[2], WriteDoubleMetric))
        self.assertEqual(result[2].name, 'interface-flap-status')
        self.assertEqual(result[2].tags['name'], 'ethernet1/3')
        self.assertEqual(result[2].value, 0)

        self.assertTrue(isinstance(result[3], WriteDoubleMetric))
        self.assertEqual(result[3].name, 'interface-flap-status')
        self.assertEqual(result[3].tags['name'], 'ethernet1/4')
        self.assertEqual(result[3].value, 0)

        self.assertTrue(isinstance(result[4], WriteDoubleMetric))
        self.assertEqual(result[4].name, 'interface-flap-status')
        self.assertEqual(result[4].tags['name'], 'ethernet1/5')
        self.assertEqual(result[4].value, 0)

        self.assertTrue(isinstance(result[3], WriteDoubleMetric))
        self.assertEqual(result[3].name, 'interface-flap-status')
        self.assertEqual(result[3].tags['name'], 'ethernet1/4')
        self.assertEqual(result[3].value, 0)

        self.assertTrue(isinstance(result[4], WriteDoubleMetric))
        self.assertEqual(result[4].name, 'interface-flap-status')
        self.assertEqual(result[4].tags['name'], 'ethernet1/5')
        self.assertEqual(result[4].value, 0)

    def test_show_system_log_entries_error(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/show_system_log_entries_error.input', {'int_name_list': 'ethernet1/1;ethernet1/2;ethernet1/3;ethernet1/4;ethernet1/5', 'log_query_job_id': '587' }, {})

        # Assert
        self.assertEqual(0, len(result))



if __name__ == '__main__':
    unittest.main()
