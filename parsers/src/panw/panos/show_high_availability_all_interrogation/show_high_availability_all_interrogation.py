from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser
import re

class ShowHighAvailabilityAllInterrogation(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data.strip() != '':
            xml_data = helper_methods.parse_data_as_xml(raw_data)
            if  xml_data:
                try:
                    if (xml_data['response']['result']['enabled'] == "yes"):
                        high_availability = "true"
                    else:
                        high_availability = "false"
                    self.write_tag('high-availability', high_availability )
                except KeyError:
                    pass
                try:
                    local = xml_data['response']['result']['group']['local-info']['ha1-macaddr']
                    peer = xml_data['response']['result']['group']['peer-info']['ha1-macaddr']
                    if local < peer:
                        cluster_id = local
                    else:
                        cluster_id = peer
                    self.write_tag('cluster-id', cluster_id )
                except KeyError:
                    try:
                        local = xml_data['response']['result']['group']['local-info']['mgmt-macaddr']
                        peer = xml_data['response']['result']['group']['peer-info']['mgmt-macaddr']
                        if local < peer:
                            cluster_id = local
                        else:
                            cluster_id = peer
                        self.write_tag('cluster-id', cluster_id )
                    except KeyError:
                        pass
            return self.output
