import os
import unittest

from panw.panos.show_high_availability_all_interrogation.show_high_availability_all_interrogation import ShowHighAvailabilityAllInterrogation
from parser_service.public.action import *

class TestShowHighAvailabilityAllInterrogation(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = ShowHighAvailabilityAllInterrogation()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_valid_no_ha_pa_220(self):
        expected_results = [{'key': 'high-availability', 'value': 'false'}]

        # Act
        result = self.parser.parse_file(self.current_dir + '/valid_no_ha_pa220.input', {}, {})
        # Assert
        self.assertEqual(1, len(result))

        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i]['key'], result[i].key)
            self.assertEqual(expected_results[i]['value'], result[i].value)

    def test_valid_ha_active_active_v8_0_5_pavm(self):
        expected_results = [{'key': 'high-availability', 'value': 'true'}, {'key': 'cluster-id', 'value': '00:50:56:90:40:1e'}]

        # Act
        result = self.parser.parse_file(self.current_dir + '/valid_ha_active_active_v8_0_5_pavm.input', {}, {})
        # Assert
        self.assertEqual(2, len(result))

        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i]['key'], result[i].key)
            self.assertEqual(expected_results[i]['value'], result[i].value)

    def test_valid_ha_active_active_v7_1_6_pa3020(self):
        expected_results = [{'key': 'high-availability', 'value': 'true'}, {'key': 'cluster-id', 'value': '24:0b:0a:01:b8:c7'}]

        # Act
        result = self.parser.parse_file(self.current_dir + '/valid_ha_active_pasive_v7_1_6_pa3020.input', {}, {})
        # Assert
        self.assertEqual(2, len(result))

        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i]['key'], result[i].key)
            self.assertEqual(expected_results[i]['value'], result[i].value)

if __name__ == '__main__':
    unittest.main()