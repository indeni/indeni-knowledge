from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class ShowSystemStateFilterSP(BaseParser):
    def prepare_interface_metric(self, interface, if_stat: str, hexa_value):
        if interface[if_stat] is None or interface[if_stat] == '':
            stat = 0
        else:
            if hexa_value:
                stat = int(interface[if_stat], 16)
            else:
                stat = interface[if_stat]
        return int(stat)

    def check_string(self, string: str) -> bool:
        return all(ord(item) < 128 for item in string)

    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data and self.check_string(raw_data):
            xml_data = helper_methods.parse_data_as_xml(raw_data)
            raw_data_parsed = []
            if xml_data and xml_data['response']['@status'] == 'success':
                raw_data_parsed = helper_methods.parse_data_as_list(raw_data, 'panw_panos_show_system_state_filter_sys_s_p.textfsm')
            if raw_data_parsed:
                interfaces = []
                for interface in raw_data_parsed:
                    if interface['slot'] and interface['port']:
                        if interface['port'].startswith('ha'):
                            interface_name = "Ethernet" + interface['slot'] + "/" + interface['port']
                        else:
                            interface_name = "Ethernet" + interface['slot'] + "/" + interface['port'].split('p')[-1]
                        interfaces.append(interface_name)
                        metric_tags_string = {'name': interface_name, 'im.identity-tags': 'name', 'im.dstype.displaytype': 'string'}
                        metric_tags_double = {'name': interface_name, 'im.identity-tags': 'name'}

                        if interface['admin_state'] and interface['admin_state'] is not None:
                            if interface['admin_state'] in ['PowerDown', 'Disabled']:
                                admin_state = 0
                            else:
                                admin_state = 1
                            self.write_double_metric('network-interface-admin-state', metric_tags_double, 'gauge', admin_state, True, 'Network Interfaces - admin state', 'state', 'name')

                        if admin_state == 1:
                            if interface['link_status'] and interface['link_status'] is not None:
                                if 'Down' in interface['link_status']:
                                    if_state = 0
                                else:
                                    if_state = 1
                                self.write_double_metric('network-interface-state', metric_tags_double, 'gauge', if_state, True, 'Network Interfaces - state', 'state', 'name')

                        speed = ''

                        if interface['speed_mode'] and interface['speed_mode'] is not None and interface['speed_mode'] != 'auto':
                            mode = ''
                            if 'half' in interface['speed_mode']:
                                mode = 'half'
                            elif 'full' in interface['speed_mode']:
                                mode = 'full'
                            self.write_complex_metric_string('network-interface-duplex', metric_tags_string, mode, True, 'Network Interfaces - duplex settings')

                            if 'auto' not in interface['speed_mode']:
                                speed = interface['speed_mode']
                                speed = speed.replace('-half', '')
                                speed = speed.replace('-full', '')
                                speed = speed.replace('Mb/s','M')
                                speed = speed.replace('Gb/s','G')
                                self.write_complex_metric_string('network-interface-speed', metric_tags_string, speed, True, 'Network Interfaces - speed')

                        if interface['mtu']:
                            self.write_complex_metric_string('network-interface-mtu', metric_tags_string, interface['mtu'], True,'Network Interfaces - MTU')

                        if interface['mac_address']:
                            self.write_complex_metric_string('network-interface-mac', metric_tags_string, interface['mac_address'], True, 'Network Interfaces - MAC Address')

                        # Validating that interface admin state is up and link is up, before reporting counters to the DB:
                        if admin_state == 1 and if_state == 1:
                            if interface['duration']:
                                seconds = int(interface['duration'])
                                if seconds > 0:
                                    rate_rx = (int(interface['rx_bytes']) * 8 / seconds) / 1000000
                                    rate_tx = (int(interface['tx_bytes']) * 8 / seconds) / 1000000
                                    self.write_double_metric('network-interface-rx-rate-mbps', metric_tags_double, 'gauge', round(rate_rx,4), True, 'Network Interfaces - RX rate (Mbps)', 'number', 'name')
                                    self.write_double_metric('network-interface-tx-rate-mbps', metric_tags_double, 'gauge', round(rate_tx,4), True, 'Network Interfaces - TX rate (Mbps)', 'number', 'name')
                                    self.write_double_metric('network-interface-total-rate-mbps', metric_tags_double, 'gauge', round(round(rate_rx,4)+round(rate_tx,4),4), True, 'Network Interfaces - Total rate (Mbps)', 'number', 'name')

                            # Reporting speed as bandwidth:
                            if speed:
                                if 'M' in speed:
                                    bandwidth = int(speed.replace('M', ''))
                                    self.write_double_metric('network-interface-bandwidth-mbps', metric_tags_double, 'gauge', bandwidth, False, 'number', 'name')
                                elif 'G' in speed:
                                    bandwidth = int(speed.replace('G', '')) * 1000
                                    self.write_double_metric('network-interface-bandwidth-mbps', metric_tags_double, 'gauge', bandwidth, False, 'number', 'name')

                            if interface['crc'] is not None:
                                interface_stat = self.prepare_interface_metric(interface, 'crc', True)
                                self.write_double_metric('network-interface-rx-frame', metric_tags_double, 'counter', interface_stat, True, 'Network Interfaces - CRC Errors', 'number', 'name')

                            if interface['errors'] is not None:
                                interface_stat = self.prepare_interface_metric(interface, 'errors', True)
                                self.write_double_metric('network-interface-rx-errors', metric_tags_double, 'counter', interface_stat, True, 'Network Interfaces - RX Errors', 'number', 'name')

                if interfaces:
                    self.write_complex_metric_object_array('network-interfaces', {}, dict.fromkeys(interfaces), False)

        return self.output
