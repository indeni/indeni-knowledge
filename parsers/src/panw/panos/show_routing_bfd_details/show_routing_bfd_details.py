from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class ShowRoutingBfdDetails(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            xml_data = helper_methods.parse_data_as_xml(raw_data)
            if xml_data and xml_data['response']['@status'] == 'success':
                if xml_data['response'].get('result'):
                    data = [xml_data['response']['result']['entry']]
                    for session in data:
                        if (session['mode'] == 'Active' and session['demand-mode'] == 'Disabled'):
                            tags = {}
                            tags['name'] = f"{session['local-ip-address']},{session['neighbor-ip-address']}({session['protocol']})"
                            tags['im.identity-tags'] = 'name|node'
                            tags['node'] = 'local'
                            self.write_complex_metric_string('bfd-local-state', tags, session['state-local'], True, 'BFD node status')
                            self.write_complex_metric_string('bfd-local-diag-code', tags, session['local-diag-code'], True, "BFD status last change reason")
                            tags['node'] = 'remote'
                            self.write_complex_metric_string('bfd-remote-state', tags, session['state-remote'], True, 'BFD node status')
                            self.write_complex_metric_string('bfd-remote-diag-code', tags, session['last-received-remote-diag-code'], True, "BFD status last change reason")
        return self.output