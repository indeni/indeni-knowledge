from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser
import re

class ShowRoutingProtocolBgpPeer(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            xml_data = helper_methods.parse_data_as_xml(raw_data)

            if xml_data and xml_data['response']['@status'] == 'success':
                response = xml_data['response']['result']
                # print(response)
                if response and 'entry' in response:
                    bgp_entries = response['entry']
                    if type(response['entry']) != list:
                        bgp_entries = [response['entry']]
                    for entry in bgp_entries:
                        peer_name = entry['@peer']
                        peer_group = entry['peer-group']
                        peer_address = re.sub(':[0-9]*', '', entry['peer-address'])
                        peer_status = entry['status']
                        tags = {'name': 'BGP Group: ' + peer_group + ' Peer Name: ' + peer_name + ' Peer Address: ' + peer_address}
                        self.write_double_metric('bgp-state', tags, 'gauge', 1 if 'Established' in peer_status or 'Idle' in peer_status else 0, True, 'BGP Peers - State', 'boolean', 'name')
        return self.output
