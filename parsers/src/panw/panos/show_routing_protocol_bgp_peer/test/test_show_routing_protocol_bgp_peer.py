import os
import unittest
from panw.panos.show_routing_protocol_bgp_peer.show_routing_protocol_bgp_peer import ShowRoutingProtocolBgpPeer
from parser_service.public.action import *


class TestShowRoutingProtocolBgpPeer(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = ShowRoutingProtocolBgpPeer()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_valid_input(self):
        result = self.parser.parse_file(self.current_dir + '/valid_one_peer_per_group.xml.input', {}, {})
        self.assertEqual(2, len(result))

        self.assertTrue(isinstance(result[0], WriteDoubleMetric))
        self.assertEqual('bgp-state', result[0].name)
        self.assertEqual('BGP Group: SomeName Peer Name: 10.3.1.11 Peer Address: 10.3.1.11', result[0].tags['name'])
        self.assertEqual(0, result[0].value)

        self.assertTrue(isinstance(result[1], WriteDoubleMetric))
        self.assertEqual('bgp-state', result[1].name)
        self.assertEqual('BGP Group: Some other name Peer Name: 10.3.1.12 Peer Address: 10.3.1.12', result[1].tags['name'])
        self.assertEqual(1, result[1].value)

    def test_valid_input_several_peers(self):
        result = self.parser.parse_file(self.current_dir + '/valid_several_peers_per_group.xml.input', {}, {})
        self.assertEqual(4, len(result))

        self.assertTrue(isinstance(result[0], WriteDoubleMetric))
        self.assertEqual('bgp-state', result[0].name)
        self.assertEqual('BGP Group: PG-1 Peer Name: PA-500-2 Peer Address: 2.2.2.2', result[0].tags['name'])
        self.assertEqual(0, result[0].value)

        self.assertTrue(isinstance(result[1], WriteDoubleMetric))
        self.assertEqual('bgp-state', result[1].name)
        self.assertEqual('BGP Group: PG-2 Peer Name: PG-2-1 Peer Address: 5.5.5.5', result[1].tags['name'])
        self.assertEqual(0, result[1].value)

        self.assertTrue(isinstance(result[2], WriteDoubleMetric))
        self.assertEqual('bgp-state', result[2].name)
        self.assertEqual('BGP Group: PG-1 Peer Name: PA-500 Peer Address: 10.10.10.3', result[2].tags['name'])
        self.assertEqual(1, result[2].value)

        self.assertTrue(isinstance(result[3], WriteDoubleMetric))
        self.assertEqual('bgp-state', result[3].name)
        self.assertEqual('BGP Group: PG-2 Peer Name: PG-2-2 Peer Address: 12.1.1.1', result[3].tags['name'])
        self.assertEqual(0, result[3].value)

    def test_not_valid_input(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/empty.xml.input', {}, {})
        # Assert
        self.assertEqual(0, len(result))


    def test_valid_pan_10_one_bgp_peer(self):
        expected_results = [{
            'action_type': 'WriteDoubleMetric',
            'timestamp': 1599641288072,
            'tags': {
                'name': 'BGP Group: PAN_10_BGP_peer_1 Peer Name: PAN_10_peer_bgp_1 Peer Address: 10.0.102.3',
                'live-config': 'true',
                'display-name': 'BGP Peers - State',
                'im.dstype.displayType': 'boolean',
                'im.identity-tags': 'name'
                },
            'value': 0,
            'name': 'bgp-state',
            'ds_type': 'gauge'
            }
        ]

        # Act
        result = self.parser.parse_file(self.current_dir + '/valid_one_bgp_peer_panos10.xml.input', {}, {})
        # Assert
        self.assertEqual(1, len(result))

        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i]['tags']['name'], result[i].tags['name'])
            self.assertEqual(expected_results[i]['name'], result[i].name)
            self.assertEqual(expected_results[i]['value'], result[i].value)


    def test_valid_one_bgp_peer_idle_state(self):
        expected_results = [{
            'action_type': 'WriteDoubleMetric',
            'timestamp': 1612099328032,
            'tags': {
                'name': 'BGP Group: PAN_10_BGP_peer_1 Peer Name: PAN_10_peer_bgp_1 Peer Address: 10.0.102.3',
                'live-config': 'true',
                'display-name': 'BGP Peers - State',
                'im.dstype.displayType': 'boolean',
                'im.identity-tags': 'name'
                },
            'value': 1,
            'name': 'bgp-state',
            'ds_type': 'gauge'
            }
        ]

        # Act
        result = self.parser.parse_file(self.current_dir + '/valid_one_bgp_peer_idle_state.xml.input', {}, {})
        # Assert
        self.assertEqual(1, len(result))

        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i]['tags']['name'], result[i].tags['name'])
            self.assertEqual(expected_results[i]['name'], result[i].name)
            self.assertEqual(expected_results[i]['value'], result[i].value)


if __name__ == '__main__':
    unittest.main()
