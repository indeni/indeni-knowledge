# Debug status lines look like this:
#Debug level is info
/Debug level/ {
    debugtags["name"] = "debug.user-id.get"

    state = 1
    #The default state for this debug level is info so info will set the state to 0 or not in debug.
    if ($NF == "info" || $NF == "off") {
        state = 0
    }
    writeDoubleMetric("debug-status", debugtags, "gauge", state, "false")  # Converted to new syntax by change_ind_scripts.py script
}