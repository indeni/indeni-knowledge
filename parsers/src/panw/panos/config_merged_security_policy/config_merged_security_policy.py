from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

def clears(self: str):
    string = self.strip("[']").replace("'", '')
    return string
    

def dict_check(my_dict: dict, iterator: list) -> bool:
    try:
        dict_iter = my_dict
        iter_list = []
        for item in iterator:
            dict_iter = dict_iter.get(item)
        return True
    except:
        return False


class ConfigMergedSecurityPolicy(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict, required=None):
        if raw_data and 'status="success"' in raw_data:
            # Base XML parser
            data = helper_methods.parse_data_as_xml(raw_data)

            # # Parse Rule base with action allow:
            rules_list = ''
            if dict_check(data, ['response','result','devices','entry','vsys','entry','rulebase','security','rules','entry']) is True:
                rules_list = data['response']['result']['devices']['entry']['vsys']['entry']['rulebase']['security']['rules']['entry']
                if type(rules_list) != list:
                    rules_list = [rules_list]
            
            # Report the policy structure:
            if rules_list:
                rules_db_list = []
                for rule in rules_list:
                    if rule['action'] == 'allow':
                        rules_data = {'name': rule['@name'] + ', ' + clears(str(rule['from']['member'])) + ', ' + clears(str(rule['source']['member'])) + ', ' + clears(str(rule['to']['member'])) + ', ' + clears(str(rule['destination']['member'])) + ', ' + clears(str(rule['application']['member'])) + ', ' + clears(str(rule['service']['member']))}
                        rules_db_list.append(rules_data)
                self.write_complex_metric_object_array('panos-security-policy-audit', {}, rules_db_list, False)
            
            # Reporting if a service has "any":
                for rule in rules_list:
                    if rule['action'] == 'allow':
                        tags = {'name': rule['@name']}
                        self.write_double_metric('panos-security-policy-service-misconfigured', tags, 'gauge', 0 if 'any' in rule['service']['member'] else 1, True, 'Security Policies Allowing Traffic', 'state', 'name')
            
                # Preparing variables for report:
                pre_rulebase = ''
                if dict_check(data, ['response','result','panorama','vsys','entry','pre-rulebase','security','rules','entry']) is True:
                    pre_rulebase = data['response']['result']['panorama']['vsys']['entry']['pre-rulebase']['security']['rules']['entry']
                    if type(pre_rulebase) is list:
                        pre_rulebase = pre_rulebase[-1]
                post_rulebase = ''
                if dict_check(data, ['response','result','panorama','vsys','entry','post-rulebase','security','rules','entry']) is True:
                    post_rulebase = data['response']['result']['panorama']['vsys']['entry']['post-rulebase']['security']['rules']['entry']
                    if type(post_rulebase) is list:
                        post_rulebase = post_rulebase[-1]
                last_rules_list = rules_list[-1]

                if pre_rulebase and post_rulebase and last_rules_list:
                    # Pre variables
                    # print(pre_rulebase[-1])
                    pre_service_setting = pre_rulebase['service']['member']
                    pre_source = pre_rulebase['source']['member']
                    pre_destination = pre_rulebase['destination']['member']
                    pre_application = pre_rulebase['application']['member']
                    pre_action = pre_rulebase['action']
                    pre_profiles = pre_rulebase.get('profile-setting', {}).get('profiles', {})
                    pre_policy_name = pre_rulebase['@name']
                    # post variables
                    post_service_setting = post_rulebase['service']['member']
                    post_source = post_rulebase['source']['member']
                    post_destination = post_rulebase['destination']['member']
                    post_application = post_rulebase['application']['member']
                    post_profiles = post_rulebase.get('profile-setting', {}).get('profiles', {})
                    post_action = post_rulebase['action']
                    post_policy_name = post_rulebase['@name']
                    
                    # Last rules-set variables:
                    service_setting = last_rules_list['service']['member']
                    source = last_rules_list['source']['member']
                    destination = last_rules_list['destination']['member']
                    application = last_rules_list['application']['member']
                    action = last_rules_list['action']
                    profiles = last_rules_list.get('profile-setting', {}).get('profiles', {})
                    policy_name = last_rules_list['@name']

                    # Tags and report last rule is blocking to not, to DB:
                    if post_policy_name and post_policy_name != '':
                        tags = {'policy': post_policy_name}
                        if 'any' not in (post_service_setting, post_source, post_destination, post_application) or 'block' not in post_action or 'deny' not in post_action or 'drop' not in post_action or post_profiles != 0:
                            block_any_value = 0
                        else:
                            block_any_value = 1
                    elif policy_name and policy_name != '':
                        tags = {'policy': policy_name}
                        if 'any' not in (service_setting, source, destination, application) or 'block' not in action or 'deny' not in action or 'drop' not in action or profiles != 0:
                            block_any_value = 0
                        else:
                            block_any_value = 1
                    elif pre_policy_name:
                        tags = {'policy': pre_policy_name}
                        if 'any' not in (pre_service_setting, pre_source, pre_destination, pre_application) or 'block' not in pre_action or 'deny' not in pre_action or 'drop' not in pre_action or pre_profiles != 0:
                            block_any_value = 0
                        else:
                            block_any_value = 1
                    self.write_double_metric('panos-security-policy-block-any', tags, 'gauge', block_any_value, False)
        return self.output
