import os
import unittest
 
from panw.panos.config_merged_security_policy.config_merged_security_policy import ConfigMergedSecurityPolicy
from parser_service.public.action import *

class TestConfigMergedSecurityPolicy(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = ConfigMergedSecurityPolicy()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_valid_input_few_items_in_service(self):
        expected_results = [{'name': None, 'metric': 'panos-security-policy-audit', 'value': [{'name': 'block DNS, any, 10.11.95.31, any, 4.2.2.2, 8.8.8.8, dns, application-default'},
                            {'name': 'Test_allow, any, any, trust, any, any, any, application-default, any'},
                            {'name': 'block wildfire servers_App-id, any, 10.11.95.31, any, panos.wildfire.paloaltonetworks.com, wildfire.paloaltonetworks.com, paloalto-wildfire-cloud, application-default'},
                            {'name': 'block wildfire servers-Ping, any, 10.11.95.31, any, panos.wildfire.paloaltonetworks.com, wildfire.paloaltonetworks.com, ping, application-default'},
                            {'name': 'block wildfire servers-Traceroute, any, 10.11.95.31, any, panos.wildfire.paloaltonetworks.com, wildfire.paloaltonetworks.com, traceroute, application-default'},
                            {'name': 'untrust-in, untrust, 10.0.0.0-10.255.255.255, 172.16.0.0-172.31.255.255, any, any, any, application-default'},
                            {'name': 'trust-to-untrust, trust, any, untrust, any, 3pc, 51.com-games, application-default'},
                            {'name': 'trust-vlan, User-Vlan, any, User-Vlan, any, any, any'},
                            {'name': 'trust-trust, trust, any, trust, any, unknown-tcp, unknown-udp, any'},
                            {'name': 'trust-trust ping, trust, any, trust, any, ping, any'},
                            {'name': 'allow_10.10.10.0, any, any, any, any, any, any'}]},
                            {'name': 'block DNS', 'metric': 'panos-security-policy-service-misconfigured', 'value': 1},
                            {'name': 'Test_allow', 'metric': 'panos-security-policy-service-misconfigured', 'value': 0}, 
                            {'name': 'block wildfire servers_App-id', 'metric': 'panos-security-policy-service-misconfigured', 'value': 1}, 
                            {'name': 'block wildfire servers-Ping', 'metric': 'panos-security-policy-service-misconfigured', 'value': 1}, 
                            {'name': 'block wildfire servers-Traceroute', 'metric': 'panos-security-policy-service-misconfigured', 'value': 1}, 
                            {'name': 'untrust-in', 'metric': 'panos-security-policy-service-misconfigured', 'value': 1}, 
                            {'name': 'trust-to-untrust', 'metric': 'panos-security-policy-service-misconfigured', 'value': 1}, 
                            {'name': 'trust-vlan', 'metric': 'panos-security-policy-service-misconfigured', 'value': 0}, 
                            {'name': 'trust-trust', 'metric': 'panos-security-policy-service-misconfigured', 'value': 0}, 
                            {'name': 'trust-trust ping', 'metric': 'panos-security-policy-service-misconfigured', 'value': 0}, 
                            {'name': 'allow_10.10.10.0', 'metric': 'panos-security-policy-service-misconfigured', 'value': 0}, 
                            {'policy': 'panorama-post-dns', 'metric': 'panos-security-policy-block-any', 'value': 0}]

        # Act
        result = self.parser.parse_file(self.current_dir + '/valid_input_few_items_in_service.input', {}, {})
        # Assert
        self.assertEqual(13, len(result))

        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i].get('name'), result[i].tags.get('name'))
            self.assertEqual(expected_results[i]['metric'], result[i].name)
            self.assertEqual(expected_results[i]['value'], result[i].value)
            self.assertEqual(expected_results[i].get('policy'), result[i].tags.get('policy'))


    def test_valid_input_regular_policy(self):
        expected_results = [{'name': None, 'metric': 'panos-security-policy-audit', 'value': [{'name': 'untrust-in, untrust, 172.16.0.0-172.31.255.255, any, any, any, application-default'}, 
                            {'name': 'ikp-3185-new-test, untrust, 172.16.0.0-172.31.255.255, any, any, any, application-default'}, 
                            {'name': 'trust-to-untrust, trust, any, untrust, any, any, application-default'}, 
                            {'name': 'trust-vlan, User-Vlan, any, User-Vlan, any, any, any'}, 
                            {'name': 'trust-trust, trust, any, trust, any, unknown-tcp, unknown-udp, any'}]}, 
                            {'name': 'untrust-in', 'metric': 'panos-security-policy-service-misconfigured', 'value': 1}, 
                            {'name': 'ikp-3185-new-test', 'metric': 'panos-security-policy-service-misconfigured', 'value': 1}, 
                            {'name': 'trust-to-untrust', 'metric': 'panos-security-policy-service-misconfigured', 'value': 1}, 
                            {'name': 'trust-vlan', 'metric': 'panos-security-policy-service-misconfigured', 'value': 0}, 
                            {'name': 'trust-trust', 'metric': 'panos-security-policy-service-misconfigured', 'value': 0}, 
                            {'policy': 'panorama-post-dns', 'metric': 'panos-security-policy-block-any', 'value': 0}]

        # Act
        result = self.parser.parse_file(self.current_dir + '/valid_input_regular_policy.input', {}, {})
        # Assert
        self.assertEqual(7, len(result))

        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i].get('name'), result[i].tags.get('name'))
            self.assertEqual(expected_results[i]['metric'], result[i].name)
            self.assertEqual(expected_results[i]['value'], result[i].value)
            self.assertEqual(expected_results[i].get('policy'), result[i].tags.get('policy'))


    def test_invalid_empty(self):
        expected_results = []

        # Act
        result = self.parser.parse_file(self.current_dir + '/invalid_empty.input', {}, {})
        # Assert
        self.assertEqual(0, len(result))


    def test_valid_no_rulebase_vsys(self):
        expected_results = []

        # Act
        result = self.parser.parse_file(self.current_dir + '/test_valid_no_rulebase_vsys.input', {}, {})
        # Assert
        self.assertEqual(0, len(result))


    def test_valid_no_pre_rulebase(self):
        expected_results = [{'name': None, 'metric': 'panos-security-policy-audit', 'value': [{'name': 'Google NTP server, any, US, any, Google NTP server, any, application-default'}, {'name': 'allow_10.10.10.0, any, 10.10.10.0, ATE_test, 10.10.10.0, any, any'}]},
                            {'name': 'Google NTP server', 'metric': 'panos-security-policy-service-misconfigured', 'value': 1},
                            {'name': 'allow_10.10.10.0', 'metric': 'panos-security-policy-service-misconfigured', 'value': 0}]

        # Act
        result = self.parser.parse_file(self.current_dir + '/test_valid_no_pre_rulebase.input', {}, {})
        # Assert
        self.assertEqual(3, len(result))

        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i].get('name'), result[i].tags.get('name'))
            self.assertEqual(expected_results[i]['metric'], result[i].name)
            self.assertEqual(expected_results[i]['value'], result[i].value)


if __name__ == '__main__':
    unittest.main()