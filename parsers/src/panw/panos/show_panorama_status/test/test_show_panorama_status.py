import os
import unittest

from panw.panos.show_panorama_status.show_panorama_status import ShowPanoramaStatus
from parser_service.public.action import *

class TestShowPanoramaStatus(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = ShowPanoramaStatus()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_2panorama_servers(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/2panorama_servers.input', {}, {})
        # Assert
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].value[0], {'id': '1', 'mgmt_server': '10.11.95.11'})
        self.assertEqual(result[0].value[1], {'id': '2', 'mgmt_server': '10.11.95.12'})

    def test_panorama_servers_empty(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/panorama_servers_empty.input', {}, {})
        # Assert
        self.assertEqual(result,[])

if __name__ == '__main__':
    unittest.main()