from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class ShowPanoramaStatus(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data = helper_methods.parse_data_as_list(raw_data, 'show_panorama_status.textfsm')
            if data:
                managements = []
                for entry in data:
                    management_server = {}
                    management_server['id'] = entry['id']
                    management_server['mgmt_server'] = entry['ip_address']
                    managements.append(management_server)
                tags = {}
                self.write_complex_metric_object_array("management-servers",tags,managements, True,"Management Server")

        return self.output
