from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class ShowVPNStep1(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            xml_data = helper_methods.parse_data_as_xml(raw_data)

            if xml_data and xml_data['response']['@status'] == 'success':
                response = xml_data['response']['result']
                entries = response.get('entries')
                if entries:
                    entry = entries.get('entry')
                    if entry:
                        if isinstance(entry, list):
                            for entry in response['entries']['entry']:
                                self.write_dynamic_variable('gw_name_tunnel_name', entry['gw'] + '|' + entry['name'])
                        else:
                            self.write_dynamic_variable('gw_name_tunnel_name', entry['gw'] + '|' + entry['name'])
        return self.output
