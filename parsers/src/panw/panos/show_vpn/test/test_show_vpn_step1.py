import os
import unittest
from panw.panos.show_vpn.show_vpn_step1 import ShowVPNStep1


class TestShowVPNStep1(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = ShowVPNStep1()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_valid_input(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/valid_input_vpn_step1.xml', {}, {})
        # # Assert
        self.assertEqual(5, len(result))

        self.assertEqual('gw_name_tunnel_name', result[0].key)
        self.assertEqual('ATE_test|ATE_test:interesting-traffic', result[0].value)

        self.assertEqual('gw_name_tunnel_name', result[1].key)
        self.assertEqual('ATE_test|ATE_test:super_interesting_traffic', result[1].value)

        self.assertEqual('gw_name_tunnel_name', result[2].key)
        self.assertEqual('GW_DOWN|ATE_tunnel_failed', result[2].value)

        self.assertEqual('gw_name_tunnel_name', result[3].key)
        self.assertEqual('ATE_test|ATE_test_2:other_traffic', result[3].value)

        self.assertEqual('gw_name_tunnel_name', result[4].key)
        self.assertEqual('ATE_test_2|ATE_test_2:other_traffic', result[4].value)

    def test_valid_input_single_entry(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/valid_input_vpn_step1_single_entry.xml', {}, {})
        # # Assert
        self.assertEqual(1, len(result))
        self.assertEqual('gw_name_tunnel_name', result[0].key)
        self.assertEqual('Indeni-GW-Untrust|PA5060-to-PA500', result[0].value)

    def test_valid_input_no_tunnels(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/valid_input_vpn_step1_no_tunnels.xml', {}, {})
        # # Assert
        self.assertEqual(0, len(result))

    def test_invalid_input(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/invalid_input_empty.xml', {}, {})
        # Assert
        self.assertEqual(0, len(result))


if __name__ == '__main__':
    unittest.main()
