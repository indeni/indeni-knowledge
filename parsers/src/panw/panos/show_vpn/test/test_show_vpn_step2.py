import os
import unittest
from panw.panos.show_vpn.show_vpn_step2 import ShowVPNStep2


class TestShowVPNStep1(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = ShowVPNStep2()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_valid_input_up_1(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/valid_input_vpn_step2_up.xml', {'gw_name_tunnel_name': 'ATE_test|ATE_test:interesting-traffic'}, {})
        # Assert
        self.assertEqual(1, len(result))
        self.assertEqual('vpn-phase1-state', result[0].name)
        self.assertEqual('ATE_test - ATE_test:interesting-traffic', result[0].tags['name'])
        self.assertEqual(1, result[0].value)

    def test_valid_input_up_2(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/valid_input_vpn_step2_up.xml', {'gw_name_tunnel_name': 'ATE_test_2|ATE_test_2:other_traffic'}, {})
        # Assert
        self.assertEqual(1, len(result))
        self.assertEqual('vpn-phase1-state', result[0].name)
        self.assertEqual('ATE_test_2 - ATE_test_2:other_traffic', result[0].tags['name'])
        self.assertEqual(1, result[0].value)

    def test_valid_input_down(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/valid_input_vpn_step2_down.xml', {'gw_name_tunnel_name': 'GW_DOWN|ATE_tunnel_failed'}, {})

        # # Assert
        self.assertEqual(1, len(result))
        self.assertEqual('vpn-phase1-state', result[0].name)
        self.assertEqual('GW_DOWN - ATE_tunnel_failed', result[0].tags['name'])
        self.assertEqual(0, result[0].value)

    def test_valid_input_1_entry(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/valid_input_vpn_step2_1_entry.xml', {'gw_name_tunnel_name': 'GW_DOWN|ATE_tunnel_failed'}, {})

        # # Assert
        self.assertEqual(1, len(result))
        self.assertEqual('vpn-phase1-state', result[0].name)
        self.assertEqual('GW_DOWN - ATE_tunnel_failed', result[0].tags['name'])
        self.assertEqual(0, result[0].value)

    def test_invalid_input(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/invalid_input_empty.xml', {}, {})
        # Assert
        self.assertEqual(0, len(result))


if __name__ == '__main__':
    unittest.main()
