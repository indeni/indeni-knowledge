from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class ShowVPNStep2(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data and 'gw_name_tunnel_name' in dynamic_var:
            xml_data = helper_methods.parse_data_as_xml(raw_data)
            gw_name_tunnel_name = dynamic_var['gw_name_tunnel_name'].split('|')
            gw_name_dynamic = gw_name_tunnel_name[0]
            tunnel_name_dynamic = gw_name_tunnel_name[1]

            if xml_data and xml_data['response']['@status'] == 'success':
                response = xml_data['response']['result']
                gw_found = 0
                if response and 'entry' in response:
                    if isinstance(response['entry'], list):
                        for entry in response['entry']:
                            gw_name = entry['name']
                            if gw_name in gw_name_dynamic:
                                gw_found = 1
                    else:
                        gw_name = response['entry']['name']
                        if gw_name in gw_name_dynamic:
                            gw_found = 1
                self.write_double_metric('vpn-phase1-state', {'name': gw_name_dynamic + ' - ' + tunnel_name_dynamic}, 'gauge', gw_found, True, 'VPN Tunnels - IKE Phase 1 State', 'state', 'name')
        return self.output
