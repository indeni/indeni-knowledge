from parser_service.public.helper_methods import *
from parser_service.public.base_parser import BaseParser

class ExternalDynamicList(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:  # We check if input is not empty
            data = parse_data_as_xml(raw_data)
            if data and 'result' in data['response']:
                if data['response']['@status'] == "success":
                    index = 0
                    for edl_type in data['response']['result']:
                        tags = {}
                        edl_type = list(data['response']['result'].keys())[index]
                        tags['type'] = edl_type
                        capacity = (float(data['response']['result'][edl_type]['running-cap'])/float(data['response']['result'][edl_type]['total-cap']))*100
                        self.write_double_metric('panos-edl-list-capacity', tags, 'gauge', capacity, True, 'EDL - Entries Capacity', 'percentage', 'type')
                        index += 1
        return self.output
