import os
import unittest

from panw.panos.external_dynamic_list.edl_list_empty.panos_edl_list_empty_step1 import ExternalDynamicList
from parser_service.public.action import *

class TestExternalDynamicList(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = ExternalDynamicList()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_one_list(self):
        expected_results = [
            {'action_type': 'WriteDynamicVariable',
            'timestamp': 1599566147615,
            'key': 'edl_name',
            'value': 'testing-github'
            },
            {'action_type': 'WriteDynamicVariable',
            'timestamp': 1599566147615,
            'key': 'edl_type',
            'value': 'url'
            }
        ]

        # Act
        result = self.parser.parse_file(self.current_dir + '/step1_one_list.input', {}, {})
        # Assert
        self.assertEqual(2, len(result))

        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i]['action_type'], result[i].action_type)
            self.assertEqual(expected_results[i]['key'], result[i].key)
            self.assertEqual(expected_results[i]['value'], result[i].value)


    def test_2_lists(self):
        expected_results = [
            {'action_type': 'WriteDynamicVariable',
            'timestamp': 1599566147615,
            'key': 'edl_name',
            'value': 'testing-github'
            },
            {'action_type': 'WriteDynamicVariable',
            'timestamp': 1599566147615,
             'key': 'edl_type',
             'value': 'url'
             },
            {'action_type': 'WriteDynamicVariable',
            'timestamp': 1599566147615,
             'key': 'edl_name',
             'value': 'testing-second_list'
             },
            {'action_type': 'WriteDynamicVariable',
            'timestamp': 1599566147615,
             'key': 'edl_type',
             'value': 'url'
             }
        ]

        # Act
        result = self.parser.parse_file(self.current_dir + '/step1_two_lists.input', {}, {})
        # Assert
        self.assertEqual(4, len(result))

        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i]['action_type'], result[i].action_type)
            self.assertEqual(expected_results[i]['key'], result[i].key)
            self.assertEqual(expected_results[i]['value'], result[i].value)


    def test_step1_empty_list(self):
        expected_results = [
            {'action_type': 'WriteDynamicVariable',
            'timestamp': 1599566147615,
            'key': 'edl_name',
            'value': 'default'
            },
            {'action_type': 'WriteDynamicVariable',
            'timestamp': 1599566147615,
            'key': 'edl_type',
            'value': 'default'
            }
        ]

        # Act
        result = self.parser.parse_file(self.current_dir + '/step1_empty_list.input', {}, {})
        # Assert
        self.assertEqual(2, len(result))
        
        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i]['action_type'], result[i].action_type)
            self.assertEqual(expected_results[i]['key'], result[i].key)
            self.assertEqual(expected_results[i]['value'], result[i].value)

if __name__ == '__main__':
    unittest.main()