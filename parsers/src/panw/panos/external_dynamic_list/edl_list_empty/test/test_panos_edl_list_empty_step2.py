import os
import unittest

from panw.panos.external_dynamic_list.edl_list_empty.panos_edl_list_empty_step2 import ExternalDynamicList
from parser_service.public.action import *

class TestExternalDynamicList(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = ExternalDynamicList()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_second_step_url_empty_list(self):
        expected_results = [{'action_type': 'WriteDoubleMetric', 'tags': {'name': 'testing-github', 'live-config': 'true', 'display-name': 'EDL - List Empty', 'im.dstype.displayType': 'boolean', 'im.identity-tags': 'name'}, 'value': 0, 'name': 'panos-edl-list-empty', 'ds_type': 'gauge'}]

        # Act
        result = self.parser.parse_file(self.current_dir + '/step2_url_empty_list.input', {'edl_name': 'testing-github', 'edl_type': 'url'}, {})
        # Assert
        self.assertEqual(1, len(result))

        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i]['action_type'], result[i].action_type)
            self.assertEqual(expected_results[i]['name'], result[i].name)
            self.assertEqual(expected_results[i]['value'], result[i].value)
            self.assertEqual(expected_results[i]['tags'], result[i].tags)


if __name__ == '__main__':
    unittest.main()