from parser_service.public.helper_methods import *
from parser_service.public.base_parser import BaseParser

class ExternalDynamicList(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data and 'status="success"' in raw_data:  # We check if input is not empty
            data = parse_data_as_xml(raw_data)
            if data and 'result' in data['response']:
                tags = {}
                tags['name'] = dynamic_var['edl_name']
                # Code for IP based lists:
                if dynamic_var['edl_type'] == 'ip':
                    state = 0
                    if data['response']['result']['external-list']['total-valid'] == "1":
                        if data['response']['result']['external-list']['valid-members']['member'] == "0.0.0.0/32" :
                            state = 1
                # Code for url based lists:
                if dynamic_var['edl_type'] == 'url':
                    state = 1
                    values_list = []
                    values_list.append(data['response']['result']['external-list']['total-valid'])
                    values_list.append(data['response']['result']['external-list']['total-ignored'])
                    values_list.append(data['response']['result']['external-list']['total-invalid'])
                    if all(item == '0' for item in values_list):
                        state = 0
            self.write_double_metric('panos-edl-list-empty', tags, 'gauge', state, True, 'EDL - List Empty', 'boolean', 'name')
        return self.output
