from parser_service.public.base_parser import BaseParser
from parser_service.public import helper_methods
    
class ExternalDynamicList(BaseParser):
    
    def write_var(self, var1: str, var2: str):
        self.write_dynamic_variable('edl_name', var1)
        self.write_dynamic_variable('edl_type', var2)
    
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        # As this is multi-step IND, we need to return some value, to avoid the IND from crashing.
        edl_name = 'default'
        edl_type = 'default'
        try:    
            data = helper_methods.parse_data_as_xml(raw_data)
            edl_configured = data['response']['result']['external-list']['entry']
            if type(edl_configured) != list:
                edl_configured = [edl_configured]
            for single_edl in edl_configured:
                edl_name = single_edl['@name']
                edl_type = next(iter(single_edl['type']))
                self.write_var(edl_name, edl_type)
        except:
            self.write_var(edl_name, edl_type)
        return self.output
