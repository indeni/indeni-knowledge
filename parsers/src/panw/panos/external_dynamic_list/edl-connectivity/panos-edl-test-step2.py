from parser_service.public.helper_methods import *
from parser_service.public.base_parser import BaseParser

class ExternalDynamicList(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data and dynamic_var['edl_url'] != 'default':
            data = parse_data_as_xml(raw_data)
            if data:
                tags = {}
                test_result = data['response']['@status']
                tags['url'] = dynamic_var['edl_url']
                type_str = '{}{}'.format(dynamic_var['edl_url'], 'edl_type')
                name_str = '{}{}'.format(dynamic_var['edl_url'], 'edl_name')
                tags['type'] = dynamic_var[type_str]
                tags['name'] = dynamic_var[name_str]
                self.write_double_metric('panos-edl-list-status', tags, 'gauge', 1 if test_result != 'error' else 0, True, 'EDL - Connectivity', 'boolean', 'name')
        return self.output
