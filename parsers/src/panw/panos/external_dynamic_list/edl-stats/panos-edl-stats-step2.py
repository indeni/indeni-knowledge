from parser_service.public.helper_methods import *
from parser_service.public.base_parser import BaseParser
from datetime import datetime

class ExternalDynamicList(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:  # We check if input is not empty
            data = parse_data_as_xml(raw_data)
            if data and 'result' in data['response']:
                if data['response']['@status'] == "success":
                    tags = {}
                    tags['edl.name'] = dynamic_var['edl_name']
                    tags['edl.vsys'] = data['response']['result']['external-list']['vsys']
                    edl_next_update = data['response']['result']['external-list']['next-update']
                    dt_object = datetime.strptime(edl_next_update, "%a %b %d %H:%M:%S %Y")
                    seconds_epoch = datetime.timestamp(dt_object)
                    edl_referenced_in_policy = data['response']['result']['external-list']['referenced']
                    edl_valid = data['response']['result']['external-list']['valid']
                    edl_auth_valid = data['response']['result']['external-list']['auth-valid']
                    self.write_double_metric('panos-edl-list-update', tags, 'gauge', seconds_epoch, True, 'EDL - Next Update','date', 'edl.name')
                    self.write_double_metric('panos-edl-list-ref-policy', tags, 'gauge', 1 if edl_referenced_in_policy == "Yes" else 0, True, 'EDL - Presence in policy', 'boolean', 'edl.name')
                    self.write_double_metric('panos-edl-list-valid', tags, 'gauge', 1 if edl_valid == "Yes" else 0, True, 'EDL - Validation', 'boolean', 'edl.name')
                    self.write_double_metric('panos-edl-list-auth-valid', tags, 'gauge', 1 if edl_auth_valid == "Yes" else 0, True, 'EDL - Authentication Validation', 'boolean', 'edl.name')
            return self.output
