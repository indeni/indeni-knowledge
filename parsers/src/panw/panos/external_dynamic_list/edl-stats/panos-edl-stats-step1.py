from parser_service.public.helper_methods import *
from parser_service.public.base_parser import BaseParser

class ExternalDynamicList(BaseParser):

    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        # As this is multi-step IND, we need to return some value, to avoid the IND from crashing.
        edl_name = '-default-'
        try:
            data = parse_data_as_xml(raw_data)
            edl_configured = data['response']['result']['external-list']['entry']
            if type(edl_configured) != list:
                edl_configured = [edl_configured]
            for single_edl in edl_configured:
                edl_name = single_edl['@name']
                self.write_dynamic_variable('edl_name', edl_name)
        except:
            self.write_dynamic_variable('edl_name', edl_name)
        return self.output
