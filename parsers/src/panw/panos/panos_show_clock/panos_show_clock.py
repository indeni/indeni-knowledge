from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class PanosShowClock(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data = helper_methods.parse_data_as_xml(raw_data)
            if data and 'result' in data['response']:
                if data['response']['@status'] == "success":
                    parsed_data = helper_methods.parse_data_as_list(data['response']['result'], 'panos_show_clock.textfsm')
                    if parsed_data:
                        timezone_list = []
                        timezone = {}
                        timezone['current'] = parsed_data[0]['timezone']
                        timezone_list.append(timezone)
                        self.write_complex_metric_object_array('current-timezone', {}, timezone_list, False, "Timezone code")
        return self.output