import os
import unittest
from panw.panos.panos_show_clock.panos_show_clock import PanosShowClock
from parser_service.public.action import *

class TestPanosShowClock(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = PanosShowClock()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_timezone_CEST(self):
        result = self.parser.parse_file(self.current_dir + '/panos_show_clock.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual('PST',result[0].value[0]['current'])

if __name__ == '__main__':
    unittest.main()