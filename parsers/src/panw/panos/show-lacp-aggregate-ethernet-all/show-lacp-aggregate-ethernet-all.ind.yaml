name: panos-show-lacp-aggregate-ethernet-all
description: Fetch the status of LACP interfaces
type: monitoring
monitoring_interval: 5 minute
requires:
    vendor: paloaltonetworks
    os.name: panos
    product: firewall
comments:
    panos-bond-state:
        why: |
            Link aggregation (LACP) is used to provide redundancy at the interface level. Instead of relying on a specific interface to work 100% of the time on a given device, an administrator could define multiple physical interfaces to behave as one group. While providing additional stability, the challenge with this mode of operation is that many times there are hidden failures which are ignored. Imagine, for example, if one of the physical interfaces were to be disconnected. Traffic would continue to flow without an issue (thanks to LACP) but the level of redundancy has been descreased. Knowing about such events is therefore, important.
        how: |
            This alert uses the Palo Alto Networks API to retrieve the current status all LACP (ae) interfaces (the equivalent of running "show lacp aggregate-ethernet all" in CLI).
        can-with-snmp: true
        can-with-syslog: true
    panos-bond-slave-state:
        why: |
            Capture state for bond slave interfaces. Tracking the bond slaves is a good indicator of potential issues.
        how: |
            This alert uses the Palo Alto Networks API to retrieve the current status all LACP (ae) interfaces (the equivalent of running "show lacp aggregate-ethernet all" in CLI).
        can-with-snmp: true
        can-with-syslog: true
steps:
-   run:
        type: HTTP
        command: /api?type=op&cmd=<show><lacp><aggregate-ethernet>all</aggregate-ethernet></lacp></show>
    parse:
        type: XML
        file: show-lacp-aggregate-ethernet-all.parser.1.xml.yaml
