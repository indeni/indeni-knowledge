from unittest import result
from parser_service.public.base_parser import BaseParser
from parser_service.public import helper_methods
class PanosShowSnmpTrapCommunity(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data = helper_methods.parse_data_as_xml(raw_data)
            if data and 'result' in data['response']:
                if data['response']['@status'] == "success" and data['response']['result'] is not None:
                    try:
                        list = []
                        data_list = data['response']['result']['snmptrap']['entry']
                        for community_list in data_list:
                            entry = {}
                            if isinstance(community_list,dict):
                                item_list = community_list['version']['v2c']['server']['entry']
                                if isinstance(item_list, dict):
                                    entry['version'] = item_list['@name']
                                    entry['manager'] = item_list['manager']
                                    entry['community'] = item_list['community']
                                    list.append(entry)
                            if isinstance(data_list,dict):
                                multicase_list = data_list['version']['v2c']['server']['entry']
                                if isinstance(multicase_list,dict):
                                    if community_list == 'version':
                                        entry['version'] = multicase_list['@name']
                                        entry['manager'] = multicase_list['manager']
                                        entry['community'] = multicase_list['community']
                                        list.append(entry)
                                else:
                                    for multicase_community in multicase_list:
                                        if community_list == 'version':
                                            entry = {}
                                            entry['version'] = multicase_community['@name']
                                            entry['manager'] = multicase_community['manager']['#text']
                                            entry['community'] = multicase_community['community']['#text']
                                            list.append(entry)
                        self.write_complex_metric_object_array('snmp-trap-community', {}, list, True,'SNMP-Trap')
                    except Exception:
                        pass

        return self.output