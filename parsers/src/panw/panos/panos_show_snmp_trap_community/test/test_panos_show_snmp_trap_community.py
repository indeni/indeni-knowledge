import os
import unittest
from panw.panos.panos_show_snmp_trap_community.panos_show_snmp_trap_community import PanosShowSnmpTrapCommunity
from parser_service.public.action import *

class TestPanosShowSnmpTrapCommunity(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = PanosShowSnmpTrapCommunity()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_snmp_trap_single_community(self):
        data_list = self.parser.parse_file(self.current_dir + '/snmp_trap_single_community.input', {}, {})
        self.assertEqual(1, len(data_list))
        self.assertEqual(1, len(data_list[0].value))
        self.assertEqual('SNMP_test', data_list[0].value[0]['version'])
        self.assertEqual('10.11.95.204', data_list[0].value[0]['manager'])
        self.assertEqual('trap_community', data_list[0].value[0]['community'])

    def test_snmp_trap_multiple_community(self):
        data_list = self.parser.parse_file(self.current_dir + '/snmp_trap_multiple_community.input', {}, {})
        self.assertEqual(1, len(data_list))
        self.assertEqual(2, len(data_list[0].value))
        self.assertEqual('SNMP_test', data_list[0].value[0]['version'])
        self.assertEqual('10.11.95.203', data_list[0].value[0]['manager'])
        self.assertEqual('trap_community', data_list[0].value[0]['community'])
        self.assertEqual('SecondTrapToTest', data_list[0].value[1]['version'])
        self.assertEqual('10.10.10.10', data_list[0].value[1]['manager'])
        self.assertEqual('second_community', data_list[0].value[1]['community'])

    def test_snmp_trap_no_community(self):
        data_list = self.parser.parse_file(self.current_dir + '/snmp_trap_no_community.input', {}, {})
        self.assertEqual(isinstance(data_list,list), True)

    def test_snmp_trap_multicase_community(self):
        data_list = self.parser.parse_file(self.current_dir + '/snmp_trap_multicase_community.input', {}, {})
        self.assertEqual(1, len(data_list))
        self.assertEqual('SNMP Server 1', data_list[0].value[0]['version'])
        self.assertEqual('57.14.4.74', data_list[0].value[0]['manager'])
        self.assertEqual('firewallmon', data_list[0].value[0]['community'])
        self.assertEqual('SNMP Server 2', data_list[0].value[1]['version'])
        self.assertEqual('57.14.4.75', data_list[0].value[1]['manager'])
        self.assertEqual('firewallmon', data_list[0].value[1]['community'])

    def test_snmp_v3(self):
        data_list = self.parser.parse_file(self.current_dir + '/snmp_v3.input', {}, {})
        self.assertEqual(0, len(data_list))


if __name__ == '__main__':
    unittest.main()