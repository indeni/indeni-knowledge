BEGIN {
	tags["name"] = "dp0"
}

#DP dp0:
/^DP/ {
	dataplane = $2
	sub(/:/, "", dataplane)
	tags["name"] = dataplane
}

#        Current Entries: 94
/^\s+Current Entries/ {
	writeDoubleMetric("ssl-decrypt-memory-current", tags, "gauge", $3, "false")  # Converted to new syntax by change_ind_scripts.py script
}

#        Allocated 1928, Freed 1834
/^\s+Allocated/ {
	limit = $2
	sub(/,/, "", limit)
	# Don't output limit when it's 0, since we don't want NearingCapacityTemplateRule to be triggered when usage is 0 and limit is 0
	if (limit != 0) {
		writeDoubleMetric("ssl-decrypt-memory-limit", tags, "gauge", limit, "false")  # Converted to new syntax by change_ind_scripts.py script
	}
}