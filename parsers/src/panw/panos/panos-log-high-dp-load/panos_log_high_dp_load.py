from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class PanosLogHighDpLoad(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        data = helper_methods.parse_data_as_xml(raw_data)
        output = data['response']['result']
        tags = {}
        try:
            entries = output['enable-log-high-dp-load']
            if entries == '':
                value = 0.0
            elif entries['#text'] != 'yes':
                value = 0.0
            else:
                value = 1.0
        except:
            value = 0.0

        self.write_double_metric('pan-log-high-dp-load', tags, 'gauge', value, True, 'Logging enabled for high DP', 'state')

        return self.output

# Test your code Here
# helper_methods.print_list(TestParser().parse_file("/Users/cysk92/indeni-knowledge/parsers/test/panw/panos/panos-log-high-dp-load/logging-disabled-output-empty/input_0_0", {}))
