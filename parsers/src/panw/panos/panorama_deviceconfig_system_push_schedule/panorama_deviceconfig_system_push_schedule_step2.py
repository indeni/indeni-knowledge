from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

# Status values
# PEND : push scheduled and pending to execute 
# FIN : push scheduled and finalized, no more push related with this config
# DISABLED : configred but not enabled

class PanoramaCommitNotScheduledStep2(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data = helper_methods.parse_data_as_xml(raw_data)
            if data and 'result' in data['response']:
                if data['response']['@status'] == "success":
                    tags = {}
                    tags['name'] = dynamic_var['schedule_name']
                    self.write_double_metric('push-scheduled-status', tags, 'gauge', 1 if data['response']['result']['status'] == 'PEND' else 0, True, "Config Push Scheduled", 'state', 'name')
        return self.output



