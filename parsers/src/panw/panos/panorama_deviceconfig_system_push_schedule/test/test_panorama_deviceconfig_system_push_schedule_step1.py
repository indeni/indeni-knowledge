import os
import unittest
from panw.panos.panorama_deviceconfig_system_push_schedule.panorama_deviceconfig_system_push_schedule_step1 import PanoramaCommitNotScheduledStep1
from parser_service.public.action import *

class TestPanoramaCommitNotScheduled(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = PanoramaCommitNotScheduledStep1()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_commit_not_scheduled(self):
        result = self.parser.parse_file(self.current_dir + '/panorama_commit_not_scheduled.input', {}, {})
        self.assertEqual(4, len(result))
        self.assertEqual(result[0].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[0].key, 'schedule_name')
        self.assertEqual(result[0].value, 'daily-test-disabled')
        self.assertEqual(result[1].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[1].key, 'schedule_name')
        self.assertEqual(result[1].value, 'weekly-test-disabled')
        self.assertEqual(result[2].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[2].key, 'schedule_name')
        self.assertEqual(result[2].value, 'daily-test-not-disabled')
        self.assertEqual(result[3].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[3].key, 'schedule_name')
        self.assertEqual(result[3].value, 'weekly-test-not-disabled')

    def test_commit_not_scheduled_empty(self):
        result = self.parser.parse_file(self.current_dir + '/panorama_commit_not_scheduled_empty.input', {}, {})
        self.assertEqual(0, len(result))

if __name__ == '__main__':
    unittest.main()