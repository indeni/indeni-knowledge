import os
import unittest
from panw.panos.panorama_deviceconfig_system_push_schedule.panorama_deviceconfig_system_push_schedule_step2 import PanoramaCommitNotScheduledStep2
from parser_service.public.action import *

class TestPanoramaCommitNotScheduledStep2(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = PanoramaCommitNotScheduledStep2()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_commit_not_scheduled_daily_test_disabled(self):
        result = self.parser.parse_file(self.current_dir + '/panorama_commit_not_scheduled_daily_test_disabled.input', {'schedule_name': 'daily-test-disabled'}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'push-scheduled-status')
        self.assertEqual(result[0].tags['name'], 'daily-test-disabled')
        self.assertEqual(result[0].tags['display-name'], 'Config Push Scheduled')
        self.assertEqual(result[0].tags['im.dstype.displayType'], 'state')
        self.assertEqual(result[0].value, 0)

    def test_commit_not_scheduled_weekly_test_disabled(self):
        result = self.parser.parse_file(self.current_dir + '/panorama_commit_not_scheduled_weekly_test_disabled.input', {'schedule_name': 'weekly-test-disabled'}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'push-scheduled-status')
        self.assertEqual(result[0].tags['name'], 'weekly-test-disabled')
        self.assertEqual(result[0].tags['display-name'], 'Config Push Scheduled')
        self.assertEqual(result[0].tags['im.dstype.displayType'], 'state')
        self.assertEqual(result[0].value, 0)

    def test_commit_not_scheduled_daily_test_not_disabled(self):
        result = self.parser.parse_file(self.current_dir + '/panorama_commit_not_scheduled_daily_test_not_disabled.input', {'schedule_name': 'daily-test-not-disabled'}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'push-scheduled-status')
        self.assertEqual(result[0].tags['name'], 'daily-test-not-disabled')
        self.assertEqual(result[0].tags['display-name'], 'Config Push Scheduled')
        self.assertEqual(result[0].tags['im.dstype.displayType'], 'state')
        self.assertEqual(result[0].value, 1)

    def test_commit_not_scheduled_weekly_test_not_disabled(self):
        result = self.parser.parse_file(self.current_dir + '/panorama_commit_not_scheduled_weekly_test_not_disabled.input', {'schedule_name': 'weekly-test-not-disabled'}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'push-scheduled-status')
        self.assertEqual(result[0].tags['name'], 'weekly-test-not-disabled')
        self.assertEqual(result[0].tags['display-name'], 'Config Push Scheduled')
        self.assertEqual(result[0].tags['im.dstype.displayType'], 'state')
        self.assertEqual(result[0].value, 0)

if __name__ == '__main__':
    unittest.main()