from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class PanoramaCommitNotScheduledStep1(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data = helper_methods.parse_data_as_xml(raw_data)
            if data and 'result' in data['response']:
                if data['response']['@status'] == "success":
                    if data['response']['result']['push-schedule'] is not None:
                        for extracted_data in data['response']['result']['push-schedule']['entry']:
                            self.write_dynamic_variable('schedule_name', extracted_data['@name'])
        return self.output

