from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class ShowSystemLogDbQuota(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        self.debug('start parser')

        # Step 1 : Data Extraction
        data = helper_methods.parse_data_as_list(raw_data, 'show_system_logdb_quota.textfsm')

        # Step 2 : Data Processing
        def scale(value):
            # Function to convert values from disk usage to GB, then from STR to FLOAT
            if 'G' in value:
                new_value = float(value.strip('G'))
            elif 'M' in value:
                new_value = float(value.strip('M')) / 1024
            elif 'K' in value:
                new_value = float(value.strip('K')) / 1048576
            else:
                new_value = 0
            return new_value

        # Splitting the output into two dictionaries
        # This allow us comparing the current used size to quota for all log types
        quotas = dict()
        usage = dict()

        for nested_list in data:
            if nested_list['TYPE'] == 'Quotas':
                # some log types have different name, which can be fixed by Palo Alto Networks, but for now, we will do
                # the conversion here:
                if nested_list['LOG_TYPE'] == 'appstat':
                    nested_list['LOG_TYPE'] = 'appstatdb'
                elif nested_list['LOG_TYPE'] == 'dlp-logs':
                    nested_list['LOG_TYPE'] = 'dlp'
                # Convert RES_SIZE from STR to FLOAT
                quotas[nested_list['LOG_TYPE']] = float(nested_list['RES_SIZE'])
            elif nested_list['TYPE'] == 'Disk usage':
                nested_list['USED_SIZE'] = scale(nested_list['USED_SIZE'])
                usage[nested_list['LOG_TYPE']] = nested_list['USED_SIZE']

        # Step 3 : Data Reporting
        try:
            # For loop to check dict.value and calculating the log utilization for every log type
            for k in usage:
                # calculating the used_percentage and round it to 2 decimal
                usage_percentage = round((usage[k] / quotas[k]) * 100, 2)
                tags = {'name': k}
                self.write_double_metric('logdb-usage', tags, 'gauge', usage_percentage, True, 'Log Usage', 'percentage', 'name')

        except:
            pass

        return self.output
