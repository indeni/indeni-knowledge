from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class WildfireUpdateSchedule(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):

        # Step 1 : Data Extraction
        if raw_data:  # We check if input is not empty
            data = helper_methods.parse_data_as_list(raw_data, 'panos_wildfire_update_schedule.textfsm')
            if data:
                # Step 2 : Data Processing
                schedule_v = data[0]['SCHEDULE']
                action_v = data[0]['ACTION']

                # Step 3 : Data Reporting
                output = [{'Action': action_v, 'Schedule': schedule_v}]
                tags = {'name': 'Wildfire Update Schedule'}
                self.write_complex_metric_object_array('panos-wildfire-update-schedule', tags, output, True, 'Wildfire Update Schedule')
        return self.output
