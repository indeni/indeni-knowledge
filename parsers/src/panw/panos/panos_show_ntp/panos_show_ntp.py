from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class PanosShowNtp(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data = helper_methods.parse_data_as_xml(raw_data)
            if data and 'result' in data['response']:
                if data['response']['@status'] == "success":
                    if 'LOCAL' in raw_data or 'TIMEOUT' in raw_data:
                        self.write_double_metric('ntp-server-state', {}, 'gauge', 0, False)
                    else:
                        parsed_data = helper_methods.parse_data_as_list(data['response']['result']['ntp-server-1']['status'], 'panos_show_ntp.textfsm')
                        if parsed_data:
                            entry = {}
                            data_list = []
                            entry['status'] = data['response']['result']['ntp-server-1']['status']
                            entry['auth'] = data['response']['result']['ntp-server-1']['authentication-type']
                            entry['reachable'] = data['response']['result']['ntp-server-1']['reachable']
                            entry['name'] = data['response']['result']['ntp-server-1']['name']
                            entry['synched'] = data['response']['result']['synched']
                            data_list.append(entry)
                            self.write_double_metric('ntp-server-state', {}, 'gauge', 1 if data['response']['result']['ntp-server-1']['status'] in ['synched','available'] else 0, False)
        return self.output