import os
import unittest
from panw.panos.panos_show_ntp.panos_show_ntp import PanosShowNtp
from parser_service.public.action import *

class TestPanosShowNtp(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = PanosShowNtp()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))    
    
    def test_ntp_enabled(self):
        result = self.parser.parse_file(self.current_dir + '/test_show_ntp.input', {}, {})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].ds_type, 'gauge')
        self.assertEqual(result[0].name, 'ntp-server-state')
        self.assertEqual(result[0].value, 1)

    def test_ntp_enabled_local(self):
        result = self.parser.parse_file(self.current_dir + '/test_show_ntp_local.input', {}, {})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].ds_type, 'gauge')
        self.assertEqual(result[0].name, 'ntp-server-state')
        self.assertEqual(result[0].value, 0)

    def test_ntp_enabled_timeout(self):
        result = self.parser.parse_file(self.current_dir + '/test_show_ntp_timeout.input', {}, {})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].ds_type, 'gauge')
        self.assertEqual(result[0].name, 'ntp-server-state')
        self.assertEqual(result[0].value, 0)


if __name__ == '__main__':
    unittest.main()