from parser_service.public.base_parser import BaseParser
from parser_service.public import helper_methods

class PanosDynamicAddressGroupReachesCapacity(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data = helper_methods.parse_data_as_list(raw_data, 'panos_dynamic_address_group_reaches_capacity.textfsm')
            if data:
                if '0x' in data[0]['max_number']: #if returned hexa
                    dynamic_address_max_number = int(data[0]['max_number'], 16)
                else: #if returned int
                    dynamic_address_max_number = int(data[0]['max_number'])
                self.write_double_metric('dynamic-address-max-number', {}, 'gauge', dynamic_address_max_number, False)
                if data[0]['current_number']:
                    self.write_double_metric('dynamic-address-current-number', {}, 'gauge', int(data[0]['current_number']), False)
        return self.output