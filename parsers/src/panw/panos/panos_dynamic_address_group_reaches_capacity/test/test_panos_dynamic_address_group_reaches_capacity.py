import os
import unittest

from panw.panos.panos_dynamic_address_group_reaches_capacity.panos_dynamic_address_group_reaches_capacity import PanosDynamicAddressGroupReachesCapacity
from parser_service.public.action import *

class TestPanosDynamicAddressGroupReachesCapacity(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = PanosDynamicAddressGroupReachesCapacity()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_dynamic_address_group_reaches_capacity(self):
        result = self.parser.parse_file(self.current_dir + '/panos_dynamic_address_group_reaches_capacity.input', {}, {})
        self.assertEqual(2, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'dynamic-address-max-number')
        self.assertEqual(result[0].value, 2500)
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].name, 'dynamic-address-current-number')
        self.assertEqual(result[1].value, 15)

    def test_dynamic_address_group_reaches_capacity_no_addresses(self):
        result = self.parser.parse_file(self.current_dir + '/panos_dynamic_address_group_reaches_capacity_no_addresses.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'dynamic-address-max-number')
        self.assertEqual(result[0].value, 2500)

if __name__ == '__main__':
    unittest.main()