import os
import unittest

from panw.panos.show_system_state.show_system_state import RestSystemState
from parser_service.public.action import WriteDoubleMetric
 
 
class TestRestSystemState(unittest.TestCase):
 
    def setUp(self):
        # Arrange
        self.parser = RestSystemState()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))
 
    def test_valid_input(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/valid_input_vsys_1', {}, {})
        # Assert
        self.assertEqual(1, len(result))
        self.assertTrue(isinstance(result[0], WriteDoubleMetric))
        self.assertEqual('vsys', result[0].tags['name'])
        self.assertEqual(1, result[0].value)

    def test_invalid_input(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/invalid_input_empty', {}, {})
        # Assert
        self.assertEqual(0, len(result))
 
 
if __name__ == '__main__':
    unittest.main()
