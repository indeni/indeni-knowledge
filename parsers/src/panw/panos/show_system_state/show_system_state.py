from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class RestSystemState(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            xml_data = helper_methods.parse_data_as_xml(raw_data)

            if xml_data and xml_data['response']['@status'] == 'success':
                data = helper_methods.parse_data_as_list(xml_data['response']['result'], 'paloalto_panos_show_system_state_filter_pretty_cfg_general.textfsm')
                for limits in data:
                    if limits['cfg_general_licensed_vsys']:
                        tags = {'name': 'vsys'}
                        self.write_double_metric('license-elements-limit', tags, 'gauge', int(limits['cfg_general_licensed_vsys'], 16), True, 'Licenses - Limit', 'number', 'name')

        return self.output
