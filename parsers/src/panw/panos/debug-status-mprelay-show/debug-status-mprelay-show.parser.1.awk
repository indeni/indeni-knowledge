BEGIN {
    # For each flag this command supports, we list what a "normal" value is
    # Do not consolidate into show system state debugs because of all the dpx.debug matches that would be req'd -PA-Brad 2/6/2018
    normal["sw.mprelay.s1.cp.debug"] = "info"
    normal["sw.mprelay.s1.dp0.debug"] = "info"
    normal["sw.mprelay.s1.dp1.debug"] = "info"
    normal["sw.mprelay.s1.dp2.debug"] = "info"
    normal["sw.mprelay.s1.dp3.debug"] = "info"
    normal["sw.mprelay.s1.dp4.debug"] = "info"
    normal["sw.mprelay.s1.dp5.debug"] = "info"
    normal["sw.mprelay.s1.dp6.debug"] = "info"
    normal["sw.mprelay.s1.dp7.debug"] = "info"
    normal["sw.mprelay.s1.dp8.debug"] = "info"
    normal["sw.mprelay.s1.dp9.debug"] = "info"
}
# Debug status lines look like this:
# Note that the value can be all kinds of things depending on what the debug element is
#sw.ikedaemon.debug.pcap: False
/[a-zA-Z\.\'\-\s]+\:\s*(\S)+/ {
    # Sometimes the output can be in JSON format like "{'sw.mprelay.s1.dp5.debug': 'info'}"
    gsub(/(\{|\}|')/, "", $0)
    flag = $1
    sub(/:$/, "", flag)
    debugValue = $2
    #debug:info
    sub(/.+:/, "", debugValue)
    if (flag in normal) {
        debugtags["name"] = flag

        if ((normal[flag] == debugValue) || debugValue == "off") {
            state = 0
        } else {
            state = 1
        }
        writeDoubleMetric("debug-status", debugtags, "gauge", state, "false")  # Converted to new syntax by change_ind_scripts.py script
    }
}