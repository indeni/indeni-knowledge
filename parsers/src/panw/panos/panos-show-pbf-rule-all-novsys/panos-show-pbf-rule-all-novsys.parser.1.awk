# Status will look like this:
# Rule       ID   Rule State Action   Egress IF/VSYS  NextHop      NextHop Status
# ---------- ---- ---------- -------- --------------  ------------ --------------
# test_PBF   1    Active     Forward  ethernet1/3     10.66.24.1   DOWN
# HTTP Retur 1    Active     Forward  ethernet1/2.102 15.0.2.1     UP
# SSH with m 2    Disabled   Forward  ethernet1/2.102 15.0.2.1     DOWN

NF < 2 {
   next
}

$(NF-1) ~ /[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}/ {

    state_pos = match($0, / +(Active|Disabled)/)
    rule = substr($0,1,state_pos-1)
    tags["name"] = rule

    state = 1
    if ($NF == "DOWN") {
        state = 0
    }
    writeDoubleMetric("pan-pbf-rule-status-novsys", tags, "gauge", state, "true", "PBF Rule", "state", "name")
}