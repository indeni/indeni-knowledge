from parser_service.public.base_parser import BaseParser
from parser_service.public import helper_methods


class PanosShowInterfaceManagement(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data = helper_methods.parse_data_as_xml(raw_data)
            if data and 'result' in data['response']:
                if data['response']['@status'] == "success":
                    if data['response']['result']['info']['ip']:
                        if data['response']['result']['info']['ip'] not in ['N/A', 'unknown']:
                            tags = {}
                            tags['name'] = data['response']['result']['info']['name']
                            tags['im.identity-tags'] = 'name'
                            self.write_complex_metric_string('network-interface-ipv4-address', tags, data['response']['result']['info']['ip'], True,'Network Interfaces - IPv4 Address')
                            self.write_complex_metric_string('network-interface-ipv4-subnet', tags, data['response']['result']['info']['netmask'], True,'Network Interfaces - IPv4 Netmask',)
        return self.output