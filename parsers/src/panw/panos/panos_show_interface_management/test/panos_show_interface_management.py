import os
import unittest
from panw.panos.panos_show_interface_management.panos_show_interface_management import PanosShowInterfaceManagement
from parser_service.public.action import *

class TestPanosShowInterfaceManagement(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = PanosShowInterfaceManagement()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_int_management(self):
        data = self.parser.parse_file(self.current_dir + '/int_management.input', {}, {})
        self.assertEqual(2, len(data))
        self.assertEqual(data[0].action_type, 'WriteComplexMetric')
        self.assertEqual(data[0].name, 'network-interface-ipv4-address')
        self.assertEqual(data[0].tags['name'], 'Management Interface')
        self.assertEqual(data[0].tags['display-name'], 'Network Interfaces - IPv4 Address')
        self.assertEqual(data[0].value['value'], '10.11.95.6')

        self.assertEqual(data[1].action_type, 'WriteComplexMetric')
        self.assertEqual(data[1].name, 'network-interface-ipv4-subnet')
        self.assertEqual(data[1].tags['name'], 'Management Interface')
        self.assertEqual(data[1].tags['display-name'], 'Network Interfaces - IPv4 Netmask')
        self.assertEqual(data[1].value['value'], '255.255.255.0')


if __name__ == '__main__':
    unittest.main()