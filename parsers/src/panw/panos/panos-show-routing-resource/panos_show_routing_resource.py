from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser
import xml.etree.ElementTree as ET

class RouteSummary(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            root = ET.fromstring(raw_data)
            try:
                routes_limit_xpath = root.find('result/entry/All-Routes/limit')
                tags = {'name': 'routes-limit'}
                if routes_limit_xpath is not None:
                    routes_limit = int(routes_limit_xpath .text)
                    self.write_double_metric('routes-limit', tags, 'gauge', routes_limit, True, 'Routes - Limit')

                route_usage_xpath = root.find('result/entry/All-Routes/total')
                if route_usage_xpath is not None:
                    route_usage = int(route_usage_xpath .text)
                    self.write_double_metric('routes-usage', tags, 'gauge', route_usage, True, 'Routes - Count')

            except Exception as e:
                pass
        return self.output
