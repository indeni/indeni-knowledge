#Number of sessions that match filter: 36839
/^Number of sessions that match filter:/ {
    session_count_tags["interface"] = dynamic("interface_name")
    writeDoubleMetric("network-interface-session-count", session_count_tags, "gauge", $NF, "true", "Network Interface Session Count", "number", "interface")  # Converted to new syntax by change_ind_scripts.py script
}