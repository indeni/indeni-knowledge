BEGIN {
    linesmatched = 0
}

function addLine(filename, line, info) {
    # Grab the log's timestamp
    logtimestamp = 0

    if (match($0, /([0-9]{4})-([0-9]){2}-([0-9]){2} ([0-9]){2}:([0-9]){2}:([0-9]){2}/)) {
        _year = substr($0, RSTART, 4)
        _month = substr($0, RSTART+5, 2)
        _day = substr($0, RSTART+8, 2)
        _hh = substr($0, RSTART+11, 2)
        _mm = substr($0, RSTART+14, 2)
        _ss = substr($0, RSTART+17, 2)
        logtimestamp = datetime(_year, _month, _day, _hh, _mm, _ss)
    } else {
        logtimestamp = now()
    }

    # Need to be careful we don't flood the server with a massive metric
    if ((now() - logtimestamp) <= 3600*24*7) {
        linesmatched += 1
        logs[linesmatched, "line"] = filename ": " line
        logs[linesmatched, "info"] = info
        logstimestamps[linesmatched] = logtimestamp

        # We limit the logs array's size, so once we've hit a certain number of
        # matched lines we work to ensure we always remove the oldest
        if (linesmatched > 50) {
            # Remove oldest log
            oldest = now()
            logtoremove = linesmatched
            for (logid in logstimestamps) {
                timestamp = logstimestamps[logid]
                if (timestamp < oldest) {
                    oldest = timestamp
                    logtoremove = logid
                }
            }

            delete logs[logtoremove, "line"]
            delete logs[logtoremove, "info"]
            delete logstimestamps[logtoremove]
        }
    }
}

/rror.*ldap.*bind/ {
    addLine("useridd.log", $0, "Review https://live.paloaltonetworks.com/t5/Management-Articles/Useful-CLI-Commands-to-Troubleshoot-LDAP-Connection/ta-p/57217")
}

/Alarm.*True/ {
    addLine("ehmon.log", $0, "Review https://live.paloaltonetworks.com/t5/Learning-Articles/CLI-Commands-to-View-Hardware-Status/ta-p/61027")
}

/log.*purged/ {
    addLine("ms.log", $0, "Review https://live.paloaltonetworks.com/t5/Management-Articles/Difference-Between-Data-in-Logs-and-Predefined-Reports/ta-p/56869")
}

/Unable to enable cfgagent/{
    addLine("pan_dhcpd.log", $0, "Review https://live.paloaltonetworks.com/t5/Management-Articles/CLI-Commands-to-Troubleshoot-DHCP/ta-p/59416")
}

/Due to negotiation timeout/ {
    addLine("ikemgr.log", $0, "Review https://live.paloaltonetworks.com/t5/Management-Articles/IPSec-VPN-Error-IKE-Phase-2-Negotiation-is-Failed-as-Initiator/ta-p/60725")
}

/pca954x_select/ {
    addLine("controlplane-console-output.log", $0, "Review https://live.paloaltonetworks.com/t5/Management-Articles/Palo-Alto-Networks-PA-5000-Series-Power-Supply-Unit-Error/ta-p/54124")
}

/Unable to save MP Cache/ {
    addLine("devsrvr.log", $0, "Review https://live.paloaltonetworks.com/t5/Management-Articles/BrightCloud-Error-in-devsrvr-log-Unable-to-save-MP-Cache/ta-p/54166")
}

/Failed to purge old uploaded files/ {
    addLine("ms.log", $0, "Review https://live.paloaltonetworks.com/t5/Management-Articles/Software-Download-Error-Failed-to-download-due-to-server-error/ta-p/57458")
}

/Error: _pan_email/ {
    addLine("ms.log", $0, "Review https://live.paloaltonetworks.com/t5/Learning-Articles/How-to-Send-a-Test-Email-to-Verify-Email-Profile-Settings/ta-p/60956")
}

/Error:Domain Name Invalid/ {
    addLine("ha_agent.log", $0, "Review https://live.paloaltonetworks.com/t5/Management-Articles/HA-Sync-Failure-Due-to-Inconsistent-Management-Settings/ta-p/59260")
}

/pan_comm_get_tcp_conn_gen/ {
    addLine("ms.log", $0, "Review https://live.paloaltonetworks.com/t5/tkb/articleprintpage/tkb-id/Management-TKB/article-id/514")
}

/pan_ssl_load_tcas/ {
    addLine("supervisor.log", $0, "Review https://live.paloaltonetworks.com/t5/Management-Articles/Wrong-Certificate-used-when-SSL-Decryption-is-enabled/ta-p/58710")
}

/Error downloading latest URL database/ {
    addLine("pan_bc_download.log", $0, "Review https://live.paloaltonetworks.com/t5/Management-Articles/PAN-DB-URL-Activation-Changes-to-Not-Active-after-Refresh/ta-p/53783")
}

/mgmtsrvr insync: NO/ {
    addLine("ha_agent.log", $0, "Review https://live.paloaltonetworks.com/t5/Management-Articles/HA-Active-Passive-Unable-to-Push-Config-to-Peer/ta-p/60850")
}

/Message from socket exceeds internal buffer/ {
    addLine("useridd.log", $0, "Review https://live.paloaltonetworks.com/t5/tkb/articleprintpage/tkb-id/Management-TKB/article-id/1584")
}

/enabling rate limiting for/ {
    addLine("useridd.log", $0, "Review https://live.paloaltonetworks.com/t5/Management-Articles/Unknown-IP-Rate-Limit-Mitigation-for-User-ID-Mappings/ta-p/58581")
}

/Firewall unable.*for.*service/ {
    addLine("devsrvr.log", $0, "Review https://live.paloaltonetworks.com/t5/tkb/articleprintpage/tkb-id/Management-TKB/article-id/3627")
}

/error while loading serial number/ {
    addLine("ms.log", $0, "Review https://live.paloaltonetworks.com/t5/tkb/articleprintpage/tkb-id/Management-TKB/article-id/896")
}

/pan_ocsp_parse_response/ {
    addLine("sslmgr.log", $0, "Review https://live.paloaltonetworks.com/t5/tkb/articleprintpage/tkb-id/ConfigurationArticles/article-id/973")
}

/Error reading dnsproxy persistent cache xml file/ {
    addLine("dnsproxyd.log", $0, "Contact Palo Alto Networks TAC and refer to this log line.")
}

/Content time below threshold/ {
    addLine("ms.log", $0, "Review https://live.paloaltonetworks.com/t5/Management-Articles/Dynamic-updates-scheduled-with-a-threshold-set-but-are-never-or/ta-p/65952")
}

/unsuccessful authentication attempts threshold reached/ {
    addLine("authd.log", $0, "Review https://live.paloaltonetworks.com/t5/Management-Articles/How-to-Unlock-Users-on-the-Lock-List-for-Failed-Maximum/ta-p/66195")
}

/Unexpected error from radius server/ {
    addLine("authd.log", $0, "Review https://live.paloaltonetworks.com/t5/Management-Articles/How-to-Identify-Secret-Key-Mismatch-Between-Palo-Alto-Networks/ta-p/54612")
}

/Template lab mismatch/ {
    addLine("ms.log", $0, "Review https://live.paloaltonetworks.com/t5/Management-Articles/Panorama-Error-template-incompatible-Due-to-Multi-VSYS/ta-p/60881")
}

#Dec 31 10:20:19 INFO: mgmtsrvr: User restart reason - Virtual memory limit exceeded (2575744 > 2560000)
/Virtual memory limit exceeded/ {
    addLine("masterd.log", $0, "Review https://live.paloaltonetworks.com/t5/Management-Articles/Error-mgmtsrvr-virtual-memory-limit-exceeded-restarting/ta-p/65992")
}

END {
    writeComplexMetricObjectArray("interesting-logs", t, logs, "false")  # Converted to new syntax by change_ind_scripts.py script
}