from parser_service.public.base_parser import BaseParser
from parser_service.public import helper_methods

class PanosShowConfigMerge(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data = helper_methods.parse_data_as_xml(raw_data)
            try:
                web_timeout = int(data['response']['result']['config']['devices']['entry']['deviceconfig']['setting']['management']['idle-timeout'])
                self.write_double_metric('web-timeout', {}, 'gauge', web_timeout * 60000, False)
                self.write_double_metric('missing-web-timeout', {}, 'gauge', 1, False)
            except:
                self.write_double_metric('missing-web-timeout', {}, 'gauge', 0, False)

        return self.output