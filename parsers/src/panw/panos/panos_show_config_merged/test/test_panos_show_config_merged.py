import os
import unittest
from panw.panos.panos_show_config_merged.panos_show_config_merged import PanosShowConfigMerge
from parser_service.public.action import *

class TestPanosShowConfigMerge(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = PanosShowConfigMerge()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_idle_timeout(self):
        result = self.parser.parse_file(self.current_dir + '/panos_show_config_merged_idle_timeout.input', {}, {})
        self.assertEqual(2, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'web-timeout')
        self.assertEqual(result[0].value, 600000)
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].name, 'missing-web-timeout')
        self.assertEqual(result[1].value, 1)

    def test_timeout_mismatch(self):
        result = self.parser.parse_file(self.current_dir + '/panos_show_config_merged_timeout_mismatch.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'missing-web-timeout')
        self.assertEqual(result[0].value, 0)

    def test_missing_timeout(self):
        result = self.parser.parse_file(self.current_dir + '/panos_show_config_merged_missing_idle_timeout.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'missing-web-timeout')
        self.assertEqual(result[0].value, 0)

if __name__ == '__main__':
    unittest.main()