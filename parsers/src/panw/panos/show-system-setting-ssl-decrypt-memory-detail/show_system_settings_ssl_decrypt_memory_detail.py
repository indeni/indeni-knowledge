from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class ShowSystemSSLResourcesParser(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        data = helper_methods.parse_data_as_object(raw_data, 'show_system_settings_ssl_decrypt_memory_detail.textfsm')
        if data:
            total_k = int(data['total_chunks']) * int(data['page_size']) / 1024
            used_k = int(data['current_usage']) / 1024

            mem_tags = {'name': 'SSL Proxy Memory', 'resource-metric': 'true'}
            self.write_double_metric('memory-free-kbytes', mem_tags, 'gauge', total_k - used_k, True, 'Memory - Free', 'kilobytes', 'name')
            self.write_double_metric('memory-total-kbytes', mem_tags, 'gauge', total_k, True, 'Memory - Total', 'kilobytes', 'name')
            self.write_double_metric('memory-usage', mem_tags, 'gauge', used_k / total_k * 100, True, 'Memory Usage', 'percentage', 'name')

        return self.output
