import os
import unittest
from panw.panos.panos_show_interface_all.panos_show_interface_all import PanosShowInterfaceAll
from parser_service.public.action import *

class TestPanosShowInterfaceAll(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = PanosShowInterfaceAll()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_panorama(self):
        data = self.parser.parse_file(self.current_dir + '/panorama.input', {}, {'product': 'panorama'})
        self.assertEqual(2, len(data))
        self.assertEqual(data[0].action_type, 'WriteComplexMetric')
        self.assertEqual(data[0].name, 'network-interface-ipv4-address')
        self.assertEqual(data[0].tags['name'], 'Management Interface')
        self.assertEqual(data[0].tags['display-name'], 'Network Interfaces - IPv4 Address')
        self.assertEqual(data[0].value['value'], '10.11.95.11')

        self.assertEqual(data[1].action_type, 'WriteComplexMetric')
        self.assertEqual(data[1].name, 'network-interface-ipv4-subnet')
        self.assertEqual(data[1].tags['name'], 'Management Interface')
        self.assertEqual(data[1].tags['display-name'], 'Network Interfaces - IPv4 Netmask')
        self.assertEqual(data[1].value['value'], '255.255.255.0')

    def test_physical_subinterfaces_tunnel(self):
        data = self.parser.parse_file(self.current_dir + '/physical_subinterfaces_tunnel.input', {}, {'product': 'firewall'})
        self.assertEqual(12, len(data))

        self.assertEqual(data[0].action_type, 'WriteComplexMetric')
        self.assertEqual(data[0].name, 'network-interface-ipv4-address')
        self.assertEqual(data[0].tags['name'], 'ethernet1/1')
        self.assertEqual(data[0].tags['display-name'], 'Network Interfaces - IPv4 Address')
        self.assertEqual(data[0].value['value'], '10.100.100.2')

        self.assertEqual(data[1].action_type, 'WriteComplexMetric')
        self.assertEqual(data[1].name, 'network-interface-ipv4-subnet')
        self.assertEqual(data[1].tags['name'], 'ethernet1/1')
        self.assertEqual(data[1].tags['display-name'], 'Network Interfaces - IPv4 Netmask')
        self.assertEqual(data[1].value['value'], '255.255.255.0')

        self.assertEqual(data[2].action_type, 'WriteComplexMetric')
        self.assertEqual(data[2].name, 'network-interface-ipv4-address')
        self.assertEqual(data[2].tags['name'], 'ethernet1/3')
        self.assertEqual(data[2].tags['display-name'], 'Network Interfaces - IPv4 Address')
        self.assertEqual(data[2].value['value'], '10.11.80.229')

        self.assertEqual(data[3].action_type, 'WriteComplexMetric')
        self.assertEqual(data[3].name, 'network-interface-ipv4-subnet')
        self.assertEqual(data[3].tags['name'], 'ethernet1/3')
        self.assertEqual(data[3].tags['display-name'], 'Network Interfaces - IPv4 Netmask')
        self.assertEqual(data[3].value['value'], '255.255.255.255')

        self.assertEqual(data[4].action_type, 'WriteComplexMetric')
        self.assertEqual(data[4].name, 'network-interface-ipv4-address')
        self.assertEqual(data[4].tags['name'], 'ethernet1/4.200')
        self.assertEqual(data[4].tags['display-name'], 'Network Interfaces - IPv4 Address')
        self.assertEqual(data[4].value['value'], '10.200.101.1')

        self.assertEqual(data[5].action_type, 'WriteComplexMetric')
        self.assertEqual(data[5].name, 'network-interface-ipv4-subnet')
        self.assertEqual(data[5].tags['name'], 'ethernet1/4.200')
        self.assertEqual(data[5].tags['display-name'], 'Network Interfaces - IPv4 Netmask')
        self.assertEqual(data[5].value['value'], '255.255.255.0')

        self.assertEqual(data[6].action_type, 'WriteComplexMetric')
        self.assertEqual(data[6].name, 'network-interface-ipv4-address')
        self.assertEqual(data[6].tags['name'], 'ethernet1/5')
        self.assertEqual(data[6].tags['display-name'], 'Network Interfaces - IPv4 Address')
        self.assertEqual(data[6].value['value'], '10.0.102.2')

        self.assertEqual(data[7].action_type, 'WriteComplexMetric')
        self.assertEqual(data[7].name, 'network-interface-ipv4-subnet')
        self.assertEqual(data[7].tags['name'], 'ethernet1/5')
        self.assertEqual(data[7].tags['display-name'], 'Network Interfaces - IPv4 Netmask')
        self.assertEqual(data[7].value['value'], '255.255.255.0')

        self.assertEqual(data[8].action_type, 'WriteComplexMetric')
        self.assertEqual(data[8].name, 'network-interface-ipv4-address')
        self.assertEqual(data[8].tags['name'], 'ethernet1/6')
        self.assertEqual(data[8].tags['display-name'], 'Network Interfaces - IPv4 Address')
        self.assertEqual(data[8].value['value'], '10.0.103.2')

        self.assertEqual(data[9].action_type, 'WriteComplexMetric')
        self.assertEqual(data[9].name, 'network-interface-ipv4-subnet')
        self.assertEqual(data[9].tags['name'], 'ethernet1/6')
        self.assertEqual(data[9].tags['display-name'], 'Network Interfaces - IPv4 Netmask')
        self.assertEqual(data[9].value['value'], '255.255.255.0')

        self.assertEqual(data[10].action_type, 'WriteComplexMetric')
        self.assertEqual(data[10].name, 'network-interface-ipv4-address')
        self.assertEqual(data[10].tags['name'], 'tunnel.2')
        self.assertEqual(data[10].tags['display-name'], 'Network Interfaces - IPv4 Address')
        self.assertEqual(data[10].value['value'], '172.16.0.1')

        self.assertEqual(data[11].action_type, 'WriteComplexMetric')
        self.assertEqual(data[11].name, 'network-interface-ipv4-subnet')
        self.assertEqual(data[11].tags['name'], 'tunnel.2')
        self.assertEqual(data[11].tags['display-name'], 'Network Interfaces - IPv4 Netmask')
        self.assertEqual(data[11].value['value'], '255.255.255.0')


if __name__ == '__main__':
    unittest.main()