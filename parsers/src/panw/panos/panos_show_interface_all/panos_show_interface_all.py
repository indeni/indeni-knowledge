from parser_service.public.base_parser import BaseParser
from parser_service.public import helper_methods

def cidr_to_netmask(cidr):
    cidr = int(cidr)
    mask = (0xffffffff >> (32 - cidr)) << (32 - cidr)
    return (str( (0xff000000 & mask) >> 24)   + '.' +
        str( (0x00ff0000 & mask) >> 16)   + '.' +
        str( (0x0000ff00 & mask) >> 8)    + '.' +
        str( (0x000000ff & mask)))

class PanosShowInterfaceAll(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data = helper_methods.parse_data_as_xml(raw_data)
            if data and 'result' in data['response']:
                if data['response']['@status'] == "success":
                    if device_tags.get('product') == 'firewall':
                        for interface in data['response']['result']['ifnet']['entry']:
                            if interface['ip'] != 'N/A':
                                tags = {}
                                tags['name'] = interface['name']
                                tags['im.identity-tags'] = 'name'
                                self.write_complex_metric_string('network-interface-ipv4-address', tags, interface['ip'].split('/')[0], True,'Network Interfaces - IPv4 Address')
                                self.write_complex_metric_string('network-interface-ipv4-subnet', tags, cidr_to_netmask(interface['ip'].split('/')[1]), True,'Network Interfaces - IPv4 Netmask',)
                    if device_tags.get('product') == 'panorama':
                        for interface in data['response']['result']:
                            if data['response']['result'][interface]['ip']:
                                if data['response']['result'][interface]['ip'] not in ['N/A', 'unknown']:
                                    tags = {}
                                    tags['name'] = data['response']['result'][interface]['name']
                                    tags['im.identity-tags'] = 'name'
                                    self.write_complex_metric_string('network-interface-ipv4-address', tags, data['response']['result'][interface]['ip'], True,'Network Interfaces - IPv4 Address')
                                    self.write_complex_metric_string('network-interface-ipv4-subnet', tags, data['response']['result'][interface]['netmask'], True,'Network Interfaces - IPv4 Netmask',)
        return self.output