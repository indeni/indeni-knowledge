#[ 0] Packet Buffers            :   262062/262144   0x8000000020c00000
#[ 1] Work Queue Entries        :   491466/491520   0x8000000410000000
/^DP / {
    dp = $2
    sub(/\:/, "", dp)
}

#[ 0] Packet Buffers            :    57343/57344    0x7f0002005d00
#[10] SML VM Vchecks            :    65536/65536    0x7f005c226ef8
/^\[[0-9 ]+\]/ {
    # Get the pool name
    line = $0
    sub(/\[.*?\]\s*/, "", line)
    sub(/\:.*/, "", line)
    sub(/[ ]$/, "", line)
    poolname = line

    # Get pool utilization
    util = $(NF-1)
    split(util, util_parts, "/")
    limit = util_parts[2]
    used = limit - util_parts[1]

    pooltags["name"] = dp "-" poolname
    writeDoubleMetric("dataplane-pool-used", pooltags, "gauge", used, "false")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("dataplane-pool-limit", pooltags, "gauge", limit, "false")  # Converted to new syntax by change_ind_scripts.py script
}