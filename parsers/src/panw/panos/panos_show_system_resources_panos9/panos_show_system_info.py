from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class PanosShowSystemInfo(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data = helper_methods.parse_data_as_xml(raw_data)
            if data and 'result' in data['response']:
                if data['response']['@status'] == "success":
                    mem_limit = data['response']['result']['system'].get('vm-cap-tier')
                    if mem_limit is not None and mem_limit !='unknown':
                        if mem_limit.startswith('T'):
                            mem_limit_k = float(mem_limit.split('-')[1].replace('GB', '')) * 1000000
                        else:
                            mem_limit_k = float(mem_limit.split(' ')[0]) * 1000000
                        self.write_dynamic_variable('mem_limit_k', str(mem_limit_k))
        return self.output
