from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

units = {
    'G': 1000000,
    'M': 1000,
    'K': 1,
}

class ResourcesParser(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            xml_data = helper_methods.parse_data_as_xml(raw_data)
            if xml_data and 'result' in xml_data['response']:
                if xml_data['response']['@status'] == "success":
                    command_out = xml_data['response']['result']

                data = helper_methods.parse_data_as_object(command_out, 'panos_show_system_resources_panos9_zombie.textfsm')
                if data:
                    if data['zombie']:
                        self.write_double_metric('tasks-zombies', {}, 'gauge', float(data['zombie']), True, "Zombie processes", 'number', None)

                data = helper_methods.parse_data_as_object(command_out, 'panos-show-system-resources-panos9.textfsm')
                if data:
                    mem_tags = {'name': 'PA firewall management plane', 'resource-metric': 'true'}

                    if data['free_k']:
                        self.write_double_metric('memory-free-kbytes', mem_tags, 'gauge', float(data['free_k'])*int(units[data['units']]), True, "Memory - Free", "kilobytes", "name")

                    if data['total_k']:
                        self.write_double_metric('memory-total-kbytes', mem_tags, 'gauge', float(data['total_k'])*int(units[data['units']]), True, "Memory - Total", "kilobytes", "name")

                    if data['used_k'] and data['total_k']:
                        if dynamic_var.get('mem_limit_k'):
                            self.write_double_metric('memory-lic-limit-kbytes', mem_tags, 'gauge', float(dynamic_var['mem_limit_k']), True, "Memory - License Limit", "kilobytes", "name")
                            self.write_double_metric('memory-usage', mem_tags, 'gauge', float(data['used_k']) / float(dynamic_var['mem_limit_k']) * 100, True, "Memory Usage", "percentage", "name")
                        else:
                            self.write_double_metric('memory-usage', mem_tags, 'gauge', float(data['used_k']) / float(data['total_k']) * 100, True, "Memory Usage", "percentage", "name")

                    data = helper_methods.parse_data_as_list(raw_data, 'panos_show_system_resources_panos9_processes.textfsm')
                    if data:
                        ntpd_found = False
                        for process in data:
                            if process.get('command') == 'ntpd':
                                self.write_double_metric('process-state-ntpd-status', {}, 'gauge', 1, False)
                                ntpd_found = True
                        if not ntpd_found:
                            self.write_double_metric('process-state-ntpd-status', {}, 'gauge', 0, False)
        return self.output
