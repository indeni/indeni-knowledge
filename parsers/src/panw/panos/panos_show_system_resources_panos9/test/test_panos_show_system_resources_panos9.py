import os
import unittest
from panw.panos.panos_show_system_resources_panos9.panos_show_system_resources_panos9 import ResourcesParser
from panw.panos.panos_show_system_resources_panos9.panos_show_system_info import PanosShowSystemInfo
from parser_service.public.action import *
class TestResourcesParser(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser_step1 = PanosShowSystemInfo()
        self.parser_step2 = ResourcesParser()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_show_system_info(self):
        # Act
        result = self.parser_step1.parse_file(self.current_dir + '/show_system_info.input', {}, {})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].action_type,'WriteDynamicVariable')
        self.assertEqual(result[0].key,'mem_limit_k')
        self.assertEqual(result[0].value, '6500000.0')

    def test_show_system_info_with_tier(self):
        # Act
        result = self.parser_step1.parse_file(self.current_dir + '/show_system_info_with_tier.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[0].key, 'mem_limit_k')
        self.assertEqual(result[0].value, '6500000.0')

    def test_show_system_info_no_lic_limit(self):
        # Act
        result = self.parser_step1.parse_file(self.current_dir + '/show_system_info_no_lic_limit.input', {}, {})
        self.assertEqual(0,len(result))

    def test_show_system_info_vm_cap_tier_unknown(self):
        # Act
        result = self.parser_step1.parse_file(self.current_dir + '/show_system_info_vm_cap_tier_unknown.input', {}, {})
        self.assertEqual(0,len(result))

    def test_memory_limitation_by_lic(self):
        result = self.parser_step2.parse_file(self.current_dir + '/test_normal_mp_utilization.input', {'mem_limit_k':6500000.0}, {})
        self.assertEqual(6,len(result))
        self.assertEqual(result[0].name,'tasks-zombies')
        self.assertEqual(result[0].tags['display-name'], 'Zombie processes')
        self.assertEqual(result[0].value, 123)
        self.assertEqual(result[1].tags['name'],'PA firewall management plane')
        self.assertEqual(result[1].tags['resource-metric'], 'true')
        self.assertEqual(result[1].tags['display-name'], 'Memory - Free')
        self.assertEqual(result[1].tags['im.dstype.displayType'], 'kilobytes')
        self.assertEqual(result[1].tags['im.identity-tags'], 'name')
        self.assertEqual(result[1].value, 449272)
        self.assertEqual(result[2].tags['name'],'PA firewall management plane')
        self.assertEqual(result[2].tags['resource-metric'], 'true')
        self.assertEqual(result[2].tags['display-name'], 'Memory - Total')
        self.assertEqual(result[2].tags['im.dstype.displayType'], 'kilobytes')
        self.assertEqual(result[2].tags['im.identity-tags'], 'name')
        self.assertEqual(result[2].value, 9203300)
        self.assertEqual(result[3].tags['name'],'PA firewall management plane')
        self.assertEqual(result[3].tags['resource-metric'], 'true')
        self.assertEqual(result[3].tags['display-name'], 'Memory - License Limit')
        self.assertEqual(result[3].tags['im.dstype.displayType'], 'kilobytes')
        self.assertEqual(result[3].tags['im.identity-tags'], 'name')
        self.assertEqual(result[3].value, 6500000)
        self.assertEqual(result[4].tags['name'],'PA firewall management plane')
        self.assertEqual(result[4].tags['resource-metric'], 'true')
        self.assertEqual(result[4].tags['display-name'], 'Memory Usage')
        self.assertEqual(result[4].tags['im.dstype.displayType'], 'percentage')
        self.assertEqual(result[4].tags['im.identity-tags'], 'name')
        self.assertEqual(result[4].value, 63.475876923076925)
        self.assertEqual(result[5].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[5].ds_type, 'gauge')
        self.assertEqual(result[5].name, 'process-state-ntpd-status')
        self.assertEqual(result[5].value, 1)

    def test_empty_input(self):
        # Act
        result = self.parser_step2.parse_file(self.current_dir + '/test_empty_input.input', {}, {})
        self.assertEqual(0,len(result))

    def test_high_mp_utilization(self):
        result = self.parser_step2.parse_file(self.current_dir + '/test_high_mp_utilization.input', {}, {})
        self.assertEqual(5,len(result))
        self.assertEqual(result[0].name,'tasks-zombies')
        self.assertEqual(result[0].tags['display-name'], 'Zombie processes')
        self.assertEqual(result[0].value, 321)
        self.assertEqual(result[1].tags['name'],'PA firewall management plane')
        self.assertEqual(result[1].tags['resource-metric'], 'true')
        self.assertEqual(result[1].tags['display-name'], 'Memory - Free')
        self.assertEqual(result[1].tags['im.dstype.displayType'], 'kilobytes')
        self.assertEqual(result[1].tags['im.identity-tags'], 'name')
        self.assertEqual(result[1].value, 206240)
        self.assertEqual(result[2].tags['name'],'PA firewall management plane')
        self.assertEqual(result[2].tags['resource-metric'], 'true')
        self.assertEqual(result[2].tags['display-name'], 'Memory - Total')
        self.assertEqual(result[2].tags['im.dstype.displayType'], 'kilobytes')
        self.assertEqual(result[2].tags['im.identity-tags'], 'name')
        self.assertEqual(result[2].value, 4119652)
        self.assertEqual(result[3].tags['name'],'PA firewall management plane')
        self.assertEqual(result[3].tags['resource-metric'], 'true')
        self.assertEqual(result[3].tags['display-name'], 'Memory Usage')
        self.assertEqual(result[3].tags['im.dstype.displayType'], 'percentage')
        self.assertEqual(result[3].tags['im.identity-tags'], 'name')
        self.assertEqual(result[3].value, 74.2960813194901)
        self.assertEqual(result[4].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[4].ds_type, 'gauge')
        self.assertEqual(result[4].name, 'process-state-ntpd-status')
        self.assertEqual(result[4].value, 1)


    def test_normal_mp_utilization(self):
        result = self.parser_step2.parse_file(self.current_dir + '/test_normal_mp_utilization.input', {}, {})
        self.assertEqual(5,len(result))
        self.assertEqual(result[0].name,'tasks-zombies')
        self.assertEqual(result[0].tags['display-name'], 'Zombie processes')
        self.assertEqual(result[0].value, 123)
        self.assertEqual(result[1].tags['name'],'PA firewall management plane')
        self.assertEqual(result[1].tags['resource-metric'], 'true')
        self.assertEqual(result[1].tags['display-name'], 'Memory - Free')
        self.assertEqual(result[1].tags['im.dstype.displayType'], 'kilobytes')
        self.assertEqual(result[1].tags['im.identity-tags'], 'name')
        self.assertEqual(result[1].value, 449272)
        self.assertEqual(result[2].tags['name'],'PA firewall management plane')
        self.assertEqual(result[2].tags['resource-metric'], 'true')
        self.assertEqual(result[2].tags['display-name'], 'Memory - Total')
        self.assertEqual(result[2].tags['im.dstype.displayType'], 'kilobytes')
        self.assertEqual(result[2].tags['im.identity-tags'], 'name')
        self.assertEqual(result[2].value, 9203300)
        self.assertEqual(result[3].tags['name'],'PA firewall management plane')
        self.assertEqual(result[3].tags['resource-metric'], 'true')
        self.assertEqual(result[3].tags['display-name'], 'Memory Usage')
        self.assertEqual(result[3].tags['im.dstype.displayType'], 'percentage')
        self.assertEqual(result[3].tags['im.identity-tags'], 'name')
        self.assertEqual(result[3].value, 44.831006269490295)
        self.assertEqual(result[4].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[4].ds_type, 'gauge')
        self.assertEqual(result[4].name, 'process-state-ntpd-status')
        self.assertEqual(result[4].value, 1)



if __name__ == '__main__':
    unittest.main()