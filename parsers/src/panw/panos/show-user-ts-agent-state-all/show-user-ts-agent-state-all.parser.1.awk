#Agent: Some USERID(vsys: vsys1) Host: 1.1.1.1:222
/^\s*Agent/ {
    line = $0
    sub(/^\s*Agent:\s*/, "", line)
    agent_name = line
}

#Status                                            : not-conn:idle(Error: Failed to connect to User-ID-Agent at 1.1.1.1(1.1.1.1):222
#OR
#Status                                            : conn:Get IPs
#! (could also be several other connected states like conn:idle )
/^\sStatus/ {
    if ($3 ~ /^conn/) {
        state = 1
    } else {
        state = 0
    }

    agent_tags["name"] = agent_name
    writeDoubleMetric("ts_agent_state", agent_tags, "gauge", state, "true", "Terminal Services Agents", "state", "name")  # Converted to new syntax by change_ind_scripts.py script
}