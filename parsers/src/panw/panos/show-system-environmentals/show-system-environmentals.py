from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class ShowSystemEnvironmentals(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data and raw_data.strip() != '':
            xml_data = helper_methods.parse_data_as_xml(raw_data)
            if xml_data and xml_data['response']['@status'] == 'success':
                thermal_list = xml_data['response']['result'].get('thermal')
                if thermal_list:
                    for slot_id in thermal_list:
                        thermal_slot_list = thermal_list[slot_id]['entry']
                        if not (isinstance(thermal_slot_list, list)):
                            thermal_slot_list = [thermal_slot_list]
                        for thermal_slot in thermal_slot_list:
                            tags = {'name': '{}: {}'.format(slot_id, thermal_slot['description'])}
                            self.write_double_metric('hardware-element-status', tags, 'gauge', 0 if thermal_slot['alarm'] == 'True' else 1, True, 'Hardware Elements State', 'state', 'name')
                            self.write_double_metric('temperature-sensor-current', tags, 'gauge', float(thermal_slot['DegreesC']), True, 'Temperature - Current', 'number', 'name')
                            self.write_double_metric('temperature-sensor-max', tags, 'gauge', float(thermal_slot['max']), True, 'Temperature - Max Limit', 'number', 'name')
    
                fan_list = xml_data['response']['result'].get('fan')
                if fan_list:
                    for slot_id in fan_list:
                        fan_slot_list = fan_list[slot_id]['entry']
                        # In case of one slot, the "fan_slot_list" already is the dictionary we want.
                        # The following line check is this is already a dictionary or list we need to iterate.
                        # If this is the dictionary already, just collect the needed data and report it.
                        if isinstance(fan_slot_list, dict):
                            tags = {'name': '{}: {}'.format(slot_id, fan_slot_list['description'])}
                            self.write_double_metric('hardware-element-status', tags, 'gauge', 0 if fan_slot_list['alarm'] == 'True' else 1, True, 'Hardware Elements State', 'state', 'name')
                        else:
                            for fan_slot in fan_slot_list:
                                tags = {'name': '{}: {}'.format(slot_id, fan_slot['description'])}
                                self.write_double_metric('hardware-element-status', tags, 'gauge', 0 if fan_slot['alarm'] == 'True' else 1, True, 'Hardware Elements State', 'state', 'name')

                power_list = xml_data['response']['result'].get('power')
                if power_list:
                    for slot_id in power_list:
                        power_slot_list = power_list[slot_id]['entry']
                        for power_slot in power_slot_list:
                            tags = {'name': '{}: {}'.format(slot_id, power_slot['description'])}
                            self.write_double_metric('hardware-element-status', tags, 'gauge', 0 if power_slot['alarm'] == 'True' else 1, True, 'Hardware Elements State', 'state', 'name')

                power_supply_list = xml_data['response']['result'].get('power-supply')
                if power_supply_list:
                    for slot_id in power_supply_list:
                        power_supply_slot_list = power_supply_list[slot_id]['entry']
                        for power_supply_slot in power_supply_slot_list:
                            tags = {'name': '{}: {}'.format(slot_id, power_supply_slot['description'])}
                            self.write_double_metric('hardware-element-status', tags, 'gauge', 0 if power_supply_slot['alarm'] == 'True' else 1, True, 'Hardware Elements State', 'state', 'name')
                            self.write_double_metric('power-supply-inserted', tags, 'gauge', 1 if power_supply_slot['Inserted'] == 'True' else 0, True, 'PSU Detected', 'state', 'name')
        return self.output
