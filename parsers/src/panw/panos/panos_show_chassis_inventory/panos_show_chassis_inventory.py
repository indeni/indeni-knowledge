from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class PanosShowChassisInventory(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data = helper_methods.parse_data_as_list(raw_data, 'panos_show_chassis_inventory.textfsm')
            if data:
                for parsed_data in data:
                    tags = {}
                    serial_list = []
                    tags['slot.id'] = parsed_data['slot_id']
                    tags['type'] = parsed_data['name']
                    serial = parsed_data['serial']
                    serial_list.append(serial)
                    self.write_complex_metric_object_array('card-serial-number', tags, serial_list, True, 'Serial Numbers - Chassis')
        return self.output