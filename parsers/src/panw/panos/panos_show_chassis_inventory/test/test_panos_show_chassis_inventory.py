import os
import unittest
from panw.panos.panos_show_chassis_inventory.panos_show_chassis_inventory import PanosShowChassisInventory
from parser_service.public.action import *

class TestPanosShowChassisInventory(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = PanosShowChassisInventory()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_show_chassis_inventory(self):
        result = self.parser.parse_file(self.current_dir + '/panos_show_chassis_inventory.input', {}, {})
        self.assertEqual(9, len(result))
        self.assertEqual(result[0].action_type, 'WriteComplexMetric')
        self.assertEqual(result[0].name, 'card-serial-number')
        self.assertEqual(result[0].tags['type'], 'Chassis')
        self.assertEqual(result[0].tags['slot.id'], '')
        self.assertEqual(result[0].value[0], '010108001644')
        self.assertEqual(result[1].action_type, 'WriteComplexMetric')
        self.assertEqual(result[1].name, 'card-serial-number')
        self.assertEqual(result[1].tags['type'], 'PA-7000-20GXM-NPC')
        self.assertEqual(result[1].value[0], '013701000814')
        self.assertEqual(result[1].tags['slot.id'], '1')
        self.assertEqual(result[2].action_type, 'WriteComplexMetric')
        self.assertEqual(result[2].name, 'card-serial-number')
        self.assertEqual(result[2].tags['type'], 'PA-7000-20GQ-NPC')
        self.assertEqual(result[2].value[0], '011701002948')
        self.assertEqual(result[2].tags['slot.id'], '2')
        self.assertEqual(result[3].action_type, 'WriteComplexMetric')
        self.assertEqual(result[3].name, 'card-serial-number')
        self.assertEqual(result[3].tags['type'], 'empty')
        self.assertEqual(result[3].tags['slot.id'], '3')
        self.assertEqual(result[3].value[0], '')
        self.assertEqual(result[4].action_type, 'WriteComplexMetric')
        self.assertEqual(result[4].name, 'card-serial-number')
        self.assertEqual(result[4].tags['type'], 'PA-7050-SMC')
        self.assertEqual(result[4].value[0], '002101001677')
        self.assertEqual(result[4].tags['slot.id'], '4')
        self.assertEqual(result[5].action_type, 'WriteComplexMetric')
        self.assertEqual(result[5].name, 'card-serial-number')
        self.assertEqual(result[5].tags['type'], 'empty')
        self.assertEqual(result[5].value[0], '')
        self.assertEqual(result[5].tags['slot.id'], '5')
        self.assertEqual(result[6].action_type, 'WriteComplexMetric')
        self.assertEqual(result[6].name, 'card-serial-number')
        self.assertEqual(result[6].tags['type'], 'empty')
        self.assertEqual(result[6].tags['slot.id'], '6')
        self.assertEqual(result[6].value[0], '')
        self.assertEqual(result[7].action_type, 'WriteComplexMetric')
        self.assertEqual(result[7].name, 'card-serial-number')
        self.assertEqual(result[7].tags['type'], 'PA-7000-LPC')
        self.assertEqual(result[7].value[0], '001401001198')
        self.assertEqual(result[7].tags['slot.id'], '7')
        self.assertEqual(result[8].action_type, 'WriteComplexMetric')
        self.assertEqual(result[8].name, 'card-serial-number')
        self.assertEqual(result[8].tags['type'], 'empty')
        self.assertEqual(result[8].tags['slot.id'], '8')
        self.assertEqual(result[8].value[0], '')

if __name__ == '__main__':
    unittest.main()