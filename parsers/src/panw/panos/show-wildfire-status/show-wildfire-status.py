from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser
import re

class ShowWildfireStatus(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        ok_status = ['Idle', 'Getting reports', 'Registering', 'Downloading server list','Forwarding files']
        
        # Check if input is empty to avoid some errors
        if raw_data.strip() != '':
            # Parse the raw data as xml
            xml_data = helper_methods.parse_data_as_xml(raw_data)
            # Wrap the path in try/except to keep the script simpler(otherwise a check will be necessary for each section)
            if xml_data and xml_data['response']['result']['member'] is not None:
                #Plain text CLI command output returned into the API call
                data = helper_methods.parse_data_as_list(raw_data, 'paloalto_panos_show_wildfire_status_cloud.textfsm')
                for cloud in data:
                    if cloud.get('status') and cloud['status'] != 'Disabled due to configuration':
                        tags = {'name': cloud['wildfire_cloud']}
                        self.write_double_metric('wildfire-license', tags, 'gauge', 1 if cloud['license'] == "yes" else 0, 
                                                True, 'WildFire Cloud License', 'boolean', 'name')
                        self.write_double_metric('wildfire-connected', tags, 'gauge', 1 if any(x in cloud['status'] for x in ok_status) else 0, 
                                                True, 'WildFire Cloud Connected', 'boolean', 'name')
                        self.write_double_metric('wildfire-registered', tags, 'gauge', 1 if cloud['registered'] == "yes" else 0, 
                                                True, 'WildFire Cloud Registered', 'boolean', 'name')
        return self.output

