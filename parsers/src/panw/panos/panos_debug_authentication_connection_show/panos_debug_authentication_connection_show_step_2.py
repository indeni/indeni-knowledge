from parser_service.public.base_parser import BaseParser
from parser_service.public import helper_methods
import re
class PanosDebugAuthenticationConnectionShowStep2(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data = helper_methods.parse_data_as_list(raw_data, 'panos_debug_authentication_connection_show_step_2.textfsm')
            if data:
                tags = {}
                tags['name'] = dynamic_var['server'].split('|')[0]
                error_found = 0
                for parsed_data in data:
                    if len(re.findall(re.escape(dynamic_var['server'].split('|')[0]),parsed_data['message'], flags=0)) != 0 and len(re.findall(r'failed|down',parsed_data['message'], flags=0)) != 0: 
                        error_found = 1
                self.write_double_metric('ldap-server-log-error', tags, 'gauge', error_found, False)
        return self.output