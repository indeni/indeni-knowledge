import os
import unittest
from panw.panos.panos_debug_authentication_connection_show.panos_debug_authentication_connection_show_step_2 import PanosDebugAuthenticationConnectionShowStep2
from parser_service.public.action import *

class TestPanosDebugAuthenticationConnectionShowStep2(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = PanosDebugAuthenticationConnectionShowStep2()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_panos_debug_authentication_connection_show_step_2_ad_nok(self):
        result = self.parser.parse_file(self.current_dir + '/panos_debug_authentication_connection_show_step_2_ad_nok.input', {'server': '10.11.80.30'}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'ldap-server-log-error')
        self.assertEqual(result[0].tags['name'], '10.11.80.30')
        self.assertEqual(result[0].value, 1)

    def test_panos_debug_authentication_connection_show_step_2_ad_ok(self):
        result = self.parser.parse_file(self.current_dir + '/panos_debug_authentication_connection_show_step_2_ad_ok.input', {'server': '10.11.80.31'}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'ldap-server-log-error')
        self.assertEqual(result[0].tags['name'], '10.11.80.31')
        self.assertEqual(result[0].value, 0)

if __name__ == '__main__':
    unittest.main()