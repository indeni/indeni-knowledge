import os
import unittest
from panw.panos.panos_debug_authentication_connection_show.panos_debug_authentication_connection_show_step_1 import PanosDebugAuthenticationConnectionShowStep1
from parser_service.public.action import *

class TestPanosDebugAuthenticationConnectionShowStep1(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = PanosDebugAuthenticationConnectionShowStep1()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_panos_debug_authentication_connection_show_step_1(self):
        result = self.parser.parse_file(self.current_dir + '/panos_debug_authentication_connection_show_step_1.input', {}, {})
        self.assertEqual(4, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'ldap-server-status')
        self.assertEqual(result[0].tags['name'], '10.11.80.30')
        self.assertEqual(result[0].tags['display-name'], 'LDAP Connectivity status')
        self.assertEqual(result[0].tags['im.dstype.displayType'], 'state')
        self.assertEqual(result[0].value, 1)
        self.assertEqual(result[1].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[1].key, 'server')
        self.assertEqual(result[1].value, '10.11.80.30')
        self.assertEqual(result[2].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[2].name, 'ldap-server-status')
        self.assertEqual(result[2].tags['name'], '10.11.80.31')
        self.assertEqual(result[2].tags['display-name'], 'LDAP Connectivity status')
        self.assertEqual(result[2].tags['im.dstype.displayType'], 'state')
        self.assertEqual(result[2].value, 1)
        self.assertEqual(result[3].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[3].key, 'server')
        self.assertEqual(result[3].value, '10.11.80.31')

if __name__ == '__main__':
    unittest.main()