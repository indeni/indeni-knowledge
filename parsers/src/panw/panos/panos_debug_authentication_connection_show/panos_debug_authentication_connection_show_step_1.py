from parser_service.public.base_parser import BaseParser
from parser_service.public import helper_methods

class PanosDebugAuthenticationConnectionShowStep1(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data = helper_methods.parse_data_as_list(raw_data, 'panos_debug_authentication_connection_show_step_1.textfsm')
            if data:
                for parsed_data in data:
                    server = parsed_data['server']
                    tags = {}
                    tags['name'] = server
                    self.write_double_metric('ldap-server-status', tags, 'gauge', 1 if parsed_data['active'] == 'yes' else 0, True, 'LDAP Connectivity status', 'state' )
                    self.write_dynamic_variable('server', server)
        return self.output