#sw-version: 9.0.0
/sw-version/ {
    sw_version = $2
    split(sw_version, version_vals, ".")
    major = version_vals[1]
    minor = version_vals[2]
    patch = version_vals[3]
    
    command_pre_v8 = "<show><neighbor>all<%2Fneighbor><%2Fshow>"
    command_post_v8 = "<show><neighbor><interface><%2Finterface><%2Fneighbor><%2Fshow>"

    if (major < 8) {
        writeDynamicVariable("dynCommand", command_pre_v8)
    }
    else {
        writeDynamicVariable("dynCommand", command_post_v8)
    }   
}