from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class MP_CPU_Parser(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        # Step 1 : Data Extraction
        data = helper_methods.parse_data_as_object(raw_data, 'panos-mp-cpu.textfsm')

        # Step 2 : Data Processing
        mp_cpu_usage = int(data['utilization'])

        # Step 3 : Data Reporting
        tags = {'name': 'CPU Usage', 'cpu-id': 'MP', 'cpu-is-avg': 'false', 'resource-metric': 'true', 'im.identity-tags' : 'cpu-id', 'dp': 'false'}
        self.write_double_metric('cpu-usage', tags, 'gauge', mp_cpu_usage, True ,'CPU Usage', 'percentage','cpu-id')

        # The method must return self.output
        return self.output
