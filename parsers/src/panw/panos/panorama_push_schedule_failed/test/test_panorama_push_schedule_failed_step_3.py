import os
import unittest
from panw.panos.panorama_push_schedule_failed.panorama_push_schedule_failed_step_3 import PanoramaPushScheduleFailedStep3
from parser_service.public.action import *

class TestPanoramaPushScheduleFailedStep3(unittest.TestCase):

    def setUp(self):
        self.parser = PanoramaPushScheduleFailedStep3()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_panorama_push_schedule_step3_failed(self):
        result = self.parser.parse_file(self.current_dir + '/panorama_push_schedule_step3_failed.input', {'push_schedule_name': 'daily-test-push', 'job_id': '250'}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].tags['push_schedule_name'], 'daily-test-push')
        self.assertEqual(result[0].value, 0)

    def test_panorama_push_schedule_step3_ok(self):
        result = self.parser.parse_file(self.current_dir + '/panorama_push_schedule_step3_ok.input', {'push_schedule_name': 'daily-test-push', 'job_id': '250'}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].tags['push_schedule_name'], 'daily-test-push')
        self.assertEqual(result[0].value, 1)


    def test_panorama_push_schedule_job_no_ok(self):
        result = self.parser.parse_file(self.current_dir + '/panorama_push_schedule_failed_invalid_value.input', {'push_schedule_name': 'daily-test-not-disabled', 'job_id': '0'}, {})
        self.assertEqual(0, len(result))


if __name__ == '__main__':
    unittest.main()