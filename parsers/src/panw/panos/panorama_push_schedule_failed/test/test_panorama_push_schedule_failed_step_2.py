import os
import unittest
from panw.panos.panorama_push_schedule_failed.panorama_push_schedule_failed_step_2 import PanoramaPushScheduleFailedStep2
from parser_service.public.action import *

class TestPanoramaPushScheduleFailedStep2(unittest.TestCase):

    def setUp(self):
        self.parser = PanoramaPushScheduleFailedStep2()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_panorama_push_schedule_daily(self):
        result = self.parser.parse_file(self.current_dir + '/panorama_push_schedule_daily.input', {'push_schedule_name': 'daily-test-push'}, {})
        self.assertEqual(2, len(result))
        self.assertEqual(result[0].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[0].key, 'push_schedule_name')
        self.assertEqual(result[0].value, 'daily-test-push')
        self.assertEqual(result[1].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[1].key, 'job_id')
        self.assertEqual(result[1].value, '250')

    def test_panorama_push_schedule_daily_disabled(self):
        result = self.parser.parse_file(self.current_dir + '/panorama_push_schedule_daily_disabled.input', {'push_schedule_name': 'daily-test-disabled'}, {})
        self.assertEqual(2, len(result))
        self.assertEqual(result[0].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[0].key, 'push_schedule_name')
        self.assertEqual(result[0].value, 'daily-test-disabled')
        self.assertEqual(result[1].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[1].key, 'job_id')
        self.assertEqual(result[1].value, '0')

    def test_panorama_push_schedule_daily_no_changes(self):
        result = self.parser.parse_file(self.current_dir + '/panorama_push_schedule_daily_no_changes.input', {'push_schedule_name': 'daily-test-not-disabled'}, {})
        self.assertEqual(2, len(result))
        self.assertEqual(result[0].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[0].key, 'push_schedule_name')
        self.assertEqual(result[0].value, 'daily-test-not-disabled')
        self.assertEqual(result[1].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[1].key, 'job_id')
        self.assertEqual(result[1].value, '0')

    def test_panorama_push_schedule_weekly_not_disabled(self):
        result = self.parser.parse_file(self.current_dir + '/panorama_push_schedule_weekly_not_disabled.input', {'push_schedule_name': 'weekly-test-not-disabled'}, {})
        self.assertEqual(2, len(result))
        self.assertEqual(result[0].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[0].key, 'push_schedule_name')
        self.assertEqual(result[0].value, 'weekly-test-not-disabled')
        self.assertEqual(result[1].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[1].key, 'job_id')
        self.assertEqual(result[1].value, '0')

if __name__ == '__main__':
    unittest.main()