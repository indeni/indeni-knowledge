import os
import unittest
from panw.panos.panorama_push_schedule_failed.panorama_push_schedule_failed_step_1 import PanoramaPushScheduleFailedStep1
from parser_service.public.action import *

class TestPanoramaPushScheduleFailedStep1(unittest.TestCase):

    def setUp(self):
        self.parser = PanoramaPushScheduleFailedStep1()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_panorama_push_schedule_failed_step_1(self):
        result = self.parser.parse_file(self.current_dir + '/panorama_push_schedule_failed_step_1.input', {}, {})
        self.assertEqual(4, len(result))
        self.assertEqual(result[0].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[0].key, 'push_schedule_name')
        self.assertEqual(result[0].value, 'daily-test-not-disabled')
        self.assertEqual(result[1].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[1].key, 'push_schedule_name')
        self.assertEqual(result[1].value, 'daily-test-disabled')
        self.assertEqual(result[2].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[2].key, 'push_schedule_name')
        self.assertEqual(result[2].value, 'weekly-test-not-disabled')
        self.assertEqual(result[3].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[3].key, 'push_schedule_name')
        self.assertEqual(result[3].value, 'daily-test-push')

if __name__ == '__main__':
    unittest.main()