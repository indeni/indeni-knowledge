from parser_service.public.base_parser import BaseParser
from parser_service.public import helper_methods

class PanoramaPushScheduleFailedStep1(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data = helper_methods.parse_data_as_list(raw_data, 'panorama_push_schedule_failed_step_1.textfsm')
            if data:
                for parsed_data in data:
                    push_schedule_name = parsed_data['policy_name']
                    self.write_dynamic_variable('push_schedule_name', push_schedule_name)
        return self.output