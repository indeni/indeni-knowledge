from parser_service.public.base_parser import BaseParser
from parser_service.public import helper_methods
from datetime import datetime

class PanoramaPushScheduleFailedStep3(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            if dynamic_var.get('job_id') != '0':
                tags = {}
                tags['push_schedule_name'] = dynamic_var.get('push_schedule_name')
                data = helper_methods.parse_data_as_list(raw_data, 'panorama_push_schedule_failed_step_3.textfsm')
                status = 1 if data[0]['result'] == 'OK' else 0
                self.write_double_metric('push-schedule-status', tags, 'gauge', status, False)
        return self.output