from parser_service.public.base_parser import BaseParser
from parser_service.public import helper_methods

class PanoramaPushScheduleFailedStep2(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data = helper_methods.parse_data_as_list(raw_data, 'panorama_push_schedule_failed_step_2.textfsm')
            if not data:
                job_id = "0"
                push_schedule_name = dynamic_var['push_schedule_name']
            if data:
                for parsed_data in data:
                    push_schedule_name = dynamic_var['push_schedule_name']
                    job_id = parsed_data['job_id']
            self.write_dynamic_variable('push_schedule_name', push_schedule_name)
            self.write_dynamic_variable('job_id', job_id)
        return self.output