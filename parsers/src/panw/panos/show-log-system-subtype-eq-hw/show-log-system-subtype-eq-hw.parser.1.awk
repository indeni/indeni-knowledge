BEGIN {
metric_tags["live-config"] = "true"
metric_tags["display-name"] = "Slot"
metric_tags["im.identity-tags"] = "name"
}

# SAMPLE Output
#Domain,Receive Time,Serial #,Type,Threat/Content Type,Config Version,Generate Time,Virtual System,Event ID,Object,fmt,id,module,Severity,Description,Sequence Number,Action Flags,dg_hier_level_1,dg_hier_level_2,dg_hier_level_3,dg_hier_level_4,Virtual System Name,Device Name
#1,10/7/18 18:43,0,SYSTEM,hw,0,10/7/18 18:43,,slot-up,,0,0,general,informational,Slot 1 (PA-5220) is up.,6.60979E+18,0x8000000000000000,0,0,0,0,,PA-5220
#1,10/7/18 18:43,0,SYSTEM,hw,0,10/7/18 18:43,,slot-up,,0,0,general,informational,Slot 1 (PA-5220) is up.,6.60979E+18,0x8000000000000000,0,0,0,0,,PA-5220
#1,10/7/18 18:42,0,SYSTEM,hw,0,10/7/18 18:42,,slot-up,,0,0,general,informational,Slot 1 (PA-5220) is up.,6.60979E+18,0x8000000000000000,0,0,0,0,,PA-5220
#1,10/7/18 18:42,0,SYSTEM,hw,0,10/7/18 18:42,,slot-starting,,0,0,general,informational,Slot 1 (PA-5220) is starting.,6.60979E+18,0x8000000000000000,0,0,0,0,,PA-5220
#1,10/7/18 18:42,0,SYSTEM,hw,0,10/7/18 18:42,,slot-starting,,0,0,general,informational,Slot 1 (PA-5220) is starting.,6.60979E+18,0x8000000000000000,0,0,0,0,,PA-5220
#1,10/7/18 18:40,0,SYSTEM,hw,0,10/7/18 18:40,,slot-starting,,0,0,general,informational,Slot 1 (PA-5220) is starting.,6.60979E+18,0x8000000000000000,0,0,0,0,,PA-5220
#1,10/7/18 18:39,0,SYSTEM,hw,0,10/7/18 18:39,,slot-starting,,0,0,general,informational,Slot 1 (PA-5220) is starting.,6.60979E+18,0x8000000000000000,0,0,0,0,,PA-5220
#1,10/7/18 18:39,0,SYSTEM,hw,0,10/7/18 18:38,,slot-starting,,0,0,general,informational,Slot 1 (PA-5220) is starting.,6.60979E+18,0x8000000000000000,0,0,0,0,,PA-5220
#1,10/7/18 18:28,0,SYSTEM,hw,0,10/7/18 18:28,,slot-up,,0,0,general,informational,Slot 1 (PA-5220) is up.,6.60979E+18,0x8000000000000000,0,0,0,0,,PA-5220
#1,10/7/18 18:27,0,SYSTEM,hw,0,10/7/18 18:27,,slot-up,,0,0,general,informational,Slot 1 (PA-5220) is up.,6.60979E+18,0x8000000000000000,0,0,0,0,,PA-5220
#1,10/7/18 18:27,0,SYSTEM,hw,0,10/7/18 18:27,,slot-up,,0,0,general,informational,Slot 1 (PA-5220) is up.,6.60979E+18,0x8000000000000000,0,0,0,0,,PA-5220
#1,10/7/18 18:27,0,SYSTEM,hw,0,10/7/18 18:27,,slot-starting,,0,0,general,informational,Slot 1 (PA-5220) is starting.,6.60979E+18,0x8000000000000000,0,0,0,0,,PA-5220
#1,10/7/18 18:27,0,SYSTEM,hw,0,10/7/18 18:27,,slot-starting,,0,0,general,informational,Slot 1 (PA-5220) is starting.,6.60979E+18,0x8000000000000000,0,0,0,0,,PA-5220
#1,10/7/18 18:25,0,SYSTEM,hw,0,10/7/18 18:25,,slot-starting,,0,0,general,informational,Slot 1 (PA-5220) is starting.,6.60979E+18,0x8000000000000000,0,0,0,0,,PA-5220
#1,10/7/18 18:24,0,SYSTEM,hw,0,10/7/18 18:23,,slot-starting,,0,0,general,informational,Slot 1 (PA-5220) is starting.,6.60979E+18,0x8000000000000000,0,0,0,0,,PA-5220
#1,10/7/18 18:24,0,SYSTEM,hw,0,10/7/18 18:23,,slot-starting,,0,0,general,informational,Slot 1 (PA-5220) is starting.,6.60979E+18,0x8000000000000000,0,0,0,0,,PA-5220

{
    #The first line includes the names of the fields, so this can be ignored. Our focus should be on the latest log message to get the status of the slot, therefore no need to go through the other lines
    if ( NR == 2 )   {
        #The last log will be on the 2nd line of the output, therefore we are using NR = 2. If there are no logs, the line will be empty.
        split($0, log_message, ",")
        gsub(/[\"\.]/, "" , log_message[15])
        #We are interested in description field which is #15 of the list, for example;
        ##1,10/7/18 18:24,0,SYSTEM,hw,0,10/7/18 18:23,,slot-starting,,0,0,general,informational,Slot 1 (PA-5220) is starting.,6.60979E+18,0x8000000000000000,0,0,0,0,,PA-5220
        #We need to get this part "Slot 1 (PA-5220) is starting."
        split(log_message[15], error, " ")
        #error[2] is going to match the slot #, for example; 1 from "Slot 1"
        if (error[2] == /[1-9]/) {
            slot = "Slot " error[2]
        }
        metric_tags["name"] = slot
        metric_tags["im.dstype.displaytype"] = "number"
        #error[5] js going to match the latest reported state, for example; starting or up
        status = error[5]
        if (status == "up") {
            blade_state = 1
        } else {
            blade_state = 0
        }
        metric_tags["im.dstype.displaytype"] = "state"
        writeDoubleMetric("blade-state", metric_tags, "gauge", blade_state, "true", "Slot status", "state", "name")  # Converted to new syntax by change_ind_scripts.py script
        exit
    }
}