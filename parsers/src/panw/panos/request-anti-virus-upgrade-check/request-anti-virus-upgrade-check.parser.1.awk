BEGIN {
    version_found = 0
    latest_version = 0
}

# If the commnand request-anti-virus-upgrade-check fails to run it will return Server error : followed by the reason such as generic communicaton error.

#8024-4749             45MB  2018/05/24 18:57:10 CDT        yes    current
#8019-4720             45MB  2018/05/14 16:10:35 CDT         no         no
#8023-4746             45MB  2018/05/22 20:23:44 CDT        yes   previous

# Can also be preceded by a "#" in some OS versions:
##8024-4749             45MB  2018/05/24 18:57:10 CDT        yes    current
##8019-4720             45MB  2018/05/14 16:10:35 CDT         no         no
##8023-4746             45MB  2018/05/22 20:23:44 CDT        yes   previous

/^[0-9\#]/ {

    # This version_found gets set if it finds a version after running the panos command.
    version_found = 1

    released_on_date = $3
    split(released_on_date, date_vals, "/")
    year = date_vals[1]
    month = date_vals[2]
    day = date_vals[3]
    release_time = $4
    split(release_time, time_vals, ":")
    hour = time_vals[1]
    minute = time_vals[2]
    second = time_vals[3]

    release_epoch = datetime(year, month, day, hour, minute, second)

    # See if latest version entry is less than this release's release epoch.
    if ( latest_version < release_epoch ) {
        # If it is less than the latest version this will now set the latest_version variable to the release_epoch.
        latest_version = release_epoch
        # Now also set the av_version_name to the first column of data containing a number or starting with a # if it happens to be set that way in your version of code.
        av_version_name = $1
        sub(/^#/, "", av_version_name)
        # If this latest available version of anti-virus is the "current" installed anti-virus then mark av_is_current as true, otherwise false.
        if ($7 == "current") {
            av_is_newest = "true"
        } else {
            av_is_newest = "false"
        }
        version_tags["newest_version"] = av_version_name
    }
}

/^Error/ {
    # This version_found gets set if it finds a version after running the panos command.
    error_found = 1
    line = $NF
    error_tags[i++, "line"] = $0
}

END {

    if (version_found) {
        writeComplexMetricString("antivirus-update-state", version_tags, av_is_newest, "false")  # Converted to new syntax by change_ind_scripts.py script
    }

    if (error_found) {
        writeDoubleMetric("external-service-error", error_tags, "gauge", error_found, "false")  # Converted to new syntax by change_ind_scripts.py script
    }
}
