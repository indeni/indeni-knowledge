from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class ShowSystemStateFilterNS(BaseParser):
    def prepare_interface_metric(self, interface, if_stat: str):
        if interface[if_stat] is None or interface[if_stat] == '':
            stat = 0
        else:
            stat = interface[if_stat]
        return int(stat)

    def prepare_interface_metric_bits(self, interface, if_stat: str):
        if interface[if_stat] is None or interface[if_stat] == '':
            stat = 0
        else:
            stat = int(interface[if_stat]) * 8 
        return int(stat)

    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            xml_data = helper_methods.parse_data_as_xml(raw_data)

            if xml_data and xml_data['response']['@status'] == 'success':
                response = xml_data['response']['result']
                if 'ifnet' in response:
                    for interface in response['ifnet']['ifnet']['entry']:
                        interface_name = interface['name'].capitalize()
                        metric_tags = {'name': interface_name, 'im.identity-tags': 'name'}

                        interface_stat = self.prepare_interface_metric(interface, 'ipackets')
                        self.write_double_metric('network-interface-rx-packets', metric_tags, 'counter', interface_stat, True, 'Network Interfaces - RX Packets', 'number', 'name')

                        interface_stat = self.prepare_interface_metric(interface, 'opackets')
                        self.write_double_metric('network-interface-tx-packets', metric_tags, 'counter', interface_stat, True, 'Network Interfaces - TX Packets', 'number', 'name')

                        interface_stat_rx = self.prepare_interface_metric_bits(interface, 'ibytes')
                        self.write_double_metric('network-interface-rx-bits', metric_tags, 'counter', interface_stat_rx, True, 'Network Interfaces - RX Bits', 'bits', 'name')

                        interface_stat_tx = self.prepare_interface_metric_bits(interface, 'obytes')
                        self.write_double_metric('network-interface-tx-bits', metric_tags, 'counter', interface_stat_tx, True, 'Network Interfaces - TX Bits', 'bits', 'name')

                        self.write_double_metric('network-interface-total-bits', metric_tags, 'counter', interface_stat_rx + interface_stat_tx, False)

                        interface_stat = self.prepare_interface_metric(interface, 'idrops')
                        self.write_double_metric('network-interface-rx-dropped', metric_tags, 'counter', interface_stat, True, 'Network Interfaces - Drops', 'number', 'name')

        return self.output

# helper_methods.print_list(ShowSystemStateFilterNS().parse_file("/Users/shoukyd/proj/indeni-kd/parsers/src/panw/panos/show_counter_interface/test/valid_input_show_counter_interface", {}, {}))
