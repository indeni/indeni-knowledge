import os
import unittest
from panw.panos.show_counter_interface.show_counter_interface import ShowSystemStateFilterNS


class TestShowSystemStateFilterNS(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = ShowSystemStateFilterNS()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_valid_input(self):
        expected_results = [{'name': 'Ethernet1/1', 'metric': 'network-interface-rx-packets', 'value': 8},
                            {'name': 'Ethernet1/1', 'metric': 'network-interface-tx-packets', 'value': 8},
                            {'name': 'Ethernet1/1', 'metric': 'network-interface-rx-bits', 'value': 4736},
                            {'name': 'Ethernet1/1', 'metric': 'network-interface-tx-bits', 'value': 4736},
                            {'name': 'Ethernet1/1', 'metric': 'network-interface-total-bits', 'value': 9472},
                            {'name': 'Ethernet1/1', 'metric': 'network-interface-rx-dropped', 'value': 0},
                            {'name': 'Ethernet1/2', 'metric': 'network-interface-rx-packets', 'value': 0},
                            {'name': 'Ethernet1/2', 'metric': 'network-interface-tx-packets', 'value': 0},
                            {'name': 'Ethernet1/2', 'metric': 'network-interface-rx-bits', 'value': 0},
                            {'name': 'Ethernet1/2', 'metric': 'network-interface-tx-bits', 'value': 0},
                            {'name': 'Ethernet1/2', 'metric': 'network-interface-total-bits', 'value': 0},
                            {'name': 'Ethernet1/2', 'metric': 'network-interface-rx-dropped', 'value': 0},
                            {'name': 'Ethernet1/3', 'metric': 'network-interface-rx-packets', 'value': 1701991},
                            {'name': 'Ethernet1/3', 'metric': 'network-interface-tx-packets', 'value': 1665028},
                            {'name': 'Ethernet1/3', 'metric': 'network-interface-rx-bits', 'value': 1828010120},
                            {'name': 'Ethernet1/3', 'metric': 'network-interface-tx-bits', 'value': 1349321400},
                            {'name': 'Ethernet1/3', 'metric': 'network-interface-total-bits', 'value': 3177331520},
                            {'name': 'Ethernet1/3', 'metric': 'network-interface-rx-dropped', 'value': 0},
                            {'name': 'Ethernet1/4', 'metric': 'network-interface-rx-packets', 'value': 1377220},
                            {'name': 'Ethernet1/4', 'metric': 'network-interface-tx-packets', 'value': 1327811},
                            {'name': 'Ethernet1/4', 'metric': 'network-interface-rx-bits', 'value': 806109344},
                            {'name': 'Ethernet1/4', 'metric': 'network-interface-tx-bits', 'value': 498377008},
                            {'name': 'Ethernet1/4', 'metric': 'network-interface-total-bits', 'value': 1304486352},
                            {'name': 'Ethernet1/4', 'metric': 'network-interface-rx-dropped', 'value': 0}]

        # Act
        result = self.parser.parse_file(self.current_dir + '/valid_input_show_counter_interface', {}, {})

        # # Assert
        self.assertEqual(90, len(result))

        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i]['name'], result[i].tags['name'])
            self.assertEqual(expected_results[i]['metric'], result[i].name)
            self.assertEqual(expected_results[i]['value'], result[i].value)

    def test_invalid_input(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/invalid_input_empty', {}, {})
        # Assert
        self.assertEqual(0, len(result))


if __name__ == '__main__':
    unittest.main()
