BEGIN {
}

# Sample line from the output of "show hardware stats" on the ACME Humidifier:
# Current humidity: 27%
# Fan speed: 200 RPM
# Water remaining: 1.72 L

/Current humidity/ {
	# $NF means "give me the last field" in awk.
	percentage=$NF

	# Remove the "%", we don't include that in the value we store (a double metric can only be a number with a decimal place, nothing around it)
	sub(/%/, "", percentage)

	# We write the humidity level metric, informing the db that this is a gauge that is updated every 5 minutes.
	# We also include information for "live config".
	writeDoubleMetric("humidity", null, "gauge", percentage, "true", "Hardware - Stats", "percentage", "")  # Converted to new syntax by change_ind_scripts.py script
}

/Fan speed/ {
	# Get the one-before-last field - we don't need the "RPM" stuff
	speed=$(NF-1)

	# We write the fan speed metric, same parameters as humidity.
	writeDoubleMetric("fan-speed", null, "gauge", speed, "true", "Hardware - Stats", "number", "")  # Converted to new syntax by change_ind_scripts.py script
}

/Water remaining/ {
	water=$(NF-1)

	# We write the water remaining metric, same parameters as humidity.
	writeDoubleMetric("water-remaining", null, "gauge", water, "true", "Hardware - Stats", "number", "")  # Converted to new syntax by change_ind_scripts.py script
}

END {
}