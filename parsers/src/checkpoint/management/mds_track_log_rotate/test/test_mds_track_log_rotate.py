import os
import unittest

from checkpoint.management.mds_track_log_rotate.mds_track_log_rotate import MdsTrackLogRotate
from parser_service.public.action import *

class TestMdsTrackLogRotate(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = MdsTrackLogRotate()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_mds_multiple_cma_multiple_gateways(self):
        result = self.parser.parse_file(self.current_dir + '/mds_multiple_cma_multiple_gateways.input', {}, {})
        self.assertEqual(6, len(result))

        self.assertEqual('cma-read-log-rate', result[0].name)
        self.assertEqual(50, result[0].value)
        self.assertEqual('CMA_1_R81', result[0].tags['cma'])
        self.assertEqual('cma|im.name', result[0].tags['im.identity-tags'])
        self.assertEqual('CMA Log Rate', result[0].tags['display-name'])

        self.assertEqual('cma-log-server-receive-rate', result[1].name)
        self.assertEqual(45, result[1].value)
        self.assertEqual('CMA_1_R81', result[1].tags['cma'])
        self.assertEqual('cma|im.name', result[1].tags['im.identity-tags'])
        self.assertEqual('CMA Log Rate', result[1].tags['display-name'])

        self.assertEqual('cma-read-log-rate', result[2].name)
        self.assertEqual(0, result[2].value)
        self.assertEqual('CMA_2_R81', result[2].tags['cma'])
        self.assertEqual('cma|im.name', result[2].tags['im.identity-tags'])
        self.assertEqual('CMA Log Rate', result[2].tags['display-name'])

        self.assertEqual('cma-log-server-receive-rate', result[3].name)
        self.assertEqual(0, result[3].value)
        self.assertEqual('CMA_2_R81', result[3].tags['cma'])
        self.assertEqual('cma|im.name', result[3].tags['im.identity-tags'])
        self.assertEqual('CMA Log Rate', result[3].tags['display-name'])

        self.assertEqual('cma-read-log-rate', result[4].name)
        self.assertEqual(0, result[4].value)
        self.assertEqual('CLM_1_Server', result[4].tags['cma'])
        self.assertEqual('cma|im.name', result[4].tags['im.identity-tags'])
        self.assertEqual('CMA Log Rate', result[4].tags['display-name'])

        self.assertEqual('cma-log-server-receive-rate', result[5].name)
        self.assertEqual(0, result[5].value)
        self.assertEqual('CLM_1_Server', result[5].tags['cma'])
        self.assertEqual('cma|im.name', result[5].tags['im.identity-tags'])
        self.assertEqual('CMA Log Rate', result[5].tags['display-name'])

    def test_mlm_single_clm(self):
        result = self.parser.parse_file(self.current_dir + '/mlm_single_clm.input', {}, {})
        self.assertEqual(2, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'cma-read-log-rate')
        self.assertEqual(result[0].tags['cma'], 'CLM_1')
        self.assertEqual(result[0].tags['live-config'], 'true')
        self.assertEqual(result[0].tags['display-name'], 'CMA Log Rate')
        self.assertEqual(result[0].tags['im.dstype.displayType'], 'number')
        self.assertEqual(result[0].tags['im.identity-tags'], 'cma|im.name')
        self.assertEqual(result[0].value, 0)
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].name, 'cma-log-server-receive-rate')
        self.assertEqual(result[1].tags['cma'], 'CLM_1')
        self.assertEqual(result[1].tags['live-config'], 'true')
        self.assertEqual(result[1].tags['display-name'], 'CMA Log Rate')
        self.assertEqual(result[1].tags['im.dstype.displayType'], 'number')
        self.assertEqual(result[1].tags['im.identity-tags'], 'cma|im.name')
        self.assertEqual(result[1].value, 0)

if __name__ == '__main__':
    unittest.main()