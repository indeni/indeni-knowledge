from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class MdsTrackLogRotate(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):

        parsed_data = helper_methods.parse_data_as_list(raw_data, 'mds_track_log_rotate.textfsm')
        if parsed_data:
            for cma_data in parsed_data:
                tags = {}
                tags['cma'] = cma_data['cma_name']
                self.write_double_metric('cma-read-log-rate', tags, 'gauge', int(cma_data['read_log_rate']), True, 'CMA Log Rate', 'number', 'cma|im.name')
                self.write_double_metric('cma-log-server-receive-rate', tags, 'gauge', int(cma_data['total_log_receive_rate']), True, 'CMA Log Rate', 'number', 'cma|im.name')
        return self.output
