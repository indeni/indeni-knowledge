#Result: CP_R80.20_MDS1_CMA1_Server 10.11.94.74 CP_R80.20_MDS1_CMA1_Server_2 10.11.94.75
#Result: CP_R80.20_MDS1_CMA2_Server 10.11.94.76 CP_R80.20_MDS1_CMA2_Server_2 10.11.94.77
/^Result/{
    if (NF == 5) {
        step2 = $2 ":" $3 ":" $4 ":" $5
        writeDynamicVariable("secondaryInfo", step2)
    }
    else {
        step2 = $2 ":" $3 ":NotPresent:0.0.0.0"
        writeDynamicVariable("secondaryInfo", step2)
    }
}


#STEP 3 -- Collect HA state

# secondaryInfo example: CP_R80.20_MDS1_CMA1_Server:10.11.94.74:CP_R80.20_MDS1_CMA1_Server_2:10.11.94.75
# secondaryInfo example: CP_R80.20_MDS1_CMA2_Server:10.11.94.76:CP_R80.20_MDS1_CMA2_Server_2:10.11.94.77