from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class CpstatVsec(BaseParser):


    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):

        # Step 1 : Data Extraction
        data = helper_methods.parse_data_as_list(raw_data, 'checkpoint_gaia_cloudguard.textfsm')

        # Step 2 : Data Processing
        if data:
            try:
                if ('on' in data[0]['cloudguard_status'])\
                    or ('enabled' in data[0]['cloudguard_status']):
                    cloudguard_status = 'true'
                else:
                    cloudguard_status = 'false'

                # Step 3 : Data Reporting
                self.write_tag('cloudguard-controller',cloudguard_status )
        
            except KeyError:
                pass

        return self.output
