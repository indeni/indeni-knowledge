# Checkpoint labels all of its certs with it's own "Status": Pending, Valid, Renewed, Expired, or Revoked. (Note: pretty
# sure this is a Checkpoint thing, not an X.509 thing). This script only looks at Expired, Valid and Renewed certs
# (we think that "Renewed" certs are just certs that, for a given Subject, have been revoked or expired and subsequently
# re-issued). We are intentionally ignoring Revoked and Pending certs, since we don't care about the expiration dates of
# those certs. See also https://indeni.atlassian.net/browse/IKP-1525.


# NOTE: This file and cpca_client-lscert-mds.ind have very similar code. If you edit the code here, please look at the
# other file to see if the changes apply there as well.

# This function has a SIDE EFFECT: it uses 'getline'
function getExpirationDate() {
    #Not_Before: Sat Jul  9 11:21:26 2016   Not_After: Fri Jul  9 11:21:26 2021
    getline  # SIDE EFFECT

    expire_month = $(NF - 3)
    expire_day = $(NF - 2)
    expire_time = $(NF - 1)
    expire_year = $NF

    sub(/,/, "", expire_year)   # Sometimes a comma follows the year

    return date(expire_year, parseMonthThreeLetter(expire_month), expire_day)
}

# A Checkpoint device can have many certs with the same name (CN) and status. In that case, choose the one with the
# _latest_ expiration date.
function handleDuplicateName(name, certs, new_expiration) {
    if (name in certs) {
        existing_expiration = certs[name]
        if (new_expiration > existing_expiration)
            certs[name] = new_expiration
    } else {
        certs[name] = new_expiration
    }
}

BEGIN {
    # These statements 'declare' (create) these empty arrays before we actually use them, so that we can pass them into
    # 'handleDuplicateName'. This is just a hack/convention to declare arrays in awk.
    split("", expired_certs)
    split("", report_these_certs)
}

# Info for each certificate comes from three lines in a row. This script matches only on the first of the three lines,
# then uses 'getline' to process the entire record, e.g:

#Subject = CN=lab-CP-GW4 VPN Certificate,O=lab-CPMGMTR7730..9uifq5
#Status = Valid   Kind = IKE   Serial = 49229   DP = 1
#Not_Before: Mon Dec 26 10:47:02 2016   Not_After: Sun Dec 26 10:47:02 2021
/^Subject = / {

    # First, in case the cert name has spaces, we split on $0 and not $3.
    # Splitting on '=' gives us most of the string, but then we need to deal with the trailing ',X', where X can be any
    # of the X.509 acronyms (usually 'O=...' but I've seen 'OU=...'). So we need to further parse. AWK's string
    # manipulation support is poor, so this is a bit of a hack, but it seems to work.
    split($0, name_array, "=")  # split on the whole line: CNs can have spaces
    CN = name_array[3]
    split(CN, cn_arr, ",")
    last_str_len = length(cn_arr[arraylen(cn_arr)])  # in case there are multiple commas in the cert name...
    offset_from_end = last_str_len + 1  # do NOT combine this arithmetic with the next line. Scary JAWK bug...
    CN = substr(CN, 1, length(CN) - offset_from_end)

    #Status = Valid   Kind = SIC   Serial = 2562   DP = 0
    getline
    status = $3
    if (status == "Expired") {
        handleDuplicateName(CN, expired_certs, getExpirationDate())
    } else if (status == "Valid" || status == "Renewed") {
        handleDuplicateName(CN, report_these_certs, getExpirationDate())
    } # Note: if status is anything but Expired, Valid, or Renewed, just ignore it and do nothing.
}


END {
    # Only report on expired certs if they _don't_ have a valid corollary.
    for (expired_name in expired_certs) {
        if (!(expired_name in report_these_certs))
            report_these_certs[expired_name] = expired_certs[expired_name]
    }

    for (cert_name in report_these_certs) {
        cert_tags["name"] = cert_name
        cert_expiration = report_these_certs[cert_name]
        writeDoubleMetric("certificate-expiration", cert_tags, "gauge", cert_expiration, "true", "Certificate Expiration", "date", "name")  # Converted to new syntax by change_ind_scripts.py script
    }
}