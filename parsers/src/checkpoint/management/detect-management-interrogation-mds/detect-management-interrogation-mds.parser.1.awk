function setNameSetData () {
    dataName = $1
    gsub(":", "", dataName)

    # Set the data variable
    data = $2
    gsub("\\(", "", data)
    gsub("\\)", "", data)
    gsub("\"", "", data)
}


#| CMA |MDM-VSX_Management_Server | 10.10.6.14      | up 1531    | up 1616  | up 1493  | up 1720  |
/^\| CMA \|/ {
    vsName = $3

    # Remove starting "|"
    gsub(/\|/, "", vsName)
}

#:MySICname ("CN=lab-CP-GW2-R7730,O=lab-CP-MGMT-R7730..o2sn6g")
#:MySICname ("cn=cp_mgmt,o=lab-CP-MGMT-R7730-PRIMARY..smyyhc")
#:MySICname ("CN=cp_mgmt_lab-CP-MGMT-R7730-HA,O=lab-CP-MGMT-R7730-PRIMARY..smyyhc")
/:MySICname/ {
    # To determine SIC name

    split($0, lineArr, /(CN\=|cn\=|,O\=|,o\=|\.\.)/)

    sicName = lineArr[2]
    gsub(/^cp_mgmt_/, "", sicName)

    # If the sic name is only "cp_mgmt" then we neeed to check the O instead of the CN to get the name of the device
    if (sicName == "cp_mgmt") {
        sicName = lineArr[3]
    }
}

######## parsing C files ########

##############################
# The C files in check point consists of data, stored in sections. Each data has a name and a value. Each section has only a name.
# The sections are in hierarchies, and thus a section can contain multiple sub-section
# The section names can be in different formats, so we match against all of them
#############################

# This regex matches all the relevant section headers:
#
# Name example 1:
# :ike_p1 (
#
# Name example 2:
# : (MyIntranet
# : (ReferenceObject
#
# Name example 3:
# :ike_p1_dh_grp (ReferenceObject
# Any line with an ":" followed by any characters then a space, followed by a "(" but not ending with a ")"
#
# We are only looking for sections of the 2nd level in the hierarchy (one tab)
#
/^\t:.+\([^)]*$/ {
    sectionName = $1
    gsub(":", "", sectionName)
    sectionName = trim(sectionName)

    # meaning this line follows the 2nd name example
    if (sectionName == "") {
        sectionName = $2
        gsub("\\(", "", sectionName)
        sectionName = trim(sectionName)
    }
    next
}

#:management (true)
/:management / {
    # set variables "data" and "dataName"
    setNameSetData()

    # If the value is "true" and it is found under this devices configuration, then this device is a management server
    # To find out if the device in the config that has ":management (true) is the local device the script checks two things.
    # 1. Does the name in the local SIC certificate have the same name
    # 2. Does the CMA name in the mdsstat output have the same name
    if ( (sectionName == sicName || sectionName == vsName ) && data == "true") {
        writeTag("role-management", "true")
        # we found the relevant tag, no point in further parsing
        exit
    }
}