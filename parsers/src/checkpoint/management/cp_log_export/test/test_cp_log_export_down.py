import os
import unittest

from checkpoint.management.cp_log_export.cp_log_export_down import CpLogExport
from parser_service.public.action import WriteDoubleMetric


class TestCpLogExport(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = CpLogExport()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_valid_input_healthy(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/valid_input_healthy.input', {}, {})

        # Assert
        self.assertEqual(2, len(result))

        self.assertTrue(isinstance(result[0], WriteDoubleMetric))
        self.assertEqual(result[0].name, 'log-exporter-status')
        self.assertEqual(result[0].tags['name'], 'test domain-server - Domain_1_Server')
        self.assertEqual(result[0].value, 1)

    def test_valid_input_one_bad_exporter(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/valid_input_one_bad_exporter.input', {}, {})

        # Assert
        self.assertEqual(2, len(result))

        self.assertTrue(isinstance(result[1], WriteDoubleMetric))
        self.assertEqual(result[1].name, 'log-exporter-status')
        self.assertEqual(result[1].tags['name'], 'test domain-server - Domain_2_Server')
        self.assertEqual(result[1].value, 0)


if __name__ == '__main__':
    unittest.main()
