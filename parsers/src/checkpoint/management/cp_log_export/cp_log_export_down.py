from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class CpLogExport(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        # Step 1 : Data Extraction
        if raw_data:
            data = helper_methods.parse_data_as_list(raw_data, 'checkpoint_gaia_cp_log_export.textfsm')
            if data:
                # Step 2: Data reporting
                for line in data:
                    tags = {'name': str(line['Name']).replace(': :', ' -'),
                            'last log read at': line['Last_log'],
                            'debug file': line['Debug_file']}
                    self.write_double_metric('log-exporter-status', tags, 'gauge', 0 if 'Running (' not in line['Status'] else 1, False, "name")
        return self.output
