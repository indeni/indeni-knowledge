function addVsTags(tags) {
    tags["vs.ip"] = vs_ip
    tags["vs.name"] = vs_name
}

function dumpLicense() {
    addVsTags(t)
    writeComplexMetricObjectArray("licenses", t, licenses, "false")  # Converted to new syntax by change_ind_scripts.py script
    # Reset arrays and variables
    delete licenses
    ilicense = ""
    features = ""
    ifeature = ""
    delete features_arr
}

BEGIN {
    FS = "[ |]+"
}

#pre R80 output
#| CMA |MDM-VSX_Management_Server | 10.10.6.14      | up 1531    | up 1616  | up 1493  | up 1720  |
#R80 and R80.10 output
#| CMA | MDM-VSX_Management_Server | 10.10.6.14      | up 1531    | up 1616  | up 1493  | up 1720  |
#R80.20 output
#| CMA  | CP_R80.20_MDS1_CMA1_2    | 10.11.94.76     | up 30113    | up 30081    | up 29878    | up 30917    |
/^\| CMA/ {
    if (vs_name != "") {
    # write the previous vs's data
        dumpLicense()
    }
    vs_name = $3
    vs_ip = $4
}

#192.168.250.5    never       aMnLnwjT6HedbJTAehzPVrt3f7YejRc2iQij  ::CK-00-1C-7F-3E-CB-38 fw1:6.0:swb evnt:6.0:smrt_evnt fw1:6.0:fwc fw1:6.0:ca fw1:6.0:rtmui fw1:6.0:sstui fw1:6.0:fwlv fw1:6.0:cmd evnt:6.0:alzd5 evnt:6.0:alzc1 evnt:6.0:alzs1 fw1:6.0:fwc fw1:6.0:ca fw1:6.0:rtmui fw1:6.0:sstui fw1:6.0:fwlv fw1:6.0:cmd evnt:6.0:alzd5 evnt:6.0:alzc1 evnt:6.0:alzs1 fw1:6.0:swb fw1:6.0:cluster-1 fw1:6.0:swb fw1:6.0:cpxmgmt_qos_u_sites fw1:6.0:sprounl
/fw1:/ {
    ilicense++
    licenses[ilicense, "ip"] = $1
    licenses[ilicense, "expiration"] = $2
    licenses[ilicense, "signature"] = $3
    licenses[ilicense, "ck"] = $4
    features = $4
    for (i = 5; i <= NF; i++) {
        ifeature++
        features_arr[ifeature, "name"] = $i
        features = sprintf("%s %s", features, $i)
    }
    licenses[ilicense, "features"] = features
}

END {
    dumpLicense()
}