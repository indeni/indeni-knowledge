from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class MonitorAdminLoginTableLockedObjects(BaseParser):

    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        data = helper_methods.parse_data_as_list(raw_data, 'monitor_admin_login_table_locked_objects.textfsm')
        if data:
            self.write_double_metric('monitor-admin-login-table-locked-objects', {}, 'gauge', 1 if data[0]['total_rows'] != '0' else 0, False)
        return self.output