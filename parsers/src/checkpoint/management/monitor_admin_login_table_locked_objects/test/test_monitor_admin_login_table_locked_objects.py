import os
import unittest
from checkpoint.management.monitor_admin_login_table_locked_objects.monitor_admin_login_table_locked_objects import MonitorAdminLoginTableLockedObjects

class TestMonitorAdminLoginTableLockedObjects(unittest.TestCase):

    def setUp(self):
        self.parser = MonitorAdminLoginTableLockedObjects()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_monitor_admin_login_table_locked_objects_empty(self):
        result = self.parser.parse_file(self.current_dir + '/monitor_admin_login_table_locked_objects_empty.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'monitor-admin-login-table-locked-objects')
        self.assertEqual(result[0].value, 0)

    def test_monitor_admin_login_table_locked_objects(self):
        result = self.parser.parse_file(self.current_dir + '/monitor_admin_login_table_locked_objects.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'monitor-admin-login-table-locked-objects')
        self.assertEqual(result[0].value, 1)

if __name__ == '__main__':
    unittest.main()