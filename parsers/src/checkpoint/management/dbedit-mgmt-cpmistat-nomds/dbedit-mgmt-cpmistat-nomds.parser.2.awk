/^Result/{
    if (NF == 5) {
        step2 = $2 ":" $3 ":" $4 ":" $5
        writeDynamicVariable("secondary_info", step2)
    }
    else {
        step2 = $2 ":" $3 ":NotPresent:0.0.0.0"
        writeDynamicVariable("secondary_info", step2)
    }
}


#STEP 3 -- Collect HA state

# secondary_info example: cp-lab-MGMTR8010:192.168.194.85:lab-cp-MGMTR8010:192.168.194.93