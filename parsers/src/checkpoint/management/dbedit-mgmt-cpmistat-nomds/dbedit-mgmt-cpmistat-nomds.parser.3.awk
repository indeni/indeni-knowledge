function addVsTags(tags) {
    tags["vs.ip"] = primary_ip
    tags["vs.name"] = primary
}

BEGIN {
    exiting = 1
}


#cpmistat -o schema -r mg lab-cp-MGMTR8010  HAip: 192.168.194.93  vsIP: 192.168.194.85  vsName: cp-lab-MGMTR8010 Status: 1 Sync: Synchronized
/^cpmistat -o schema -r mg/ {

    exiting = 0

    secondary = $6
    primary = $12
    primary_ip = $10
    status = $14
    line = $0
    status_message = substr(line, index(line, $16))

    host_and_vs = secondary ";" primary
    status_arr[host_and_vs] = primary_ip ";" status ";" status_message

    # Reset variables
    status_message = ""
}


END {
     if (exiting)
            exit

    for (id in status_arr) {
        split(status_arr[id], split_data_arr, ";")
        split(id, split_id_arr, ";")
        secondary = split_id_arr[1]
        primary = split_id_arr[2]
        primary_ip = split_data_arr[1]
        status = split_data_arr[2]
        status_message2 = split_data_arr[3]
        if (status < 2) {
            addVsTags(t)
            t["name"] = secondary
            writeDoubleMetric("mgmt-ha-sync-state", t, "gauge", status, "false")  # Converted to new syntax by change_ind_scripts.py script
            writeComplexMetricString("mgmt-ha-sync-state-description", t, status_message2, "false")  # Converted to new syntax by change_ind_scripts.py script
        }
    }
}