echo -e "query network_objects, (management='true') & (primary_management='true')\n-q\n" | ${nice-path} -n 15 dbedit -local | awk '/Object Name:/ {print "CMA: "$3} /^    ipaddr\:/ {print $2}' |  awk '!a[$0]++' | awk 'NF > 0' | xargs -n 3 echo

#CMA: cp-lab-MGMTR8010 192.168.194.85