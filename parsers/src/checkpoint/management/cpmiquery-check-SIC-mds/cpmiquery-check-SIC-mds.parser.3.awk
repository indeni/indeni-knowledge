function addVsTags(tags) {
    tags["vs.ip"] = vsIp
    tags["vs.name"] = vsName
}

BEGIN {
    status = 0
}

# Result ip: 10.11.94.72 hostname: CP-R80.20-VSX1-1 SICstate: OK vsName: CP_R80.20_MDS1_CMA1_Server vsIp: 10.11.94.74
/^Result/ {
    if ($7 == "OK") {status = 1}
    else {status = 0}
    ip = $3
    hostname = $5
    vsName = $9
    vsIp = $11
    statusArr = ip " " vsIp " " vsName " " hostname " " status
}

END{
    split(statusArr,splitArr," ")
    t["name"] = splitArr[4]
    t["ip"] = splitArr[1]
    addVsTags(t)
    status = splitArr[5]
    writeDoubleMetric("trust-connection-state", t, "gauge", status, "false")  # Converted to new syntax by change_ind_scripts.py script
}