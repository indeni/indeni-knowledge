############
# Why: Check if the device is a CheckPoint management server, to determine if we should run scripts management scripts against it.
# How: Using cpprod_util
###########

/1/ {
	writeTag("role-management", "true")
}