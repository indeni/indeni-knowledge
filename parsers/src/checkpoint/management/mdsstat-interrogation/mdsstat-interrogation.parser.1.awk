############
# Why: Check if the device is running MDS(Provider-1) or MLM
# How: Check if command "mdsstat" gives any relevant output.
###########

BEGIN {
    FS = "|"
}

#| MDS |  -  | 10.10.6.13      | up 15549   | up 15547 | up 15544 | up 15638 |
#| MDS | -   | 10.11.94.100    | up 12587   | up 12580 | up 5570  | N/R      |
/^\s*\|\s*MDS/ {
    #check whether its MDS or MLM and add respective tags
    #We can distinguish between MDS and MLM by checking cpca status in mdsstat output. If the value is "N/R"
    #( N/R:The process is not relevant), then we can add a tag that its MLM , if not add a tag that its MDS
    cpca = $8
    if (cpca ~ "N/R") {
        writeTag("mlm", "true")
    }
    #Writing default MDS tag so that the existing script which uses mds wont be affected
    writeTag("mds", "true")

    # MDS has virtual systems, like VSX
    writeTag("vsx", "true")
}

#| CMA |  R7730_Management_Server       | 10.10.6.10      | up 26197   | up 26156 | up 26088 | up 26345 |
/CMA.*[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}/ {
    vs_count++
}

END {
    writeTag("vs-count", vs_count)
    writeTag("license-vs-ratio", "1")
}