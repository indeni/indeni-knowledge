######## parsing C files ########

##############################
# The C files in check point consists of data, stored in sections. Each data has a name and a value. Each section has only a name.
# The sections are in hierarchies, and thus a section can contain multiple sub-section
# The section names can be in different formats, so we match against all of them
# Since it is important to know how far down in the hierarchy we are, we also store that.
#############################

## Section name sections
## Here we will set the section name and on which level they are

# Name format 1
# :ike_p1 (
/:.+ \($/ {

    sectionName = $1

    # Removing junk
    sub(":", "", sectionName)
    sectionName = trim(sectionName)

    # Will count nr of tabs to see on which level we are
    sectionDepth = gsub(/\t/, "")
    sectionDepth++

    # Array to look up name
    sectionArray[sectionDepth] = sectionName
}


# Name example 2
# : (MyIntranet
# : (ReferenceObject
/: \(.+$/ {

    sectionName = $2

    # Removing junk
    gsub("\\(", "", sectionName)
    sectionName = trim(sectionName)

    # Will count nr of tabs to see on which level we are
    sectionDepth = gsub(/\t/, "")
    sectionDepth++

    # Array to look up name
    sectionArray[sectionDepth] = sectionName
}



#Name example 3
# :ike_p1_dh_grp (ReferenceObject
# Any line with an ":" followed by any characters then a space, followed by a "(" but not ending with a ")"
/:.+ \(.[^)]*$/ {

    sectionName = $1

    # Removing junk
    gsub(":", "", sectionName)
    sectionName = trim(sectionName)

    # Will count nr of tabs to see on which level we are
    sectionDepth = gsub(/\t/,"")
    sectionDepth++

    # Array to look up name
    sectionArray[sectionDepth] = sectionName
}

#Name example 4
# (
/^\($/ {

    # Will count nr of tabs to see on which level we are
    sectionName = ""
    sectionDepth = gsub(/\t/, "")
    sectionDepth++
}

# Decrease section depth by 1
# Match for a tab with a ")" after it and then line end
#	)
/\t\)$/ {
    # Tracks which level we are in the sections.
    # We encountered a ) and thus we are one level higher
    sectionDepth--
}

# Decrease section depth by 1
# Match for a tab with a ")" after it and then line end
# )
/^\)$/ {
    # Tracks which level we are in the sections.
    # We encountered a ) and thus we are one level higher
    sectionDepth--
}

#:management (true)
/:management / {
    data = $2
    gsub(/[()\"]/, "", data)
    
    # If the value is "true" and the sic name ~matches~ the netobj_objects.C section name, then this device is a
    # management server
    if (data == "true") {
        writeManagementTagTrue = 0
        if (sectionArray[2] == sicName) {
            writeManagementTagTrue = 1
        } else {
            if (split(sicName, sicParts, ".")) {
                if (sicParts[1] == sectionArray[2])
                    writeManagementTagTrue = 1
            }
        }

        if (writeManagementTagTrue == 1)
            writeTag("role-management", "true")
    }
}


#:MySICname ("CN=lab-CP-GW2-R7730,O=lab-CP-MGMT-R7730..o2sn6g")
#:MySICname ("cn=cp_mgmt,o=lab-CP-MGMT-R7730-PRIMARY..smyyhc")
#:MySICname ("CN=cp_mgmt_lab-CP-MGMT-R7730-HA,O=lab-CP-MGMT-R7730-PRIMARY..smyyhc")
/:MySICname/ {
    # To determine SIC name

    # extract CN and remove junk
    split($2, sicNameArr, ",")
    sicName = sicNameArr[1]
    gsub(/^\(\"CN=|^\(\"cn=/, "", sicName)
    gsub(/^cp_mgmt_/, "", sicName)

    # If the sic name is only "cp_mgmt" then we neeed to check the O instead of the CN to get the name of the device
    if (sicName == "cp_mgmt") {
        sicName = sicNameArr[2]
        gsub(/O=|o=|\.\..*/, "", sicName)
    }
}