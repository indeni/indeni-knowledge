#Object Name: lab-CP-GW5-1
/^Object Name:/ {
	gateway_name = $3
}

#    ipaddr: 192.168.194.87
/^    ipaddr\:/ {
    id++
    known_devices[id, "name"] = gateway_name
    known_devices[id, "ip"] = $2

}

END {
    writeComplexMetricObjectArray("known-devices", null, known_devices, "false")  # Converted to new syntax by change_ind_scripts.py script
}