# 			:http_max_concurrent_connections (1000)
/[0-9]/ {
	propname=$1
	gsub(/\:/, "", propname)
	paramtags["name"] = propname

	propvalue=$2
	gsub(/(\(|\))/, "", propvalue)

    writeDoubleMetric("global-property-value", paramtags, "gauge", propvalue, "true", "Global Properties", "number", "name")  # Converted to new syntax by change_ind_scripts.py script
}