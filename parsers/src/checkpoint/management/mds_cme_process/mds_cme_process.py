from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class MdsCmeProcessStatus(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        # cme: unrecognized service
        # cme module not loaded
        # cme is stopped
        # cme (pid 7301) is running...
        if raw_data and raw_data.strip() != '':
            state = -1
            if 'cme is stopped' in raw_data:
                state = 0
            elif 'is running' in raw_data:
                state = 1

            if state != -1:
                self.write_double_metric('mds_cme_process_status', {}, 'gauge', state, True, 'MDS CME Status', 'state')

        return self.output