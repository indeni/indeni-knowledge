import os
import unittest

from checkpoint.management.mds_cme_process.mds_cme_process import MdsCmeProcessStatus
from parser_service.public.action import WriteDoubleMetric


class TestMdsCmeProcessStatus(unittest.TestCase):
    def setUp(self):
        self.parser = MdsCmeProcessStatus()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))


    def test_MdsCmeProcessStatus_up(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/mds_cme_process_up.input', {}, {})

        # Assert
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].name, 'mds_cme_process_status')
        self.assertEqual(result[0].value, 1)


    def test_MdsCmeProcessStatus_down(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/mds_cme_process_down.input', {}, {})

        # Assert
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].name, 'mds_cme_process_status')
        self.assertEqual(result[0].value, 0)

    def test_MdsCmeProcessStatus_missing(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/mds_cme_process_missing.input', {}, {})

        # Assert
        self.assertEqual(0, len(result))
