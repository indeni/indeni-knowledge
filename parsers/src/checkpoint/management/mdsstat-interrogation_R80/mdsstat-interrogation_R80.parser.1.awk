############
# Why: Check if the device is running as MDS(Provider-1) or MLM
# How: Check if command "mdsstat" gives output that indicates an MDS environment.
###########

# When reading this script, keep in mind that whether we're interrogating the actual MDS host or one of the CMAs, the
# "SSH context" is the same. I.e., when the SSH command executes, it doesn't (can't) know whether it is hitting
# the actual host IP or one of the CMA IPs. So, during interrogation, we don't actually make a distinction between them:
# whether it's the host or a CMA, we run exactly the same script (this one).

BEGIN {
    FS = "|"
}


#| MDS |  -  | 10.10.6.13      | up 15549   | up 15547 | up 15544 | up 15638 |
#|MDS|  -  | 10.10.6.13      | up 15549   | up 15547 | up 15544 | up 15638 |
#| MDS  |          -     | 10.11.94.100    | up 12587 | up 12580 | up 5570  | N/R |
# DON'T MATCH:
#| CMA | Example_VS_1     | 192.168.194.52  | up 6483    | up 6450  | up 6413  | up 10054 |

# R81.10
#+------+-----------------------------------------------------+-----------------+-------------+-------------+-------------+-------------+-------------+
#| Type | Name                                                | IP address      | FWM         | FWMHA       | FWD         | CPD         | CPCA        |
#+------+-----------------------------------------------------+-----------------+-------------+-------------+-------------+-------------+-------------+
#| MDS  |                          -                          | 11.222.33.20    | up 93833    | up 93838    | up 93831    | up 20199    | N/R         |

/^\s*\|\s*MDS/ {
    #check whether its MDS or MLM and add respective tags
    #We can distinguish between MDS and MLM by checking cpca status in mdsstat output. If the value is "N/R"
    #( N/R:The process is not relevant), then we can add a tag that its MLM , if not add a tag that its MDS
    cpca = $(NF-1)
    if (cpca ~ "N/R") {
        writeTag("mlm", "true")
    }
    #Writing default MDS tag so that the existing script which uses mds wont be affected
    writeTag("mds", "true")
    # Whether this is CMA or actual MDS host, it's a "management server"
    writeTag("role-management", "true")
    # MDS is a virtual system (CMAs) like VSX
    writeTag("vsx", "true")

    is_mds_or_mlm = 1
}

#| CMA | R7730_Management_Server | 10.10.6.10  | up 26197  | up 26156 | up 26088 | up 26345 |
/CMA.*[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}/ {
    vs_count++
}

END {
    # don't write these tags for a non-MDS environment
    if (is_mds_or_mlm) {
        writeTag("vs-count", vs_count)
        writeTag("license-vs-ratio", "1")
    }
}