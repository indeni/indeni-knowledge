from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class PdpdStatusShow(BaseParser):

    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        # Step 1 : Data Extraction
        data = helper_methods.parse_data_as_list(raw_data, 'checkpoint_gaia_cloud_guard_web_api_mds.textfsm')

        # Step 2 : Data Processing
        if data:
            for cma in data:
                tags = {}
                if (cma['protocol'] == "tcp" and cma['dst_port'] == "5908" and cma['state'] == "LISTEN"):
                    web_api_status = 1
                else:
                    web_api_status = 0
                if (cma['pdpd_daemon_start_time'] != "") :
                    pdpd_daemon_status = 1
                else:
                    pdpd_daemon_status = 0
                
            # Step 3 : Data Reporting
            tags['vs.name'] = cma['cma_name']
            tags['gw.name'] = cma ['gw_name']
            tags['gw.ip'] = cma ['gw_ip']
            self.write_double_metric('pdpd-daemon-status', tags, 'gauge', pdpd_daemon_status, True, 'CloudGuard - Identity Awareness PDPD daemon status:', 'state', "vs.name|gw.name|gw.ip")
            self.write_double_metric('web-api-status', tags, 'gauge', web_api_status, True, 'CloudGuard - Identity Awareness Web API status:', 'state', "vs.name|gw.name|gw.ip")
        return self.output
