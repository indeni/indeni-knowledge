from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class ApiStatus(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        # Step 1 : Data Extraction
        if raw_data:
            data = helper_methods.parse_data_as_list(raw_data, 'checkpoint_api_status.textfsm')
            if data:
                tags = {
                    'api-type': 'Management',
                    'data': 'Enabled'
                }
                self.write_double_metric('management-api-enabled', tags, 'gauge', 1 if data[0]['automatic_start'] == 'Enabled' else 0, True, 'API', 'state', 'api-type|data')
                tags['data'] = 'Status'
                self.write_double_metric('management-api-status', tags, 'gauge', 1 if data[0]['readiness'] == 'SUCCESSFUL' else 0, True, 'API', 'state', 'api-type|data')
        return self.output
