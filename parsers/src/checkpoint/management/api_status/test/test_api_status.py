import os
import unittest

from checkpoint.management.api_status.api_status import ApiStatus
from parser_service.public.action import *

class TestApiStatus(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = ApiStatus()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_api_status_successful(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/api_status_successful.input', {}, {})
        # Assert
        self.assertEqual(2, len(result))
        self.assertEqual(result[0].name, 'management-api-enabled')
        self.assertEqual(result[0].value, 1)
        self.assertEqual(result[0].tags['api-type'], 'Management')
        self.assertEqual(result[0].tags['data'], 'Enabled')
        self.assertEqual(result[1].name, 'management-api-status')
        self.assertEqual(result[1].value, 1)
        self.assertEqual(result[1].tags['api-type'], 'Management')
        self.assertEqual(result[1].tags['data'], 'Status')

    def test_api_status_failed(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/api_status_failed.input', {}, {})
        # Assert
        self.assertEqual(2, len(result))
        self.assertEqual(result[0].name, 'management-api-enabled')
        self.assertEqual(result[0].value, 1)
        self.assertEqual(result[0].tags['api-type'], 'Management')
        self.assertEqual(result[0].tags['data'], 'Enabled')
        self.assertEqual(result[1].name, 'management-api-status')
        self.assertEqual(result[1].value, 0)
        self.assertEqual(result[1].tags['api-type'], 'Management')
        self.assertEqual(result[1].tags['data'], 'Status')


if __name__ == '__main__':
    unittest.main()