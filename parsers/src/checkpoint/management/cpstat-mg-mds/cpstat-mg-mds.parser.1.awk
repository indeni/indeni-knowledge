############
# ToDo: Add the status message to a descriptive tag in the future
###########

function reset_vars() {
    vs_name = ""
    vs_ip = ""
    is_started = ""
    active_status = ""
    status = ""
    CPM_status = 0
    DMS_status = 0
}

function addVsTags(tags) {
    tags["vs.ip"] = vs_ip
    tags["vs.name"] = vs_name
}

function dumpStatus() {
    addVsTags(t)

    overallStatus = 0

    if ((status == "OK") || ((CPM_status == 1) && (DMS_status == 1)))
        overallStatus = 1

    if ( is_started == 1 && ( active_status == "active" || active_status == "standby" ) && overallStatus == 1 ) {
        writeDoubleMetric("mgmt-status", t, "gauge", "1", "true", "Management Services Status", "state", "vs.name")
    } else {
        writeDoubleMetric("mgmt-status", t, "gauge", "0", "true", "Management Services Status", "state", "vs.name")
    }
    # clear tags and variables
    delete t
    reset_vars()
}

BEGIN {
    FS = "[ |]+"
    reset_vars()
}

/Checking status of Domain Management Server/ {
    if (vs_name != "") {
        # write the previous mds's data
        dumpStatus()
    }
}

#pre R80 output
#| CMA |MDM-VSX_Management_Server | 10.10.6.14      | up 1531    | up 1616  | up 1493  | up 1720  |
#R80 and R80.10 output
#| CMA | MDM-VSX_Management_Server | 10.10.6.14      | up 1531    | up 1616  | up 1493  | up 1720  |
#R80.20 output
#| CMA  | CP_R80.20_MDS1_CMA1_2    | 10.11.94.76     | up 30113    | up 30081    | up 29878    | up 30917    |
/^\| CMA/ {
    if (vs_name != "") {
        # write the previous mds's data
        dumpStatus()
    }
    vs_name = $3
    vs_ip = $4
    # Remove starting "|"
    #gsub(/\|/, "", vs_name)
}

#Is started:    1
/Is started/ {
    split($0, split_arr, ":")
    is_started = trim(split_arr[2])
}

#Active status: active
/Active status/ {
    split($0, split_arr, ":")
    active_status = trim(split_arr[2])
}

#Status:        OK
/Status/ {
    split($0, split_arr, ":")
    status = trim(split_arr[2])
}

/CPM: Check Point Security Management Server is running and ready/ {
    CPM_status = 1
}

# Total Domain  1     1 up   0 down
/Total Domain Management Servers checked:/ {
    if ($(NF-1) == 0)
        DMS_status = 1
}

END {
    dumpStatus()
}