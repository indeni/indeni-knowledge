############
# Script explanation: This script is a bit complicated. Here is how it works
# Step 1: List all CMA
# Step 2: For each CMA, list all secondary management servers by:
#		Parse netobj_objects.C and find all hosts that have the following two properties:
#		:primary_management (false)
#		:management (true)
# Step 3: For each of the devices found in step 2, run cpmistat command against them.
# To be able to query as many devices as possible, and have everything ready within the 30s ind script timeout, run cpmistat in parallel (since it has a 35s timeout), and record the PID of each command so we can kill them all after 20s.
###########

function addVsTags(tags) {
    tags["vs.ip"] = vsIp
    tags["vs.name"] = vsName
}


# Process cpmistat -o schema -r mg lab-CP-MGMT-R7730-HA started for MDS: lab-CP-MGMT-R7730_Management_Server IP: 10.10.6.10 with pid: 28708
/^Process \"cpmistat -o schema -r mg/ {
    # We have attempted to connect to this host. If we do not get a valid response we need to mark this one as fail.
    hostname = $7
    vsName = $11
    vsIp = $13
    status = 0
    statusMessage = "nomessage"

    # Hostname can be the same for several CMAs, combining with VSNAME to have unique index
    iIndex = hostname ";" vsName
    statusArr[iIndex] = vsIp ";" status ";" statusMessage
}

# :mgStatusOK (0)
# :mgStatusOK (1)
/:mgStatusOK/ {
    # We got some result from the attempt, but we dont know from who
    status = $2
    gsub(/^\(/,"",status)
    gsub(/\)$/,"",status)
}

# :mgSyncStatus (Lagging)
# :mgSyncStatus (Synchronized)
# :mgSyncStatus ("N/R (Self synchronization is not relevant)")
/:mgSyncStatus/ {
    # We got some result from the attempt, but we dont know from who
    # We will store the message in the meantime
    split($0,splitArr,"mgSyncStatus")
    statusMessage = trim(splitArr[2])
    gsub(/^\(/,"",statusMessage)
    gsub(/\)$/,"",statusMessage)
    gsub(/\"/,"",statusMessage)
}


# cpmistat -o schema -r mg lab-CP-MGMT-R7730-HA vsIP: 10.10.6.10 vsName: lab-CP-MGMT-R7730_Management_Server
/^cpmistat -o schema -r mg/ {
    # We now know from who the above result was from
    hostname = $6
    vsName = $10
    vsIp = $8

    iIndex = hostname ";" vsName
    statusArr[iIndex] = vsIp ";" status ";" statusMessage

    # Reset variables
    statusMessage = ""
    status = ""
}


END {
    for (id in statusArr) {
        split(statusArr[id],splitDataArr,";")
        split(id,splitIdArr,";")
        hostname = splitIdArr[1]
        vsName = splitIdArr[2]
        vsIp = splitDataArr[1]
        status = splitDataArr[2]
        statusMessage = splitDataArr[3]
        if (status < 2) {
            addVsTags(t)
            t["name"] = hostname
            writeDoubleMetric("mgmt-ha-sync-state", t, "gauge", status, "false")  # Converted to new syntax by change_ind_scripts.py script
            writeComplexMetricString("mgmt-ha-sync-state-description", t, statusMessage, "false")  # Converted to new syntax by change_ind_scripts.py script
        }
    }
}