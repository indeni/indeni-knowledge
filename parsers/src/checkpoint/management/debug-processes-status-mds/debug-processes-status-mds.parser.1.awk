
function addVsTags(tags) {
    tags["vs.name"] = vsname
    tags["vs.filename"] = filename
}

BEGIN {
    vsname = ""
}

#_CMA:CP_R80.20_MDS1_CMA1_1
/_CMA:/ {
    split($0, split_arr_vsname, ":")
    vsname = trim(split_arr_vsname[2])
}


#      1 /opt/CPmds-R80.20/customers/CP_R80.20_MDS1_CMA1_1/CPshrd-R80.20/log/cpd.elg
#      1 /opt/CPmds-R80.20/customers/CP_R80.20_MDS1_CMA1_1/CPsuite-R80.20/fw1/log/cplog_debug.elg
#      1 /opt/CPmds-R80.20/customers/CP_R80.20_MDS1_CMA1_1/CPsuite-R80.20/fw1/log/fwd.elg
#      1 /opt/CPmds-R80.20/customers/CP_R80.20_MDS1_CMA1_1/CPsuite-R80.20/fw1/log/fwm.elg
/.elg/{
    filename = $2
    addVsTags(vstags)
    log_files = $1

    writeDoubleMetric("debug-file-counter", vstags, "gauge", log_files, "false")
}
