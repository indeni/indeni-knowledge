BEGIN {
    status = 0
}

#Result ip: 10.0.2.34 hostname: GW11 SICstate: OK
/^Result/ {
    if ($7 == "OK")
        status = 1
    else
        status = 0

    t["ip"] = $3
    t["name"] = $5
    writeDoubleMetric("trust-connection-state", t, "gauge", status, "false")  # Converted to new syntax by change_ind_scripts.py script
}