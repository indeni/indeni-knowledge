from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class CpstatVsec(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:

            global_data = helper_methods.parse_data_as_object(raw_data, 'checkpoint_gaia_cpstat_vsec_global_values.textfsm')
            if global_data:
                tags = {}
                self.write_double_metric('cloudguard-controller-status', tags, 'gauge', 0 if 'off' in global_data['vsec_controller_status'] else 1, True, 'CloudGuard - Controller Status','state')
                self.write_double_metric('cloudguard-controller-disconnected-datacenters', tags, 'gauge', int(global_data['disconnected_dc']), True, 'CloudGuard - Number of Data Centers disconnected','number')
                self.write_double_metric('cloudguard-controller-total-datacenters', tags, 'gauge', int(global_data['total_dc']), True, 'CloudGuard - Number of Data Centers','number')
                self.write_double_metric('cloudguard-controller-imported-objects', tags, 'gauge', int(global_data['imported_dc_objects']), True, 'CloudGuard - Number of imported Data Center objects','number')
                self.write_double_metric('cloudguard-controller-gateways-enforcing-objects', tags, 'gauge', int(global_data['gws_enforcing_objects']), True, 'CloudGuard - Number of gateways enforcing Data Center objects','number')

            dc_data = helper_methods.parse_data_as_list(raw_data, 'checkpoint_gaia_cpstat_vsec_datacenters_info.textfsm')
            if dc_data:
                for dc in dc_data:
                    tags = {}
                    tags['dc.name'] = dc['dc_name'].strip()
                    tags['dc.type'] = dc['dc_type'].strip()
                    self.write_double_metric('cloudguard-controller-standby', tags, 'gauge', 1 if 'standby' in dc['vsec_controller_status'] else 0, True, 'CloudGuard - Controller Standby','state', 'dc.name|dc.type')
                    self.write_double_metric('cloudguard-datacenter-connection-status', tags, 'gauge', 1 if dc['dc_connection_status'].strip() == 'Connected' else 0, True, 'CloudGuard - Data Center connection status', 'state', 'dc.name|dc.type')
                    self.write_double_metric('cloudguard-datacenter-imported-objects', tags, 'gauge', int(dc['dc_imported_objects']), True, 'CloudGuard - Imported Data Center objects', 'number', 'dc.name|dc.type')
                    self.write_double_metric('cloudguard-datacenter-controller-updates', tags, 'gauge', int(dc['cloudguard_controller_updates']), True, 'CloudGuard Controller updates', 'number', 'dc.name|dc.type')

            gw_enforcing_data = helper_methods.parse_data_as_list(raw_data, 'checkpoint_gaia_cpstat_vsec_gateways_info.textfsm')
            if gw_enforcing_data:
                for gw in gw_enforcing_data:
                    tags = {}
                    tags['gw.name'] = gw['gw_name']
                    tags['gw.ip'] = gw['gw_ip']
                    tags['gw.os_version'] = gw['gw_os_version']
                    self.write_double_metric('cloudguard-gateway-update-status', tags, 'gauge', 1 if gw['gw_update_status'] == 'Succeeded' else 0, True, 'CloudGuard Gateway Update status','state','gw.name|gw.ip|gw.os_version')

        return self.output
