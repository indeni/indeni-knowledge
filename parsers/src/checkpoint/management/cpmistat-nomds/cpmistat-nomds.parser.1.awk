############
# Script explanation: This script is a bit complicated. Here is how it works
# Step 1: Get a list of all secondary management servers. 
#		Parse netobj_objects.C and find all hosts that have the following two properties:
#		:primary_management (false)
#		:management (true)
# Step 2: For each of the devices found in step 1, run cpmistat command against them.
# Step 3: To be able to query as many devices as possible, and have everything ready within the 30s ind script timeout, run cpmistat in parallel (since it has a 35s timeout), and record the PID of each command so we can kill them all after 20s.
###########


# Process "cpmistat -o schema -r mg HA-Management2" started
/^Process \"cpmistat -o schema -r mg/ {
	# We have attempted to connect to this host
	gsub(/\"/,"",$7)
	statusArr[$7] = 0
}

# :mgStatusOK (0)
# :mgStatusOK (1)
/:mgStatusOK/ {
	# We got some result from the attempt, but we dont know from who
	status = $2
	gsub(/^\(/,"",status)
	gsub(/\)$/,"",status)
}

# :mgSyncStatus (Lagging)
# :mgSyncStatus (Synchronized)
# :mgSyncStatus ("N/R (Self synchronization is not relevant)")
/:mgSyncStatus/ {
	# We got some result from the attempt, but we dont know from who
	
	split($0,splitArr,"mgSyncStatus")
	statusMessage = trim(splitArr[2])
	gsub(/^\(/,"",statusMessage)
	gsub(/\)$/,"",statusMessage)
	gsub(/\"/,"",statusMessage)
}


# cpmistat -o schema -r mg lab-CP-MGMT1-2
/^cpmistat -o schema -r mg/ {
	# We now know from who the above result was from

	statusMessageArr[$6] = statusMessage
	statusArr[$6] = status
}


END {
	# Write metrics
	for (id in statusArr) {
		t["name"] = id
		if (statusMessageArr[id] != "N/R (Self synchronization is not relevant)") {
			if (statusMessageArr[id] != "") {
				writeComplexMetricString("mgmt-ha-sync-state-description", t, statusMessageArr[id], "false")  # Converted to new syntax by change_ind_scripts.py script
			} else {
				writeComplexMetricString("mgmt-ha-sync-state-description", t, "No message (Timeout)", "false")  # Converted to new syntax by change_ind_scripts.py script
			}
		}
		if (statusArr[id] != "") {
			writeDoubleMetric("mgmt-ha-sync-state", t, "gauge", statusArr[id], "false")  # Converted to new syntax by change_ind_scripts.py script
		}
	}
}