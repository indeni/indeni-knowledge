import os
import unittest
from checkpoint.management.mgmt_cli_show_domains.mgmt_cli_show_domains_mgmt.mgmt_cli_show_dns_domains_mgmt import ChkpMgmtCliShowDnsDomainsMgmt

class TestChkpMgmtCliShowDnsDomainsMgmt(unittest.TestCase):

    def setUp(self):
        self.parser = ChkpMgmtCliShowDnsDomainsMgmt()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_dns_domains(self):
        result = self.parser.parse_file(self.current_dir + '/mgmt_cli_show_dns_domains_mgmt.input.json', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].action_type, 'WriteComplexMetric')
        self.assertEqual(result[0].name, 'dns-domains-not-fqdn')
        self.assertEqual(result[0].tags['name'], '.www.example.com')
        self.assertEqual(result[0].value['value'], 1)
        
    def test_dns_domains_empty(self):
        result = self.parser.parse_file(self.current_dir + '/mgmt_cli_show_dns_domains_mgmt_empty.input.json', {}, {})
        self.assertEqual(0, len(result))

if __name__ == '__main__':
    unittest.main()