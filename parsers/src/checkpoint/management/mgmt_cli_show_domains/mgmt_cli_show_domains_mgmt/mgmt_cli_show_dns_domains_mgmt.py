from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser
import json

def validate_json(raw_data):
    try:
        json.loads(raw_data) 
    except json.decoder.JSONDecodeError:
        return False
    else:
        return True 
class ChkpMgmtCliShowDnsDomainsMgmt(BaseParser):

    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            if validate_json(raw_data):
                data = helper_methods.parse_data_as_json(raw_data)
                if data['total'] != 0:
                    tags = {}
                    for parsed_data in data['objects']:
                        tags['name'] = parsed_data['name']
                        self.write_complex_metric_string('dns-domains-not-fqdn', tags, 1 if parsed_data['is-sub-domain'] == True else 0, False)
        return self.output