import os
import unittest
from checkpoint.management.mgmt_cli_show_domains.mgmt_cli_show_domains_mds.mgmt_cli_show_domains_mds_step_1 import ChkpMgmtCliShowDomainsMdsStep1

class TestChkpMgmtCliShowDomainsMdsStep1(unittest.TestCase):

    def setUp(self):
        self.parser = ChkpMgmtCliShowDomainsMdsStep1()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_dns_domains_step_1(self):
        result = self.parser.parse_file(self.current_dir + '/mgmt_cli_show_domains_mds_step_1.input.json', {}, {}) 
        self.assertEqual(3, len(result))
        self.assertEqual(result[0].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[0].key, 'cma-domain')
        self.assertEqual(result[0].value, 'CLM_1')
        self.assertEqual(result[1].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[1].key, 'cma-domain')
        self.assertEqual(result[1].value, 'CMA_1_R81')
        self.assertEqual(result[2].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[2].key, 'cma-domain')
        self.assertEqual(result[2].value, 'CMA_2_R81')

    def test_dns_domains_empty(self):
        result = self.parser.parse_file(self.current_dir + '/mgmt_cli_show_domains_mds_empty.input.json', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[0].key, 'cma-domain')
        self.assertEqual(result[0].value, ' ')

    def test_mgmt_cli_api_ip_access_not_allowed(self):
        result = self.parser.parse_file(self.current_dir + '/mgmt_cli_api_ip_access_not_allowed.input.json', {}, {}) 
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[0].key, 'cma-domain')
        self.assertEqual(result[0].value, ' ')

    def test_mgmt_cli_api_not_running(self):
        result = self.parser.parse_file(self.current_dir + '/mgmt_cli_api_not_running.input.json', {}, {}) 
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[0].key, 'cma-domain')
        self.assertEqual(result[0].value, ' ')

if __name__ == '__main__':
    unittest.main()