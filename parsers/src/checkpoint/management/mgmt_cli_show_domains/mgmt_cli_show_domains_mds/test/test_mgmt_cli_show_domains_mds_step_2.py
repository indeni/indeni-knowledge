import os
import unittest
from checkpoint.management.mgmt_cli_show_domains.mgmt_cli_show_domains_mds.mgmt_cli_show_domains_mds_step_2 import ChkpMgmtCliShowDomainsMdsStep2

class TestChkpMgmtCliShowDomainsMdsStep2(unittest.TestCase):
        
    def setUp(self):
        self.parser = ChkpMgmtCliShowDomainsMdsStep2()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_dns_domains_step_2(self):
        result = self.parser.parse_file(self.current_dir + '/mgmt_cli_show_domains_mds_step_2.input.json', {'cma-domain': 'CMA_2_R81'}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'dns-domains-not-fqdn')
        self.assertEqual(result[0].tags['vs.name'], 'CMA_2_R81')
        self.assertEqual(result[0].value, 1)

    def test_dns_domains_empty(self):
        result = self.parser.parse_file(self.current_dir + '/mgmt_cli_show_domains_mds_empty.input.json', {}, {})
        self.assertEqual(0, len(result))

if __name__ == '__main__':
    unittest.main()