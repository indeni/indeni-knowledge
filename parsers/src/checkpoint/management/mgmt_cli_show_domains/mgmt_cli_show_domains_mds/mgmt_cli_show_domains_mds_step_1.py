from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser
import json


def validate_json(raw_data):
    try:
        json.loads(raw_data)
    except json.decoder.JSONDecodeError:
        return False
    else:
        return True 

class ChkpMgmtCliShowDomainsMdsStep1(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            if validate_json(raw_data):
                data = helper_methods.parse_data_as_json(raw_data)
                if data and 'objects' in data.keys():
                    for single_object in data['objects']:
                        if single_object['type'] == 'domain' and single_object['domain']['domain-type'] == 'mds':
                            cma_domain = single_object.get('name')
                            self.write_dynamic_variable('cma-domain', cma_domain)
        if len(self.output) == 0:
            self.write_dynamic_variable('cma-domain', ' ')
        return self.output