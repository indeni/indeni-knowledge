from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser
import json


def validate_json(raw_data):
    try:
        json.loads(raw_data) # put JSON-data to a variable
    except json.decoder.JSONDecodeError:
        return False # in case json is invalid
    else:
        return True 
class ChkpMgmtCliShowDomainsMdsStep2(BaseParser):

    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            if validate_json(raw_data):
                data = helper_methods.parse_data_as_json(raw_data)
                if data['total'] > 0:
                    tags = {}
                    tags['vs.name'] = dynamic_var['cma-domain']
                    sub_domain = False
                    domain_objects = data.get('objects')
                    if domain_objects:
                        for single_object in domain_objects:
                            tags['object-name'] = single_object['name']
                            if single_object['is-sub-domain']: 
                                sub_domain = True
                    self.write_double_metric('dns-domains-not-fqdn', tags, 'gauge', 1 if sub_domain == True else 0, False)
        return self.output