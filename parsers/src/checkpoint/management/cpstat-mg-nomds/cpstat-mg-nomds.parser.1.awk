############
# ToDo: Add the status message to a descriptive tag in the future
###########

BEGIN {
	# A lot of data is separated with ":" so using as delimiter instead of whitespace
	FS=":"
}

#Is started:    1
/Is started/ {
	isStarted = trim($2)
}


# Active status: active
# Active status: standby
/Active status/ {
	activeStatusMessage = trim($2)
	if (trim($2) == "active" || trim($2) == "standby") {
		activeStatus = 1
	}
}


# Status:        OK
# Status:        The Internal Certificate Authority (ICA) certificate is valid until Jan 19 03:14:07 2038 GMT
/^Status:/ {
    statusFound = 1
	statusMessage= $0
	sub(/^Status:\s*/, "", statusMessage)
	if ((statusMessage ~ "^OK") || (statusMessage ~ "^The Internal Certificate Authority ")) {
		status = 1
	}
}


END {
	if (statusFound) {
		if ( isStarted == 1 && activeStatus == 1 && status == 1 ) {
			mgmtStatus = 1
		} else {
			mgmtStatus = 0
			# Write data collected to troubleshoot
			totalMessages = "is-started: " isStarted " - active-status-message: " activeStatusMessage " - status-message: " statusMessage
			writeComplexMetricString("mgmt-status-description", null, totalMessages, "false")  # Converted to new syntax by change_ind_scripts.py script
		}
		# Write metric
		writeDoubleMetric("mgmt-status", null, "gauge", mgmtStatus, "true", "Management Services Status", "state", "")  # Converted to new syntax by change_ind_scripts.py script
	}
}