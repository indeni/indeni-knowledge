import os
import unittest

from checkpoint.management.check_edl_list.check_edl_list import CheckEdlList
from parser_service.public.action import *

class TestCheckEdlList(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = CheckEdlList()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_basic_log_error(self):
        expected_results = [{'name': 'fw73vc01p2cp', 'metric': 'edl_list_state', 'value': 1},
                            {'name': 'fw73vc01p2cps', 'metric': 'edl_list_state', 'value': 0},
                            {'name': 'fw73vc01p2cps', 'metric': 'edl_list_failure', 'value': {'value': "Transfer Failed. Check sums of upgrade packages don't match"}},
                            {'name': 'fw73vc01p2cpp', 'metric': 'edl_list_state', 'value': 1}]

        # Act
        result = self.parser.parse_file(self.current_dir + '/basic_log_error.input', {}, {})
        # Assert
        self.assertEqual(4, len(result))

        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i]['name'], result[i].tags['name'])
            self.assertEqual(expected_results[i].get('failure'), result[i].tags.get('failure'))
            self.assertEqual(expected_results[i]['metric'], result[i].name)
            self.assertEqual(expected_results[i]['value'], result[i].value)

    def test_log_error_multiple_entries(self):

        expected_results = [{'name': '10.46.79.4', 'metric': 'edl_list_state', 'value': 1},
                            {'name': '10.177.127.187', 'metric': 'edl_list_state', 'value': 1}]
        # Act
        result = self.parser.parse_file(self.current_dir + '/log_error_multiple_entries.input', {}, {})
        # Assert
        self.assertEqual(2, len(result))
        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i]['name'], result[i].tags['name'])
            self.assertEqual(expected_results[i]['metric'], result[i].name)
            self.assertEqual(expected_results[i]['value'], result[i].value)

    def test_no_info_returned(self):

        expected_results = [{'name': '10.178.17.235', 'metric': 'edl_list_state', 'value': 1}]
        # Act
        result = self.parser.parse_file(self.current_dir + '/no_info_returned.input', {}, {})
        # Assert
        self.assertEqual(1, len(result))
        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i]['name'], result[i].tags['name'])
            self.assertEqual(expected_results[i]['metric'], result[i].name)
            self.assertEqual(expected_results[i]['value'], result[i].value)

if __name__ == '__main__':
    unittest.main()