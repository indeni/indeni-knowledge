from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class CheckEdlList(BaseParser):
    def _clean_list(self, data: list) -> list:
        clean_list = []
        for item in data:
            clean = item.replace('--\n', '')
            clean_list.append(clean)
        return clean_list

    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data and 'Uploading the package' in raw_data:
            # Create list of all logs:
            base_split = raw_data.split(' Uploading the package to ')[1:]
            logs_list = self._clean_list(base_split)
            status_ok_msg_list = ['Transfer successful',
                                'IP information has not changed. Exiting.']

            # Create list of FW in logs, taking only the first occurrence:
            fw_list = []
            for log in logs_list:
                fw_name_list = helper_methods.parse_data_as_list(log, 'checkpoint_edl_list_metadata.textfsm')
                for fw_name in fw_name_list:
                    if fw_name['FW'] not in fw_list:
                        fw_list.append(fw_name['FW'])

            # Create list of log per FW, taking only first occurrence:
            clean_log_list = []
            for fw in fw_list:
                iter_string = f'FIREWALL_IP_UPDATED: {fw}'
                for log in logs_list:
                    if iter_string in log and not any(iter_string in item for item in clean_log_list):
                        clean_log_list.append(log)

            #Iterating the list, and report metrics:
            for log in clean_log_list:
                fw_name_list = helper_methods.parse_data_as_list(log, 'checkpoint_edl_list_metadata.textfsm')
                for fw_name in fw_name_list:
                    tags = {'name': fw_name['FW']}
                    if any(ok_msg in log for ok_msg in status_ok_msg_list):
                        self.write_double_metric('edl_list_state', tags, 'gauge', 1, False)
                    else:
                        failed_log = helper_methods.parse_data_as_object(log, 'checkpoint_edl_list_failure.textfsm')
                        failure = failed_log.get('failure', 'No failure message found')
                        self.write_double_metric('edl_list_state', tags, 'gauge', 0, False)
                        self.write_complex_metric_string('edl_list_failure', tags, failure, False)
        return self.output
