from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class CpstatVsecMds(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        tags = {'error': ''}
        azure_dc_error_log = []
        generic_dc_error_log = []
        
        if raw_data:
            azure_dc_errors = helper_methods.parse_data_as_list(raw_data, 'checkpoint_gaia_parse_cloud_proxy_elg_azure.textfsm')
            if azure_dc_errors:
                for errors in azure_dc_errors:
                    if errors['azure_error_message']:
                        azure_dc_error_log.append({'error': errors['azure_error_message']})
            generic_dc_errors = helper_methods.parse_data_as_list(raw_data, 'checkpoint_gaia_parse_cloud_proxy_elg_generic_dc.textfsm')
            if generic_dc_errors:
                for errors in generic_dc_errors:
                    if errors['generic_dc_error_message']:
                        generic_dc_error_log.append({'error': errors['generic_dc_error_message']})

        no_duplicates_azure = [i for n, i in enumerate(azure_dc_error_log) if i not in azure_dc_error_log[n + 1:]]
        self.write_complex_metric_object_array('cloudguard-datacenter-azure-log-error', tags, no_duplicates_azure, True, 'Azure connectivity problem')

        no_duplicates_generic_dc = [i for n, i in enumerate(generic_dc_error_log) if i not in generic_dc_error_log[n + 1:]]
        self.write_complex_metric_object_array('cloudguard-datacenter-generic-dc-log-error', tags, no_duplicates_generic_dc, True, 'Generic DC connectivity problem')

        return self.output
