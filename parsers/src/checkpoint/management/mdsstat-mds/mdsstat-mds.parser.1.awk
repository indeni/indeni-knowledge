#Reads the status of the critical processes for each CMA by running "mdsstat" on MDS server
#   Below are the status of the processes of Multi-Domain Server and Domain Management Servers:
#
#   up: The process is up.
#   down: The process is down.
#   pnd: The process is pending initialization.
#   init: The process is initializing.
#   N/A: The process's PID is not yet available.
#   N/R: The process is not relevant for this Multi-Domain Server.
#   24/07/2019 - Adding "init" status also to be ok as many customer are getting false alert during transitionary "init" state.

BEGIN {
	# Input is divided on pipe
	FS = "|"
	fwmhaCol = 0
}

/\| Type \|.*FWMHA/ {
	fwmhaCol = 1
}

#R81.10
#+------+-----------------------------------------------------+-----------------+-------------+-------------+-------------+-------------+-------------+
#| Type | Name                                                | IP address      | FWM         | FWMHA       | FWD         | CPD         | CPCA        |
#+------+-----------------------------------------------------+-----------------+-------------+-------------+-------------+-------------+-------------+
#| MDS  |                          -                          | 11.222.33.20    | up 93833    | up 93838    | up 93831    | up 20199    | N/R         |
#+------+-----------------------------------------------------+-----------------+-------------+-------------+-------------+-------------+-------------+
#| CMA  | cma_name                                            | 11.222.33.444   | up 56815    | down        | up 56912    | up 55009    | down       |

#| Type | Name                                 | IP address      | FWM        | FWD      | CPD      | CPCA     |
#| CMA  |  MGMT-MDM-VS1_Management_Server      | 192.168.197.34  | up 5814    | up 5739  | up 5546  | up 8450  |
#| MDS  |   -                                  | 192.168.197.33  | down       | up 5840  | up 5837  | up 8375  |
#| CMA  |lab-CP-MGMT-MDM-VS1_Management_Server | 192.168.197.34  | init 6878  | up 6831  | up 16818 | up 10107 |

/^\s*\|\s*(MDS|CMA)/ {
	# Remove old tags
	delete tags

	vs_name = trim($3)
	vs_ip = trim($4)

	fwm = $5

	if (fwmhaCol == 0) {
		fwd =  $6
		cpd =  $7
		cpca = $8
	}
	else {
		fwmha = $6
		fwd =   $7
		cpd =   $8
		cpca =  $9
	}
	# Set VS tags if this is the CMA, but do not set them if this is the MDS
	if (trim($2) != "MDS") {
		tags["vs.ip"] = vs_ip
		tags["vs.name"] = vs_name
	}

	# FWM
	if (fwm ~ "up" || fwm ~ "init") {
		fwm_status = 1
	} else {
		fwm_status = 0
	}
	tags["process-name"] = "FWM"
	writeDoubleMetric("process-state", tags, "gauge", fwm_status, "false")  # Converted to new syntax by change_ind_scripts.py script

	# FWMHA
    if (fwmhaCol == 1) {
        if (fwmha ~ "up" || fwmha ~ "init") {
			fwmha_status = 1
        } else {
			fwmha_status = 0
        }
        tags["process-name"] = "FWMHA"
        writeDoubleMetric("process-state", tags, "gauge", fwmha_status, "false")  # Converted to new syntax by change_ind_scripts.py script
	}

	# FWD
	if (fwd ~ "up" || fwd ~ "init") {
		fwd_status = 1
	} else {
		fwd_status = 0
	}
	tags["process-name"] = "FWD"
	writeDoubleMetric("process-state", tags, "gauge", fwd_status, "false")  # Converted to new syntax by change_ind_scripts.py script

	# CPD
	if (cpd ~ "up" || cpd ~ "init") {
		cpd_status = 1
	} else {
		cpd_status = 0
	}
	tags["process-name"] = "CPD"
	writeDoubleMetric("process-state", tags, "gauge", cpd_status, "false")  # Converted to new syntax by change_ind_scripts.py script

	# CPCA
	if (cpca ~ "up" || cpca ~ "init") {
		cpca_status = 1
	} else {
		cpca_status = 0
	}
	tags["process-name"] = "CPCA"
	writeDoubleMetric("process-state", tags, "gauge", cpca_status, "false")  # Converted to new syntax by change_ind_scripts.py script
}