#############################################################################
#                            SSH OUTPUT                                     #
#############################################################################
#Object: CP_R80.20_MDS1_CMA1_Server 10.11.94.74 CP-R80.20-VSX1-2 10.11.94.73#
#Object: CP_R80.20_MDS1_CMA1_Server 10.11.94.74 CP-R80.20-VSX1-1 10.11.94.72#
#############################################################################
#
# Step1: Array [vsName:vsIP] = GWxName:GWxIP;GWyName:GWyIP ....
# Step2: tags = [vsName , vsIP]  and known_devices: array of GWs
#
##############################################################################

BEGIN {
    id = 0
}

/^Object:/ {
    domain = $2 ":" $3
    if (info_array [domain] == "")
        info_array [domain] = $5 ":" $4
    else
        info_array [domain] = info_array [domain] ";" $5 ":" $4
}

END {
    for ( VS in info_array) {
        if (match(info_array[VS], ";")==-1) {
            split_data_array[1] = info_array[VS]
        }
        else {
            split(info_array[VS], split_data_array, ";")
        }
        split(VS, split_vs_array, ":")
        tags["vs.name"] = split_vs_array[1]
        tags["vs.ip"] = split_vs_array[2]
        id = 0
        for (device in split_data_array) {
            id++
            split(split_data_array[device], split_device, ":")
            known_devices[id, "name"] = split_device[2]
            known_devices[id, "ip"] = split_device[1]
        }
        writeComplexMetricObjectArray("known-devices", tags, known_devices, "false")  # Converted to new syntax by change_ind_scripts.py script
        delete known_devices
    }
}