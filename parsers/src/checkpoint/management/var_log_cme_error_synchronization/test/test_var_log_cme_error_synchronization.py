import os
import unittest
from checkpoint.management.var_log_cme_error_synchronization.var_log_cme_error_synchronization import VarLogCmeErrorSynchronization
from parser_service.public.action import *

class TestVarLogCmeErrorSynchronization(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = VarLogCmeErrorSynchronization()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_var_log_cme_error_synchronization_found(self):
        result = self.parser.parse_file(self.current_dir + '/var_log_cme_error_synchronization_found.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'cme-error-synchronization')
        self.assertEqual(result[0].value, 1)

    def test_var_log_cme_error_synchronization(self):
        result = self.parser.parse_file(self.current_dir + '/var_log_cme_error_synchronization.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'cme-error-synchronization')
        self.assertEqual(result[0].value, 0)

if __name__ == '__main__':
    unittest.main()