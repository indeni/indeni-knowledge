from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class VarLogCmeErrorSynchronization(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            self.write_double_metric('cme-error-synchronization', {}, 'gauge', 1 if "Error during synchronization with Security Gateways" in raw_data else 0, False)
        return self.output