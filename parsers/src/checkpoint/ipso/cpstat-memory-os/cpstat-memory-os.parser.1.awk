# Total Real Memory (Bytes):     2136915968
/Total Real/ {
	totalRam = $NF / 1024
}

# Active Real Memory (Bytes):    339283968
/Active Real/ {
	usedRam = $NF / 1024
}

# Free Real Memory (Bytes):      1797632000
/Free Real/ {
	freeRam = $NF / 1024
}

END { 
	if (freeRam && usedRam) {
		ramtag["name"] = "RAM"
		usageRam = (usedRam / totalRam) * 100
		writeDoubleMetric("memory-free-kbytes", ramtag, "gauge", freeRam, "true", "Memory - Free", "kbytes", "name")  # Converted to new syntax by change_ind_scripts.py script
		writeDoubleMetric("memory-total-kbytes", ramtag, "gauge", totalRam, "true", "Memory - Total", "kbytes", "name")  # Converted to new syntax by change_ind_scripts.py script

		ramtag["resource-metric"] = "true"
		writeDoubleMetric("memory-usage", ramtag, "gauge", usageRam, "true", "Memory - Usage", "percentage", "name")  # Converted to new syntax by change_ind_scripts.py script
	}
}