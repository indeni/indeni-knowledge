function ignoreInterface(_name) {
    if (_name ~ /^(loop|soverf|stof|tun).*/) {
        return 1
    } else {
        return 0
    }
}

############
# Script explanation: We should avoid running clish commands due to the excessive logs in /var/log/messages that creates. So ifconfig and parsing /config/active instead.
###########

BEGIN {
    bitMaskToSubnet["32"] = "255.255.255.255"
    bitMaskToSubnet["31"] = "255.255.255.254"
    bitMaskToSubnet["30"] = "255.255.255.252"
    bitMaskToSubnet["29"] = "255.255.255.248"
    bitMaskToSubnet["28"] = "255.255.255.240"
    bitMaskToSubnet["27"] = "255.255.255.224"
    bitMaskToSubnet["26"] = "255.255.255.192"
    bitMaskToSubnet["25"] = "255.255.255.128"
    bitMaskToSubnet["24"] = "255.255.255.0"
    bitMaskToSubnet["23"] = "255.255.254.0"
    bitMaskToSubnet["22"] = "255.255.252.0"
    bitMaskToSubnet["21"] = "255.255.248.0"
    bitMaskToSubnet["20"] = "255.255.240.0"
    bitMaskToSubnet["19"] = "255.255.224.0"
    bitMaskToSubnet["18"] = "255.255.192.0"
    bitMaskToSubnet["17"] = "255.255.128.0"
    bitMaskToSubnet["16"] = "255.255.0.0"
    bitMaskToSubnet["15"] = "255.254.0.0"
    bitMaskToSubnet["14"] = "255.252.0.0"
    bitMaskToSubnet["13"] = "255.248.0.0"
    bitMaskToSubnet["12"] = "255.240.0.0"
    bitMaskToSubnet["11"] = "255.224.0.0"
    bitMaskToSubnet["10"] = "255.192.0.0"
    bitMaskToSubnet["9"] = "255.128.0.0"
    bitMaskToSubnet["8"] = "255.0.0.0"
    bitMaskToSubnet["7"] = "254.0.0.0"
    bitMaskToSubnet["6"] = "252.0.0.0"
    bitMaskToSubnet["5"] = "248.0.0.0"
    bitMaskToSubnet["4"] = "240.0.0.0"
    bitMaskToSubnet["3"] = "224.0.0.0"
    bitMaskToSubnet["2"] = "192.0.0.0"
    bitMaskToSubnet["1"] = "128.0.0.0"
    bitMaskToSubnet["0"] = "0.0.0.0"
}

#loop0c0:  lname loop0c0 flags=57<UP,PHYS_AVAIL,LINK_AVAIL,LOOPBACK,MULTICAST>
#eth1c0:  lname eth1c0 flags=e0<BROADCAST,MULTICAST,AUTOLINK>
/^.+:\s+/ {
    interfaceName = $1
    #eth1c0:
    sub(/:/, "", interfaceName)

    if (!ignoreInterface(interfaceName)) {
        statTags["name"] = interfaceName

        type = interfaceName
        #eth1c0
        sub(/[0-9].*/, "", type)
        writeComplexMetricString("network-interface-type", statTags, type, "true", "Network Interfaces - type")  # Converted to new syntax by change_ind_scripts.py script
        interfaces[interfaceName, "driver"] = type
    } else {
        interfaceName = ""
    }
}

#eth4c0:  lname eth4c0 flags=e4<UP,BROADCAST,MULTICAST,AUTOLINK>
#eth2c0:  lname eth2c0 flags=e7<UP,PHYS_AVAIL,LINK_AVAIL,BROADCAST,MULTICAST,AUTOLINK>
/lname.*flags=/ {
    if (interfaceName != "") {
        if ($0 ~ /PHYS_AVAIL/) {
            linkstate = 1
        } else {
            linkstate = 0
        }
        writeDoubleMetric("network-interface-state", statTags, "gauge", linkstate, "true", "Network Interfaces - Up/Down", "state", "name")  # Converted to new syntax by change_ind_scripts.py script

        if ($0 ~ /[^A-Z]UP[^A-Z]/) {
            adminState = 1
        } else {
            adminState = 0
        }
        writeDoubleMetric("network-interface-admin-state", statTags, "gauge", adminState, "true", "Network Interfaces - Enabled/Disabed", "state", "name")  # Converted to new syntax by change_ind_scripts.py script
    }
}

#         ether 00:a0:8e:b2:22:37 speed 10M half duplex
/[0-9a-fA-F]{2}:[0-9a-fA-F]{2}:[0-9a-fA-F]{2}:[0-9a-fA-F]{2}:[0-9a-fA-F]{2}:[0-9a-fA-F]{2}/ {
    if (interfaceName != "") {
        match($0, /[0-9a-fA-F]{2}:[0-9a-fA-F]{2}:[0-9a-fA-F]{2}:[0-9a-fA-F]{2}:[0-9a-fA-F]{2}:[0-9a-fA-F]{2}/)

        macaddress = substr($0, RSTART, RLENGTH)
        writeComplexMetricString("network-interface-mac", statTags, macaddress, "false")  # Converted to new syntax by change_ind_scripts.py script
    }
}

#         inet mtu 1500 172.16.20.30/24 broadcast 172.16.20.255
/ inet / {
    if (interfaceName != "") {
        ipAndBitMask = $4
        split(ipAndBitMask, ipArray, "/")
        ip = ipArray[1]
        bitMask = ipArray[2]
        subnet = bitMaskToSubnet[bitMask]

        ipTags["name"] = interfaceName
        ipTags["im.identity-tags"] = "name"
        ipTags["im.dstype.displaytype"] = "string"

        writeComplexMetricString("network-interface-ipv4-address", ipTags, ip, "true", "Network Interfaces - IPv4 Address")  # Converted to new syntax by change_ind_scripts.py script
        writeComplexMetricString("network-interface-ipv4-subnet", ipTags, subnet, "true", "Network Interfaces - IPv4 Netmask")  # Converted to new syntax by change_ind_scripts.py script
    }
}

#        inet6 mtu 63000 ::1 --> ::1
/^\s.+mtu\s/ {
    if (interfaceName != "") {
        mtu = $0
        #        inet6 mtu 63000 ::1 --> ::1
        sub(/.* mtu /, "", mtu)
        #mtu 63000 ::1 --> ::1
        sub(/ .*/, "", mtu)

        interfaces[interfaceName, "mtu"] = mtu
        writeDoubleMetric("network-interface-mtu", tags, "gauge", mtu, "true", "Network Interfaces - MTU", "number", "name")  # Converted to new syntax by change_ind_scripts.py script
    }
}

#         ether 00:a0:8e:b2:22:37 speed 10M half duplex
/ speed .* duplex/ {
    if (interfaceName != "") {
        speed = $0
        #         ether 00:a0:8e:b2:22:37 speed 10M half duplex
        sub(/.* speed /, "", speed)
        #10M half duplex
        sub(/ .*/, "", speed)

        # speed metric need special tags for alert purpose
        speed_tags["name"] = interfaceName
        speed_tags["im.identity-tags"] = "name"
        writeComplexMetricString("network-interface-speed", speed_tags, speed, "true", "Network Interfaces - speed")  # Converted to new syntax by change_ind_scripts.py script

        duplex = $0
        if ($0 ~ /full duplex/) {
            writeComplexMetricString("network-interface-duplex", statTags, "full", "true", "Network Interfaces - duplex settings")  # Converted to new syntax by change_ind_scripts.py script
        } else if ($0 ~ /half duplex/) {
            writeComplexMetricString("network-interface-duplex", statTags, "half", "true", "Network Interfaces - duplex settings")  # Converted to new syntax by change_ind_scripts.py script
        }
    }
}

#interface:eth2c0:comments my\ comments
/interface:[a-z0-9]+:comments/ {
    if (interfaceName != "") {
        # Get interface name
        split($0, lineParts, ":")
        statTags["name"] = lineParts[2]

        # If the comment is in two words or more, a backslash is inserted. We need to remove it
        commentLine = $0
        gsub(/interface:[a-z0-9]+:comments/, "", commentLine)
        gsub(/(\\)/, "", commentLine)
        writeComplexMetricString("network-interface-description", statTags, trim(commentLine), "false")  # Converted to new syntax by change_ind_scripts.py script
    }
}

#Name         Mtu   Network     Address             Ipkts Ierrs     Ibytes    Opkts Oerrs     Obytes  Coll Drop
#eth2         16018 <Link>      0:a0:8e:b2:22:35    39706   440    5481041    28917     0   10158270  1898   0
/^Name\s+Mtu\s+Network\s+/ {
    if (interfaceName != "") {
        inNetstat = "true"
        getColumns(trim($0), "[ \t]+", columns)
    }
}

#eth2         16018 <Link>      0:a0:8e:b2:22:35    39706   440    5481041    28917     0   10158270  1898   0
/[0-9]/ {
    if (inNetstat == "true") {
        name = getColData(trim($0), columns, "Name")
        statTags["name"] = name

        if (!ignoreInterface(name)) {
            writeDoubleMetric("network-interface-rx-packets", statTags, "counter", getColData(trim($0), columns, "Ipkts"), "true", "Network Interfaces - RX Packets", "number", "name")  # Converted to new syntax by change_ind_scripts.py script

            writeDoubleMetric("network-interface-rx-errors", statTags, "counter", getColData(trim($0), columns, "Ierrs"), "true", "Network Interfaces - RX Errors", "number", "name")  # Converted to new syntax by change_ind_scripts.py script

            writeDoubleMetric("network-interface-rx-bits", statTags, "counter", getColData(trim($0), columns, "Ibytes") * 8, "true", "Network Interfaces - RX Bits", "bits", "name")  # Converted to new syntax by change_ind_scripts.py script

            writeDoubleMetric("network-interface-rx-dropped", statTags, "counter", getColData(trim($0), columns, "Drop"), "true", "Network Interfaces - RX Dropped", "number", "name")  # Converted to new syntax by change_ind_scripts.py script

            writeDoubleMetric("network-interface-tx-packets", statTags, "counter", getColData(trim($0), columns, "Opkts"), "true", "Network Interfaces - TX Packets", "number", "name")  # Converted to new syntax by change_ind_scripts.py script

            writeDoubleMetric("network-interface-tx-errors", statTags, "counter", getColData(trim($0), columns, "Oerrs"), "true", "Network Interfaces - TX Errors", "number", "name")  # Converted to new syntax by change_ind_scripts.py script

            writeDoubleMetric("network-interface-tx-bits", statTags, "counter", getColData(trim($0), columns, "Obytes") * 8, "true", "Network Interfaces - TX Bits", "bits", "name")  # Converted to new syntax by change_ind_scripts.py script

            writeDoubleMetric("network-interface-tx-collisions", statTags, "counter", getColData(trim($0), columns, "Coll"), "true", "Network Interfaces - TX Collisions", "number", "name")  # Converted to new syntax by change_ind_scripts.py script
        }
    }
}


END {
    writeComplexMetricObjectArray("network-interfaces", null, interfaces, "false")  # Converted to new syntax by change_ind_scripts.py script
}