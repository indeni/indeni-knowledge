# The following two sections has been added by request of Dan Shouky
# https://indeni.atlassian.net/browse/IKP-1221

# Unfortunately, the following code is duplicated in many .ind scripts.
# If you change something in the following two sections, please find all
# of the other instances of this code and make the change there also.

#Could not acquire the config lock
/Could not acquire the config lock/ {
	if (NR == 1) {
		next
	}
}

#CLINFR0829  Unable to get user permissions
#CLINFR0819  User: johndoe denied access via CLI
#CLINFR0599  Failed to build ACLs
/(CLINFR0829\s+Unable to get user permissions|CLINFR0819\s+User: .+ denied access via CLI|CLINFR0599\s+Failed to build ACLs)/ {
	exit
}

# The command returns a variety of tables, with a variety of columns. The script here will need to
# address them accordingly.

/(Location|ID)/ {
	delete columns
	getColumns(trim($0), "[ \t]+", columns)
}

# 1       SYS_FAN1  Normal  109            108           155  
# PS-A  Yes      n/a     n/a     OK      0  
# 1       SYSTEM    Good    40             75          1  
# 1       3.3V      Good    3.300    3.266     -0.034    3.096     3.487     
/[0-9]/ {
	name = getColData(trim($0), columns, "Location")
	if (name == "" || name == null) {
		name = getColData(trim($0), columns, "ID")   # The power supply status table is different
	}

	statusName = getColData(trim($0), columns, "Status")

	if (statusName == "Good" || statusName == "OK" || statusName == "Normal") {
		status = 1
	} else {
		status = 0
	}
	hwTags["name"] = name
	writeDoubleMetric("hardware-element-status", hwTags, "gauge", status, "true", "Hardware Status", "state", "name")  # Converted to new syntax by change_ind_scripts.py script
}