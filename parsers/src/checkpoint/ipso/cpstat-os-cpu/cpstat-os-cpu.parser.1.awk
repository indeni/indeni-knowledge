# from the "cpu" flavor:
# CPU Usage (%):       0
/CPU Usage.*[0-9]/ {
    cputags["cpu-is-avg"] = "true"
    cputags["cpu-id"] = "all-average"
    cputags["resource-metric"] = "true"

    usage = $NF

    writeDoubleMetric("cpu-usage", cputags, "gauge", usage, "true", "CPU", "percentage", "cpu-id")  # Converted to new syntax by change_ind_scripts.py script
}


# from the "multi_cpu" flavor:
# ---------------------------------------------------------------------------------
# |CPU#|User Time(%)|System Time(%)|Idle Time(%)|Usage(%)|Run queue|Interrupts/sec|
# ---------------------------------------------------------------------------------
# |   1|           1|             0|          98|       2|        2|            19|
# ---------------------------------------------------------------------------------
/CPU.*User Time/ {
    # Get rid of the spaces in the column names (like "User Time" 's space) because it
    # confuses the getColumns function
    colNames = trim($0)
    gsub(/ /, "", colNames)
    getColumns(colNames, "[ \t\|]+", columns)
}

/ [0-9].* [0-9].* [0-9]/ {
    cpuId = getColData(trim($0), columns, "CPU#")
    usage = getColData(trim($0), columns, "Usage(%)")

    cputags["cpu-id"] = cpuId
    cputags["cpu-is-avg"] = "false"
    cputags["resource-metric"] = "false"

    writeDoubleMetric("cpu-usage", cputags, "gauge", usage, "true", "CPU", "percentage", "cpu-id")  # Converted to new syntax by change_ind_scripts.py script
}