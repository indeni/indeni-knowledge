BEGIN {
	snmpUnencrypted = "false"
	enableStatus = "false"
}

#process:snmpd t
/process:snmpd t/ {
	enableStatus = "true"
}

#snmp:version v1/v2/v3
/snmp:version/ {
	snmpVersion = $2
	if (snmpVersion != "v3-Only") {
		# SNMPv3 only is not set, which means unencrypted SNMP communication.
		snmpUnencrypted = "true"
	}
}

#snmp:community:public t
/snmp:community|snmp:writecommunity/ {
	split($1, splitArr, /:/)
	iCommunity++
	communities[iCommunity, "community"] = splitArr[3]
	
	if (splitArr[2] == "writecommunity") {
		communities[iCommunity, "permissions"] = "read-write"
	} else if (splitArr[2] == "community") {
		communities[iCommunity, "permissions"] = "read-only"
	}
}

#snmp:syscontact Mr\ Cool
/^snmp:syscontact/ {
	snmpContact = $0
	sub(/^snmp:syscontact /, "", snmpContact)
	# Remove escape chars
	gsub(/\\ /, " ", snmpContact)
	snmpContact = tolower(snmpContact)
}

#snmp:syslocation superduperlocation\ here
/snmp:syslocation/ {
	snmpLocation = $0
	gsub(/snmp:syslocation/, "", snmpLocation)
	# remove escape chars
	gsub(/\\ /, " ", snmpLocation)
	snmpLocation = trim(snmpLocation)
}

#snmp:traps:authorizationError on
# snmp:traps:NOKIA-IPSO-SYSTEM-MIB:systemtrapconfigurationsavechangeenable t
/snmp:traps:/ {
    sub(/(NOKIA-IPSO-SYSTEM-MIB|VRRP-MIB|rfc1157):/, "", $1) # In IPSO, there's the MIB name in the middle for some traps
	split($1,splitArr,/:/)
	trapName = trim(splitArr[3])
	if ($2 == "on" || $2 == "t") {
		trapStatus = "enabled"
	} else {
		trapStatus = "disabled"
	}
	
	iTrap++
	traps[iTrap, trapName] = trapStatus
}

#snmp:trap_rcv:1.1.1.1:version v2
#snmp:trap_rcv:1.1.1.1:community testcommunity
/snmp:trap_rcv:/ {
	split($1,splitArr,/:/)
	trapIp = splitArr[3]
	
	trapReciever[trapIp, "ip"] = trapIp
	if (trim(splitArr[4]) == "version") {
		trapReciever[trapIp, "version"] = $2
	} else if (trim(splitArr[4]) == "community") {
		trapReciever[trapIp, "community"] = $2
	}
}

#snmp:trapsPollingFrequency 100
/snmp:trapsPollingFrequency/ {
	iTrap++
	# Not supported in IPSO, ignored
	traps[iTrap, "pollingfrequency"] = $2
}

#snmp:v3:user:testuser:seclvl authPrivReq
/snmp:v3:user:.*seclvl/ {
	split($1,splitArr,/:/)
	user = splitArr[4]
	security = $2
	
	snmpUser[user, "username"] = user
	snmpUser[user, "security"] = security
	
	if (security == "authNoPriv") {
		# No encryption
		snmpUnencrypted = "true"
	}
}

#snmp:roUser:testuser t
#snmp:rwUser:testuser t
/(snmp:rwUser:|snmp:roUser:)/ {
	split($1,splitArr,/:/)
	user = splitArr[3]
	
	if (splitArr[2] == "rwUser") {
		snmpUser[user, "permissions"] = "ReadWrite"
	} else if (splitArr[2] == "roUser") {
		snmpUser[user, "permissions"] = "ReadOnly"
	}
}

END {
	# Only write SNMP data if SNMP is enabled.
	if (enableStatus != "true") {
		enableStatus = "false"
		writeComplexMetricString("snmp-enabled", null, enableStatus, "false")  # Converted to new syntax by change_ind_scripts.py script
	} else if (enableStatus == "true") {
		writeComplexMetricString("snmp-enabled", null, enableStatus, "false")  # Converted to new syntax by change_ind_scripts.py script
		writeComplexMetricObjectArray("snmp-communities", null, communities, "false")  # Converted to new syntax by change_ind_scripts.py script
		writeComplexMetricObjectArray("snmp-traps-status", null, traps, "false")  # Converted to new syntax by change_ind_scripts.py script
		writeComplexMetricObjectArray("snmp-traps-receiver", null, trapReciever, "false")  # Converted to new syntax by change_ind_scripts.py script
		writeComplexMetricObjectArray("snmp-users", null, snmpUser, "false")  # Converted to new syntax by change_ind_scripts.py script
		writeComplexMetricString("snmp-version", null, snmpVersion, "false")  # Converted to new syntax by change_ind_scripts.py script
		writeComplexMetricString("snmp-contact", null, snmpContact, "true", "SNMP - Contact")  # Converted to new syntax by change_ind_scripts.py script
		writeComplexMetricString("snmp-location", null, snmpLocation, "true", "SNMP - Location")  # Converted to new syntax by change_ind_scripts.py script
		writeComplexMetricString("unencrypted-snmp-configured", null, snmpUnencrypted, "false")  # Converted to new syntax by change_ind_scripts.py script
	} else {
		writeComplexMetricString("snmp-enabled", null, "false", "false")  # Converted to new syntax by change_ind_scripts.py script
	}
}