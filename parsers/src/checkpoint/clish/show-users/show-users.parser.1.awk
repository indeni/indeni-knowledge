BEGIN {
	# Lines are separated by ":"
	FS=":"
}

# mrma:users:user:indeni t
/mrma:users:user:/ {
	# Get only lines with 4 columns, the others are not relevant or duplicates
    if (NF == 4) {
		iuser++
		user=$4
		gsub(/ t/,"",user)
		users[iuser, "username"]=user
	}
}

END {
	writeComplexMetricObjectArray("users", null, users, "false")  # Converted to new syntax by change_ind_scripts.py script
}