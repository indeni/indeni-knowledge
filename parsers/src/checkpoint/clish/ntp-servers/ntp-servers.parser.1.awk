BEGIN {
    # Lines are separated by ":"
    FS = ":"

    num_fields = 3;
}

#ntp:servers:secondary someserever.com
#ntp:servers:primary time.nist.gov
/^ntp:servers:(primary|secondary)/ {
    data = $3
    split(data, split_arr, " ")

    type = split_arr[1]
    server = split_arr[2]

    ntp_arr[server, "type"] = type
    ntp_arr[server, "ipaddress"] = server
}

#ntp:server:time.nist.gov:version 3
#ntp:server:someserever.com:version 1
/^ntp:server:.*:version/ {
    server = $3
    version = $NF
    sub(/version /, "", version)
    ntp_arr[server, "version"] = version
}


END {

    # This final section is there to verify that we actually got all the data we were looking for.
    # For some reason Checkpoint devices sometimes seem to omit the last line when issuing "grep"
    # to filter the output. This final piece of code verifies that the number of items in the
    # object array is divisible by num_fields (the number of entries per object array).

    # For more information:
    # https://indeni.atlassian.net/browse/IKP-1840

    total_found_fields = 0
    for (i in ntp_arr) {
        total_found_fields ++
    }

    if (total_found_fields % num_fields == 0) {
        writeComplexMetricObjectArray("ntp-servers", null, ntp_arr, "true", "NTP Servers")  # Converted to new syntax by change_ind_scripts.py script
    } # TODO: Throw exception if this is not true
}