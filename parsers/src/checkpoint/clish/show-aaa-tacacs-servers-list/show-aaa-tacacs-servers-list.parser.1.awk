# aaa_tacp:auth_profile:base_tacplus_authprofile:tacp_srv:1:timeout 5
# aaa:auth_profile:base_tacplus_authprofile:tacplus_srv:1:timeout 5

/aaa[^:]*:auth_profile:base_tacplus_authprofile:tacp[a-z_]*_srv:[0-9]+:timeout/ {
	split($1,splitArr,":")
	priority=splitArr[5]

	tacacs[priority, "priority"]=priority
	tacacs[priority, "timeout"]=$NF
}

# aaa_tacp:auth_profile:base_tacplus_authprofile:tacp_srv:1:host 1.1.11.1
# aaa :auth_profile:base_tacplus_authprofile:tacplus_srv:2:host 3.3.3.3
/aaa[^:]*:auth_profile:base_tacplus_authprofile:tacp[a-z_]*_srv:[0-9]+:host/ {
	split($1,splitArr,":")
	priority=splitArr[5]
	tacacs[priority, "host"]=$NF
}

# For Gaia:
# aaa_tacp:auth_profile:base_tacplus_authprofile:tacp_srv:state on
/aaa[^:]*:auth_profile:base_tacplus_authprofile:tacp_srv:state/ {
	if ($NF == "on") {
		status = "true"
	} else {
		status = "false"
	}
	writeComplexMetricString("tacacs-enabled", null, status, "false")  # Converted to new syntax by change_ind_scripts.py script
}

# For IPSO:
# aaa:auth_profile:base_tacplus_authprofile t
/aaa:auth_profile:base_tacplus_authprofile \S/ {
	if ($NF == "t") {
		status = "true"
	} else {
		status = "false"
	}
	writeComplexMetricString("tacacs-enabled", null, status, "false")  # Converted to new syntax by change_ind_scripts.py script
}

END {
	writeComplexMetricObjectArray("tacacs-servers", null, tacacs, "true", "TACACS Servers")  # Converted to new syntax by change_ind_scripts.py script
}