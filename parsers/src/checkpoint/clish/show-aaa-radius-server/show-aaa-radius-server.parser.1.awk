# aaa:auth_profile:base_radius_authprofile:radius_srv:1:timeout 4
/aaa:auth_profile:base_radius_authprofile:radius_srv:[0-9]+:timeout/ {
	split($1, timeoutSplitArr, ":")
	priority = timeoutSplitArr[5]

	servers[priority, "priority"] = priority
	servers[priority, "timeout"] = $NF
}

# aaa:auth_profile:base_radius_authprofile:radius_srv:1:host 2.2.2.2
/aaa:auth_profile:base_radius_authprofile:radius_srv:[0-9]+:host/ {
	split($1, hostSplitArr, ":")
	priority = hostSplitArr[5]
	servers[priority, "host"] = $NF
}

# aaa:auth_profile:base_radius_authprofile:radius_srv:1:port 1812
/aaa:auth_profile:base_radius_authprofile:radius_srv:[0-9]+:port/ {
	split($1, portSplitArr, ":")
	priority = portSplitArr[5]
	servers[priority, "port"] = $NF
}

# aaa:auth_profile:base_radius_authprofile:radius_srv:super-user-uid 96
/aaa:auth_profile:base_radius_authprofile:radius_srv:super-user-uid/ {
	if (arraylen(servers)) {
		writeComplexMetricString("radius-super-user-id", null, $NF, "false")  # Converted to new syntax by change_ind_scripts.py script
	}
}

END {
	writeComplexMetricObjectArray("radius-servers", null, servers, "false")  # Converted to new syntax by change_ind_scripts.py script
}