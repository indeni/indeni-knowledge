import json
from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser
import re

class CheckpointShowDhcpServerAllGw(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            parsed_data = helper_methods.parse_data_as_list(raw_data, 'show_dhcp_server_all.textfsm')
            scope_parsed_data = helper_methods.parse_data_as_list(raw_data, 'show_dhcp_server_scopes.textfsm')
            if parsed_data:
                dhcp_data = []
                all_pools = []
                tags = {}
                self.write_double_metric('dhcp-server-status', {}, 'gauge', 1 if parsed_data[0]['dhcp_server_state'] == 'Enabled' else 0, True, 'DHCP server status', 'state')
                for data in parsed_data:
                    entry = {}
                    entry['subnet'] = data['subnet']
                    entry['state'] = data['dhcp_subnet_state']
                    entry['netmask'] = data['netmask']
                    entry['default_gateway'] = data['default_gateway']
                    entry['dns'] = data['dns']
                    entry['default_lease'] = data['default_lease']
                    entry['max_lease'] = data['max_lease']
                    entry['domain'] = data['domain']
                    subnet_pools = [scope for scope in scope_parsed_data if scope['subnet'] == data['subnet']]
                    entry['pools'] = subnet_pools
                    dhcp_data.append({'subnet_config': str(json.dumps(entry))})
                    tags['name'] = entry['subnet']
                    self.write_double_metric('dhcp-scope-status', tags, 'gauge', 1 if entry['state'] == 'Enabled' else 0, True, 'DHCP scope status', 'state', 'name')
                    for dhcp_pool_data in subnet_pools:
                        tags['name'] = f"scope: {dhcp_pool_data['subnet']} pool: {dhcp_pool_data['pool']}"
                        self.write_double_metric('dhcp-pool-status', tags, 'gauge', 1 if dhcp_pool_data['pool_status'] == 'enabled' else 0, True, 'DHCP pool status', 'state', 'name')
                    all_pools += subnet_pools
                self.write_complex_metric_object_array('dhcp-server-config', {}, dhcp_data, False)
        return self.output
    
class CheckpointShowDhcpServerAllVsx(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            vs_info_parsed = helper_methods.parse_data_as_list(raw_data, 'vs_info.textfsm')
            blade_vs_regex = r'(^\d+)\_(\d+)\:?(\d+)?'
            outputs = re.split(blade_vs_regex, raw_data, 0, re.MULTILINE)
            outputs.pop(0)
            id = 0
            dhcp_data = []
            while id < len(outputs):
                blade_metrics = CheckpointShowDhcpServerAllGw.parse(CheckpointShowDhcpServerAllGw() ,outputs[id+3], {}, {})
                for metric in blade_metrics:
                    if outputs[id+2] is not None:
                        metric.tags['vs.id'] = outputs[id+2]
                        metric.tags['vs.name'] = [vs_info['vs_name'] for vs_info in vs_info_parsed if vs_info['vs_id']== outputs[id+2]][0]
                    if outputs[id+1] != '99':
                        metric.tags['name'] = ' '.join(('VSID_{}'.format(outputs[id+2]), metric.tags['name']))
                    if outputs[id+1] is not None and outputs[id+1] != '99':
                        metric.tags['blade'] = outputs[id+1]
                        metric.tags['name'] = ' '.join(('blade_{}'.format(outputs[id+1]), metric.tags['name']))
                    if metric.name == 'dhcp-server-status':
                        self.write_double_metric('dhcp-server-status', metric.tags, 'gauge', metric.value, metric.tags.get('live-config'), metric.tags.get('display-name'), metric.tags.get('im.dstype.displayType'), metric.tags.get('im.identity-tags'))
                    if metric.name == 'dhcp-scope-status':
                        self.write_double_metric('dhcp-scope-status', metric.tags, 'gauge', metric.value, metric.tags.get('live-config'), metric.tags.get('display-name'), metric.tags.get('im.dstype.displayType'), metric.tags.get('im.identity-tags'))
                    if metric.name == 'dhcp-server-config':
                        for entry in metric.value:
                            dhcp_config = f'{entry["subnet_config"][:1]}"VS_ID": "{metric.tags["vs.id"]}", {entry["subnet_config"][1:]}'
                            dhcp_data.append({'subnet_config':dhcp_config})
                    if metric.name == 'dhcp-pool-status':
                        self.write_double_metric('dhcp-pool-status', metric.tags, 'gauge', metric.value, metric.tags.get('live-config'), metric.tags.get('display-name'), metric.tags.get('im.dstype.displayType'), metric.tags.get('im.identity-tags'))
                id += 4
            self.write_complex_metric_object_array('dhcp-server-config', {}, dhcp_data, False)
        return self.output