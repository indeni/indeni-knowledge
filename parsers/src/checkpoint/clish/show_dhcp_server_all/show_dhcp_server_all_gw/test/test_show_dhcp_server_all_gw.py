import os
import unittest
from checkpoint.clish.show_dhcp_server_all.show_dhcp_server_all_gw.show_dhcp_server_all_gw import ShowDhcpServerAllGw
from parser_service.public.action import *

class TestShowDhcpServerAllGw(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = ShowDhcpServerAllGw()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_show_dhcp_server_all_enabled_gw(self):
        result = self.parser.parse_file(self.current_dir + '/show_dhcp_server_all_enabled_gw.input', {}, {})
        self.assertEqual(9, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'dhcp-server-status')
        self.assertEqual(result[0].tags, {'live-config': 'true', 'display-name': 'DHCP server status', 'im.dstype.displayType': 'state'})
        self.assertEqual(result[0].value, 1)
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].name, 'dhcp-scope-status')
        self.assertEqual(result[1].tags, {'name': '172.16.50.0', 'live-config': 'true', 'display-name': 'DHCP scope status', 'im.dstype.displayType': 'state', 'im.identity-tags': 'name'})
        self.assertEqual(result[1].value, 1)
        self.assertEqual(result[2].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[2].name, 'dhcp-pool-status')
        self.assertEqual(result[2].tags, {'name': 'scope: 172.16.50.0 pool: 172.16.50.20-172.16.50.90', 'live-config': 'true', 'display-name': 'DHCP pool status', 'im.dstype.displayType': 'state', 'im.identity-tags': 'name'})
        self.assertEqual(result[2].value, 1)
        self.assertEqual(result[3].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[3].name, 'dhcp-pool-status')
        self.assertEqual(result[3].tags, {'name': 'scope: 172.16.50.0 pool: 172.16.50.120-172.16.50.150', 'live-config': 'true', 'display-name': 'DHCP pool status', 'im.dstype.displayType': 'state', 'im.identity-tags': 'name'})
        self.assertEqual(result[3].value, 0)
        self.assertEqual(result[4].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[4].name, 'dhcp-pool-status')
        self.assertEqual(result[4].tags, {'name': 'scope: 172.16.50.0 pool: 172.16.50.155-172.16.50.254', 'live-config': 'true', 'display-name': 'DHCP pool status', 'im.dstype.displayType': 'state', 'im.identity-tags': 'name'})
        self.assertEqual(result[4].value, 1)
        self.assertEqual(result[5].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[5].name, 'dhcp-scope-status')
        self.assertEqual(result[5].tags, {'name': '172.16.51.0', 'live-config': 'true', 'display-name': 'DHCP scope status', 'im.dstype.displayType': 'state', 'im.identity-tags': 'name'})
        self.assertEqual(result[5].value, 0)
        self.assertEqual(result[6].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[6].name, 'dhcp-pool-status')
        self.assertEqual(result[6].tags, {'name': 'scope: 172.16.51.0 pool: 172.16.51.10-172.16.51.99', 'live-config': 'true', 'display-name': 'DHCP pool status', 'im.dstype.displayType': 'state', 'im.identity-tags': 'name'})
        self.assertEqual(result[6].value, 1)
        self.assertEqual(result[7].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[7].name, 'dhcp-scope-status')
        self.assertEqual(result[7].tags, {'name': '172.16.52.0', 'live-config': 'true', 'display-name': 'DHCP scope status', 'im.dstype.displayType': 'state', 'im.identity-tags': 'name'})
        self.assertEqual(result[7].value, 0)
        self.assertEqual(result[8].action_type, 'WriteComplexMetric')
        self.assertEqual(result[8].name, 'dhcp-server-config')
        self.assertEqual(result[8].tags, {})
        self.assertEqual(result[8].value, [{'subnet_config': '{"subnet": "172.16.50.0", "state": "Enabled", "netmask": "24", "default_gateway": "172.16.50.103", "dns": "172.16.50.101,172.16.50.103", "default_lease": "43200", "max_lease": "86400", "domain": "example.com", "pools": [{"subnet": "172.16.50.0", "pool": "172.16.50.20-172.16.50.90", "status": "enabled", "type": "Include"}, {"subnet": "172.16.50.0", "pool": "172.16.50.120-172.16.50.150", "status": "disabled", "type": "Include"}, {"subnet": "172.16.50.0", "pool": "172.16.50.155-172.16.50.254", "status": "enabled", "type": "Exclude"}]}'}, 
                                           {'subnet_config': '{"subnet": "172.16.51.0", "state": "Disabled", "netmask": "24", "default_gateway": "", "dns": "", "default_lease": "43200", "max_lease": "86400", "domain": "", "pools": [{"subnet": "172.16.51.0", "pool": "172.16.51.10-172.16.51.99", "status": "enabled", "type": "Include"}]}'}, 
                                           {'subnet_config': '{"subnet": "172.16.52.0", "state": "Disabled", "netmask": "24", "default_gateway": "", "dns": "", "default_lease": "43200", "max_lease": "86400", "domain": "", "pools": []}'}])

    def test_show_dhcp_server_all_enabled_min_configuration_required_gw(self):
        result = self.parser.parse_file(self.current_dir + '/show_dhcp_server_all_enabled_min_configuration_required_gw.input', {}, {})
        self.assertEqual(3, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'dhcp-server-status')
        self.assertEqual(result[0].tags, {'live-config': 'true', 'display-name': 'DHCP server status', 'im.dstype.displayType': 'state'})
        self.assertEqual(result[0].value, 1)
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].name, 'dhcp-scope-status')
        self.assertEqual(result[1].tags, {'name': '172.16.50.0', 'live-config': 'true', 'display-name': 'DHCP scope status', 'im.dstype.displayType': 'state', 'im.identity-tags': 'name'})
        self.assertEqual(result[1].value, 1)
        self.assertEqual(result[2].action_type, 'WriteComplexMetric')
        self.assertEqual(result[2].name, 'dhcp-server-config')
        self.assertEqual(result[2].tags, {})
        self.assertEqual(result[2].value[0], {'subnet_config': '{"subnet": "172.16.50.0", "state": "Enabled", "netmask": "24", "default_gateway": "", "dns": "", "default_lease": "", "max_lease": "", "domain": "", "pools": []}'})

    def test_show_dhcp_server_all_disabled_gw(self):
        result = self.parser.parse_file(self.current_dir + '/show_dhcp_server_all_disabled_gw.input', {}, {})
        self.assertEqual(9, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'dhcp-server-status')
        self.assertEqual(result[0].tags, {'live-config': 'true', 'display-name': 'DHCP server status', 'im.dstype.displayType': 'state'})
        self.assertEqual(result[0].value, 0)
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].name, 'dhcp-scope-status')
        self.assertEqual(result[1].tags, {'name': '172.16.50.0', 'live-config': 'true', 'display-name': 'DHCP scope status', 'im.dstype.displayType': 'state', 'im.identity-tags': 'name'})
        self.assertEqual(result[1].value, 1)
        self.assertEqual(result[2].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[2].name, 'dhcp-pool-status')
        self.assertEqual(result[2].tags, {'name': 'scope: 172.16.50.0 pool: 172.16.50.20-172.16.50.90', 'live-config': 'true', 'display-name': 'DHCP pool status', 'im.dstype.displayType': 'state', 'im.identity-tags': 'name'})
        self.assertEqual(result[2].value, 1)
        self.assertEqual(result[3].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[3].name, 'dhcp-pool-status')
        self.assertEqual(result[3].tags, {'name': 'scope: 172.16.50.0 pool: 172.16.50.120-172.16.50.150', 'live-config': 'true', 'display-name': 'DHCP pool status', 'im.dstype.displayType': 'state', 'im.identity-tags': 'name'})
        self.assertEqual(result[3].value, 0)
        self.assertEqual(result[4].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[4].name, 'dhcp-pool-status')
        self.assertEqual(result[4].tags, {'name': 'scope: 172.16.50.0 pool: 172.16.50.155-172.16.50.254', 'live-config': 'true', 'display-name': 'DHCP pool status', 'im.dstype.displayType': 'state', 'im.identity-tags': 'name'})
        self.assertEqual(result[4].value, 1)
        self.assertEqual(result[5].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[5].name, 'dhcp-scope-status')
        self.assertEqual(result[5].tags, {'name': '172.16.51.0', 'live-config': 'true', 'display-name': 'DHCP scope status', 'im.dstype.displayType': 'state', 'im.identity-tags': 'name'})
        self.assertEqual(result[5].value, 0)
        self.assertEqual(result[6].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[6].name, 'dhcp-pool-status')
        self.assertEqual(result[6].tags, {'name': 'scope: 172.16.51.0 pool: 172.16.51.10-172.16.51.99', 'live-config': 'true', 'display-name': 'DHCP pool status', 'im.dstype.displayType': 'state', 'im.identity-tags': 'name'})
        self.assertEqual(result[6].value, 1)
        self.assertEqual(result[7].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[7].name, 'dhcp-scope-status')
        self.assertEqual(result[7].tags, {'name': '172.16.52.0', 'live-config': 'true', 'display-name': 'DHCP scope status', 'im.dstype.displayType': 'state', 'im.identity-tags': 'name'})
        self.assertEqual(result[7].value, 0)
        self.assertEqual(result[8].action_type, 'WriteComplexMetric')
        self.assertEqual(result[8].name, 'dhcp-server-config')
        self.assertEqual(result[8].tags, {})
        self.assertEqual(result[8].value, [{'subnet_config': '{"subnet": "172.16.50.0", "state": "Enabled", "netmask": "24", "default_gateway": "172.16.50.103", "dns": "172.16.50.101,172.16.50.103", "default_lease": "43200", "max_lease": "86400", "domain": "example.com", "pools": [{"subnet": "172.16.50.0", "pool": "172.16.50.20-172.16.50.90", "status": "enabled", "type": "Include"}, {"subnet": "172.16.50.0", "pool": "172.16.50.120-172.16.50.150", "status": "disabled", "type": "Include"}, {"subnet": "172.16.50.0", "pool": "172.16.50.155-172.16.50.254", "status": "enabled", "type": "Exclude"}]}'}, 
                                          {'subnet_config': '{"subnet": "172.16.51.0", "state": "Disabled", "netmask": "24", "default_gateway": "", "dns": "", "default_lease": "43200", "max_lease": "86400", "domain": "", "pools": [{"subnet": "172.16.51.0", "pool": "172.16.51.10-172.16.51.99", "status": "enabled", "type": "Include"}]}'}, 
                                          {'subnet_config': '{"subnet": "172.16.52.0", "state": "Disabled", "netmask": "24", "default_gateway": "", "dns": "", "default_lease": "43200", "max_lease": "86400", "domain": "", "pools": []}'}])

    def test_show_dhcp_server_all_disabled_no_configuration_gw(self):
        result = self.parser.parse_file(self.current_dir + '/show_dhcp_server_all_disabled_no_configuration_gw.input', {}, {})
        self.assertEqual(0, len(result))

if __name__ == '__main__':
    unittest.main()