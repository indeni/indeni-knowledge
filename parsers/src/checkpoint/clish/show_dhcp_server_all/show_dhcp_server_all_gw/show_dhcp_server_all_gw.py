import json
from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class ShowDhcpServerAllGw(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            parsed_data = helper_methods.parse_data_as_list(raw_data, 'show_dhcp_server_all.textfsm')
            scope_parsed_data = helper_methods.parse_data_as_list(raw_data, 'show_dhcp_server_scopes.textfsm')
            if parsed_data:
                dhcp_data = []
                all_pools = []
                tags = {}
                self.write_double_metric('dhcp-server-status', {}, 'gauge', 1 if parsed_data[0]['dhcp_server_state'] == 'Enabled' else 0, True, 'DHCP server status', 'state')
                for data in parsed_data:
                    entry = {}
                    entry['subnet'] = data['subnet']
                    entry['state'] = data['dhcp_subnet_state']
                    entry['netmask'] = data['netmask']
                    entry['default_gateway'] = data['default_gateway']
                    entry['dns'] = data['dns']
                    entry['default_lease'] = data['default_lease']
                    entry['max_lease'] = data['max_lease']
                    entry['domain'] = data['domain']
                    subnet_pools = [scope for scope in scope_parsed_data if scope['subnet'] == data['subnet']]
                    entry['pools'] = subnet_pools
                    dhcp_data.append({'subnet_config': str(json.dumps(entry))})
                    tags['name'] = entry['subnet']
                    self.write_double_metric('dhcp-scope-status', tags, 'gauge', 1 if entry['state'] == 'Enabled' else 0, True, 'DHCP scope status', 'state', 'name')
                    for dhcp_pool_data in subnet_pools:
                        tags['name'] = f"scope: {dhcp_pool_data['subnet']} pool: {dhcp_pool_data['pool']}"
                        self.write_double_metric('dhcp-pool-status', tags, 'gauge', 1 if dhcp_pool_data['status'] == 'enabled' else 0, True, 'DHCP pool status', 'state', 'name')
                    all_pools += subnet_pools
                self.write_complex_metric_object_array('dhcp-server-config', {}, dhcp_data, False)
        return self.output
    
    