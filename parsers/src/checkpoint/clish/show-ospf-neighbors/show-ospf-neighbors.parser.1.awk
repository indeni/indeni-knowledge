# The following two sections has been added by request of Dan Shouky
# https://indeni.atlassian.net/browse/IKP-1221

# Unfortunately, the following code is duplicated in many .ind scripts.
# If you change something in the following two sections, please find all
# of the other instances of this code and make the change there also.

#Could not acquire the config lock
/Could not acquire the config lock/ {
    if (NR == 1) {
        next
    }
}

#CLINFR0829  Unable to get user permissions
#CLINFR0819  User: johndoe denied access via CLI
#CLINFR0599  Failed to build ACLs
/(CLINFR0829\s+Unable to get user permissions|CLINFR0819\s+User: .+ denied access via CLI|CLINFR0599\s+Failed to build ACLs)/ {
    exit
}

# Make sure line starts with some ID
#1.142.128.82      1     2WAY           39      10.201.134.32     10.201.134.1      0
#1.142.128.104     1     FULL/BDR       34      10.201.134.131    10.201.134.1      0
#10.0.0.50         1     INIT/DR        34      10.0.0.50         10.0.0.4          0
/^[0-9\.]{4}/ {
    # Verify we have the number of columns we thought we would
    if (NF == 7) {

        state_desc = $3

        tags["name"] = $1 " priority: " $2 " address: " $5
        tags["state"] = state_desc

        state = 0
        if (match(state_desc, /FULL|2WAY/)) {
            state = 1
        }

        writeDoubleMetric("ospf-state", tags, "gauge", state, "true", "OSPF Neighbors", "state", "name")  # Converted to new syntax by change_ind_scripts.py script

    }
}