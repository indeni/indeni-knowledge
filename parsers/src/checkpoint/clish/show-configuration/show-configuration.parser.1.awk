BEGIN {
    i = 0;
}

/^\s*$/ {
    next   
}

{
    configurationObj[i++, "line"] = $0
}

END {
    writeComplexMetricObjectArray("configuration-content", null, configurationObj, "false")  # Converted to new syntax by change_ind_scripts.py script
}