############
# Why: Get which timezone are configured, to make sure they are the same across sites or devices.
# How: Parse the gaia configuration file to get the information.
# Caveats: We avoid running clish commands due to the excessive logs in /var/log/messages that creates.
###########

# timezone Etc/GMT-12
/timezone/ {
	timezone=$2
}

END {
	if (timezone) {
		writeComplexMetricString("timezone", null, timezone, "true", "Timezone")  # Converted to new syntax by change_ind_scripts.py script
	}
}