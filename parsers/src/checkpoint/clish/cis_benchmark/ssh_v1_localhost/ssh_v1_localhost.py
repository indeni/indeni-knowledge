from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class Sshv1Localhost(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data and raw_data.strip() != '':
            ssh_v1_disabled_message = ['SSH protocol v.1 is no longer supported', 'Protocol major versions differ: 1 vs. 2']
            tags ={
                'control' : 'Remote management using SSH v2 and not SSH v1',
                'level' : '1'
            }
            self.write_double_metric('ssh-version-1-enabled', tags, 'gauge', 0 if raw_data in ssh_v1_disabled_message else 1, True, 'SSH Benchmark', 'state', "control")

        return self.output