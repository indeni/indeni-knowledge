import os
import unittest

from checkpoint.clish.cis_benchmark.ssh_v1_localhost.ssh_v1_localhost import Sshv1Localhost
from parser_service.public.action import WriteDoubleMetric


class TestSshv1Localhost(unittest.TestCase):
    def setUp(self):
        self.parser = Sshv1Localhost()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))


    def test_sshv1_not_supported(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/sshv1_not_supported.input', {}, {})

        # Assert
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].name, 'ssh-version-1-enabled')
        self.assertEqual(result[0].value, 0)


    def test_ssh_v2(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/ssh_v2.input', {}, {})

        # Assert
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].name, 'ssh-version-1-enabled')
        self.assertEqual(result[0].value, 0)