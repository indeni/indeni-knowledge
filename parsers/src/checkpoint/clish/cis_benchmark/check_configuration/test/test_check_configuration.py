import os
import unittest
from checkpoint.clish.cis_benchmark.check_configuration.check_configuration import CheckConfiguration
from parser_service.public.action import WriteDoubleMetric
class TestCheckConfiguration(unittest.TestCase):
    def setUp(self):
        self.parser = CheckConfiguration()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_banner_on_message_on(self):
        result = self.parser.parse_file(self.current_dir + '/banner_on_message_on.input', {}, {})
        self.assertEqual(12, len(result))
        self.assertEqual(result[0].name, 'login-banner')
        self.assertEqual(result[0].value['value'], '"Organization Banner"')
        self.assertEqual(result[0].tags['cis.control'], "Ensure 'Login Banner' is set")
        self.assertEqual(result[0].tags['cis.level'], '1')
        self.assertEqual(result[0].tags['cis.benchmark'], 'true')

    def test_banner_on_message_off(self):
        result = self.parser.parse_file(self.current_dir + '/banner_on_message_off.input', {}, {})
        self.assertEqual(12, len(result))
        self.assertEqual(result[0].name, 'login-banner')
        self.assertEqual(result[0].value['value'], 'This system is for authorized use only.')
        self.assertEqual(result[0].tags['cis.control'], "Ensure 'Login Banner' is set")
        self.assertEqual(result[0].tags['cis.level'], '1')
        self.assertEqual(result[0].tags['cis.benchmark'], 'true')

    def test_banner_off_message_on(self):
        result = self.parser.parse_file(self.current_dir + '/banner_off_message_on.input', {}, {})
        self.assertEqual(12, len(result))
        self.assertEqual(result[0].name, 'login-banner')
        self.assertEqual(result[0].value['value'], '')
        self.assertEqual(result[0].tags['cis.control'], "Ensure 'Login Banner' is set")
        self.assertEqual(result[0].tags['cis.level'], '1')
        self.assertEqual(result[0].tags['cis.benchmark'], 'true')

    def test_inactivity_timeout_8(self):
        result = self.parser.parse_file(self.current_dir + '/inactivity_timeout_8.input', {}, {})
        self.assertEqual(12, len(result))
        self.assertEqual(result[1].name, 'ssh-timeout')
        self.assertEqual(result[1].value, 480000)
        self.assertEqual(result[1].tags['cis.control'], "SSH session timeout for inactivity")
        self.assertEqual(result[1].tags['cis.level'], '1')
        self.assertEqual(result[1].tags['cis.benchmark'], 'true')

    def test_inactivity_timeout_10(self):
        result = self.parser.parse_file(self.current_dir + '/inactivity_timeout_10.input', {}, {})
        self.assertEqual(12, len(result))
        self.assertEqual(result[1].name, 'ssh-timeout')
        self.assertEqual(result[1].value, 600000)
        self.assertEqual(result[1].tags['cis.control'], "SSH session timeout for inactivity")
        self.assertEqual(result[1].tags['cis.level'], '1')
        self.assertEqual(result[1].tags['cis.benchmark'], 'true')

    def test_web_timeout(self):
        result = self.parser.parse_file(self.current_dir + '/inactivity_timeout_10.input', {}, {})
        self.assertEqual(12, len(result))
        self.assertEqual(result[2].name, 'web-timeout')
        self.assertEqual(result[2].value, 600000)

    def test_missing_timeout(self):
        result = self.parser.parse_file(self.current_dir + '/missing_inactivity_timeout.input', {}, {})
        self.assertEqual(12, len(result))
        self.assertEqual(result[3].name, 'missing-web-timeout')
        self.assertEqual(result[3].value, 0)

    def test_password_length_10(self):
        result = self.parser.parse_file(self.current_dir + '/password_length_10.input', {}, {})
        self.assertEqual(12, len(result))
        self.assertEqual(result[4].name, 'min-passwd-length')
        self.assertEqual(result[4].value, 6)
        self.assertEqual(result[4].tags['cis.control'], 'Ensure Minimum Password Length is set to 14 or higher')
        self.assertEqual(result[4].tags['cis.level'], '1')
        self.assertEqual(result[4].tags['cis.benchmark'], 'true')

    def test_password_complexity_2(self):
        result = self.parser.parse_file(self.current_dir + '/password_complexity_2.input', {}, {})
        self.assertEqual(12, len(result))
        self.assertEqual(result[5].name, 'chkp-password-complexity')
        self.assertEqual(result[5].value, 2)
        self.assertEqual(result[5].tags['cis.control'], 'Ensure Password Complexity is set to 3')
        self.assertEqual(result[5].tags['cis.level'], '1')
        self.assertEqual(result[5].tags['cis.benchmark'], 'true')

    def test_password_denial_on_fail_false(self):
        result = self.parser.parse_file(self.current_dir + '/password_denial_on_fail_false.input', {}, {})
        self.assertEqual(12, len(result))
        self.assertEqual(result[6].name, 'chkp-max-login-failed-attempts')
        self.assertEqual(result[6].value, 1)
        self.assertEqual(result[6].tags['cis.control'], 'Ensure Deny access after failed login attempts is not selected')
        self.assertEqual(result[6].tags['cis.level'], '1')
        self.assertEqual(result[6].tags['cis.benchmark'], 'true')

    def test_allowed_client_host_any(self):
        result = self.parser.parse_file(self.current_dir + '/allowed_client_host_any.input', {}, {})
        self.assertEqual(12, len(result))
        self.assertEqual(result[7].name, 'chkp-allowed-host-any')
        self.assertEqual(result[7].value, 0)
        self.assertEqual(result[7].tags['cis.control'], 'Ensure allowed-client is set to those necessary for device management')
        self.assertEqual(result[7].tags['cis.level'], '2')
        self.assertEqual(result[7].tags['cis.benchmark'], 'true')

    def test_allowed_client_host_not_any(self):
        result = self.parser.parse_file(self.current_dir + '/allowed_client_host_not_any.input', {}, {})
        self.assertEqual(12, len(result))
        self.assertEqual(result[7].name, 'chkp-allowed-host-any')
        self.assertEqual(result[7].value, 1)
        self.assertEqual(result[7].tags['cis.control'], 'Ensure allowed-client is set to those necessary for device management')
        self.assertEqual(result[7].tags['cis.level'], '2')
        self.assertEqual(result[7].tags['cis.benchmark'], 'true')

    def test_logging_mgmtauditlogs_off(self):
        result = self.parser.parse_file(self.current_dir + '/logging_mgmtauditlogs_off.input', {}, {})
        self.assertEqual(12, len(result))
        self.assertEqual(result[8].name, 'chkp-mgmtauditlogs-enabled')
        self.assertEqual(result[8].value, 0)
        self.assertEqual(result[8].tags['cis.control'], 'Ensure mgmtauditlogs is set to on')
        self.assertEqual(result[8].tags['cis.level'], '1')

    def test_logging_mgmtauditlogs_on(self):
        result = self.parser.parse_file(self.current_dir + '/logging_mgmtauditlogs_on.input', {}, {})
        self.assertEqual(12, len(result))
        self.assertEqual(result[8].name, 'chkp-mgmtauditlogs-enabled')
        self.assertEqual(result[8].value, 1)
        self.assertEqual(result[8].tags['cis.control'], 'Ensure mgmtauditlogs is set to on')
        self.assertEqual(result[8].tags['cis.level'], '1')

    def test_logging_auditlog_disable(self):
        result = self.parser.parse_file(self.current_dir + '/logging_auditlog_disable.input', {}, {})
        self.assertEqual(12, len(result))
        self.assertEqual(result[9].name, 'chkp-auditlog-enabled')
        self.assertEqual(result[9].value, 0)
        self.assertEqual(result[9].tags['cis.control'], 'Ensure auditlog is set to permanent')
        self.assertEqual(result[9].tags['cis.level'], '1')

    def test_logging_auditlog_permanent(self):
        result = self.parser.parse_file(self.current_dir + '/logging_auditlog_permanent.input', {}, {})
        self.assertEqual(12, len(result))
        self.assertEqual(result[9].name, 'chkp-auditlog-enabled')
        self.assertEqual(result[9].value, 1)
        self.assertEqual(result[9].tags['cis.control'], 'Ensure auditlog is set to permanent')
        self.assertEqual(result[9].tags['cis.level'], '1')

    def test_command_failed(self):
        result = self.parser.parse_file(self.current_dir + '/command-failed.input', {}, {})
        self.assertEqual(0, len(result))

    def test_remote_syslog(self):
        result = self.parser.parse_file(self.current_dir + '/logging_remote_syslog.input', {}, {})
        self.assertEqual(result[11].name, 'chkp-remote-syslog-enabled')
        self.assertEqual(result[11].value, {'value': ['10.10.10.1', '10.10.10.2']})

    def test_logging_cplogs_off(self):
        result = self.parser.parse_file(self.current_dir + '/logging_cplogs_off.input', {}, {})
        self.assertEqual(12, len(result))
        self.assertEqual(result[10].name, 'chkp-systemlog-enabled')
        self.assertEqual(result[10].value, 0)
        self.assertEqual(result[10].tags['cis.control'], 'Ensure cplogs is set to on')
        self.assertEqual(result[10].tags['cis.level'], '1')

    def test_logging_cplogs_on(self):
        result = self.parser.parse_file(self.current_dir + '/logging_cplogs_on.input', {}, {})
        self.assertEqual(12, len(result))
        self.assertEqual(result[10].name, 'chkp-systemlog-enabled')
        self.assertEqual(result[10].value, 1)
        self.assertEqual(result[10].tags['cis.control'], 'Ensure cplogs is set to on')
        self.assertEqual(result[10].tags['cis.level'], '1')

    def test_command_failed(self):
        result = self.parser.parse_file(self.current_dir + '/command-failed.input', {}, {})
        self.assertEqual(0, len(result))

if __name__ == '__main__':
    unittest.main()