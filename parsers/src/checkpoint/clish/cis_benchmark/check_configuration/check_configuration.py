from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class CheckConfiguration(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data and raw_data.strip() != '':
            result = helper_methods.parse_data_as_list(raw_data, 'check_point_check_configuration.textfsm')
            if result:
                # print(result)
                tags = {
                    'cis.benchmark': 'true',
                    'cis.control' : 'Ensure \'Login Banner\' is set',
                    'cis.level' : '1'
                }
                banner = ''
                if result[0]['banner_state'] == 'on':
                    if len(result[0]['banner_text']) > 0:
                        banner = result[0]['banner_text']
                    else:
                        banner = 'This system is for authorized use only.'
                self.write_complex_metric_string('login-banner', tags, banner, False)

                tags ={
                    'cis.benchmark': 'true',
                    'cis.control' : 'SSH session timeout for inactivity',
                    'cis.level' : '1'
                }
                self.write_double_metric('ssh-timeout', tags, 'gauge', int(result[0]['inactivity_timeout'])*60000, False)


                self.write_double_metric('web-timeout', {}, 'gauge', int(result[0]['web_timeout'])*60000, False)
                self.write_double_metric('missing-web-timeout', {}, 'gauge', 0 if int(result[0]['web_timeout'])==0 else 1, False)


                tags ={
                    'cis.benchmark': 'true',
                    'cis.control' : 'Ensure Minimum Password Length is set to 14 or higher',
                    'cis.level' : '1'
                }
                self.write_double_metric('min-passwd-length', tags, 'gauge', int(result[0]['min_password_length']), False)

                tags ={
                    'cis.benchmark': 'true',
                    'cis.control' : 'Ensure Password Complexity is set to 3',
                    'cis.level' : '1'
                }
                self.write_double_metric('chkp-password-complexity', tags, 'gauge', int(result[0]['password_complexity']), False)

                tags ={
                    'cis.benchmark': 'true',
                    'cis.control' : 'Ensure Deny access after failed login attempts is not selected',
                    'cis.level' : '1'
                }
                self.write_double_metric('chkp-max-login-failed-attempts', tags, 'gauge', 1 if result[0]['password_deny_on_fail'] == 'false'  else 0, False)

                tags ={
                    'cis.benchmark': 'true',
                    'cis.control' : 'Ensure allowed-client is set to those necessary for device management',
                    'cis.level' : '2'
                }
                self.write_double_metric('chkp-allowed-host-any', tags, 'gauge', 0 if result[0]['allowed_client_host'] == 'any-host'  else 1, False)

                tags ={
                    'cis.benchmark': 'true',
                    'cis.control' : 'Ensure mgmtauditlogs is set to on',
                    'cis.level' : '1'
                }
                self.write_double_metric('chkp-mgmtauditlogs-enabled', tags, 'gauge', 1 if result[0]['mgmtauditlogs'] == 'on'  else 0, False)

                tags ={
                    'cis.benchmark': 'true',
                    'cis.control' : 'Ensure auditlog is set to permanent',
                    'cis.level' : '1'
                }
                self.write_double_metric('chkp-auditlog-enabled', tags, 'gauge', 1 if result[0]['auditlog'] == 'permanent'  else 0, False)
                tags ={
                    'cis.benchmark': 'true',
                    'cis.control' : 'Ensure cplogs is set to on',
                    'cis.level' : '1'
                }
                self.write_double_metric('chkp-systemlog-enabled', tags, 'gauge', 1 if result[0]['systemlog'] == 'on'  else 0, False)

                tags ={
                    'cis.benchmark': 'true',
                    'cis.control' : 'Ensure remote logging servers are configured',
                }
                self.write_complex_metric_string('chkp-remote-syslog-enabled', tags, result[0]['syslog_address'] if (len(result[0]['syslog_address']) > 0 ) else '', False)
  

        return self.output

# helper_methods.print_list(CheckConfiguration().parse_file("/Users/shoukyd/proj/indeni-kd/parsers/src/checkpoint/clish/cis_benchmark/check_configuration/test/banner_on_message_on.input", {}, {}))