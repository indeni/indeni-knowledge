from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser
#from checkpoint.clish.show_bgp_peers.show_bgp_peers_gw.show_bgp_peers_gw import ShowBgpPeersGw
import re

class ShowBgpPeersGw(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        # Step 1 : Data Extraction
        if raw_data:
            data = helper_methods.parse_data_as_list(raw_data, 'show_bgp_peers.textfsm')
            tags = {}
            if data:
                for peer in data:
                    tags['name'] = peer['peer']
                    self.write_double_metric('bgp-state', tags, 'gauge', 1 if peer['state'] in ['Established', 'Idle'] else 0 , False)
        return self.output

class ShowBgpPeersVsx(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            vs_info_parsed = helper_methods.parse_data_as_list(raw_data, 'vs_info.textfsm')
            blade_vs_regex = r'(^\d+)\_(\d+)\:?(\d+)?'
            outputs = re.split(blade_vs_regex, raw_data, 0, re.MULTILINE)
            outputs.pop(0)
            id = 0
            while id < len(outputs):
                blade_metrics = ShowBgpPeersGw.parse(ShowBgpPeersGw() ,outputs[id+3], {}, {})
                for metric in blade_metrics:
                    if outputs[id+2] is not None:
                        metric.tags['vs.id'] = outputs[id+2]
                        metric.tags['vs.name'] = [vs_info['vs_name'] for vs_info in vs_info_parsed if vs_info['vs_id']== outputs[id+2]][0]
                        if outputs[id+1] != '99':
                            metric.tags['name'] = ' '.join(('VSID_{}'.format(outputs[id+2]), metric.tags['name']))
                    if outputs[id+1] is not None and outputs[id+1] != '99':
                        metric.tags['blade'] = outputs[id+1]
                        metric.tags['name'] = ' '.join(('blade_{}'.format(outputs[id+1]), metric.tags['name']))
                    if metric.name == 'bgp-state':
                        self.write_double_metric('bgp-state', metric.tags, 'gauge', metric.value, metric.tags.get('live-config'), metric.tags.get('display-name'), metric.tags.get('im.dstype.displayType'), metric.tags.get('im.identity-tags'))
                id += 4
        return self.output