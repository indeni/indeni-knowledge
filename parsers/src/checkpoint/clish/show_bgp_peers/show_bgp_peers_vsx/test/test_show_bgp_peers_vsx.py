import unittest
from checkpoint.clish.show_bgp_peers.show_bgp_peers_vsx.show_bgp_peers_vsx import ShowBgpPeersVsx
from parser_service.public.action import *

class TestShowBgpPeersVsx(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = ShowBgpPeersVsx()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))



    def test_multiple_vs_bgp(self):
        extracted_data = self.parser.parse_file(self.current_dir + '/multiple_vs_bgp.input', {}, {})
        self.assertEqual(4, len(extracted_data))
        self.assertEqual(extracted_data[0].name, 'bgp-state')
        self.assertEqual(extracted_data[0].value, 1)
        self.assertEqual(extracted_data[0].tags['vs.id'], '0')
        self.assertEqual(extracted_data[0].tags['vs.name'], 'VSX-1')
        self.assertEqual(extracted_data[0].tags['name'], '81.0.7.10')

        self.assertEqual(extracted_data[1].name, 'bgp-state')
        self.assertEqual(extracted_data[1].value, 1)
        self.assertEqual(extracted_data[1].tags['vs.id'], '1')
        self.assertEqual(extracted_data[1].tags['vs.name'], 'VS-1-R81')
        self.assertEqual(extracted_data[1].tags['name'], '81.0.7.10')

        self.assertEqual(extracted_data[2].name, 'bgp-state')
        self.assertEqual(extracted_data[2].value, 1)
        self.assertEqual(extracted_data[2].tags['vs.id'], '2')
        self.assertEqual(extracted_data[2].tags['vs.name'], 'VS-2-R81')
        self.assertEqual(extracted_data[2].tags['name'], '81.0.7.10')

        self.assertEqual(extracted_data[3].name, 'bgp-state')
        self.assertEqual(extracted_data[3].value, 1)
        self.assertEqual(extracted_data[3].tags['vs.id'], '3')
        self.assertEqual(extracted_data[3].tags['vs.name'], 'Router')
        self.assertEqual(extracted_data[3].tags['name'], '81.0.7.10')

    def test_new_bash_input(self):
        extracted_data = self.parser.parse_file(self.current_dir + '/new_bash_input.input', {}, {})
        self.assertEqual(1, len(extracted_data))
        self.assertEqual(extracted_data[0].name, 'bgp-state')
        self.assertEqual(extracted_data[0].value, 1)
        self.assertEqual(extracted_data[0].tags['vs.id'], '0')
        self.assertEqual(extracted_data[0].tags['vs.name'], 'VSX-1')
        self.assertEqual(extracted_data[0].tags['name'], '81.0.7.10')

if __name__ == '__main__':
    unittest.main()