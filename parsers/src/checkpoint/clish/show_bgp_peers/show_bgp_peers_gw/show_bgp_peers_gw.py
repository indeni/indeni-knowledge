from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class ShowBgpPeersGw(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        # Step 1 : Data Extraction
        if raw_data:
            data = helper_methods.parse_data_as_list(raw_data, 'show_bgp_peers.textfsm')
            tags = {}
            if data:
                for peer in data:
                    tags['name'] = peer['peer']
                    self.write_double_metric('bgp-state', tags, 'gauge', 1 if peer['state'] in ['Established', 'Idle'] else 0 , False)
        return self.output
