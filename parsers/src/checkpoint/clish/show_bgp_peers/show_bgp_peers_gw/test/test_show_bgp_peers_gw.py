import os
import unittest

from checkpoint.clish.show_bgp_peers.show_bgp_peers_gw.show_bgp_peers_gw import ShowBgpPeersGw
from parser_service.public.action import *

class TestShowBgpPeersGw(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = ShowBgpPeersGw()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_peer_idle(self):
        result = self.parser.parse_file(self.current_dir + '/peer_idle.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].name, 'bgp-state')
        self.assertEqual(result[0].value, 1)
        self.assertEqual(result[0].tags['name'], '81.0.7.10')

    def test_error_established_idle(self):
        result = self.parser.parse_file(self.current_dir + '/error_established_idle.input', {}, {})
        self.assertEqual(3, len(result))
        self.assertEqual(result[0].name, 'bgp-state')
        self.assertEqual(result[0].value, 0)
        self.assertEqual(result[0].tags['name'], '215.117.172.229')
        self.assertEqual(result[1].name, 'bgp-state')
        self.assertEqual(result[1].value, 1)
        self.assertEqual(result[1].tags['name'], '215.117.172.231')
        self.assertEqual(result[2].name, 'bgp-state')
        self.assertEqual(result[2].value, 1)
        self.assertEqual(result[2].tags['name'], '215.117.172.232')

    def test_could_not_acquire_the_config_lock(self):
        result = self.parser.parse_file(self.current_dir + '/could_not_acquire_the_config_lock.input', {}, {})
        self.assertEqual(3, len(result))
        self.assertEqual(result[0].name, 'bgp-state')
        self.assertEqual(result[0].value, 0)
        self.assertEqual(result[0].tags['name'], '215.117.172.229')
        self.assertEqual(result[1].name, 'bgp-state')
        self.assertEqual(result[1].value, 1)
        self.assertEqual(result[1].tags['name'], '215.117.172.231')
        self.assertEqual(result[2].name, 'bgp-state')
        self.assertEqual(result[2].value, 1)
        self.assertEqual(result[2].tags['name'], '215.117.172.232')

    def test_user_denied_access(self):
        result = self.parser.parse_file(self.current_dir + '/user_denied_access.input', {}, {})
        self.assertEqual(0, len(result))

    def test_unable_to_get_user_permission(self):
        result = self.parser.parse_file(self.current_dir + '/unable_to_get_user_permission.input', {}, {})
        self.assertEqual(0, len(result))

    def test_failed_to_build_acl(self):
        result = self.parser.parse_file(self.current_dir + '/failed_to_build_acl.input', {}, {})
        self.assertEqual(0, len(result))



if __name__ == '__main__':
    unittest.main()

    