# DAClient os run on a schedule and updates the da info in memory. When this does not match the saved config the config state will go to "unsaved" even though the administrator has not changed it.
# To work around this the value of "last_sent_da_info" stored in memory and on disk will be compared, and if they do not match it will assume that the config is saved, even though the command returns "unsaved"
# stty rows 80 ; is needed to make the script work in IPSO

BEGIN {
	unsavedState = ""
	dbgetRevision = ""
	configRevision = ""
}

# The following two sections has been added by request of Dan Shouky
# https://indeni.atlassian.net/browse/IKP-1221

# Unfortunately, the following code is duplicated in many .ind scripts.
# If you change something in the following two sections, please find all
# of the other instances of this code and make the change there also.

#Could not acquire the config lock
/Could not acquire the config lock/ {
	if (NR == 1) {
		next
	}
}

#CLINFR0829  Unable to get user permissions
#CLINFR0819  User: johndoe denied access via CLI
#CLINFR0599  Failed to build ACLs
/(CLINFR0829\s+Unable to get user permissions|CLINFR0819\s+User: .+ denied access via CLI|CLINFR0599\s+Failed to build ACLs)/ {
    criticalError = 1
	exit
}

#saved
#unsaved
/^(saved|unsaved)$/ {
	# When checking the savestate the device could return "unsaved" now and then, when the savestate is really "saved"
	# The script will run twice, and if any of the two results are "saved" then that is the assumed state.
	
	# Count each state
	if ( $1 == "unsaved" ) {
		unsaved++
	} else if ($1 == "saved") {
		saved++
	}

	if (saved >= 1) {
		unsavedState = 0
	} else if (unsaved >= 1) {
		unsavedState = 1
	}
}

#1505042713
/^[0-9]+$/ {
	dbgetRevision = $1
}

#installer:last_sent_da_info 1505042713
/:last_sent_da_info/ {
	configRevision = $2
}

END {

    # There was an issue when fetching the config, see the note above regarding IKP-1221
    # Exiting the script
    if (criticalError){
        exit
    }
	
	if (dbgetRevision != configRevision) {
		# If these revision are not a match, the configuration will show as unsaved even though it is saved.
		unsavedState = 0
	}

	if (unsavedState != "") {
		writeDoubleMetric("config-unsaved", null, "gauge", unsavedState, "true", "Configuration Unsaved?", "boolean", "")  # Converted to new syntax by change_ind_scripts.py script
	}
}