from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class ClishShowLom(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):

        # Step 1 : Data Extraction
        data = helper_methods.parse_data_as_list(raw_data, 'checkpoint_gaia_show_lom.textfsm')

        # Step 2 : Data Processing
        if data:
            # Step 3 : Data Reporting
            self.write_complex_metric_string('lom-firmware-version', {}, data[0]['lom_firmware_version'], True, 'LOM - Firmware version')
            self.write_complex_metric_string('lom-ip-address', {}, data[0]['lom_ip_address'], True, 'LOM - IP address')
            self.write_double_metric('lom-ip-address-default-value', {}, 'gauge', 1 if '192.168.0.100' in data[0]['lom_ip_address'] else 0, False)

        return self.output
