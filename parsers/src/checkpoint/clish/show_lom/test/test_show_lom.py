import os
import unittest

from checkpoint.clish.show_lom.show_lom import ClishShowLom
from parser_service.public.action import WriteComplexMetric, WriteDoubleMetric

class TestClishShowLom(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = ClishShowLom()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_lom_present_default_ip(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/lom_present_default_ip.input', {}, {})

        # Assert
        self.assertEqual(3, len(result))

        self.assertTrue(isinstance(result[0], WriteComplexMetric))
        self.assertEqual('2.17', result[0].value['value'])
        self.assertEqual('lom-firmware-version', result[0].name)
        self.assertEqual('LOM - Firmware version', result[0].tags['display-name'])
        self.assertTrue(result[0].tags['live-config'])

        self.assertTrue(isinstance(result[1], WriteComplexMetric))
        self.assertEqual("192.168.0.100", result[1].value['value'])
        self.assertEqual('lom-ip-address', result[1].name)
        self.assertEqual('LOM - IP address', result[1].tags['display-name'])
        self.assertTrue(result[0].tags['live-config'])

        self.assertTrue(isinstance(result[2], WriteDoubleMetric))
        self.assertEqual(1, result[2].value)
        self.assertEqual('lom-ip-address-default-value', result[2].name)
        self.assertEqual('gauge', result[2].ds_type)

    def test_lom_present_not_default_ip(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/lom_present_not_default_ip.input', {}, {})

        # Assert
        self.assertEqual(3, len(result))

        self.assertTrue(isinstance(result[0], WriteComplexMetric))
        self.assertEqual('3.17', result[0].value['value'])
        self.assertEqual('lom-firmware-version', result[0].name)
        self.assertEqual('LOM - Firmware version', result[0].tags['display-name'])
        self.assertTrue(result[0].tags['live-config'])

        self.assertTrue(isinstance(result[1], WriteComplexMetric))
        self.assertEqual("10.11.25.27", result[1].value['value'])
        self.assertEqual('lom-ip-address', result[1].name)
        self.assertEqual('LOM - IP address', result[1].tags['display-name'])
        self.assertTrue(result[0].tags['live-config'])

        self.assertTrue(isinstance(result[2], WriteDoubleMetric))
        self.assertEqual(0, result[2].value)
        self.assertEqual('lom-ip-address-default-value', result[2].name)
        self.assertEqual('gauge', result[2].ds_type)

    def test_lom_not_present(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/lom_not_present.input', {}, {})
        self.assertEqual(0, len(result))

if __name__ == '__main__':
    unittest.main()