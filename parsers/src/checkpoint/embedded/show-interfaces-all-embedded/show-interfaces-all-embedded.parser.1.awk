# The following two sections has been added by request of Dan Shouky
# https://indeni.atlassian.net/browse/IKP-1221

# Unfortunately, the following code is duplicated in many .ind scripts.
# If you change something in the following two sections, please find all
# of the other instances of this code and make the change there also.

#Could not acquire the config lock
/Could not acquire the config lock/ {
    if (NR == 1) {
        next
    }
}

#CLINFR0829  Unable to get user permissions
#CLINFR0819  User: johndoe denied access via CLI
#CLINFR0599  Failed to build ACLs
/(CLINFR0829\s+Unable to get user permissions|CLINFR0819\s+User: .+ denied access via CLI|CLINFR0599\s+Failed to build ACLs)/ {
    critical_error = 1
    exit
}

function getValue(s) {
    sub(/^.+:/, "", s)
    return s
}

# Function to calculate number of binary 1s in a decimal number
function count1s(N) {
    r = ""                    # initialize result to empty (not 0)
    while (N != 0) {            # as long as number still has a value
        r = ((N%2)?"1":"0") r   # prepend the modulos2 to the result
        N = int(N/2)            # shift right (integer division by 2)
    }

    # count number of 1s
    r = gsub(/1/, "", r)
    # Return result
    return r
}

# Function to convert a subnet_mask (example: 255.255.255.0) to subnet prefix (example: 24)
function subnetMaskToPrefix(subnet_mask) {
    split(subnet_mask, subnet_mask_arr, "\\.")
    prefix = count1s(subnet_mask_arr[1]) + count1s(subnet_mask_arr[2]) + count1s(subnet_mask_arr[3]) + count1s(subnet_mask_arr[4])
    return prefix
}


###########
# Script explanation: We want to get information about interfaces from several sources, for example ethtool. However the interfaces are named different
# in clish and ifconfig, and since ifconfig shows the same as ethtool we opted for ifconfig on gaia embedded.
# Also using clish for some of the info was too slow, since to display all the info we wanted we would have to print the detailed view for each interface.
# Due to this ifconfig is used for most of the info, but since we could not get data such as admin-state from it, we also use clish.
# The interfaces om gaia embedded is named differently in ifconfig and clish, so we also need to convert it correctly.
###########

#LAN1.246      Link encap:Ethernet  HWaddr 00:1C:7F:23:26:EB
/Link encap:/ {

    interface_name = $1
    stat_tags["name"] = interface_name
    interfaces[interface_name, "name"] = interface_name

    # Type
    type = getValue($3)
    writeComplexMetricString("network-interface-type", stat_tags, type, "true", "Network Interfaces - type")  # Converted to new syntax by change_ind_scripts.py script

    # MAC
    writeComplexMetricString("network-interface-mac", stat_tags, $5, "true", "Network Interfaces - MAC Address")  # Converted to new syntax by change_ind_scripts.py script

    next
}

#inet addr:192.168.245.2  Bcast:192.168.245.255  Mask:255.255.255.0
/inet addr:/ {
    ip = getValue($2)
    subnet = subnetMaskToPrefix(getValue($4))


    writeComplexMetricString("network-interface-ipv4-address", stat_tags, ip, "true", "Network Interfaces - IPv4 Address")  # Converted to new syntax by change_ind_scripts.py script
    writeComplexMetricString("network-interface-ipv4-subnet", stat_tags, subnet, "true", "Network Interfaces - IPv4 Netmask")  # Converted to new syntax by change_ind_scripts.py script

    # Make array for interfacename -> IP
    interface_arr[interface_name] = ip

    next
}


#BROADCAST MULTICAST  MTU:1500  Metric:1
#UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
/ MTU:/ {
    mtu = getValue($(NF-1))
    interfaces[interface_name, "mtu"] = mtu
    writeDoubleMetric("network-interface-mtu", stat_tags, "gauge", mtu, "true", "Network Interfaces - MTU", "number", "name")  # Converted to new syntax by change_ind_scripts.py script
    next
}

#		TX bytes:2096249117 packets:2975595223 errors:0 dropped:0 overruns:0 carrier:0
#		RX bytes:3891794260 packets:264187186 errors:0 dropped:0 overruns:0 frame:0
#		RX bytes:3964467449 (3.6 GiB)  TX bytes:922468769 (879.7 MiB)
#		RX packets:123982210 errors:0 dropped:0 overruns:0 frame:0
#		TX packets:61771739 errors:0 dropped:0 overruns:0 carrier:0
/(X bytes:|X packets:)/ {

    # Go over the line, field by field.
    for (i = 1; i <= NF; i++) {

        # Detect if current field contains RX or TX.
        #RX
        if ($i ~ /^(RX|TX)$/) {
            metric_prefix = tolower($i)
        }

        # Detect if the current field is data that should be stored
        #bytes:3964467449
        #errors:0
        if ($i ~ /[a-z]:[0-9]+$/) {
            split($i, stat_parts, ":")
            name = stat_parts[1]
            value = stat_parts[2]

            if (name == "bytes") {
                name = "bits"
                unit = "bits"
                value = value * 8
            } else {
                unit = "number"
            }

            writeDoubleMetric("network-interface-" metric_prefix "-" name, stat_tags, "gauge", value, "true", "Network Interfaces - " metric_prefix " " name, unit, "name")  # Converted to new syntax by change_ind_scripts.py script
        }
    }
    next
}

#driver: vmxnet3
/^driver:/ {
    interfaces[interface_name, "driver"] = $2

    next
}

#        Speed: 100Mb/s
/^\s+Speed:/ {
    speed = $2
    gsub(/b\/s/, "", speed)

    next

}

#        Duplex: Full
/^\s+Duplex:/ {
    duplex = tolower($2)

    next
}


#Link detected: yes
/Link detected:/ {
    if ($3 == "yes") {
        link_state = 1
    } else {
        link_state = 0
    }
    writeDoubleMetric("network-interface-state", stat_tags, "gauge", link_state, "true", "Network Interfaces - Up/Down", "state", "name")  # Converted to new syntax by change_ind_scripts.py script
    if (link_state == 1) {
        # Speed and duplex is incorrectly set as half/10Mbit when link state is down, so do not write this metric unless link is up.
        writeComplexMetricString("network-interface-duplex", stat_tags, duplex, "true", "Network Interfaces - duplex settings")  # Converted to new syntax by change_ind_scripts.py script
            # Need extra tag for speed alert
            speed_tags["name"] = interface_name
            speed_tags["im.identity-tags"] = "name"
        writeComplexMetricString("network-interface-speed", speed_tags, speed, "true", "Network Interfaces - speed")  # Converted to new syntax by change_ind_scripts.py script
    }

    next
}

#        Auto-negotiation: on
/^\s+Auto-negotiation:/ {
    interfaces[interface_name, "auto-negotiation"] = $2

    next
}

#name:                         LAN4
/^[Nn]ame:/ {
    interface_name_clish = $2

    next
}

#ipv4-address: 1.1.1.1
/^ipv4-address:/ || /^IPv4 Address:/{
    ip_address_clish = $2

    next
}

#status:                       up
#status:                       off
#status:                       disconnected
#status:                       100/full
#status:                       1/full
#Status: Disabled

/^[Ss]tatus:/ {

    name_found = 0

    # Admin state
    if (($2 != "off") && ($2 != "Disabled")) {
        admin_state = 1
    } else {
        admin_state = 0
    }

    # Translate the interface in clish to corresponding in ifconfig
    for (id in interface_arr) {
        if (interface_arr[id] == ip_address_clish) {
            clish_tags["name"] = id
            name_found = 1
        }
    }

    # If nothing found due to no IP on interface, fall back to using the name from clish command
    if (name_found != 1) {
        clish_tags["name"] = interface_name_clish
    }

    # Write data
    writeDoubleMetric("network-interface-admin-state", clish_tags, "gauge", admin_state, "true", "Network Interfaces - Enabled/Disabed", "state", "name")  # Converted to new syntax by change_ind_scripts.py script

    next
}

#description: internal interface1
/^description:/ {
    comment = $0
    split($0, comment_arr, ":")
    if (comment_arr[2] != "") {
        writeComplexMetricString("network-interface-description", clish_tags, trim(comment_arr[2]), "false")  # Converted to new syntax by change_ind_scripts.py script
    }

    next
}

END {

    # There was an issue when fetching the config, see the note above regarding IKP-1221
    # Exiting the script
    if (critical_error) {
        exit
    }

    writeComplexMetricObjectArray("network-interfaces", null, interfaces, "false")  # Converted to new syntax by change_ind_scripts.py script

}