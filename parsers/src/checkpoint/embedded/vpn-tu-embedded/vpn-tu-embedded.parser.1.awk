BEGIN {
}

# Get the peer's IP address
# Peer  197.45.79.20 SAs:
/Peer.*SAs/ {
    peer = $2
    # For each peer we'll look to see that we found at least one SPI for inbound, and one for outbound
    foundinbound = "false"
    foundoutbound = "false"
    section=""
}
/INBOUND/ {
    section="INBOUND"
}
/OUTBOUND/ {
    section="OUTBOUND"
}
/0x[a-z0-9]+/ {
    if (peer != "") {
        if (section == "INBOUND") {
            foundinbound="true"
        }
        if (section == "OUTBOUND") {
            foundoutbound="true"
        }

        # If we found both inbound and outbound, write the metric
        if (foundinbound == "true" && foundoutbound == "true") {
            vpntags["peerip"] = peer
            up = 1.0
            writeDoubleMetric("vpn-tunnel-state", vpntags, "gauge", up, "true", "VPN Tunnels", "state", "peerip")  # Converted to new syntax by change_ind_scripts.py script

            # Reset the peer so we don't get back in here
            peer = ""
        }
    }
}