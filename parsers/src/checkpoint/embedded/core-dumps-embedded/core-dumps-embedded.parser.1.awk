############
# Script explanation: Busybox has other ls parameters, so we have a separate script for embedded.
###########

#-rw-r--r--    1 root     root            0 Sat Nov 19 09:19:56 2016 /var/log/dump/usermode/testcrash.gz
#-rw-r--r--    1 root     root            0 Sat Nov 1 09:19:56 2016 /var/log/dump/usermode/testcrash2.gz
/root/ {

	# Exclude some directories
	if ($11 !~ "/var/crash/bounds|/var/crash/minfree") {

		ifile++

		# starting data
		createYear=$10
		createMonth=parseMonthThreeLetter($7)
		createDay=$8
		createTime=$9

		# time
		gsub(/\.[0-9]+/,"",createTime)
		split(createTime, createTimeArr,":")	
		
		# Create complex object array	
		files[ifile, "path"]=$11
		files[ifile, "created"]=datetime(createYear,createMonth,createDay,createTimeArr[1],createTimeArr[2],createTimeArr[3])
	}
}

END {
	# Write complex metric
	writeComplexMetricObjectArray("core-dumps", null, files, "false")  # Converted to new syntax by change_ind_scripts.py script
}