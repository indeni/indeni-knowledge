# The following two sections has been added by request of Dan Shouky
# https://indeni.atlassian.net/browse/IKP-1221

# Unfortunately, the following code is duplicated in many .ind scripts.
# If you change something in the following two sections, please find all
# of the other instances of this code and make the change there also.

#Could not acquire the config lock
/Could not acquire the config lock/ {
    if (NR == 1) {
        next
    }
}

#CLINFR0829  Unable to get user permissions
#CLINFR0819  User: johndoe denied access via CLI
#CLINFR0599  Failed to build ACLs
/(CLINFR0829\s+Unable to get user permissions|CLINFR0819\s+User: .+ denied access via CLI|CLINFR0599\s+Failed to build ACLs)/ {
    exit
}

#agent: true
#agent: false
/^agent:/ {
	writeComplexMetricString("snmp-enabled", null, $NF, "false")  # Converted to new syntax by change_ind_scripts.py script
}