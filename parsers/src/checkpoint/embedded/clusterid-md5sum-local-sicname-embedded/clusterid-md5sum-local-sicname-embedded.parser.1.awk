############
# Why: Determine if two gateways are in a cluster together, so we know when to compare config between them.
# How: Do a md5sum of the cluster name
###########

# fdd5a3a9cdb08cb2417fdde7517bb99f  -
/[a-f0-9]{32}/ {
	writeTag("cluster-id", $1)
}