BEGIN {
    no_of_cores = 0
}

#  11:23:29 up 8 days, 17:07, load average: 0.00, 0.01, 0.00
/load average/ {
    one_min_average = trim($(NF-2))
    gsub(",", "", one_min_average)
    five_min_average = trim($(NF-1))
    gsub(",", "", five_min_average)
    fifteen_min_average = trim($NF)
    gsub(",", "", fifteen_min_average)
    next    
}

#4
/^[0-9]+/{
    no_of_cores = trim($NF)
}



END {
    writeDoubleMetric("load-average-one-minute-live-config", null, "gauge", one_min_average, "true", "Load Average (1 Minute)", "number", "")
    writeDoubleMetric("load-average-five-minutes-live-config", null, "gauge", five_min_average, "true", "Load Average (5 Minutes)", "number", "")
    writeDoubleMetric("load-average-fifteen-minutes-live-config", null, "gauge", fifteen_min_average, "true", "Load Average (15 Minutes)", "number", "")


    if (no_of_cores > 0) {
        one_min_normalized = one_min_average / no_of_cores
        five_min_normalized = five_min_average / no_of_cores
        fifteen_min_normalized = fifteen_min_average / no_of_cores
    } else {
        one_min_normalized = one_min_average
        five_min_normalized = five_min_average
        fifteen_min_normalized = fifteen_min_average
    }

    writeDoubleMetric("load-average-one-minute", null, "gauge", one_min_normalized, "false")
    writeDoubleMetric("load-average-five-minutes", null, "gauge", five_min_normalized, "false")
    writeDoubleMetric("load-average-fifteen-minutes", null, "gauge", fifteen_min_normalized, "false")
}