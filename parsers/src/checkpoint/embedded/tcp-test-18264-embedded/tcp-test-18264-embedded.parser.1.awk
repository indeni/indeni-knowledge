#Scanning 10.11.94.99 ports 18264 to 18264
# Port	Proto	State	Service
#18264	tcp	open	unknown
#0 closed, 1 open, 0 timed out ports

#Case there is no connectivity
/^Scanning/ {
    ip = $2
    ca_arr[ip] = 0
}

#HPING 10.11.94.32 (eth0 10.11.94.32): S set, 40 headers + 0 data bytes
#len=46 ip=10.11.94.32 ttl=64 DF id=0 sport=18264 flags=SA seq=0 win=29200 rtt=3.1 ms
#len=46 ip=10.11.94.32 ttl=64 DF id=0 sport=18264 flags=SA seq=1 win=29200 rtt=221.8 ms
#len=46 ip=10.11.94.32 ttl=64 DF id=0 sport=18264 flags=SA seq=2 win=29200 rtt=33.3 ms
#
#--- 10.11.94.32 hping statistic ---
#3 packets tramitted, 3 packets received, 0% packet loss
#round-trip min/avg/max = 3.1/86.1/221.8 ms

/closed/ {
    ca_arr[ip] = $3
}



END {
    ca_access = 0
    for (ip in ca_arr) {
        if (ca_arr[ip] == 1){
            ca_access = 1
        }
        ca_tags["name"] = ip
        if (ca_tags_all["name"] == "") {
            ca_tags_all["name"] = ca_tags["name"]
        } else {
            ca_tags_all["name"] = ca_tags_all["name"]":"ca_tags["name"]
        }
        writeDoubleMetric("ca-status", ca_tags, "gauge", ca_arr[ip], "true", "Certificate Authorities Accessible", "state", "name")  # Converted to new syntax by change_ind_scripts.py script
    }
    writeDoubleMetric("ca-accessible", ca_tags_all, "gauge", ca_access, "false", "Certificate Authorities Accessible", "state", "name")
}