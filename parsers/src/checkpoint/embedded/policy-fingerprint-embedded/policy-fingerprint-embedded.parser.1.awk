# localhost Lima-Office  1Feb2017  8:21:56 :  [>WAN ] [<WAN ] [<LAN2] [>LAN1.245] [<LAN1.245] [>LAN1.246] [<LAN1.246]
/^localhost/ {
	policyName=$2
	fingerprint = policyName
}

# d6815e725173138e0dbbdbad672d04b1  -
/[a-f0-9]{32}/ {
	if (fingerprint == "-") {
		fingerprint = ""
	} else {
		fingerprint = fingerprint " " $1
	}

	writeComplexMetricString("policy-installed-fingerprint", null, fingerprint, "false")  # Converted to new syntax by change_ind_scripts.py script
}