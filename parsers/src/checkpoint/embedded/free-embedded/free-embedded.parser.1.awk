#  Mem:       514980       336216       178764            0            0
/Mem:/ {
    total_ram = $2
    free_ram = $4
    used_ram = $3
    ramtag["name"] = "RAM"
    usage_ram_percent = (used_ram / total_ram) * 100
    writeDoubleMetric("memory-free-kbytes", ramtag, "gauge", free_ram, "true", "Memory - Free", "kbytes", "name")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("memory-total-kbytes", ramtag, "gauge", total_ram, "true", "Memory - Total", "kbytes", "name")  # Converted to new syntax by change_ind_scripts.py script

    ramtag_memory_usage["name"] = "RAM"
    ramtag_memory_usage["resource-metric"] = "true"
    writeDoubleMetric("memory-usage", ramtag_memory_usage, "gauge", usage_ram_percent, "true", "Memory - Usage", "percentage", "name")  # Converted to new syntax by change_ind_scripts.py script
}


# Swap:      4225084        108    4224976
/Swap:/ {
    total_swap = $2
    used_swap = $3
    free_swap = $4

    # if swap is 0 then the calculation will fail since the result will be "NaN"
    if (total_swap != 0) {
        swaptag["name"] = "swap"
        usage_swap_percent = (used_swap / total_swap) * 100
        writeDoubleMetric("memory-free-kbytes", swaptag, "gauge", free_swap, "true", "Memory - Free", "kbytes", "name")  # Converted to new syntax by change_ind_scripts.py script
        writeDoubleMetric("memory-total-kbytes", swaptag, "gauge", total_swap, "true", "Memory - Total", "kbytes", "name")  # Converted to new syntax by change_ind_scripts.py script

        swaptag_memory_usage["name"] = "swap"
        swaptag_memory_usage["resource-metric"] = "true"
        writeDoubleMetric("memory-usage", swaptag_memory_usage, "gauge", usage_swap_percent, "true", "Memory - Usage", "percentage", "name")  # Converted to new syntax by change_ind_scripts.py script
    }
}