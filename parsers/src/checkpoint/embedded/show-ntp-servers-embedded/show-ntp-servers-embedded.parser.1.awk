# The following two sections has been added by request of Dan Shouky
# https://indeni.atlassian.net/browse/IKP-1221

# Unfortunately, the following code is duplicated in many .ind scripts.
# If you change something in the following two sections, please find all
# of the other instances of this code and make the change there also.

#Could not acquire the config lock
/Could not acquire the config lock/ {
    if (NR == 1) {
        next
    }
}

#CLINFR0829  Unable to get user permissions
#CLINFR0819  User: johndoe denied access via CLI
#CLINFR0599  Failed to build ACLs
/(CLINFR0829\s+Unable to get user permissions|CLINFR0819\s+User: .+ denied access via CLI|CLINFR0599\s+Failed to build ACLs)/ {
    criticalError = 1
    exit
}

# primary:                      192.168.245.199
# secondary:                    192.168.10.199
/(?:[0-9]{1,3}\.){3}[0-9]{1,3}/ {
	intp++
	ntps[intp, "ipaddress"]=$2
	gsub(/:/,"",$1)
	ntps[intp, "type"]=$1
}

END {

    # There was an issue when fetching the config, see the note above regarding IKP-1221
    # Exiting the script
    if (criticalError){
        exit
    }

	if (intp > 0) {
		writeComplexMetricObjectArray("ntp-servers", null, ntps, "true", "NTP Servers")  # Converted to new syntax by change_ind_scripts.py script
	}
}