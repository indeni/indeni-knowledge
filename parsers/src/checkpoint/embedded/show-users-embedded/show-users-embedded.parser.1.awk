############
# Script explanation: clish "show users admin" truncates usernames longer than 6 characters. Using /etc/passwd instead.
############

# indeni-new:x:0:0:Linux User,,,:/:/bin/bash
/Linux User/ {
    split($0,userArr,":")
	iuser++
    users[iuser, "username"]=userArr[1]
}

END {
	writeComplexMetricObjectArray("users", null, users, "false")  # Converted to new syntax by change_ind_scripts.py script
}