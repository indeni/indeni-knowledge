# The following two sections has been added by request of Dan Shouky
# https://indeni.atlassian.net/browse/IKP-1221

#Could not acquire the config lock
/Could not acquire the config lock/ {
    if (NR == 1) {
        next
    }
}

#CLINFR0829  Unable to get user permissions
#CLINFR0819  User: johndoe denied access via CLI
#CLINFR0599  Failed to build ACLs
/(CLINFR0829\s+Unable to get user permissions|CLINFR0819\s+User: .+ denied access via CLI|CLINFR0599\s+Failed to build ACLs)/ {
    exit
}

# domainname: test.local
/domainname:/ {
    if ( NF >= 2 && $2 != "" ) {
	writeComplexMetricString("domain", null, $2, "true", "Domain")  # Converted to new syntax by change_ind_scripts.py script
    }
}