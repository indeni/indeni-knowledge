from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class ChkpFwFastAccelStatusVsx(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        data_parsed = helper_methods.parse_data_as_list(raw_data, 'fw_ctl_fast_accel_status_vsx.textfsm')
        if data_parsed:
            for vs_data in data_parsed:
                self.write_double_metric('fast-accel-status', {'vs.id': vs_data['vs_id'], 'vs.name': vs_data['vs_name']}, 'gauge',
                                                    1 if 'enabled' in vs_data['fw_fast_accel_status']  else 0, True, 'Fast Acceleration Status', 'state', 'vs.id|vs.name')
        return self.output
