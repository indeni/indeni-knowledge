import os
import unittest

from checkpoint.firewall.mgmt_server_ip_address_vsx.mgmt_server_ip_address_vsx import ChkpMgmtServerIpAddressVsx


class TestChkpMgmtServerIpAddressVsx(unittest.TestCase):

    def setUp(self):
        self.parser = ChkpMgmtServerIpAddressVsx()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_3VS_2mgmts(self):
        result = self.parser.parse_file(self.current_dir + '/3VS_2mgmts.input', {}, {})
        self.assertEqual(3, len(result))
        self.assertEqual(result[0].tags['vs.id'], '0')
        self.assertEqual(result[0].tags['vs.name'], 'VSX-1')
        self.assertEqual(result[0].value[0], {'id': "1", 'mgmt_server': '10.11.94.138'})
        self.assertEqual(result[1].tags['vs.id'], '1')
        self.assertEqual(result[1].tags['vs.name'], 'VS-1-R81')
        self.assertEqual(result[1].value[0], {'id': "1", 'mgmt_server': '10.11.94.139'})
        self.assertEqual(result[2].tags['vs.id'], '2')
        self.assertEqual(result[2].tags['vs.name'], 'VS-2-R81')
        self.assertEqual(result[2].value[0], {'id': "1", 'mgmt_server': '10.11.94.139'})
if __name__ == '__main__':
    unittest.main()