# localhost InitialPolicy 12Feb2017 19:13:38 :  [>eth0] [<eth0]
/^localhost/ {
	policyName=$2
	fingerprint = policyName
}

# 16f7b38c2a9e2f96a6faf3000f2050ff  /opt/CPsuite-R77/fw1/state/local/FW1/local.str
#/[a-f0-9]{32}/ {
/local\.str/ {
	if (fingerprint == "-") {
		fingerprint = ""
	} else if ($1 ~ /[a-f0-9]{32}/) {
		fingerprint = fingerprint " " $1
	}

	writeComplexMetricString("policy-installed-fingerprint", null, fingerprint, "false")  # Converted to new syntax by change_ind_scripts.py script
}