from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class FWCtlGetintDynamicDispatcherFwd(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            parsed_data = helper_methods.parse_data_as_list(raw_data, 'checkpoint_gaia_fw_ctl_get_int.textfsm')
            if parsed_data:
                for parameter in parsed_data:
                    tags = {}
                    tags['name'] = parameter['name']
                    self.write_double_metric('firewall-kparam-dynamic-dispatcher', tags, "gauge", int(parameter['value']), 'true', 'Kernel Parameters (Dynamic Dipatcher)', 'number', 'name')
        return self.output


