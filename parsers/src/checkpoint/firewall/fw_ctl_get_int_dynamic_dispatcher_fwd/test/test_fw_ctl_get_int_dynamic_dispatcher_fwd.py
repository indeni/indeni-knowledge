import os
import unittest

from checkpoint.firewall.fw_ctl_get_int_dynamic_dispatcher_fwd.fw_ctl_get_int_dynamic_dispatcher_fwd import FWCtlGetintDynamicDispatcherFwd
from parser_service.public.action import *


class TestFWCtlGetintDynamicDispatcherFwd(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = FWCtlGetintDynamicDispatcherFwd()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_kernel_parameters_one_failing(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/kernel_parameters_one_failing.input', {}, {})
        # Assert
        self.assertEqual(2, len(result))
        self.assertEqual('firewall-kparam-dynamic-dispatcher', result[0].name)
        self.assertEqual('fwmultik_enable_round_robin', result[0].tags['name'])
        self.assertEqual(0, result[0].value)
        self.assertEqual('firewall-kparam-dynamic-dispatcher', result[1].name)
        self.assertEqual('fwmultik_enable_increment_first', result[1].tags['name'])
        self.assertEqual(1, result[1].value)

    def test_kernel_parameters_set_to_0(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/kernel_parameters_set_to_0.input', {}, {})
        # Assert
        self.assertEqual(2, len(result))
        self.assertEqual('firewall-kparam-dynamic-dispatcher', result[0].name)
        self.assertEqual('fwmultik_enable_round_robin', result[0].tags['name'])
        self.assertEqual(0, result[0].value)
        self.assertEqual('firewall-kparam-dynamic-dispatcher', result[1].name)
        self.assertEqual('fwmultik_enable_increment_first', result[1].tags['name'])
        self.assertEqual(0, result[1].value)

    def test_kernel_parameters_set_to_1(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/kernel_parameters_set_to_1.input', {}, {})
        # Assert
        self.assertEqual(2, len(result))
        self.assertEqual('firewall-kparam-dynamic-dispatcher', result[0].name)
        self.assertEqual('fwmultik_enable_round_robin', result[0].tags['name'])
        self.assertEqual(1, result[0].value)
        self.assertEqual('firewall-kparam-dynamic-dispatcher', result[1].name)
        self.assertEqual('fwmultik_enable_increment_first', result[1].tags['name'])
        self.assertEqual(1, result[1].value)

if __name__ == '__main__':
    unittest.main()
