function addVsTags(tags) {
	tags["vs.id"] = vsid
    tags["vs.name"] = vsname
    tags["im.identity-tags"] = "vs.id|vs.name"
}

function dumpCoreData () {

	# Add tags with vsname and vsid
	addVsTags(t)
	
	if ( coreXL == 1 ) {
		writeComplexMetricString("corexl-cores-enabled", t, icpu, "true", "CoreXL - Cores Enabled")  # Converted to new syntax by change_ind_scripts.py script
	}
	
	# clear variables
	coreXL = ""
	icpu = ""
}

BEGIN {
	#FS="|"
	icpu=0
	vsid=""
	vsname=""
}


# VSID:            0
/VSID:/ {
    # Dump data of previous VS if needed
    if (vsid != "") {
		dumpCoreData()
    }
	vsid = trim($NF)
}

# Name:            VSX-CXL2-Gear
/Name:/ {
	vsname = trim($NF)
}


#  0 | Yes     | 11     |        2722 |    15702
/Yes/ {
	addVsTags(coreXLTags)

	connections = trim($7)
	coreXLTags["cpu-id"] = $1
	writeDoubleMetric("corexl-cpu-connections", coreXLTags, "gauge", trim(connections), "false")  # Converted to new syntax by change_ind_scripts.py script
	icpu++
	coreXL=1
}