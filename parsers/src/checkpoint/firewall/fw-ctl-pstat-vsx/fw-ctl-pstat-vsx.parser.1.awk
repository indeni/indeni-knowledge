# SSH Command Notes
# See: https://indeni.atlassian.net/browse/IKP-2110
# We've had problems with scripts (and SSH commands) taking a very long time to run on large VSX deployments. This
# command includes some performance optimizations; e.g., it uses xargs to run the subsequent Check Point CLI commands
# in parallel bash processes. Here's the basic outline:
# * Capture the VSID and Name and pass each one to xargs.
# * xargs 'calls' bash, spawning it in up to -P processes.
# * Initialize the shell with /etc/profile.d/vsenv.sh. Required to run vsenv command.
# * Split out the VS ID and name from idAndName -- using idAndName like this avoids having to run 'vsx stat' for each VS.
# * Set the vsenv and run the actual Check Point command. Capture the output into 'result' and echo it.
# Specific notes:
# -n max args; i.e., deal with one VS at a time
# -P max number of parallel processes
# -I{} -- in the following command to xargs, for each line of input from stdin, replace {} with that input. In this case, run bash passing each line from stdin as the variable 'idAndName'.
# "" -> get value of var as string
# '' -> print/create this exact string
# `` -> execute this string


function addVsTags(tags) {
    tags["vs.id"] = vsid
    tags["vs.name"] = vsname
}

BEGIN {
    vsid = ""
    vsname = ""
}

#_VSID:Name 3:lab-CP-VSX1_vsx-2

/^_VSID:Name/ {
    split($2, id_and_name, ":")
    vsid = id_and_name[1]
    vsname = id_and_name[2]
    next
}

# Kernel   memory used:   9% (69 MB out of 703 MB) - below watermark

/Kernel\s+memory\s+used.*out of.*/ {
    usage_ref = $4
    usage = substr(usage_ref, 1, length(usage_ref)-1)
    addVsTags(vstags)
    writeDoubleMetric("kernel-memory-usage", vstags, "gauge", usage, "true", "Kernel Memory", "percentage", "vs.id|vs.name")  # Converted to new syntax by change_ind_scripts.py script
}

# Virtual   memory used:   9% (69 MB out of 703 MB) - below watermark

/Virtual\s+memory\s+used.*out of.*/ {
    usage_ref = $4
    usage = substr(usage_ref, 1, length(usage_ref)-1)
    addVsTags(vstags)
    writeDoubleMetric("virtual-memory-usage", vstags, "gauge", usage, "true", "Virtual Memory", "percentage", "vs.id|vs.name")  # Converted to new syntax by change_ind_scripts.py script
}

# Aggressive Aging is disabled
# Aggressive Aging is enabled, not active
# Aggressive Aging is not active
# the following should trigger the issue
# Aggressive Aging is active
# Aggressive Aging is enabled, active
# Aggressive Aging is enabled, in detect mode
/Aggressive Aging/ {
    chkp_agressive_aging = 0
    message = trim($0)
    if ((message ~ "is active") || (message ~ "is enabled, active") || ( message ~ "in detect mode")) {
        chkp_agressive_aging = 1
    }
    writeDoubleMetric("chkp-agressive-aging", vstags, "gauge", chkp_agressive_aging, "false")  # Converted to new syntax by change_ind_scripts.py script
}