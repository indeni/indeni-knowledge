import os
import unittest

from checkpoint.firewall.cpstat_identity_server_disconnected.cpstat_identity_server_disconnected import CpstatIdentityServerDisconnected
from parser_service.public.action import *


class TestCpstatIdentityServerDisconnected(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = CpstatIdentityServerDisconnected()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_cpstat_identity_server_disconnected_0_events_vsx(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/cpstat_identity_server_disconnected_0_events_vsx.input', {}, {'vsx': 'true'})
        # print(result)
        # Assert
        self.assertEqual(14, len(result))

        self.assertEqual(result[0].name, 'chkp_cpstat_identity_server_disconnected')
        self.assertEqual(result[0].value, 1)
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].tags, {'name': 'vs.id - 0 vs.name - CP-R80.20-VSX1-1', 'servername': 'server1', 'display-name': 'Identity Server'})

        self.assertEqual(result[1].name, 'chkp_cpstat_identity_server_events_received_zero')
        self.assertEqual(result[1].value, 0)
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].tags, {'name': 'vs.id - 0 vs.name - CP-R80.20-VSX1-1', 'servername': 'server1', 'display-name': 'Identity Server'})

        self.assertEqual(result[2].name, 'chkp_cpstat_identity_server_disconnected')
        self.assertEqual(result[2].value, 1)
        self.assertEqual(result[2].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[2].tags, {'name': 'vs.id - 0 vs.name - CP-R80.20-VSX1-1', 'servername': 'server2', 'display-name': 'Identity Server'})

        self.assertEqual(result[3].name, 'chkp_cpstat_identity_server_events_received_zero')
        self.assertEqual(result[3].value, 1)
        self.assertEqual(result[3].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[3].tags, {'name': 'vs.id - 0 vs.name - CP-R80.20-VSX1-1', 'servername': 'server2', 'display-name': 'Identity Server'})

        self.assertEqual(result[4].name, 'chkp_cpstat_identity_server_disconnected')
        self.assertEqual(result[4].value, 0)
        self.assertEqual(result[4].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[4].tags, {'name': 'vs.id - 0 vs.name - CP-R80.20-VSX1-1', 'servername': 'server3', 'display-name': 'Identity Server'})

        self.assertEqual(result[5].name, 'chkp_cpstat_identity_server_events_received_zero')
        self.assertEqual(result[5].value, 1)
        self.assertEqual(result[5].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[5].tags, {'name': 'vs.id - 0 vs.name - CP-R80.20-VSX1-1', 'servername': 'server3', 'display-name': 'Identity Server'})

        self.assertEqual(result[6].name, 'chkp_cpstat_identity_server_disconnected')
        self.assertEqual(result[6].value, 0)
        self.assertEqual(result[6].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[6].tags, {'name': 'vs.id - 0 vs.name - CP-R80.20-VSX1-1', 'servername': 'server4', 'display-name': 'Identity Server'})

        self.assertEqual(result[7].name, 'chkp_cpstat_identity_server_events_received_zero')
        self.assertEqual(result[7].value, 0)
        self.assertEqual(result[7].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[7].tags, {'name': 'vs.id - 0 vs.name - CP-R80.20-VSX1-1', 'servername': 'server4', 'display-name': 'Identity Server'})

        self.assertEqual(result[8].name, 'chkp_cpstat_identity_server_disconnected')
        self.assertEqual(result[8].value, 1)
        self.assertEqual(result[8].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[8].tags, {'name': 'vs.id - 0 vs.name - CP-R80.20-VSX1-2', 'servername': 'server1', 'display-name': 'Identity Server'})

        self.assertEqual(result[9].name, 'chkp_cpstat_identity_server_events_received_zero')
        self.assertEqual(result[9].value, 0)
        self.assertEqual(result[9].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[9].tags, {'name': 'vs.id - 0 vs.name - CP-R80.20-VSX1-2', 'servername': 'server1', 'display-name': 'Identity Server'})

        self.assertEqual(result[10].name, 'chkp_cpstat_identity_server_disconnected')
        self.assertEqual(result[10].value, 1)
        self.assertEqual(result[10].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[10].tags, {'name': 'vs.id - 0 vs.name - CP-R80.20-VSX1-2', 'servername': 'server2', 'display-name': 'Identity Server'})

        self.assertEqual(result[11].name, 'chkp_cpstat_identity_server_events_received_zero')
        self.assertEqual(result[11].value, 1)
        self.assertEqual(result[11].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[11].tags, {'name': 'vs.id - 0 vs.name - CP-R80.20-VSX1-2', 'servername': 'server2', 'display-name': 'Identity Server'})

        self.assertEqual(result[12].name, 'chkp_cpstat_identity_server_disconnected')
        self.assertEqual(result[12].value, 0)
        self.assertEqual(result[12].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[12].tags, {'name': 'vs.id - 0 vs.name - CP-R80.20-VSX1-2', 'servername': 'server3', 'display-name': 'Identity Server'})

        self.assertEqual(result[13].name, 'chkp_cpstat_identity_server_events_received_zero')
        self.assertEqual(result[13].value, 1)
        self.assertEqual(result[13].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[13].tags, {'name': 'vs.id - 0 vs.name - CP-R80.20-VSX1-2', 'servername': 'server3', 'display-name': 'Identity Server'})


if __name__ == '__main__':
    unittest.main()
