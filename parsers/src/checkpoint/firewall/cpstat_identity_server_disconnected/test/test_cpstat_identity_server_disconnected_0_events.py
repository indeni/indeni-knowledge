import os
import unittest

from checkpoint.firewall.cpstat_identity_server_disconnected.cpstat_identity_server_disconnected import CpstatIdentityServerDisconnected
from parser_service.public.action import *


class TestCpstatIdentityServerDisconnected(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = CpstatIdentityServerDisconnected()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_chkp_cpstat_identity_server_disconnected_0_events_vsx(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/cpstat_identity_server_disconnected_0_events.input', {}, {})
        # print(result)
        # Assert
        self.assertEqual(8, len(result))

        self.assertEqual(result[0].name, 'chkp_cpstat_identity_server_disconnected')
        self.assertEqual(result[0].value, 1)
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].tags, {'display-name': 'Identity Server', 'servername': 'server1'})

        self.assertEqual(result[1].name, 'chkp_cpstat_identity_server_events_received_zero')
        self.assertEqual(result[1].value, 0)
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].tags, {'display-name': 'Identity Server', 'servername': 'server1'})

        self.assertEqual(result[2].name, 'chkp_cpstat_identity_server_disconnected')
        self.assertEqual(result[2].value, 1)
        self.assertEqual(result[2].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[2].tags, {'display-name': 'Identity Server', 'servername': 'server2'})

        self.assertEqual(result[3].name, 'chkp_cpstat_identity_server_events_received_zero')
        self.assertEqual(result[3].value, 1)
        self.assertEqual(result[3].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[3].tags, {'display-name': 'Identity Server', 'servername': 'server2'})

        self.assertEqual(result[4].name, 'chkp_cpstat_identity_server_disconnected')
        self.assertEqual(result[4].value, 0)
        self.assertEqual(result[4].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[4].tags, {'display-name': 'Identity Server', 'servername': 'server3'})

        self.assertEqual(result[5].name, 'chkp_cpstat_identity_server_events_received_zero')
        self.assertEqual(result[5].value, 1)
        self.assertEqual(result[5].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[5].tags, {'display-name': 'Identity Server', 'servername': 'server3'})

        self.assertEqual(result[6].name, 'chkp_cpstat_identity_server_disconnected')
        self.assertEqual(result[6].value, 0)
        self.assertEqual(result[6].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[6].tags, {'display-name': 'Identity Server', 'servername': 'server4'})

        self.assertEqual(result[7].name, 'chkp_cpstat_identity_server_events_received_zero')
        self.assertEqual(result[7].value, 0)
        self.assertEqual(result[7].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[7].tags, {'display-name': 'Identity Server', 'servername': 'server4'})


if __name__ == '__main__':
    unittest.main()
