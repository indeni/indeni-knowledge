#!/bin/bash
  
source /etc/profile.d/vsenv.sh
vsx=$( (vsx stat) 2>&1)
if [[ "$vsx" = *"VSX is not supported"* ]]; then ${nice-path} -n 15 cpstat identityServer -f idc ; else for id in $(fw vsx stat -l | awk '/^VSID:/{ vsid=$NF }; /^Type:/{vstype=$NF};/^Name:/{ if (vstype != "Switch" || vstype != "Router" ) { print vsid ":" $NF ":" vstype } } '); do vsid=$(echo "$id" | cut -d ":" -f 1); name=$(echo "$id" | cut -d ":" -f 2); type=$(echo "$id" | cut -d ":" -f 3); echo "_VSID:Name:Type $vsid:$name:$type"; vsenv "$vsid" ; ${nice-path} -n 15 cpstat identityServer -f idc ; done ; vsenv 0 ; fi