from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class CpstatIdentityServerDisconnected(BaseParser):
    def write_metric(self, identityservers_data: list, tags: dict):
        if identityservers_data:
            for identityserver in identityservers_data:
                for identityserver_item in identityserver:
                    if identityserver_item == 'name':
                        metric_tags = {}
                        metric_tags = tags.copy()
                        metric_tags.update({'servername': identityserver[identityserver_item]})
                        metric_value_disconnected = 1
                        metric_value_received_zero = 1

                    if identityserver_item == 'status':
                        if identityserver[identityserver_item] == 'Disconnected':
                            metric_value_disconnected = 0
                        self.write_double_metric('chkp_cpstat_identity_server_disconnected', metric_tags, 'gauge', metric_value_disconnected, False, 'Identity Server')

                    if identityserver_item == 'events_received':
                        if identityserver[identityserver_item] == '0':
                            metric_value_received_zero = 0
                        self.write_double_metric('chkp_cpstat_identity_server_events_received_zero', metric_tags, 'gauge', metric_value_received_zero, False, 'Identity Server')
        return

    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        # Step 1 : Data Extraction
        if raw_data and len(raw_data) > 1:
            # Checking if the device is VSX, to determine best code:
            if device_tags.get('vsx') == 'true':
                basic = raw_data.split('_VSID:Name:Type ')

                for vs_data in basic:
                    identityservers_data = helper_methods.parse_data_as_list(vs_data, 'cpstat_identity_server_disconnected.textfsm')
                    vs_metadata = helper_methods.parse_data_as_object(vs_data, 'vs_metadata_parse.textfsm')
                    if vs_metadata and identityservers_data:
                        if vs_metadata['vs_type'] != 'Switch' and vs_metadata['vs_type'] != 'Router':
                            tags = {'name': 'vs.id - ' + vs_metadata['vs_id'] + ' vs.name - ' + vs_metadata['name']}
                            self.write_metric(identityservers_data, tags)
            # If the device is not VSX:
            else:
                identityservers_data = helper_methods.parse_data_as_list(raw_data, 'cpstat_identity_server_disconnected.textfsm')
                self.write_metric(identityservers_data, {})
        return self.output
