from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class CheckpointInterrogation(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            cpstat_data = helper_methods.parse_data_as_list(raw_data, 'checkpoint_cpstat_os.textfsm')
            # Sometimes the order can be different, and textfsm might produce 2 dicts, the following code merge it to one:
            if cpstat_data:
                # Step 1 : Data Extraction
                if len(cpstat_data) > 1:
                    cpstat_data = {k: v for d in cpstat_data for k, v in d.items() if v != ''}
                else:
                    cpstat_data = cpstat_data[0]
                chassis_data = helper_methods.parse_data_as_object(raw_data, 'checkpoint_asg_stat.textfsm')
                IPSO_model = helper_methods.parse_data_as_object(raw_data, 'checkpoint_ipso_ipsctl.textfsm')
                release = raw_data.splitlines()[0]
                model = ''

                # Step 2: Data Manipulation and reporting:
                self.write_tag('vendor', 'checkpoint')
                # Models:
                if cpstat_data and cpstat_data != '':
                    # Adding maestro tags:
                    if 'Orchestrator' in cpstat_data['model']:
                        self.write_tag('maestro-orchestrator', 'true')
                        self.write_tag('asg', 'true')
                    elif 'Maestro' in cpstat_data['model'] and 'Orchestrator' not in cpstat_data['model']:
                        self.write_tag('maestro-gw', 'true')
                        self.write_tag('asg', 'true')
                    # Getting all Models:
                    if cpstat_data['model'] == '' and chassis_data and chassis_data['SP_type'] != '':
                        self.write_tag('model', chassis_data['SP_type'])
                    elif cpstat_data['model'] == '' and cpstat_data['os_name'] == 'IPSO':
                        self.write_tag('model', IPSO_model['IPSO_model'])
                    else:
                        self.write_tag('model', cpstat_data['model'])
                # OS name:
                    if 'Embedded' in cpstat_data['os_name']:
                        self.write_tag('os.name', 'gaia-embedded')
                    elif 'Gaia' in cpstat_data['os_name'] and 'Embedded' not in cpstat_data['os_name']:
                        self.write_tag('os.name', 'gaia')
                    elif 'SecurePlatform' in cpstat_data['os_name']:
                        self.write_tag('os.name', 'secureplatform')
                    # OS name if cpstat os does not contain the version data:
                    elif release:
                        if 'Embedded' in release:
                            self.write_tag('os.name', 'gaia-embedded')
                        elif 'Gaia' in release and 'Embedded' not in release:
                            self.write_tag('os.name', 'gaia')
                        elif 'SecurePlatform' in release:
                            self.write_tag('os.name', 'secureplatform')
                #IPSO OS data:
                    if cpstat_data['os_name'] == 'IPSO':
                        self.write_tag('os.name', cpstat_data['os_name'])
                        self.write_tag('ipso.os.version', cpstat_data['ipso_version'])
                # OS version and chassis tag for chassis versions:
                    if chassis_data and chassis_data != '':
                        if chassis_data['SP_type'] != 'Maestro':
                            self.write_tag('chassis', 'true')
                            self.write_tag('asg', 'true')
                        self.write_tag('os.version', chassis_data['SP_version'])
                        self.write_tag('os.version.num', str(chassis_data['SP_version']).replace('R', '').replace('SP', ''))
                    else:
                        self.write_tag('os.version', cpstat_data['version'])
                        self.write_tag('os.version.num', str(cpstat_data['version']).replace('R', ''))
        return self.output
