import os
import unittest
from checkpoint.firewall.checkpoint_interrogation.checkpoint_interrogation import CheckpointInterrogation
from parser_service.public.action import *


class TestCheckpointInterrogation(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = CheckpointInterrogation()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_valid_R80_20_SP_chassis(self):
        # Act 
        result = self.parser.parse_file(self.current_dir + '/test_valid_R80_20_SP_chassis.txt', {}, {})

        # Assert
        self.assertEqual(7, len(result))

        self.assertTrue(isinstance(result[0], WriteTag))
        self.assertEqual('vendor', result[0].key)
        self.assertEqual('checkpoint', result[0].value)

        self.assertEqual('model', result[1].key)
        self.assertEqual('VMware Virtual Platform', result[1].value)

        self.assertEqual('chassis', result[3].key)
        self.assertEqual('true', result[3].value)

        self.assertEqual('os.version', result[5].key)
        self.assertEqual('R80.20SP', result[5].value)

        self.assertEqual('os.version.num', result[6].key)
        self.assertEqual('80.20', result[6].value)


    def test_valid_R80_40_FW(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/test_valid_R80_40_FW.txt', {}, {})

        # Assert
        self.assertEqual(5, len(result))

        self.assertTrue(isinstance(result[0], WriteTag))
        self.assertEqual('vendor', result[0].key)
        self.assertEqual('checkpoint', result[0].value)

        self.assertEqual('model', result[1].key)
        self.assertEqual('VMware Virtual Platform', result[1].value)

        self.assertEqual('os.version', result[3].key)
        self.assertEqual('R80.40', result[3].value)

        self.assertEqual('os.version.num', result[4].key)
        self.assertEqual('80.40', result[4].value)


    def test_test_valid_R77_20_80_Embedded_CP1100(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/test_valid_R77_20_80_Embedded_CP1100.txt', {}, {})

        # Assert
        self.assertEqual(5, len(result))

        self.assertTrue(isinstance(result[0], WriteTag))
        self.assertEqual('vendor', result[0].key)
        self.assertEqual('checkpoint', result[0].value)

        self.assertEqual('model', result[1].key)
        self.assertEqual('CP 1100', result[1].value)

        self.assertEqual('os.name', result[2].key)
        self.assertEqual('gaia-embedded', result[2].value)


    def test_valid_R76_Chassis(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/test_valid_R76_Chassis.txt', {}, {})

        # Assert
        self.assertEqual(7, len(result))

        self.assertTrue(isinstance(result[0], WriteTag))
        self.assertEqual('vendor', result[0].key)
        self.assertEqual('checkpoint', result[0].value)

        self.assertEqual('model', result[1].key)
        self.assertEqual('61000', result[1].value)

        self.assertEqual('os.name', result[2].key)
        self.assertEqual('gaia', result[2].value)

        self.assertEqual('chassis', result[3].key)
        self.assertEqual('true', result[3].value)

        self.assertEqual('os.version', result[5].key)
        self.assertEqual('R76SP.30', result[5].value)

        self.assertEqual('os.version.num', result[6].key)
        self.assertEqual('76.30', result[6].value)

    def test_valid_R77_30_GW(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/test_valid_R77_30_GW.txt', {}, {})

        # Assert
        self.assertEqual(5, len(result))

        self.assertTrue(isinstance(result[0], WriteTag))
        self.assertEqual('vendor', result[0].key)
        self.assertEqual('checkpoint', result[0].value)

        self.assertEqual('model', result[1].key)
        self.assertEqual('Check Point 2200', result[1].value)

        self.assertEqual('os.name', result[2].key)
        self.assertEqual('gaia', result[2].value)

        self.assertEqual('os.version', result[3].key)
        self.assertEqual('R77.30', result[3].value)


    def test_valid_R80_20SP_MHO_170(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/test_valid_R80_20SP_MHO_170.txt', {}, {})

        # Assert
        self.assertEqual(7, len(result))

        self.assertTrue(isinstance(result[0], WriteTag))
        self.assertEqual('vendor', result[0].key)
        self.assertEqual('checkpoint', result[0].value)

        self.assertEqual('model', result[3].key)
        self.assertEqual('Maestro Orchestrator 170', result[3].value)

        self.assertEqual('maestro-orchestrator', result[1].key)
        self.assertEqual('true', result[1].value)

        self.assertEqual('asg', result[2].key)
        self.assertEqual('true', result[2].value)

        self.assertEqual('os.version', result[5].key)
        self.assertEqual('R80.20', result[5].value)

        self.assertEqual('os.version.num', result[6].key)
        self.assertEqual('80.20', result[6].value)

    def test_valid_R80_20SP_SG_HA_LS(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/test_valid_R80_20SP_SG_HA_LS.txt', {}, {})

        # Assert
        self.assertEqual(7, len(result))

        self.assertTrue(isinstance(result[0], WriteTag))
        self.assertEqual('vendor', result[0].key)
        self.assertEqual('checkpoint', result[0].value)

        self.assertEqual('model', result[3].key)
        self.assertEqual('Check Point Maestro', result[3].value)

        self.assertEqual('maestro-gw', result[1].key)
        self.assertEqual('true', result[1].value)

        self.assertEqual('asg', result[2].key)
        self.assertEqual('true', result[2].value)

        self.assertEqual('os.version', result[5].key)
        self.assertEqual('R80.20SP', result[5].value)

        self.assertEqual('os.version.num', result[6].key)
        self.assertEqual('80.20', result[6].value)

    def test_valid_R80_20SP_SG_VS_LS(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/test_valid_R80_20SP_SG_VS_LS.txt', {}, {})

        # Assert
        self.assertEqual(7, len(result))

        self.assertTrue(isinstance(result[0], WriteTag))
        self.assertEqual('vendor', result[0].key)
        self.assertEqual('checkpoint', result[0].value)

        self.assertEqual('model', result[3].key)
        self.assertEqual('Check Point Maestro', result[3].value)

        self.assertEqual('maestro-gw', result[1].key)
        self.assertEqual('true', result[1].value)

        self.assertEqual('asg', result[2].key)
        self.assertEqual('true', result[2].value)

        self.assertEqual('os.version', result[5].key)
        self.assertEqual('R80.20SP', result[5].value)

        self.assertEqual('os.version.num', result[6].key)
        self.assertEqual('80.20', result[6].value)

    def test_valid_IPSO(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/test_valid_IPSO.input', {}, {})

        # Assert
        self.assertEqual(6, len(result))

        self.assertTrue(isinstance(result[0], WriteTag))
        self.assertEqual('vendor', result[0].key)
        self.assertEqual('checkpoint', result[0].value)

        self.assertEqual('model', result[1].key)
        self.assertEqual('IP390', result[1].value)

        self.assertEqual('os.name', result[2].key)
        self.assertEqual('IPSO', result[2].value)

        self.assertEqual('ipso.os.version', result[3].key)
        self.assertEqual('6.2-GA055b06', result[3].value)

        self.assertEqual('os.version', result[4].key)
        self.assertEqual('R75.47', result[4].value)

        self.assertEqual('os.version.num', result[5].key)
        self.assertEqual('75.47', result[5].value)

    def test_valid_R80_20_embedded(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/test_valid_R80_20_embedded.input', {}, {})

        # Assert
        self.assertEqual(5, len(result))

        self.assertTrue(isinstance(result[0], WriteTag))
        self.assertEqual('vendor', result[0].key)
        self.assertEqual('checkpoint', result[0].value)

        self.assertEqual('model', result[1].key)
        self.assertEqual('CP 1590', result[1].value)

        self.assertEqual('os.name', result[2].key)
        self.assertEqual('gaia-embedded', result[2].value)

        self.assertEqual('os.version', result[3].key)
        self.assertEqual('R80.20', result[3].value)

        self.assertEqual('os.version.num', result[4].key)
        self.assertEqual('80.20', result[4].value)


    def test_valid_IPSO_R76(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/test_valid_IPSO_R76.input', {}, {})

        # Assert
        self.assertEqual(6, len(result))

        self.assertTrue(isinstance(result[0], WriteTag))
        self.assertEqual('vendor', result[0].key)
        self.assertEqual('checkpoint', result[0].value)

        self.assertEqual('model', result[1].key)
        self.assertEqual('IP390', result[1].value)

        self.assertEqual('os.name', result[2].key)
        self.assertEqual('IPSO', result[2].value)

        self.assertEqual('ipso.os.version', result[3].key)
        self.assertEqual('6.2-GA083a02_MR4A207', result[3].value)

        self.assertEqual('os.version', result[4].key)
        self.assertEqual('R76', result[4].value)

        self.assertEqual('IP390', result[1].value)

        self.assertEqual('os.name', result[2].key)
        self.assertEqual('IPSO', result[2].value)

    def test_invalid_data(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/test_invalid_data.input', {}, {})

        # Assert
        self.assertEqual(0, len(result))


if __name__ == '__main__':
    unittest.main()