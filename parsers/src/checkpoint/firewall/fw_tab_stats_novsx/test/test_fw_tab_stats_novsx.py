import os
import unittest

from checkpoint.firewall.fw_tab_stats_novsx.fw_tab_stats_novsx import FwTabStatsNoVsx
from parser_service.public.action import WriteComplexMetric


class TestFwTabStatsNoVsx(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = FwTabStatsNoVsx()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_two_coreXL_instances(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/two_coreXL_instances.input', {}, {})

        # Assert
        self.assertEqual(34, len(result))

        for single_result in result:
            if single_result.tags['name'] == 'inbound_traffic_marker' and single_result.tags['corexl-id'] == '0':
                if single_result.name == 'kernel-table-actual':
                    self.assertEqual(0, single_result.value)
                else:
                    self.assertEqual(25000, single_result.value)
            elif single_result.tags['name'] == 'dpd_initiator_sn' and single_result.tags['corexl-id'] == '1':
                if single_result.name == 'kernel-table-actual':
                    self.assertEqual(0, single_result.value)
                else:
                    self.assertEqual(25000, single_result.value)
            elif single_result.tags['name'] == 'host_ip_addrs' and single_result.tags['corexl-id'] == '0':
                if single_result.name == 'kernel-table-actual':
                    self.assertEqual(7, single_result.value)
                else:
                     self.assertEqual(25000, single_result.value)

    def test_no_nat_table_limit(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/no_nat_table_limit.input', {}, {})

        # Assert
        self.assertEqual(267, len(result))

        for single_result in result:
            if single_result.tags['name'] == 'fwx_alloc':
                if single_result.name == 'nat-connections':
                    if  single_result.tags['corexl-id'] == '0':
                        self.assertEqual(320, single_result.value)
                    if  single_result.tags['corexl-id'] == '1':
                        self.assertEqual(321, single_result.value)
                    if  single_result.tags['corexl-id'] == '2':
                        self.assertEqual(283, single_result.value)


if __name__ == '__main__':
    unittest.main()