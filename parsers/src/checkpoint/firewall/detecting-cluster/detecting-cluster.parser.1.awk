############
# Why: Try multiple methods to determine if the device is part of a cluster, and if it is using ClusterXL or VRRP.
# How: Check for files created when joining a cluster as well as cluster status commands.
###########

# The following two sections has been added by request of Dan Shouky
# https://indeni.atlassian.net/browse/IKP-1221

# Unfortunately, the following code is duplicated in many .ind scripts.
# If you change something in the following two sections, please find all
# of the other instances of this code and make the change there also.

#Could not acquire the config lock
/Could not acquire the config lock/ {
    if (NR == 1) {
        next
    }
}

#CLINFR0829  Unable to get user permissions
#CLINFR0819  User: johndoe denied access via CLI
#CLINFR0599  Failed to build ACLs
/(CLINFR0829\s+Unable to get user permissions|CLINFR0819\s+User: .+ denied access via CLI|CLINFR0599\s+Failed to build ACLs)/ {
    critical_error = 1
    exit
}

#2
/^[0-9]$/ {
    if ($1 > 1) {
        cluster++
    }
}

#Cluster Mode:   Sync only (OPSEC) with IGMP Membership
#Cluster Mode:   High Availability (Active Up) with IGMP Membership
#Cluster Mode:   HA Over LS
/^Cluster Mode\:\s+/ {
    cluster++
}

#Cluster Mode:   High Availability (Active Up) with IGMP Membership
#Cluster Mode:   Sync only (IPSO cluster) with IGMP Membership
#Cluster Mode:   HA Over LS
/Load Sharing|High Availability|Sync only|HA Over LS/ {
    cluster_xl = 1
}

#VRRP State
/VRRP State/ {
    vrrp = 1
}

#| Chassis 1                                                                    |
/^\| Chassis/ {
    # For 61k
    cluster++
}

END {

    # There was an issue when fetching the config, see the note above regarding IKP-1221
    # Exiting the script
    if (critical_error) {
        exit
    }

    if (cluster > 0) {
        writeTag("high-availability", "true")
    }

    # If the device is using VRRP it is not using ClusterXL
    if (vrrp) {
        writeTag("vrrp", "true")
    } else if (cluster_xl) {
        writeTag("clusterxl", "true")
    }
}