#HPING 10.11.40.45 (eth0 10.11.40.45): S set, 40 headers + 0 data bytes
#
#--- 10.11.40.45 hping statistic ---
#3 packets tramitted, 0 packets received, 100% packet loss
#round-trip min/avg/max = 0.0/0.0/0.0 ms

# nc output
# 10.11.94.150 18264 open
# 10.11.94.150 18264: Connection refused
# 10.11.94.222 18264: No route to host

$2 ~ "18264" {
    ip = $1
    ca_arr[ip] = 0

    if ((NF == 3) && ($NF == "open")) {
        ca_arr[ip] = 1
    }
}

END {
    ca_access = 0
    for (ip in ca_arr) {
        if (ca_arr[ip] == 1) {
            ca_access = 1
        }
        ca_tags["name"] = ip
        if (ca_tags_all["name"] == "") {
            ca_tags_all["name"] = ca_tags["name"]
        } else {
            ca_tags_all["name"] = ca_tags_all["name"]":"ca_tags["name"]
        }
        writeDoubleMetric("ca-status", ca_tags, "gauge", ca_arr[ip], "true", "Certificate Authorities Accessible", "state", "name")
    }
    writeDoubleMetric("ca-accessible", ca_tags_all, "gauge", ca_access, "false", "Certificate Authorities Accessible", "state", "name")
}