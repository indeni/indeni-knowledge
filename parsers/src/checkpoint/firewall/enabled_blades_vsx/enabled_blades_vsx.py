from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class EnabledBladesVsx(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            basic = helper_methods.parse_data_as_list(raw_data, 'checkpoint_enabled_blades_vsx.textfsm')

            # Clear empty values:
            if basic:
                full_list = [item for item in basic if item['vsid'] != '']

            # Loop over the list, and create metrics:
            if full_list:
                for entry in full_list:
                    tags = {'vs.id': entry['vsid'],
                            'vs.name': entry['vs_name']}
                    split_features = entry['Blades'].split()
                    if split_features:
                        features = []
                        for feature in split_features:
                            line = {'name': feature}
                            features.append(line)
                        if features:
                            self.write_complex_metric_object_array('features-enabled', tags, features, False)
        return self.output
