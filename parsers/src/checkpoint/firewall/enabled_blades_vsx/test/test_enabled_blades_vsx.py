import os
import unittest

from checkpoint.firewall.enabled_blades_vsx.enabled_blades_vsx import EnabledBladesVsx
from parser_service.public.action import *

class TestEnabledBladesVsx(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = EnabledBladesVsx()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_invalid_input_clean(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/invalid_input.input', {}, {})
        # Assert
        self.assertEqual(0, len(result))

    def test_valid_input_some_errors(self):
        expected_results = [{'name': None, 'vs.id': '0', 'vs.name': 'lab-CP-VSXVSLS1', 'metric': 'features-enabled', 'value': [{'name': 'fw'}, {'name': 'ips'}]},
                            {'name': None, 'vs.id': '2', 'vs.name': 'lab-CP-VSXVSLS1_lab-CP-vs1', 'metric': 'features-enabled', 'value': [{'name': 'fw'}, {'name': 'ips'}]}]

        # Act
        result = self.parser.parse_file(self.current_dir + '/valid_input_some_errors.input', {}, {})
        # Assert
        self.assertEqual(2, len(result))

        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i]['metric'], result[i].name)
            self.assertEqual(expected_results[i]['value'], result[i].value)
            self.assertEqual(expected_results[i]['vs.id'], result[i].tags['vs.id'])
            self.assertEqual(expected_results[i]['vs.name'], result[i].tags['vs.name'])


    def test_valid_input_many_errors(self):
        expected_results = [{'name': None, 'vs.id': '0', 'vs.name': 'CP-R80.10-VSX2-1', 'metric': 'features-enabled', 'value': [{'name': 'fw'}, {'name': 'ips'}]},
                            {'name': None, 'vs.id': '2', 'vs.name': 'CP-R80.10-VSX2-VSYS1', 'metric': 'features-enabled', 'value': [{'name': 'fw'}, {'name': 'vpn'}, {'name': 'urlf'}, {'name': 'av'}, {'name': 'appi'}, {'name': 'ips'}, {'name': 'identityServer'}, {'name': 'anti_bot'}, {'name': 'vpn'}]},
                            {'name': None, 'vs.id': '3', 'vs.name': 'CP-R80.10-VSX2-VSYS2', 'metric': 'features-enabled', 'value': [{'name': 'fw'}, {'name': 'vpn'}, {'name': 'cvpn'}, {'name': 'ips'}, {'name': 'identityServer'}, {'name': 'anti_bot'}, {'name': 'vpn'}]}]

        # Act
        result = self.parser.parse_file(self.current_dir + '/valid_input_many_errors.input', {}, {})
        # Assert
        self.assertEqual(3, len(result))

        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i]['metric'], result[i].name)
            self.assertEqual(expected_results[i]['value'], result[i].value)
            self.assertEqual(expected_results[i]['vs.id'], result[i].tags['vs.id'])
            self.assertEqual(expected_results[i]['vs.name'], result[i].tags['vs.name'])


    def test_valid_input_clean(self):
        expected_results = [{'name': None, 'vs.id': '0', 'vs.name': 'Manchester_VSX_Cluster01', 'metric': 'features-enabled', 'value': [{'name': 'fw'}]},
                            {'name': None, 'vs.id': '2', 'vs.name': 'Softcat_Manchester_VS', 'metric': 'features-enabled', 'value': [{'name': 'fw'}, {'name': 'vpn'}, {'name': 'ips'}, {'name': 'vpn'}]},
                            {'name': None, 'vs.id': '3', 'vs.name': 'Cardlytics_Manchester_vs', 'metric': 'features-enabled', 'value': [{'name': 'fw'}, {'name': 'vpn'}, {'name': 'ips'}, {'name': 'vpn'}]},
                            {'name': None, 'vs.id': '4', 'vs.name': 'BluesourceStorage_Manchester_VS', 'metric': 'features-enabled', 'value': [{'name': 'fw'}, {'name': 'vpn'}, {'name': 'ips'}, {'name': 'mon'}, {'name': 'vpn'}]},
                            {'name': None, 'vs.id': '10', 'vs.name': 'RFU_VS_Manchester', 'metric': 'features-enabled', 'value': [{'name': 'fw'}, {'name': 'vpn'}, {'name': 'ips'}, {'name': 'vpn'}]},
                            {'name': None, 'vs.id': '12', 'vs.name': 'MooreStephens_VS_Manchester', 'metric': 'features-enabled', 'value': [{'name': 'fw'}, {'name': 'vpn'}, {'name': 'ips'}, {'name': 'vpn'}]},
                            {'name': None, 'vs.id': '13', 'vs.name': 'Nandos_VS_ManchesterDC', 'metric': 'features-enabled', 'value': [{'name': 'fw'}, {'name': 'vpn'}, {'name': 'ips'}, {'name': 'vpn'}]},
                            {'name': None, 'vs.id': '14', 'vs.name': 'MartinCurrie_vs_Manchester', 'metric': 'features-enabled', 'value': [{'name': 'fw'}, {'name': 'vpn'}, {'name': 'ips'}, {'name': 'vpn'}]},
                            {'name': None, 'vs.id': '15', 'vs.name': 'MobiusLife_vs_Manchester', 'metric': 'features-enabled', 'value': [{'name': 'fw'}, {'name': 'vpn'}, {'name': 'ips'}, {'name': 'vpn'}]}]

        # Act
        result = self.parser.parse_file(self.current_dir + '/valid_input_clean.input', {}, {})
        # Assert
        self.assertEqual(9, len(result))

        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i]['metric'], result[i].name)
            self.assertEqual(expected_results[i]['value'], result[i].value)
            self.assertEqual(expected_results[i]['vs.id'], result[i].tags['vs.id'])
            self.assertEqual(expected_results[i]['vs.name'], result[i].tags['vs.name'])


if __name__ == '__main__':
    unittest.main()