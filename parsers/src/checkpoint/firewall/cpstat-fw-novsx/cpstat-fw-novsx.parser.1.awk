#Policy name: Standard
/Policy name/ {
    policyname = $0
    gsub(/Policy name\:/, "", policyname)
    policyname = trim(policyname)
}

#Install time: Mon May  9 03:05:41 2016
/Install time/ {
    install_time = $0

    # Remove the "install time:" part
    gsub(/Install time\:/, "", install_time)

    # Remove some double spaces
    gsub(/  /, " ", install_time)

    split(trim(install_time), install_time_arr, " ")
    month_num = parseMonthThreeLetter(install_time_arr[2])
    day = install_time_arr[3]
    time = install_time_arr[4]

    split(time, time_arr, ":")
    hour = time_arr[1]
    minute = time_arr[2]
    second = time_arr[3]
    year = install_time_arr[5]
}

#ISP link table
#------------------------
#|Name   |Status|Role   |
#------------------------
#|ISP-1  |OK    |Primary|
#|ISP-2  |OK    |Backup |
#------------------------

/^\s*\|\s*ISP/{
    isp_output = $0
    # Preserve the "Status" string it may be an error message
    split(isp_output, isp_arr, "|")
    tags["isp-link"] = trim(isp_arr[2]) " - " trim(isp_arr[4])
    writeComplexMetricString("isp-link-status", tags, trim(isp_arr[3]), "false")  # Converted to new syntax by change_ind_scripts.py script
}

END {
    if (policyname != "") {
        policystatus = 1
    } else {
        policystatus = 0
    }

    writeDoubleMetric("policy-installed", null, "gauge", policystatus, "true", "Firewall Policy", "state", "name")  # Converted to new syntax by change_ind_scripts.py script
    writeComplexMetricString("policy-name", null, policyname, "true", "Firewall Policy Name")  # Converted to new syntax by change_ind_scripts.py script

    if (install_time != "") {
        writeDoubleMetric("policy-install-last-modified", null, "gauge", datetime(year, month_num, day, hour, minute, second), "true", "Firewall Policy - Last Modified", "date", "")  # Converted to new syntax by change_ind_scripts.py script
    }
}