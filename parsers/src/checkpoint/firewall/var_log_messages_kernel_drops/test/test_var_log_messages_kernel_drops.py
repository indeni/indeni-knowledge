import os
import unittest

from checkpoint.firewall.var_log_messages_kernel_drops.var_log_messages_kernel_drops import VarLogMessagesKernelDrops
from parser_service.public.action import WriteDoubleMetric


class TestVarLogMessagesKernelDrops(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = VarLogMessagesKernelDrops()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_errors_found(self):
        result = self.parser.parse_file(self.current_dir + '/errors_found.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertTrue(isinstance(result[0], WriteDoubleMetric))
        self.assertTrue(result[0].name, 'kernel-packet-drops')
        self.assertTrue(result[0].tags['display-name'], 'Kernel Packet Drops')
        self.assertEqual(result[0].value, 1)

    def test_failed_input(self):
        result = self.parser.parse_file(self.current_dir + '/failed_input.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertTrue(isinstance(result[0], WriteDoubleMetric))
        self.assertTrue(result[0].name, 'kernel-packet-drops')
        self.assertTrue(result[0].tags['display-name'], 'Kernel Packet Drops')
        self.assertEqual(result[0].value, 0)

if __name__ == '__main__':
    unittest.main()
