from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class VarLogMessagesKernelDrops(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        data = helper_methods.parse_data_as_list(raw_data, 'var_log_messages_kernel_drops.textfsm')
        self.write_double_metric('kernel-packet-drops', {}, 'gauge', 1 if data else 0, True, 'Kernel Packet Drops','state')
        return self.output
