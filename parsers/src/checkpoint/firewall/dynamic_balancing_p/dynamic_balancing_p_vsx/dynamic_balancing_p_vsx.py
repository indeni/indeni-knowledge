from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser
import re


class ChkpDynamicBalancingStatusVsx(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            vs_info_parsed = helper_methods.parse_data_as_list(raw_data, 'vs_info.textfsm')
            blade_vs_regex = r'(^\d+)\_(\d+)\:?(\d+)?'
            outputs = re.split(blade_vs_regex, raw_data, 0, re.MULTILINE)
            outputs.pop(0)
            id = 0
            while id < len(outputs):
                tags = {}
                if outputs[id+2] is not None:
                    tags['vs.id'] = outputs[id+2]
                    tags['vs.name'] = [vs_info['vs_name'] for vs_info in vs_info_parsed if vs_info['vs_id']== outputs[id+2]][0]
                    if outputs[id+1] != '99':
                        tags['name'] = ' '.join(('VSID_{}'.format(outputs[id+2]), tags['name']))
                if outputs[id+1] is not None and outputs[id+1] != '99':
                    tags['blade'] = outputs[id+1]
                    tags['name'] = ' '.join(('blade_{}'.format(outputs[id+1]), tags['name']))
                
                data_parsed = helper_methods.parse_data_as_list(outputs[id+3], 'dynamic_balancing_p_gw.textfsm')
                if data_parsed:
                    self.write_double_metric('dynamic-balancing-status', tags, 'gauge', 1 if data_parsed[0]['dynamic_balancing_status'] in ['on', 'On']  else 0, True, 'Dynamic Balancing Status', 'state', 'vs.name')
                id += 4
        return self.output