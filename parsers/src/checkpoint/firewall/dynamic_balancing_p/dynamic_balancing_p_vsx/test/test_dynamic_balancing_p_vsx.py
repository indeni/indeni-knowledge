import os
import unittest

from checkpoint.firewall.dynamic_balancing_p.dynamic_balancing_p_vsx.dynamic_balancing_p_vsx import ChkpDynamicBalancingStatusVsx
from parser_service.public.action import *

class TestChkpDynamicBalancingStatusVsx(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = ChkpDynamicBalancingStatusVsx()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_3vs_all_on(self):
        result = self.parser.parse_file(self.current_dir + '/3vs_all_on.input', {}, {})
        self.assertEqual(3, len(result))
        self.assertEqual(result[0].value,1)
        self.assertEqual(result[0].action_type,'WriteDoubleMetric')
        self.assertEqual(result[0].name,'dynamic-balancing-status')
        self.assertEqual(result[0].tags['vs.name'],'VSX-1')
        self.assertEqual(result[0].tags['vs.id'],'0')
        self.assertEqual(result[1].value,1)
        self.assertEqual(result[1].action_type,'WriteDoubleMetric')
        self.assertEqual(result[1].name,'dynamic-balancing-status')
        self.assertEqual(result[1].tags['vs.name'],'VS-1-R81')
        self.assertEqual(result[1].tags['vs.id'],'1')
        self.assertEqual(result[2].value,1)
        self.assertEqual(result[2].action_type,'WriteDoubleMetric')
        self.assertEqual(result[2].name,'dynamic-balancing-status')
        self.assertEqual(result[2].tags['vs.name'],'VS-2-R81')
        self.assertEqual(result[2].tags['vs.id'],'2')

    def test_3vs_all_off(self):
        result = self.parser.parse_file(self.current_dir + '/3vs_all_off.input', {}, {})
        self.assertEqual(3, len(result))
        self.assertEqual(result[0].value,0)
        self.assertEqual(result[0].action_type,'WriteDoubleMetric')
        self.assertEqual(result[0].name,'dynamic-balancing-status')
        self.assertEqual(result[0].tags['vs.name'],'VSX-1')
        self.assertEqual(result[0].tags['vs.id'],'0')
        self.assertEqual(result[1].value,0)
        self.assertEqual(result[1].action_type,'WriteDoubleMetric')
        self.assertEqual(result[1].name,'dynamic-balancing-status')
        self.assertEqual(result[1].tags['vs.name'],'VS-1-R81')
        self.assertEqual(result[1].tags['vs.id'],'1')
        self.assertEqual(result[2].value,0)
        self.assertEqual(result[2].action_type,'WriteDoubleMetric')
        self.assertEqual(result[2].name,'dynamic-balancing-status')
        self.assertEqual(result[2].tags['vs.name'],'VS-2-R81')
        self.assertEqual(result[2].tags['vs.id'],'2')

    def test_3vs_1_off(self):
        result = self.parser.parse_file(self.current_dir + '/3vs_1_off.input', {}, {})
        self.assertEqual(3, len(result))
        self.assertEqual(result[0].value,1)
        self.assertEqual(result[0].action_type,'WriteDoubleMetric')
        self.assertEqual(result[0].name,'dynamic-balancing-status')
        self.assertEqual(result[0].tags['vs.name'],'VSX-1')
        self.assertEqual(result[0].tags['vs.id'],'0')
        self.assertEqual(result[1].value,0)
        self.assertEqual(result[1].action_type,'WriteDoubleMetric')
        self.assertEqual(result[1].name,'dynamic-balancing-status')
        self.assertEqual(result[1].tags['vs.name'],'VS-1-R81')
        self.assertEqual(result[1].tags['vs.id'],'1')
        self.assertEqual(result[2].value,1)
        self.assertEqual(result[2].action_type,'WriteDoubleMetric')
        self.assertEqual(result[2].name,'dynamic-balancing-status')
        self.assertEqual(result[2].tags['vs.name'],'VS-2-R81')
        self.assertEqual(result[2].tags['vs.id'],'2')

if __name__ == '__main__':
    unittest.main()