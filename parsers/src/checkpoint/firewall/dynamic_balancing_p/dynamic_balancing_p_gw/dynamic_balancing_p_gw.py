from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class ChkpDynamicBalancingStatusGw(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        data_parsed = helper_methods.parse_data_as_list(raw_data, 'dynamic_balancing_p_gw.textfsm')
        if data_parsed:
            self.write_double_metric('dynamic-balancing-status', {}, 'gauge', 1 if data_parsed[0]['dynamic_balancing_status'] in ['on', 'On']  else 0, True,
                                                    'Dynamic Balancing Status', 'state')
        return self.output
