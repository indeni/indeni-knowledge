import os
import unittest

from checkpoint.firewall.dynamic_balancing_p.dynamic_balancing_p_gw.dynamic_balancing_p_gw import ChkpDynamicBalancingStatusGw
from parser_service.public.action import *

class TestChkpDynamicBalancingStatusGw(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = ChkpDynamicBalancingStatusGw()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_dynamic_balancing_disabled(self):
        result = self.parser.parse_file(self.current_dir + '/dynamic_balancing_disabled.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].value,0)
        self.assertEqual(result[0].action_type,'WriteDoubleMetric')
        self.assertEqual(result[0].name,'dynamic-balancing-status')

    def test_dynamic_balancing_enabled(self):
        result = self.parser.parse_file(self.current_dir + '/dynamic_balancing_enabled.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].value,1)
        self.assertEqual(result[0].action_type,'WriteDoubleMetric')
        self.assertEqual(result[0].name,'dynamic-balancing-status')

    def test_dynamic_balancing_not_supported(self):
        result = self.parser.parse_file(self.current_dir + '/dynamic_balancing_not_supported.input', {}, {})
        self.assertEqual(0, len(result))


if __name__ == '__main__':
    unittest.main()