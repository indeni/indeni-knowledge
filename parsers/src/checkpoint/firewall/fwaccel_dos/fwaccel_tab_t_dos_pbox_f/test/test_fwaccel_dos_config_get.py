import os
import unittest

from checkpoint.firewall.fwaccel_dos.fwaccel_tab_t_dos_pbox_f.fwaccel_tab_t_dos_pbox import FwaccelDosPboxTableEntries


class TestFwaccelDosPboxTableEntries(unittest.TestCase):

    def setUp(self):
        self.parser = FwaccelDosPboxTableEntries()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_R80_30_4_entries_in_table(self):
        result = self.parser.parse_file(self.current_dir + '/R80_30_4_entries_in_table.input', {}, {})
        self.assertEqual(2, len(result))
        self.assertEqual(result[0].tags['ip.blocked'], '187.136.10.46/32')
        self.assertEqual(result[0].name, 'securexl-dos-pbox-tab-entry')
        self.assertEqual(result[1].tags['ip.blocked'], '45.227.255.0/24')
        self.assertEqual(result[1].name, 'securexl-dos-pbox-tab-entry')

if __name__ == '__main__':
    unittest.main()
