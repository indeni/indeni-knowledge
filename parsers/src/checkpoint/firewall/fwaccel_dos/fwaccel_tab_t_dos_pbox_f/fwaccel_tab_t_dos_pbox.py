from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser
import ipaddress

class FwaccelDosPboxTableEntries(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            parsed_data = helper_methods.parse_data_as_list(raw_data, 'checkpoint_gaia_fwaccel_tab_t_dos_pbox.textfsm')
            if parsed_data:
                tags = {}
                if (len(parsed_data[0]['whitelist_cidr']) != 0):
                    blocked_exists = (int(parsed_data[0]['total_entries']) != 0 and len(parsed_data[0]['blocked_ips']) != 0)
                    for whitelist_cidr in parsed_data[0]['whitelist_cidr']:
                        tags['ip.blocked'] = whitelist_cidr
                        status = 1
                        if (blocked_exists):
                            for blocked_ip in parsed_data[0]['blocked_ips']:
                                try:
                                    if ipaddress.ip_address(blocked_ip) in ipaddress.ip_network(whitelist_cidr):
                                        status = 0
                                        break
                                except:
                                    pass
                        self.write_double_metric('securexl-dos-pbox-tab-entry', tags, 'gauge', status, False)

        return self.output
