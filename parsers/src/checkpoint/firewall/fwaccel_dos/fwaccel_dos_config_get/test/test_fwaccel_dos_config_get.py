import os
import unittest

from checkpoint.firewall.fwaccel_dos.fwaccel_dos_config_get.fwaccel_dos_config_get import FwaccelDosConfigGet


class TestFwaccelDosConfigGet(unittest.TestCase):

    def setUp(self):
        self.parser = FwaccelDosConfigGet()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_R80_40_default_config(self):
        result = self.parser.parse_file(self.current_dir + '/R80_40_default_config.input', {}, {})
        self.assertEqual(10, len(result))
