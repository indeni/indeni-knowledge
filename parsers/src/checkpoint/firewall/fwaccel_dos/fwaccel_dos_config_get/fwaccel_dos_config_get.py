from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class FwaccelDosConfigGet(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            parsed_data = helper_methods.parse_data_as_list(raw_data, 'checkpoint_gaia_fwaccel_dos_config_get.textfsm')
            if parsed_data:
                tags = {}
                tags['im.identity-tags'] = 'type'
                if parsed_data[0]['rate_limit']:
                    tags['type'] = 'Rate Limit'
                    self.write_complex_metric_string('securexl-dos-rate-limit-status', tags, parsed_data[0]['rate_limit'], True, 'SecureXL - DoS')
                if parsed_data[0]['rule_cache']:
                    tags['type'] = 'Rule Cache'
                    self.write_complex_metric_string('securexl-dos-rule-cache-status', tags, parsed_data[0]['rule_cache'], True, 'SecureXL - DoS')
                if parsed_data[0]['pbox']:
                    tags['type'] = 'Pbox'
                    self.write_complex_metric_string('securexl-dos-pbox-status', tags, parsed_data[0]['pbox'], True, 'SecureXL - DoS')
                if parsed_data[0]['deny_list']:
                    tags['type'] = 'Deny List'
                    self.write_complex_metric_string('securexl-dos-deny-list-status', tags, parsed_data[0]['deny_list'], True, 'SecureXL - DoS')
                if parsed_data[0]['drop_frags']:
                    tags['type'] = 'Drop Frags'
                    self.write_complex_metric_string('securexl-dos-drop-frags-status', tags, parsed_data[0]['drop_frags'], True, 'SecureXL - DoS')
                if parsed_data[0]['drop_opts']:
                    tags['type'] = 'Drop Opts'
                    self.write_complex_metric_string('securexl-dos-drop-opts-status', tags, parsed_data[0]['drop_opts'], True, 'SecureXL - DoS')
                if parsed_data[0]['internal']:
                    tags['type'] = 'Internal'
                    self.write_complex_metric_string('securexl-dos-internal-status', tags, parsed_data[0]['internal'], True, 'SecureXL - DoS')
                if parsed_data[0]['monitor']:
                    tags['type'] = 'Monitor'
                    self.write_complex_metric_string('securexl-dos-monitor-status', tags, parsed_data[0]['monitor'], True, 'SecureXL - DoS')
                if parsed_data[0]['log_drops']:
                    tags['type'] = 'Log Drops'
                    self.write_complex_metric_string('securexl-dos-log-drops-status', tags, parsed_data[0]['log_drops'], True, 'SecureXL - DoS')
                if parsed_data[0]['log_pbox']:
                    tags['type'] = 'Log Pbox'
                    self.write_complex_metric_string('securexl-dos-log-pbox-status', tags, parsed_data[0]['log_pbox'], True, 'SecureXL - DoS')
        return self.output
