from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class FwaccelDosStatsGet(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            parsed_data = helper_methods.parse_data_as_list(raw_data, 'checkpoint_gaia_fwaccel_dos_stats_get.textfsm')
            if parsed_data:
                tags = {}
                if parsed_data[0]['pbox_pkt_drop']:
                    self.write_double_metric('securexl-dos-pbox-pkt-dropped', tags, 'gauge', int(parsed_data[0]['pbox_pkt_drop']), False)
        return self.output
