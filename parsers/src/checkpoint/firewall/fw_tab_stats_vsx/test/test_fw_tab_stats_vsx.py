import os
import unittest

from checkpoint.firewall.fw_tab_stats_vsx.fw_tab_stats_vsx import FwTabStatsVsx
from parser_service.public.action import WriteComplexMetric


class TestFwTabStatsVsx(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = FwTabStatsVsx()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_basic_test(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/basic_test.input', {}, {})

        # Assert
        self.assertEqual(113, len(result))
        for single_result in result:
            if single_result.tags['name'] == 'fwx_alloc' and single_result.tags['corexl-id'] == '0' and single_result.tags['vs.name'] =='USASHRAPEQUIFW':
                if single_result.name == 'nat-connections':
                    self.assertEqual(1, single_result.value)
                else:
                    self.assertEqual(17000, single_result.value)
            elif single_result.tags['name'] == 'dcerpc_maps' and single_result.tags['corexl-id'] == '0':
                if single_result.name == 'kernel-table-actual':
                    self.assertEqual(0, single_result.value)
                else:
                    self.assertEqual(25000, single_result.value)
            elif single_result.tags['name'] == 'host_ip_addrs_all' and single_result.tags['corexl-id'] == '0':
                if single_result.name == 'kernel-table-actual':
                    self.assertEqual(16, single_result.value)
                else:
                     self.assertEqual(25000, single_result.value)


    def test_two_vs(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/two_vs.input', {}, {})

        # Assert
        self.assertEqual(250, len(result))
        for single_result in result:
            if single_result.tags['name'] == 'fwx_alloc' and single_result.tags['corexl-id'] == '0' and single_result.tags['vs.name'] =='USASHRAPEQUIFW':
                if single_result.name == 'nat-connections':
                    self.assertEqual(49994, single_result.value)
                else:
                    self.assertEqual(200000, single_result.value)
            if single_result.tags['name'] == 'fwx_alloc' and single_result.tags['corexl-id'] == '0' and single_result.tags['vs.name'] =='asfafafs':
                if single_result.name == 'nat-connections':
                    self.assertEqual(49994, single_result.value)
                else:
                    self.assertEqual(200000, single_result.value)
            elif single_result.tags['name'] == 'dcerpc_maps' and single_result.tags['corexl-id'] == '0' and single_result.tags['vs.name'] =='asfafafs':
                if single_result.name == 'kernel-table-actual':
                    self.assertEqual(0, single_result.value)
                else:
                    self.assertEqual(25000, single_result.value)
            elif single_result.tags['name'] == 'dcerpc_maps' and single_result.tags['corexl-id'] == '0' and single_result.tags['vs.name'] =='USASHRAPEQUIFW':
                if single_result.name == 'kernel-table-actual':
                    self.assertEqual(0, single_result.value)
                else:
                    self.assertEqual(25000, single_result.value)

    def test_two_vs_output_interleaved(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/two_vs_output_interleaved.input', {}, {})

        # Assert
        self.assertEqual(250, len(result))
        for single_result in result:
            if single_result.tags['name'] == 'fwx_alloc' and single_result.tags['corexl-id'] == '0' and single_result.tags['vs.name'] =='USASHRAPEQUIFW':
                if single_result.name == 'nat-connections':
                    self.assertEqual(49994, single_result.value)
                else:
                    self.assertEqual(200000, single_result.value)
            if single_result.tags['name'] == 'fwx_alloc' and single_result.tags['corexl-id'] == '0' and single_result.tags['vs.name'] =='asfafafs':
                if single_result.name == 'nat-connections':
                    self.assertEqual(49994, single_result.value)
                else:
                    self.assertEqual(200000, single_result.value)

if __name__ == '__main__':
    unittest.main()