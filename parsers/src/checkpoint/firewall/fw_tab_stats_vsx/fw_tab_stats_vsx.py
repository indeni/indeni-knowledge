from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class FwTabStatsVsx(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        tables_to_collect = ["pep_identity_index",
                                            "fwx_alloc",
                                            "fwx_alloc_global",
                                            "connections",
                                            "cphwd_db",
                                            "cphwd_tmpl",
                                            "cphwd_dev_conn_table",
                                            "cphwd_vpndb",
                                            "cphwd_dev_identity_table",
                                            "cphwd_dev_revoked_ips_table",
                                            "cphwd_pslglue_conn_db",
                                            "f2f_addresses",
                                            "tcp_f2f_ports",
                                            "udp_f2f_ports",
                                            "tcp_f2f_conns",
                                            "udp_f2f_conns",
                                            "dos_suspected",
                                            "dos_penalty_box",
                                            "client_auth",
                                            "pdp_sessions",
                                            "pdp_super_sessions",
                                            "pdp_ip",
                                            "crypt_resolver_DB",
                                            "cluster_active_robo",
                                            "appi_connections",
                                            "cluster_connections_nat",
                                            "cryptlog_table",
                                            "DAG_ID_to_IP",
                                            "DAG_IP_to_ID",
                                            "decryption_pending",
                                            "encryption_requests",
                                            "IKE_SA_table",
                                            "ike2esp",
                                            "ike2peer",
                                            "inbound_SPI",
                                            "initial_contact_pending",
                                            "ipalloc_tab",
                                            "IPSEC_mtu_icmp",
                                            "IPSEC_mtu_icmp_wait",
                                            "L2TP_lookup",
                                            "L2TP_MSPI_cluster_update",
                                            "L2TP_sessions",
                                            "L2TP_tunnels",
                                            "MSPI_by_methods",
                                            "MSPI_cluster_map",
                                            "MSPI_cluster_update",
                                            "MSPI_cluster_reverse_map",
                                            "MSPI_req_connections",
                                            "MSPI_requests",
                                            "outbound_SPI",
                                            "peer2ike",
                                            "peers_count",
                                            "persistent_tunnels",
                                            "rdp_dont_trap",
                                            "rdp_table",
                                            "resolved_link",
                                            "Sep_my_IKE_packet",
                                            "SPI_requests",
                                            "udp_enc_cln_table",
                                            "udp_response_nat",
                                            "VIN_SA_to_delete",
                                            "vpn_active",
                                            "vpn_routing",
                                            "XPO_names",
                                            "vpn_queues",
                                            "ikev2_sas",
                                            "sam_requests",
                                            "tnlmon_life_sign",
                                            "string_dictionary_table",
                                            "dns_cache_tbl"]

        if raw_data:
            table_limits_all = helper_methods.parse_data_as_list(raw_data, 'fw_tab_grep_limit.textfsm')
            table_values_all = helper_methods.parse_data_as_list(raw_data, 'fw_tab_s.textfsm')

            if table_limits_all and table_values_all:
                table_limits = []
                table_values = []
                for limit in table_limits_all:
                    if limit['table_name'] in tables_to_collect:
                        table_limits.append(limit)
                for value in table_values_all:
                    if value['table_name'] in tables_to_collect:
                        table_values.append(value)


                for value in table_values:
                    table_tags = {}
                    table_tags["name"] = value['table_name']
                    table_tags["corexl-id"] = value['CoreXL_inst']
                    if table_tags["corexl-id"] == '':
                        table_tags["corexl-id"] = '0'
                    table_tags['vs.id'] = value['vs_id']
                    table_tags['vs.name'] = value['vs_name']
                    if table_tags["name"] == "pep_identity_index":
                        self.write_double_metric("identity-awareness-users-actual", table_tags, "gauge", int(value['table_num_entries']), True, "Identity Awareness User - Current", "number", "vs.id|vs.name|corexl-id")
                    elif ('fwx_alloc' or 'fwx_alloc_global') in table_tags["name"]:
                        self.write_double_metric("nat-connections", table_tags, "gauge", int(value['table_num_entries']), True, "NAT Connections - Current", "number", "vs.id|vs.name|corexl-id")
                    else:
                        self.write_double_metric("kernel-table-actual", table_tags, "gauge", int(value['table_num_entries']), False)
                    for limit in table_limits:
                        if limit['table_name'] == value['table_name'] and limit['CoreXL_inst'] == value ['CoreXL_inst'] and limit['vs_id'] == value ['vs_id']:
                            if table_tags["name"] == "pep_identity_index":
                                self.write_double_metric("identity-awareness-users-limit", table_tags, "gauge", int(limit['table_limit']), True, "Identity Awareness User - Limit", "number", "vs.id|vs.name|corexl-id")
                            elif ('fwx_alloc' or 'fwx_alloc_global') in table_tags["name"]:
                                self.write_double_metric("nat-connections-limit", table_tags, "gauge", int(limit['table_limit']), True, "NAT Connections - Limit", "number", "vs.id|vs.name|corexl-id")
                            else:
                                self.write_double_metric("kernel-table-limit", table_tags, "gauge", int(limit['table_limit']), False)


        return self.output