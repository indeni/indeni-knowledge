BEGIN {
	FS="|"
	icpu=0
}

#  0 | Yes     | 11     |        2722 |    15702
/Yes/ {
	connections=$4
	coreXLTags["cpu-id"] = $1
	writeDoubleMetric("corexl-cpu-connections", coreXLTags, "gauge", trim(connections), "false")  # Converted to new syntax by change_ind_scripts.py script
	icpu++
	coreXL=1
}

END {
	if ( coreXL == 1 ) {
		writeComplexMetricString("corexl-cores-enabled", null, icpu, "true", "CoreXL - Cores Enabled")  # Converted to new syntax by change_ind_scripts.py script
	}
}