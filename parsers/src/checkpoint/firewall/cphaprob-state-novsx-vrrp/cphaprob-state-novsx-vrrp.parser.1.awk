############
# Why: Important to know the status of the cluster
# How: Use both cphaprob state and show vrrp to get the complete picture
## This script is for VRRP clusters
# A remote peer can have the following states
# Down
# Active
# A local peer can have the following states
# Down
# Ready
# Active
# HA module not started.
### The cluster as a whole can have several states regarding different aspects of the cluster
## Redundancy
# The non-active member could be:
# Ready - This member will take over if the Active one goes down, but it is not syncing state tables, so the failover will cut all connections
# Down - This member will not take over in case the active member goes offline
## Health
# Active - This member is healthy and forwarding traffic
## VRRP
# A VRRP cluster shows both   members as "Active" in cphaprob state. This is normal.
###########

BEGIN {
    cluster_is_active = 0
    tags["name"] = "VRRP"
}

# The following two sections has been added by request of Dan Shouky
# https://indeni.atlassian.net/browse/IKP-1221
# Unfortunately, the following code is duplicated in many .ind scripts.
# If you change something in the following two sections, please find all
# of the other instances of this code and make the change there also.
#Could not acquire the config lock
/Could not acquire the config lock/ {
    if (NR == 1) {
        next
    }
}

#CLINFR0829  Unable to get user permissions
#CLINFR0819  User: johndoe denied access via CLI
#CLINFR0599  Failed to build ACLs
/(CLINFR0829\s+Unable to get user permissions|CLINFR0819\s+User: .+ denied access via CLI|CLINFR0599\s+Failed to build ACLs)/ {
    critical_error = 1
    exit
}

# Find out the real status on the local member, since cphaprob state always shows "Active" even when Standby
#        In Backup state 2
#        In Master state 0
/\s+(In Master state|In Backup state)/ {
    if ($2 == "Backup" && $4 > 0) {
        remote_role = "Active"
    } else {
        remote_role = "Standby"
    }
    if ($2 == "Master" && $4 > 0) {
        local_role = "Active"
    } else {
        local_role = "Standby"
    }
}

# Determine cluster mode
#Cluster Mode:   Sync only (OPSEC) with IGMP Membership
/^Cluster Mode\:\s+Sync only/ {
  clustermode = "Sync only"
  writeComplexMetricString("cluster-mode", tags, clustermode, "false")  # Converted to new syntax by change_ind_scripts.py script
}
# Has the cluster at least one healthy member forwarding traffic?
# Match any cluster member being Active (but not Active Attention)
#1 (local)  10.10.6.21      Active
/^\d+\s.*Active$/ {
    cluster_is_active = 1
}

#Number     Unique Address  Assigned Load   State
/Number|ID\s*Unique\s*Address\s*Firewall\s*State/ {
  # Store the state columns for use with getColData
  getColumns(trim($0), "[ ]{2,}", state_columns)
}

/\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/ {
    number = getColData(trim($0), state_columns, "Number")
    unique_address = getColData(trim($0), state_columns, "Unique Address")
    state = getColData(trim($0), state_columns, "Firewall State (*)")

    if (state ~ /Active/) {
        if (local_role == "Active") {
        local_state = 1
        }
        else
        {
            local_state = 0
        }
    }
    else {
        local_state = 0
    }

    all_members_index++
    all_members[all_members_index, "state-description"] = state
    all_members[all_members_index, "id"] = $1
    all_members[all_members_index, "unique-ip"] = unique_address

    if ($2 ~ /local/) {
        all_members[all_members_index, "is-local"] = 1
        if (state == "Active" && local_role) {
            all_members[all_members_index, "state-description"] = local_role
        }

        local_state_description = state
        writeComplexMetricString("cluster-member-active-live-config", tags, local_state_description, "true", "Cluster member state (this)")  # Converted to new syntax by change_ind_scripts.py script
        writeDoubleMetric("cluster-member-active", tags, "gauge", local_state, "false")  # Converted to new syntax by change_ind_scripts.py script
    }
    else {
        all_members[all_members_index, "is-local"] = 0
        if (state == "Active" && remote_role) {
            all_members[all_members_index, "state-description"] = remote_role
        }
    }
}

END {
    # There was an issue when fetching the config, see the note above regarding IKP-1221
    # Exiting the script
    if (critical_error) {
        exit
    }

    # Not good if both members have active interfaces
    if (local_role == "Active" && remote_role == "Active") {
        both_active = "true"
    }

    # If at least one member forwarding traffic is found it is ok. If the cluster services are not running the state is
    # unclear and an alert is issued.
    
    clusterstate = 0
    if (cluster_is_active && both_active != "true") {
        clusterstate = 1
    }
    cluster_state_description = "Local role: " local_role " - Remote role: " remote_role

    writeComplexMetricString("cluster-state-live-config", tags, cluster_state_description, "true", "Cluster State")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("cluster-state", tags, "gauge", clusterstate, "false")  # Converted to new syntax by change_ind_scripts.py script

    # Write status of all members
    if (all_members_index) {
        writeComplexMetricObjectArray("cluster-member-states", tags, all_members, "false")  # Converted to new syntax by change_ind_scripts.py script
    }
}