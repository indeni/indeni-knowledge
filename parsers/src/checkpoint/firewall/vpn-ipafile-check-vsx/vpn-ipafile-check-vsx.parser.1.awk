function addVsTags(tags) {
	tags["vs.id"] = vsid
	tags["vs.name"] = vsname
}

function dumpLineData() {
    addVsTags(linetags)
	writeComplexMetricObjectArray("ipassignment-conf-errors", linetags, lines, "false")  # Converted to new syntax by change_ind_scripts.py script
	delete lines
	iline = 0
}

BEGIN {
	vsid = ""
	vsname = ""
}

#VSID:            0
/VSID:/ {
	if (vsid != "") {
		# write the previous VS's data
		dumpLineData()
	}

	vsid = trim($NF)
}

#Name:            lab-CP-VSXVSLS-1-R7730
/Name:/ {
	vsname = trim($NF)
}

#Invalid IP address specification in line 0058
#Line 0001 appears to be a comment because of second parameter(gateway or address)
/Line|line/ {
	iline++
	lines[iline, "line"] = $0
}

END {
	# The last VS is handled here:
	dumpLineData()
}