# For each VS:
# .elg files listed if were modified (something wrote in file or created) last during last 60 minutes (-mmin option)
# number in file name is deleted (indicates the order into the log rotation)
# files ordered to check the number of files of each proces, number of files printed before file name

function addVsTags(tags) {
    tags["vs.id"] = vsid
    tags["vs.name"] = vsname
    tags["vs.filename"] = filename
    tags["im.identity-tags"] = "vs.id|vs.name"
}

BEGIN {
    vsid = ""
    vsname = ""
}

#_VSID:0
/_VSID:/ {
    split($0, split_arr_vsid, ":")
    vsid = trim(split_arr_vsid[2])
}

#_Name:VSX-CXL2-Gear
/_Name:/ {
    split($0, split_arr_vsname, ":")
    vsname = trim(split_arr_vsname[2])
}


#      1 /opt/CPshrd-R80/CTX/CTX00002/log/cpd.elg
#      5 /opt/CPsuite-R80/fw1/CTX/CTX00002/log/fwd.elg
#      1 /opt/CPsuite-R80/fw1/CTX/CTX00002/log/pdpd.elg
/.elg/{
    filename = $2
    addVsTags(vstags)
    log_files = $1

    writeDoubleMetric("debug-file-counter", vstags, "gauge", log_files, "false")
}
