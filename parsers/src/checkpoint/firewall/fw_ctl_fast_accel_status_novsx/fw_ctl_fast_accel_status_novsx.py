from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class ChkpFwFastAccelStatusNoVsx(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        data_parsed = helper_methods.parse_data_as_list(raw_data, 'fw_ctl_fast_accel_status_novsx.textfsm')
        if data_parsed:
            self.write_double_metric('fast-accel-status', {}, 'gauge', 1 if 'enabled' in data_parsed[0]['fw_fast_accel_status']  else 0, True,
                                                    'Fast Acceleration Status', 'state')
        return self.output
