import string

from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser
import datetime

class ThreatPreventionPolicyNoVsx(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:  # We check input is not emtpy
            lines = raw_data.splitlines()
            if len(lines) == 2:
                threat_prevention_policy_delta = -1
                if len(lines[0]) == 10 and lines[0].isdigit():
                    currentTime = int(lines[0])
                    if lines[1].startswith('policy'):  # if doesnt start with empty space then is userlist
                        policy = lines[1].split()
                        datetimeFormat = '%b %d %X %Y'
                        policyText = policy[5] + ' ' +policy[6] + ' ' + policy[7] + ' ' + policy[8]
                        try:
                            policyTime = datetime.datetime.strptime(policyText, datetimeFormat)
                            threat_prevention_policy_delta = currentTime - int(policyTime.strftime('%s'))
                        except (ValueError, TypeError):
                            pass

                        if threat_prevention_policy_delta != -1:
                            tags = {'threat': lines[1]}
                            self.write_double_metric('threat_prevention_policy_delta', {}, 'gauge', threat_prevention_policy_delta, False)
                            self.write_double_metric('threat_prevention_policy_live', tags, 'gauge', threat_prevention_policy_delta, True, 'Threat Prevention Policy', 'number', 'threat')

        return self.output
