import os
import unittest

from checkpoint.firewall.threat_prevention_policy_novsx.threat_prevention_policy_novsx import ThreatPreventionPolicyNoVsx

class TestThreatPreventionPolicyNoVsx(unittest.TestCase):

    def setUp(self):
        self.parser = ThreatPreventionPolicyNoVsx()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_delta_24(self):
        result = self.parser.parse_file(self.current_dir + '/delta_24.input', {}, {})        # Assert
        self.assertEqual(2, len(result))

        self.assertEqual(result[0].name, 'threat_prevention_policy_delta')
        self.assertEqual(result[0].value, 46194)

        self.assertEqual(result[1].tags['display-name'],'Threat Prevention Policy')
        self.assertEqual(result[1].tags['threat'],'policy = \'Policy: DC1-EXT-PLC Thu Jan 24 03:21:04 2022 (traditional=1)\'')
        self.assertEqual(result[1].value, 46194)

    def test_missing_input(self):
        result = self.parser.parse_file(self.current_dir + '/missing_input.input', {}, {})
        self.assertEqual(0, len(result))

    def test_invalid_date(self):
        result = self.parser.parse_file(self.current_dir + '/invalid_date.input', {}, {})
        self.assertEqual(0, len(result))


if __name__ == '__main__':
    unittest.main()
