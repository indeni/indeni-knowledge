import os
import unittest

from checkpoint.firewall.fwaccel_stat_top_talkers_novsx.top_talkers_src_ip.fwaccel_stat_top_talkers_src_ip_novsx import FwaccelStatTopTalkersSrcIpNoVsx


class TestFwaccelStatTopTalkersSrcIpNoVsx(unittest.TestCase):

    def setUp(self):
        self.parser = FwaccelStatTopTalkersSrcIpNoVsx()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_top_talkers_src(self):
        result = self.parser.parse_file(self.current_dir + '/top_talkers_src.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].tags['display-name'], 'Top 10 Accelerated Connections from Source IP Addresses')
        self.assertEqual(result[0].name,'top-talker-src-accelerate')
        self.assertEqual(result[0].value[0]['#'], '1')
        self.assertEqual(result[0].value[0]['src_ip'], '10.181.64.93')
        self.assertEqual(result[0].value[0]['counter'], '43155')
        self.assertEqual(result[0].value[1]['#'], '2')
        self.assertEqual(result[0].value[1]['src_ip'], '10.181.64.36')
        self.assertEqual(result[0].value[1]['counter'], '22516')
        self.assertEqual(result[0].value[2]['#'], '3')
        self.assertEqual(result[0].value[2]['src_ip'], '10.181.64.92')
        self.assertEqual(result[0].value[2]['counter'], '13697')
        self.assertEqual(result[0].value[3]['#'], '4')
        self.assertEqual(result[0].value[3]['src_ip'], '10.181.64.54')
        self.assertEqual(result[0].value[3]['counter'], '11300')
        self.assertEqual(result[0].value[4]['#'], '5')
        self.assertEqual(result[0].value[4]['src_ip'], '10.181.201.25')
        self.assertEqual(result[0].value[4]['counter'], '8419')
        self.assertEqual(result[0].value[5]['#'], '6')
        self.assertEqual(result[0].value[5]['src_ip'], '10.180.200.146')
        self.assertEqual(result[0].value[5]['counter'], '6700')
        self.assertEqual(result[0].value[6]['#'], '7')
        self.assertEqual(result[0].value[6]['src_ip'], '10.181.64.63')
        self.assertEqual(result[0].value[6]['counter'], '5509')
        self.assertEqual(result[0].value[7]['#'], '8')
        self.assertEqual(result[0].value[7]['src_ip'], '10.173.246.143')
        self.assertEqual(result[0].value[7]['counter'], '4131')
        self.assertEqual(result[0].value[8]['#'], '9')
        self.assertEqual(result[0].value[8]['src_ip'], '10.173.246.146')
        self.assertEqual(result[0].value[8]['counter'], '4057')
        self.assertEqual(result[0].value[9]['#'], '10')
        self.assertEqual(result[0].value[9]['src_ip'], '10.173.246.145')
        self.assertEqual(result[0].value[9]['counter'], '3985')

if __name__ == '__main__':
    unittest.main()
