from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class FwaccelStatTopTalkersSrcIpNoVsx(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            parsed_data = helper_methods.parse_data_as_list(raw_data, 'fwaccel_stat_top_talkers_src_ip_novsx.textfsm')
            if parsed_data:
                parsed_data_ordered = sorted(parsed_data, key=lambda d: int(d['conn_sum']), reverse=True)
                order = 1
                top_talkers = []
                for talker in parsed_data_ordered:
                    item = {}
                    item['#'] = str(order)
                    item['src_ip'] = talker['src_ip']
                    item['counter'] = str(talker['conn_sum'])
                    top_talkers.append(item)
                    order = order+1
                self.write_complex_metric_object_array('top-talker-src-accelerate', {}, top_talkers, True, 'Top 10 Accelerated Connections from Source IP Addresses')
        return self.output
