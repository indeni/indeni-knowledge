import os
import unittest

from checkpoint.firewall.fwaccel_stat_top_talkers_novsx.top_talkers_dst_port.fwaccel_stat_top_talkers_dst_port_novsx import FwaccelStatTopTalkersDstPortNoVsx


class TestFwaccelStatTopTalkersDstPortNoVsx(unittest.TestCase):

    def setUp(self):
        self.parser = FwaccelStatTopTalkersDstPortNoVsx()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_top_talkers_port(self):
        result = self.parser.parse_file(self.current_dir + '/top_talkers_port.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].tags['display-name'], 'Top 10 Accelerated Connections to Destination Ports')
        self.assertEqual(result[0].name,'top-talker-port-accelerate')
        self.assertEqual(result[0].value[0]['#'], '1')
        self.assertEqual(result[0].value[0]['port'], '9100')
        self.assertEqual(result[0].value[0]['counter'], '98245')
        self.assertEqual(result[0].value[1]['#'], '2')
        self.assertEqual(result[0].value[1]['port'], '9997')
        self.assertEqual(result[0].value[1]['counter'], '20200')
        self.assertEqual(result[0].value[2]['#'], '3')
        self.assertEqual(result[0].value[2]['port'], '9093')
        self.assertEqual(result[0].value[2]['counter'], '17101')
        self.assertEqual(result[0].value[3]['#'], '4')
        self.assertEqual(result[0].value[3]['port'], '9996')
        self.assertEqual(result[0].value[3]['counter'], '15724')
        self.assertEqual(result[0].value[4]['#'], '5')
        self.assertEqual(result[0].value[4]['port'], '2059')
        self.assertEqual(result[0].value[4]['counter'], '9020')
        self.assertEqual(result[0].value[5]['#'], '6')
        self.assertEqual(result[0].value[5]['port'], '636')
        self.assertEqual(result[0].value[5]['counter'], '7747')
        self.assertEqual(result[0].value[6]['#'], '7')
        self.assertEqual(result[0].value[6]['port'], '1514')
        self.assertEqual(result[0].value[6]['counter'], '7353')
        self.assertEqual(result[0].value[7]['#'], '8')
        self.assertEqual(result[0].value[7]['port'], '443')
        self.assertEqual(result[0].value[7]['counter'], '4723')
        self.assertEqual(result[0].value[8]['#'], '9')
        self.assertEqual(result[0].value[8]['port'], '17472')
        self.assertEqual(result[0].value[8]['counter'], '3588')
        self.assertEqual(result[0].value[9]['#'], '10')
        self.assertEqual(result[0].value[9]['port'], '53')
        self.assertEqual(result[0].value[9]['counter'], '3540')


if __name__ == '__main__':
    unittest.main()
