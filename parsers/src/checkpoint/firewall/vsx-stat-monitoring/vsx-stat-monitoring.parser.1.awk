#Number of Virtual Systems allowed by license:          25
/^Number of Virtual Systems allowed by license:/ {
    vsAllowed = $8
}

#Virtual Systems [active / configured]:                  2 / 2
/^Virtual Systems \[active \/ configured\]:/ {
    vsConfigured = $8
    vsActive = $6
}

END {
    tags["name"] = "virtual-systems"
    writeDoubleMetric("license-elements-limit", tags, "gauge", vsAllowed, "false")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("license-elements-used", tags, "gauge", vsActive, "false")  # Converted to new syntax by change_ind_scripts.py script
}