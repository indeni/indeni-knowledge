BEGIN {
    found_vsx = "false"
}

#VSX Gateway Status
/VSX Gateway Status/ {
    found_vsx = "true"
}

#Virtual Systems [active / configured]:                 2 / 2
/^Virtual Systems.* \d+/ {
    # $NF is configured systems
    vs_count = $NF
}

END {
    if (found_vsx == "true") {
        writeTag("vsx", found_vsx)
        if ( vs_count ) {
    	    writeTag("vs-count", vs_count)
    	    writeTag("license-vs-ratio", 10)
  		}
   	}
}