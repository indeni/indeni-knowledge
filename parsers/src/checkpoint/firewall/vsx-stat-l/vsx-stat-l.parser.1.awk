function addVsTags(tags) {
    tags["vs.id"] = vsid
    tags["vs.name"] = vsname
    tags["vs.type"] = type
}

BEGIN {
    FS = ":"
    vsid = ""
    vsname = ""
    type = ""
    policy = ""
}

#VSID:            0
/VSID:/ {
    vsid = trim($NF)
}

#Name:            VSX-CXL1-Metal
/Name:/ {
    vsname = trim($NF)
}

#Type:            VSX Gateway
#Type:            Virtual Switch
#Type:            Virtual System
/Type/ {
    type = trim($NF)
}

#Security Policy: Standard
#Security Policy: <Not Applicable>
/Security Policy/ {
    policy = trim($NF)
}


#Connections number: 2337
/Connections number.*\d/ {
    connections = trim($NF)
    # Connection data only applicable for elements of the VSX that have a policy
    if (policy != "") {
        addVsTags(t)
        writeDoubleMetric("concurrent-connections", t, "gauge", connections, "true", "Connections - Current", "number", "vs.id|vs.name")  # Converted to new syntax by change_ind_scripts.py script
    }
}

#Connections limit:  14900
/Connections limit.*\d/ {
    connection_limit = trim($NF)
    # Connection data only applicable for elements of the VSX that have a policy and if the limit is not zero.
    if (policy != "" && connection_limit != "0") {
        addVsTags(tags)
        writeDoubleMetric("concurrent-connections-limit", tags, "gauge", connection_limit, "true", "Connections - Capacity", "number", "vs.id|vs.name")  # Converted to new syntax by change_ind_scripts.py script
		writeComplexMetricString("concurrent-connections-limit-snapshot", tags, connection_limit, "false")
    }
}