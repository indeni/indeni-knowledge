from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class CheckpointIpsStatGw(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data = helper_methods.parse_data_as_list(raw_data, 'ips_stat_gw.textfsm')
            if data:
                if data[0].get('bypass'):
                    self.write_double_metric('ips-bypass', {}, 'gauge', 1 if data[0]['bypass'] == 'On' else 0, True)
        return self.output