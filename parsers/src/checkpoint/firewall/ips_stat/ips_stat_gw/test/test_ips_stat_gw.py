import os
import unittest
from checkpoint.firewall.ips_stat.ips_stat_gw.ips_stat_gw import CheckpointIpsStatGw
from parser_service.public.action import *

class TestCheckpointIpsStatGw(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = CheckpointIpsStatGw()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_bypass_on(self):
        result = self.parser.parse_file(self.current_dir + '/bypass_on.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].name, 'ips-bypass')
        self.assertEqual(result[0].value, 1)

    def test_bypass_off(self):
        result = self.parser.parse_file(self.current_dir + '/bypass_off.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].name, 'ips-bypass')
        self.assertEqual(result[0].value, 0)

    def test_blade_disabled(self):
        result = self.parser.parse_file(self.current_dir + '/blade_disabled.input', {}, {})
        self.assertEqual(0, len(result))

if __name__ == '__main__':
    unittest.main()