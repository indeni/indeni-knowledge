import os
import unittest
from checkpoint.firewall.ips_stat.ips_stat_vsx.ips_stat_vsx import CheckpointIpsStatVsx
from parser_service.public.action import *

class TestCheckpointIpsStatVsx(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = CheckpointIpsStatVsx()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_ips_stat_vsx(self):
        result = self.parser.parse_file(self.current_dir + '/ips_stat_vsx.input', {}, {})
        self.assertEqual(2, len(result))
        self.assertEqual(result[0].name, 'ips-bypass')
        self.assertEqual(result[0].tags['vs.id'], '0')
        self.assertEqual(result[0].tags['vs.name'], 'VSX-1')
        self.assertEqual(result[0].value['value'], 1)
        self.assertEqual(result[1].name, 'ips-bypass')
        self.assertEqual(result[1].tags['vs.id'], '1')
        self.assertEqual(result[1].tags['vs.name'], 'VS-1-R81')
        self.assertEqual(result[1].value['value'], 0)

if __name__ == '__main__':
    unittest.main()