import os
import unittest
from checkpoint.firewall.ips_stat.ips_stat_sp.ips_stat_sp import CheckpointIpsStatSp
from parser_service.public.action import *

class TestCheckpointIpsStatSp(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = CheckpointIpsStatSp()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_asg_ips_stat_sp(self):
        result = self.parser.parse_file(self.current_dir + '/ips_stat_sp.input', {}, {})
        self.assertEqual(2, len(result))
        self.assertEqual(result[0].name, 'ips-bypass')
        self.assertEqual(result[0].tags['vs.id'], '0')
        self.assertEqual(result[0].tags['vs.name'], 'VSX-1')
        self.assertEqual(result[0].value['value'], 0)
        self.assertEqual(result[1].name, 'ips-bypass')
        self.assertEqual(result[1].tags['vs.id'], '1')
        self.assertEqual(result[1].tags['vs.name'], 'VS-1-R81')
        self.assertEqual(result[1].value['value'], 1)

if __name__ == '__main__':
    unittest.main()