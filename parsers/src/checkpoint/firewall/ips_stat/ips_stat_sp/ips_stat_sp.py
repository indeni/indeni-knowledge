from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser
import re

class CheckpointIpsStatGw(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data = helper_methods.parse_data_as_list(raw_data, 'ips_stat_gw.textfsm')
            if data:
                if data[0].get('bypass'):
                    self.write_complex_metric_string('ips-bypass', {}, 1 if data[0]['bypass'] == 'On' else 0, True)
        return self.output

class CheckpointIpsStatSp(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            vs_data = helper_methods.parse_data_as_list(raw_data, 'ips_stat_sp.textfsm')
            blade_vs_regex = r'(^\d+)\_(\d+)\:?(\d+)?'
            outputs = re.split(blade_vs_regex, raw_data, 0, re.MULTILINE)
            outputs.pop(0)
            id = 0
            while id < len(outputs):
                blade_metrics = CheckpointIpsStatGw.parse(CheckpointIpsStatGw() ,outputs[id+3], {}, {})
                for metric in blade_metrics:
                    if outputs[id+2] is not None:
                        metric.tags['vs.id'] = outputs[id+2]
                        metric.tags['vs.name'] = [vs_info['vs_name'] for vs_info in vs_data if vs_info['vs_id']== outputs[id+2]][0]
                        if outputs[id+1] != '99':
                            metric.tags['name'] = ' '.join(('VSID_{}'.format(outputs[id+2]), metric.tags['name']))
                    if outputs[id+1] is not None and outputs[id+1] != '99':
                        metric.tags['blade'] = outputs[id+1]
                        metric.tags['name'] = ' '.join(('blade_{}'.format(outputs[id+1]), metric.tags['name']))
                    if metric.name == 'ips-bypass':
                        self.write_double_metric('ips-bypass', metric.tags, 'gauge', metric.value, True)
                id += 4
        return self.output