from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class ChkpFwFastAccelTableVsx(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        data_parsed = helper_methods.parse_data_as_list(raw_data, 'fw_ctl_fast_accel_show_table_vsx.textfsm')
        if data_parsed:
            vs_list=list(dict.fromkeys([entry['vs_id'] for entry in data_parsed]))
            for vs in vs_list:
                vs_rules = [x for x in data_parsed if x['vs_id']==vs]
                if len(vs_rules[0]['src']) > 0:
                    self.write_double_metric('fast-accel-table-status', {'vs.id': vs_rules[0]['vs_id'], 'vs.name': vs_rules[0]['vs_name']}, 'gauge', 1, False, 'state')
                    self.write_complex_metric_object_array('fast-accel-table-entries', {'vs.id': vs_rules[0]['vs_id'], 'vs.name': vs_rules[0]['vs_name'], 'im.identity-tags': 'vs.id|vs.name'}, vs_rules, True, 'Fast Acceleration Table')
                else:
                    self.write_double_metric('fast-accel-table-status', {'vs.id': vs_rules[0]['vs_id'], 'vs.name': vs_rules[0]['vs_name']}, 'gauge', 0, False, 'state', 'vs.id')
                    self.write_complex_metric_object_array('fast-accel-table-entries', {'vs.id': vs_rules[0]['vs_id'], 'vs.name': vs_rules[0]['vs_name'], 'im.identity-tags': 'vs.id|vs.name'}, [], True, 'Fast Acceleration Table')
        return self.output
