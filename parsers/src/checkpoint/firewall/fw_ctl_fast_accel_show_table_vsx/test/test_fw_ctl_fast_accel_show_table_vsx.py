import os
import unittest

from checkpoint.firewall.fw_ctl_fast_accel_show_table_vsx.fw_ctl_fast_accel_show_table_vsx import ChkpFwFastAccelTableVsx
from parser_service.public.action import *

class TestChkpFwFastAccelTableVsx(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = ChkpFwFastAccelTableVsx()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_fw_ctl_fast_accel_rules_in_table(self):
        expected_results = [{'vs.id': '0', 'vs.name': 'VSX1-1',  'value': 1, 'name': 'fast-accel-table-status'},
                                        {'vs.id': '0', 'vs.name': 'VSX1-1', 'value': [{'vs_id': '0', 'vs_name': 'VSX1-1',  'src': 'any', 'dst': '192.168.3.0/16', 'port': '16', 'protocol': '17' },
                                                                                                        {'vs_id': '0', 'vs_name': 'VSX1-1',  'src': '1.1.1.1/32', 'dst': '2.2.2.0/24', 'port': '80', 'protocol': '6' },
                                                                                                        {'vs_id': '0', 'vs_name': 'VSX1-1',  'src': '5.5.5.5/32', 'dst': '2.2.2.0/24', 'port': '80', 'protocol': '6',}], 'name': 'fast-accel-table-entries'},
                                        {'vs.id': '1', 'vs.name': 'VS_1_R80.40', 'value': 1, 'name': 'fast-accel-table-status'},
                                        {'vs.id': '1', 'vs.name': 'VS_1_R80.40', 'value': [{'vs_id': '1', 'vs_name': 'VS_1_R80.40',  'src': '255.0.0.0/8', 'dst': '255.240.0.0/12', 'port': '16', 'protocol': 'any' },
                                                                                                        {'vs_id': '1', 'vs_name': 'VS_1_R80.40',  'src': '192.168.3.0/16', 'dst': 'any', 'port': '16', 'protocol': '17'},
                                                                                                        {'vs_id': '1', 'vs_name': 'VS_1_R80.40',  'src': '9.9.9.9/32', 'dst': '2.2.2.0/24', 'port': '80', 'protocol': '6'},
                                                                                                        {'vs_id': '1', 'vs_name': 'VS_1_R80.40', 'src': '7.7.7.7/32', 'dst': '2.2.2.0/24', 'port': '80', 'protocol': '6' }], 'name': 'fast-accel-table-entries'}]

        # Act
        result = self.parser.parse_file(self.current_dir + '/fw_ctl_fast_accel_rules_in_table.input', {}, {})
        # Assert
        self.assertEqual(4, len(result))

        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i]['name'], result[i].name)
            self.assertEqual(expected_results[i]['value'], result[i].value)
            self.assertEqual(expected_results[i]['vs.id'], result[i].tags['vs.id'])
            self.assertEqual(expected_results[i]['vs.name'], result[i].tags['vs.name'])


    def test_fw_ctl_fast_accel_no_rules_in_table_2_vs(self):
        expected_results = [{'vs.id': '0', 'vs.name': 'VSX1-1', 'value': 0, 'name': 'fast-accel-table-status'},
                                        {'vs.id': '0', 'vs.name': 'VSX1-1', 'value': [], 'name': 'fast-accel-table-entries'},
                                        {'vs.id': '1', 'vs.name': 'VS_1_R80.40', 'value': 0, 'name': 'fast-accel-table-status'},
                                        {'vs.id': '1', 'vs.name': 'VS_1_R80.40', 'value': [], 'name': 'fast-accel-table-entries'}]
        # Act
        result = self.parser.parse_file(self.current_dir + '/fw_ctl_fast_accel_no_rules_in_table_2_vs.input', {}, {})
        # Assert
        self.assertEqual(4, len(result))

        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i]['name'], result[i].name)
            self.assertEqual(expected_results[i]['value'], result[i].value)
            self.assertEqual(expected_results[i]['vs.id'], result[i].tags['vs.id'])
            self.assertEqual(expected_results[i]['vs.name'], result[i].tags['vs.name'])


    def test_fw_ctl_fast_accel_2_vs_no_rules_in_1_vs(self):
        expected_results = [{'vs.id': '0', 'vs.name': 'VSX1-1', 'value': 0, 'name': 'fast-accel-table-status'},
                                        {'vs.id': '0', 'vs.name': 'VSX1-1', 'value': [], 'name': 'fast-accel-table-entries'},
                                        {'vs.id': '1', 'vs.name': 'VS_1_R80.40', 'value': 1, 'name': 'fast-accel-table-status'},
                                        {'vs.id': '1', 'vs.name': 'VS_1_R80.40', 'value': [{'vs_id': '1', 'vs_name': 'VS_1_R80.40', 'src': '255.0.0.0/8', 'dst': '255.240.0.0/12', 'port': '16', 'protocol': 'any'},
                                                                                                                {'vs_id': '1', 'vs_name': 'VS_1_R80.40', 'src': '192.168.3.0/16', 'dst': 'any', 'port': '16', 'protocol': '17'},
                                                                                                                {'vs_id': '1', 'vs_name': 'VS_1_R80.40', 'src': '9.9.9.9/32', 'dst': '2.2.2.0/24', 'port': '80', 'protocol': '6'},
                                                                                                                {'vs_id': '1', 'vs_name': 'VS_1_R80.40', 'src': '7.7.7.7/32', 'dst': '2.2.2.0/24', 'port': '80', 'protocol': '6'}], 'name': 'fast-accel-table-entries'}]

        # Act
        result = self.parser.parse_file(self.current_dir + '/fw_ctl_fast_accel_2_vs_no_rules_in_1_vs.input', {}, {})
        # Assert
        self.assertEqual(4, len(result))

        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i]['name'], result[i].name)
            self.assertEqual(expected_results[i]['value'], result[i].value)
            self.assertEqual(expected_results[i]['vs.id'], result[i].tags['vs.id'])
            self.assertEqual(expected_results[i]['vs.name'], result[i].tags['vs.name'])

if __name__ == '__main__':
    unittest.main()
