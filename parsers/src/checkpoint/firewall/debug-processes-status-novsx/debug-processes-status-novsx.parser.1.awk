# .elg files listed if were modified (something wrote in file or created) last during last 60 minutes (-mmin option)
# number in file name is deleted (indicates the order into the log rotation)
# files ordered to check the number of files of each proces, number of files printed before file name

#      1 /opt/CPshrd-R80.20/log/cpd.elg
#      1 /opt/CPshrd-R80.20/log/cpwd.elg
#      1 /opt/CPsuite-R80.20/fw1/log/cplog_debug.elg
#      5 /opt/CPsuite-R80.20/fw1/log/fwd.elg


/.elg$/{
    tags["filename"] = $2
    log_files = $1

    writeDoubleMetric("debug-file-counter", tags, "gauge", log_files, "false")
}
