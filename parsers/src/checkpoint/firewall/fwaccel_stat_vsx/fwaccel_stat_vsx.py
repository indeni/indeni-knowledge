from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class FwaccelStatVsx(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            parsed_data_status = helper_methods.parse_data_as_list(raw_data, 'fwaccel_stat_status_vsx.textfsm')
            if parsed_data_status:
                tags = {}
                for virtual_system in parsed_data_status:
                    if  virtual_system['name']:
                        tags = {"name": virtual_system['name'], 'im.identity-tags': 'name', 'im.dstype.displaytype': 'string'}
                    else:
                        tags = {"name": 'Firewall', 'im.identity-tags': 'name', 'im.dstype.displaytype': 'string'}
                    tags['vs.id'] = virtual_system['vsid']
                    tags['vs.name'] = virtual_system['vsname']
                    tags['im.identity-tags'] = 'vs.id|vs.name'
                    self.write_complex_metric_string('securexl-status', tags, virtual_system['accelerator_status'], True, 'SecureXL - Status')

                parsed_data_templates = helper_methods.parse_data_as_list(raw_data, 'fwaccel_stat_disabled_template_vsx.textfsm')
                if parsed_data_templates:
                    for template in parsed_data_templates:
                        tags = {}
                        tags['vs.id'] = template['vsid']
                        tags['vs.name'] = template['vsname']
                        tags['type'] = template['template_type']
                        if template['template_layer']:
                            tags['layer'] = template['template_layer']
                        if template['template_disabled_from_rule']:
                            tags['disabledfromrule'] = template['template_disabled_from_rule']
                        tags['im.identity-tags'] = 'vs.id|vs.name|type'
                        if tags['type'] == 'Accept':
                            self.write_complex_metric_string('securexl-accept-template-status', tags, template['template_status'], True, 'SecureXL - Templates')
                        if tags['type'] == 'Drop':
                            self.write_complex_metric_string('securexl-drop-template-status', tags, template['template_status'], True, 'SecureXL - Templates')
                        if tags['type'] == 'NAT':
                            self.write_complex_metric_string('securexl-nat-template-status', tags, template['template_status'], True, 'SecureXL - Templates')
                        if tags['type'] == 'NMR':
                            self.write_complex_metric_string('securexl-nmr-template-status', tags, template['template_status'], True, 'SecureXL - Templates')
                        if tags['type'] == 'NMT':
                            self.write_complex_metric_string('securexl-nmt-template-status', tags, template['template_status'], True, 'SecureXL - Templates')
        return self.output
