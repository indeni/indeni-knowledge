from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class CpstatIdentityServerLoginsLdap(BaseParser):
    def unsuccess_ratio_calc(self, success: int, unsuccess: int) -> int:
        unsuccess_ratio = (unsuccess / (unsuccess + success) ) * 100
        return unsuccess_ratio
    
    def data_check(self, data: str) -> bool:
        if data and int(data) != 0:
            return True

    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        # Step 1 : Data Extraction
        if raw_data and len(raw_data) > 1:
            # Checking if the device is VSX, to determine best code:
            unsuccessful_logins_ratio = 0
            unsuccessful_ldap_ratio = 0
            if device_tags.get('vsx') == 'true':
                basic = raw_data.split('_VSID:Name:Type ')

                for vs_data in basic:
                    logins_data = helper_methods.parse_data_as_object(vs_data, 'checkpoint_gaia_cpstat_identity_server_logins.textfsm')
                    vs_metadata = helper_methods.parse_data_as_object(vs_data, 'vs_metadata_parse.textfsm')
                    if vs_metadata and logins_data:
                        if vs_metadata['vs_type'] != 'Switch' and vs_metadata['vs_type'] != 'Router':
                            tags = {'name': 'vs.id - ' + vs_metadata['vs_id'] + ' vs.name - ' + vs_metadata['name']}
                            #Here we are calulating unsuccesful login using username and password ratio
                            if self.data_check(logins_data['success_login']) and self.data_check(logins_data['unsuccess_login']):
                                unsuccessful_logins_ratio = self.unsuccess_ratio_calc(int(logins_data['success_login']), int(logins_data['unsuccess_login']))
                                self.write_double_metric('unsuccesful-login-ratio-username-pass', tags, 'gauge', unsuccessful_logins_ratio, False)
                            
                            #Here we are calulating unsuccesful LDAP Queries ratio prior to R81
                            if 'Successful LDAP Queries' in vs_data:
                                if self.data_check(logins_data['success_ldap']) and self.data_check(logins_data['unsuccess_ldap']):
                                    unsuccessful_ldap_ratio = self.unsuccess_ratio_calc(int(logins_data['success_ldap']), int(logins_data['unsuccess_ldap']))
                                    self.write_double_metric('unsuccessful-ldap-queries-ratio', tags, 'gauge', unsuccessful_ldap_ratio, False)
                            
                            #Here we are calulating unsuccesful LDAP Queries for R81 and above
                            else:
                                if self.data_check(logins_data['success_directory']) and self.data_check(logins_data['unsuccess_directory']):
                                    unsuccessful_ldap_ratio = self.unsuccess_ratio_calc(int(logins_data['success_directory']), int(logins_data['unsuccess_directory']))
                                    self.write_double_metric('unsuccessful-ldap-queries-ratio', tags, 'gauge', unsuccessful_ldap_ratio, False)
            # If the device is not VSX:
            else:
                logins_data = helper_methods.parse_data_as_object(raw_data, 'checkpoint_gaia_cpstat_identity_server_logins.textfsm')
                if logins_data:
                    #Here we are calulating unsuccesful login using username and password ratio
                    if self.data_check(logins_data['success_login']) and self.data_check(logins_data['unsuccess_login']):
                        unsuccessful_logins_ratio = self.unsuccess_ratio_calc(int(logins_data['success_login']), int(logins_data['unsuccess_login']))
                        self.write_double_metric('unsuccesful-login-ratio-username-pass', {}, 'gauge', unsuccessful_logins_ratio, False)
                    
                    #Here we are calulating unsuccesful LDAP Queries ratio prior to R81
                    if 'Successful LDAP Queries' in raw_data:
                        if self.data_check(logins_data['success_ldap']) and self.data_check(logins_data['unsuccess_ldap']):
                            unsuccessful_ldap_ratio = self.unsuccess_ratio_calc(int(logins_data['success_ldap']), int(logins_data['unsuccess_ldap']))
                            self.write_double_metric('unsuccessful-ldap-queries-ratio', {}, 'gauge', unsuccessful_ldap_ratio, False)
                    
                    #Here we are calulating unsuccesful LDAP Queries for R81 and above
                    else:
                        if self.data_check(logins_data['success_directory']) and self.data_check(logins_data['unsuccess_directory']):
                            unsuccessful_ldap_ratio = self.unsuccess_ratio_calc(int(logins_data['success_directory']), int(logins_data['unsuccess_directory']))
                            self.write_double_metric('unsuccessful-ldap-queries-ratio', {}, 'gauge', unsuccessful_ldap_ratio, False)
        return self.output
