import os
import unittest
 
from checkpoint.firewall.cpstat_identity_server_logins_ldap.parser.cpstat_identity_server_logins_ldap import CpstatIdentityServerLoginsLdap
from parser_service.public.action import *

class TestCpstatIdentityServerLoginsLdap(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = CpstatIdentityServerLoginsLdap()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_valid_R81_vsx_report_ratio(self):
        expected_results = [{'name': 'vs.id - 0 vs.name - VSX-1', 'metric': 'unsuccesful-login-ratio-username-pass', 'value': 71.42857142857143},
                            {'name': 'vs.id - 2 vs.name - VS-2-R81', 'metric': 'unsuccesful-login-ratio-username-pass', 'value': 19.230769230769234},
                            {'name': 'vs.id - 2 vs.name - VS-2-R81', 'metric': 'unsuccessful-ldap-queries-ratio', 'value': 22.22222222222222}]

        # Act
        result = self.parser.parse_file(self.current_dir + '/valid_R81_vsx_report_ratio.input', {}, {'vsx': 'true'})
        # Assert
        self.assertEqual(3, len(result))

        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i]['name'], result[i].tags['name'])
            self.assertEqual(expected_results[i]['metric'], result[i].name)
            self.assertEqual(expected_results[i]['value'], result[i].value)


    def test_valid_sample_output_above_threshold_value_vsx(self):
        expected_results = [{'name': 'vs.id - 0 vs.name - CP-R80.20-VSX1-1', 'metric': 'unsuccesful-login-ratio-username-pass', 'value': 43.13725490196079},
                            {'name': 'vs.id - 0 vs.name - CP-R80.20-VSX1-1', 'metric': 'unsuccessful-ldap-queries-ratio', 'value': 12.173913043478262},
                            {'name': 'vs.id - 2 vs.name - VS1', 'metric': 'unsuccesful-login-ratio-username-pass', 'value': 44.230769230769226},
                            {'name': 'vs.id - 2 vs.name - VS1', 'metric': 'unsuccessful-ldap-queries-ratio', 'value': 11.790393013100436},
                            {'name': 'vs.id - 3 vs.name - VS2', 'metric': 'unsuccesful-login-ratio-username-pass', 'value': 40.816326530612244},
                            {'name': 'vs.id - 3 vs.name - VS2', 'metric': 'unsuccessful-ldap-queries-ratio', 'value': 11.403508771929824}]

        # Act
        result = self.parser.parse_file(self.current_dir + '/valid_sample_output_above_threshold_value_vsx.input', {}, {'vsx': 'true'})
        # Assert
        self.assertEqual(6, len(result))

        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i]['name'], result[i].tags['name'])
            self.assertEqual(expected_results[i]['metric'], result[i].name)
            self.assertEqual(expected_results[i]['value'], result[i].value)


    def test_valid_sample_output_above_threshold_value(self):
        expected_results = [{'name': None, 'metric': 'unsuccesful-login-ratio-username-pass', 'value': 40.816326530612244},
                            {'name': None, 'metric': 'unsuccessful-ldap-queries-ratio', 'value': 10.222222222222223}]

        # Act
        result = self.parser.parse_file(self.current_dir + '/valid_sample_output_above_threshold_value.input', {}, {})
        # Assert
        self.assertEqual(2, len(result))

        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i]['name'], result[i].tags.get('name'))
            self.assertEqual(expected_results[i]['metric'], result[i].name)
            self.assertEqual(expected_results[i]['value'], result[i].value)


    def test_valid_sample_output_below_threshold_value_vsx(self):
        expected_results = [{'name': 'vs.id - 0 vs.name - CP-R80.20-VSX1-1', 'metric': 'unsuccesful-login-ratio-username-pass', 'value': 39.21568627450981},
                            {'name': 'vs.id - 0 vs.name - CP-R80.20-VSX1-1', 'metric': 'unsuccessful-ldap-queries-ratio', 'value': 9.821428571428571},
                            {'name': 'vs.id - 2 vs.name - VS1', 'metric': 'unsuccesful-login-ratio-username-pass', 'value': 38.0},
                            {'name': 'vs.id - 2 vs.name - VS1', 'metric': 'unsuccessful-ldap-queries-ratio', 'value': 9.00900900900901},
                            {'name': 'vs.id - 3 vs.name - VS2', 'metric': 'unsuccesful-login-ratio-username-pass', 'value': 36.734693877551024},
                            {'name': 'vs.id - 3 vs.name - VS2', 'metric': 'unsuccessful-ldap-queries-ratio', 'value': 8.597285067873303}]

        # Act
        result = self.parser.parse_file(self.current_dir + '/valid_sample_output_below_threshold_value_vsx.input', {}, {'vsx': 'true'})
        # Assert
        self.assertEqual(6, len(result))

        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i]['name'], result[i].tags.get('name'))
            self.assertEqual(expected_results[i]['metric'], result[i].name)
            self.assertEqual(expected_results[i]['value'], result[i].value)


    def test_valid_sample_output_below_threshold_value(self):
        expected_results = [{'name': None, 'metric': 'unsuccesful-login-ratio-username-pass', 'value': 39.21568627450981},
                            {'name': None, 'metric': 'unsuccessful-ldap-queries-ratio', 'value': 9.821428571428571}]

        # Act
        result = self.parser.parse_file(self.current_dir + '/valid_sample_output_below_threshold_value.input', {}, {})
        # Assert
        self.assertEqual(2, len(result))

        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i]['name'], result[i].tags.get('name'))
            self.assertEqual(expected_results[i]['metric'], result[i].name)
            self.assertEqual(expected_results[i]['value'], result[i].value)


    def test_valid_sample_output_with_null_value_vsx(self):
        expected_results = [{'name': 'vs.id - 0 vs.name - CP-R80.20-VSX1-1', 'metric': 'unsuccessful-ldap-queries-ratio', 'value': 9.821428571428571},
                            {'name': 'vs.id - 2 vs.name - VS1', 'metric': 'unsuccesful-login-ratio-username-pass', 'value': 38.0},
                            {'name': 'vs.id - 2 vs.name - VS1', 'metric': 'unsuccessful-ldap-queries-ratio', 'value': 9.00900900900901}]

        # Act
        result = self.parser.parse_file(self.current_dir + '/valid_sample_output_with_null_value_vsx.input', {}, {'vsx': 'true'})
        # Assert
        self.assertEqual(3, len(result))

        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i]['name'], result[i].tags.get('name'))
            self.assertEqual(expected_results[i]['metric'], result[i].name)
            self.assertEqual(expected_results[i]['value'], result[i].value)


    def test_valid_sample_output_with_null_value(self):
        expected_results = []

        # Act
        result = self.parser.parse_file(self.current_dir + '/valid_sample_output_with_null_value.input', {}, {})
        # Assert
        self.assertEqual(0, len(result))


    def valid_zero_value_R80_30_data(self):
        expected_results = []

        # Act
        result = self.parser.parse_file(self.current_dir + '/valid_zero_value_R80_30_data.input', {}, {})
        # Assert
        self.assertEqual(0, len(result))


if __name__ == '__main__':
    unittest.main()
