# cphaprob list only reports the devices with problems. This means that if there is a problem and it's solved indeni would
# alert like it should, but if the issue heals itself indeni would not not be able to auto-resolve the alert.
#
# To solve this we've added the -l switch to the command section. However, since -l is not available on SPLAT we need to run
# both "cphaprob list" and "cphaprob -l list" to account for both versions.
# As a consequence of that some information would be echoed twice on platforms supporting both commands.
# A verification has been added to avoid the same pnote state being written twice.
#
# We will also set the device state to 1 (true) in order to clear the pnote in case it was missed.

BEGIN {

    # Define a list of device names for which we _always_ want to report a state. Here is why: if we get a pnote, then
    # create an alert, if the device name 'disappears' from the 'cphaprob list' output, the alert will never resolve.
    # We've seen this happen for certain devices in production.
    # See: https://indeni.atlassian.net/browse/IKP-1748
    known_critical_devices["Problem Notification"] = ""
    known_critical_devices["HA Initialization"] = ""
    known_critical_devices["Interface Active Check"] = ""
    known_critical_devices["Load Balancing Configuration"] = ""
    known_critical_devices["Recovery Delay"] = ""
    known_critical_devices["Synchronization"] = ""
    known_critical_devices["Filter"] = ""
    known_critical_devices["fwd"] = ""
    known_critical_devices["cphad"] = ""
    known_critical_devices["routed"] = ""
    known_critical_devices["FIB"] = ""
    known_critical_devices["cvpnd"] = ""
    known_critical_devices["ted"] = ""
    known_critical_devices["VSX"] = ""
    known_critical_devices["Instances"] = ""
    known_critical_devices["admin_down"] = ""
    known_critical_devices["host_monitor"] = ""
    known_critical_devices["failover"] = ""
}

#Device Name: admin_down
/^Device Name:\s/ {
    device_name = $0
    sub(/^Device Name:\s/, "", device_name)
}

#Current state: OK
#Current state: problem (non-blocking)
#Current state: problem
/^Current state:\s/ {

    state_description = $3

    state = 1
    if (state_description == "problem")
        state = 0

    if (!(device_name in states)) {
        states[device_name] = state
    }
}

END {

    for (device_name in states) {

        tags["name"] = device_name
        state = states[device_name]

        writeDoubleMetric("clusterxl-pnote-state", tags, "gauge", state, "true", "ClusterXL Devices", "state", "name")  # Converted to new syntax by change_ind_scripts.py script
    }

    for (critical_name in known_critical_devices) {
        # If we haven't already reported the state of a 'known critical device', report an OK state here.
        if (!(critical_name in states)) {
            tags["name"] = critical_name
            writeDoubleMetric("clusterxl-pnote-state", tags, "gauge", 1, "false")  # Converted to new syntax by change_ind_scripts.py script
        }
    }
}