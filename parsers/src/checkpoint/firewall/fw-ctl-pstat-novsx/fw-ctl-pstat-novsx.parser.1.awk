BEGIN {
    devicename = ""
}

#  Memory used: 20% (871 MB out of 4163 MB) - below watermark

/^  Memory used.*out of.*/ {
    usage_ref = $3
    usage = substr(usage_ref, 1, length(usage_ref)-1)
    writeDoubleMetric("kernel-memory-usage", null, "gauge", usage, "true", "Kernel Memory", "percentage", "")  # Converted to new syntax by change_ind_scripts.py script
}

/Kernel\s+memory\s+used.*out of.*/ {
    usage_ref = $4
    usage = substr(usage_ref, 1, length(usage_ref)-1)
    writeDoubleMetric("kernel-memory-usage", null, "gauge", usage, "true", "Kernel Memory", "percentage", "")  # Converted to new syntax by change_ind_scripts.py script
}

# Aggressive Aging is disabled
# Aggressive Aging is enabled, not active
# Aggressive Aging is not active
# the following should trigger the issue
# Aggressive Aging is active
# Aggressive Aging is enabled, active
# Aggressive Aging is enabled, in detect mode
/^  Aggressive Aging/ {
    chkp_agressive_aging = 0
    message = trim($0)
    if ((message ~ "is active") || (message ~ "is enabled, active") || ( message ~ "in detect mode")) {
        chkp_agressive_aging = 1
    }
    writeDoubleMetric("chkp-agressive-aging", null, "gauge", chkp_agressive_aging, "false")  # Converted to new syntax by change_ind_scripts.py script
}

#  Concurrent Connections: 13 (Unlimited)
#  Concurrent Connections: 0% (5 out of 24900) - below watermark

/^  Concurrent Connections: 0\% \(0 out of 1\)/ {
    next
}

/^  Concurrent Connections:/ {
    if ($NF == "(Unlimited)") {
        connections = $3
    } else {
        connections = $4
        sub(/\(/,"",connections)
        connection_limit = $7
        sub(/\)/,"",connection_limit)
        connection_usage = (connections/connection_limit)
        writeDoubleMetric("concurrent-connections-usage", null, "gauge", connection_usage, "true", "Concurrent Connections Usage", "number", "")  # Converted to new syntax by change_ind_scripts.py script
        writeDoubleMetric("concurrent-connections-limit", null, "gauge", connection_limit, "true", "Concurrent Connections Limit", "number", "")  # Converted to new syntax by change_ind_scripts.py script
		writeComplexMetricString("concurrent-connections-limit-snapshot", null, connection_limit, "false")
    }
    writeDoubleMetric("concurrent-connections", null, "gauge", connections, "true", "Concurrent Connections", "number", "")  # Converted to new syntax by change_ind_scripts.py script
}