function writePnoteStates() {

    tags["vs.id"] = vs_id
    tags["vs.name"] = vs_name

    if (tolower(vs_type) != "switch") {
        for (device_name in states) {
            tags["name"] = device_name
            writeDoubleMetric("clusterxl-pnote-state", tags, "gauge", states[device_name], "true", "ClusterXL Devices", "state", "name|vs.id|vs.name")  # Converted to new syntax by change_ind_scripts.py script
        }

        # If we haven't already reported the state of a 'known critical device', report an OK state here.
        for (critical_name in known_critical_devices) {
            if (!(critical_name in states)) {
                tags["name"] = critical_name
                writeDoubleMetric("clusterxl-pnote-state", tags, "gauge", 1, "true", "ClusterXL Devices", "state", "name|vs.id|vs.name")  # Converted to new syntax by change_ind_scripts.py script
            }
        }
    }
}

# Parse out the cluster state, starting from the end of the line -- handles multi-word state value. E.g.,
#2          10.11.94.11     0%              ClusterXL Inactive or Machine is Down
function getClusterState(lastStateField, lastStateFieldNum){
    buildState = lastStateField
    for (field = lastStateFieldNum - 1 ; field > 2; field--) {
        if (index($field, "%"))
            break
        else
            buildState = $field " " buildState
    }
    return buildState
}

BEGIN {
    vs_id = ""
    vs_name = ""
    vs_type = ""
    device_name = ""


    # Define a list of device names for which we _always_ want to report a state. Here is why: if we get a pnote, then
    # create an alert, if the device name 'disappears' from the 'cphaprob list' output, the alert will never resolve.
    # We've seen this happen for certain devices in production.
    # See: https://indeni.atlassian.net/browse/IKP-1748
    known_critical_devices["Problem Notification"] = ""
    known_critical_devices["HA Initialization"] = ""
    known_critical_devices["Interface Active Check"] = ""
    known_critical_devices["Load Balancing Configuration"] = ""
    known_critical_devices["Recovery Delay"] = ""
    known_critical_devices["Synchronization"] = ""
    known_critical_devices["Filter"] = ""
    known_critical_devices["fwd"] = ""
    known_critical_devices["cphad"] = ""
    known_critical_devices["routed"] = ""
    known_critical_devices["FIB"] = ""
    known_critical_devices["cvpnd"] = ""
    known_critical_devices["ted"] = ""
    known_critical_devices["VSX"] = ""
    known_critical_devices["Instances"] = ""
    known_critical_devices["admin_down"] = ""
    known_critical_devices["host_monitor"] = ""
    known_critical_devices["failover"] = ""
}

#_VSID:Name 3:lab-CP-VSX1_vsx-2

/^_VSID:Name:Type/ {
    # Dump data of previous VS if needed
    if (vs_id != "") {
        writePnoteStates()

        # clear the states from the previous vs_id
        delete states
        cluster_state = ""
    }
    split($2, id_and_name, ":")
    vs_id = id_and_name[1]
    vs_name = id_and_name[2]
    vs_type = id_and_name[3]

    next
}

#Device Name: admin_down
/Device Name/ {
    device_name = trim(substr($0, index($0, ":")+1))
    next
}

#Current state: OK
#Current state: problem (non-blocking)
#Current state: problem
/Current state/ {
    state_desc = $3
    state = 0
    if (state_desc != "problem") {
        state = 1
    }

    states[device_name] = state
}


# This is R80.20 output. We use this info later to correctly parse out the state column values.
#ID         Unique Address  Assigned Load   State          Name
/^ID/{
    name_index = index($0, "Name")
}

# This is < R80.20 output
#Number     Unique Address  Assigned Load   State
/^Number/ {
    name_index = -1
}

# Pre-R80.20, there is no member 'Name' column
#1          192.168.252.253 0%              Standby
#2 (local)  10.11.94.11     100%            Active
#2          10.11.94.11     0%              ClusterXL Inactive or Machine is Down

# R80.20 has an extra column for cluster member Name
#1 (local)  192.100.102.101 100%            ACTIVE    CP-R80.20-GW8-2
#2          1.1.1.1         0%              ClusterXL Inactive or Machine    CP-R80.20-GW8-1
/\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/ {
    if ($2 == "(local)") {
        line = trim($0)
        # name_index will be greater than zero if its R80.20 output
        if (name_index > 0 ) {
        # Checking if the output has any data in Name column field (R80.20)
            if (length(line) >= name_index){
                cluster_state = getClusterState($(NF - 1), NF - 1)
            } else {
            # This loop handles if Name column field is empty in R80.20 (seen in VSX deployments)
                cluster_state = getClusterState($NF, NF)
            }
        } else {
            cluster_state = getClusterState($NF, NF)  # pre-R80.20
        }
        
        # checking the VS state in the local machine and adding "Synchronization" state to 1 if the local state is "BACKUP
        # Ref - https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk114557
        cluster_state = tolower(cluster_state)
        if (cluster_state == "backup") {
            states["Synchronization"] = 1
        }
    }
}

END  {
    # Write out the states for the last vs_id.
    writePnoteStates()
}