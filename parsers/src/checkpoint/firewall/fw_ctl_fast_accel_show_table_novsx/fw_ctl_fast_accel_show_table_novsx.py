from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class ChkpFwFastAccelTableNoVsx(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        data_parsed = helper_methods.parse_data_as_list(raw_data, 'fw_ctl_fast_accel_show_table_novsx.textfsm')
        if data_parsed:
            self.write_double_metric('fast-accel-table-status', {}, 'gauge', 1, False, 'state')
            self.write_complex_metric_object_array('fast-accel-table-entries', {}, data_parsed, True, 'Fast Acceleration Table')
        else:
            self.write_double_metric('fast-accel-table-status', {}, 'gauge', 0, False, 'state')
            self.write_complex_metric_object_array('fast-accel-table-entries', {}, [], True, 'Fast Acceleration Table')
        return self.output
