import os
import unittest

from checkpoint.firewall.fw_ctl_fast_accel_show_table_novsx.fw_ctl_fast_accel_show_table_novsx import ChkpFwFastAccelTableNoVsx
from parser_service.public.action import *

class TestChkpFwFastAccelTableVsx(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = ChkpFwFastAccelTableNoVsx()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_fw_ctl_fast_accel_rules_in_table(self):
        expected_results = [{'value': 1, 'name': 'fast-accel-table-status'},
                                        {'value': [{'src': '255.0.0.0/8', 'dst': '255.240.0.0/12', 'port': '16', 'protocol': 'any'},
                                                                                                        {'src': '192.168.3.0/16', 'dst': 'any', 'port': '16', 'protocol': '17'},
                                                                                                        {'src': '9.9.9.9/32', 'dst': '2.2.2.0/24', 'port': '80', 'protocol': '6'},
                                                                                                        {'src': '7.7.7.7/32', 'dst': '2.2.2.0/24', 'port': '80', 'protocol': '6'}], 'name': 'fast-accel-table-entries'}]

        # Act
        result = self.parser.parse_file(self.current_dir + '/fw_ctl_fast_accel_rules_in_table.input', {}, {})
        # Assert
        self.assertEqual(2, len(result))

        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i]['name'], result[i].name)
            self.assertEqual(expected_results[i]['value'], result[i].value)

    def test_fw_ctl_fast_accel_no_rules_in_table(self):
        expected_results = [{'value': 0, 'name': 'fast-accel-table-status'},
                                        {'value': [], 'name': 'fast-accel-table-entries'}]

        # Act
        result = self.parser.parse_file(self.current_dir + '/fw_ctl_fast_accel_no_rules_in_table.input', {}, {})
        # Assert
        self.assertEqual(2, len(result))

        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i]['name'], result[i].name)
            self.assertEqual(expected_results[i]['value'], result[i].value)


if __name__ == '__main__':
    unittest.main()