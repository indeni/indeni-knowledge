#fwha_monitor_all_vlan = 0
/^fwha_monitor_all_vlan/ {
    paramtags["name"] = $1
    writeDoubleMetric("firewall-kparam", paramtags, "gauge", $NF, "true", "Kernel Parameters (selected)", "number", "name")  # Converted to new syntax by change_ind_scripts.py script
}
#fwha_mac_magic = 254 new way to adquire info starting on R80.10
/^MAC magic:/ {
    paramtags["name"] = "fwha_mac_magic"
    writeDoubleMetric("firewall-kparam", paramtags, "gauge", $NF, "true", "Kernel Parameters (selected)", "number", "name")  # Converted to new syntax by change_ind_scripts.py script
}

#fwha_vmac_global_param_enabled = 0
/^fwha_vmac_global_param_enabled/ {
    paramtags["name"] = $1
    writeDoubleMetric("firewall-kparam", paramtags, "gauge", $NF, "true", "Kernel Parameters (selected)", "number", "name")
    if ($NF == 0) {
       value = "false"
    } else {
       value = "true"
    }
    writeComplexMetricString("vmac-mode", paramtags, value, "false")   
}