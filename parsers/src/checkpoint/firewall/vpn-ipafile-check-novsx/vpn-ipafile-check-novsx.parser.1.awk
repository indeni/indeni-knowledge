# There are two default lines that triggers warnings. The script excludes them by using "grep -v" when running the command.

#Invalid IP address specification in line 0057
#Line 0001 appears to be a comment because of second parameter(gateway or address)
/Line |line / {
	iline++
	lines[iline, "line"] = $0
}

END {
	writeComplexMetricObjectArray("ipassignment-conf-errors", null, lines, "false")  # Converted to new syntax by change_ind_scripts.py script
}