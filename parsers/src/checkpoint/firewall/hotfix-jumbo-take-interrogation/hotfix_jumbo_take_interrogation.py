from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class HotfixJumboTakeInterrogation(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        # Step 1 : Data Extraction
        extracted_data = helper_methods.parse_data_as_list(raw_data, "hotfix_jumbo_take_interrogation.textfsm")

        def get_key_value(key: str, data: dict):
            if key in data and data[key] != '':
                return data[key]
            return None

        # Step 2 : Data Processing
        # Extracted data would look like this
        # [{'hf_jumbo_take': '189'},
        # {'hf_jumbo_take': '189'},
        # {'hf_jumbo_take': '189'},
        # {'hf_jumbo_take': '189'}]
        if extracted_data is not None and len(extracted_data) != 0:
            # Since one Jumbo HF per device , the Take number will be same on all modules in a device
            single_value = extracted_data[0]
            hf_jumbo_take = get_key_value('hf_jumbo_take', single_value)

            if hf_jumbo_take is not None:
                jumbo_take = hf_jumbo_take
                self.write_tag("hotfix-jumbo-take", jumbo_take)

        # Step 3 : Data Reporting
        # The method must return self.output
        return self.output

# Test your code Here
#helper_methods.print_list(HotfixJumboTakeInterrogation().parse_file("input.json",{}))