BEGIN {
    # Define descriptions for known processes. Descriptions from SK97638
    process_arr["AVI_DEL_TMP_FILES"] = "Shell script (from ‘$FWDIR/bin/‘) that periodically deletes various old temporary Anti-Virus files"
    process_arr["CI_CLEANUP"] = "Shell script (from $FWDIR/bin/) that periodically deletes various old temporary Anti-Virus files"
    process_arr["CI_HTTP_SERVER"] = "HTTP Server for Content Inspection"
    process_arr["CIHS"] = "HTTP Server for Content Inspection"
    process_arr["CLISH"] = "Gaia Clish CLI interface process - Clish process per session."
    process_arr["CLISHD"] = "Gaia Clish CLI interface process - general information for all Clish sessions."
    process_arr["CLONINGD"] = "Cloning Groups daemon"
    process_arr["CONFD"] = "Database and configuration"
    process_arr["CP_FILE_CONVERT"] = "Used to convert various file formats to simple textual format for scanning by the DLP engine."
    process_arr["CP_HTTP_SERVER"] = "HTTP Server for Management Portal (SmartPortal) and for OS WebUI"
    process_arr["CPCA"] = "Check Point Internal Certificate Authority (ICA)"
    process_arr["CPD"] = "Generic process for many Check Point services such as installing and fetching policy and online updates"
    process_arr["CPHACONF"] = "Cluster configuration process - installs the cluster configuration into Check Point kernel on cluster members"
    process_arr["CPHAMCSET"] = "Clustering daemon. Responsible for opening sockets on the NICs in order to allow them to pass multicast traffic CCP to the machine"
    process_arr["CPHAPROB"] = "Process that lists the state of cluster members, cluster interfaces and critical monitored components (pnotes)"
    process_arr["CPHASTART"] = "Starts the cluster and state synchronization"
    process_arr["CPHASTOP"] =  "Stops the cluster and state synchronization"
    process_arr["CPHTTPD"] = "HTTP Server for Management Portal (SmartPortal) and for OS WebUI"
    process_arr["CPLMD"] = "In order to get the data that should be presented in SmartView Tracker, FWM spawns a child process CPLMD, which reads the information from the log file and performs unification (if necessary)"
    process_arr["CPOSD"] = "SMB-specific daemon responsible for OS Networking operations"
    process_arr["CPRID_WD"] = "WatchDog for Check Point Remote Installation Daemon ""cprid""."
    process_arr["CPRID"] = "Check Point Remote Installation Daemon - distribution of packages from SmartUpdate to managed Gateways."
    process_arr["CPSEAD"] = "Responsible for Correlation Unit functionality"
    process_arr["CPSEMD"] = "Responsible for logging into the SmartEvent GUI"
    process_arr["CPSM"] = "Process is responsible for collecting and sending information to SmartView Monitor"
    process_arr["CPSNMPD"] = "Listens on UDP port 260 and is capable of responding to SNMP queries for Check Point OIDs only (under OID .1.3.6.1.4.1.2620)"
    process_arr["CPSTAT_MONITOR"] = "Process is responsible for collecting and sending information to SmartView Monitor"
    process_arr["CPVIEW_HISTORYD"] = "CPView Utility History daemon"
    process_arr["CPVIEWD"] = "CPView utility daemon"
    process_arr["CPVPNPROC"] = "Offload blocking commands from cvpnd (to prevent locks). Example: sending DynamicID."
    process_arr["CPWD"] = "WatchDog is a process that launches and monitors critical processes such as Check Point daemons on the local machine, and attempts to restart them if they fail."
    process_arr["CPWMD"] = "Check Point Web Management Daemon - back-end for Management Portal / SmartPortal"
    process_arr["CTASD"] = "Commtouch Anti-Spam daemon"
    process_arr["CTIPD"] = "Commtouch IP Reputation daemon"
    process_arr["CVPND"] = "Back-end daemon of the Mobile Access Software Blade."
    process_arr["CVPNUMD"] = "Report SNMP connected users to AMON."
    process_arr["DBSYNC"] = "DBsync enables SmartEvent to synchronize data stored in different parts of the network"
    process_arr["DBWRITER"] = "Offload database commands from cvpnd (to prevent locks) and synchronize with other members."
    process_arr["DHCPD"] = "DHCP server daemon"
    process_arr["DLP_FINGERPRINT"] = "Used to identify the data according to a unique signature known as a fingerprint stored in your repository."
    process_arr["DLP_WS"] = "Check Server that either stops or processes the e-mail"

    # DLPU_N is defined with N as a variable. Since we want to keep these metrics strict we need to match all possible scenarios. Thinking here that 0-20 will suffice.
    process_arr["DLPU_0"] = "DLP Blade: Receives data from Check Point kernel"
    process_arr["DLPU_1"] = "DLP Blade: Receives data from Check Point kernel"
    process_arr["DLPU_2"] = "DLP Blade: Receives data from Check Point kernel"
    process_arr["DLPU_3"] = "DLP Blade: Receives data from Check Point kernel"
    process_arr["DLPU_4"] = "DLP Blade: Receives data from Check Point kernel"
    process_arr["DLPU_5"] = "DLP Blade: Receives data from Check Point kernel"
    process_arr["DLPU_6"] = "DLP Blade: Receives data from Check Point kernel"
    process_arr["DLPU_7"] = "DLP Blade: Receives data from Check Point kernel"
    process_arr["DLPU_8"] = "DLP Blade: Receives data from Check Point kernel"
    process_arr["DLPU_9"] = "DLP Blade: Receives data from Check Point kernel"
    process_arr["DLPU_10"] = "DLP Blade: Receives data from Check Point kernel"
    process_arr["DLPU_11"] = "DLP Blade: Receives data from Check Point kernel"
    process_arr["DLPU_12"] = "DLP Blade: Receives data from Check Point kernel"
    process_arr["DLPU_13"] = "DLP Blade: Receives data from Check Point kernel"
    process_arr["DLPU_14"] = "DLP Blade: Receives data from Check Point kernel"
    process_arr["DLPU_15"] = "DLP Blade: Receives data from Check Point kernel"
    process_arr["DLPU_16"] = "DLP Blade: Receives data from Check Point kernel"
    process_arr["DLPU_17"] = "DLP Blade: Receives data from Check Point kernel"
    process_arr["DLPU_18"] = "DLP Blade: Receives data from Check Point kernel"
    process_arr["DLPU_19"] = "DLP Blade: Receives data from Check Point kernel"
    process_arr["DLPU_20"] = "DLP Blade: Receives data from Check Point kernel"


    process_arr["DROPBEAR"] = "Lightweight SSH server on 1100 appliance"
    process_arr["FWD"] = "Logging and spawning child processes (eg vpnd)"
    process_arr["FWK_WD"] = "Firewall Kernal deamon"
    process_arr["FWM"] = "Communication between SmartConsole applications and Security Management Server"
    process_arr["FWPUSHD"] = "Mobile Access Push Notifications daemon that is controlled by ""fwpush"" command. It is a child of fwd daemon (R77.10 and above)."
    process_arr["FWUCD"] = "UserCheck back-end daemon that sends approval / disapproval requests to user"
    process_arr["HISTORYD"] = "CPView Utility History daemon"
    process_arr["HTTPD"] = "Front-end daemon of the Mobile Access Software Blade (multi-processes)."
    process_arr["HTTPD2"] = "Web server daemon (Gaia Portal)"
    process_arr["IN.ACAPD"] = "Packet capturing daemon for SmartView Tracker logs"
    process_arr["IN.EMAILD.MTA"] = "E-Mail Security Server that receives e-mails sent by user and sends them to their destinations"
    process_arr["IN.EMAILD.POP3"] = "POP3 Security Server that receives e-mails sent by user"
    process_arr["IN.EMAILD.SMTP"] = "SMTP Security Server that receives e-mails sent by user and sends them to their destinations"
    process_arr["IN.GEOD"] = "Updates the IPS Geo Protection Database"
    process_arr["IN.MSD"] = "Mail Security Daemon that queries the Commtouch engine for reputation"
    process_arr["INTERPRETER"] = "Process is responsible for Compliance Blade database scan"
    process_arr["JAVA_SOLR"] = "Starting in R80 (SmartEvent NGSE was integrated)"
    process_arr["LEA_SESSION"] = "Responsible for OPSEC LEA session between the OPSEC LEA Client and the OPSEC LEA Server on Check Point Management Server / Log Server"
    process_arr["LOG_CONSOLIDATOR"] = "Log Consolidator for the SmartReporter product"
    process_arr["LOG_INDEXER"] = "Starting in R80 (SmartEvent NGSE was integrated)"
    process_arr["LOGCORE"] = "Starting in R80 (SmartEvent NGSE was integrated)"
    process_arr["LPD"] = "Log Parser Daemon, Search predefined patterns in log files"
    process_arr["MONITORD"] = "Hardware monitoring daemon"
    process_arr["MOVEFILESERVER"] = "Move files between cluster members in order to perform database synchronization."
    process_arr["MOVEFILESERVERDEMUXER"] = "Related to MoveFileServer process (moving files between cluster members in order to perform database synchronization)."
    process_arr["MPDAEMON"] = "Platform Portal / Multi Portal. mpdaemon process is responsible for starting these web servers"
    process_arr["PDPD"] = "Policy Decision Point daemon"
    process_arr["PEPD"] = "Policy Enforcement Point daemon"
    process_arr["PINGER"] = "Reduce the number of httpd processes performing ActiveSync."
    process_arr["PKXLD"] = "Performs asymmetric key operations for HTTPS Inspection (R77.30 and above)"
    process_arr["PM"] = "Gaia OS Process Manager (/bin/pm). Controls other processes and daemons."
    process_arr["POSTGRES"] = "PostgreSQL server. Used by Remote Access Session Visibility and Management Utility."
    process_arr["RAD"] = "Resource Advisor - responsible for the detection of Social Network widgets. The detection is done via an online Application Control database, which identifies URLs as applications"
    process_arr["RCONFD"] = "Provisioning daemon"
    process_arr["ROUTED"] = "Routing daemon"
    process_arr["RTDB"] = "Real Time database daemon"
    process_arr["RTDBD"] = "Real Time database daemon"
    process_arr["RTMD"] = "Real time traffic statistics"
    process_arr["SCANENGINE_B"] = "Third party engine"
    process_arr["SCANENGINE_K"] = "Third party engine"
    process_arr["SCANENGINE_S"] = "Third party engine"
    process_arr["SCRUB_CP_FILE_CONVERTD"] = "Used to convert various file formats to simple textual format for scanning by the DLP engine"
    process_arr["SCRUB"] = "Main CLI process for Threat Extraction"
    process_arr["SCRUBD"] = "Main Threat Extraction daemon"
    process_arr["SEARCHD"] = "Search indexing daemon"
    process_arr["SFWD"] = "Logging, Policy installation, VPN negotiation, Identity Awareness enforcement, UserCheck enforcement, etc"
    process_arr["SMARTLOG_SERVER"] = "SmartLog product"
    process_arr["SMARTVIEW"] = "SmartEvent Web Application that allows you to connect to SmartEvent NGSE server"
    process_arr["SMS"] = "Manages communication (status collection, logs collection, policy update, configuration update) with UTM-1 Edge Security Gateways"
    process_arr["SNMPD"] = "SNMP (Linux) daemon"
    process_arr["SSHD"] = "SSH daemon."
    process_arr["STATUS_PROXY"] = "Status collection of ROBO Gateways - SmartLSM / SmartProvisioning status proxy. This process runs only on Security Management Server / Domain Management Servers that are activated for Large Scale Management / SmartProvisioning."
    process_arr["STPR"] = "Status collection of ROBO Gateways - SmartLSM / SmartProvisioning status proxy. This process runs only on Security Management Server / Domain Management Servers that are activated for Large Scale Management / SmartProvisioning"
    process_arr["SVR"] = "Controller for the SmartReporter product. Traffic is sent via SSL"
    process_arr["SYSLOGD"] = "Syslog (Linux) daemon."
    process_arr["TED"] = "Threat Emulation daemon engine - responsible for emulating files and communication with the cloud"
    process_arr["UEPM"] = "Endpoint Management Server"
    process_arr["USRCHK"] = "The CLI client for the UserCheck daemon USRCHKD (this process runs only when it is called explicitly)"
    process_arr["USRCHKD"] = "Main UserCheck daemon, which deals with UserCheck requests (from CLI / from the user) that are sent from the UserCheck Web Portal"
    process_arr["VPND"] = "IKE (UDP/TCP) NAT-T Tunnel Test Reliable Datagram Protocol (RDP) Topology Update for SecureClient SSL Network Extender (SNX) SSL Network Extender (SNX) Portal Remote Access Client configuration Visitor Mode L2TP"
    process_arr["WSDNSD"] = "DNS Resolver (in R77.30 and above) - activated when Security Gateway is configured as HTTP/HTTPS Proxy, and no next proxy is used. Process is started and stopped during policy installation"
    process_arr["WSTLSD"] = "Handles SSL handshake for HTTPS Inspected connections."
    process_arr["XPAND"] = "Configuration daemon that processes and validates all user configuration requests, updates the system configuration database, and calls other utilities to carry out the request."
}

#_VSID:Name 0:lab-CP-VSXVSLS-1-R7730
/^_VSID:Name /{
    split($2, id_and_name, ":")
    vs_id = id_and_name[1]
    vs_id_to_name[vs_id] = id_and_name[2]
    next
}

/^Context is /{
    # Ingnore this line
    next
}

#Active status: active
#Active status: standby
#Active status: -
/^Active status: /{
    management_standby[vs_id] = ($NF == "standby")
    next
}

#APP        PID    STAT  #START  START_TIME             MON  COMMAND
#APP        CTX        PID    STAT  #START  START_TIME             MON  COMMAND
/^APP +(CTX|PID) / {
    # Parse the line into a column array.
    getColumns(trim($0), "[ ]{1,}", columns)

    haveCTX = ($2 == "CTX")
    next
}

#CPD        4          18259  E     1       [08:25:14] 18/9/2016   Y    cpd
#CI_CLEANUP 0          7120   E     1       [15:42:15] 14/1/2019   N    avi_del_tmp_files
/^\w+ +[0-9]+ / {
    # Put the line into a variable to avoid column variable manipulation
    in_line = $0
    gsub(/\'/, "", in_line)

    # Use getColData to parse out the data for the specific column from the current line. The current line will be
    # split according to the same separator we've passed in the getColumns function (it's stored in the "columns" variable).
    # If the column cannot be found, the result of getColData is null (not "null").

    # Get the process name in UPPER CASE, gives case-insensitive matching
    process_name = toupper(getColData(trim(in_line), columns, "APP"))

    stat = getColData(trim(in_line), columns, "STAT")

    # E stands for "existing" which means the process is running
    # T stands for terminated, which means that the process is not running
    # T is the only state for which we report '0'. Everything else will be set to 1.
    state = 1
    if (stat ~ /T$/) {
        state = 0
    }

    # Note that we _only_ report on process names that are in the process array. Everything else is ignored. See esp.
    # comments in https://indeni.atlassian.net/browse/IKP-2114.
    if (process_name in process_arr) {
        if ( haveCTX ) {  # We have VS resources
            vs_id = getColData( trim(in_line), columns, "CTX" )

            # The CPSM process is always down on standby machines so, if this is a standby, report as 1 to avoid false positives.
            if (process_name == "CPSM" && management_standby[vs_id]) {
                state = 1
            }
            admin_list[vs_id, process_name] = state
        }else {
            # TODO: We have no CTX column in the VS context. Log warning if/when we add error logging
            # TODO: Maybe we are running in a non-vs device by mistake
        }
    }else {
        # TODO: this may indicate an error state that would be worth logging if/when we add error logging
        # TODO: to .ind scripts.
    }
}

END {
    # Make sure that we report a state for every process in process_arr, even if it wasn't in the output from
    # cpwd_admin list. The reason is alert resolution. We’ve seen it happen that a process reports as T (down), so we
    # generate an alert. But, then the same process disappears entirely from the cpwd_admin output, maybe for a while
    # (possibly forever). In that case, we need the alert to resolve somehow, so we always report 1 for any
    # process that’s in process_arr but NOT in admin_list.
    for ( vs_id in vs_id_to_name ) {
        for (process_name in process_arr) { # SUBSEP \034
            admin_list_key = vs_id SUBSEP process_name
            if (!( admin_list_key in admin_list )) {
               admin_list[admin_list_key] = 1
            }
        }
    }
    for (admin_list_key in admin_list) {
        split(admin_list_key, arr, SUBSEP)
        vs_id = arr[1]
        process_name = arr[2]
        tags["vs.name"] = vs_id_to_name[vs_id]
        tags["description"] = process_arr[process_name]
        tags["process-name"] = process_name
        state = admin_list[admin_list_key]

        writeDoubleMetric("process-state", tags, "gauge", state, "false")  # Converted to new syntax by change_ind_scripts.py script
    }
}