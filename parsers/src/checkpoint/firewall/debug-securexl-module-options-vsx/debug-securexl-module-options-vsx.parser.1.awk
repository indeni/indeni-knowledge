function addVsTags(tags) {
    tags["vs.id"] = vsid
    tags["vs.name"] = vsname
    tags["name"] = name
    tags["im.identity-tags"] = "vs.id|vs.name"
}

BEGIN {
    vsid = ""
    vsname = ""
    name = ""
    fwaccel_debug_filter_status = 0
}

#_VSID:0
/_VSID:/ {
    split($0, split_arr_vsid, ":")
    vsid = trim(split_arr_vsid[2])
}

#_Name:VSX-CXL2-Gear
/_Name:/ {
    split($0, split_arr_vsname, ":")
    vsname = trim(split_arr_vsname[2])
}


#Module: fw
/^Module:/ {
     module_name = $2
}

#Enabled Kernel debugging options: error warning
#Enabled Kernel debugging options: None
/^Options_/ {
    # extract the module options
    module_options = $0
    sub("Options_", "", module_options)
    fwaccel_modules_status = 0
    # Check if the options are different than the default ones
    if (module_options != "") {
        if (module_name == "default" && module_options != "err" ) {
            fwaccel_modules_status = 1
        } else if (module_name == "db" && module_options != "err") {
            fwaccel_modules_status = 1
        } else if (module_name == "api" && module_options != "err" ) {
            fwaccel_modules_status = 1
        } else if (module_name == "pkt" && module_options != "err" ) {
            fwaccel_modules_status = 1
        } else if (module_name == "infras" && module_options != "err" ) {
            fwaccel_modules_status = 1
        } else if (module_name == "tmpl" && module_options != "err" ) {
            fwaccel_modules_status = 1
        } else if (module_name == "vpn" && module_options != "err" ) {
            fwaccel_modules_status = 1
        } else if (module_name == "nac" && module_options != "err" ) {
            fwaccel_modules_status = 1
        } else if (module_name == "cpaq" && module_options != "error" ) {
            fwaccel_modules_status = 1
        } else if (module_name == "synatk" && module_options != "") {
            fwaccel_modules_status = 1
        } else if (module_name == "adp" && module_options != "err" ) {
            fwaccel_modules_status = 1
        } else if (module_name == "dos" && module_options != "err" ) {
            fwaccel_modules_status = 1
        }
    }

    # Write data
    name = "fwaccel debug - " module_name " module"
    addVsTags(vstags)
    writeDoubleMetric("debug-status", vstags, "gauge", fwaccel_modules_status, "false")  # Converted to new syntax by change_ind_scripts.py script
}


#Debug filter not set.
#Debug filter: "<*,*,*,*,*>"
/^Debug filter/ {
    if (NF == 4) {
        fwaccel_debug_filter_status = 0
    } else {
        fwaccel_debug_filter_status = 1
    }
    name = "fwaccel debug - filter status"
    addVsTags(vstags)
    writeDoubleMetric("debug-status", vstags, "gauge", fwaccel_debug_filter_status, "false")  # Converted to new syntax by change_ind_scripts.py script
}
