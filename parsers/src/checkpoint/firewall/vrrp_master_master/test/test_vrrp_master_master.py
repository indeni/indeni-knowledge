import os
import unittest

from checkpoint.firewall.vrrp_master_master.vrrp_master_master import VrrpStates
from parser_service.public.action import WriteComplexMetric


class TestLowSevParser1(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = VrrpStates()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_valid_master_backup_state_healthy(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/valid_master_backup_state_healthy.input', {}, {})

        # Assert
        self.assertEqual(4, len(result))

        self.assertTrue(isinstance(result[0], WriteComplexMetric))
        self.assertEqual('bond1.99', result[0].tags['vrrp.name'])
        self.assertEqual('VRID1', result[0].tags['vrrp.vrid'])
        self.assertEqual('Backup', result[0].value['value'])

        self.assertTrue(isinstance(result[1], WriteComplexMetric))
        self.assertEqual('bond1.99', result[1].tags['vrrp.name'])
        self.assertEqual('VRID2', result[1].tags['vrrp.vrid'])
        self.assertEqual('Master', result[1].value['value'])


    def test_valid_master_master_state_problem(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/valid_master_master_state_problem.input', {}, {})

        # Assert
        self.assertEqual(4, len(result))

        self.assertTrue(isinstance(result[0], WriteComplexMetric))
        self.assertEqual('bond1.99', result[0].tags['vrrp.name'])
        self.assertEqual('VRID1', result[0].tags['vrrp.vrid'])
        self.assertEqual('Master', result[0].value['value'])

        self.assertTrue(isinstance(result[1], WriteComplexMetric))
        self.assertEqual('bond1.99', result[1].tags['vrrp.name'])
        self.assertEqual('VRID2', result[1].tags['vrrp.vrid'])
        self.assertEqual('Master', result[1].value['value'])

        self.assertTrue(isinstance(result[3], WriteComplexMetric))
        self.assertEqual('eth2', result[3].tags['vrrp.name'])
        self.assertEqual('VRID1', result[3].tags['vrrp.vrid'])
        self.assertEqual('Backup', result[3].value['value'])


if __name__ == '__main__':
    unittest.main()
