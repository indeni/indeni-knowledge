from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class VrrpStates(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            total_interface_name_status = helper_methods.parse_data_as_list(raw_data, 'checkpoint_gaia_show_vrrp_interfaces.textfsm')
            if total_interface_name_status:
                for i in total_interface_name_status:
                    tags = {'vrrp.name': i['Interface_name'],
                            'vrrp.vrid': 'VRID' + i['VRID']}
                    vrrp_value = i['State']
                    self.write_complex_metric_string('vrrp-master-master', tags, vrrp_value, False)
        return self.output
