from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class EnabledBladesNoVsx(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            getting_lines = raw_data.splitlines()

            # Clearing data to one list:
            split_features = ''
            if getting_lines:
                for entry in getting_lines:
                    if 'fw' in entry:
                        split_features = entry.split()

            # Create array and report to DB:
            if split_features:
                features = []
                for feature in split_features:
                    line = {'name': feature}
                    features.append(line)
                self.write_complex_metric_object_array('features-enabled', {}, features, False)
        return self.output
