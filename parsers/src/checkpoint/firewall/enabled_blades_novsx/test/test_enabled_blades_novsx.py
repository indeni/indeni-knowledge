import os
import unittest

from checkpoint.firewall.enabled_blades_novsx.enabled_blades_novsx import EnabledBladesNoVsx
from parser_service.public.action import *

class TestEnabledBladesNoVsx(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = EnabledBladesNoVsx()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_error_input(self):
        expected_results = [{'metric': 'features-enabled', 'value': [{'name': 'fw'}, {'name': 'vpn'}, {'name': 'urlf'}, {'name': 'av'}, {'name': 'appi'}, {'name': 'ips'}, {'name': 'identityServer'}, {'name': 'anti_bot'}, {'name': 'vpn'}]}]

        # Act
        result = self.parser.parse_file(self.current_dir + '/error_input.input', {}, {})
        # Assert
        self.assertEqual(1, len(result))

        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i]['metric'], result[i].name)
            self.assertEqual(expected_results[i]['value'], result[i].value)

    def test_invalid_input(self):
        expected_results = []

        # Act
        result = self.parser.parse_file(self.current_dir + '/invalid_input.input', {}, {})
        # Assert
        self.assertEqual(0, len(result))

        for i in range(len(expected_results)):
            self.assertEqual([], [])


    def test_valid_input(self):
        expected_results = [{'metric': 'features-enabled', 'value': [{'name': 'fw'}, {'name': 'vpn'}, {'name': 'urlf'}, {'name': 'av'}, {'name': 'appi'}, {'name': 'ips'}, {'name': 'identityServer'}, {'name': 'anti_bot'}, {'name': 'vpn'}]}]

        # Act
        result = self.parser.parse_file(self.current_dir + '/valid_input.input', {}, {})
        # Assert
        self.assertEqual(1, len(result))

        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i]['metric'], result[i].name)
            self.assertEqual(expected_results[i]['value'], result[i].value)


    def test_valid_input_nice(self):
        expected_results = [{'name': None, 'metric': 'features-enabled', 'value': [{'name': 'fw'}, {'name': 'vpn'}, {'name': 'ips'}]}]

        # Act
        result = self.parser.parse_file(self.current_dir + '/valid_input_nice.input', {}, {})
        # Assert
        self.assertEqual(1, len(result))

        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i]['metric'], result[i].name)
            self.assertEqual(expected_results[i]['value'], result[i].value)


if __name__ == '__main__':
    unittest.main()