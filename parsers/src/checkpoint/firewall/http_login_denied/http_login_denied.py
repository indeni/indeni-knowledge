from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser
from datetime import datetime

class ChkpHttpLoginDenied(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data_current_time = helper_methods.parse_data_as_list(raw_data, 'http_login_denied_current_time.textfsm')
            if data_current_time:
                time_format = '%m %d %H:%M:%S %Y'
                current_time = data_current_time[0]['current_date']
                ct_object = datetime.strptime(current_time, time_format)
                current_time_epoch = datetime.timestamp(ct_object)
                data_entry_time = helper_methods.parse_data_as_list(raw_data, 'http_login_denied_entry_time.textfsm')
                user_list = []
                if data_entry_time:
                    time_format = '%b %d %H:%M:%S %Y'
                    for data_parsed in data_entry_time:
                        user_list.append(data_parsed['username'] + ' from ' + data_parsed['ip_addr'])
                        unique_list_users = [i for n, i in enumerate(user_list) if i not in user_list[n + 1:]]
                        log_time = data_parsed['entry_time']
                        lg_object = datetime.strptime(log_time, time_format)
                        log_time_epoch = datetime.timestamp(lg_object)
                        time_delta = current_time_epoch - log_time_epoch
                        data_parsed['entry_time'] = time_delta
                        data_parsed['username'] = f"{data_parsed['username']} from {data_parsed['ip_addr']}"
                    for user in unique_list_users:
                        last_hour = False
                        failed_count = 0
                        for entry in data_entry_time:
                            if entry['username'] == user and entry['entry_time'] < 3600:
                                last_hour = True
                                failed_count += 1
                        self.write_double_metric('http-logins-denied', {'username': user}, 'gauge', failed_count if last_hour else 0, False)
        return self.output