import unittest
from checkpoint.firewall.http_login_denied.http_login_denied import ChkpHttpLoginDenied
from parser_service.public.action import *

class TestHttpLoginDenied(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = ChkpHttpLoginDenied()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_http_login_denied(self):
        data = self.parser.parse_file(self.current_dir + '/http_login_denied.input', {}, {})
        self.assertEqual(6,len(data))
        self.assertEqual(data[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(data[0].name, 'http-logins-denied')
        self.assertEqual(data[0].tags['username'], 'indeni from 192.168.19.1')
        self.assertEqual(data[0].value, 0)
        self.assertEqual(data[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(data[1].name, 'http-logins-denied')
        self.assertEqual(data[1].tags['username'], 'test from 192.168.250.1')
        self.assertEqual(data[1].value, 0)
        self.assertEqual(data[2].action_type, 'WriteDoubleMetric')
        self.assertEqual(data[2].name, 'http-logins-denied')
        self.assertEqual(data[2].tags['username'], 'indeni from 192.168.250.1')
        self.assertEqual(data[2].value, 0)
        self.assertEqual(data[3].action_type, 'WriteDoubleMetric')
        self.assertEqual(data[3].name, 'http-logins-denied')
        self.assertEqual(data[3].tags['username'], 'YoViJugarAlZar from 192.168.250.3')
        self.assertEqual(data[3].value, 0)
        self.assertEqual(data[4].action_type, 'WriteDoubleMetric')
        self.assertEqual(data[4].name, 'http-logins-denied')
        self.assertEqual(data[4].tags['username'], 'PeluqueriaPepe from 192.168.250.3')
        self.assertEqual(data[4].value, 0)
        self.assertEqual(data[5].action_type, 'WriteDoubleMetric')
        self.assertEqual(data[5].name, 'http-logins-denied')
        self.assertEqual(data[5].tags['username'], 'dev from 192.168.250.1')
        self.assertEqual(data[5].value, 5)

    def test_http_login_denied_empty(self):
        data = self.parser.parse_file(self.current_dir + '/http_login_denied_empty.input', {}, {})
        self.assertEqual(0,len(data))
        
if __name__ == '__main__':
    unittest.main()