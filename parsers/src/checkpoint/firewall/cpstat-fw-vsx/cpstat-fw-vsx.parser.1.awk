function addVsTags(tags) {
    tags["vs.id"] = vsid
    tags["vs.name"] = vsname
    tags["policy-name"] = policyname 
}

function dumpPolicyData() {
    addVsTags(policytags)

    if (policyname != "") {
        policystatus = 1
    } else {
        policystatus = 0
    }

    # Should not write a metric if this is a virtual switch, since they do not have a policy.
    if (vstype != "Switch") {
        writeDoubleMetric("policy-installed", policytags, "gauge", policystatus, "true", "Firewall Policy", "state", "policy-name|vs.name|vs.id")  # Converted to new syntax by change_ind_scripts.py script
    }

    # Should not write a metric if this is a virtual switch, since they do not have a policy or if there is no installTime.
    if (install_time != "" && vstype != "Switch") {
        writeDoubleMetric("policy-install-last-modified", policytags, "gauge", datetime(year, month_num, day, hour, minute, second), "true", "Firewall Policy - Last Modified", "date", "vs.name|vs.id")  # Converted to new syntax by change_ind_scripts.py script
    }
    policyname = ""
    install_time = ""
    month_num = ""
    day = ""
    time = ""
    hour = ""
    minute = ""
    second = ""
    year = ""
    delete install_time_arr
}

BEGIN {
    vsid = ""
    vsname = ""
}

#_VSID:Name:Type 3:lab-CP-VSX1_vsx-2:VSX Gateway
/^_VSID:Name:Type/ {
    # Dump data of previous VS if needed
    if (vsid != "") {
        dumpPolicyData()
    }
    split($2, id_and_name, ":")
    vsid = id_and_name[1]
    vsname = id_and_name[2]
    vstype = id_and_name[3]

    next
}

#Policy name: Standard
/Policy name/ {
    policyname = $0
    gsub(/Policy name\:/, "", policyname)
    policyname = trim(policyname)
}

#Install time: Mon May  9 03:05:41 2016
/Install time/ {
    install_time = $0

    # Remove the "install time:" part
    gsub(/Install time\:/, "", install_time)

    # Remove some double spaces
    gsub(/  /, " ", install_time)

    split(trim(install_time), install_time_arr, " ")
    month_num = parseMonthThreeLetter(install_time_arr[2])
    day = install_time_arr[3]
    time = install_time_arr[4]

    split(time, time_arr, ":")
    hour = time_arr[1]
    minute = time_arr[2]
    second = time_arr[3]
    year = install_time_arr[5]
}

#ISP link table
#------------------------
#|Name   |Status|Role   |
#------------------------
#|ISP-1  |OK    |Primary|
#|ISP-2  |OK    |Backup |
#------------------------

/^\s*\|\s*ISP/{
    isp_output = $0
    # Preserve the "Status" string it may be an error message
    split(isp_output, isp_arr, "|")
    tags["isp-link"] = vsname "(" vsid ") " trim(isp_arr[2]) " - " trim(isp_arr[4])
    writeComplexMetricString("isp-link-status", tags, trim(isp_arr[3]), "false")  # Converted to new syntax by change_ind_scripts.py script
}

END {
    # Dump data of last VS
    if (vsid != "") {
        dumpPolicyData()
    }
}