from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class FwaccelStatNoVsx(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            parsed_data_status = helper_methods.parse_data_as_list(raw_data, 'fwaccel_stat_status.textfsm')
            if parsed_data_status:
                tags = {}
                if  parsed_data_status[0]['name']:
                    tags = {"name": parsed_data_status[0]['name'], 'im.identity-tags': 'name', 'im.dstype.displaytype': 'string'}
                else:
                    tags = {"name": 'Firewall', 'im.identity-tags': 'name', 'im.dstype.displaytype': 'string'}
                self.write_complex_metric_string('securexl-status', tags, parsed_data_status[0]['accelerator_status'], True, 'SecureXL - Status')

                parsed_data_templates = helper_methods.parse_data_as_list(raw_data, 'fwaccel_stat_disabled_template.textfsm')
                if parsed_data_templates:
                    for template in parsed_data_templates:
                        tags = {}
                        status = ''
                        tags['type'] = template['template_type']
                        if template['template_layer']:
                            status = ', Layer {}'.format(template['template_layer'])
                        if template['template_disabled_from_rule']:
                            status = ' {} disables template offloads from rule #{}'.format(status, template['template_disabled_from_rule'])
                        tags['im.identity-tags'] = 'type'
                        if tags['type'] == 'Accept':
                            self.write_complex_metric_string('securexl-accept-template-status', tags, template['template_status'], True, 'SecureXL - Templates')
                        if tags['type'] == 'Drop':
                            self.write_complex_metric_string('securexl-drop-template-status', tags, template['template_status'], True, 'SecureXL - Templates')
                        if tags['type'] == 'NAT':
                            self.write_complex_metric_string('securexl-nat-template-status', tags, template['template_status'], True, 'SecureXL - Templates')
                        if tags['type'] == 'NMR':
                            self.write_complex_metric_string('securexl-nmr-template-status', tags, template['template_status'], True, 'SecureXL - Templates')
                        if tags['type'] == 'NMT':
                            self.write_complex_metric_string('securexl-nmt-template-status', tags, template['template_status'], True, 'SecureXL - Templates')

        return self.output
