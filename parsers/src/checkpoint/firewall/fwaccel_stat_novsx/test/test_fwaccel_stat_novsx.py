import os
import unittest

from checkpoint.firewall.fwaccel_stat_novsx.fwaccel_stat_novsx import FwaccelStatNoVsx


class TestFwaccelStatNoVsx(unittest.TestCase):

    def setUp(self):
        self.parser = FwaccelStatNoVsx()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_fwaccel_disabled_by_firewall(self):
        result = self.parser.parse_file(self.current_dir + '/fwaccel_disabled_by_firewall.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].name, 'securexl-status')
        self.assertEqual(result[0].tags['name'], 'Firewall')
        self.assertEqual(result[0].value['value'], 'disabled by Firewall')

    def test_fwaccel_stat_off_by_firewall(self):
        result = self.parser.parse_file(self.current_dir + '/fwaccel_stat_off_by_firewall.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].name, 'securexl-status')
        self.assertEqual(result[0].tags['name'], 'Firewall')
        self.assertEqual(result[0].value['value'], 'off by Firewall (too many general errors (10) (caller: cphwd_add_conn_stat_cb))')

    def test_fwaccel_stat_off(self):
        result = self.parser.parse_file(self.current_dir + '/fwaccel_stat_off.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].name, 'securexl-status')
        self.assertEqual(result[0].tags['name'], 'Firewall')
        self.assertEqual(result[0].value['value'], 'off')

    def test_fwaccel_stat_on_novsx(self):
        result = self.parser.parse_file(self.current_dir + '/fwaccel_stat_on_novsx.input', {}, {})
        self.assertEqual(4, len(result))
        self.assertEqual(result[0].name, 'securexl-status')
        self.assertEqual(result[0].tags['name'], 'Firewall')
        self.assertEqual(result[0].value['value'], 'on')
        self.assertEqual(result[1].name, 'securexl-accept-template-status')
        self.assertEqual(result[1].value['value'], 'enabled')
        self.assertEqual(result[1].tags['type'], 'Accept')
        self.assertEqual(result[2].name, 'securexl-drop-template-status')
        self.assertEqual(result[2].value['value'], 'disabled')
        self.assertEqual(result[2].tags['type'], 'Drop')
        self.assertEqual(result[3].name, 'securexl-nat-template-status')
        self.assertEqual(result[3].value['value'], 'disabled by user')
        self.assertEqual(result[3].tags['type'], 'NAT')

    def test_fwaccel_stat_waiting_policy_load(self):
        result = self.parser.parse_file(self.current_dir + '/fwaccel_stat_waiting_policy_load.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].name, 'securexl-status')
        self.assertEqual(result[0].tags['name'], 'Firewall')
        self.assertEqual(result[0].value['value'], 'waiting for policy load')

    def test_R80_20_fwaccel_disabled_novsx(self):
        result = self.parser.parse_file(self.current_dir + '/R80_20_fwaccel_disabled_novsx.input', {}, {})
        self.assertEqual(4, len(result))
        self.assertEqual(result[0].name, 'securexl-status')
        self.assertEqual(result[0].tags['name'], 'SND')
        self.assertEqual(result[0].value['value'], 'disabled')
        self.assertEqual(result[1].name, 'securexl-accept-template-status')
        self.assertEqual(result[1].value['value'], 'enabled')
        self.assertEqual(result[1].tags['type'], 'Accept')
        self.assertEqual(result[2].name, 'securexl-drop-template-status')
        self.assertEqual(result[2].value['value'], 'disabled')
        self.assertEqual(result[2].tags['type'], 'Drop')
        self.assertEqual(result[3].name, 'securexl-nat-template-status')
        self.assertEqual(result[3].value['value'], 'enabled')
        self.assertEqual(result[3].tags['type'], 'NAT')

    def test_R80_20_fwaccel_enabled_and_disabled_from_rule_novsx(self):
        result = self.parser.parse_file(self.current_dir + '/R80_20_fwaccel_enabled_and_disabled_from_rule_novsx.input', {}, {})
        self.assertEqual(4, len(result))
        self.assertEqual(result[0].name, 'securexl-status')
        self.assertEqual(result[0].tags['name'], 'SND')
        self.assertEqual(result[0].value['value'], 'enabled')
        self.assertEqual(result[1].name, 'securexl-accept-template-status')
        self.assertEqual(result[1].value['value'], 'disabled by Firewall')
        self.assertEqual(result[1].tags['type'], 'Accept')
        self.assertEqual(result[2].name, 'securexl-drop-template-status')
        self.assertEqual(result[2].value['value'], 'disabled')
        self.assertEqual(result[2].tags['type'], 'Drop')
        self.assertEqual(result[3].name, 'securexl-nat-template-status')
        self.assertEqual(result[3].value['value'], 'disabled by Firewall')
        self.assertEqual(result[3].tags['type'], 'NAT')

    def test_R80_20_fwaccel_enabled_novsx(self):
        result = self.parser.parse_file(self.current_dir + '/R80_20_fwaccel_enabled_novsx.input', {}, {})
        self.assertEqual(4, len(result))
        self.assertEqual(result[0].name, 'securexl-status')
        self.assertEqual(result[0].tags['name'], 'SND')
        self.assertEqual(result[0].value['value'], 'enabled')
        self.assertEqual(result[1].name, 'securexl-accept-template-status')
        self.assertEqual(result[1].value['value'], 'enabled')
        self.assertEqual(result[1].tags['type'], 'Accept')
        self.assertEqual(result[2].name, 'securexl-drop-template-status')
        self.assertEqual(result[2].value['value'], 'disabled')
        self.assertEqual(result[2].tags['type'], 'Drop')
        self.assertEqual(result[3].name, 'securexl-nat-template-status')
        self.assertEqual(result[3].value['value'], 'enabled')
        self.assertEqual(result[3].tags['type'], 'NAT')

    def test_R80_XX_sk145533_bug(self):
        result = self.parser.parse_file(self.current_dir + '/R80_XX_sk145533_bug.input', {}, {})
        self.assertEqual(6, len(result))
        self.assertEqual(result[0].name, 'securexl-status')
        self.assertEqual(result[0].tags['name'], 'SND')
        self.assertEqual(result[0].value['value'], 'enabled')
        self.assertEqual(result[1].name, 'securexl-accept-template-status')
        self.assertEqual(result[1].value['value'], 'disabled by Firewall')
        self.assertEqual(result[1].tags['type'], 'Accept')
        self.assertEqual(result[2].name, 'securexl-drop-template-status')
        self.assertEqual(result[2].value['value'], 'enabled')
        self.assertEqual(result[2].tags['type'], 'Drop')
        self.assertEqual(result[3].name, 'securexl-nat-template-status')
        self.assertEqual(result[3].value['value'], 'disabled by user')
        self.assertEqual(result[3].tags['type'], 'NAT')
        self.assertEqual(result[4].name, 'securexl-nmr-template-status')
        self.assertEqual(result[4].value['value'], 'enabled')
        self.assertEqual(result[4].tags['type'], 'NMR')
        self.assertEqual(result[5].name, 'securexl-nmt-template-status')
        self.assertEqual(result[5].value['value'], 'enabled')
        self.assertEqual(result[5].tags['type'], 'NMT')