from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class MaestroSmoImageAutoCloneState(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data = helper_methods.parse_data_as_list(raw_data, 'maestro_smo_image_auto_clone_state.textfsm')
            if data:
                self.write_double_metric('smo-image-auto-clone-state', {}, 'gauge', 1 if data[0]['state'] == 'on' else 0, True, "Maestro configuration - SMO image auto-clone state", "state")
        return self.output