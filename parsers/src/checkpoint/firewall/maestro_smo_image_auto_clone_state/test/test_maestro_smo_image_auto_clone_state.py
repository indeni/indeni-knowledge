import os
import unittest
from checkpoint.firewall.maestro_smo_image_auto_clone_state.maestro_smo_image_auto_clone_state import MaestroSmoImageAutoCloneState
from parser_service.public.action import *

class TestShowMaestroConfigurationOrchestratorSiteAmount(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = MaestroSmoImageAutoCloneState()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_maestro_smo_auto_clone_enabled(self):
        result = self.parser.parse_file(self.current_dir + '/smo_auto_clone_enabled.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'smo-image-auto-clone-state')
        self.assertEqual(result[0].tags['live-config'], 'true')
        self.assertEqual(result[0].tags['display-name'], 'Maestro configuration - SMO image auto-clone state')
        self.assertEqual(result[0].tags['im.dstype.displayType'], 'state')
        self.assertEqual(result[0].value, 1)

    def test_maestro_smo_auto_clone_disabled(self):
        result = self.parser.parse_file(self.current_dir + '/smo_auto_clone_disabled.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'smo-image-auto-clone-state')
        self.assertEqual(result[0].tags['live-config'], 'true')
        self.assertEqual(result[0].tags['display-name'], 'Maestro configuration - SMO image auto-clone state')
        self.assertEqual(result[0].tags['im.dstype.displayType'], 'state')
        self.assertEqual(result[0].value, 0)

if __name__ == '__main__':
    unittest.main()