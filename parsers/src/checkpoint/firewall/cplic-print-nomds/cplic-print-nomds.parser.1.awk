# 1  | 38IH618   | 30Apr2017  | CPSB-ABOT-M-1Y
/^\d{1,2}  \| / {
    ddmmmyyyy = $5
    dd=substr(ddmmmyyyy, 1, length(ddmmmyyyy)-7) # need to handle 1 or 2 digits for day
	mmm=substr(ddmmmyyyy, length(dd)+1, 3)
    mm=parseMonthThreeLetter(mmm)
	yyyy=substr(ddmmmyyyy, length(ddmmmyyyy)-3, length(ddmmmyyyy))
	
    contracttags["name"] = $3 " - " $NF
    contractexpiration=date(yyyy, mm, dd)
    writeDoubleMetric("contract-expiration", contracttags, "gauge", contractexpiration, "true", "Support Contract Expiration", "date", "")  # Converted to new syntax by change_ind_scripts.py script
}


# 192.168.250.5    never       CPSB-ADNC-M CPSB-EVCR-10 CK-00-1C-7F-3E-CB-38
# 10.10.6.10       30Sep2017   CPSG-C-8-U CPSB-FW CPSB-VPN CPSB-ADN CPSB-ACCL CPSB-IPSA CPSB-DLP CPSB-SSLVPN-50 CPSB-IA CPSG-VSX-25S CPSB-SWB CPSB-IPS CPSB-ASPM CPSB-URLF CPSB-AV CPSB-APCL CPSB-ABOT-L CK-043C32F48B44
/(?:[0-9]{1,3}\.){3}[0-9]{1,3}/ {
	if (alreadyFound[$NF] == 1)
	 next

	# extract the features, split on at least 2 spaces or more
	split($0,splitArr,/\s{2,}+/)
	gsub(/\sCK.+/, "", splitArr[3])
	
	t["features"] = splitArr[3]
	t["name"] = $NF

	alreadyFound[$NF] = 1
	
	if ($2 == "never") {
		# Since it never expires we set a expiry date very far in the future
		writeDoubleMetric("license-expiration", t, "gauge", date(2099,12,31), "false")  # Converted to new syntax by change_ind_scripts.py script
	} else {
		
		ddmmmyyyy = $2
		 dd=substr(ddmmmyyyy, 1, length(ddmmmyyyy)-7) # need to handle 1 or 2 digits for day
		mmm=substr(ddmmmyyyy, length(dd)+1, 3)
		mm=parseMonthThreeLetter(mmm)
		yyyy=substr(ddmmmyyyy, length(ddmmmyyyy)-3, length(ddmmmyyyy))
	
		writeDoubleMetric("license-expiration", t, "gauge", date(yyyy, mm, dd), "true", "License Expiration - CK", "date", "name")  # Converted to new syntax by change_ind_scripts.py script
	}
}