function addVsTags(tags) {
    tags["vs.id"] = vsid
    tags["vs.name"] = vsname
    tags["name"] = name
    tags["im.identity-tags"] = "vs.id|vs.name"
}

BEGIN {
    vsid = ""
    vsname = ""
    name = ""
    tcpdump_status = 0
    kernel_debug_status = 0
    buffer_size_status = 0
    kernel_debug_enabled_per_vsid = 0
    block_name = ""
}

# Note that while the tcpdump grep works for both gateway and management, fw ctl debug is only valid for gateways. See
# unit tests. 
# This script no longer runs vs Management.

#_VSID:0
/_VSID:/ {
    split($0, split_arr_vsid, ":")
    vsid = trim(split_arr_vsid[2])
}

#_Name:VSX-CXL2-Gear
/_Name:/ {
    split($0, split_arr_vsname, ":")
    vsname = trim(split_arr_vsname[2])

    #if kdebug/zdebug enable in this VSID on gateway
    if (kernel_debug_enabled_per_vsid == 1 && kernel_debug_vsid[vsid] != 1) {
        kernel_debug_vsid[vsid] = 0
    }
    name = "firewall kernel debug - process kdebug/zdebug"
    addVsTags(vstags)
    writeDoubleMetric("debug-status", vstags, "gauge", kernel_debug_vsid[vsid], "false")
}

#Kernel debugging buffer size: 50KB
/buffer size:/ {

    buffer_size = $NF
    gsub(/KB/, "", buffer_size)
    if (buffer_size > 50) {
        buffer_size_status = 1
    }
    name = "firewall kernel debug - buffer Size"
    addVsTags(vstags)
    writeDoubleMetric("debug-status", vstags, "gauge", buffer_size_status, "false")
    buffer_size = 0
}

# HOST: or PPK_)
/^.*:$/ {
    block_name = $1
    gsub(/:/,"",block_name)
}

#Module: fw
/Module:/ {
     module_name = $NF
}

#Enabled Kernel debugging options: error warning
#Enabled Kernel debugging options: None
/debugging options:/ {
    # extract the module options
    module_options = $0
    split(module_options, module_options_arr, ":")
    module_options = trim(module_options_arr[2])

    kernel_modules_status = 0

    # Check if the options are different than the default ones
   if (module_name == "kiss" && (module_options != "error warning" && module_options != "error warning htab_bl_err")){
       kernel_modules_status = 1
    } else if (module_name == "kissflow" && module_options != "error warning") {
        kernel_modules_status = 1
    } else if (module_name == "fw" && (module_options != "error warning" && module_options != "None") ) {
        kernel_modules_status = 1
    } else if (module_name == "h323" && module_options != "error") {
        kernel_modules_status = 1
    } else if (module_name == "cpcode" && module_options != "error warning") {
        kernel_modules_status = 1
    } else if (module_name == "upconv" && module_options != "error warning info") {
        kernel_modules_status = 1
    } else if (module_name == "WS_SIP" && module_options != "None") {
        kernel_modules_status = 1
    } else if (module_name == "multik" && module_options != "None") {
        kernel_modules_status = 1
    } else if (module_name == "UC" && module_options != "None") {
        kernel_modules_status = 1
    } else if (module_name == "dlpk" && module_options != "None") {
        kernel_modules_status = 1
    } else if (module_name == "dlpuk" && module_options != "None") {
        kernel_modules_status = 1
    } else if (module_name == "gtp" && module_options != "None") {
        kernel_modules_status = 1
    } else if (module_name == "CPAS" && module_options != "error warning") {
        kernel_modules_status = 1
    } else if (module_name == "WSIS" && module_options != "None") {
        kernel_modules_status = 1
    } else if (module_name == "VPN" && (module_options != "err" && module_options != "None") ) {
        kernel_modules_status = 1
    } else if (module_name == "UPIS" && module_options != "None") {
        kernel_modules_status = 1
    } else if (module_name == "BOA" && module_options != "None") {
        kernel_modules_status = 1
    } else if (module_name == "cmi_loader" && module_options != "None") {
        kernel_modules_status = 1
    } else if (module_name == "NRB" && module_options != "None") {
        kernel_modules_status = 1
    } else if (module_name == "SGEN" && module_options != "None") {
        kernel_modules_status = 1
    } else if (module_name == "RAD_KERNEL" && module_options != "None") {
        kernel_modules_status = 1
    } else if (module_name == "WS" && module_options != "None") {
        kernel_modules_status = 1
    } else if (module_name == "APPI" && module_options != "None") {
        kernel_modules_status = 1
    } else if (module_name == "UP" && module_options != "None") {
        kernel_modules_status = 1
    } else if (module_name == "MALWARE" && module_options != "None") {
        kernel_modules_status = 1
    } else if (module_name == "CI" && module_options != "None") {
        kernel_modules_status = 1
    } else if (module_name == "SFT" && module_options != "None") {
        kernel_modules_status = 1
    } else if (module_name == "ICAP_CLIENT" && module_options != "None") {
        kernel_modules_status = 1
    } else if (module_name == "FILEAPP" && module_options != "None") {
        kernel_modules_status = 1
    } else if (module_name == "dlpda" && module_options != "None") {
        kernel_modules_status = 1
    } else if (module_name == "cluster" && module_options != "None") {
        kernel_modules_status = 1
    } else if (module_name == "PSL" && module_options != "error warning") {
        kernel_modules_status = 1
    } else if (module_name == "seqvalid" && module_options != "error warning") {
        kernel_modules_status = 1
    } else if (module_name == "synatk" && module_options != "None") {
        kernel_modules_status = 1
    } else if (module_name == "MUX" && module_options != "error warning") {
        kernel_modules_status = 1
    } else if (module_name == "accel_pm_mgr" && module_options != "error warning") {
        kernel_modules_status = 1
    } else if (module_name == "accel_apps" && module_options != "error warning") {
        kernel_modules_status = 1
    } else if (module_name == "fg" && module_options != "error") {
        kernel_modules_status = 1
    }

    # Write data
    if (block_name == "")
        name = "firewall kernel debug - " module_name " module"
    else
        name = "firewall kernel debug - " block_name " - " module_name " module"
    addVsTags(vstags)
    writeDoubleMetric("debug-status", vstags, "gauge", kernel_modules_status, "false")
    block_name = ""
    module_name = ""
}

#_PID pcap     23315  2.0  0.0   4532  1152 pts/37   S+   23:12   0:00 tcpdump -nni any
/^_PID/&&/tcpdump/ {
    tcpdump_status = 1
}

#_PID admin     5529  0.5  0.9 540648 35772 pts/9    S+   10:06   0:00 fw ctl kdebug -f
#_PID admin    12343  0.8  0.3 108084 22592 pts/36   S+   00:03   0:00 fw ctl kdebug -v 3 -T -f
/^_PID/&&/debug/ {
    global_kernel_debug = 1 #If -v is not present debug enable at VSX level

    for ( i = 1 ; i <= NF ; i++ ) {
        if ( $i == "-v" ) #If -v is present debug enable at VS level
            global_kernel_debug = 0
            kernel_debug_enabled_per_vsid = 1
            kernel_debug_vsid[$(i+1)] = 1 #Identifying which VS has kernel debug enbaled
    }
    if ( global_kernel_debug == 1 ) {
        kernel_debug_status = 1
    }
}

END {
    tags["name"] = "firewall kernel debug - process tcpdump"
    writeDoubleMetric("debug-status", tags, "gauge", tcpdump_status, "false")

    #if kdebug/zdebug enable globally on gateway
    tags["name"] = "firewall kernel debug - process kdebug/zdebug"
    writeDoubleMetric("debug-status", tags, "gauge", kernel_debug_status, "false")


}
