############
# Why: Get information from different interesting kernel parameters.
# How: Using "fw ctl get"
###########

#fwha_monitor_all_vlan = 0
/=/ {
    paramtags["name"] = $1
    writeDoubleMetric("firewall-kparam", paramtags, "gauge", $NF, "true", "Kernel Parameters (selected)", "number", "name")  # Converted to new syntax by change_ind_scripts.py script
}

#fwha_vmac_global_param_enabled = 0
/^fwha_vmac_global_param_enabled/ {
    tags["name"] = $1
    if ($NF == 0) {
       value = "false"
    } else {
       value = "true"
    }
    writeComplexMetricString("vmac-mode", tags, value, "false")
}