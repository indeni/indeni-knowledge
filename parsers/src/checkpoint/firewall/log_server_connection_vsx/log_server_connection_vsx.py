from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class LogServerConnectionVsx(BaseParser):

    @staticmethod
    def _get_key_value(key: str, data: dict):
        if key in data and data[key] != '':
            return data[key]
        return None

    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):

        def copy_previous_vs_tags(vs_tags_param):
            """ Make sure that vs_tags never has keys it shouldn't. Just copy the valid keys.
                This is just in case a later programmer inadvertently adds an extra key to this
                dict without really thinking about it.
            """
            if 'vs.id' in vs_tags_param and 'vs.name' in vs_tags_param:
                return {'vs.id': vs_tags_param['vs.id'], 'vs.name': vs_tags_param['vs.name']}
            return {}

        # Log server state values:
        # 0=to log servers
        # 1=local configured
        # 2=local due to connectivity issues
        # 3=local due to high rate

        # 2/20/19 There are situations where the gateway has actually lost connectivity with the log server but the
        # system has decided not to report as such.
        # Instead, you should track the status values of the Overall and Local Logging Mode Status
        # If the CLM or logging server is down, the overall status and local log mode status will change value to 3
        # Eventually, the status changes to 2 if the log servers are detected as being down

        # Data Extraction
        # The extracted data should look like this:
        # [{'vs_id': '0', 'vs_name': 'VS0', 'overall_state': '0', 'local_state': '0', 'server_ip': '', 'server_state': ''},
        #  {'vs_id': '0', 'vs_name': '', 'overall_state': '', 'local_state': '', 'server_ip': '10.11.94.73', 'server_state': '0'},
        #  {'vs_id': '1', 'vs_name': 'VSW-1', 'overall_state': '0', 'local_state': '0', 'server_ip': '', 'server_state': ''},
        #  {'vs_id': '1', 'vs_name': '', 'overall_state': '', 'local_state': '', 'server_ip': '10.11.94.74', 'server_state': '0'},
        #  {'vs_id': '1', 'vs_name': '', 'overall_state': '', 'local_state': '', 'server_ip': '10.11.94.75', 'server_state': '0'},
        #  {'vs_id': '2', 'vs_name': 'VS2', 'overall_state': '0', 'local_state': '0', 'server_ip': '', 'server_state': ''},
        #  {'vs_id': '2', 'vs_name': '', 'overall_state': '', 'local_state': '', 'server_ip': '10.11.94.75', 'server_state': '0'}]
        extracted_data = helper_methods.parse_data_as_list(raw_data, 'log_server_connection_vsx.textfsm')

        # Step 2 : Data Processing
        if extracted_data is not None and len(extracted_data) != 0:
            vs_tags = {}
            for data_elem in extracted_data:
                # Note: each row must have a VSID
                vs_tags = copy_previous_vs_tags(vs_tags)
                vs_id = LogServerConnectionVsx._get_key_value('vs_id', data_elem)
                if vs_id is not None:
                    vs_tags['vs.id'] = vs_id
                    vs_name = LogServerConnectionVsx._get_key_value('vs_name', data_elem)
                    if vs_name is not None:
                        vs_tags['vs.name'] = vs_name
                        overall_state = LogServerConnectionVsx._get_key_value('overall_state', data_elem)
                        local_state = LogServerConnectionVsx._get_key_value('local_state', data_elem)

                        if local_state is not None:
                            local_state = int(local_state)
                            # Check Point reports states > 1 as 'bad', so we need to flip the logic here.
                            # 0 - logging to log server; 1 - configured to log locally - intentional
                            not_logging_locally = 0  # i.e., we _are_ logging locally (not not) -- the alert state
                            if local_state < 2:      # Anything greater than 1 is a bad state
                                not_logging_locally = 1
                            self.write_double_metric('not-logging-locally', vs_tags, 'gauge', not_logging_locally, False, None, None, 'vs.name')
                            self.write_double_metric('local-logging-status-live-config', vs_tags, 'gauge', local_state, True, 'Local Logging Mode Status', 'number', 'vs.name')

                        if overall_state is not None and local_state is not None:
                            overall_state = int(overall_state)
                            # 0 - logging to log server; 1 - configured to log locally
                            log_server_is_communicating = 1
                            if local_state > 1 and overall_state > 1:
                                log_server_is_communicating = 0
                            overall_tags = vs_tags.copy()  # shallow copy
                            overall_tags['name'] = 'All Log Servers'
                            self.write_double_metric('log-server-communicating-vsx', overall_tags, 'gauge', log_server_is_communicating, False)
                            
                    else:  # This is an 'IP row'
                        server_ip = LogServerConnectionVsx._get_key_value('server_ip', data_elem)
                        server_state = LogServerConnectionVsx._get_key_value('server_state', data_elem)

                        server_ip_data = []
                        if server_ip is not None:
                            entry_data = {}
                            entry_data ['server_ip'] = server_ip
                            server_ip_data.append(entry_data)
                        if server_state is not None:
                            server_state = int(server_state)
                            report_state = 0
                            if server_state == 0 or server_state == 2:
                                report_state = 1
                            if 'vs.name' in vs_tags:
                                ip_tags = vs_tags.copy()  # shallow copy
                                ip_tags['name'] = server_ip
                                self.write_double_metric('log-server-communicating-vsx', ip_tags, 'gauge', report_state, False)
                        self.write_complex_metric_object_array('logging-server-ip', ip_tags, server_ip_data, False, "Logging server IP")
                                    
        # The method must return self.output
        return self.output
