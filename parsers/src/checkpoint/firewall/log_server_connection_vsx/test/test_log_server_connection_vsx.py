import os
import unittest
from checkpoint.firewall.log_server_connection_vsx.log_server_connection_vsx import LogServerConnectionVsx
from parser_service.public.action import *

class TestLogServerConnectionVsx(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = LogServerConnectionVsx()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_log_server_connection_vsx(self):
        extracted_data = self.parser.parse_file(self.current_dir + '/R80_30_3VS_log_server_connected.input', {}, {})
        self.assertEqual(17,len(extracted_data))
        self.assertEqual('0', extracted_data[3].tags['vs.id'])
        self.assertEqual('VS0', extracted_data[3].tags['vs.name'])
        self.assertEqual('10.11.94.73',extracted_data[3].tags['name'])
        self.assertEqual('1', extracted_data[8].tags['vs.id'])
        self.assertEqual('VSW-1', extracted_data[8].tags['vs.name'])
        self.assertEqual('10.11.94.74',extracted_data[8].tags['name'])
        self.assertEqual('1', extracted_data[10].tags['vs.id'])
        self.assertEqual('VSW-1', extracted_data[10].tags['vs.name'])
        self.assertEqual('10.11.94.75',extracted_data[10].tags['name'])
        self.assertEqual('2', extracted_data[15].tags['vs.id'])
        self.assertEqual('VS2', extracted_data[15].tags['vs.name'])
        self.assertEqual('10.11.94.75',extracted_data[15].tags['name'])

    def test_R80_30_log_server_disconnected(self):
        extracted_data = self.parser.parse_file(self.current_dir + '/R80_30_log_server_disconnected.input', {}, {})
        self.assertEqual(10, len(extracted_data))
        self.assertEqual('0',extracted_data[3].tags['vs.id'])
        self.assertEqual('VS0',extracted_data[3].tags['vs.name'])
        self.assertEqual('10.11.94.74',extracted_data[3].tags['name'])
        self.assertEqual('1',extracted_data[8].tags['vs.id'])
        self.assertEqual('VS1',extracted_data[8].tags['vs.name'])
        self.assertEqual('10.11.94.75',extracted_data[8].tags['name'])

    def test_log_server_connected_but_clm_down(self):
        extracted_data = self.parser.parse_file(self.current_dir + '/log_server_connected_but_clm_down.input', {}, {})
        self.assertEqual(12, len(extracted_data))
        self.assertEqual('0',extracted_data[3].tags['vs.id'])
        self.assertEqual('VS0',extracted_data[3].tags['vs.name'])
        self.assertEqual('192.168.197.30',extracted_data[3].tags['name'])
        self.assertEqual('0',extracted_data[5].tags['vs.id'])
        self.assertEqual('VS0',extracted_data[5].tags['vs.name'])
        self.assertEqual('192.168.197.31',extracted_data[5].tags['name'])
        self.assertEqual('1',extracted_data[10].tags['vs.id'])
        self.assertEqual('VS1',extracted_data[10].tags['vs.name'])
        self.assertEqual('192.168.197.131',extracted_data[10].tags['name'])

    def test_configured_to_log_locally(self):
        extracted_data = self.parser.parse_file(self.current_dir + '/configured_to_log_locally.input', {}, {})
        self.assertEqual(3, len(extracted_data))

    def test_no_data_returned(self):
        extracted_data = self.parser.parse_file(self.current_dir + '/no_data_returned.input', {}, {})
        self.assertEqual(0, len(extracted_data))

    def test_log_server_ip(self):
        extracted_data = self.parser.parse_file(self.current_dir + '/R80_30_3VS_log_server_connected.input', {}, {})
        self.assertEqual(17,len(extracted_data))
        self.assertEqual('0', extracted_data[4].tags['vs.id'])
        self.assertEqual('10.11.94.73',extracted_data[4].value[0]['server_ip'])
        self.assertEqual('1', extracted_data[9].tags['vs.id'])
        self.assertEqual('10.11.94.74',extracted_data[9].value[0]['server_ip'])
        self.assertEqual('1', extracted_data[11].tags['vs.id'])
        self.assertEqual('10.11.94.75',extracted_data[11].value[0]['server_ip'])
        self.assertEqual('2', extracted_data[16].tags['vs.id'])
        self.assertEqual('10.11.94.75',extracted_data[16].value[0]['server_ip'])

if __name__ == '__main__':
    unittest.main()