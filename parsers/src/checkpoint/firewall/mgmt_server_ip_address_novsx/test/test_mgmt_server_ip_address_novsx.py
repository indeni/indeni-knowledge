import os
import unittest

from checkpoint.firewall.mgmt_server_ip_address_novsx.mgmt_server_ip_address_novsx import ChkpMgmtServerIpAddressNoVsx


class TestChkpMgmtServerIpAddressNoVsx(unittest.TestCase):

    def setUp(self):
        self.parser = ChkpMgmtServerIpAddressNoVsx()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_simple_gateway(self):
        result = self.parser.parse_file(self.current_dir + '/simple_gateway.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].value[0], {'id': "1", 'mgmt_server': '10.11.94.138'})

if __name__ == '__main__':
    unittest.main()