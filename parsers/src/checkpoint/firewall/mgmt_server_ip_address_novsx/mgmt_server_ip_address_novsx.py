from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class ChkpMgmtServerIpAddressNoVsx(BaseParser):

    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        # Step 1 : Data Extraction
        data = helper_methods.parse_data_as_list(raw_data, 'mgmt_server_ip_address_novsx.textfsm')
        # Step 2 : Data Processing and Reporting
        if data:
            for entry in data:
                managements = []
                management_server = {}
                management_server['id'] = "1"
                management_server['mgmt_server'] = entry['ip_address']
                managements.append(management_server)
                self.write_complex_metric_object_array("management-servers",{},managements, True,"Management Server")
        return self.output