import os
import unittest
from checkpoint.firewall.log_server_connection_novsx.log_server_connection_novsx import LogServerConnectionNoVsx
from parser_service.public.action import *

class TestLogServerConnectionNoVsx(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = LogServerConnectionNoVsx()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_R77_30_log_server_connected(self):
        extracted_data = self.parser.parse_file(self.current_dir + '/R77_30_log_server_connected.input', {}, {})
        self.assertEqual(5,len(extracted_data))
        self.assertEqual('192.168.197.30', extracted_data[2].tags['name'])
        self.assertEqual('192.168.197.31', extracted_data[3].tags['name'])

    def test_R77_30_log_servers_not_connected(self):
        extracted_data = self.parser.parse_file(self.current_dir + '/R77_30_log_servers_not_connected.input', {}, {})
        self.assertEqual(3, len(extracted_data))
        self.assertEqual('192.168.197.30', extracted_data[2].value[0]['server_ip'])
        self.assertEqual('192.168.197.31', extracted_data[2].value[1]['server_ip'])

    def test_log_servers_connected_but_clm_down(self):
        extracted_data = self.parser.parse_file(self.current_dir + '/log_servers_connected_but_clm_down.input', {}, {})
        self.assertEqual(5, len(extracted_data))
        self.assertEqual('192.168.197.30', extracted_data[2].tags['name'])
        self.assertEqual('192.168.197.31', extracted_data[3].tags['name'])

    def test_configured_to_log_locally(self):
        extracted_data = self.parser.parse_file(self.current_dir + '/configured_to_log_locally.input', {}, {})
        self.assertEqual(3, len(extracted_data))

    def test_no_data_returned(self):
        extracted_data = self.parser.parse_file(self.current_dir + '/no_data_returned.input', {}, {})
        self.assertEqual(0, len(extracted_data))

    def test_log_server_ip(self):
        extracted_data = self.parser.parse_file(self.current_dir + '/R77_30_log_server_connected.input', {}, {})
        self.assertEqual(5,len(extracted_data))
        self.assertEqual('192.168.197.30', extracted_data[4].value[0]['server_ip'])
        self.assertEqual('192.168.197.31', extracted_data[4].value[1]['server_ip'])

if __name__ == '__main__':
    unittest.main()