from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class LogServerConnectionNoVsx(BaseParser):

    @staticmethod
    def _get_key_value(key: str, data: dict):
        if key in data and data[key] != '':
            return data[key]
        return None

    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):

        # Log server state values:
        # 0=to log servers
        # 1=local configured
        # 2=local due to connectivity issues
        # 3=local due to high rate

        # 2/20/19 There are situations where the gateway has actually lost connectivity with the log server but the
        # system has decided not to report as such.
        # Instead, you should track the status values of the Overall and Local Logging Mode Status
        # If the CLM or logging server is down, the overall status and local log mode status will change value to 3
        # Eventually, the status changes to 2 if the log servers are detected as being down

        # Data Extraction
        # The extracted data should look like this:
        # [{'overall_state': '0', 'local_state': '0', 'server_ip': '', 'server_state': ''},
        # {'overall_state': '', 'local_state': '', 'server_ip': '192.168.197.30', 'server_state': '0'},
        # {'overall_state': '', 'local_state': '', 'server_ip': '192.168.197.31', 'server_state': '2'}]
        extracted_data = helper_methods.parse_data_as_list(raw_data, 'log_server_connection_novsx.textfsm')

        # Step 2 : Data Processing
        if extracted_data is not None and len(extracted_data) != 0:
            general_state = extracted_data[0]
            overall_state = LogServerConnectionNoVsx._get_key_value('overall_state', general_state)
            local_state = LogServerConnectionNoVsx._get_key_value('local_state', general_state)

            if local_state is not None:
                local_state = int(local_state)
                # Check Point reports states > 1 state as 'bad', so we need to flip the logic here.
                # 0 - logging to log server; 1 - configured to log locally - intentional
                not_logging_locally = 0  # i.e., we _are_ logging locally (not not) -- this is the alert state
                if local_state < 2:      # Anything greater than 1 is a bad state
                    not_logging_locally = 1
                self.write_double_metric('not-logging-locally', {}, 'gauge', not_logging_locally, False)
                self.write_double_metric('local-logging-status-live-config', {}, 'gauge', local_state, True, 'Local Logging Mode Status', 'number')

# TODO IKP-4113 need to review and consider whether to delete or re-add
#            if overall_state is not None and local_state is not None:
#                overall_state = int(overall_state)
#                log_server_is_communicating = 1
#                # 0 - logging to log server; 1 - configured to log locally
#                if local_state > 1 and overall_state > 1:
#                    log_server_is_communicating = 0
#                overall_tag = {'name': 'All Log Servers'}
#                self.write_double_metric('log-server-communicating', overall_tag, 'gauge', log_server_is_communicating, False)


            server_ip_data = []    
            if len(extracted_data) > 1:
                for i in range (len (extracted_data)):
                    data_line = extracted_data[i]
                    server_ip = LogServerConnectionNoVsx._get_key_value('server_ip', data_line)
                    server_state = LogServerConnectionNoVsx._get_key_value('server_state', data_line)
                    if server_ip is not None:
                        entry_data = {}
                        entry_data ['server_ip'] = server_ip
                        server_ip_data.append(entry_data)
                    if server_state is not None:
                        server_state = int(server_state)
                        report_state = 0
                        if server_state == 0 or server_state == 2:
                                report_state = 1
                                self.write_double_metric('log-server-communicating', {'name': server_ip}, 'gauge', report_state, False)
            self.write_complex_metric_object_array('logging-server-ip', {}, server_ip_data, False)

        # The method must return self.output
        return self.output