
from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class InterfaceErrors(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            lines = raw_data.strip().splitlines()
            check_string = "Fake Tx hang detected with timeout of"
            if lines:
                interface_name_set = []
                for line in lines:
                    if check_string in line:
                        interface_name = line.strip().split()[8][:-1]
                        if interface_name:
                            if interface_name  not in interface_name_set:
                                interface_name_set.append(interface_name)
                for interface in interface_name_set:
                    tags = {'name': interface}
                    self.write_double_metric('fake-tx-hang', tags, 'gauge', 0, False, "name")
        return self.output
