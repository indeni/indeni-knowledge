import os
import unittest

from checkpoint.firewall.interface_fake_tx_hang.interface_fake_tx_hang import InterfaceErrors
from parser_service.public.action import WriteDoubleMetric


class TestLowSevParser1(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = InterfaceErrors()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_valid_input(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/test_valid_2_items.txt', {}, {})

        # Assert
        self.assertEqual(2, len(result))

        self.assertTrue(isinstance(result[0], WriteDoubleMetric))
        self.assertEqual('eth1-01', result[0].tags['name'])
        self.assertEqual('0', str(result[0].value))

        self.assertTrue(isinstance(result[1], WriteDoubleMetric))
        self.assertEqual('eth1-02', result[1].tags['name'])
        self.assertEqual('fake-tx-hang', str(result[1].name))
        self.assertEqual('gauge', str(result[1].ds_type))


    def test_failed_input(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/failed_input.input', {}, {})

        # Assert
        self.assertEqual(0, len(result))


if __name__ == '__main__':
    unittest.main()
