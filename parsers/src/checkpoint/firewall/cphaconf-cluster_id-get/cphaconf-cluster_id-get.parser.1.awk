# Check Point relies on a made up MAC address to communicate when two or more gateways are formed into a cluster.
# Since they have all had the default value, there are issues when placing more than one cluster on the same L2 segment.
# To avoid this the magic MAC needs to be changed in these scenarios.
# There are three different ways of doing this, depending on version
# Below R77.30 (method 1)
#   - Set the magic MAC manually with kernel parameters
#
# R77.30 - R80 (method 2)
#   - Set the magic MAC via a new command, called "cphaconf cluster_id"
#
# R80.10 - (method 3)
#   - The gateway listens which MAC addresses other clusters uses, and select another one.
#
# The magic MAC should not be confused with the cluster-id tag
# This script will check several things
# 1. Is the magic mac set in the fwkern.conf file? If so then it should not be set with the "cphaconf cluster_id set" command.
# 2. Is the magic mac set in the fwkern.conf different from the one set with "cphaconf cluster_id set"?
# 3. Which magic mac is the one currently set?

BEGIN {
    fwha_mac = "NO ID FOUND"
    fwha_mac_boot = ""
    fwha_forward_boot = ""
}

###########################################################################
#
#       Read values if they are set in the fwkern.conf file
#
###########################################################################

#fwha_mac_magic=254
/^fwha_mac_magic=[0-9]/ {
    split($0, mac_magic_split_arr, "=")
    fwha_mac_boot = mac_magic_split_arr[2]
}

#fwha_mac_forward_magic=253
/^fwha_mac_forward_magic=[0-9]/ {
    split($0, mac_forward_split_arr, "=")
    fwha_forward_boot = mac_forward_split_arr[2]
}

###########################################################################
#
#       Read kernel values. They are set in a pre R80.10 environment
#
###########################################################################

#fwha_mac_magic = 120
/^fwha_mac_magic/ {
    fwha_mac = $3
    # It is not recommended to set the value in fwkern.conf as well.
    if (fwha_mac_boot != "") {
        warning = 1
    }
}

#fwha_mac_forward_magic = 119
/^fwha_mac_forward_magic/ {
    # It is not recommended to set the value in fwkern.conf as well.
    if (fwha_forward_boot != "") {
        warning = 1
    }
}


###########################################################################
#
#       Read values if R80.10 or higher.
#
###########################################################################


#MAC magic:         1
/^MAC magic: / {
    fwha_mac = $3
}

###########################################################################
#
#       If cluster ID is different in kernel from boot.conf
#
###########################################################################

# cphaconf cluster_id: WARNING: different values for cluster_id: kernel VALUE_1 ha_boot.conf: VALUE_2
/WARNING: different values for cluster_id/ {
    warning = 1
}

###########################################################################
#
#       END
#
###########################################################################

END {
    writeComplexMetricString("cluster-id-number", null, fwha_mac, "false")

    # If both the new and olf method is configured at the same time, that is not good
    if (warning == 1) {
        conflict = "true"
    } else {
        conflict = "false"
    }
    writeComplexMetricString("chkp-cluster-id-conflict", null, conflict, "false")
}