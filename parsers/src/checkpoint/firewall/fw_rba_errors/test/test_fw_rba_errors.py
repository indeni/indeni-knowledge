import os
import unittest

from checkpoint.firewall.fw_rba_errors.fw_rba_errors import RbaErrors
from parser_service.public.action import *


class TestRbaErrors(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = RbaErrors()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_fw_rba_errors_cli_error(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/fw_rba_errors_cli_error.input', {}, {})
        # Assert
        self.assertEqual(1, len(result))

    def test_fw_rba_errors_cli_no_access(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/fw_rba_errors_cli_no_access.input', {}, {})
        # Assert
        self.assertEqual(1, len(result))

    def test_fw_rba_errors_empty(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/fw_rba_errors_empty.input', {}, {})
        # Assert
        self.assertEqual(0, len(result))


if __name__ == '__main__':
    unittest.main()
