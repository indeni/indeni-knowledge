
from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class RbaErrors(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            tags = {}
            data = helper_methods.parse_data_as_list(raw_data, 'fw_rba_errors.textfsm')
            if data:
                for username in data:
                    self.write_double_metric('fw_rba_errors', tags, 'gauge', 0, False, '')

        return self.output
