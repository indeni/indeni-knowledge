# SSH Command Notes
# See: https://indeni.atlassian.net/browse/IKP-2110
# We've had problems with scripts (and SSH commands) taking a very long time to run on large VSX deployments. This
# command includes some performance optimizations; e.g., it uses xargs to run the subsequent Check Point CLI commands
# in parallel bash processes. Here's the basic outline:
# * Capture the VSID and Name and pass each one to xargs.
# * xargs 'calls' bash, spawning it in up to -P processes.
# * Initialize the shell with /etc/profile.d/vsenv.sh. Required to run vsenv command.
# * Split out the VS ID and name from idAndName -- using idAndName like this avoids having to run 'vsx stat' for each VS.
# * Set the vsenv and run the actual Check Point command. Capture the output into 'result' and echo it.
# Specific notes:
# -n max args; i.e., deal with one VS at a time
# -P max number of parallel processes
# -I{} -- in the following command to xargs, for each line of input from stdin, replace {} with that input. In this case, run bash passing each line from stdin as the variable 'idAndName'.
# "" -> get value of var as string
# '' -> print/create this exact string
# `` -> execute this string


function addVsTags(tags) {
    tags["vs.id"] = vsid
    tags["vs.name"] = vsname
}

BEGIN {
    vsid=""
    vsname=""
}

#_VSID:Name:Type 1:lab-CP-VSX1_VSW:Switch
/^_VSID:Name:Type/ {
    split($2, id_name_type, ":")
    vsid = id_name_type[1]
    vsname = id_name_type[2]
    type = id_name_type[3]
}

# localhost InitialPolicy 12Feb2017 19:13:38 :  [>eth0] [<eth0]
/^localhost/ {
	policyName=$2
	fingerprint = policyName
}

# 16f7b38c2a9e2f96a6faf3000f2050ff  /opt/CPsuite-R77/fw1/state/local/FW1/local.str
#/[a-f0-9]{32}/ {
/local\.str/ {
	if (fingerprint == "-") {
		fingerprint = ""
	} else if ($1 ~ /[a-f0-9]{32}/) {
		fingerprint = fingerprint " " $1
	}

	addVsTags(policytags)
	# Should not write a metric if this is a virtual switch, since they do not have a policy.
	if (type != "Switch") {
		writeComplexMetricString("policy-installed-fingerprint", policytags, fingerprint, "false")  # Converted to new syntax by change_ind_scripts.py script
	}
}