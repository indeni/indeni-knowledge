from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

drivers_allowed = ['igb', 'ixgbe', 'i40e', 'i40evf', 'mlx5_core', 'ena', 'virtio_net', 'vmxnet3']

class MqMngShow(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data_parsed = helper_methods.parse_data_as_list(raw_data, 'mq_mng_show.textfsm')
            for interface in data_parsed:
                tags = {}
                tags['name'] = interface['int_name']
                self.write_double_metric('multi-queue-mode', tags, 'gauge', 0 if interface['mq_mode'] in ['Off', 'None'] and interface['driver'] in drivers_allowed else 1, True, 'Multi-Queue Status', 'state', 'name')
        return self.output