import unittest
from checkpoint.firewall.mq_mng_show.mq_mng_show_gw.mq_mng_show_gw import MqMngShow
from parser_service.public.action import *

class TestMqMngShow(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = MqMngShow()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))



    def test_all_int_mq_ok(self):
        extracted_data = self.parser.parse_file(self.current_dir + '/all_int_mq_ok.input', {}, {})
        self.assertEqual(4,len(extracted_data))
        self.assertEqual(extracted_data[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(extracted_data[0].name, 'multi-queue-mode')
        self.assertEqual(extracted_data[0].value, 1)
        self.assertEqual(extracted_data[0].tags['name'], 'eth0')
        self.assertEqual(extracted_data[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(extracted_data[1].name, 'multi-queue-mode')
        self.assertEqual(extracted_data[1].value, 1)
        self.assertEqual(extracted_data[1].tags['name'], 'eth1')
        self.assertEqual(extracted_data[2].action_type, 'WriteDoubleMetric')
        self.assertEqual(extracted_data[2].name, 'multi-queue-mode')
        self.assertEqual(extracted_data[2].value, 1)
        self.assertEqual(extracted_data[2].tags['name'], 'eth2')
        self.assertEqual(extracted_data[3].action_type, 'WriteDoubleMetric')
        self.assertEqual(extracted_data[3].name, 'multi-queue-mode')
        self.assertEqual(extracted_data[3].value, 1)
        self.assertEqual(extracted_data[3].tags['name'], 'eth3')

    def test_one_int_mq_none(self):
        extracted_data = self.parser.parse_file(self.current_dir + '/one_int_mq_none.input', {}, {})
        self.assertEqual(4,len(extracted_data))
        self.assertEqual(extracted_data[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(extracted_data[0].name, 'multi-queue-mode')
        self.assertEqual(extracted_data[0].value, 1)
        self.assertEqual(extracted_data[0].tags['name'], 'eth0')
        self.assertEqual(extracted_data[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(extracted_data[1].name, 'multi-queue-mode')
        self.assertEqual(extracted_data[1].value, 1)
        self.assertEqual(extracted_data[1].tags['name'], 'eth1')
        self.assertEqual(extracted_data[2].action_type, 'WriteDoubleMetric')
        self.assertEqual(extracted_data[2].name, 'multi-queue-mode')
        self.assertEqual(extracted_data[2].value, 0)
        self.assertEqual(extracted_data[2].tags['name'], 'eth2')
        self.assertEqual(extracted_data[3].action_type, 'WriteDoubleMetric')
        self.assertEqual(extracted_data[3].name, 'multi-queue-mode')
        self.assertEqual(extracted_data[3].value, 1)
        self.assertEqual(extracted_data[3].tags['name'], 'eth3')

    def test_one_int_mq_off(self):
        extracted_data = self.parser.parse_file(self.current_dir + '/one_int_mq_off.input', {}, {})
        self.assertEqual(4,len(extracted_data))
        self.assertEqual(extracted_data[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(extracted_data[0].name, 'multi-queue-mode')
        self.assertEqual(extracted_data[0].value, 1)
        self.assertEqual(extracted_data[0].tags['name'], 'eth0')
        self.assertEqual(extracted_data[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(extracted_data[1].name, 'multi-queue-mode')
        self.assertEqual(extracted_data[1].value, 1)
        self.assertEqual(extracted_data[1].tags['name'], 'eth1')
        self.assertEqual(extracted_data[2].action_type, 'WriteDoubleMetric')
        self.assertEqual(extracted_data[2].name, 'multi-queue-mode')
        self.assertEqual(extracted_data[2].value, 0)
        self.assertEqual(extracted_data[2].tags['name'], 'eth2')
        self.assertEqual(extracted_data[3].action_type, 'WriteDoubleMetric')
        self.assertEqual(extracted_data[3].name, 'multi-queue-mode')
        self.assertEqual(extracted_data[3].value, 1)
        self.assertEqual(extracted_data[3].tags['name'], 'eth3')

if __name__ == '__main__':
    unittest.main()