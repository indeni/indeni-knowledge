from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser
#from checkpoint.firewall.mq_mng_show.mq_mng_show_gw.mq_mng_show_gw import MqMngShow
import re

drivers_allowed = ['igb', 'ixgbe', 'i40e', 'i40evf', 'mlx5_core', 'ena', 'virtio_net', 'vmxnet3']

class MqMngShow(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data_parsed = helper_methods.parse_data_as_list(raw_data, 'mq_mng_show.textfsm')
            for interface in data_parsed:
                tags = {}
                tags['name'] = interface['int_name']
                self.write_double_metric('multi-queue-mode', tags, 'gauge', 0 if interface['mq_mode'] in ['Off', 'None'] and interface['driver'] in drivers_allowed else 1, True, 'Multi-Queue Status', 'state', 'name')
        return self.output
class MqMngShowSp(BaseParser):

    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            vs_info_parsed = helper_methods.parse_data_as_list(raw_data, 'vs_info.textfsm')
            blade_vs_regex = r'(^\d+)\_(\d+)\:?(\d+)?$'
            outputs = re.split(blade_vs_regex, raw_data, 0, re.MULTILINE)
            outputs.pop(0)
            id = 0
            while id < len(outputs):
                blade_metrics = MqMngShow.parse(MqMngShow() ,outputs[id+3], {}, {})
                for metric in blade_metrics:
                    name = metric.tags['name']
                    if outputs[id+2] is not None:
                        metric.tags['vs.id'] = outputs[id+2]
                        metric.tags['vs.name'] = [vs_info['vs_name'] for vs_info in vs_info_parsed if vs_info['vs_id']== outputs[id+2]][0]
                        metric.tags['name'] = ' '.join(('VSID_{}'.format(outputs[id+2]), metric.tags['name']))
                    if outputs[id+1] is not None and outputs[id+1] != '99':
                        metric.tags['blade'] = outputs[id+1]
                        metric.tags['name'] = ' '.join(('blade_{}'.format(outputs[id+1]), metric.tags['name']))
                    if metric.name == 'multi-queue-mode':
                        self.write_double_metric('multi-queue-mode', metric.tags, 'gauge', metric.value, metric.tags.get('live-config'), metric.tags.get('display-name'), metric.tags.get('im.dstype.displayType'), metric.tags.get('im.identity-tags'))
                id += 4
        return self.output