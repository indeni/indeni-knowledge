from parser_service.public.base_parser import BaseParser
from parser_service.public import helper_methods

class ChkpContractFileSize(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            try:
                contract_file_size = int(raw_data)/1024/1024 # Passing value from bytes to MB
                self.write_double_metric('chkp-contract-file-size', {}, 'gauge', contract_file_size, False)
            except:
                pass
        return self.output