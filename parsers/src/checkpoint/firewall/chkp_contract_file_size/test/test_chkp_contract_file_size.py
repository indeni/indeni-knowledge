import os
import unittest
from checkpoint.firewall.chkp_contract_file_size.chkp_contract_file_size import ChkpContractFileSize
from parser_service.public.action import *

class TestChkpContractFileSize(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = ChkpContractFileSize()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))    
    
    def test_contract_file_size_high(self):
        result = self.parser.parse_file(self.current_dir + '/chkp_contract_file_size_high.input', {}, {})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'chkp-contract-file-size')
        self.assertEqual(result[0].value, 1.5028610229492188)

    def test_contract_file_size_low(self):
        result = self.parser.parse_file(self.current_dir + '/chkp_contract_file_size_low.input', {}, {})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'chkp-contract-file-size')
        self.assertEqual(result[0].value, 0.0021047592163085938)

    def test_chkp_contract_file_size_invalid(self):
        result = self.parser.parse_file(self.current_dir + '/chkp_contract_file_size_invalid.input', {}, {})
        self.assertEqual(0,len(result))

if __name__ == '__main__':
    unittest.main()