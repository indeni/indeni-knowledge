# create a set of the cpu id's
function storeCpuAssignments(store) {
    for (i = 1; i <= NF; i++) {  # Loop through the field identifiers.
        if ($i ~ /^[0-9]+$/) {   # If the field is exactly one or more digits and nothing else,
            store[$i] = ""       # set the map key to field value, and the val of the key to empty.
            if ($i > highest_core_id)
                highest_core_id = $i
        }
    }
}

BEGIN {
    highest_core_id = 0
    is_core_conflict = 0
}

#eth1-05: CPU 0
#Ge0-0: CPU 0
#Te0-1: CPU 0
#fw_0: CPU 5
#VS_1 fwk: CPU 1 2 3
#Daemon mpdaemon: CPU 1 2 3
/CPU [0-9]/ {
#Aug 22, 2019 - added "|^loop.*:\s*CPU" to parse out loopback
#  This hasn't been tested in the lab because you cannot assign a core to a loopback interface
#  https://community.checkpoint.com/t5/General-Topics/CoreXL-and-Loopback/m-p/61613#M12481
    if ($0 ~ /^eth.*:\s*CPU|^Ge.*:\s*CPU|^Te.*:\s*CPU|^Sync.*:\s*CPU|^Mgmt.*:\s*CPU|^loop.*:\s*CPU/) {
        storeCpuAssignments(snd_assignments)
    } else {
        storeCpuAssignments(fw_assignments)
    }
}

END {

    # If there are only 2 cores, then its normal for the SND and fw_worker to share CPU cores;
    # never alert on this.
    if (highest_core_id > 1) {
        if (arraylen(fw_assignments) > 0 && arraylen(snd_assignments) > 0) {
            # Check if the same CPU ID is both in fw and snd arrays
            for (id in snd_assignments) {
                if (id in fw_assignments) {
                    is_core_conflict = 1
                    break
                }
            }
        } else {
            # TODO: this may indicate an error state that would be worth logging if/when we add error logging
            # TODO: to .ind scripts.
        }
    }

    writeDoubleMetric("corexl-core-assigned-workers-and-nics", null, "gauge", is_core_conflict, "false")  # Converted to new syntax by change_ind_scripts.py script
}