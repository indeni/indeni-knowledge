############
# A cluster member may have the following states:
#
######   These states are OK:    #################
# Active - Everything is OK.
# Ready - This member will take over if the Active one goes down, but it is not syncing state tables, so the failover will cut all connections.
# Standby - In HA, the member is waiting for an active member to fail in order to start packet forwarding.
# Initializing - Transient state. The cluster member is booting up, but the Security Gateway is not yet ready.
# Active (Under Freeze) OR Standby (Under Freeze) - A policy is being pushed on the cluster. This is a normal,
#     transient state. We should not alert in this case.
# backup - Applies only to a VSX Cluster in Virtual System Load Sharing mode with three or more Cluster Members configured.
#       State of a Virtual System on a third (and so on) VSX Cluster Member.
# init - The Cluster Member is in the phase after the boot and until the Full Sync completes
#
######   These states are a problem: ##############
# Down - a critical device is down.
# Active Attention - A problem has been detected, but the cluster member is still forwarding packets. This member is not healthy
#     and would like to fail over, but there is no other member that can take over. In # any other situation the state of the member would be down.
# ClusterXL Inactive or Machine is Down - Only remote peers can have this state. The local member cannot talk to this cluster member.
# HA module not started  - Only local members can have this state. For some reason, HA process is stopped.
#       ****    R80.20 new states  *****
# Ref link : https://sc1.checkpoint.com/documents/R80.20_GA/WebAdminGuides/EN/CP_R80.20_CLI_ReferenceGuide/html_frameset.htm?topic=documents/R80.20_GA/WebAdminGuides/EN/CP_R80.20_CLI_ReferenceGuide/195509
# active(!) , active(!f) , active(!p) , active(!fp) -A problem was detected, but the Cluster Member still forwards packets, because it is 
#         the only member in the cluster, or because there are no other Active members in the cluster. 
#         In any other situation, the state of the member is Down. 
# lost - The peer Cluster Member lost connectivity to this local Cluster Member (for example, while the peer Cluster Member is rebooted).

### Some of these states refer to aspects of the cluster as a whole: #############
## Redundancy (High Availability)
# The non-active member could be:
# Standby - No problems: the member is waiting for an active member to fail in order to start packet forwarding.
# Ready - This member will take over if the Active one goes down, but it is not syncing state tables, so the failover will cut all connections.
# Down - This member will not take over in the case that the active member goes offline.
# ClusterXL Inactive or Machine is Down - This member will not take over in the case that the active member goes offline.

## Health   #################
# Active - This member is healthy and forwarding traffic.
# Active Attention - A problem has been detected, but the cluster member is still forwarding packets. This member is not healthy
#     and would like to fail over, but there is no other member that can take over. In # any other situation the state of the member would be down.
###########

function setTags() {
    tags["vs.id"] = vs_id
    tags["vs.name"] = vs_name
    tags["name"] = "ClusterXL"
    tags["im.identity-tags"] = "vs.id|vs.name"
}

function dumpClusterStateData() {
    setTags()
    #Writing up the metric's only if we have full-data in output
    if (have_full_data == 1) {
        # If we found at least one member forwarding traffic, and no cluster members in an unhealthy state then its ok
        cluster_state = 0
        if (cluster_is_active && cluster_is_healthy) {
            cluster_state = 1
            cluster_state_description = "Active"
        }

        # Write status of all members
        writeComplexMetricObjectArray("cluster-member-states", tags, all_members, "false")  # Converted to new syntax by change_ind_scripts.py script

        if (vs_type != "Switch") {
            writeComplexMetricString("cluster-member-active-live-config", tags, local_state_description, "true", "Cluster member state (this)")  # Converted to new syntax by change_ind_scripts.py script
            writeDoubleMetric("cluster-member-active", tags, "gauge", local_state, "false")  # Converted to new syntax by change_ind_scripts.py script
            writeComplexMetricString("cluster-state-live-config", tags, cluster_state_description, "true", "Cluster State")  # Converted to new syntax by change_ind_scripts.py script
            writeDoubleMetric("cluster-state", tags, "gauge", cluster_state, "false")  # Converted to new syntax by change_ind_scripts.py script
        }
    }
}

function resetVariables() {
    cluster_is_active = 0
    cluster_is_healthy = 1
    cluster_state_description = ""
    all_members_index = 0
    local_state = 0
    local_state_description = "Unknown"
    vs_id = ""
    vs_name = ""
    delete tags
    state = ""
    have_full_data = 0
    delete all_members

}

function buildState(base_state) {
    new_state = base_state
    for (a_field = NF - 1 ; a_field > 2; a_field--) {
        if (index($a_field, "%"))
            break
        else
            new_state = $a_field " " new_state
    }
    return new_state
}


BEGIN {
    resetVariables()
    name_index = 0
}

#VSID:            0
/^VSID:\s+[0-9]/ {
    # Dump data of previous VS if needed
    if (vs_id != "") {
        dumpClusterStateData()
    }
    resetVariables()
    vs_id = $NF
}

#Name:            lab-CP-VSX1-R7730
/^Name:\s+/ {
    vs_name = trim($NF)
}

#Type:            VSX Gateway
#Type:            Virtual Switch
/^Type:\s+/ {
    vs_type = trim($NF)
}

# Determine cluster mode
#Cluster Mode:   VSX High Availability (Active Up) with IGMP Membership
#Cluster Mode:   Virtual System Load Sharing
/^Cluster Mode:\s+/ {
    cluster_mode = trim(substr($0, index($0, ":") + 1))
    setTags()
    writeComplexMetricString("cluster-mode", tags, cluster_mode, "false")  # Converted to new syntax by change_ind_scripts.py script
}

# Match local cluster member not having cluster services started
#HA module not started.
/^HA module not started./ {
    #setting have_full_data to 1 to make sure we have recieved complete output data 
    have_full_data = 1
    cluster_is_healthy = 0
    cluster_state_description = "Unhealthy: " $0
}

/^ID/{
    name_index = index($0, "Name")
}

/^Number/ {
    name_index = -1
}

#1          192.168.252.253 0%              Standby
#2 (local)  10.11.94.11     100%            Active
#2          10.11.94.11     0%              ClusterXL Inactive or Machine is Down
#1 (local)  192.100.102.101 100%            ACTIVE   vs0-local
/\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/ {
    #setting have_full_data to 1 to make sure we have recieved complete output data 
    have_full_data = 1
    number = $1
    unique_address = $2
    assigned_load = $3
    if ($2 == "(local)") {
        number = $1 " " $2
        unique_address = $3
        assigned_load = $4
    }

    line = trim($0)
    #name_index will be greater than zero if its R80.20 output, adding loop to handle it
    if (name_index > 0 ) {
        #Checking if the output has any data in Name field
        if (length(line) >= name_index) {
            state = $(NF - 1)
            for (field = NF - 2; field > 2; field--) {
                if (index($field, "%"))
                    break
                else
                    state = $field " " state
            }
        } else {
            #This loop handles if Name field is Empty
            state = buildState(trim($NF))
        }
    } else {
        state = buildState(trim($NF))
    }

    all_members_index++
    all_members[all_members_index, "state-description"] = state
    all_members[all_members_index, "id"] = $1
    all_members[all_members_index, "assigned-load-percentage"] = assigned_load
    all_members[all_members_index, "unique-ip"] = unique_address

    all_members[all_members_index, "is-local"] = 0
    if ($2 ~ /local/) {
        all_members[all_members_index, "is-local"] = 1

        # Here, we intentionally match "Active","Active Attention","Active(!)","Active(!P)","Active(!F)"and "Active(!FP)". Even though except "Active",
        # remaining mentioned states is usually a problem state, in this case we consider it ok because the member is still forwarding traffic.
        local_state = 0
        if ( tolower(state) ~ /active/ ) {
            local_state = 1
        }

        local_state_description = state
    }

    # NOTE: The following checks look at ALL members of the cluster.

    # For the cluster to be healthy overall, one member must be 'Active'. We intentionally match anything that starts
    # with "Active", except "Active Attention","Active(!)","Active(!P)","Active(!F)","Active(!FP)". E.g, Active (Under Freeze) is fine.
    if (tolower(state) ~ /^active/ && tolower(state) !~ /^active attention|active\(!\)|active\(!f\)|active\(!p\)|active\(!fp\)/) {
        cluster_is_active = 1
    }

    if (tolower(state) ~ /^active attention|down|active\(!\)|active\(!f\)|active\(!p\)|active\(!fp\)|lost/) {
        cluster_is_healthy = 0
        cluster_state_description = "Unhealthy: " state
    }
}


END {
    # Dump data of last VS
    if (vs_id != "") {
        dumpClusterStateData()
    }
}