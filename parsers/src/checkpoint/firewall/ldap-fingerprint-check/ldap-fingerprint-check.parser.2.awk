#${multistepList} = 192.168.197.14:899711ebae4fb70d5c6ac95b66c238b3:indeni.local__AD
BEGIN {
    # If the multistepList variable is empty, exit the script. This handles the case in which we find no ldap
    # fingerprints in step 1
    multistepList = dynamic("multistepList")
    if (multistepList == "") {
        exit
    }
}

#b4ccc832624f1f5707ef726ef0dc36cd  -
/^[a-f0-9]{32}/ {
    currentFingerprint = $1
}

#fingerprint: b4ccc832624f1f5707ef726ef0dc36cd
/^fingerprint: / {
    storedFingerprint = $2
}

#name: indeni.local__AD
/^name: / {
    # for LDAP name
    name = $2
}

END {
    # If the multistepList variable is empty, exit the script. This handles the case in which we find no ldap
    # fingerprints in step 1
    if (multistepList == "") {
        exit
    }

    # Make sure that we have two fingerprint to compare, otherwise report no issues.
    if (currentFingerprint && storedFingerprint) {
        if (currentFingerprint == storedFingerprint) {
            status = 1
        } else {
            status = 0
        }
    } else {
        status = 1
    }

    tags["name"] = name
    writeDoubleMetric("ldap-integration-fingerprint-matched", tags, "gauge", status, "false")  # Converted to new syntax by change_ind_scripts.py script
}