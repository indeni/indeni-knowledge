from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class FwaccelStatTopTalkersDstIpVsx(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            parsed_data = helper_methods.parse_data_as_list(raw_data, 'fwaccel_stat_top_talkers_dst_ip_vsx.textfsm')
            if parsed_data:
                vs_list=list(dict.fromkeys([entry['vs_id'] for entry in parsed_data]))
                for vs in vs_list:
                    vs_top_talkers = [x for x in parsed_data if x['vs_id']==vs]
                    parsed_data_ordered = sorted(vs_top_talkers, key=lambda d: int(d['conn_sum']), reverse=True)
                    tags = {}
                    tags['vs.id'] = parsed_data_ordered[0]['vs_id']
                    tags['vs.name'] = parsed_data_ordered[0]['vs_name']
                    tags['im.identity-tags'] = 'vs.id|vs.name'
                    order = 1
                    top_talkers = []
                    for talker in parsed_data_ordered:
                        item = {}
                        item['#'] = str(order)
                        item['dst_ip'] = talker['dst_ip']
                        item['counter'] = str(talker['conn_sum'])
                        top_talkers.append(item)
                        order = order+1
                    self.write_complex_metric_object_array('top-talker-dst-accelerate', tags, top_talkers, True, 'Top 10 Accelerated Connections to Destination IP Address')

        return self.output
