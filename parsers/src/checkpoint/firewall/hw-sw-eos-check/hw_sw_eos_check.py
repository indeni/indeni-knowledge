from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser
import calendar

############
# Why: Compare todays date against the time when the current device will be out of support, to alert before that happens.
# How: Get EOS data from check point webpage, and compare with todays date.
# Caveats: EOS list needs to be updated manually.
###########

# hardware and software EOS updated 18/02/2020
# Define all hardware and software end of support dates
# Based on https://www.checkpoint.com/support-services/support-life-cycle-policy/#softwaresupport
# Based on https://www.checkpoint.com/support-services/support-life-cycle-policy/#appliancessupport

# Hardware
# Model is only the model number, example: UTM-1 570 = 570
# Month/day is in number with 2 digits
hardware = {
    # Enterprise Appliance
    '2200': ['06', '2022'],
    '3100': ['12', '2025'],
    '3200': ['12', '2025'],
    '4200': ['06', '2022'],
    '4400': ['06', '2022'],
    '4600': ['06', '2022'],
    '4800': ['06', '2022'],
    '5100': ['12', '2025'],
    '5200': ['12', '2025'],
    '5400': ['12', '2025'],
    '5600': ['12', '2025'],
    '5800': ['09', '2025'],
    '5900': ['12', '2025'],
    '6500': ['12', '2025'],
    '6800': ['12', '2025'],
    '12200': ['06', '2022'],
    '12400': ['06', '2022'],
    '12600': ['06', '2022'],
    '12800': ['06', '2022'],
    '13500': ['06', '2022'],
    '13800': ['06', '2022'],
    '15400': ['12', '2025'],
    '15600': ['12', '2025'],
    '16000': ['03', '2027'],
    # High End Data Center Appliances
    '21400': ['06', '2022'],
    '21600': ['11', '2019'],
    '21700': ['06', '2022'],
    '21800': ['06', '2022'],
    '23500': ['12', '2025'],
    '23800': ['12', '2025'],
    '23900': ['12', '2025'],
    # UTM-1
    '130': ['04', '2017'],
    '270': ['04', '2017'],
    '570': ['04', '2017'],
    '1070': ['04', '2017'],
    '2070': ['04', '2017'],
    '3070': ['04', '2017'],
    '450': ['10', '2013'],
    '1050': ['10', '2013'],
    '2050': ['10', '2013'],
    # Power-1
    '5070': ['04', '2017'],
    '9070': ['04', '2017'],
    '11060': ['04', '2017'],
    '11070': ['04', '2017'],
    '11080': ['04', '2017'],
    # VSX
    '11285': ['04', '2017'],
    '11085': ['04', '2017'],
    '11275': ['04', '2017'],
    '11075': ['04', '2017'],
    '11265': ['04', '2017'],
    '11065': ['04', '2017'],
    '9090': ['04', '2017'],
    '9070': ['04', '2017'],
    '3070': ['04', '2017'],
    # Branch Office Appliances
    '1120': ['06', '2022'],
    '1140': ['06', '2022'],
    '1180': ['06', '2022'],
    '1430': ['10', '2024'],
    '1450': ['10', '2024'],
    '1470': ['10', '2024'],
    '1490': ['10', '2024'],
    # SMB Appliances
    '620': ['06', '2022'],
    '640': ['06', '2022'],
    '680': ['06', '2022'],
    '730': ['10', '2024'],
    '750': ['10', '2024'],
    '770': ['10', '2024'],
    '790': ['10', '2024'],
    '910': ['08', '2026'],
    # Ruggedized Appliances
    '1200R': ['12', '2025'],
    # Smart-1 Appliances
    '205': ['09', '2022'],
    '210': ['09', '2022'],
    '225': ['12', '2023'],
    '405': ['09', '2026'],
    '410': ['09', '2026'],
    '525': ['09', '2024'],
    '625': ['09', '2026'],
    '3050': ['12', '2023'],
    '3150': ['12', '2023'],
    '5150': ['09', '2026'],
    '5050': ['09', '2026'],
}

software = {
    'R70': ['03', '2013'],
    'R70.1': ['03', '2013'],
    'R70.20': ['03', '2013'],
    'R70.30': ['03', '2013'],
    'R70.40': ['03', '2013'],
    'R70.50': ['03', '2013'],
    'R71': ['04', '2014'],
    'R71.1': ['04', '2014'],
    'R71.20': ['04', '2014'],
    'R71.30': ['04', '2014'],
    'R71.40': ['04', '2014'],
    'R71.45': ['04', '2014'],
    'R71.50': ['04', '2014'],
    'R75': ['01', '2015'],
    'R75SP': ['04', '2017'],
    'R75.10': ['01', '2015'],
    'R75.30': ['08', '2015'],
    'R75.40VS': ['07', '2016'],
    'R75.40': ['04', '2016'],
    'R75.45': ['04', '2016'],
    'R75.46': ['04', '2016'],
    'R75.47': ['04', '2016'],
    'R76': ['02', '2017'],
    'R76SP': ['03', '2018'],
    'R77': ['08', '2017'],
    'R77.10': ['08', '2017'],
    'R77.20': ['08', '2017'],
    'R75.20': ['08', '2015'],
    'R77.30': ['09', '2019'],
    'R80': ['01', '2022'],
    'R80.10': ['01', '2022'],
    'R80.20': ['09', '2022'],
    'R80.20SP': ['02', '2023'],
    'R80.30': ['09', '2022'],
    'R80.40': ['04', '2024'],
    'R81': ['10', '2024'],
    'R81.10': ['07', '2025'],
    'R81.20': ['11', '2026']
}

software_embedded_gaia = {
    '600': {'R75.20': ['05', '2017'], 'R77.20.7': ['05', '2020'], 'R77.20.80': ['06', '2022']},
    '700': {'R77.20.7': ['05', '2020'], 'R77.20.80': ['06', '2021'], 'R77.20.81': ['06', '2021'], 'R77.20.85': ['10', '2024']},
    '900': {'R77.20.80': ['06', '2021'], 'R77.20.81': ['06', '2021'], 'R77.20.85': ['10', '2024']},
    '1100': {'R75.20': ['05', '2017'], 'R77.20.7': ['05', '2020'], 'R77.20.80': ['06', '2022']},
    '1200': {'R77.20.7': ['05', '2020'], 'R77.20.80': ['06', '2021'], 'R77.20.81': ['12', '2025']},
    '1400': {'R77.20.7': ['05', '2020'], 'R77.20.80': ['06', '2021'], 'R77.20.81': ['06', '2021'], 'R77.20.85': ['10', '2024']},
    '1500': {'R80.20': ['06', '2024'], 'R81.10': ['07', '2026']},
    '1600': {'R80.20': ['06', '2024'], 'R81.10': ['07', '2026']},
    '1800': {'R80.20': ['06', '2024'], 'R81.10': ['07', '2026']}
}


class HwSwEOSCheck(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        extracted_data = helper_methods.parse_data_as_list(raw_data, 'hw_sw_eos_check.textfsm')
        if extracted_data is not None:
            for entry in extracted_data:
                version = entry['version']
                model = entry['model']
                soft_version_month = None
                soft_version_year = None

                # For Gaia Embedded we need logic to compare for ex model 1100 to 1122
                # To do this we get the "major version": 1100 --> 11. Then, we try to match the 11 (from the original support
                # string) with the found version; e.g., 11 matches 1122. So, we know that 1122 is a 'model 11': it's valid.
                if version:
                    if version in software:
                        soft_version_month = software[version][0]
                        soft_version_year = software[version][1]

                if 'Gaia Embedded' in entry['os']:
                    if model:
                        model_index = model[:-2] + '00'
                        if model_index in software_embedded_gaia:
                            embedded_gaia_eos_dict = software_embedded_gaia[model_index]
                    if version:
                        if version.startswith('R77.20.7'):
                            version = 'R77.20.7'
                        if version in embedded_gaia_eos_dict:  # check if customer is using minor version for Gaia Embedded
                            embedded_gaia_eos = embedded_gaia_eos_dict[version]
                            soft_version_month = embedded_gaia_eos[0]
                            soft_version_year = embedded_gaia_eos[1]

                if soft_version_year and soft_version_month:
                    timetuplesw = (int(soft_version_year), int(soft_version_month), 1, 0, 0, 0, 0, 0, 0)
                    secssw = calendar.timegm(timetuplesw)
                    self.write_double_metric('software-eos-date', {}, 'gauge', secssw, True, 'Software End of Support', 'date', None)

                if model:
                    if model in hardware:
                        timetuplehw = (int(hardware[model][1]), int(hardware[model][0]), 1, 0, 0, 0, 0, 0, 0)
                        secshw = calendar.timegm(timetuplehw)
                        self.write_double_metric('hardware-eos-date', {}, 'gauge', secshw, True, 'Hardware End of Support', 'date', None)

        return self.output
