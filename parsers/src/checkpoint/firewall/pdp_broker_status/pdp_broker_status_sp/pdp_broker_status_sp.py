from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser
import re

class PdpBrokerStatusSp(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            vs_info_parsed = helper_methods.parse_data_as_list(raw_data, 'vs_info.textfsm')
            blade_vs_regex = r'(^\d+)\_(\d+)\:?(\d+)?'
            outputs = re.split(blade_vs_regex, raw_data, 0, re.MULTILINE)
            outputs.pop(0)
            id = 0
            while id < len(outputs):
                data_parsed_publishers = helper_methods.parse_data_as_list(outputs[id+3], 'pdp_broker_status_publishers.textfsm')
                data_parsed_subscribers = helper_methods.parse_data_as_list(outputs[id+3], 'pdp_broker_status_subscribers.textfsm')
                for publisher in data_parsed_publishers:
                    tags = {}
                    tags['name'] = publisher['name']
                    tags['ip-address'] = publisher['ip_addr']
                    if outputs[id+2] is not None:
                        tags['vs.id'] = outputs[id+2]
                        tags['vs.name'] = [vs_info['vs_name'] for vs_info in vs_info_parsed if vs_info['vs_id']== outputs[id+2]][0]
                        if outputs[id+1] != '99':
                            tags['name'] = ' '.join(('VSID_{}'.format(outputs[id+2]), tags['name']))
                    if outputs[id+1] is not None and outputs[id+1] != '99':
                        tags['blade'] = outputs[id+1]
                        tags['name'] = ' '.join(('blade_{}'.format(outputs[id+1]), tags['name']))
                    self.write_double_metric('pdp-broker-publisher-status', tags, 'gauge', 1 if publisher['status'] in ['Connected'] else 0, True, 'PDP Broker Status - Publishers', 'state', 'name')
                for subscribers in data_parsed_subscribers:
                    tags = {}
                    tags['name'] = subscribers['name']
                    tags['ip-address'] = subscribers['ip_addr']
                    tags['vs.id'] = 'test_tag'
                    if outputs[id+2] is not None:
                        tags['vs.id'] = outputs[id+2]
                        tags['vs.name'] = [vs_info['vs_name'] for vs_info in vs_info_parsed if vs_info['vs_id']== outputs[id+2]][0]
                        if outputs[id+1] != '99':
                            tags['name'] = ' '.join(('VSID_{}'.format(outputs[id+2]), tags['name']))
                    if outputs[id+1] is not None and outputs[id+1] != '99':
                        tags['blade'] = outputs[id+1]
                        tags['name'] = ' '.join(('blade_{}'.format(outputs[id+1]), tags['name']))
                    self.write_double_metric('pdp-broker-subscriber-status', tags, 'gauge', 1 if subscribers['status'] in ['Connected'] else 0, True, 'PDP Broker Status - Subscribers', 'state', 'name')
                id += 4
        return self.output