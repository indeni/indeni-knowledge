import unittest
from checkpoint.firewall.pdp_broker_status.pdp_broker_status_gw.pdp_broker_status_gw import PdpBrokerStatusGw
from parser_service.public.action import *

class TestPdpBrokerStatusGw(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = PdpBrokerStatusGw()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_suscribers_publishers(self):
        data = self.parser.parse_file(self.current_dir + '/suscribers_publishers.input', {}, {})
        self.assertEqual(4,len(data))
        self.assertEqual(data[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(data[0].name, 'pdp-broker-publisher-status')
        self.assertEqual(data[0].tags['name'], 'GW1')
        self.assertEqual(data[0].tags['ip-address'], '10.80.11.135')
        self.assertEqual(data[0].value, 1)
        self.assertEqual(data[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(data[1].name, 'pdp-broker-subscriber-status')
        self.assertEqual(data[1].tags['name'], 'GW1')
        self.assertEqual(data[1].tags['ip-address'], '10.80.11.135')
        self.assertEqual(data[1].value, 1)
        self.assertEqual(data[2].action_type, 'WriteDoubleMetric')
        self.assertEqual(data[2].name,'pdp-broker-subscriber-status')
        self.assertEqual(data[2].tags['name'], 'GW2')
        self.assertEqual(data[2].tags['ip-address'], '10.80.11.138')
        self.assertEqual(data[2].value, 1)
        self.assertEqual(data[3].action_type, 'WriteDoubleMetric')
        self.assertEqual(data[3].name, 'pdp-broker-subscriber-status')
        self.assertEqual(data[3].tags['name'], 'GW3')
        self.assertEqual(data[3].tags['ip-address'], '10.80.11.141')
        self.assertEqual(data[3].value, 1)


if __name__ == '__main__':
    unittest.main()