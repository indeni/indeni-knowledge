from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class PdpBrokerStatusGw(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data_parsed_publishers = helper_methods.parse_data_as_list(raw_data, 'pdp_broker_status_publishers.textfsm')
            data_parsed_subscribers = helper_methods.parse_data_as_list(raw_data, 'pdp_broker_status_subscribers.textfsm')
            for publisher in data_parsed_publishers:
                tags = {}
                tags['name'] = publisher['name']
                tags['ip-address'] = publisher['ip_addr']
                self.write_double_metric('pdp-broker-publisher-status', tags, 'gauge', 1 if publisher['status'] in ['Connected'] else 0, True, 'PDP Broker Status - Publishers', 'state', 'name')
            for subscribers in data_parsed_subscribers:
                tags = {}
                tags['name'] = subscribers['name']
                tags['ip-address'] = subscribers['ip_addr']
                self.write_double_metric('pdp-broker-subscriber-status', tags, 'gauge', 1 if subscribers['status'] in ['Connected'] else 0, True, 'PDP Broker Status - Subscribers', 'state', 'name')
        return self.output