from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class CloudVersion(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        # Step 1 : Data Extraction
        data = helper_methods.parse_data_as_list(raw_data, 'checkpoint_gaia_cat_etc_cloud_version.textfsm')

        # Step 2 : Data Processing
        if data:
            try:
                # Step 3 : Data Reporting
                self.write_tag('cloudguard-gateway', data[0]['platform'])
            except KeyError:
                pass
        return self.output
