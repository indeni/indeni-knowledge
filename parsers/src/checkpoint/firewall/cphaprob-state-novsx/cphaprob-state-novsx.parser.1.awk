############
# A cluster member may have the following states:
#
######   These states are OK:    #################
# Active - Everything is OK.
# Ready - This member will take over if the Active one goes down, but it is not syncing state tables, so the failover will cut all connections.
# Standby - In HA, the member is waiting for an active member to fail in order to start packet forwarding.
# Initializing - Transient state. The cluster member is booting up, but the Security Gateway is not yet ready.
# Active (Under Freeze) OR Standby (Under Freeze) - A policy is being pushed on the cluster. This is a normal,
#     transient state. We should not alert in this case.
# backup - Applies only to a VSX Cluster in Virtual System Load Sharing mode with three or more Cluster Members configured.
#       State of a Virtual System on a third (and so on) VSX Cluster Member.
# init - The Cluster Member is in the phase after the boot and until the Full Sync completes
#
######   These states are a problem: ##############
# Down - a critical device is down.
# Active Attention - A problem has been detected, but the cluster member is still forwarding packets. This member is not healthy
#     and would like to fail over, but there is no other member that can take over. In # any other situation the state of the member would be down.
# ClusterXL Inactive or Machine is Down - Only remote peers can have this state. The local member cannot talk to this cluster member.
# HA module not started  - Only local members can have this state. For some reason, HA process is stopped.
#       ****    R80.20 new states  *****
# Ref link : https://sc1.checkpoint.com/documents/R80.20_GA/WebAdminGuides/EN/CP_R80.20_CLI_ReferenceGuide/html_frameset.htm?topic=documents/R80.20_GA/WebAdminGuides/EN/CP_R80.20_CLI_ReferenceGuide/195509
# active(!) , active(!f) , active(!p) , active(!fp) -A problem was detected, but the Cluster Member still forwards packets, because it is 
#         the only member in the cluster, or because there are no other Active members in the cluster. 
#         In any other situation, the state of the member is Down. 
# lost - The peer Cluster Member lost connectivity to this local Cluster Member (for example, while the peer Cluster Member is rebooted).

### Some of these states refer to aspects of the cluster as a whole: #############
## Redundancy (High Availability)
# The non-active member could be:
# Standby - No problems: the member is waiting for an active member to fail in order to start packet forwarding.
# Ready - This member will take over if the Active one goes down, but it is not syncing state tables, so the failover will cut all connections.
# Down - This member will not take over in the case that the active member goes offline.
# ClusterXL Inactive or Machine is Down - This member will not take over in the case that the active member goes offline.

## Health   #################
# Active - This member is healthy and forwarding traffic.
# Active Attention - A problem has been detected, but the cluster member is still forwarding packets. This member is not healthy
#     and would like to fail over, but there is no other member that can take over. In # any other situation the state of the member would be down.
###########

# Parse out the cluster state, starting from the end of the line -- handles multi-word state value. E.g.,
#2          10.11.94.11     0%              ClusterXL Inactive or Machine is Down
function getClusterState(lastStateField, lastStateFieldNum){
    buildState = lastStateField
    for (field = lastStateFieldNum - 1 ; field > 2; field--) {
        if (index($field, "%"))
            break
        else
            buildState = $field " " buildState
    }
    return buildState
}


BEGIN {
    cluster_is_active = 0
    cluster_is_healthy = 1
    cluster_state_description = "Unhealthy"
    local_state = 0
    local_state_description = "Unknown"

    all_members_index = 0

    tags["name"] = "ClusterXL"
    name_index = 0
    
}

# This is R80.20 output. We use this info later to correctly parse out the state column values.
#ID         Unique Address  Assigned Load   State          Name
/^ID/{
    name_index = index($0, "Name")
}

# This is < R80.20 output
#Number     Unique Address  Assigned Load   State
/^Number/ {
    name_index = -1
}

# Determine cluster mode
#Cluster Mode:   VSX High Availability (Active Up) with IGMP Membership
/^Cluster Mode:\s+/ {
    cluster_mode = trim(substr($0, index($0, ":") + 1))
    writeComplexMetricString("cluster-mode", tags, cluster_mode, "false")  # Converted to new syntax by change_ind_scripts.py script
}


# Pre-R80.20, there is no member 'Name' column
#1          192.168.252.253 0%              Standby
#2 (local)  10.11.94.11     100%            Active
#2          10.11.94.11     0%              ClusterXL Inactive or Machine is Down

# R80.20 has an extra column for cluster member Name
#1 (local)  192.100.102.101 100%            ACTIVE    CP-R80.20-GW8-2
#2          1.1.1.1         0%              ClusterXL Inactive or Machine    CP-R80.20-GW8-1
/\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/ {

    number = $1
    unique_address = $2
    assigned_load = $3
    if ($2 == "(local)") {
        number = $1 " " $2
        unique_address = $3
        assigned_load = $4
    }
    
    line = trim($0)
    # name_index will be greater than zero if its R80.20 output
    if (name_index > 0 ) {
        # Checking if the output has any data in Name column field (R80.20)
        if (length(line) >= name_index){
            state = getClusterState($(NF - 1), NF - 1)
        } else {
            # This loop handles if Name column field is empty in R80.20 (seen in VSX deployments)
            state = getClusterState($NF, NF)
        }
    } else {
        state = getClusterState($NF, NF)  # pre-R80.20
    }
    
    all_members_index++
    all_members[all_members_index, "state-description"] = state
    all_members[all_members_index, "id"] = $1
    all_members[all_members_index, "assigned-load-percentage"] = assigned_load
    all_members[all_members_index, "unique-ip"] = unique_address

    all_members[all_members_index, "is-local"] = 0
    if ($2 ~ /local/) {
        all_members[all_members_index, "is-local"] = 1

        # Here, we intentionally match "Active","Active Attention","Active(!)","Active(!P)","Active(!F)"and "Active(!FP)". Even though except "Active",
        # remaining mentioned states is usually a problem state, in this case we consider it ok because the member is still forwarding traffic.
        local_state = 0
        if ( tolower(state) ~ /active/ ) {
            local_state = 1
        }

        local_state_description = state
    }

    # NOTE: The following checks look at ALL members of the cluster.

    # For the cluster to be healthy overall, one member must be 'Active'. We intentionally match anything that starts
    # with "Active", except "Active Attention","Active(!)","Active(!P)","Active(!F)","Active(!FP)". E.g, Active (Under Freeze) is fine.
    if (tolower(state) ~ /^active/ && tolower(state) !~ /^active attention|active\(!\)|active\(!f\)|active\(!p\)|active\(!fp\)/) {
        cluster_is_active = 1
    }

    if (tolower(state) ~ /^active attention|down|active\(!\)|active\(!f\)|active\(!p\)|active\(!fp\)|lost/) {
        cluster_is_healthy = 0
        cluster_state_description = cluster_state_description ": " state
    }

}

# Match local cluster member not having cluster services started
#HA module not started.
/^HA module not started./ {
    cluster_is_healthy = 0
    cluster_state_description = cluster_state_description ": " $0
}

END {
    writeComplexMetricString("cluster-member-active-live-config", tags, local_state_description, "true", "Local cluster member state (this device)")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("cluster-member-active", tags, "gauge", local_state, "false")  # Converted to new syntax by change_ind_scripts.py script

    # If we found at least one member forwarding traffic, and no cluster members in an unhealthy state then we say that
    # the overall cluster state is ok.
    cluster_state = 0
    if (cluster_is_active && cluster_is_healthy) {
        cluster_state = 1
        cluster_state_description = "Active"
    }

    writeComplexMetricString("cluster-state-live-config", tags, cluster_state_description, "true", "Cluster State")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("cluster-state", tags, "gauge", cluster_state, "false")  # Converted to new syntax by change_ind_scripts.py script

    writeComplexMetricObjectArray("cluster-member-states", tags, all_members, "false")  # Converted to new syntax by change_ind_scripts.py script
}