import os
import unittest

from checkpoint.firewall.log_kernel_connection_refused.log_kernel_connection_refused import ChkpLogKernelConnectionRefused


class TestChkpLogKernelConnectionRefused(unittest.TestCase):

    def setUp(self):
        self.parser = ChkpLogKernelConnectionRefused()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_some_errors(self):
        result = self.parser.parse_file(self.current_dir + '/some_errors.input', {}, {})
        self.assertEqual(3, len(result))
        self.assertEqual(result[0].value, 51)
        self.assertEqual(result[0].tags['name'], '/opt/CPsuite-R80.40/fw1/log/pdpd.elg')
        self.assertEqual(result[1].value, 36)
        self.assertEqual(result[1].tags['name'], '/opt/CPsuite-R80.40/fw1/log/pepd.elg')
        self.assertEqual(result[2].value, 999)
        self.assertEqual(result[2].tags['name'], '/opt/CPsuite-R80.40/fw1/log/vpnd.elg')

if __name__ == '__main__':
    unittest.main()
