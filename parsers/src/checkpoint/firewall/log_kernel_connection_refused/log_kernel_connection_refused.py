from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class ChkpLogKernelConnectionRefused(BaseParser):

    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        # Step 1 : Data Extraction
        data = helper_methods.parse_data_as_list(raw_data, 'log_kernel_connection_refused.textfsm')
        # Step 2 : Data Processing and Reporting
        log_kernel_connection_refused = []

        if data:
            for entry in data:
                index = [i for i, j in enumerate(log_kernel_connection_refused) if j['filename'] == entry['filename']]
                if len(index) > 0:
                    log_kernel_connection_refused[index[0]]['errors'] += int(entry['errors'])
                else:
                    new_entry = {'errors': int(entry['errors']), 'filename': entry['filename']}
                    log_kernel_connection_refused.append(new_entry)
            for data_to_return in log_kernel_connection_refused:
                self.write_double_metric('log-kernel-connection-refused', {'name': data_to_return['filename']}, 'counter', data_to_return['errors'], False)

        return self.output