from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class CphaprobIfNoVsx(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        # Step 1 : Data Extraction
        if raw_data and raw_data.strip() != '':
            states = helper_methods.parse_data_as_list(raw_data, 'checkpoint_gaia_cphaprob_a_if_state.textfsm')
            topology = helper_methods.parse_data_as_list(raw_data, 'checkpoint_gaia_cphaprob_a_if_topology.textfsm')
            meta_data = helper_methods.parse_data_as_list(raw_data, 'checkpoint_gaia_cphaprob_a_if_meta_data.textfsm')

            # Step 2: Data manipulation and report
            # Getting required and secured interfaces + ccp mode.

            tags = {}
            list_int_not_up = ""
            list_int_not_sec = ""
            # Getting number of UP interfaces, and up secured:
            if states and len(states)>0:
                up_interfaces = 0
                up_sync_interfaces = 0
                all_interfaces_names = []
                [all_interfaces_names.append(x['interface']) for x in states if x['interface'] not in all_interfaces_names]
                new_states_list = []
                for interface in all_interfaces_names:
                    new_item = {}
                    for item in states:
                        if item['interface'] == interface:
                            combined_keys = new_item.keys() | item.keys()
                            new_item = {k : new_item.get(k, '') + ' ' + item.get(k, '') for k in combined_keys}
                    new_states_list.append(new_item)

                for line in new_states_list:
                    if 'DOWN' in line['state']:
                        list_int_not_up = list_int_not_up + ' ' + line['interface'].split()[0]
                    elif 'UP' in line['state']:
                        up_interfaces += 1

                    # Getting the number of secured and non-secured:
                    if line['attribute'] and line['state'] :
                        if 'S' in line['attribute'] and 'UP' in line['state'] :
                            up_sync_interfaces += 1
                        else:
                            list_int_not_sec = list_int_not_sec + ' ' + line['interface']

                    if line['old_att']:
                        if '(secured)' in line['old_att'] and 'UP' in line['old_att']:
                            up_sync_interfaces += 1
                        else:
                            list_int_not_sec = list_int_not_sec + ' ' + line['interface'].split()[0]
                        if 'DOWN' in line['old_att']:
                            list_int_not_up = list_int_not_up + ' ' + line['interface'].split()[0]
                        elif 'UP' in line['old_att']:
                            up_interfaces += 1
                tags ['interfaces.down'] = list_int_not_up.lstrip()
                self.write_double_metric('cphaprob-up-interfaces', tags, 'gauge', up_interfaces, False)
                self.write_double_metric('cphaprob-up-secured-interfaces', tags, 'gauge', up_sync_interfaces, False)

            if meta_data and len(meta_data)>0:
                for line in meta_data:
                    if line['required_if_num']:
                        self.write_double_metric('cphaprob-required-interfaces', tags, 'gauge', int(line['required_if_num']), False)
                    if line['secured_if_num']:
                        self.write_double_metric('cphaprob-required-secured-interfaces', tags, 'gauge', int(line['secured_if_num']), False)
                    if line['CCP_mode'] and line['CCP_mode'] != 'Automatic':
                        self.write_complex_metric_string("clusterxl-ccp-mode", {}, line['CCP_mode'], True, "CCP Mode")

            # Getting the CCP mode state for old versions:
                ccp_old_list = [line['CCP_mode_old'] for line in states]
                ccp_old_clear_list = list(filter(None, ccp_old_list))
                if ccp_old_clear_list:
                    self.write_complex_metric_string("clusterxl-ccp-mode", {}, ccp_old_clear_list[0], True, "CCP Mode")

            # Getting the VIP and the corresponding interfaces
            if topology and len(topology)>0:
                topology_new_list = []
                for line in topology:
                    if_dict = {line['if_vip']: line['VIP']}
                    topology_new_list.append(if_dict)
                self.write_complex_metric_object_array('cluster-vip', {}, topology_new_list, False)
        return self.output

