import os
import unittest

from checkpoint.firewall.cphaprob_a_if_novsx.cphaprob_a_if_no_vsx import CphaprobIfNoVsx
from parser_service.public.action import WriteDoubleMetric


class TestCphaprobIfNoVsx(unittest.TestCase):
    def setUp(self):
        # Arrange
        self.parser = CphaprobIfNoVsx()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_test_R8X_splited_lines(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/test_valid_R8X_splited_lines.input', {}, {})

        # Assert
        self.assertEqual(6, len(result))

        self.assertTrue(result[0].name, 'cphaprob-up-interfaces')
        self.assertEqual(result[0].value, 1)
        self.assertTrue(isinstance(result[0], WriteDoubleMetric))

        self.assertTrue(result[1].name, 'cphaprob-up-secured-interfaces')
        self.assertEqual(result[1].value, 1)
        self.assertTrue(isinstance(result[1], WriteDoubleMetric))

        self.assertTrue(result[2].name, 'cphaprob-required-interfaces')
        self.assertEqual(result[2].value, 2)
        self.assertTrue(isinstance(result[2], WriteDoubleMetric))

        self.assertTrue(result[3].name, 'cphaprob-required-secured-interfaces')
        self.assertEqual(result[3].value, 1)
        self.assertTrue(isinstance(result[3], WriteDoubleMetric))

        self.assertTrue(result[4].name, 'clusterxl-ccp-mode')
        self.assertEqual(result[4].value['value'], 'Manual (Unicast)')
        self.assertEqual(result[4].action_type, 'WriteComplexMetric')

        self.assertTrue(result[5].name, 'cluster-vip')
        self.assertEqual(result[5].action_type, 'WriteComplexMetric')
        self.assertEqual(result[5].value[0]['eth0'], '10.11.94.145')    

    def test_valid_input_healthy_cluster_member(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/test_valid_input_healthy_cluster_member.input', {}, {})

        # Assert
        self.assertEqual(6, len(result))

        self.assertTrue(result[0].name, 'cphaprob-up-interfaces')
        self.assertEqual(result[0].value, 5)
        self.assertTrue(isinstance(result[0], WriteDoubleMetric))

        self.assertTrue(result[1].name, 'cphaprob-up-secured-interfaces')
        self.assertEqual(result[1].value, 1)
        self.assertTrue(isinstance(result[1], WriteDoubleMetric))

        self.assertTrue(result[2].name, 'cphaprob-required-interfaces')
        self.assertEqual(result[2].value, 5)
        self.assertTrue(isinstance(result[2], WriteDoubleMetric))

        self.assertTrue(result[3].name, 'cphaprob-required-secured-interfaces')
        self.assertEqual(result[3].value, 1)
        self.assertTrue(isinstance(result[3], WriteDoubleMetric))

        self.assertTrue(result[4].name, 'clusterxl-ccp-mode')
        self.assertEqual(result[4].value['value'], 'Manual (Unicast)')
        self.assertEqual(result[4].action_type, 'WriteComplexMetric')

        self.assertTrue(result[5].name, 'cluster-vip')
        self.assertEqual(result[5].action_type, 'WriteComplexMetric')

    def test_valid_input_azure_R80_40(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/test_valid_input_azure_R80_40.input', {}, {})

        # Assert
        self.assertEqual(6, len(result))

        self.assertTrue(result[0].name, 'cphaprob-up-interfaces')
        self.assertEqual(result[0].value, 1)
        self.assertTrue(isinstance(result[0], WriteDoubleMetric))

        self.assertTrue(result[1].name, 'cphaprob-up-secured-interfaces')
        self.assertEqual(result[1].value, 1)
        self.assertTrue(isinstance(result[1], WriteDoubleMetric))

        self.assertTrue(result[2].name, 'cphaprob-required-interfaces')
        self.assertEqual(result[2].value, 1)
        self.assertTrue(isinstance(result[2], WriteDoubleMetric))

        self.assertTrue(result[3].name, 'cphaprob-required-secured-interfaces')
        self.assertEqual(result[3].value, 1)
        self.assertTrue(isinstance(result[3], WriteDoubleMetric))

        self.assertTrue(result[4].name, 'clusterxl-ccp-mode')
        self.assertEqual(result[4].value['value'], 'Manual (Unicast)')
        self.assertEqual(result[4].action_type, 'WriteComplexMetric')

        self.assertTrue(result[5].name, 'cluster-vip')
        self.assertEqual(result[5].action_type, 'WriteComplexMetric')

    def test_valid_expansion_cards(self):
        expected_results = [
                            {'name': None, 'metric': 'cphaprob-up-interfaces', 'value': 6},
                            {'name': None, 'metric': 'cphaprob-up-secured-interfaces', 'value': 1},
                            {'name': None, 'metric': 'cphaprob-required-interfaces', 'value': 6},
                            {'name': None, 'metric': 'cphaprob-required-secured-interfaces', 'value': 1},
                            {'name': None, 'metric': 'clusterxl-ccp-mode', 'value': {'value': 'Manual (Unicast)'}},
                            {'name': None, 'metric': 'cluster-vip', 'value': [{'eth2-01': '134.159.65.104'}, {'eth2-02': '192.168.0.158'}, {'eth2-03': '192.168.0.190'}, {'eth2-04': '172.16.4.1'}, {'eth2-05': '192.168.101.4'}]}]

        # Act
        result = self.parser.parse_file(self.current_dir + '/test_valid_expansion_cards.input', {}, {})
        # Assert
        self.assertEqual(6, len(result))

        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i].get('name'), result[i].tags.get('name'))
            self.assertEqual(expected_results[i]['metric'], result[i].name)
            self.assertEqual(expected_results[i]['value'], result[i].value)


    def test_valid_with_vmac(self):
        expected_results = [
                            {'name': None, 'metric': 'cphaprob-up-interfaces', 'value': 3},
                            {'name': None, 'metric': 'cphaprob-up-secured-interfaces', 'value': 1},
                            {'name': None, 'metric': 'cphaprob-required-interfaces', 'value': 3},
                            {'name': None, 'metric': 'cphaprob-required-secured-interfaces', 'value': 1},
                            {'name': None, 'metric': 'clusterxl-ccp-mode', 'value': {'value': 'Manual (Unicast)'}},
                            {'name': None, 'metric': 'cluster-vip', 'value': [{'eth1-01': '172.18.10.5'}, {'eth1-02': '192.168.200.1'}, {'eth3': '195.59.5.62'}]}]

        # Act
        result = self.parser.parse_file(self.current_dir + '/test_valid_with_vmac.input', {}, {})
        # Assert
        self.assertEqual(6, len(result))

        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i].get('name'), result[i].tags.get('name'))
            self.assertEqual(expected_results[i]['metric'], result[i].name)
            self.assertEqual(expected_results[i]['value'], result[i].value)


    def test_valid_old_cluster(self):
        expected_results = [
                            {'name': None, 'metric': 'cphaprob-up-interfaces', 'value': 2},
                            {'name': None, 'metric': 'cphaprob-up-secured-interfaces', 'value': 1},
                            {'name': None, 'metric': 'cphaprob-required-interfaces', 'value': 3},
                            {'name': None, 'metric': 'cphaprob-required-secured-interfaces', 'value': 1},
                            {'name': None, 'metric': 'clusterxl-ccp-mode', 'value': {'value': 'multicast'}},
                            {'name': None, 'metric': 'cluster-vip', 'value': [{'eth1': '1.2.3.4'}, {'bond0.56': '172.30.56.1'}, {'bond0.215': '172.30.215.1'}, {'bond0.60': '172.30.60.1'}, {'bond0.43': '172.30.43.1'}, {'bond0.150': '172.30.150.1'}, {'bond0.160': '172.30.160.1'}, {'bond0.158': '172.30.158.1'}, {'bond0.85': '172.30.85.1'}, {'bond0.156': '172.30.156.1'}, {'bond0.109': '172.30.109.1'}, {'bond0.45': '172.30.45.1'}, {'bond0.670': '10.103.70.1'}, {'bond0.155': '172.30.155.1'}, {'bond0.154': '172.30.154.1'}, {'bond0.110': '172.30.110.1'}, {'bond0.46': '172.30.46.1'}]}]

        # Act
        result = self.parser.parse_file(self.current_dir + '/test_valid_old_cluster.input', {}, {})
        # Assert
        self.assertEqual(6, len(result))

        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i].get('name'), result[i].tags.get('name'))
            self.assertEqual(expected_results[i]['metric'], result[i].name)
            self.assertEqual(expected_results[i]['value'], result[i].value)


    def test_valid_embedded_cluster(self):
        expected_results = [
                            {'name': None, 'metric': 'cphaprob-up-interfaces', 'value': 5},
                            {'name': None, 'metric': 'cphaprob-up-secured-interfaces', 'value': 1},
                            {'name': None, 'metric': 'cphaprob-required-interfaces', 'value': 7},
                            {'name': None, 'metric': 'cphaprob-required-secured-interfaces', 'value': 1},
                            {'name': None, 'metric': 'clusterxl-ccp-mode', 'value': {'value': 'multicast'}},
                            {'name': None, 'metric': 'cluster-vip', 'value': [{'Mgmt': '1.2.3.4'}, {'Lan1': '172.30.56.1'}, {'Lan2': '172.30.215.1'}, {'Lan3': '172.30.60.1'}, {'Lan4': '172.30.43.1'}, {'Lan5': '172.30.150.1'}, {'Lan6': '172.30.160.1'}]}]

        # Act
        result = self.parser.parse_file(self.current_dir + '/test_valid_embedded_cluster.input', {}, {})
        # Assert
        self.assertEqual(6, len(result))

        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i].get('name'), result[i].tags.get('name'))
            self.assertEqual(expected_results[i]['metric'], result[i].name)
            self.assertEqual(expected_results[i]['value'], result[i].value)


    def test_valid_R77_30_disconnected_if(self):
        expected_results = [{'name': None, 'metric': 'cphaprob-up-interfaces', 'value': 3},
                            {'name': None, 'metric': 'cphaprob-up-secured-interfaces', 'value': 1},
                            {'name': None, 'metric': 'cphaprob-required-interfaces', 'value': 3},
                            {'name': None, 'metric': 'cphaprob-required-secured-interfaces', 'value': 1},
                            {'name': None, 'metric': 'clusterxl-ccp-mode', 'value': {'value': 'multicast'}},
                            {'name': None, 'metric': 'cluster-vip', 'value': [{'eth2': '30.30.31.10'}, {'eth3': '30.30.32.10'}]}]

        # Act
        result = self.parser.parse_file(self.current_dir + '/test_valid_R77_30_disconnected_if.input', {}, {})
        # Assert
        self.assertEqual(6, len(result))

        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i].get('name'), result[i].tags.get('name'))
            self.assertEqual(expected_results[i]['metric'], result[i].name)
            self.assertEqual(expected_results[i]['value'], result[i].value)


    def test_valid_R80_20_no_VIP(self):
        expected_results = [{'name': None, 'metric': 'cphaprob-up-interfaces', 'value': 1},
                            {'name': None, 'metric': 'cphaprob-up-secured-interfaces', 'value': 1},
                            {'name': None, 'metric': 'cphaprob-required-interfaces', 'value': 1},
                            {'name': None, 'metric': 'cphaprob-required-secured-interfaces', 'value': 1},
                            {'name': None, 'metric': 'clusterxl-ccp-mode', 'value': {'value': 'unicast'}}]

        # Act
        result = self.parser.parse_file(self.current_dir + '/test_valid_R80_20_no_VIP.input', {}, {})
        # Assert
        self.assertEqual(5, len(result))

        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i].get('name'), result[i].tags.get('name'))
            self.assertEqual(expected_results[i]['metric'], result[i].name)
            self.assertEqual(expected_results[i]['value'], result[i].value)

    def test_valid_missing_fields(self):
        expected_results = [{'name': None, 'metric': 'cphaprob-up-interfaces', 'value': 0},
                            {'name': None, 'metric': 'cphaprob-up-secured-interfaces', 'value': 0},
                            {'name': None, 'metric': 'clusterxl-ccp-mode', 'value': {'value': 'Manual (Multicast)'}}]

        # Act
        result = self.parser.parse_file(self.current_dir + '/test_valid_missing_fields.input', {}, {})
        # Assert
        self.assertEqual(4, len(result))

        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i].get('name'), result[i].tags.get('name'))
            self.assertEqual(expected_results[i]['metric'], result[i].name)
            self.assertEqual(expected_results[i]['value'], result[i].value)

    def test_test_R81_10_splited_lines(self):
        expected_results = [{'name': None, 'metric': 'cphaprob-up-interfaces', 'value': 3},
                            {'name': None, 'metric': 'cphaprob-up-secured-interfaces', 'value': 1},
                            {'name': None, 'metric': 'cphaprob-required-interfaces', 'value': 5},
                            {'name': None, 'metric': 'cphaprob-required-secured-interfaces', 'value': 1}]

        # Act
        result = self.parser.parse_file(self.current_dir + '/test_valid_R81_10_splited_lines.input', {}, {})

        # Assert
        self.assertEqual(6, len(result))


        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i].get('name'), result[i].tags.get('name'))
            self.assertEqual(expected_results[i]['metric'], result[i].name)
            self.assertEqual(expected_results[i]['value'], result[i].value)

if __name__ == '__main__':
    unittest.main()