# Exclude error detected in IKP-2524 amd IKP-2971
#/bin/nice: -n 15 grep -v -e ^# -e "^[[:space:]]*$" $file | awk -v prefix="$file " '{print prefix $0}'
/\/bin\/nice/{
    next
}

# Match example:
#/etc/resolv.conf nameserver 8.8.8.8
# Exclude lines that don't start with the directory name eg:
#grep: /opt/CPsuite-R77/fw1/conf/local.arp: No such file or directory
/^[\$\/]/{
    i_line++
    file_name = $1
    full_line = $0
    config_line = substr ( full_line, length ( file_name ) + 1)
    lines_config [i_line, file_name] = config_line
}

END {
    # Write complex metric - after we've collected all the files
    writeComplexMetricObjectArray("lines-config-files", null, lines_config, "false")  # Converted to new syntax by change_ind_scripts.py script
}