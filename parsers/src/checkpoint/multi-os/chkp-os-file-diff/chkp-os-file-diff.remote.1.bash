#followiong regex is used multiple times in this script: /^(#|[[:space:]]*$|\/\/|\/\*|[[:blank:]]\*)|\*\/$/ 
#this expresion filter avoid to print empty and comment lines
# # : Avoid to print lines which starts with #
# [[:space:]]*$ : Avoid to print lines where there are only spaces or tab
# [[:space:]]\* : Avoid to print lines which starts with spaces and \* 
# \/\/ : Avoid to print lines which starts with //
# \/\* : Avoid to print lines which starts with /*
# \*\/$ : Avoid to print lines which end with */
#
#  2> /dev/null  to avoid print errors in bash commands

file="$FWDIR/conf/*.def"
(${nice-path}  -n 15 ls -1  $file | xargs awk '!/^(#|[[:space:]]*$|\/\/|\/\*|[[:space:]]\*)|\*\/$/ {print FILENAME" " $0}') 2> /dev/null

file="/etc/sysctl.conf"
(${nice-path}  -n 15 awk '!/^(#|[[:space:]]*$|\/\/|\/\*|[[:space:]]\*)|\*\/$/ {print FILENAME" " $0}' $file) 2> /dev/null

file="/etc/modprobe.conf"
(${nice-path}  -n 15 awk '!/^(#|[[:space:]]*$|\/\/|\/\*|[[:space:]]\*)|\*\/$/ {print FILENAME" " $0}' $file) 2> /dev/null

file="/etc/rc.local"
(${nice-path}  -n 15 sort $file | md5sum | awk -v prefix="$file " '{print prefix "md5sum = "$1}') 2> /dev/null

file="/etc/rc.sysinit"
(${nice-path}  -n 15 sort $file | md5sum | awk -v prefix="$file " '{print prefix "md5sum = "$1}') 2> /dev/null

file="/etc/scpusers"
(${nice-path}  -n 15 awk '!/^(#|[[:space:]]*$|\/\/|\/\*|[[:space:]]\*)|\*\/$/ {print FILENAME" " $0}' $file) 2> /dev/null

file="/etc/grub.conf"
exception="password"
(${nice-path}  -n 15 grep -v "$exception" $file | sort | md5sum | awk -v prefix="$file " '{print prefix "md5sum = "$1}') 2> /dev/null

file="/etc/resolv.conf"
(${nice-path}  -n 15 awk '!/^(#|[[:space:]]*$|\/\/|\/\*|[[:space:]]\*)|\*\/$/ {print FILENAME" " $0}' $file) 2> /dev/null

file="/etc/syslog.conf"
(${nice-path}  -n 15 awk '!/^(#|[[:space:]]*$|\/\/|\/\*|[[:space:]]\*)|\*\/$/ {print FILENAME" " $0}' $file) 2> /dev/null

file="$FWDIR/boot/modules/fwkern.conf"
(${nice-path}  -n 15 awk '!/^(#|[[:space:]]*$|\/\/|\/\*|[[:space:]]\*)|\*\/$|fwx_bridge_reroute_ipv4/ {print FILENAME" " $0}' $file) 2> /dev/null

file="$FWDIR/conf/local.arp"
(${nice-path}  -n 15 awk '!/^(#|[[:space:]]*$|\/\/|\/\*|[[:space:]]\*)|\*\/$/ {print FILENAME" " $1}' $file) 2> /dev/null