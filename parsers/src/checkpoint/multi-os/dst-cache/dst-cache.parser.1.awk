# ip_dst_cache          20     48    320   12    1 : tunables   54   27    8 : slabdata      4      4      0
/ip_dst_cache/ {
	writeDoubleMetric("destination-cache-usage", null, "gauge", $3, "false")  # Converted to new syntax by change_ind_scripts.py script
}


# max 1048576
/max / {
	writeDoubleMetric("destination-cache-limit", null, "gauge", $2, "false")  # Converted to new syntax by change_ind_scripts.py script
}