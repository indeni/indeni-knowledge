BEGIN {
	hostname_exists = 0
}

# hostname: lab-CPMGMTR7730
/^hostname:/ {
	hostname = $2
}

# 127.0.0.1 localhost
# 10.10.6.10 lab-CPMGMTR7730
# 10.10.6.27      lab-CP-GW6-R77SPLAT     lab-CP-GW6-R77SPLAT.test.local
/(?:[0-9]{1,3}\.){3}[0-9]{1,3}/ {

	if ( $0 ~ hostname ) {
		hostname_exists = 1
	}
}

END {
	writeDoubleMetric("hostname-exists-etc-hosts", null, "gauge", hostname_exists, "false")  # Converted to new syntax by change_ind_scripts.py script
}