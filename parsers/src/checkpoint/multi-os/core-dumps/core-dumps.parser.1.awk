# -rw-r--r-- 1 admin root 2898543 2016-07-12 08:52:01.000000000 +0200 sshd.14383.core.gz
/^[rwx-]{10}/ {
	# Exclude some directories
	if ($9 !~ "/var/crash/bounds|/var/crash/minfree") {

		ifile++

		# starting data
		createDate=$6
		createTime=$7

		# year,month,day
		split(createDate,dateArray,"-")
		createYear=dateArray[1]
		createMonth=dateArray[2]
		createDay=dateArray[3]

		# time
		gsub(/\.[0-9]+/,"",createTime)
		split(createTime, createTimeArr,":")	
		
		# Create complex object array	
		files[ifile, "path"]=$9	
		files[ifile, "created"]=datetime(createYear,createMonth,createDay,createTimeArr[1],createTimeArr[2],createTimeArr[3])
	}
}

END {
	# Write complex metric
	writeComplexMetricObjectArray("core-dumps", null, files, "false")  # Converted to new syntax by change_ind_scripts.py script
}