#VolumeID:0 RaidLevel: RAID-1 NumberOfDisks:0 RaidSize:1296385023GB State:DEGRADED Flags:ENABLED USING_INTERIM_RECOVERY_MODE
/State:/ {

	# The line can look very different. Need to search for the "State:" message.
	for (i = 1; i<=NF; i++) {
		if (match($i, /State:/)) {
			stateMessage = $i
			gsub(/State:/, "", stateMessage)
		}
	}
	
	# Allowing only "OPTIMAL" or "ONLINE" status, except for "MISSING" which needs to be allowed due to possible bug. sk104580
	if (stateMessage != "OPTIMAL" && stateMessage != "MISSING" && stateMessage != "ONLINE") {
		state = 0
	} else {
		state = 1
	}
	
	tags["name"] = "RAID " $1
	writeDoubleMetric("hardware-element-status", tags, "gauge", state, "false")  # Converted to new syntax by change_ind_scripts.py script
}