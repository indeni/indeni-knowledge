############
# Script explanation: The command uses "cphaprob" which is only available when in a cluster.
###########

BEGIN {
    bondmode_ha = 0
}

#Bond name:      bond0
/Bond name/ {
    bondname = $NF
}

# Bond mode:      High Availability
/Bond mode/ {
    if ($0 ~ /High Availability/)
        bondmode_ha = 1

    mode = substr($0,17)
    writeComplexMetricString("bond-mode", null, mode, "true", "Bond Interfaces - Mode")
}

#Bond status:    UP
/Bond status/ {
    bondstatus = $NF
    isup = 0
    if (bondstatus == "UP") {
        isup = 1
    }
    bondstatustags["name"] = bondname
    writeDoubleMetric("bond-state", bondstatustags, "gauge", isup, "true", "Bond Interfaces", "state", "name")  # Converted to new syntax by change_ind_scripts.py script
}

#Configured slave interfaces: 2
/Configured slave interfaces/ {
    writeDoubleMetric("configured-slaves", bondstatustags, "gauge", $NF, "false")  # Converted to new syntax by change_ind_scripts.py script
}

#In use slave interfaces:     1
/In use slave interfaces/ {
    writeDoubleMetric("bond-slaves-in-use", bondstatustags, "gauge", $NF, "false")  # Converted to new syntax by change_ind_scripts.py script
}

#Required slave interfaces:   1
/Required slave interfaces/ {
    writeDoubleMetric("bond-slaves-required", bondstatustags, "gauge", $NF, "false")  # Converted to new syntax by change_ind_scripts.py script
}

#Lines should end with status Yes/No, like:
#eth3            | Active          | Yes
/(Yes|No)$/ {
    t["name"] = $1
    state_desc = $(NF-2)
    t["bond-name"] = bondname

    up = 0
    if (match(state_desc, "Active")) {
        up = 1
    }

    # Backup state is normal for HA mode
    if (match(state_desc, "Backup") && bondmode_ha == 1) {
        up = 1
    }

    writeDoubleMetric("bond-slave-state", t, "gauge", up, "true", "Bond Slave Interfaces", "state", "name|bond-name")  # Converted to new syntax by change_ind_scripts.py script
}