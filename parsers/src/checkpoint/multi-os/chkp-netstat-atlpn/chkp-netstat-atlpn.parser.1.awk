#tcp        0      0 0.0.0.0:62297               0.0.0.0:*                   LISTEN      5467/fwd
/\s+LISTEN\s+/ {
    split( $4, local_addr_port_array, ":" )
    split( $NF, process_id_name_array, "/" )
    port = local_addr_port_array[2]
    process_name = process_id_name_array[2]
    
    ports[port, "port"] = port
    ports[port, "process"] = process_name
}

END {
    writeComplexMetricObjectArray("listening-ports", null, ports, "false")  # Converted to new syntax by change_ind_scripts.py script
}