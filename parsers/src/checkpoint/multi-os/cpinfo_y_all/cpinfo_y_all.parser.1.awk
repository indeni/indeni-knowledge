BEGIN {
    hf_module = ""
    hf_name = ""
    hf_take = "N/A"
    hf_jumbo_take = 0
}

############
# Caveats: As cpuse is used more and more, we might want to get information that way in the future.
# 2/26/19: cpinfo -y -all is not supported on any GAiA release. Instead, cpinfo -y all is supported and will continue to be supported in R80.X
# cpinfo will provide hotfix take information consistently starting on version R80.X. As a result, this script should also collect the hotfix take only if the version is running on R80 and newer.
# cpinfo will also include FW1 build number and os version consistently in the future. This can be used to determine whether the hotfix take is taken from here.
# 4/13/19: Did not factor in the fact that hotfix names can exclude the value "HOTFIX". Added "JUMBO_HF" as a parameter.
###########

#[CPFC]
/^\[/{
    hf_module = $0
}

#Before R80.20 output:
#   HOTFIX_R77_30
#   HOTFIX_GEYSER_HF_BASE_861
#R80.20 and after output:
#   HOTFIX_R80_20_JUMBO_HF_MAIN	Take:  33

/HOTFIX/ || /JUMBO_HF/ || /JHF/ {
# Hotfixes can appear multiple times. The below function will dedupe the entries before being parsed.
# Additionally, we want to parse the take number. Take should be consistent, so deduping is unnecessary.
# Take only appears on Jumbo HF


    hf_name = $1
    hf_index = hf_module":"hf_name
    hotfix_arr[hf_index,"name"] = hf_name
    hotfix_arr[hf_index,"module"] = hf_module
    if ($(NF-1) == "Take:") { #If hotfix is Jumbo HF
        hf_jumbo_take = $NF
        hotfix_arr[hf_index,"take"] = hf_jumbo_take
    } else {
        hotfix_arr[hf_index,"take"] = hf_take
    }
}

END {
    writeComplexMetricObjectArray("hotfixes", null, hotfix_arr, "true", "Installed Hotfixes")  # Converted to new syntax by change_ind_scripts.py script
    if (hf_jumbo_take != 0) {
        writeComplexMetricString("hotfix-jumbo-take", null, hf_jumbo_take, "true", "Installed Hotfix Take")  # Converted to new syntax by change_ind_scripts.py script
    }
}