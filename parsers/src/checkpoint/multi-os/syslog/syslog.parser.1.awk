##*.err                   @1.1.1.1
/.*#.*(?:[0-9]{1,3}\.){3}[0-9]{1,3}/ {
    # This section would skip lines that are commented out, but not those who are commented in the end, such as:
    # *.err                   @1.1.1.1 #This is a comment
    next
}

#mail.*                     @1.1.1.1
#uucp.crit;news.crit        @2.2.2.2
#*.*                         @1.2.3.4
/(?:[0-9]{1,3}\.){3}[0-9]{1,3}/ {

    ip = $2

    #@1.1.1.1
    gsub(/@/, "", ip)


    split($1, splitArr, ";")

    for (id in splitArr) {

        split(splitArr[id], severityFacilityArr, ".")
        iDevice++
        syslogServers[iDevice, "host"] = ip
        syslogServers[iDevice, "severity"] = severityFacilityArr[2]
    }
}

END {
    writeComplexMetricObjectArray("syslog-servers", null, syslogServers, "true", "Syslog servers")  # Converted to new syntax by change_ind_scripts.py script
}