from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class ChkpId(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        # Step 1 : Data Extraction
        data = helper_methods.parse_data_as_object(raw_data, 'checkpoint_gaia_chkp_id.textfsm')

        # Step 2 : Data Processing and Reporting
        if data:
            self.write_complex_metric_string('user-id', {}, data['uid_value'], False)
        else:
            self.write_complex_metric_string('user-id', {}, '-1', False)
        return self.output
