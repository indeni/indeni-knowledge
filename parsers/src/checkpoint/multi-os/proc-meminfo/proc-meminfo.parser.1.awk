# VmallocUsed:     44812 kB
/VmallocUsed/ {
	writeDoubleMetric("vmalloc-used-kbytes", null, "gauge", $2, "false")  # Converted to new syntax by change_ind_scripts.py script
}

# VmallocTotal:     116728 kB
/VmallocTotal/ {
	writeDoubleMetric("vmalloc-total-kbytes", null, "gauge", $2, "false")  # Converted to new syntax by change_ind_scripts.py script
}