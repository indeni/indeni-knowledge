# output from clock (=hwclock)
#time: Fri Sep  1 05:15:20 2017  -0.016178 seconds
#time: Mon May 27 13:54:47 2019  -0.015984 seconds
# change from clock to date
# time: 13 07 53 18 04 2023
/^time: / {
    current_hour = $2
    current_minute = $3
    current_second = $4
    current_day = $5
    current_month = $6
    current_year = $7

    current_time = datetime(current_year, current_month, current_day, current_hour, current_minute, current_second)
}


#/var/log/secure:Feb  6 14:35:20 2018 lab-CP-GW4-1 sshd[23508]: Failed password for hawkeyetmp from 192.168.201.13 port 64407 ssh2
#/var/log/secure.1:Jul 27 17:42:37 Mario sshd[29849]: Failed password for indeni from 10.10.1.1 port 63913 ssh2
#May 24 05:39:12 2019 CP-R80 sshd[29154]: Failed password for admin from 192.168.250.2 port 49546 ssh2
#May 26 08:15:50 2019 CP-R80 sshd[6234]: Failed password for bgtest from 192.168.250.1 port 57821 ssh2
/ Failed password for / {

    # include the source IP in the username: "admin from 192.168.250.2"
    user = $(NF-5) " from " $(NF-3)
    #Since the output is different in R80.20SP adding this loop
    if ($1 !~ /^\//) {
        failed_month = parseMonthThreeLetter($1)
    } else {
        split($1, fileAndMonth, ":")
        failed_month = parseMonthThreeLetter(fileAndMonth[2])
    }
    failed_day = $2

    # Sometimes, the year is missing from the log line. (I don't know why this can happen.)
    # Usually, we expect that the year of the log line should be the same as the current year. However, if we just
    # passed the new year, the log line year might be from the previous year, not the current year. To handle this case,
    # I've created this heuristic:

    if ( $4 !~ /[0-9]{4}/) {  # if the year doesn't exist in the log line
        difference = current_month - failed_month

        if (difference == 0 || difference == 1) { # log month and current month are the same, or log month is previous month; assume current year
            failed_year = current_year
        } else if (difference > 1) {  # we're in the same year, but the log line is at least a month old: we don't care about it, so move to next record
            next
        } else if (difference == -11) {  # the curr month is Jan and log month is Dec; assume previous year
            failed_year = current_year - 1
        } else {  # difference is negative and NOT -11. We are probably in the previous year, but definitely past the hour failure window. Ignore this record, move to the next.
            next
        }
    } else {
        failed_year = $4
    }


    #10:18:30
    split($3, failed_time_split_arr, ":")
    failed_hour = failed_time_split_arr[1]
    failed_minute = failed_time_split_arr[2]
    failed_second = failed_time_split_arr[3]

    failed_time = datetime(failed_year, failed_month, failed_day, failed_hour, failed_minute, failed_second)
    diff_time = current_time - failed_time

    if (diff_time < 3600) { # failed time is within 60min from current time
        failed[user]++
    }

    users[user] = 1
}

END {
    for (user in users) {
        tags["username"] = user
        failureCount = failed[user]
        if (failureCount == "")
            failureCount = 0
        writeDoubleMetric("failed-logins", tags, "gauge", failureCount, "false")
    }
}
