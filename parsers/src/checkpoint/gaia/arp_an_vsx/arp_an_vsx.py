from parser_service.public.base_parser import BaseParser
from parser_service.public import helper_methods
import re

class UnixArpAnVsx(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            parsed_data = helper_methods.parse_data_as_list(raw_data,'arp_an_vsx.textfsm')
            if parsed_data:
                vs_list=list(dict.fromkeys([entry['vs_id'] for entry in parsed_data]))
                total_arp_entries = 0
                for vs in vs_list:
                    vs_arp_entries = [x for x in parsed_data if x['vs_id']==vs]
                    tags = {}
                    tags['vs.id'] = vs_arp_entries[0]['vs_id']
                    tags['vs.name'] = vs_arp_entries[0]['vs_name']
                    list_entries = []
                    static_entries = []
                    for arp_data in vs_arp_entries:
                        entry = {}
                        entry['mac'] = arp_data['mac_address']
                        entry['targetip'] = arp_data['ip_address']
                        entry['interface'] = arp_data['interface']
                        entry['success'] = '0' if re.search('/incomplete/', arp_data['mac_address']) is not None else '1'
                        list_entries.append(entry)   
                        if len(arp_data['perm']) > 0 :
                            static_entries.append(entry)
                    total_arp_entries =+ len(list_entries)
                    self.write_complex_metric_object_array('arp-table', tags, list_entries, False, 'arp table')
                    self.write_complex_metric_object_array('static-arp', tags, static_entries, False, 'static arp')
            self.write_double_metric('arp-total-entries', {}, 'gauge', total_arp_entries, False, 'arp total entries')         

        return self.output