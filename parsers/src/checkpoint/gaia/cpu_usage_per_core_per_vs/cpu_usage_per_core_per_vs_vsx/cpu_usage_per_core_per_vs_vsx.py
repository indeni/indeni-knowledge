from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser



class CpuUsagePerCorePerVsVsx(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            cores_parsed = helper_methods.parse_data_as_list(raw_data, 'core_list.textfsm')
            vs_info_parsed = helper_methods.parse_data_as_list(raw_data, 'vs_info.textfsm')
            data_parsed = helper_methods.parse_data_as_list(raw_data, 'vs_cpu_vsx.textfsm')
            for vs in vs_info_parsed:
                vs_cpu_usage = 0
                vs_cores_active = 0
                for core in cores_parsed[0]['core_id']:
                    usage_list = [x['usage'] for x in data_parsed if x['vs_id'] == vs['vs_id'] and x['core_id'] == core]
                    if usage_list:
                        tags = {}
                        tags['vs.id'] = vs['vs_id']
                        tags['vs.name'] = vs['vs_name']
                        tags['cpu.core.id'] = core
                        vs_cpu_usage += sum(float(x) for x in usage_list)
                        self.write_double_metric('live-config-only-vs-cpu-usage', tags, 'gauge', sum(float(x) for x in usage_list), True, 'CPU Usage per Core per VS', 'percentage', 'vs.id|cpu.core.id')
                        vs_cores_active += 1
                tags = {}
                tags['vs.id'] = vs['vs_id']
                tags['vs.name'] = vs['vs_name']
                if vs_cpu_usage!= 0:
                    vs_avg_usage = vs_cpu_usage/vs_cores_active
                else:
                    vs_avg_usage = vs_cpu_usage
                self.write_double_metric('vs-cpu-usage', tags, 'gauge', vs_avg_usage, True, 'Average CPU Usage per VS', 'percentage', 'vs.name|vs.id')
        return self.output