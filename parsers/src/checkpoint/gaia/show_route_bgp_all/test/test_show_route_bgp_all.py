import os
import unittest

from checkpoint.gaia.show_route_bgp_all.show_route_bgp_all import ShowRouteBgpAll
from parser_service.public.action import *

class TestShowRouteBgpAll(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = ShowRouteBgpAll()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_hidden_and_inactive_routes(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/hidden_and_inactive_routes.input', {}, {})
        # Assert
        self.assertEqual(2, len(result))
        self.assertEqual(result[0].name, 'bgp-active-routes-counter')
        self.assertEqual(result[0].value, 0)
        self.assertEqual(result[1].name, 'bgp-hidden-routes-counter')
        self.assertEqual(result[1].value, 4)



if __name__ == '__main__':
    unittest.main()