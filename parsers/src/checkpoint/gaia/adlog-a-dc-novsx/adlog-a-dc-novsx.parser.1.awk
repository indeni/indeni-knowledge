BEGIN {
    # The input is separated on many spaces.
    FS = "[ ]{3,}"
}

#Domain Name               IP Address                Events (last hour)   Connection state
/(Domain Name|IP Address|Events|Connection state)/ {
    # Parse the line into a column array.
    getColumns(trim($0), "(\t+|[ ]{3,})", columns)
}


#test.local             192.168.123.50            14269                has connection
# or for 61k
#lab.indeni.com;    10.11.80.30;        has connection;         69
# (note the semi-colons; also note that the test file contains the tabbed command output)
/^[a-zA-Z].+(\s{2,}|\t+)([0-9]{1,3}\.){3}[0-9]{1,3}/ {
    # Use getColData to parse out the data for the specific column from the current line. The current line will be
    # split according to the same separator we've passed in the getColumns function (it's stored in the "columns" variable).
    # If the column cannot be found, the result of getColData is null (not "null").

    row = trim($0)
    connectionState = getColData(row, columns, "Connection state")
    ip = getColData(row, columns, "IP Address")
    gsub(/;/, "", ip)

    if (connectionState == "has connection") {
        serverArr[ip]++
    } else if (ip in serverArr) {
        # Value exists in array but is IP do not have connection this time, do nothing
    } else {
        serverArr[ip] = 0
    }
}

END {
    for (ip in serverArr) {
        if (serverArr[ip] > 1) {
            status = 1
        } else {
            status = 0
        }

        tags["name"] = ip
        writeDoubleMetric("identity-integration-connection-state", tags, "gauge", status, "false")  # Converted to new syntax by change_ind_scripts.py script
    }
}