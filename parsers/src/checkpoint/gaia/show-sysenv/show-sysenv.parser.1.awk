# The following two sections has been added by request of Dan Shouky
# https://indeni.atlassian.net/browse/IKP-1221

#Could not acquire the config lock
/Could not acquire the config lock/ {
    if (NR == 1) {
        next
    }
}

#CLINFR0829  Unable to get user permissions
#CLINFR0819  User: johndoe denied access via CLI
#CLINFR0599  Failed to build ACLs
/(CLINFR0829\s+Unable to get user permissions|CLINFR0819\s+User: .+ denied access via CLI|CLINFR0599\s+Failed to build ACLs)/ {
    exit
}

/^$/ {
    section = ""
}

# Gets the column names, sets the section variable and skips to the next line
function prepareSection(sec) {
    getColumns($0, /\s{2,}/, columns)
    section = sec
}

function getCol(c) {
    return getColData($0, columns, c)
}

#################################################################
#   Fan sensor data
#################################################################

#Number  Location  Status  Current Value  Normal Value  Fan limit
/Normal\sValue\s+Fan limit\s*$/{
    prepareSection("Fan Sensors Status")
    next
}

#1       SYS_FAN1  Normal  109            108           155
#5       PS_FAN    Normal  82             84            120
/^[0-9]+\s+(SYS|PS)_FAN/ && section == "Fan Sensors Status" {

    name = getCol("Location")
    type = "Fan"
    currentValue = getCol("Current Value")
    upperLimit = getCol("Fan limit")

    hwTags["name"] = name
    hwTags["type"] = type

    if (currentValue >= upperLimit) {
        status = 0
    } else {
        status = 1
    }

    writeDoubleMetric("hardware-element-status", hwTags, "gauge", status, "true", "Hardware Status", "state", "name")  # Converted to new syntax by change_ind_scripts.py script
    next

}

#################################################################
#   Power Supply Status
#################################################################

#ID  Present  Volts   Amps    Status  Revision
/Volts\s+Amps\s+Status\s+Revision\s*/{
    prepareSection("Power Supply Status")
    next
}

#PS-A  Yes      n/a     n/a     OK      0
/^PS/ && section == "Power Supply Status" {

    name = getCol("ID")
    psStatus = getCol("Status")

    hwTags["name"] = name
    hwTags["type"] = "PSU"

    if (psStatus == "OK"){
        status = 1
    } else {
        status = 0
    }

    writeDoubleMetric("hardware-element-status", hwTags, "gauge", status, "true", "Hardware Status", "state", "name")  # Converted to new syntax by change_ind_scripts.py script
    next
}

#################################################################
#   Temperature Sensor Status
#################################################################

#Number  Location  Status  Current Value  Temp Limit  Temp Hysteres
/Temp\sLimit\s+Temp\sHysteres\s*$/{
    prepareSection("Temperature Sensor Status")
    next
}

#2       CPU       Good    48             75          0
/^[0-9]+\s+/ && section == "Temperature Sensor Status" {

    number = getCol("Number")
    location = getCol("Location")
    upperLimit = getCol("Temp Limit")
    currentValue = getCol("Current Value")

    hwTags["name"] = location "-" number
    hwTags["type"] = "Temperature"

    if (currentValue <= upperLimit) {
        status = 1
    } else {
        status = 0
    }

    writeDoubleMetric("hardware-element-status", hwTags, "gauge", status, "true", "Hardware Status", "state", "name")  # Converted to new syntax by change_ind_scripts.py script
    next

}

#################################################################
#   Voltage Sesors Status
#################################################################

#Number  Location  Status  Nominal  Measured  Error     Low Limit  High Limit
/Measured\s+Error\s+Low\sLimit\s+High\sLimit\s*$/{
    prepareSection("Voltage Sensors Status")
    next
}

#7       1.1V      Good    1.100    1.100     n/a       1.040     1.148
/^[0-9]+\s+/ && section == "Voltage Sensors Status" {

    location = getCol("Location")
    currentValue = getCol("Measured")
    lowerLimit = getCol("Low Limit")
    upperLimit = getCol("High Limit")

    # "lowerLimit" can sometimes be the word "None". Replacing that with a negative number to make calculations work.
    if (lowerLimit == "None") {
        lowerLimit = -1000
    }

    if (currentValue >= lowerLimit && currentValue <= upperLimit) {
        status = 1
    } else {
        status = 0
    }

    hwTags["name"] = location
    hwTags["type"] = "Voltage"

    writeDoubleMetric("hardware-element-status", hwTags, "gauge", status, "true", "Hardware Status", "state", "name")  # Converted to new syntax by change_ind_scripts.py script
    next

}

#################################################################
#   Hardware Information
#################################################################

#Name        Value       unit          type        status    Maximum     Minimum
#Name        Value       Unit          Type        Status    Maximum     Minimum
/Name\s+Value\s+[Uu]nit\s+[Tt]ype\s+[Ss]tatus\s+Maximum\s+Minimum\s*$/ {
    # depending on the version, lower case and upper case exist, so the regular expression should deal with both
    prepareSection("Hardware Information")
    next
}


#Sys Temp     32.00        Celsius       Temperature    0         80           None
#CPU0 FAN     5625.00      RPM           Fan            0         14000        600
#Power Supply #2   Up           On/Off        Power          0         Up           Down
#Power Supply #2   Up           Up/Down       Power          0         Up           Down
#Vbat         3.14         Volt          Voltage        0         3.6          2.0
/(Volt|Celsius|RPM|On\/Off|Up\/Down)\s+(Voltage|Temperature|Fan|Power)/ && section == "Hardware Information" {
    # depending on the version, On/Off and Up/Down both exist, so the regular expression should deal with both
    name = getCol("Name")
    value = getCol("Value")
    lowerLimit = getCol("Minimum")
    upperLimit = getCol("Maximum")
    type = getCol("type")

    # "lowerLimit" can sometimes be the word "None". Replacing that with a negative number to make calculations work.
    if (lowerLimit == "None") {
        lowerLimit = -1000
    }

    #This handles PSU = Down and also if the limits are breached
    if (value == "Down" || value < lowerLimit || value > upperLimit ) {
        status = 0
    } else {
        status = 1
    }

    hwTags["name"] = name
    hwTags["type"] = type

    writeDoubleMetric("hardware-element-status", hwTags, "gauge", status, "true", "Hardware Status", "state", "name")  # Converted to new syntax by change_ind_scripts.py script

}