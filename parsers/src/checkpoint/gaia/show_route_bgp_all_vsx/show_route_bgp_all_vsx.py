from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class ShowRouteBgpAllVsx(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        # Step 1 : Data Extraction
        if raw_data:
            data = helper_methods.parse_data_as_list(raw_data, 'show_route_bgp_all_vsx.textfsm')
            tags = {}
            if data:
                list_vs = ([i['vs_id']+':'+i['vs_name'] for i in data ])
                list_vs = set(list_vs)
                for vs in list_vs:
                    total_active_routes = 0 #Initialization in the case there is no BGP route
                    total_hidden = 0
                    total_active_routes = sum([1 for i in data if len(i['states'])==0 and len(i['route'])>0 and i['vs_id']==vs.split(':')[0]])
                    total_hidden = sum([1 for i in data if 'H' in i['states']  and len(i['route'])>0 and i['vs_id']==vs.split(':')[0]])
                    tags['vs.id'] = vs.split(':')[0]
                    tags['vs.name'] = vs.split(':')[1]
                    self.write_double_metric('bgp-active-routes-counter', tags, 'gauge', total_active_routes , False)
                    self.write_double_metric('bgp-hidden-routes-counter', tags, 'gauge', total_hidden , False)
        return self.output
