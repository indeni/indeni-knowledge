import os
import unittest

from checkpoint.gaia.show_route_bgp_all_vsx.show_route_bgp_all_vsx import ShowRouteBgpAllVsx
from parser_service.public.action import *

class TestShowRouteBgpAllVsx(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = ShowRouteBgpAllVsx()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_hidden_and_inactive_routes(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/hidden_and_inactive_routes.input', {}, {})
        # Assert
        self.assertEqual(8, len(result))
        result.sort(key=lambda x: (x.tags.get('vs.id'), x.name))
        self.assertEqual(result[0].name, 'bgp-active-routes-counter')
        self.assertEqual(result[0].value, 0)
        self.assertEqual(result[0].tags['vs.id'], '0')
        self.assertEqual(result[0].tags['vs.name'], 'VSX-1')
        self.assertEqual(result[1].name, 'bgp-hidden-routes-counter')
        self.assertEqual(result[1].value, 0)
        self.assertEqual(result[1].tags['vs.id'], '0')
        self.assertEqual(result[1].tags['vs.name'], 'VSX-1')

        self.assertEqual(result[2].name, 'bgp-active-routes-counter')
        self.assertEqual(result[2].value, 0)
        self.assertEqual(result[2].tags['vs.id'], '1')
        self.assertEqual(result[2].tags['vs.name'], 'VS-1-R81')
        self.assertEqual(result[3].name, 'bgp-hidden-routes-counter')
        self.assertEqual(result[3].value, 4)
        self.assertEqual(result[3].tags['vs.id'], '1')
        self.assertEqual(result[3].tags['vs.name'], 'VS-1-R81')

        self.assertEqual(result[4].name, 'bgp-active-routes-counter')
        self.assertEqual(result[4].value, 0)
        self.assertEqual(result[4].tags['vs.id'], '2')
        self.assertEqual(result[4].tags['vs.name'], 'VS-2-R81')
        self.assertEqual(result[5].name, 'bgp-hidden-routes-counter')
        self.assertEqual(result[5].value, 0)
        self.assertEqual(result[5].tags['vs.id'], '2')
        self.assertEqual(result[5].tags['vs.name'], 'VS-2-R81')

        self.assertEqual(result[6].name, 'bgp-active-routes-counter')
        self.assertEqual(result[6].value, 0)
        self.assertEqual(result[6].tags['vs.id'], '3')
        self.assertEqual(result[6].tags['vs.name'], 'Router')
        self.assertEqual(result[7].name, 'bgp-hidden-routes-counter')
        self.assertEqual(result[7].value, 0)
        self.assertEqual(result[7].tags['vs.id'], '3')
        self.assertEqual(result[7].tags['vs.name'], 'Router')


if __name__ == '__main__':
    unittest.main()