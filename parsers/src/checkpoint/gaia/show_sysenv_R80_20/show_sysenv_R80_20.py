from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class ShowSysEnv(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        # Step 1 : Data Extraction
        if raw_data:
            data = helper_methods.parse_data_as_list(raw_data, 'show_sysenv_R80_20.textfsm')
            data_stripped = []
            if data:
                for sensor in data:
                    new_data = {key: value.strip() for key, value in sensor.items()}
                    data_stripped.append(new_data)

            # Step 2 : Data Processing
            if data_stripped:
                for sensor in data_stripped:
                    status = 0
                    hw_tags = {}
                    if (sensor['type']) == 'Voltage':
                        if sensor['value'] in ['Bad']:
                            status = 0
                        elif ((sensor['value'] in ['No Read', 'Good']) 
                            or (float(sensor['value']) <= float(sensor['maxval']) 
                            and float(sensor['value']) >= float(sensor['minval']))):
                            status = 1

                    elif (sensor['type']) == 'Temperature':
                        if sensor['minval'] in ['None']:
                            if not (float(sensor['value']) > float(sensor['maxval'])):
                                status = 1
                        elif not (float(sensor['value']) > float(sensor['maxval']) or float(sensor['value']) < float(sensor['minval'])):
                            status = 1
                    elif (sensor['type']) == 'Fan':
                        if not (float(sensor['value']) > float(sensor['maxval']) or float(sensor['value']) < float(sensor['minval'])):
                            status = 1
                    elif (sensor['type']) == 'Power':
                        if (sensor['value']).strip() == 'Up':
                            status = 1
                        elif (sensor['value']).strip() == 'Dummy':
                            status = 1
                    elif (sensor['type']) == 'BIOS':
                        if (sensor['value']).strip() == 'Valid':
                            status = 1
                    elif (sensor['type']) == 'PS':
                        if (sensor['value']).strip() == 'Present':
                            status = 1

            # Step 3 Data Reporting
                    hw_tags['name'] = sensor['name']
                    hw_tags['type'] = sensor['type']
                    self.write_double_metric("hardware-element-status", hw_tags, "gauge", status, True, "Hardware Status", "state", "name")

        return self.output
