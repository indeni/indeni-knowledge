import os
import unittest


from parser_service.public.action import WriteDoubleMetric
from checkpoint.gaia.show_sysenv_R80_20.show_sysenv_R80_20 import ShowSysEnv


class TestShowSysEnv(unittest.TestCase):
    def setUp(self):
        # Arrange
        self.parser = ShowSysEnv()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))


    def test_valid_input_R80_20(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/valid_input_R80_20.txt', {}, {})

        # Assert
        self.assertEqual(16, len(result))

        self.assertTrue(result[0].name, 'hardware-element-status')
        self.assertTrue(result[0].tags['name'], 'CPU1 FIVR PG')
        self.assertEqual(result[0].value, 1)

        self.assertTrue(result[3].name, 'hardware-element-status')
        self.assertTrue(result[3].tags['name'], 'CPU2 FIVR PG')
        self.assertEqual(result[3].value, 0)

        self.assertTrue(result[12].name, 'hardware-element-status')
        self.assertTrue(result[12].tags['name'], 'System Fan4')
        self.assertEqual(result[12].value, 1)

        self.assertTrue(result[14].name, 'hardware-element-status')
        self.assertTrue(result[14].tags['name'], 'BIOS')
        self.assertEqual(result[14].value, 1)

        self.assertTrue(result[15].name, 'hardware-element-status')
        self.assertTrue(result[15].tags['name'], 'ROMB Battery')
        self.assertEqual(result[15].value, 1)


    def test_valid_input_maestro_sg_ha_ls_R80_20SP(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/valid_input_maestro_sg_ha_ls_R80_20SP.txt', {}, {})

        # Assert
        self.assertEqual(23, len(result))

        self.assertTrue(result[4].name, 'hardware-element-status')
        self.assertTrue(result[4].tags['name'], 'CPU1 DDR4-1')
        self.assertEqual(result[4].value, 1)

        self.assertTrue(result[21].name, 'hardware-element-status')
        self.assertTrue(result[21].tags['name'], 'Power Supply #2')
        self.assertEqual(result[21].value, 0)


    def test_valid_input_show_sysenv_all_R80_20(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/valid_input_show_sysenv_all_R80_20.txt', {},
                                        {})

        # Assert
        self.assertEqual(14, len(result))

        self.assertEqual(result[4].name, 'hardware-element-status')
        self.assertEqual(result[4].tags['name'], '+5V')
        self.assertTrue(result[4].value)

        self.assertEqual(result[13].name, 'hardware-element-status')
        self.assertEqual(result[13].tags['name'], 'BIOS')
        self.assertTrue(result[13].value)


    def test_valid_input_show_sysenv_all_R80_20_include_None_values(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/valid_input_show_sysenv_all_R80_20_include_None_values.txt', {}, {})

        # Assert
        self.assertEqual(15, len(result))

        self.assertEqual(result[8].name, 'hardware-element-status')
        self.assertEqual(result[8].tags['name'], 'CPU Temp')
        self.assertEqual(result[8].value, 1)

        self.assertEqual(result[9].name, 'hardware-element-status')
        self.assertEqual(result[9].tags['name'], 'System Temp')
        self.assertEqual(result[9].value, 1)


    def test_valid_equal_values(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/valid_equal_values.input', {}, {})

        # Assert
        self.assertEqual(15, len(result))

        self.assertEqual(result[8].name, 'hardware-element-status')
        self.assertEqual(result[8].tags['name'], 'CPU Temp')
        self.assertEqual(result[8].value, 1)

        self.assertEqual(result[2].name, 'hardware-element-status')
        self.assertEqual(result[2].tags['name'], 'AVCC')
        self.assertEqual(result[2].value, 1)


    def test_valid_input_R80_30_Smart_1_5150(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/valid_input_R80_30_Smart-1_5150.input', {}, {})

        # Assert
        self.assertEqual(28, len(result))

        self.assertEqual(result[26].name, 'hardware-element-status')
        self.assertEqual(result[26].tags['name'], 'PS1 Status')
        self.assertEqual(result[26].value, 1)

        self.assertEqual(result[27].name, 'hardware-element-status')
        self.assertEqual(result[27].tags['name'], 'PS2 Status')
        self.assertEqual(result[27].value, 1)


if __name__ == '__main__':
    unittest.main()
