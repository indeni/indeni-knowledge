BEGIN{
    telnet_enabled = "false"
}

/^netaccess:telnet on/ {
    telnet_enabled = "true"
}

/^telnet on/ {
    telnet_enabled = "true"
}

END {
    writeComplexMetricString("telnet-enabled", null, telnet_enabled, "false")  # Converted to new syntax by change_ind_scripts.py script

}