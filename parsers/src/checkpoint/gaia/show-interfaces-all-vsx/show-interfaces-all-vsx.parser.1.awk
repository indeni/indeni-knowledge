###########
# Commandline Comments
#
# - list all VSs with "fw vsx stat -l"
# - list all device interfaces for each VS using "ifconfig -a"
# - print the details of each interface using "ifconfig <interface>" and "ethtool <interface>"
# - grep "/config/active" to collect interface comments. /config/active is a Check Point configuration file that
#   stores the current running configuration of the device.
#
# NOTE: For this commandline, avoid running clish commands due to the excessive logs in /var/log/messages that creates.
#
# General Script Comments
# Interface Types
# eth0 -- a 'regular' physical interface. ethtool will report correct link state: it is the physical interface itself.
# eth0.5 -- a VLAN or "sub" interface. Always contains '.'. ethtool will report correct link state, and returns same info 
#           as the corresponding physical interface.
# eth0:1 -- an 'alias' or multi-IP interface. Always contains ':'. ethtool will report correct link state, and returns 
#           same info as the corresponding physical interface.
# bond -- bond interface. Always starts with the "bond". Do not use ethtool to check the link state of a bond interface.
#         A bond is an aggregation of physical interfaces, so just check the link state of each bonded interface.
# wrp(j)192 -- a warp interface. Always starts with the "wrp". Do not use ethtool to check the link state: it is a virtual interface.
# br1 -- a bridge interface. Always starts with "br" (?). ethtool will report correct link state.
# vpnt3 -- a vpn tunnel interface. Always starts with "vpnt". Do not use ethtool to check the link state.
# pppoe9 -- a Point-to-Point Protocol Over Ethernet interface. Always starts with "pppoe". Do not use ethtool to check the link state.
# sit5 -- a vpn IPv6 tunnel interface over IPv4. Always starts with "sit". Do not use ethtool to check the link state.
#
# Specific CHKP naming convention:
# Mgmt -- Naming used by CHKP to identify management interfaces. In non-chassis devices, these are physical interfaces.
# Sync -- Naming used by CHKP to identify synchronization interface. In non-chassis devices, these are physical interfaces.
#
###########

function dumpVsInterfaceArrayData() {
    # intentionally using a different tags array for this section. We will set these tags in a loop, below -- don't
    # confuse this tag array with the one in the main AWK section -- the serve different purposes.
    state_tags["vs.id"] = vs_id
    state_tags["vs.name"] = vs_name

    if (arraylen(interfaces))
        writeComplexMetricObjectArray("network-interfaces", state_tags, interfaces, "false")  # Converted to new syntax by change_ind_scripts.py script

    for (_interface_name in admin_states) {
        # intentionally using a different tags array for this section. We are setting this tag in a loop -- don't
        # confuse this tag array with the one in the main AWK section -- the serve different purposes.
        state_tags["name"] = _interface_name

        # Need to check if this interface is a physical interface. In that case we return a metric that is going to be 
        # evaluated by scale rules and could generate alarms into the WebUI (only physical interfaces should generate alarms).
        if (_interface_name in phy_interfaces) {
            if (link_states[_interface_name] != -1) {
                writeDoubleMetric("network-interface-admin-state", state_tags, "gauge", admin_states[_interface_name], "true", "Network Interfaces - Enabled/Disabed", "state", "name")  # Converted to new syntax by change_ind_scripts.py script
                writeDoubleMetric("network-interface-state", state_tags, "gauge", link_states[_interface_name], "true", "Network Interfaces - Up/Down", "state", "name")  # Converted to new syntax by change_ind_scripts.py script
            # In the case that there is no link state, this is because it's a bond interface with a different name, 
            # e.g., "Sync" interface on asg devices. It's not a physical interface (even though we thought it was),
            # So we write the 'logical' metric: see comments below.
            } else {
                writeDoubleMetric("network-interface-admin-state-logical", state_tags, "gauge", admin_states[_interface_name], "true", "Network Interfaces - Enabled/Disabed", "state", "name")  # Converted to new syntax by change_ind_scripts.py script
            }
        } else {
            # If the interface is not physical, we use a different metric name (with "logical" as part of the name) 
            # because it shouldn't be evaluated by scala rules. I.e., we never want to alert for logical interfaces,
            # but we still want them to show up in the live config.
            # Admin state should be written always: all interfaces have admin state
            writeDoubleMetric("network-interface-admin-state-logical", state_tags, "gauge", admin_states[_interface_name], "true", "Network Interfaces - Enabled/Disabed", "state", "name")  # Converted to new syntax by change_ind_scripts.py script
    
            #link state is only written when the interface has link-state. bond, wrp, wrpj has no link state
            if (link_states[_interface_name] != -1) {
                writeDoubleMetric("network-interface-state-logical", state_tags, "gauge", link_states[_interface_name], "true", "Network Interfaces - Up/Down", "state", "name")  # Converted to new syntax by change_ind_scripts.py script
            }
        }
    }

    # Delete arrays between VSes
    delete state_tags
    delete tags
    delete interfaces
    delete admin_states
    delete link_states
    delete phy_interface
}

function getValue(s) {
    sub(/^.+:/, "", s)
    return s
}


BEGIN {
    vs_id = ""
    vs_name = ""
}

#_VSID:Name 3:lab-CP-VSX1_vsx-2
/^_VSID:Name/ {
    if (vs_id != "") {
        # Write the previous VS's data
        dumpVsInterfaceArrayData()
    }

    split($2, id_and_name, ":")
    vs_id = id_and_name[1]
    vs_name = id_and_name[2]
    next
}

#LAN1.246      Link encap:Ethernet  HWaddr 00:1C:7F:23:26:EB
/Link encap:/ {
    tags["vs.id"] = vs_id
    tags["vs.name"] = vs_name
    tags["im.identity-tags"] = "name"

    interface_name = $1
    tags["name"] = interface_name
    interfaces[interface_name, "name"] = interface_name

    admin_states[interface_name] = 0
    link_states[interface_name] = -1   # We may not find a link state for an interface. If so, we need to know that.

    # Creating a list of physical interfaces discarding all other possible types
    if ( interface_name !~ /br|wrp|vpnt|bond|pppoe|sit|[:,.]/ ) {
        phy_interfaces[interface_name]
    }

    # Type
    type = getValue($3)
    writeComplexMetricString("network-interface-type", tags, type, "true", "Network Interfaces - type")  # Converted to new syntax by change_ind_scripts.py script

    # MAC
    writeComplexMetricString("network-interface-mac", tags, $5, "true", "Network Interfaces - MAC Address")  # Converted to new syntax by change_ind_scripts.py script

    next
}

#            inet addr:192.168.245.2  Bcast:192.168.245.255  Mask:255.255.255.0
/inet addr:/ {
    ip = getValue($2)
    net_mask = getValue($4)

    ip_tags["name"] = interface_name
    ip_tags["im.identity-tags"] = "name"
    ip_tags["im.dstype.displaytype"] = "string"

    writeComplexMetricString("network-interface-ipv4-address", ip_tags, ip, "true", "Network Interfaces - IPv4 Address")  # Converted to new syntax by change_ind_scripts.py script
    writeComplexMetricString("network-interface-ipv4-subnet", ip_tags, net_mask, "true", "Network Interfaces - IPv4 Netmask")  # Converted to new syntax by change_ind_scripts.py script

    next
}

#            BROADCAST MULTICAST  MTU:1500  Metric:1
#            UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
/MTU/ {
    # MTU
    mtu = getValue($(NF - 1))
    interfaces[interface_name, "mtu"] = mtu
    writeDoubleMetric("network-interface-mtu", tags, "gauge", mtu, "true", "Network Interfaces - MTU", "number", "name")  # Converted to new syntax by change_ind_scripts.py script
    ## if admin state is "UP"  admin_state must be updated
    if ( $1 == "UP") {
        admin_states[interface_name] = 1
    }
    next
}

#        TX bytes:2096249117 packets:2975595223 errors:0 dropped:0 overruns:0 carrier:0
#        RX bytes:3891794260 packets:264187186 errors:0 dropped:0 overruns:0 frame:0
#        RX bytes:3964467449 (3.6 GiB)  TX bytes:922468769 (879.7 MiB)
#        RX packets:123982210 errors:0 dropped:0 overruns:0 frame:0
#        TX packets:61771739 errors:0 dropped:0 overruns:0 carrier:0
/(X bytes:|X packets:)/ {

    # Go over the line, field by field.
    for (i = 1; i <= NF; i++) {

        # Detect if current field contains RX or TX.
        #RX
        if ($i ~ /^(RX|TX)$/) {
            metric_prefix = tolower($i)
        }

        # Detect if the current field is data that should be stored
        #bytes:3964467449
        #errors:0
        if ($i ~ /[a-z]:[0-9]+$/) {
            split($i, stat_parts, ":")
            name = stat_parts[1]
            value = stat_parts[2]

            if (name == "bytes") {
                name = "bits"
                unit = "bits"
                value = value * 8
            } else {
                unit = "number"
            }

            writeDoubleMetric("network-interface-" metric_prefix "-" name, tags, "gauge", value, "true", "Network Interfaces - " metric_prefix " " name, unit, "name")  # Converted to new syntax by change_ind_scripts.py script
        }
    }

    next
}

#driver: vmxnet3
/driver:/ {
    interfaces[interface_name, "driver"] = $2

    next
}

#Speed: 100Mb/s
/Speed:/ {
    speed = $2
    gsub(/b\/s/, "", speed)
    # speed metric need special tags for alert purpose
    speed_tags["name"] = interface_name
    speed_tags["im.identity-tags"] = "name"
    writeComplexMetricString("network-interface-speed", speed_tags, speed, "true", "Network Interfaces - speed")  # Converted to new syntax by change_ind_scripts.py script

    next
}

#Duplex: Full
/Duplex:/ {
    duplex = tolower($2)
    writeComplexMetricString("network-interface-duplex", tags, duplex, "true", "Network Interfaces - duplex settings")  # Converted to new syntax by change_ind_scripts.py script

    next
}

#Link detected: yes
/Link detected:/ {
    if ($3 == "yes")
        link_states[interface_name] = 1
    else
        link_states[interface_name] = 0

    next
}

#Auto-negotiation: on
/Auto-negotiation:/ {
    interfaces[interface_name, "auto-negotiation"] = $2

    next
}


#interface:eth1:comments Private2\ lala
#interface:eth1:comments Test
/interface:.+:comments/ {
    line = $0
    sub(/interface:.+:comments\s*/, "", line)
    gsub(/(\\)/, "", line)  # If the comment is in two words or more, a backslash is inserted. We need to remove it.
    writeComplexMetricString("network-interface-description", tags, trim(line), "false")  # Converted to new syntax by change_ind_scripts.py script

    next
}

END {
    # Dump the last VS interfaces
    dumpVsInterfaceArrayData()
}