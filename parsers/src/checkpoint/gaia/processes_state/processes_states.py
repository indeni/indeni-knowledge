from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class ProcessesStates(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            # Parsing for whole ps aux output:
            ps_parse_dict = helper_methods.parse_data_as_list(raw_data, 'checkpoint_gaia_parser_ps_aux.textfsm')
            if ps_parse_dict:
                # syslogd handling
                syslogd_state_dict = {}
                for line in ps_parse_dict:
                    for key, value in line.items():
                        if 'syslogd' in value:
                            syslogd_state_dict.update({key: value})
                self.write_double_metric('process_state_syslogd_state', {}, 'gauge', 0 if 'syslogd' not in str(syslogd_state_dict.values()) else 1, False)

                # snmpd handling
                # regular GW / VSX with vs mode off
                snmpd_state_dict = {}
                n = 1
                for line in ps_parse_dict:
                    for key, value in line.items():
                        if 'snmpd' in value:
                            snmpd_state_dict.update({key + str(n): value})
                            n += 1
                self.write_double_metric('process_state_snmpd_state', {}, 'gauge', 0 if 'snmpd' not in str(snmpd_state_dict.values()) and 'process:snmpd t' in raw_data else 1, False)

                # Check NTP  enabled/disabled
                ntpd_parsed = helper_methods.parse_data_as_list(raw_data, 'checkpoint_show_ntp_active.textfsm')
                self.write_double_metric('process-state-ntpd-enabled', {}, 'gauge', 1 if ntpd_parsed[0]['status'] == 'Yes' else 0, False)

                # NTP state
                ps_parse_dict = helper_methods.parse_data_as_list(raw_data, 'checkpoint_gaia_parser_ps_aux.textfsm')
                if ps_parse_dict:
                    if device_tags.get('vsx') != 'true':
                        ntpd_found = 0
                        for process in ps_parse_dict:
                            if 'ntpd' in process['COMMAND']:
                                ntpd_found = 1
                        self.write_double_metric('process-state-ntpd-status', {}, 'gauge', ntpd_found, False)

                # VSX with vs mode on.
                # All VS's affected:
                if device_tags.get('vsx') == 'true' and 'vsx-proxy' in str(snmpd_state_dict.values()):
                    tags = {'name': 'All VS\'s are affected'}
                    self.write_double_metric('process_state_snmpd_vsx_state', tags, 'gauge', 0 if '/etc/snmp/vsx-proxy/CTX/' not in str(snmpd_state_dict.values()) else 1, False, 'name')

                    # If specific VS is affected
                    if '/etc/snmp/vsx-proxy/CTX/' in str(snmpd_state_dict.values()):
                        result = helper_methods.parse_data_as_object(raw_data, 'checkpoint_vsx_stat.textfsm')
                        vs_active = result['active_ids']
                        vs_set = set()
                        for value in snmpd_state_dict.values():
                            if '/etc/snmp/vsx-proxy/CTX/' in value and 'snmpd.user.conf' in value:
                                vs_id = helper_methods.parse_data_as_object(value, 'checkpoint_gaia_vs_number_snmpd.textfsm')
                                vs_set.add(int(vs_id.get('vs_num')))
                        for vs in vs_active:
                            tags = {'name': 'VS: ' + str(vs)}
                            if int(vs) in vs_set:
                                self.write_double_metric('process_state_snmpd_vsx_state', tags, 'gauge', 1, False, "name")
                            else:
                                self.write_double_metric('process_state_snmpd_vsx_state', tags, 'gauge', 0, False, "name")

        return self.output