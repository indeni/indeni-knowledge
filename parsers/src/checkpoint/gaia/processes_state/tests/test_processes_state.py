import os
import unittest
from checkpoint.gaia.processes_state.processes_states import ProcessesStates
from parser_service.public.action import WriteDoubleMetric

class TestLowSevParser1(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = ProcessesStates()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_syslogd_alert(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/test_syslogd_alert.input', {}, {})
        # Assert
        self.assertEqual(4, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'process_state_syslogd_state')
        self.assertEqual(result[0].value, 0)
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].name, 'process_state_snmpd_state')
        self.assertEqual(result[1].value, 1)
        self.assertEqual(result[2].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[2].ds_type, 'gauge')
        self.assertEqual(result[2].name, 'process-state-ntpd-enabled')
        self.assertEqual(result[2].value, 1)
        self.assertEqual(result[3].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[3].name, 'process-state-ntpd-status')
        self.assertEqual(result[3].value, 1)

    def test_syslogd_no_alert(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/test_syslogd_no_alert.input', {}, {})
        # Assert
        self.assertEqual(4, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'process_state_syslogd_state')
        self.assertEqual(result[0].value, 1)
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].name, 'process_state_snmpd_state')
        self.assertEqual(result[1].value, 1)
        self.assertEqual(result[2].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[2].name, 'process-state-ntpd-enabled')
        self.assertEqual(result[2].value, 1)
        self.assertEqual(result[3].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[3].name, 'process-state-ntpd-status')
        self.assertEqual(result[3].value, 1)

    def test_snmpd_non_vsx_alert(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/test_snmpd_non_vsx_alert.input', {}, {})
        # Assert
        self.assertEqual(4, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'process_state_syslogd_state')
        self.assertEqual(result[0].value, 1)
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].name, 'process_state_snmpd_state')
        self.assertEqual(result[1].value, 0)
        self.assertEqual(result[2].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[2].name, 'process-state-ntpd-enabled')
        self.assertEqual(result[2].value, 1)
        self.assertEqual(result[3].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[3].name, 'process-state-ntpd-status')
        self.assertEqual(result[3].value, 0)

    def test_snmpd_vsx_all_alert(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/test_snmpd_vsx_all_alert.input', {}, {'vsx': 'true'})
        # Assert
        self.assertEqual(4, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'process_state_syslogd_state')
        self.assertEqual(result[0].value, 1)
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].name, 'process_state_snmpd_state')
        self.assertEqual(result[1].value, 1)
        self.assertEqual(result[2].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[2].name, 'process-state-ntpd-enabled')
        self.assertEqual(result[2].value, 0)
        self.assertEqual(result[3].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[3].name, 'process_state_snmpd_vsx_state')
        self.assertEqual(result[3].tags['name'], "All VS's are affected")
        self.assertEqual(result[3].tags['display-name'], 'name')
        self.assertEqual(result[3].value, 0)

    def test_snmpd_vsx_specific_vs_alert(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/test_snmpd_vsx_specific_vs_alert.input', {}, {'vsx': 'true', 'vs-count': '3'})
        # Assert
        self.assertEqual(7, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'process_state_syslogd_state')
        self.assertEqual(result[0].value, 1)
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].name, 'process_state_snmpd_state')
        self.assertEqual(result[1].value, 1)
        self.assertEqual(result[2].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[2].name, 'process-state-ntpd-enabled')
        self.assertEqual(result[2].value, 0)
        self.assertEqual(result[3].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[3].name, 'process_state_snmpd_vsx_state')
        self.assertEqual(result[3].tags['name'], "All VS's are affected")
        self.assertEqual(result[3].tags['display-name'], 'name')
        self.assertEqual(result[3].value, 1)
        self.assertEqual(result[4].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[4].name, 'process_state_snmpd_vsx_state')
        self.assertEqual(result[4].tags['name'], 'VS: 1')
        self.assertEqual(result[4].tags['display-name'], 'name')
        self.assertEqual(result[4].value, 1)
        self.assertEqual(result[5].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[5].name, 'process_state_snmpd_vsx_state')
        self.assertEqual(result[5].tags['name'], 'VS: 2')
        self.assertEqual(result[5].tags['display-name'], 'name')
        self.assertEqual(result[5].value, 0)
        self.assertEqual(result[6].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[6].name, 'process_state_snmpd_vsx_state')
        self.assertEqual(result[6].tags['name'], 'VS: 3')
        self.assertEqual(result[6].tags['display-name'], 'name')
        self.assertEqual(result[6].value, 1)

    def test_snmpd_vsx_specific_vs_missing_no_alert(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/test_snmpd_vsx_specific_vs_missing_no_alert.input', {}, {'vsx': 'true', 'vs-count': '3'})
        # Assert
        self.assertEqual(6, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'process_state_syslogd_state')
        self.assertEqual(result[0].value, 1)
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].name, 'process_state_snmpd_state')
        self.assertEqual(result[1].value, 1)
        self.assertEqual(result[2].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[2].name, 'process-state-ntpd-enabled')
        self.assertEqual(result[2].value, 1)
        self.assertEqual(result[3].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[3].name, 'process_state_snmpd_vsx_state')
        self.assertEqual(result[3].tags['name'], "All VS's are affected")
        self.assertEqual(result[3].tags['display-name'], 'name')
        self.assertEqual(result[3].value, 1)
        self.assertEqual(result[4].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[4].name, 'process_state_snmpd_vsx_state')
        self.assertEqual(result[4].tags['name'], 'VS: 1')
        self.assertEqual(result[4].tags['display-name'], 'name')
        self.assertEqual(result[4].value, 1)
        self.assertEqual(result[5].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[5].name, 'process_state_snmpd_vsx_state')
        self.assertEqual(result[5].tags['name'], 'VS: 3')
        self.assertEqual(result[5].tags['display-name'], 'name')
        self.assertEqual(result[5].value, 1)
    
    def test_ntpd_ps_auxf(self):
        result = self.parser.parse_file(self.current_dir + '/test_ntpd_ps_auxf.input', {}, {})
        self.assertEqual(4, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'process_state_syslogd_state')
        self.assertEqual(result[0].value, 0)
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].name, 'process_state_snmpd_state')
        self.assertEqual(result[1].value, 1)
        self.assertTrue(result[2].action_type, WriteDoubleMetric)
        self.assertEqual('gauge', result[2].ds_type)
        self.assertEqual(1, result[2].value)
        self.assertEqual('process-state-ntpd-enabled', result[2].name)
        self.assertTrue(result[3].action_type, WriteDoubleMetric)
        self.assertEqual('gauge', result[3].ds_type)
        self.assertEqual(1, result[3].value)
        self.assertEqual('process-state-ntpd-status', result[3].name)
        
    def test_show_ntp_active(self):
        result = self.parser.parse_file(self.current_dir + '/test_show_ntp_active.input', {}, {})
        self.assertEqual(4, len(result))
        self.assertTrue(result[2].action_type, WriteDoubleMetric)
        self.assertEqual('gauge', result[2].ds_type)
        self.assertEqual(1, result[2].value)
        self.assertEqual('process-state-ntpd-enabled', result[2].name)
        self.assertTrue(result[3].action_type, WriteDoubleMetric)
        self.assertEqual('gauge', result[3].ds_type)
        self.assertEqual(1, result[3].value)
        self.assertEqual('process-state-ntpd-status', result[3].name)

    def test_processes_state_novsx(self):
        result = self.parser.parse_file(self.current_dir + '/test_processes_state_novsx.input', {}, {})
        self.assertEqual(4, len(result))
        self.assertTrue(result[2].action_type, WriteDoubleMetric)
        self.assertEqual('gauge', result[2].ds_type)
        self.assertEqual(1, result[2].value)
        self.assertEqual('process-state-ntpd-enabled', result[2].name)
        self.assertTrue(result[3].action_type, WriteDoubleMetric)
        self.assertEqual('gauge', result[3].ds_type)
        self.assertEqual(1, result[3].value)
        self.assertEqual('process-state-ntpd-status', result[3].name)

if __name__ == '__main__':
    unittest.main()