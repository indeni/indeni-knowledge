############
# Script explanation: The method for determining the jumbo take changes between major versions, and also between jumbo hotfix versions. 
# Check point KB: sk98028
# Currently supports: R75.47, R76, R77, R77.10, R77.20, R77.30, R80 (and probably future versions as well since the command is the same after R77.30)
# 2/22/17 Modifications: the original script did not account for cases where multiple hotfixes can be reported from a GAIA device.
# We need to account for only the latest. For the latest hotfixes, the command "installed_jumbo_take -n". However, older hotfixes need to be queried from their respective registry, depending on the OS version.
# The logic has been modified to only parse the first value with the grep value. This way, we only collect the latest hotfix that has been applied to the system.
###########

/[0-9]/ {
    i++
    # 159
    output[i] = $1
}

END {
    if (output[1] == "") {
        jumboTake = 0
    } else {
        jumboTake = output[1]
    }
    writeComplexMetricString("hotfix-jumbo-take", null, jumboTake, "true", "Installed Jumbo Hotfix Take")  # Converted to new syntax by change_ind_scripts.py script
}