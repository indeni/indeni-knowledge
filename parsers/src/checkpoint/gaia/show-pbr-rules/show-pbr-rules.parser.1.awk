# routed:instance:default:pbrrules:priority:6:to:2.2.2.0 t
# routed:instance:default:pbrrules:priority:6:to:2.2.2.0:masklen 24
/routed:instance:default:pbrrules:priority:[0-9]+:to/ {
	split($1,splitArr,":")
	
	if(arraylen(splitArr) == 9) {
		priority = splitArr[6]

		rules[priority, "destination"] = splitArr[8] "/" $2
	}
}

# routed:instance:default:pbrrules:priority:6:from:2.3.3.0 t
# routed:instance:default:pbrrules:priority:6:from:2.3.3.0:masklen 24
/routed:instance:default:pbrrules:priority:[0-9]+:from/ {
	split($1,splitArr,":")
	
	if(arraylen(splitArr) == 9) {
		priority = splitArr[6]

		rules[priority, "source"] = splitArr[8] "/" $2
	}
}

# routed:instance:default:pbrrules:priority:6:protocol 6
/routed:instance:default:pbrrules:priority:[0-9]+:protocol/ {
	split($1,splitArr,":")
	priority = splitArr[6]
	protocol = $NF
	if (protocol == "6") {
		protocol = "TCP"
	} else if (protocol == "17") {
		protocol = "UDP"
	} else if (protocol == "1") {
		protocol = "ICMP"
	}
	
	rules[priority, "protocol"] = protocol
}

# routed:instance:default:pbrrules:priority:6:port 20
/routed:instance:default:pbrrules:priority:[0-9]+:port/ {
	split($1,splitArr,":")
	priority = splitArr[6]
	
	rules[priority, "service-port"] = $NF
}

# routed:instance:default:pbrrules:priority:6:dev eth0
/routed:instance:default:pbrrules:priority:[0-9]+:port/ {
	split($1,splitArr,":")
	priority = splitArr[6]
	
	rules[priority, "interface"] = $NF
}

# routed:instance:default:pbrrules:priority:6:table 3
/routed:instance:default:pbrrules:priority:[0-9]+:table/ {
	split($1,splitArr,":")
	priority = splitArr[6]
	
	rules[priority, "table"] = $NF
}

# routed:instance:default:pbrrules:priority:6:tname routeDefault
/routed:instance:default:pbrrules:priority:[0-9]+:tname/ {
	split($1,splitArr,":")
	priority = splitArr[6]
	
	rules[priority, "table-name"] = $NF
}

END {
	writeComplexMetricObjectArray("pbr-rules", null, rules, "false")  # Converted to new syntax by change_ind_scripts.py script
}