import os
import unittest
from checkpoint.gaia.cpu_spike_microburst.cpu_spike_microburst import CheckpointCpuSpikeMicroburst
from parser_service.public.action import *

class TestCheckpointCpuSpikeMicroburst(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = CheckpointCpuSpikeMicroburst()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_cpu_spike_microburst(self):
        result = self.parser.parse_file(self.current_dir + '/cpu_spike_microburst.input', {}, {})
        self.assertEqual(4, len(result))
        self.assertEqual(result[0].tags['cpu-id'], '1')
        self.assertEqual(result[0].value, 0)
        self.assertEqual(result[1].tags['cpu-id'], '2')
        self.assertEqual(result[1].value, 1)
        self.assertEqual(result[2].tags['cpu-id'], '3')
        self.assertEqual(result[2].value, 1)
        self.assertEqual(result[3].tags['cpu-id'], '4')
        self.assertEqual(result[3].value, 1)

    def test_cpu_spike_microburst_empty(self):
        result = self.parser.parse_file(self.current_dir + '/cpu_spike_microburst_empty.input', {}, {})
        self.assertEqual(0, len(result))

if __name__ == '__main__':
    unittest.main()