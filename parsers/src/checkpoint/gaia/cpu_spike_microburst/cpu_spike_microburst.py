from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class CheckpointCpuSpikeMicroburst(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data = helper_methods.parse_data_as_list(raw_data, 'cpu_spike_microburst.textfsm')
            if data:
                alerted_cores = []
                alerted_cores = [x['cpu_core'] for x in data if x['cpu_core'] not in alerted_cores]
                for cpu_id in range(1,int(data[0]['num_core'])+1,1):
                    tags = {}
                    tags['cpu-id'] = str(cpu_id)
                    self.write_double_metric('cpu-microburst', tags, 'gauge', 0 if str(cpu_id) in alerted_cores else 1, False)
        return self.output