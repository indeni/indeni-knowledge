num_cpu=$(cpstat os -f cpu | grep "CPUs Number" | awk '{print $NF}');
echo "__NUM_CPU: $num_cpu";
grep -e $(date +"%0d/%0m/%0y") -e $(date +"%0m/%0d/%0y") /var/log/spike_detective/spike_detective.log | sed -n "/ $(date +\%R -d "-9 min")/,$"p