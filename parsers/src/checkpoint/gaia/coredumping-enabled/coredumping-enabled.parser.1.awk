BEGIN{
    kdump_enabled = "false"
    cdump_enabled = "false"
}
#Kdump is operational
/^(Kdump is operational)/ {
    kdump_enabled = "true"
}

#Core dumps enabled
/^(Core dumps enabled)/ {
    cdump_enabled = "true"
}

END {
    tags["dump-type"] = "Kdump"
    tags["im.dstype.displaytype"] = "Boolean"
    writeComplexMetricString("coredumping-enabled", tags, kdump_enabled, "true", "Kernel Dump")  # Converted to new syntax by change_ind_scripts.py script
    tags["dump-type"] = "Cdump"
    tags["im.dstype.displaytype"] = "Boolean"
    writeComplexMetricString("coredumping-enabled", tags, cdump_enabled, "true", "Core Dump")  # Converted to new syntax by change_ind_scripts.py script
}