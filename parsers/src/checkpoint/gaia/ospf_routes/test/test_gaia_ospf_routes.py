import os
import unittest

from checkpoint.gaia.ospf_routes.parser.gaia_ospf_routes import GaiaOspfRoutes
from parser_service.public.action import *

class TestGaiaOspfRoutes(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = GaiaOspfRoutes()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_valid_input_vsx_should_not_alert(self):
        expected_results = [{'name': '3.3.3.0 of VS_id: 0', 'metric': 'ospf_missing_routes_vsx', 'value': 0}]

        # Act
        result = self.parser.parse_file(self.current_dir + '/valid_input_vsx.input', {}, {'vsx': 'true'})
        # Assert
        self.assertEqual(1, len(result))

        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i]['name'], result[i].tags['name'])
            self.assertEqual(expected_results[i]['metric'], result[i].name)
            self.assertEqual(expected_results[i]['value'], result[i].value)


    def test_valid_input_alert_vsx(self):
        expected_results = [{'name': '3.3.3.0 of VS_id: 0', 'metric': 'ospf_missing_routes_vsx', 'value': 0},
                            {'name': '10.11.80.16 of VS_id: 1', 'metric': 'ospf_missing_routes_vsx', 'value': 1},
                            {'name': '23.5.6.0 of VS_id: 1', 'metric': 'ospf_missing_routes_vsx', 'value': 1}]

        # Act
        result = self.parser.parse_file(self.current_dir + '/valid_input_alert_vsx.input', {}, {'vsx': 'true'})
        # Assert
        self.assertEqual(3, len(result))

        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i]['name'], result[i].tags['name'])
            self.assertEqual(expected_results[i]['metric'], result[i].name)
            self.assertEqual(expected_results[i]['value'], result[i].value)


    def test_empty_input_vsx(self):
        #Arrange
        expected_results = []

        # Act
        result = self.parser.parse_file(self.current_dir + '/empty_input.input', {}, {'vsx': 'true'})

        # Assert
        self.assertEqual(0, len(result))


    def test_valid_input_should_not_alert(self):
        # Arrange
        expected_results = [{'name': '10.11.80.16', 'metric': 'ospf_missing_routes', 'value': 0},
                            {'name': '23.5.6.0', 'metric': 'ospf_missing_routes', 'value': 0}]

        # Act
        result = self.parser.parse_file(self.current_dir + '/valid_input.input', {}, {})

        # Assert
        self.assertEqual(2, len(result))

        # Assert
        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i]['name'], result[i].tags['name'])
            self.assertEqual(expected_results[i]['metric'], result[i].name)
            self.assertEqual(expected_results[i]['value'], result[i].value)


    def test_valid_input_alert(self):
        #Arrange
        expected_results = [{'name': '54.6.7.8', 'metric': 'ospf_missing_routes', 'value': 1},
                            {'name': '10.11.80.16', 'metric': 'ospf_missing_routes', 'value': 0},
                            {'name': '23.5.6.0', 'metric': 'ospf_missing_routes', 'value': 0}]

        # Act
        result = self.parser.parse_file(self.current_dir + '/valid_input_alert.input', {}, {})

        # Assert
        self.assertEqual(3, len(result))

        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i]['name'], result[i].tags['name'])
            self.assertEqual(expected_results[i]['metric'], result[i].name)
            self.assertEqual(expected_results[i]['value'], result[i].value)


    def test_valid_R81_vsx_alert(self):
        #Arrange
        expected_results = [{'name': 'default-gateway of VS_id: 2', 'metric': 'ospf_missing_routes_vsx', 'value': 0},
                            {'name': '24.5.6.0 of VS_id: 2', 'metric': 'ospf_missing_routes_vsx', 'value': 0}, 
                            {'name': '81.0.5.0 of VS_id: 2', 'metric': 'ospf_missing_routes_vsx', 'value': 0}, 
                            {'name': '81.0.20.0 of VS_id: 2', 'metric': 'ospf_missing_routes_vsx', 'value': 1}]

        # Act
        result = self.parser.parse_file(self.current_dir + '/test_valid_R81_vsx_alert.input', {}, {'vsx': 'true'})

        # Assert
        self.assertEqual(4, len(result))

        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i]['name'], result[i].tags['name'])
            self.assertEqual(expected_results[i]['metric'], result[i].name)
            self.assertEqual(expected_results[i]['value'], result[i].value)


    def test_empty_input_non_vsx(self):
        #Arrange
        expected_results = []

        # Act
        result = self.parser.parse_file(self.current_dir + '/empty_input.input', {}, {})

        # Assert
        self.assertEqual(0, len(result))


if __name__ == '__main__':
    unittest.main()
