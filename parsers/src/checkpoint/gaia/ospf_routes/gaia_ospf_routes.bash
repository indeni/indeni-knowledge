#!/bin/bash

source /etc/profile.d/vsenv.sh
vsx=$( (vsx stat) 2>&1)
linuxver=$(uname -r | cut -d '-' -f 1) ;
if [[ "$vsx" = *"VSX is not supported"* ]]; then clish -c "show route ospf" ; echo '' ; echo "==Kernel routes" ;  netstat -rn ; elif [ "$linuxver" = "2.6.18" ]; then for vrf in $(vrf list vrfs) ; do vsenv "$vrf" ; printf "set virtual-system %s\nshow route ospf\nexit\n" "$vrf" | clish ; echo '' ; echo "==Kernel routes" ; netstat -rn ; done ; vsenv 0 ; else for vrf in $(netns list) ; do if [[ $vrf != [0] ]] ; then vsenv "$vrf" ; iclid instance "$vrf" -c 'show route ospf' ; echo '' ; echo "==Kernel routes" ; netstat -rn ; fi ; done ; vsenv 0 ; clish -c 'show route ospf' ; echo '' ; echo "==Kernel routes" ; netstat -rn ; vsenv 0 ; fi