from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class GaiaOspfRoutes(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data and len(raw_data) > 1:
            # Checking if the device is VSX, to determine best code:
            if device_tags.get('vsx') == 'true':
                # Remove last line, which is not needed:
                clean_vsx_string = raw_data[:raw_data.rfind('\n')]
                # Seperating raw_data to sections:
                basic = clean_vsx_string.split('Context is set to Virtual')[1:]
                # print(basic)
                if basic:
                    for vs_data in basic:
                        vs_id = helper_methods.parse_data_as_list(vs_data, 'checkpoint_gaia_vs_id.textfsm')
                        gaia_routes, kernel_routes_list = vs_data.split('==')

                        # Parsing to create list of needed data:
                        if vs_id and gaia_routes and kernel_routes_list:
                            gaia_routes_to_kernel = helper_methods.parse_data_as_list(gaia_routes, 'checkpoint_gaia_show_route_ospf.textfsm')
                            kernel_routes = helper_methods.parse_data_as_list(kernel_routes_list, 'checkpoint_gaia_netstat_rn.textfsm')     
                            
                            # Compare the lists, and create mertic:
                            if gaia_routes_to_kernel and kernel_routes:
                                vsid = vs_id.pop()['vs_id']
                                for entry in gaia_routes_to_kernel:
                                    # Changing default route in tags:
                                    if entry['destination'] == '0.0.0.0':
                                        tags = {'name': 'default-gateway of VS_id: ' + vsid}
                                    else:
                                        tags = {'name': entry['destination'] + ' of VS_id: ' + vsid}
                                    self.write_double_metric('ospf_missing_routes_vsx', tags, 'gauge', 1 if entry not in kernel_routes else 0, False)
            # In case the machine is not VSX, use this code:
            else:
                # Seperating raw_data to sections:
                gaia_ospf_routes, kernel_routes = raw_data.split('==')
                
                # Parsing to create list of needed data:
                gaia_routes_to_kernel = helper_methods.parse_data_as_list(gaia_ospf_routes, 'checkpoint_gaia_show_route_ospf.textfsm')
                kernel_routes = helper_methods.parse_data_as_list(kernel_routes, 'checkpoint_gaia_netstat_rn.textfsm')     
                
                # Compare the lists, and create mertic:
                if gaia_routes_to_kernel and kernel_routes:
                    for entry in gaia_routes_to_kernel:
                        # Changing default route in tags:
                        if entry['destination'] == '0.0.0.0':
                            tags = {'name': 'default-gateway'}
                        else:
                            tags = {'name': entry['destination']}
                        
                        self.write_double_metric('ospf_missing_routes', tags, 'gauge', 1 if entry not in kernel_routes else 0, False)
        return self.output            
