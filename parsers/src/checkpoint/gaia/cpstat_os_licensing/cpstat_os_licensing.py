import datetime
from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class CpstatOsLicensing(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            current_time = helper_methods.parse_data_as_list(raw_data, 'cpstat_os_licensing_current_time.textfsm')
            if current_time:
                parsed_data = helper_methods.parse_data_as_list(raw_data, 'cpstat_os_licensing.textfsm')
                if parsed_data:
                    for data in parsed_data:
                        tags = {}
                        tags = {"name": data.get('name'), 'im.identity-tags': 'name', 'im.dstype.displaytype': 'string'}
                        if int(data.get('blade_status')) == 1 and data.get('entitlement_status') in ('Entitled','Expired'):
                            if int(data.get('expires_date')) != 0:
                                self.write_double_metric('license-expiration', tags, "gauge", int(data.get('expires_date')), True, "License Blade - Expiration", 'date', 'name')
                        self.write_complex_metric_string('license-blade-entitlement', tags, data.get('entitlement_status'), True, "License Blade - Entitlement")
                        self.write_double_metric('license-blade-status', tags, 'gauge',int(data.get('blade_status')), True, "License Blade - Status",'state')
                        if int(data.get('total_quota')) != 0:
                            self.write_double_metric('license-total-quota', tags, 'gauge', int(data.get('total_quota')), False)
                            self.write_double_metric('license-used-quota', tags, 'gauge', int(data.get('used_quota')), False)
        return self.output