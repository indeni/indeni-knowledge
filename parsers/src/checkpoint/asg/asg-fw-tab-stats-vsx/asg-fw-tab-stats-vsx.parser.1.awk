# SSH Command Notes
# See: https://indeni.atlassian.net/browse/IKP-2110
# We've had problems with scripts (and SSH commands) taking a very long time to run on large VSX deployments. This
# command includes some performance optimizations; e.g., it uses xargs to run the subsequent Check Point CLI commands
# in parallel bash processes. Here's the basic outline:
# * Capture the VSID and Name and pass each one to xargs.
# * xargs 'calls' bash, spawning it in up to -P processes.
# * Initialize the shell with /etc/profile.d/vsenv.sh. Required to run vsenv command.
# * Split out the VS ID and name from idAndName -- using idAndName like this avoids having to run 'vsx stat' for each VS.
# * Set the vsenv and run the actual Check Point command. Capture the output into 'result' and echo it.
# Specific notes:
# -n max args; i.e., deal with one VS at a time
# -P max number of parallel processes
# -I{} -- in the following command to xargs, for each line of input from stdin, replace {} with that input. In this case, run bash passing each line from stdin as the variable 'idAndName'.
# "" -> get value of var as string
# '' -> print/create this exact string
# `` -> execute this string

#This script was forked off of fw-tab-stats-vsx because we needed a unique script that does not parse the nat-connections metric.
#nat-connections metric is parsed from asg-perf-vsx instead.

function dumpVsTableData() {

    for (table_name in actuals) {
        if (table_name in limits && limits[table_name] != "") {
            if (table_name in accept) {
                if (table_name !~ /nrb_hitcount_table|cptls_server_cn_cache|fwx_alloc|fwx_cache|spii_multi_pset2kbuf_map|drop_tmpl_timer|fa_free_disk_space|mal_stat_src_week|rtm_view_[0-9]*/) {
                    table_tags["name"] = table_name
                    table_tags["vs.id"] = vs_id
                    table_tags["vs.name"] = vs_name

                    writeDoubleMetric("kernel-table-actual", table_tags, "gauge", actuals[table_name], "false")  # Converted to new syntax by change_ind_scripts.py script
                    writeDoubleMetric("kernel-table-limit", table_tags, "gauge", limits[table_name], "false")  # Converted to new syntax by change_ind_scripts.py script
                }
                if (table_name == "pep_identity_index") {
                    identity_tags["vs.id"] = vs_id
                    identity_tags["vs.name"] = vs_name

                    writeDoubleMetric("identity-awareness-users-actual", identity_tags, "gauge", actuals[table_name], "true", "Identity Awareness User Count", "number", "vs.id|vs.name")  # Converted to new syntax by change_ind_scripts.py script
                    writeDoubleMetric("identity-awareness-users-limit", identity_tags, "gauge", limits[table_name], "true", "Identity Awareness User Limit", "number", "vs.id|vs.name")  # Converted to new syntax by change_ind_scripts.py script
                }
            }
        }
    }
}


BEGIN {
    vs_id = ""
    vs_name = ""
    foundData = 0

    accept["fwx_alloc"] = 1
    accept["connections"] = 1
    accept["fwx_cache"] = 1
    accept["cphwd_db"] = 1
    accept["cphwd_tmpl"] = 1
    accept["cphwd_dev_conn_table"] = 1
    accept["cphwd_vpndb"] = 1
    accept["cphwd_dev_identity_table"] = 1
    accept["cphwd_dev_revoked_ips_table"] = 1
    accept["cphwd_pslglue_conn_db"] = 1
    accept["f2f_addresses"] = 1
    accept["tcp_f2f_ports"] = 1
    accept["udp_f2f_ports"] = 1
    accept["tcp_f2f_conns"] = 1
    accept["udp_f2f_conns"] = 1
    accept["dos_suspected"] = 1
    accept["dos_penalty_box"] = 1
    accept["client_auth"] = 1
    accept["pdp_sessions"] = 1
    accept["pdp_super_sessions"] = 1
    accept["pdp_ip"] = 1
    accept["crypt_resolver_DB"] = 1
    accept["cluster_active_robo"] = 1
    accept["appi_connections"] = 1
    accept["cluster_connections_nat"] = 1
    accept["cryptlog_table"] = 1
    accept["DAG_ID_to_IP"] = 1
    accept["DAG_IP_to_ID"] = 1
    accept["decryption_pending"] = 1
    accept["encryption_requests"] = 1
    accept["IKE_SA_table"] = 1
    accept["ike2esp"] = 1
    accept["ike2peer"] = 1
    accept["inbound_SPI"] = 1
    accept["initial_contact_pending"] = 1
    accept["ipalloc_tab"] = 1
    accept["IPSEC_mtu_icmp"] = 1
    accept["IPSEC_mtu_icmp_wait"] = 1
    accept["L2TP_lookup"] = 1
    accept["L2TP_MSPI_cluster_update"] = 1
    accept["L2TP_sessions"] = 1
    accept["L2TP_tunnels"] = 1
    accept["MSPI_by_methods"] = 1
    accept["MSPI_cluster_map"] = 1
    accept["MSPI_cluster_update"] = 1
    accept["MSPI_cluster_reverse_map"] = 1
    accept["MSPI_req_connections"] = 1
    accept["MSPI_requests"] = 1
    accept["outbound_SPI"] = 1
    accept["peer2ike"] = 1
    accept["peers_count"] = 1
    accept["persistent_tunnels"] = 1
    accept["rdp_dont_trap"] = 1
    accept["rdp_table"] = 1
    accept["resolved_link"] = 1
    accept["Sep_my_IKE_packet"] = 1
    accept["SPI_requests"] = 1
    accept["udp_enc_cln_table"] = 1
    accept["udp_response_nat"] = 1
    accept["VIN_SA_to_delete"] = 1
    accept["vpn_active"] = 1
    accept["vpn_routing"] = 1
    accept["XPO_names"] = 1
    accept["vpn_queues"] = 1
    accept["ikev2_sas"] = 1
    accept["sam_requests"] = 1
    accept["tnlmon_life_sign"] = 1
    accept["cptls_server_cn_cache"] = 1
    accept["string_dictionary_table"] = 1
    accept["dns_cache_tbl"] = 1
    accept["nrb_hitcount_table"] = 1
    accept["pep_identity_index"] = 1
}

#_VSID:Name 3:lab-CP-VSX1_vsx-2
/^_VSID:Name/ {
    if (foundData) {
        dumpVsTableData()
    }
    delete actuals
    delete limits
    split($2, id_and_name, ":")
    vs_id = id_and_name[1]
    vs_name = id_and_name[2]
    foundData = 1
    next
}

#-------- drop_tmpl_timer --------
/^----/ {
    if (NF == 3) {
        table_name = $2
    }

    next
}

#dynamic, id 142, attributes: expires 3600, refresh, , hashsize 512, limit 1
/dynamic.*limit \d/ {
    if (match($0, "limit \\d+")) {
        limit = substr($0, RSTART + 6, RSTART + RLENGTH - 6)
        limits[table_name] = limit
    }

    next
}

#localhost             vsx_firewalled                       0     1     1       0
/localhost.*/ {
    table_name = $2
    actual_value = $4
    actuals[table_name] = actual_value

    next
}

END {
    # The last VS is handled here:
    dumpVsTableData()
}