from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser
import re
from collections import defaultdict


class StatsCurrent(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        total_con_connections = 0
        total_throughput_dict = {}
        # Step 1: Data Extraction
        data = helper_methods.parse_data_as_list(raw_data, 'asg_stats_current.textfsm')
        for record in data:
            total_con_connections += int(record['conconnections'])
            if record['vsid'] in total_throughput_dict:
                total_throughput_dict[record['vsid']] += int(record['throughput'])
            else:
                total_throughput_dict[record['vsid']] = int(record['throughput'])
        for vsid, val in total_throughput_dict.items():
            self.write_double_metric('asg-system-total-vs-throughput', {'vs.id': vsid}, 'gauge', int(val), True,
                                     "Throughput (per VS)", 'number', "vs.id")
        self.write_double_metric('asg-system-total-concurrent-connections', {'vs.id': "all"}, 'gauge', total_con_connections
                                 , True, "Connections - VS (Total)", 'number', "vs.id")
        return self.output
