BEGIN {
    # This file is separated by "|"
    FS = "|"
}

# Identifies the chassis number of the local machine
#1_1
/^[0-9]+\_+[0-9]/{
    split($0, chassis_arr, "_")
    local_chassis = "Chassis " chassis_arr[1]
}

function convertToKB (prefix, var) {
    # Converts a number with prefix "G (Giga)" or "M (Mega)" to kilo
    if (prefix == "G") {
        var = (var * 1000) * 1000
    } else if (prefix == "M") {
        var = var * 1000
    }
    return var
}

#|1_01        |Memory                   |43%         |50%         |1.8G (!)          |
#|1_02        |Memory                   |31%         |50%         |31.3G             |
/^\|[0-9]_[0-9][0-9]/ {
    split($2, splitArr, "_")
    chassis = splitArr[1]
    blade = trim(splitArr[2])
}

#|1_04        |Memory                   |30%         |50%         |31.3G             |
#|1_02        |Memory                   |53% (!)     |50%         |1.8G (!)          |
/Memory/ {
    if (chassis_arr[1] == chassis) {
        usage_ram = $4
        #53% (!)
        #30%
        gsub(/[^0-9.]/, "", usage_ram)

        total_ram = $6
        #1.8G (!)
        #31.3G
        gsub(/[^0-9.]/, "", total_ram)

        # Extract unit prefix, for example G for giga
        unit_prefix = $6
        #1.8G (!)
        #31.3G
        gsub(/[^A-Za-z]/, "", unit_prefix)

        # Converting to kilobytes
        total_ram = convertToKB(unit_prefix, total_ram)

        # Calculating free RAM
        free_ram =  total_ram - (total_ram * (usage_ram / 100))

        ram_tags["name"] = "RAM - " "chassis: " chassis " blade: " blade
        ram_tags["resource-metric"] = "true"
        writeDoubleMetric("memory-free-kbytes", ram_tags, "gauge", free_ram, "true", "Memory - Free", "kbytes", "name")  # Converted to new syntax by change_ind_scripts.py script
        writeDoubleMetric("memory-total-kbytes", ram_tags, "gauge", total_ram, "true", "Memory - Total", "kbytes", "name")  # Converted to new syntax by change_ind_scripts.py script
        writeDoubleMetric("memory-usage", ram_tags, "gauge", usage_ram, "true", "Memory - Usage", "percentage", "name")  # Converted to new syntax by change_ind_scripts.py script
    }
}

#|            |HD: /                    |81% (!)     |80%         |4.8G (!)          |
#|            |HD: /                    |29%         |80%         |19.4G             |
/HD: / {
    if (chassis_arr[1] == chassis) {
        mount_point = $3
        #HD: /
        gsub(/HD:|[ ]/, "", mount_point)

        usage_hd_percent = $4
        #81% (!)
        #29%
        gsub(/[^0-9.]/, "", usage_hd_percent)

        total_hd = $6
        #4.8G (!)
        #19.4G
        gsub(/[^0-9.]/, "", total_hd)


        # Extract unit prefix
        unit_prefix = $6
        #1.8G (!)
        #31.3G
        gsub(/[^A-Za-z]/, "", unit_prefix)

        # Converting to kilobytes
        total_hd = convertToKB(unit_prefix, total_hd)

        # Calculating free space
        usage_hd_kb =  total_hd * (usage_hd_percent / 100)

        mount_tags["file-system"] = "chassis_" chassis "-" "blade_" blade "-" mount_point
        writeDoubleMetric("disk-usage-percentage", mount_tags, "gauge", usage_hd_percent, "true", "Mount Points - Usage", "percentage", "file-system")  # Converted to new syntax by change_ind_scripts.py script
        writeDoubleMetric("disk-used-kbytes", mount_tags, "gauge", usage_hd_kb, "true", "Mount Points - Used", "kbytes", "file-system")  # Converted to new syntax by change_ind_scripts.py script
        writeDoubleMetric("disk-total-kbytes", mount_tags, "gauge", total_hd, "true", "Mount Points - Total", "kbytes", "file-system")  # Converted to new syntax by change_ind_scripts.py script
    }
}