from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class AsgHwStat(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict, required=None):
        if raw_data:
            # Step 1: Data Extraction
            chassis_local_id_object = helper_methods.parse_data_as_object(raw_data, 'checkpoint_gaiasp_asg_stat_i_local_id.textfsm')
            chassis_required_list = helper_methods.parse_data_as_list(raw_data, 'checkpoint_gaiasp_asg_stat.textfsm')
            chassis_hw_monitor_list = helper_methods.parse_data_as_list(raw_data, 'checkpoint_gaiasp_asg_hw_monitor_v.textfsm')
            chassis_hw_monitor_stripped_list = []
            chassis_local_id = chassis_local_id_object['chassis_local_id']

            if chassis_hw_monitor_list:
                for dict in chassis_hw_monitor_list:
                    new_dict = {key: value.strip() for key, value in dict.items()}
                    chassis_hw_monitor_stripped_list.append(new_dict)

            # Reporting all elements status, and CPU temperature:
            if chassis_hw_monitor_stripped_list and chassis_local_id:
                for dict in chassis_hw_monitor_stripped_list:
                    if dict['chassis_name'] == chassis_local_id:
                        name = 'Chassis ' + dict['chassis_name'] + ":" + dict['sensor_name'] + ':' + dict['location']
                        state = dict['state']
                        element_tags = {'name': name.replace(':N/A', '')}
                        self.write_double_metric("hardware-element-status", element_tags, "gauge", int(state), True, "Hardware Status", "state", "name")

                        if dict['sensor_name'] == 'CPUtemp':
                            value = dict['current_value']
                            threshold = dict['threshold_value']
                            self.write_double_metric("temperature-sensor-current", element_tags, "gauge", int(value), True, "Temperature - Current", "number", "name")
                            self.write_double_metric("temperature-sensor-max", element_tags, "gauge", int(threshold), True, "Temperature - Max Limit", "number", "name")

            # Reporting the required amount of power units and fans for a chassis fail-over:
            if chassis_required_list and chassis_local_id:
                for dict in chassis_required_list:
                    if dict['Sensor'] == 'Power Supplies' or dict['Sensor'] == 'PSUs':
                        self.write_double_metric('power-unit-amount-required', {}, 'gauge', int(dict['Required']), False)
                        self.write_double_metric('power-unit-amount-current', {}, 'gauge', int(dict['Active']), False)
                    elif dict['Sensor'] == 'Fans':
                        self.write_double_metric('fans-amount-required', {}, 'gauge', int(dict['Required']), False)
                        self.write_double_metric('fans-amount-current', {}, 'gauge', int(dict['Active']), False)
        return self.output
