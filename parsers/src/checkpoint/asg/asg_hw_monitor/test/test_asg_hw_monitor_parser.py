import os
import unittest

from checkpoint.asg.asg_hw_monitor.asg_hw_monitor_parser import AsgHwStat
from parser_service.public.action import *

class TestEnabledBlades_NoVsx(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = AsgHwStat()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_valid_input_missing_power_unit(self):
        expected_results = [{'name': 'Chassis 1:CMM:bay 1', 'metric': 'hardware-element-status', 'value': 1},
                            {'name': 'Chassis 1:CMM:bay 2', 'metric': 'hardware-element-status', 'value': 1},
                            {'name': 'Chassis 1:CPUtemp:blade 1, CPU0', 'metric': 'hardware-element-status', 'value': 1},
                            {'name': 'Chassis 1:CPUtemp:blade 1, CPU0', 'metric': 'temperature-sensor-current', 'value': 38},
                            {'name': 'Chassis 1:CPUtemp:blade 1, CPU0', 'metric': 'temperature-sensor-max', 'value': 95},
                            {'name': 'Chassis 1:CPUtemp:blade 1, CPU1', 'metric': 'hardware-element-status', 'value': 1},
                            {'name': 'Chassis 1:CPUtemp:blade 1, CPU1', 'metric': 'temperature-sensor-current', 'value': 38},
                            {'name': 'Chassis 1:CPUtemp:blade 1, CPU1', 'metric': 'temperature-sensor-max', 'value': 95},
                            {'name': 'Chassis 1:CPUtemp:blade 2, CPU0', 'metric': 'hardware-element-status', 'value': 1},
                            {'name': 'Chassis 1:CPUtemp:blade 2, CPU0', 'metric': 'temperature-sensor-current', 'value': 36},
                            {'name': 'Chassis 1:CPUtemp:blade 2, CPU0', 'metric': 'temperature-sensor-max', 'value': 95},
                            {'name': 'Chassis 1:CPUtemp:blade 2, CPU1', 'metric': 'hardware-element-status', 'value': 1},
                            {'name': 'Chassis 1:CPUtemp:blade 2, CPU1', 'metric': 'temperature-sensor-current', 'value': 35},
                            {'name': 'Chassis 1:CPUtemp:blade 2, CPU1', 'metric': 'temperature-sensor-max', 'value': 95},
                            {'name': 'Chassis 1:CPUtemp:blade 3, CPU0', 'metric': 'hardware-element-status', 'value': 1},
                            {'name': 'Chassis 1:CPUtemp:blade 3, CPU0', 'metric': 'temperature-sensor-current', 'value': 36},
                            {'name': 'Chassis 1:CPUtemp:blade 3, CPU0', 'metric': 'temperature-sensor-max', 'value': 95},
                            {'name': 'Chassis 1:CPUtemp:blade 3, CPU1', 'metric': 'hardware-element-status', 'value': 1},
                            {'name': 'Chassis 1:CPUtemp:blade 3, CPU1', 'metric': 'temperature-sensor-current', 'value': 36},
                            {'name': 'Chassis 1:CPUtemp:blade 3, CPU1', 'metric': 'temperature-sensor-max', 'value': 95},
                            {'name': 'Chassis 1:Fan:bay 1, fan 1', 'metric': 'hardware-element-status', 'value': 1},
                            {'name': 'Chassis 1:Fan:bay 1, fan 2', 'metric': 'hardware-element-status', 'value': 1},
                            {'name': 'Chassis 1:Fan:bay 1, fan 3', 'metric': 'hardware-element-status', 'value': 1},
                            {'name': 'Chassis 1:Fan:bay 2, fan 1', 'metric': 'hardware-element-status', 'value': 1},
                            {'name': 'Chassis 1:Fan:bay 2, fan 2', 'metric': 'hardware-element-status', 'value': 1},
                            {'name': 'Chassis 1:Fan:bay 2, fan 3', 'metric': 'hardware-element-status', 'value': 1},
                            {'name': 'Chassis 1:Fan:bay 3, fan 1', 'metric': 'hardware-element-status', 'value': 1},
                            {'name': 'Chassis 1:Fan:bay 3, fan 2', 'metric': 'hardware-element-status', 'value': 1},
                            {'name': 'Chassis 1:Fan:bay 3, fan 3', 'metric': 'hardware-element-status', 'value': 1},
                            {'name': 'Chassis 1:PowerConsumption', 'metric': 'hardware-element-status', 'value': 1},
                            {'name': 'Chassis 1:PowerUnit(AC):bay 1', 'metric': 'hardware-element-status', 'value': 0},
                            {'name': 'Chassis 1:PowerUnit(AC):bay 2', 'metric': 'hardware-element-status', 'value': 1},
                            {'name': 'Chassis 1:PowerUnit(AC):bay 3', 'metric': 'hardware-element-status', 'value': 1},
                            {'name': 'Chassis 1:PowerUnit(AC):bay 4', 'metric': 'hardware-element-status', 'value': 1},
                            {'name': 'Chassis 1:PowerUnit(AC):bay 5', 'metric': 'hardware-element-status', 'value': 1},
                            {'name': 'Chassis 1:PowerUnit(AC):bay 6', 'metric': 'hardware-element-status', 'value': 1},
                            {'name': 'Chassis 1:PowerUnitFan:bay 1, fan 1', 'metric': 'hardware-element-status', 'value': 1},
                            {'name': 'Chassis 1:PowerUnitFan:bay 2, fan 1', 'metric': 'hardware-element-status', 'value': 0},
                            {'name': 'Chassis 1:PowerUnitFan:bay 3, fan 1', 'metric': 'hardware-element-status', 'value': 0},
                            {'name': 'Chassis 1:PowerUnitFan:bay 4, fan 1', 'metric': 'hardware-element-status', 'value': 0},
                            {'name': 'Chassis 1:PowerUnitFan:bay 5, fan 1', 'metric': 'hardware-element-status', 'value': 1},
                            {'name': 'Chassis 1:PowerUnitFan:bay 6, fan 1', 'metric': 'hardware-element-status', 'value': 1},
                            {'name': 'Chassis 1:SSM:bay 1', 'metric': 'hardware-element-status', 'value': 1},
                            {'name': 'Chassis 1:SSM:bay 2', 'metric': 'hardware-element-status', 'value': 1},
                            {'name': None, 'metric': 'fans-amount-required', 'value': 9},
                            {'name': None, 'metric': 'fans-amount-current', 'value': 9},
                            {'name': None, 'metric': 'power-unit-amount-required', 'value': 6},
                            {'name': None, 'metric': 'power-unit-amount-current', 'value': 5}]

        # Arrange
        result = self.parser.parse_file(self.current_dir + '/test_valid_input_missing_power_unit.input', {}, {})

        # Assert
        self.assertEqual(48, len(result))

        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i]['name'], result[i].tags.get('name'))
            self.assertEqual(expected_results[i]['metric'], result[i].name)
            self.assertEqual(expected_results[i]['value'], result[i].value)

    def test_valid_input_missing_fans(self):
        expected_results = [{'name': 'Chassis 1:CMM:bay 1', 'metric': 'hardware-element-status', 'value': 1},
                            {'name': 'Chassis 1:CMM:bay 2', 'metric': 'hardware-element-status', 'value': 1},
                            {'name': 'Chassis 1:CPUtemp:blade 1, CPU0', 'metric': 'hardware-element-status', 'value': 1},
                            {'name': 'Chassis 1:CPUtemp:blade 1, CPU0', 'metric': 'temperature-sensor-current', 'value': 38},
                            {'name': 'Chassis 1:CPUtemp:blade 1, CPU0', 'metric': 'temperature-sensor-max', 'value': 95},
                            {'name': 'Chassis 1:CPUtemp:blade 1, CPU1', 'metric': 'hardware-element-status', 'value': 1},
                            {'name': 'Chassis 1:CPUtemp:blade 1, CPU1', 'metric': 'temperature-sensor-current', 'value': 38},
                            {'name': 'Chassis 1:CPUtemp:blade 1, CPU1', 'metric': 'temperature-sensor-max', 'value': 95},
                            {'name': 'Chassis 1:CPUtemp:blade 2, CPU0', 'metric': 'hardware-element-status', 'value': 1},
                            {'name': 'Chassis 1:CPUtemp:blade 2, CPU0', 'metric': 'temperature-sensor-current', 'value': 36},
                            {'name': 'Chassis 1:CPUtemp:blade 2, CPU0', 'metric': 'temperature-sensor-max', 'value': 95},
                            {'name': 'Chassis 1:CPUtemp:blade 2, CPU1', 'metric': 'hardware-element-status', 'value': 1},
                            {'name': 'Chassis 1:CPUtemp:blade 2, CPU1', 'metric': 'temperature-sensor-current', 'value': 35},
                            {'name': 'Chassis 1:CPUtemp:blade 2, CPU1', 'metric': 'temperature-sensor-max', 'value': 95},
                            {'name': 'Chassis 1:CPUtemp:blade 3, CPU0', 'metric': 'hardware-element-status', 'value': 1},
                            {'name': 'Chassis 1:CPUtemp:blade 3, CPU0', 'metric': 'temperature-sensor-current', 'value': 36},
                            {'name': 'Chassis 1:CPUtemp:blade 3, CPU0', 'metric': 'temperature-sensor-max', 'value': 95},
                            {'name': 'Chassis 1:CPUtemp:blade 3, CPU1', 'metric': 'hardware-element-status', 'value': 1},
                            {'name': 'Chassis 1:CPUtemp:blade 3, CPU1', 'metric': 'temperature-sensor-current', 'value': 36},
                            {'name': 'Chassis 1:CPUtemp:blade 3, CPU1', 'metric': 'temperature-sensor-max', 'value': 95},
                            {'name': 'Chassis 1:Fan:bay 1, fan 1', 'metric': 'hardware-element-status', 'value': 1},
                            {'name': 'Chassis 1:Fan:bay 1, fan 2', 'metric': 'hardware-element-status', 'value': 1},
                            {'name': 'Chassis 1:Fan:bay 1, fan 3', 'metric': 'hardware-element-status', 'value': 1},
                            {'name': 'Chassis 1:Fan:bay 2, fan 1', 'metric': 'hardware-element-status', 'value': 1},
                            {'name': 'Chassis 1:Fan:bay 2, fan 2', 'metric': 'hardware-element-status', 'value': 0},
                            {'name': 'Chassis 1:Fan:bay 2, fan 3', 'metric': 'hardware-element-status', 'value': 0},
                            {'name': 'Chassis 1:Fan:bay 3, fan 1', 'metric': 'hardware-element-status', 'value': 0},
                            {'name': 'Chassis 1:Fan:bay 3, fan 2', 'metric': 'hardware-element-status', 'value': 0},
                            {'name': 'Chassis 1:Fan:bay 3, fan 3', 'metric': 'hardware-element-status', 'value': 0},
                            {'name': 'Chassis 1:PowerConsumption', 'metric': 'hardware-element-status', 'value': 1},
                            {'name': 'Chassis 1:PowerUnit(AC):bay 1', 'metric': 'hardware-element-status', 'value': 1},
                            {'name': 'Chassis 1:PowerUnit(AC):bay 2', 'metric': 'hardware-element-status', 'value': 1},
                            {'name': 'Chassis 1:PowerUnit(AC):bay 3', 'metric': 'hardware-element-status', 'value': 1},
                            {'name': 'Chassis 1:PowerUnit(AC):bay 4', 'metric': 'hardware-element-status', 'value': 1},
                            {'name': 'Chassis 1:PowerUnit(AC):bay 5', 'metric': 'hardware-element-status', 'value': 1},
                            {'name': 'Chassis 1:PowerUnit(AC):bay 6', 'metric': 'hardware-element-status', 'value': 1},
                            {'name': 'Chassis 1:PowerUnitFan:bay 1, fan 1', 'metric': 'hardware-element-status', 'value': 1},
                            {'name': 'Chassis 1:PowerUnitFan:bay 2, fan 1', 'metric': 'hardware-element-status', 'value': 1},
                            {'name': 'Chassis 1:PowerUnitFan:bay 3, fan 1', 'metric': 'hardware-element-status', 'value': 1},
                            {'name': 'Chassis 1:PowerUnitFan:bay 4, fan 1', 'metric': 'hardware-element-status', 'value': 1},
                            {'name': 'Chassis 1:PowerUnitFan:bay 5, fan 1', 'metric': 'hardware-element-status', 'value': 1},
                            {'name': 'Chassis 1:PowerUnitFan:bay 6, fan 1', 'metric': 'hardware-element-status', 'value': 1},
                            {'name': 'Chassis 1:SSM:bay 1', 'metric': 'hardware-element-status', 'value': 1},
                            {'name': 'Chassis 1:SSM:bay 2', 'metric': 'hardware-element-status', 'value': 1},
                            {'name': None, 'metric': 'fans-amount-required', 'value': 9},
                            {'name': None, 'metric': 'fans-amount-current', 'value': 5},
                            {'name': None, 'metric': 'power-unit-amount-required', 'value': 6},
                            {'name': None, 'metric': 'power-unit-amount-current', 'value': 6}]

        # Arrange
        result = self.parser.parse_file(self.current_dir + '/test_valid_input_missing_fans.input', {}, {})

        # Assert
        self.assertEqual(48, len(result))

        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i]['name'], result[i].tags.get('name'))
            self.assertEqual(expected_results[i]['metric'], result[i].name)
            self.assertEqual(expected_results[i]['value'], result[i].value)


    def test_empty_input(self):
        expected_results = []

        # Arrange
        result = self.parser.parse_file(self.current_dir + '/empty.input', {}, {})

        # Assert
        self.assertEqual(0, len(result))


    def test_partial_data(self):
        expected_results = [{'name': None, 'metric': 'fans-amount-required', 'value': 9},
                            {'name': None, 'metric': 'fans-amount-current', 'value': 9},
                            {'name': None, 'metric': 'power-unit-amount-required', 'value': 6},
                            {'name': None, 'metric': 'power-unit-amount-current', 'value': 6}]

        # Arrange
        result = self.parser.parse_file(self.current_dir + '/test_partial_data.input', {}, {})

        # Assert
        self.assertEqual(4, len(result))

        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i]['name'], result[i].tags.get('name'))
            self.assertEqual(expected_results[i]['metric'], result[i].name)
            self.assertEqual(expected_results[i]['value'], result[i].value)


if __name__ == '__main__':
    unittest.main()
