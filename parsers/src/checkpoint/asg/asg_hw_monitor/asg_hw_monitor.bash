#!/bin/bash

local_id="$(asg stat -i local_id | head -c 1)"
chassis_required_list="$(asg stat | grep -A 15 'Chassis Parameters' | grep -v 'Chassis Parameters')"

if [ "$local_id" == 1 ]; then echo "$chassis_required_list" | awk -F '|' '{print $2 $3}' ; elif [ "$local_id" == 2 ]; then echo "$chassis_required_list" | awk -F '|' '{print $2 $4}'; elif [ "$local_id" == 3 ]; then echo "$chassis_required_list" | awk -F '|' '{print $2 $5}' ; elif [ "$local_id" == 4 ]; then echo "$chassis_required_list" | awk -F '|' '{print $2 $6}' ; fi ; /bin/nice -n 15 asg hw_monitor