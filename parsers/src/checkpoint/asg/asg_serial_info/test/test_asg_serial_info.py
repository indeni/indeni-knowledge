import os
import unittest

from checkpoint.asg.asg_serial_info.asg_serial_info import AsgSerialInfo
from parser_service.public.action import *

expected_result_single_chassis = [{'metric': 'serial-numbers',
    'tags': {'live-config': 'true', 'display-name': 'Serial Numbers'},
    'value': [{'name': 'Chassis serial', 'serialnumber': '1876950/005'},
        {'name': 'CMM1', 'serialnumber': '1424623/005'},
        {'name': 'CMM2', 'serialnumber': '1234792/003'},
        {'name': 'SSM1', 'serialnumber': '0334740489'},
        {'name': 'SSM2', 'serialnumber': '0393650001'},
        {'name': 'SGM1', 'serialnumber': 'LKA0048456'},
        {'name': 'SGM2', 'serialnumber': 'LKA0034552'},
        {'name': 'SGM3', 'serialnumber': 'LKA0034960'},
        {'name': 'SGM4', 'serialnumber': 'LKA03856149'},
        {'name': 'SGM5', 'serialnumber': 'LKA0301374'},
        {'name': 'SGM6', 'serialnumber': 'LKP4860145'},
        {'name': 'SGM7', 'serialnumber': 'LKA5890900'}]}]

expected_result_dual_chassis = [{'metric': 'serial-numbers',
    'tags': {'live-config': 'true', 'display-name': 'Serial Numbers'},
    'value': [{'name': 'Chassis serial', 'serialnumber': '1420444/002'},
        {'name': 'CMM1', 'serialnumber': '1455536/005'},
        {'name': 'CMM2', 'serialnumber': '1164445/019'},
        {'name': 'SSM1', 'serialnumber': '0315540476'},
        {'name': 'SSM2', 'serialnumber': '0314388498'},
        {'name': 'SGM1', 'serialnumber': 'LKA0504817'},
        {'name': 'SGM2', 'serialnumber': 'LKA0035132'},
        {'name': 'SGM3', 'serialnumber': 'LKA0046096'},
        {'name': 'SGM4', 'serialnumber': 'LKA0068086'},
        {'name': 'SGM5', 'serialnumber': 'LKA6301392'},
        {'name': 'SGM6', 'serialnumber': 'LKP0600143'},
        {'name': 'SGM7', 'serialnumber': 'LKA2697930'}]}]

expected_result_maestro_single_chassis = [{'metric': 'serial-numbers',
    'tags': {'live-config': 'true', 'display-name': 'Serial Numbers'},
    'value': [{'name': 'SGM2', 'serialnumber': '11xxxxxxxx'},
        {'name': 'SGM3', 'serialnumber': '12xxxxxxxx'},
        {'name': 'SGM4', 'serialnumber': '13xxxxxxxx'}]}]

class TestAsgSerialInfo(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = AsgSerialInfo()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_single_chassis(self):
        result = self.parser.parse_file(self.current_dir + '/single_chassis.input', {}, {})
        self.assertEqual(1, len(result))

        for i in range(len(expected_result_single_chassis)):
            self.assertEqual(expected_result_single_chassis[i]['tags'], result[i].tags)
            self.assertEqual(expected_result_single_chassis[i]['metric'], result[i].name)
            self.assertEqual(expected_result_single_chassis[i]['value'], result[i].value)

    def test_dual_chassis(self):
        result = self.parser.parse_file(self.current_dir + '/dual_chassis.input', {}, {})
        self.assertEqual(1, len(result))

        for i in range(len(expected_result_dual_chassis)):
            self.assertEqual(expected_result_dual_chassis[i]['tags'], result[i].tags)
            self.assertEqual(expected_result_dual_chassis[i]['metric'], result[i].name)
            self.assertEqual(expected_result_dual_chassis[i]['value'], result[i].value)

    def test_maestro_single_chassis(self):
        result = self.parser.parse_file(self.current_dir + '/maestro_single_chassis.input', {}, {})
        self.assertEqual(1, len(result))

        for i in range(len(expected_result_maestro_single_chassis)):
            self.assertEqual(expected_result_maestro_single_chassis[i]['tags'], result[i].tags)
            self.assertEqual(expected_result_maestro_single_chassis[i]['metric'], result[i].name)
            self.assertEqual(expected_result_maestro_single_chassis[i]['value'], result[i].value)

if __name__ == '__main__':
    unittest.main()