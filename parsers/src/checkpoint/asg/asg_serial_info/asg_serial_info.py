from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class AsgSerialInfo(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            parsed_data = helper_methods.parse_data_as_list(raw_data, 'asg_serial_info.textfsm')
            if parsed_data:
                serial_numbers_list = []
                for entry in parsed_data :
                    if 'Chassis ID' not in entry['component_name']:
                        new_serial_number = {}
                        new_serial_number['name'] = entry['component_name'].strip()
                        new_serial_number['serialnumber'] = entry['{}{}'.format('component_serial_ch', entry['chassis_id'])].strip()
                        serial_numbers_list.append(new_serial_number)
                if len(serial_numbers_list) > 0:
                    self.write_complex_metric_object_array('serial-numbers', {}, serial_numbers_list, True, 'Serial Numbers' )
        return self.output