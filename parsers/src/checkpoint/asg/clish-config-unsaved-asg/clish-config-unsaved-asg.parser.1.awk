# The following two sections has been added by request of Dan Shouky
# https://indeni.atlassian.net/browse/IKP-1221

# Unfortunately, the following code is duplicated in many .ind scripts.
# If you change something in the following two sections, please find all
# of the other instances of this code and make the change there also.

BEGIN{
    state = ""
}

#Could not acquire the config lock
/Could not acquire the config lock/ {
    if (NR == 1) {
        next
    }
}

#CLINFR0829  Unable to get user permissions
#CLINFR0819  User: johndoe denied access via CLI
#CLINFR0599  Failed to build ACLs
/(CLINFR0829\s+Unable to get user permissions|CLINFR0819\s+User: .+ denied access via CLI|CLINFR0599\s+Failed to build ACLs)/ {
    exit
}

#1_01:
/^[0-9]_[0-9][0-9]:/ {
    split($0, splitArr, "_")
    chassis = splitArr[1]
    blade = splitArr[2]
    gsub(/:/, "", blade)

    tags["name"] = "chassis: " chassis " blade: " blade
}

#saved
#unsaved
/^(saved|unsaved)$/ {
    if ( $1 == "unsaved" ) {
        state = 1
    } else if ($1 == "saved") {
        state = 0
    }
    if (state != "") {
        writeDoubleMetric("config-unsaved", tags, "gauge", state, "true", "Configuration Unsaved?", "boolean", "")  # Converted to new syntax by change_ind_scripts.py script
    }
}