BEGIN {
    ip_regex = "^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$"
    cidr_regex = "^[0-9][0-9]?$"
}

#>> Found inconsistency between routes in DB & OS
/Found inconsistency between routes/ {
    inconsistencySection = 1
}

#C         192.0.2.0/24        is directly connected, Sync
/^C .*directly connected/ {
    # We added more extensive validation here because of IKP-2917. We couldn't repro the bug, and just decided to
    # be especially cautious.
    split($2, subnetSplitArr, "/")
    destination = trim(subnetSplitArr[1])
    if (destination !~ ip_regex)
        next  # TODO: raise exception/log error
        
    subnetPrefix = trim(subnetSplitArr[2])
    if (subnetPrefix !~ cidr_regex)
        next  # TODO: raise exception/log error
    
    if (! (destination in directRouteLookup) ) {
        directRouteLookup[destination]
        iDirectRoute++
        directRoutes[iDirectRoute, "network"] = destination
        directRoutes[iDirectRoute, "mask"] = subnetPrefix
    }
}

#S         1.0.0.0/24          via 192.168.197.254, eth1-Mgmt4
#SBR   1.1.1.0/24 via 192.168.197.252 dev eth1-Mgmt4  table 1  proto gated
/^S .*via|^SBR /  {
    # We added more extensive validation here because of IKP-2917. We couldn't repro the bug, and just decided to
    # be especially cautious.
    split($2, subnetSplitArr, "/")
    destination = trim(subnetSplitArr[1])
    if (destination !~ ip_regex)
        next  # TODO: raise exception/log error
        
    subnetPrefix = trim(subnetSplitArr[2])
    if (subnetPrefix !~ cidr_regex)
        next  # TODO: raise exception/log error
    
    nextHop = trim($4)
    gsub(/,/, "" , nextHop)
    if (nextHop !~ ip_regex)
        next  # TODO: raise exception/log error
    
    if (inconsistencySection != 1) {
        iStaticRoute++
        routes[iStaticRoute, "network"] = destination
        routes[iStaticRoute, "mask"] = subnetPrefix
        routes[iStaticRoute, "next-hop"] = nextHop
    } else if (inconsistencySection == 1) {
        iMissingRoutes++
        missingRoutes[iMissingRoutes, "network"] = destination
        missingRoutes[iMissingRoutes, "mask"] = subnetPrefix
        missingRoutes[iMissingRoutes, "next-hop"] = nextHop
    }
}

END {
    writeComplexMetricObjectArray("routes-missing-kernel", null, missingRoutes, "false")
    writeComplexMetricObjectArray("connected-networks-table", null, directRoutes, "true", "Directly Connected Networks")
    writeComplexMetricObjectArray("static-routing-table", null, routes, "false")
}