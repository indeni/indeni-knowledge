############
# Why: Compare todays date against the time when the current device will be out of support, to alert before that happens.
# How: Get EOS data from check point webpage, and compare with todays date.
# Caveats: EOS list needs to be updated manually.
###########

BEGIN {
    # hardware and software EOS updated 26/07/2019
    # Define all hardware and software end of support dates
    # Based on https://www.checkpoint.com/support-services/support-life-cycle-policy/#softwaresupport
    # Based on https://www.checkpoint.com/support-services/support-life-cycle-policy/#appliancessupport

    ## Hardware

    # hardware[MODEL] = "DAY:MONTH:YEAR"

    # Model is only the model number, example: UTM-1 570 = 570
    # Month/day is in number with 2 digits
    # examples:
    #   hardware["570"] = "01:04:2017"

    # Scalable Platforms
    hardware["41000"] = "01:12:2022"
    hardware["44000"] = "01:01:2029"
    hardware["61000"] = "01:12:2022"
    hardware["64000"] = "01:01:2029"
    hardware["170"] = "01:07:2026"
    ## Software

    # software[VERSION] = "MONTH:YEAR:HW"

    # Version is full version, example: R77.30
    # Month is in number with 2 digits
    # HW is only number model, example: 2200. Or generic if the HW doesnt matter
    # If more than one date/HW per version, separate with ","
    # the generic type needs to be at the end of the line
    # examples:
    #   software["R70.20"] = "03:2013:generic"
    #   software["R75.20"] = "05:2017:600,05:2017:1100,08:2015:generic"

    # R75
    software["R75"] = "01:2015:generic"
    software["R75.10"] = "01:2015:generic"
    software["R75.20"] = "05:2017:600,05:2017:1100,08:2015:generic"
    software["R75.30"] = "08:2015:generic"
    software["R75.40VS"] = "07:2016:generic"
    software["R75.40"] = "04:2016:generic"
    software["R75.45"] = "04:2016:generic"
    software["R75.46"] = "04:2016:generic"
    software["R75.47"] = "04:2016:generic"

    software["R75SP"] = "04:2017:generic"
    software["R75SP.035"] = "04:2017:generic"
    software["R75SP.050"] = "04:2017:generic"
    software["R75SP.051"] = "04:2017:generic"
    software["R75SP.052"] = "04:2017:generic"
    software["R75SP.40VS"] = "04:2017:generic"
    # R76

    software["R76"] = "02:2017:generic"
    software["R76SP"] = "03:2018:generic"
    software["R76SP.10"] = "03:2018:generic"
    software["R76SP.20"] = "03:2018:generic"
    software["R76SP.30"] = "03:2018:generic"
    software["R76SP.40"] = "03:2018:generic"
    software["R76SP.50"] = "04:2021:generic"

    # R80
    software["R80.20SP"] = "02:2023:generic"
    software["R80.30SP"] = "02:2023:generic"
    software["R81.10"] = "07:2025:generic"
    software["R81.20"] = "11:2026:generic"
}

# The following is used to determine accuracy of the version being used.
# Unfortunately, the Scalable platforms models (41000/61000/44000/64000) do not accurately provide OS version for the output "cpstat os"
# As a result, we run the clish command "show version all". This is supported on R70 and on R80 currently.
# cpstat is maintained to collect hardware info.

# Initial version Check Point Gaia R77.30
/^(Check Point |Multi-Domain )/ {
    version = $NF
}


##########
# Model: Check Point 2200
/Appliance Name:/ {
    model = $NF
    # Remove any letters, for example the R in "1200R" appliance
    gsub(/[A-Z]/, "", model)
}


END {

    # Calculate and write software EOS date
    split(software[version], versionDataFullArr, ",")

    for (i in versionDataFullArr) {
        split(versionDataFullArr[i], versionDataArr, ":")

        # need logic to compare for ex model 1100 to 1122
        # To do this we will count the characters in the hard coded support string (e.g., 1100 --> 4), and then strip
        # the zeros to get the "major version": 1100 --> 11. Then, we try to match the 11 (from the original support
        # string) with the found version; e.g., 11 matches 1122. So, we know that 1122 is a 'model 11': it's valid.
        # Finally, make sure that the char count of the original, unmodified support string (1100 --> 4) is equal to the
        # char count of the found model (1122 --> 4, so it's valid). This prevents, e.g., 11000 (5 chars) from being
        # a valid model in this case.
        modelCompare = versionDataArr[3]

        # Remove any letters, for example the R in "1200R" appliance
        gsub(/[A-Z]/, "", modelCompare)

        # Count chars of the model in support info
        modelCompareChars = length(modelCompare)

        # Count chars of the model on the device checked
        modelChars = length(model)

        # Remove zeros from the model name in support info
        gsub(/00/, "", modelCompare)

        if (  model ~ modelCompare &&  modelCompareChars == modelChars ) {
            if ( done != 1) {
                endSoftwareSupportMonth = versionDataArr[1]
                endSoftwareSupportYear = versionDataArr[2]
                writeDoubleMetric("software-eos-date", null, "gauge", date(endSoftwareSupportYear, endSoftwareSupportMonth, 1), "true", "Software End of Support", "date", "")  # Converted to new syntax by change_ind_scripts.py script
                done = 1
            }
        } else if ( versionDataArr[3] == "generic" ) {
            if ( done != 1) {
                endSoftwareSupportMonth = versionDataArr[1]
                endSoftwareSupportYear = versionDataArr[2]
                writeDoubleMetric("software-eos-date", null, "gauge", date(endSoftwareSupportYear, endSoftwareSupportMonth, 1), "true", "Software End of Support", "date", "")  # Converted to new syntax by change_ind_scripts.py script
                # note: "generic" should always be last in the loop, so don't need to set done = 1
            }
        }
    }

    # TODO: when we get error logging, if 'done == 0' log a warning here

    # Calculate and write HW EOS date
    split(hardware[model], modelDataArr, ":")
    endHardwareSupportDay = modelDataArr[1]
    endHardwareSupportMonth = modelDataArr[2]
    endHardwareSupportYear = modelDataArr[3]

    # Check that we actually got some data to write
    if ( endHardwareSupportDay ) {
        writeDoubleMetric("hardware-eos-date", null, "gauge", date(endHardwareSupportYear, endHardwareSupportMonth, endHardwareSupportDay), "true", "Hardware End of Support", "date", "")  # Converted to new syntax by change_ind_scripts.py script
    }  # TODO: when we get error logging, add 'else' and log a warning here

}