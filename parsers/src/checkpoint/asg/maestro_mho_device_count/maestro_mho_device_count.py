from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class MaestroSecurityGroupDeviceCountParser(BaseParser):

    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):

        # Step 1 : Data Extraction
        data = helper_methods.parse_data_as_json(raw_data)

        # Step 2 : Data Processing
        member_count = 0
        for sg in data['security_groups']:
            chassis_count = 0
            for chassis in data['security_groups'][sg]['chassis']:
                    chassis_count += 1

            if chassis_count > member_count:
                member_count = chassis_count
        # Step 3 : Data Reporting
        self.write_tag("device-count", str(member_count))

        return self.output