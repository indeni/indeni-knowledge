from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class AsgNtpUpdateTime(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            ntpServerTags = {}
            if 'offset' in raw_data:
                parsed_data = helper_methods.parse_data_as_list(raw_data, 'asg_ntp_update_time.textfsm')
                if parsed_data:
                    if parsed_data[0]['ntpdate']:
                        split_data = parsed_data[0]['ntpdate'].split(' ')
                        ntpServerTags['name'] = split_data[7]
                        self.write_double_metric('ntp-server-state', ntpServerTags, 'gauge', 1, False)
                    else:
                        self.write_double_metric('ntp-server-state', ntpServerTags, 'gauge', 0, False)
            else:
                self.write_double_metric('ntp-server-state', ntpServerTags, 'gauge', 0, False)
        return self.output