import os
import unittest
from checkpoint.asg.asg_ntp_update_time.asg_ntp_update_time import AsgNtpUpdateTime
from parser_service.public.action import *

class TestAsgNtpUpdateTime(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = AsgNtpUpdateTime()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_asg_ntp_update_time_dns_failure(self):
        result = self.parser.parse_file(self.current_dir + '/asg_ntp_update_time_dns_failure.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'ntp-server-state')
        self.assertEqual(result[0].value, 0)

    def test_asg_ntp_update_time_server_failure(self):
        result = self.parser.parse_file(self.current_dir + '/asg_ntp_update_time_server_failure.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'ntp-server-state')
        self.assertEqual(result[0].value, 0)

    def test_asg_ntp_update_time_adjust(self):
        result = self.parser.parse_file(self.current_dir + '/asg_ntp_update_time_adjust.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'ntp-server-state')
        self.assertEqual(result[0].tags['name'], '209.87.222.142')
        self.assertEqual(result[0].value, 1)
        
    def test_asg_ntp_update_time_step(self):
        result = self.parser.parse_file(self.current_dir + '/asg_ntp_update_time_step.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'ntp-server-state')
        self.assertEqual(result[0].tags['name'], '209.87.222.142')
        self.assertEqual(result[0].value, 1)

    def test_ntpdate_multiple_servers(self):
        result = self.parser.parse_file(self.current_dir + '/ntpdate_multiple_servers.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'ntp-server-state')
        self.assertEqual(result[0].tags['name'], '198.60.22.240')
        self.assertEqual(result[0].value, 1)

if __name__ == '__main__':
    unittest.main()