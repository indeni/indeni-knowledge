import os
import unittest

from checkpoint.asg.asg_route_dyn_ospf_novsx.asg_route_dyn_ospf_novsx import AsgRouteDynOspfNoVsx
from parser_service.public.action import WriteDoubleMetric


class TestAsgRouteDynOspfNoVsx(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = AsgRouteDynOspfNoVsx()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_R76SP50_3_neighbors_full(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/R76SP.50_3_neighbors_full.input', {}, {})

        # Assert
        self.assertEqual(7, len(result))

        self.assertTrue(isinstance(result[0], WriteDoubleMetric))
        self.assertEqual(result[0].name, 'ospf-state')
        self.assertEqual(result[0].tags['name'], '10.11.94.41 priority: 1 address: 10.11.94.46')
        self.assertEqual(result[0].tags['chassis.id'], '1')
        self.assertEqual(result[0].tags['blade.id'], '01')
        self.assertEqual(result[0].value, 1)

        self.assertTrue(isinstance(result[1], WriteDoubleMetric))
        self.assertEqual(result[1].name, 'ospf-state')
        self.assertEqual(result[1].tags['name'], '10.11.94.10 priority: 1 address: 10.11.94.48')
        self.assertEqual(result[1].tags['chassis.id'], '1')
        self.assertEqual(result[1].tags['blade.id'], '01')
        self.assertEqual(result[1].value, 1)

        self.assertTrue(isinstance(result[2], WriteDoubleMetric))
        self.assertEqual(result[2].name, 'ospf-state')
        self.assertEqual(result[2].tags['name'], '10.11.94.60 priority: 1 address: 10.11.94.60')
        self.assertEqual(result[2].tags['chassis.id'], '1')
        self.assertEqual(result[2].tags['blade.id'], '01')
        self.assertEqual(result[2].value, 0)

        self.assertTrue(isinstance(result[3], WriteDoubleMetric))
        self.assertEqual(result[3].name, 'ospf-state')
        self.assertEqual(result[3].tags['name'], '10.11.94.73 priority: 1 address: 10.11.94.78')
        self.assertEqual(result[3].tags['chassis.id'], '1')
        self.assertEqual(result[3].tags['blade.id'], '01')
        self.assertEqual(result[3].value, 1)

        self.assertTrue(isinstance(result[4], WriteDoubleMetric))
        self.assertEqual(result[4].name, 'ospf-state')
        self.assertEqual(result[4].tags['name'], '10.11.94.86 priority: 1 address: 10.11.94.86')
        self.assertEqual(result[4].tags['chassis.id'], '1')
        self.assertEqual(result[4].tags['blade.id'], '01')
        self.assertEqual(result[4].value, 0)

        self.assertTrue(isinstance(result[5], WriteDoubleMetric))
        self.assertEqual(result[5].name, 'ospf-state')
        self.assertEqual(result[5].tags['name'], '10.11.94.90 priority: 1 address: 10.11.94.90')
        self.assertEqual(result[5].tags['chassis.id'], '1')
        self.assertEqual(result[5].tags['blade.id'], '01')
        self.assertEqual(result[5].value, 0)

        self.assertTrue(isinstance(result[6], WriteDoubleMetric))
        self.assertEqual(result[6].name, 'ospf-state')
        self.assertEqual(result[6].tags['name'], '10.11.94.112 priority: 1 address: 10.11.94.112')
        self.assertEqual(result[6].tags['chassis.id'], '1')
        self.assertEqual(result[6].tags['blade.id'], '01')
        self.assertEqual(result[6].value, 0)

    def test_R8020SP_3_neighbors_full(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/R80.20SP_3_neighbors_full.input', {}, {})

        # Assert
        self.assertEqual(7, len(result))

        self.assertTrue(isinstance(result[0], WriteDoubleMetric))
        self.assertEqual(result[0].name, 'ospf-state')
        self.assertEqual(result[0].tags['name'], '10.11.94.41 priority: 1 address: 10.11.94.46')
        self.assertEqual(result[0].tags['chassis.id'], '1')
        self.assertEqual(result[0].tags['blade.id'], '01')
        self.assertEqual(result[0].value, 1)

        self.assertTrue(isinstance(result[1], WriteDoubleMetric))
        self.assertEqual(result[1].name, 'ospf-state')
        self.assertEqual(result[1].tags['name'], '10.11.94.10 priority: 1 address: 10.11.94.48')
        self.assertEqual(result[1].tags['chassis.id'], '1')
        self.assertEqual(result[1].tags['blade.id'], '01')
        self.assertEqual(result[1].value, 1)

        self.assertTrue(isinstance(result[2], WriteDoubleMetric))
        self.assertEqual(result[2].name, 'ospf-state')
        self.assertEqual(result[2].tags['name'], '10.11.94.60 priority: 1 address: 10.11.94.60')
        self.assertEqual(result[2].tags['chassis.id'], '1')
        self.assertEqual(result[2].tags['blade.id'], '01')
        self.assertEqual(result[2].value, 0)

        self.assertTrue(isinstance(result[3], WriteDoubleMetric))
        self.assertEqual(result[3].name, 'ospf-state')
        self.assertEqual(result[3].tags['name'], '10.11.94.73 priority: 1 address: 10.11.94.78')
        self.assertEqual(result[3].tags['chassis.id'], '1')
        self.assertEqual(result[3].tags['blade.id'], '01')
        self.assertEqual(result[3].value, 1)

        self.assertTrue(isinstance(result[4], WriteDoubleMetric))
        self.assertEqual(result[4].name, 'ospf-state')
        self.assertEqual(result[4].tags['name'], '10.11.94.86 priority: 1 address: 10.11.94.86')
        self.assertEqual(result[4].tags['chassis.id'], '1')
        self.assertEqual(result[4].tags['blade.id'], '01')
        self.assertEqual(result[4].value, 0)

        self.assertTrue(isinstance(result[5], WriteDoubleMetric))
        self.assertEqual(result[5].name, 'ospf-state')
        self.assertEqual(result[5].tags['name'], '10.11.94.90 priority: 1 address: 10.11.94.90')
        self.assertEqual(result[5].tags['chassis.id'], '1')
        self.assertEqual(result[5].tags['blade.id'], '01')
        self.assertEqual(result[5].value, 0)

        self.assertTrue(isinstance(result[6], WriteDoubleMetric))
        self.assertEqual(result[6].name, 'ospf-state')
        self.assertEqual(result[6].tags['name'], '10.11.94.114 priority: 1 address: 10.11.94.114')
        self.assertEqual(result[6].tags['chassis.id'], '1')
        self.assertEqual(result[6].tags['blade.id'], '01')
        self.assertEqual(result[6].value, 0)


if __name__ == '__main__':
    unittest.main()