
from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class AsgRouteDynOspfNoVsx(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            parsed_data = helper_methods.parse_data_as_list(raw_data, 'asg_route_dyn_ospf.textfsm')
            if parsed_data:
                for neighbour in parsed_data:
                    tags = {}
                    tags["name"] = '{} priority: {} address: {}'.format(neighbour['neighbour'], neighbour['priority'], neighbour['address'])
                    tags["state"] = neighbour['state']
                    tags['chassis.id'] = neighbour['chassis']
                    tags['blade.id'] = neighbour['blade']
                    self.write_double_metric('ospf-state', tags, 'gauge', 1 if neighbour['state'] in ['FULL', '2WAY'] else 0, True,'OSPF Neighbors', 'state', 'chassis.id|blade.id|name')
        return self.output
