import os
import unittest

from checkpoint.asg.asg_drop_monitor_R7650SP.asg_drop_monitor_R7650SP import ChkpChassisDropMonitor
from parser_service.public.action import WriteDoubleMetric


class TestChkpChassisDropMonitor(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = ChkpChassisDropMonitor()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_valid_input_R7650SP(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/valid_input_R7650SP.json', {}, {})

        # Assert
        self.assertEqual(40, len(result))

        self.assertTrue(isinstance(result[0], WriteDoubleMetric))
        self.assertEqual(656, result[0].value)
        self.assertEqual('psl_stats_dropped_udp', result[0].tags['name'])
        self.assertEqual('counter', result[0].ds_type)

        self.assertTrue(isinstance(result[15], WriteDoubleMetric))
        self.assertEqual(656, result[15].value)
        self.assertEqual('ppak_stats_hl - new conn', result[15].tags['name'])
        self.assertEqual('counter', result[15].ds_type)

    def test_failed_to_get_json(self):
        expected_results = []

        # Act
        result = self.parser.parse_file(self.current_dir + '/failed_to_get_json.input', {}, {})

        # Assert
        self.assertEqual(0, len(result))

if __name__ == '__main__':
    unittest.main()