#1_1
/^[0-9]+\_+[0-9]/{
    split($0, chassis_arr, "_")
}

#|CPU \ Blade|2_3 |2_4 |
/CPU \\ Blade/ {
    split($0, tmpcolumns_arr, "\\|")

    # Cleaning the array from empty entries that occur due to the split
    i = 1
    for (id in tmpcolumns_arr) {
        if (tmpcolumns_arr[id] != "") {
            columns_arr[i] = tmpcolumns_arr[id]
            i++
        }
    }
}

#|cpu0       |29% |2%  |
/cpu[0-9]/ {
    cpu = $1
    gsub(/\|/, "", cpu)

    split($0, tmp_usage_arr, "\\|")

    # Cleaning the array from empty entries that occur due to the split
    i = 1
    for (id in tmp_usage_arr) {
        if (tmp_usage_arr[id] != "") {
            usage_arr[i] = tmp_usage_arr[id]
            i++
        }
    }

    cpu_tags["cpu-is-avg"] = "false"

    i = 2
    while (i <= arraylen(columns_arr)) {
        if (usage_arr[i] !~ /NA/) {
            split(columns_arr[i], bladeArr, "_")
            chassis = bladeArr[1]
            blade = bladeArr[2]
            usage = usage_arr[i]
            gsub(/%/, "", usage)
            if (chassis == chassis_arr[1]) {
                cpu_tags["cpu-id"] = cpu " - chassis: " chassis " blade: " blade
                cpu_tags["blade"] = blade
                writeDoubleMetric("cpu-usage", cpu_tags, "gauge", usage, "true", "CPU", "percentage", "cpu-id")  # Converted to new syntax by change_ind_scripts.py script}
            }
        }
        i++
    }
}