from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class OrchStatDPortAllJ(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data = helper_methods.parse_data_as_list(raw_data, 'orch_stat_D_port_all_j.textfsm')
            if data:
                for parsed_data in data:
                    if parsed_data['name'] != '---':
                        tags = {}
                        tags['name'] = parsed_data['name']
                        tags['type'] = 'port-discard-counter'
                        port_discard_counters = int(parsed_data['port_total'])
                        self.write_double_metric('packet-discard-counter', tags, 'gauge', port_discard_counters, True, 'Network Interfaces - Discard counter - Port discard', 'number', 'name')
                        tags['type'] = 'ingress-tag-frame'
                        self.write_double_metric('packet-discard-counter', tags, 'gauge', int(parsed_data['ingress_tag_frame']), True, 'Network Interfaces - Discard counter - Ingress tag frame', 'number', 'name')
                        tags['type'] = 'egress-general'
                        self.write_double_metric('packet-discard-counter', tags, 'gauge', int(parsed_data['egress_general']), True, 'Network Interfaces - Discard counter - Egress general', 'number', 'name')
                        tags['type'] = 'ingress-vlan-memberships'
                        self.write_double_metric('packet-discard-counter', tags, 'gauge', int(parsed_data['ingress_vlan_members']), True, 'Network Interfaces - Discard counter - Ingress vlan membership', 'number', 'name')
                        tags['type'] = 'ingress-general'
                        self.write_double_metric('packet-discard-counter', tags, 'gauge', int(parsed_data['ingress_general']), True, 'Network Interfaces - Discard counter - Ingress general', 'number', 'name')
                        tags['type'] = 'egress-policy-engine'
                        self.write_double_metric('packet-discard-counter', tags, 'gauge', int(parsed_data['egress_policy_engine']), True, 'Network Interfaces - Discard counter - Egress policy engine', 'number', 'name')
                        tags['type'] = 'ingress-tx-link-down'
                        self.write_double_metric('packet-discard-counter', tags, 'gauge', int(parsed_data['ingress_tx_link_down']), True, 'Network Interfaces - Discard counter - Ingress tx link down', 'number', 'name')
                        tags['type'] = 'loopback-filter'
                        self.write_double_metric('packet-discard-counter', tags, 'gauge', int(parsed_data['loopback_filter']), True, 'Network Interfaces - Discard counter - Loopback filter', 'number', 'name')
                        tags['type'] = 'ingress-policy-engine'
                        self.write_double_metric('packet-discard-counter', tags, 'gauge', int(parsed_data['ingress_policy_engine']), True, 'Network Interfaces - Discard counter - Ingress policy engine', 'number', 'name')
                        tags['type'] = 'egress-stp-filter'
                        self.write_double_metric('packet-discard-counter', tags, 'gauge', int(parsed_data['egress_stp_filter']), True, 'Network Interfaces - Discard counter - Egress stp filter', 'number', 'name')
                        tags['type'] = 'egress-vlan-memberships'
                        self.write_double_metric('packet-discard-counter', tags, 'gauge', int(parsed_data['egress_vlan_members']), True, 'Network Interfaces - Discard counter - Egress vlan memberships', 'number', 'name')
                        tags['type'] = 'egress-sll'
                        self.write_double_metric('packet-discard-counter', tags, 'gauge', int(parsed_data['egress_sll']), True, 'Network Interfaces - Discard counter - Egress sll', 'number', 'name')
                        tags['type'] = 'port-isolation'
                        self.write_double_metric('packet-discard-counter', tags, 'gauge', int(parsed_data['port_isolation']), True, 'Network Interfaces - Discard counter - Port isolation', 'number', 'name')
                        tags['type'] = 'egress-hoq-stall'
                        self.write_double_metric('packet-discard-counter', tags, 'gauge', int(parsed_data['egress_hoq_stall']), True, 'Network Interfaces - Discard counter - Egress hoq stall', 'number', 'name')
                        tags['type'] = 'egress-hoq'
                        self.write_double_metric('packet-discard-counter', tags, 'gauge', int(parsed_data['egress_hoq']), True, 'Network Interfaces - Discard counter - Egress hoq', 'number', 'name')
                        tags['type'] = 'egress-link-down'
                        self.write_double_metric('packet-discard-counter', tags, 'gauge', int(parsed_data['egress_link_down']), True, 'Network Interfaces - Discard counter - Egress link down', 'number', 'name')
        return self.output