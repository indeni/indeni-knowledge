import os
import unittest
from checkpoint.asg.orch_stat_D_port_all_j.orch_stat_D_port_all_j import OrchStatDPortAllJ
from parser_service.public.action import *

class TestOrchStatTJ(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = OrchStatDPortAllJ()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_check_point_orch_stat_D_port_all_j(self):
        result = self.parser.parse_file(self.current_dir + '/orch_stat_D_port_all_j.input', {}, {})
        self.assertEqual(592, len(result))

if __name__ == '__main__':
    unittest.main()