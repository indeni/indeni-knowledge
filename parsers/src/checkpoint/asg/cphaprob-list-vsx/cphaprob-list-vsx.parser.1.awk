function addVsTags(tags) {
    tags["vs.id"] = vsid
    tags["vs.name"] = vsname
}

BEGIN {
    vsid = ""
    vsname = ""
    devicename = ""
}

# VSID:            0
/VSID:/ {
    vsid = trim($NF)
}

# Name:            VSX-CXL2-Gear
/^Name:/ {
    vsname = trim($NF)
}

# Table header
# | ID   Device Name       State   Process Status   Last Report Time   Timeout   |
/Device Name.*State/ {
    getColumns(trim($0), "[ ]{2,}", columns)
}

# --------------------------------------------------------------------------------
# | -    Recovery Delay    OK      -                0                  none      |
# --------------------------------------------------------------------------------
# | 0    Synchronization   OK      -                576543             none      |
# --------------------------------------------------------------------------------
# | 1    Filter            OK      -                576541             none      |
# --------------------------------------------------------------------------------
# | 2    VSX               OK      -                0.8                none      |
# --------------------------------------------------------------------------------
# | 3    VSX Config        OK      -                576637             none      |
# --------------------------------------------------------------------------------
/\| [0-9]+/ {
    state_desc = getColData(trim($0), columns, "State")
    pnotestatustag["name"] = getColData(trim($0), columns, "Device Name")
    addVsTags(pnotestatustag)
    # Note that the "if" below is the reverse of what cphaprob -l list ind script is doing for
    # regular CHKP devices (not 61k's)
    isup = 1
    if (state_desc != "OK") {
        isup = 0
    }
    writeDoubleMetric("clusterxl-pnote-state", pnotestatustag, "gauge", isup, "true", "ClusterXL Devices", "state", "name|vs.id|vs.name")  # Converted to new syntax by change_ind_scripts.py script
}