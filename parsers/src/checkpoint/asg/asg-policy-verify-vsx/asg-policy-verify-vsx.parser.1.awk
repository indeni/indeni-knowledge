BEGIN {
    FS = "|"
    second = "00"
    tags["vs.id"] = -1
    resetVars()
}

function resetVars() {
    chassis_and_blade = ""
    current_chassis_number = ""
    current_blade = ""
    chassis_name = ""

    policy_name = ""
    delete policy_array
    policy_date = ""

    date_whole = ""
    day = ""
    month = ""
    month_num = ""
    year = ""

    time_whole = ""
    hour = ""
    minute = ""

    policy_status = -1
}

# Dump the data for the PREVIOUS line.
function dumpData(this_chassis_number) { # Passing in current chassis number just to be clear on the purpose of this function.
    if (this_chassis_number != "") {
        if (policy_name == "")
            policy_status = 0
        else
            policy_status = 1

        policy_array[0, "policy-name"] = trim(policy_name)
        writeComplexMetricObjectArray("policy-name", tags, policy_array, "true", "Firewall Policy Name")

        writeDoubleMetric("policy-installed", tags, "gauge", policy_status, "true", "Firewall Policy", "state", "name|vs.id")

        if (policy_date != "") {
            writeDoubleMetric("policy-install-last-modified", tags, "gauge", datetime(year, month_num, day, hour, minute, second), "true", "Firewall Policy - Last Modified", "date", "name|vs.id")
        }

        resetVars()
    }
}

#1_1
/^[0-9]+\_[0-9]/ {
    split($0, chassis_arr, "_")
    local_chassis = chassis_arr[1]
}

#|0      |1_01   |CP-R80.20SP-VSX_Pol|06Aug19 20:00  |eaba3a991        |Success |
/^\|[0-9]/ {
    # Capture the VSID here, but don't set the tag yet. Need to be careful when we set the tag so that it matches
    # with the correct chassis-blade.
     vs_id = $2
}

#|0      |1_01   |CP-R80.20SP-VSX_Pol|06Aug19 20:00  |eaba3a991        |Success |
#|       |1_02   |CP-R80.20S-FW_Polic|13Sep19 05:50  |ca4abd3be        |Success |
#|       |2_01   |CP-R80.20S-FW_Polic|13Sep19 05:50  |ca4abd3be        |Fail    |
#|5      |2_01   |CP-R80.20S-FW_Polic|13Sep19 05:50  |ca4abd3be        |Anything|
/\|[0-9]_[0-9]/ {
    # Hitting a line like this triggers dumping the data for the PREVIOUS line. This is to handle multi-line
    # policy names.
    if ($0 !~ /Success/) {
        dumpData(current_chassis_number)
    } else {
        # Write previously collected data ONLY if we have some. See checks in dumpData. We're doing it this way so that we
        # can handle long policy names which span multiple lines. See unit tests.
        dumpData(current_chassis_number)

        chassis_and_blade = $3
        split(chassis_and_blade, chassis_blade_arr, "_")
        current_chassis_number = trim(chassis_blade_arr[1])

        if (local_chassis != current_chassis_number) {
            chassis_and_blade = ""
            current_chassis_number = ""   # MUST reset these here. dumpData() depends on this being 'correctly empty'.
        } else {
            # This line is critical. Only set the VSID tag when we are actually collecting data. This makes sure that
            # we don't set the tag _before_ we've written the previous line's data, and it makes sure we have the correct
            # VSID when we eventually write _this_ data.
            tags["vs.id"] = vs_id

            current_blade = trim(chassis_blade_arr[2])
            tags["name"] = "Chassis " current_chassis_number " Blade " current_blade

            policy_name = trim($4)
            policy_date = trim($5)
            split(policy_date, date_arr, " ")

            date_whole =  date_arr[1]
            day = substr(date_whole, 1, 2)
            month = substr(date_whole, 3, 3)
            month_num = parseMonthThreeLetter(month)
            year = "20"substr(date_whole, 6, 2)

            time_whole = date_arr[2]
            split(time_whole, time_arr, ":")
            hour = time_arr[1]
            minute = time_arr[2]
        }
    }
}

# The following block handles multi-line policy names. See unit tests.
#|       |       |yWithALotOf_Extra_W|               |                 |        |
$4 != "" && /^\|\s+\|\s/ {
    if (current_chassis_number != "") {
        policy_name = policy_name trim($4)
    }
}

END {
    dumpData(current_chassis_number)
}