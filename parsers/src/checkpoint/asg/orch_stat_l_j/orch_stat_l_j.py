from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class CheckpointOrchStatLJ(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data = helper_methods.parse_data_as_list(raw_data, 'orch_stat_l_j.textfsm')
            if data:
                for parsed_data in data:
                    tags = {}
                    tags['name'] = parsed_data['name']
                    if parsed_data['name'] != '---':
                        if parsed_data['admin_state'] == 'UP' and parsed_data['link_state'] == '':
                            parsed_data['link_state'] = 'UP'
                        self.write_double_metric('bond-admin-state', tags, 'gauge', 1 if parsed_data['admin_state'] == 'UP' else 0, True, 'Network Interfaces - LAG Admin State', 'state', 'name')
                        self.write_double_metric('bond-link-state', tags, 'gauge', 1 if parsed_data['link_state'] == 'UP' else 0, True, 'Network Interfaces - LAG Link State', 'state', 'name')
                        self.write_double_metric('bond-state', tags, 'gauge', 0 if parsed_data['admin_state'] == 'UP' and parsed_data['link_state'] == 'DOWN' else 1, False)
        return self.output