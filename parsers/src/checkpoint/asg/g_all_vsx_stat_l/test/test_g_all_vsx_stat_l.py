import os
import unittest

from checkpoint.asg.g_all_vsx_stat_l.g_all_vsx_stat_l import GAllVsxStatL
from parser_service.public.action import *


class TestGAllVsxStatL(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = GAllVsxStatL()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_1_blade_2_vs(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/1_blade_2_vs.input', {}, {})

        # Assert
        self.assertEqual(6, len(result))

        self.assertEqual('concurrent-connections', result[0].name)
        self.assertEqual(44, result[0].value)
        self.assertEqual('Blade:1_01 VSID:0', result[0].tags['vs.id'])
        self.assertEqual('R80.20_VSX', result[0].tags['vs.name'])

        self.assertEqual('concurrent-connections-limit', result[1].name)
        self.assertEqual(14900, result[1].value)
        self.assertEqual('Blade:1_01 VSID:0', result[1].tags['vs.id'])
        self.assertEqual('R80.20_VSX', result[1].tags['vs.name'])

        self.assertEqual('peak-concurrent-connections', result[2].name)
        self.assertEqual(62, result[2].value)
        self.assertEqual('Blade:1_01 VSID:0', result[2].tags['vs.id'])
        self.assertEqual('R80.20_VSX', result[2].tags['vs.name'])

        self.assertEqual('concurrent-connections', result[3].name)
        self.assertEqual(0, result[3].value)
        self.assertEqual('Blade:1_01 VSID:1', result[3].tags['vs.id'])
        self.assertEqual('VS_1', result[3].tags['vs.name'])

        self.assertEqual('concurrent-connections-limit', result[4].name)
        self.assertEqual(14900, result[4].value)
        self.assertEqual('Blade:1_01 VSID:1', result[4].tags['vs.id'])
        self.assertEqual('VS_1', result[4].tags['vs.name'])
        self.assertEqual('peak-concurrent-connections', result[5].name)

        self.assertEqual(0, result[5].value)
        self.assertEqual('Blade:1_01 VSID:1', result[5].tags['vs.id'])
        self.assertEqual('VS_1', result[5].tags['vs.name'])

    def test_unkown_values(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/test_unknown_values.input', {}, {})

        # Assert
        self.assertEqual(5, len(result))

        self.assertEqual('concurrent-connections', result[0].name)
        self.assertEqual(19, result[0].value)
        self.assertEqual('Blade:1_01 VSID:0', result[0].tags['vs.id'])
        self.assertEqual('R80.20_VSX', result[0].tags['vs.name'])

        self.assertEqual('concurrent-connections-limit', result[1].name)
        self.assertEqual(24900, result[1].value)
        self.assertEqual('Blade:1_01 VSID:0', result[1].tags['vs.id'])
        self.assertEqual('R80.20_VSX', result[1].tags['vs.name'])

        self.assertEqual('peak-concurrent-connections', result[2].name)
        self.assertEqual(65, result[2].value)
        self.assertEqual('Blade:1_01 VSID:0', result[2].tags['vs.id'])
        self.assertEqual('R80.20_VSX', result[2].tags['vs.name'])

        self.assertEqual('concurrent-connections-limit', result[3].name)
        self.assertEqual(24900, result[3].value)
        self.assertEqual('Blade:1_01 VSID:1', result[3].tags['vs.id'])
        self.assertEqual('VS_1', result[3].tags['vs.name'])

        self.assertEqual('peak-concurrent-connections', result[4].name)
        self.assertEqual(65, result[4].value)
        self.assertEqual('Blade:1_01 VSID:1', result[4].tags['vs.id'])
        self.assertEqual('VS_1', result[4].tags['vs.name'])

if __name__ == '__main__':
    unittest.main()