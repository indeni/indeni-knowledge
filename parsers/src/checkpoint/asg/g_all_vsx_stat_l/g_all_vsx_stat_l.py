from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class GAllVsxStatL(BaseParser):

    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data = helper_methods.parse_data_as_list(raw_data, 'checkpoint_gaiasp_g_all_vsx_stat_l.textfsm')

            if data:
                for vs in data:
                    if vs['chassis'] == vs['local_chassis']:
                        tags = {}
                        tags['vs.id'] = 'Blade:{}_{} VSID:{}'.format (vs['chassis'], vs['blade'], vs['vsid'])
                        tags['vs.name'] = vs['vsname']

                        if vs['conn_current'].isnumeric():
                            self.write_double_metric('concurrent-connections', tags, 'gauge', int(vs['conn_current']), True, 'Connections - Blade/VS (Total)', 'number', 'vs.id|vs.name')
                        if vs['conn_limit'].isnumeric():
                            self.write_double_metric('concurrent-connections-limit', tags, 'gauge', int(vs['conn_limit']), True, 'Connections - Blade/VS (Limit)', 'number', 'vs.id|vs.name')
                        if vs['conn_peak'].isnumeric():
                            self.write_double_metric('peak-concurrent-connections', tags, 'gauge', int(vs['conn_peak']), True, 'Connections - Blade/VS (Peak)', 'number', 'vs.id|vs.name')

            return self.output