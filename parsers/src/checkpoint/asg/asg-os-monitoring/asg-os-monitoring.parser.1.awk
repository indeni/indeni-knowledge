############
# Why: Store information about OS, model etc in a more readable format.
# How: Use "cpstat".
###########

#Initial version Check Point Gaia R77.30
/^(Check Point |Multi-Domain )/ {
    writeComplexMetricString("os-version", null, $NF, "true", "OS Version")  # Converted to new syntax by change_ind_scripts.py script
}

#OS Name:                       Gaia Embedded
#OS Name:                       Gaia
#OS Name:                       SecurePlatform Pro
#OS Name:                       SecurePlatform
/OS Name:/ {
    writeComplexMetricString("vendor", null, "Check Point", "true", "Vendor")  # Converted to new syntax by change_ind_scripts.py script

    split($0, split_arr, ":")
    os_name = trim(split_arr[2])
    writeComplexMetricString("os-name", null, os_name, "true", "OS Name")  # Converted to new syntax by change_ind_scripts.py script
}

#Appliance Name:                VMware Virtual Platform
#Appliance Name:                Check Point 2200
/Appliance Name:/ {
    split($0, name_arr, ":")
    gsub(/Check Point/, "", name_arr[2])
    model = trim(name_arr[2])
}

#OS build 84
/OS build/ {
    writeComplexMetricString("chkp-os-build", null, $NF, "true", "OS Build")  # Converted to new syntax by change_ind_scripts.py script
}

#OS kernel version 2.6.18-92cpx86_64
/OS kernel version/ {
    writeComplexMetricString("chkp-os-kernel", null, $NF, "true", "OS Kernel Version")  # Converted to new syntax by change_ind_scripts.py script
}

#OS edition 64-bit
/OS edition/ {
    writeComplexMetricString("chkp-os-bit", null, $NF, "true", "OS Processor Bit")  # Converted to new syntax by change_ind_scripts.py script
}

#| System Status - 61000 |
#| VSX System Status - 61000
/ System Status - / {
    asg_model = $(NF-1)
    if (model == "" && asg_model != "") {
        model = asg_model
    }
    # If no model is found, then use the asg_model if it exists
    writeComplexMetricString("model", null, model, "true", "Model")  # Converted to new syntax by change_ind_scripts.py script
}
