BEGIN {
    chassis_state = 0.0
    chassis_active = 0.0
    cluster_tags["name"] = "Chassis High Availability"
}

/^[0-9]+\_+[0-9]/{
    split($0, chassis_arr, "_")
    local_chassis = chassis_arr[1]
}

# 2/25/19: Several modifications are being made on this script
# 1. Chassis should be recognized as the cluster members as each SGM blade only represent compute resources available for the Security Group to operate. 
# This means that an SGM blade can go down and the chassis will persist as the cluster member
# 2. Chassis should only be considered as being down if the status is not ACTIVE or STANDBY. As a result, Indeni should alert "Chassis Down" only if the chassis is defined as such. 
# The chassis state should be tracked separately from cluster member status
# Live Config display names should be consistent across different devices.
# Clustering should be consider

#| Chassis 1                     ACTIVE                                         |
/^\| Chassis\s+[0-9]/ {
    chassis = $3
    if (local_chassis == chassis) {
        chassis_tags["name"] = "Chassis " $3
        chassis_tags["im.identity-tags"] = "name"
        if ($0 ~ /STANDBY/) {
            chassis_state = 1.0
            chassis_active = 0.0
        } if ($0 ~ /ACTIVE/) {
            chassis_state = 1.0
            chassis_active = 1.0
        }
        writeDoubleMetric("chassis-state", chassis_tags, "gauge", chassis_state, "false")  # Converted to new syntax by change_ind_scripts.py script
        writeDoubleMetric("cluster-member-active", chassis_tags, "gauge", chassis_active, "false")  # Converted to new syntax by change_ind_scripts.py script
        writeComplexMetricString("chkp-cluster-member-active-live-config", chassis_tags, $4, "true", "Chassis Cluster Member State")  # Converted to new syntax by change_ind_scripts.py script
    }
}


#| 1  (local)     UP             Enforcing Security        10Feb17 19:37        |
#| 3              DOWN           Inactive                  NA                   |
# 2/25/19: Consider SGM blades that have the status as "DETACTED"
/^\| [0-9].*(UP|DOWN|DETACHED)/ {

    # Remove (local) to not mess up counting columns
    gsub(/\(local\)/, "", $0)

    state = $3
    blade = $2
    # To catch a status with a space in between we need to split on two spaces or more.
    split($0, split_arr, /[ ]{2,}/)
    process = split_arr[3]

    # The blade needs to be up, and active, aka "Enforcing Security" to be considered ok.
    if (local_chassis == chassis) {
        tags["name"] = "chassis: " chassis " blade: " blade
        tags["im.identity-tags"] = "name"
        blade_state_description = state " / " process
        writeComplexMetricString("blade-state-live-config", tags, blade_state_description, "true", "Blade Status")  # Converted to new syntax by change_ind_scripts.py script
        if (state == "UP" && process == "Enforcing Security") {
            blade_state = 1
        } else {
            blade_state = 0
        }
        writeDoubleMetric("blade-state", tags, "gauge", blade_state, "false")  # Converted to new syntax by change_ind_scripts.py script
    }
    # asg is always active if blade is ok
}