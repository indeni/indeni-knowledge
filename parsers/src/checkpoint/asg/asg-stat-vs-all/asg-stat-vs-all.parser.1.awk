# 5/15/19: In order to catch the cluster member state of each VS, the command used here must be executed
# Note that the information also includes active and passive cluster states
BEGIN {
    FS = "|"
    tags["im.identity-tags"] = "vs.id|vs.name"
}


#capture the local chassis number
#captures the index value where the output of the vs cluster state on the local chassis can be found
#1_1
/^[0-9]+\_+[0-9]/{
    split($0, chassis_arr, "_")
    local_chassis = chassis_arr[1]
    #index_value is designed to capture the column index relative to the chassis number. 
    #It is added with the number 3, which provides the column index value prior to the chassis output
    #e.g. If the local chassis is "Chassis 1", the index_value = 4, which corresponds to the field index number of the row.
    #| 0      | V na-core-gw       | THIS VALUE ----> STANDBY  < ------this value| ACTIVE      | Problem    | 
    index_value = 3 + chassis_arr[1]
}

# The following fetches the relevant cluster related information by going through each row in the output
#| 0      | V na-core-gw       | STANDBY     | ACTIVE      | Problem    |
/^\| [0-9]/ {
    tags["vs.id"] = trim($2)
    tags["vs.name"] = trim($3)
    cluster_member_raw_state = toupper($index_value)
    #cluster_state_raw_config is the string value of the cluster state. Customers like to view this value in the live-config, but not for alerting purposes.
    #the value is used for cluster-state-live-config, which is parity across other checkpoint scripts that fetch cluster-state.
    cluster_state_raw_config = $(NF - 1)
    #initialize the value "i", which is used to determine if the metric "cluster-state" is up or down.
    i = 0

    if (cluster_member_raw_state ~ /ACTIVE/) {
        cluster_member_state = 1.0
    } else {
        cluster_member_state = 0.0
    }

    #The logic below increments the value "i" either when the line contains "Active" or when the line contains "Standby".
    #The value "i" is to make sure that there is at least one Active member and one Standby member,
    #OR, in a rare case, two active members, in the cluster.
    #A cluster member state can be Down, Active, or Standby.
    #| 0      | V na-core-gw       | DOWN        | ACTIVE      | Problem    |
    if ($0 ~ /ACTIVE/) {
        i++
    }
    if ($0 ~ /STANDBY/) {
        i++
    }
    if ($0 ~ /ACTIVE.*ACTIVE/) {
        i = 2
    }

    #cluster should be considered as healthy if at least 2 members in the cluster are active or standby.
    #if the value is below 
    if (i >= 2) {
        cluster_state = 1.0
    } else {
        cluster_state = 0.0
    }
    writeComplexMetricString("chkp-cluster-member-active-live-config", tags, cluster_member_raw_state, "true", "Cluster member state (this)")
    writeDoubleMetric("cluster-member-active", tags, "gauge", cluster_member_state, "false")
    writeComplexMetricString("cluster-state-live-config", tags, cluster_state_raw_config, "true", "Cluster State")
    writeDoubleMetric("cluster-state", tags, "gauge", cluster_state, "false")
}