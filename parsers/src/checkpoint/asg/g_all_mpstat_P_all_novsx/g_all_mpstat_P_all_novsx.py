from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class GAllMpstatPAll(BaseParser):

    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data = helper_methods.parse_data_as_list(raw_data, 'checkpoint_gaia_g_all_mpstat_P_all.textfsm')

            if data:
                for cpu_info in data:
                    cputags = {}
                    cputags["cpu-id"] = cpu_info['cpu']
                    if (cpu_info['cpu'] == "all") :
                        cputags['cpu-is-avg'] = "true"
                        cputags['cpu-id'] = "all-average"
                        cputags['resource-metric'] = "true"
                    else:
                        cputags['cpu-is-avg'] = "false"
                        cputags['resource-metric'] = "false"
#                    cputags['vs.id'] = 'Blade:{}_{} VSID:{}'.format (cpu_info['chassis'], cpu_info['blade'], vs['vsid'])
#                    cputags['vs.name'] = vs['vsname']
                    cputags['vs.id'] = 'Blade:{}_{}'.format (cpu_info['chassis'], cpu_info['blade'])
                    cputags['blade'] = cpu_info['blade']
                    usage = round (float(cpu_info['user']) + float(cpu_info['nice']) + float(cpu_info['sys'])+ float(cpu_info['iowait']) + \
                        float(cpu_info['irq']) + float(cpu_info['soft']) + float(cpu_info['steal']) , 2)

                    self.write_double_metric("cpu-usage", cputags, "gauge", usage, "true", "CPU", "percentage", "cpu-id")  # Converted to new syntax by change_ind_scripts.py script

                return self.output
