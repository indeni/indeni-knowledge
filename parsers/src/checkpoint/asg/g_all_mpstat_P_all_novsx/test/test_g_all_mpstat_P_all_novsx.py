import os
import unittest

from checkpoint.asg.g_all_mpstat_P_all_novsx.g_all_mpstat_P_all_novsx import GAllMpstatPAll
from parser_service.public.action import *


class TestGAllMpstatPAll(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = GAllMpstatPAll()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_multiple_devices(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/multiple_devices.input', {}, {})

        # Assert
        self.assertEqual(8, len(result))

        self.assertEqual('cpu-usage', result[0].name)
        self.assertEqual(41.79, result[0].value)
        self.assertEqual('all-average', result[0].tags['cpu-id'])
        self.assertEqual('Blade:1_01', result[0].tags['vs.id'])

        self.assertEqual('cpu-usage', result[1].name)
        self.assertEqual(15.14, result[1].value)
        self.assertEqual('0', result[1].tags['cpu-id'])
        self.assertEqual('Blade:1_01', result[1].tags['vs.id'])

        self.assertEqual('cpu-usage', result[2].name)
        self.assertEqual(50.65, result[2].value)
        self.assertEqual('1', result[2].tags['cpu-id'])
        self.assertEqual('Blade:1_01', result[2].tags['vs.id'])

        self.assertEqual('cpu-usage', result[3].name)
        self.assertEqual(50.64, result[3].value)
        self.assertEqual('2', result[3].tags['cpu-id'])
        self.assertEqual('Blade:1_01', result[3].tags['vs.id'])

        self.assertEqual('cpu-usage', result[4].name)
        self.assertEqual(50.59, result[4].value)
        self.assertEqual('3', result[4].tags['cpu-id'])
        self.assertEqual('Blade:1_01', result[4].tags['vs.id'])

        self.assertEqual('cpu-usage', result[5].name)
        self.assertEqual(6.29, result[5].value)
        self.assertEqual('all-average', result[5].tags['cpu-id'])
        self.assertEqual('Blade:1_02', result[5].tags['vs.id'])

        self.assertEqual('cpu-usage', result[6].name)
        self.assertEqual(6.47, result[6].value)
        self.assertEqual('0', result[6].tags['cpu-id'])
        self.assertEqual('Blade:1_02', result[6].tags['vs.id'])

        self.assertEqual('cpu-usage', result[7].name)
        self.assertEqual(6.12, result[7].value)
        self.assertEqual('1', result[7].tags['cpu-id'])
        self.assertEqual('Blade:1_02', result[7].tags['vs.id'])

if __name__ == '__main__':
    unittest.main()