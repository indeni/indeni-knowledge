from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class CheckpointOrchStatTJ(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data = helper_methods.parse_data_as_json(raw_data)
            if data:
                known_devices = []
                chkp_name = str(device_tags.get('device-name'))
                for topology_item in data['topology_stat']:
                    if topology_item.startswith('Security Group'):
                        device = {}
                        management_ip = data['topology_stat'][topology_item]['Management IP']
                        device['name'] = str(topology_item)
                        device['ip'] = str(management_ip)
                        known_devices.append(device)
                self.write_complex_metric_object_array('known-devices', {}, known_devices, False)
        return self.output