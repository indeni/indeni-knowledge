import os
import unittest
from checkpoint.asg.orch_stat_t_j.orch_stat_t_j import CheckpointOrchStatTJ
from parser_service.public.action import *

class TestCheckpointOrchStatTJ(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = CheckpointOrchStatTJ()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_orch_stat_t_j_single_sg(self):
        result = self.parser.parse_file(self.current_dir + '/orch_stat_t_j_single_sg.input', {}, {'device-name': 'CP-MHO-01'})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].action_type, 'WriteComplexMetric')
        self.assertEqual(result[0].name, 'known-devices')
        self.assertEqual(result[0].value, [{'name': 'Security Group 1', 'ip': '172.16.20.95'}])

    def test_orch_stat_t_j_multiple_sg(self):
        result = self.parser.parse_file(self.current_dir + '/orch_stat_t_j_multiple_sg.input', {}, {'device-name': 'CP-MHO-01'})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].action_type, 'WriteComplexMetric')
        self.assertEqual(result[0].name, 'known-devices')
        self.assertEqual(result[0].value, [{'name': 'Security Group 1', 'ip': '172.16.20.95'}, 
                                           {'name': 'Security Group 2', 'ip': '172.16.20.96'}])

if __name__ == '__main__':
    unittest.main()