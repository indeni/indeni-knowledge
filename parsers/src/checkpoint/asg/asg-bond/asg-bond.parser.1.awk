BEGIN {
    FS = "|"
}
# 2/25/19: The command shows the results of the bond across both Chassis. As a result, Indeni will run asg stat -i local_id. This way, we are aware of which chassis we are logged into
# The output is formatted with the bond's status for both Chassis 1 and Chassis 2, delimited by a "/"
# The additional parsing done in AWK will separate the results of Chassis 1 and Chassis 2 for the corresponding bondstatustags
# This script will first determine which chassis Indeni is connected to currently. It will then only parse the state of the corresponding chassis.

/^[0-9]+\_+[0-9]/{
    split($0, chassis_arr, "_")
    local_chassis = "Chassis " chassis_arr[1]
}

# +--------+-----------------------+---------------+---------+--------------+----------------------------------------------+
# |Name    |Address                |Mode           |Slaves   |Result        |Comments                                      |
# |        |                       |               |         |(ch1)/(ch2)   |                                              |
# +--------+-----------------------+---------------+---------+--------------+----------------------------------------------+
# |bond4   |(MAC)  00:1c:...       |LACP 802.3ad   |eth1-04  |OK/Failed     | - eth1-04 missing LACP packets on chassis2   |
/bond|magg.*(OK|Failed)/ {
    bondname = $2
    isup = 1
    split($0, chassis_bond, "|")
    bondstatustags["name"] = bondname
    if (chassis_arr[1] == 1) {
        if (chassis_bond[6] ~ /^Failed/)
            isup = 0
    } else {
        if (chassis_bond[6] ~ /\/Failed/) 
            isup = 0
    }

    writeDoubleMetric("bond-state", bondstatustags, "gauge", isup, "true", "Bond Interfaces", "state", "name")  # Converted to new syntax by change_ind_scripts.py script
}
