from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class MaestroSecurityGroupDeviceCountParser(BaseParser):

    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):

        # Step 3 : Data Reporting

        self.write_tag("member-count",raw_data)

        return self.output      