import os
import unittest
from checkpoint.asg.orch_stat_p_j.orch_stat_p_j import CheckpointOrchStatPJ
from parser_service.public.action import *

class TestCheckpointOrchStatLJ(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = CheckpointOrchStatPJ()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_check_point_orch_stat_p_j(self):
        result = self.parser.parse_file(self.current_dir + '/check_point_orch_stat_p_j.input', {}, {})
        self.assertEqual(392, len(result))

if __name__ == '__main__':
    unittest.main()