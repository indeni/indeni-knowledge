from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class CheckpointOrchStatPJ(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data = helper_methods.parse_data_as_list(raw_data, 'orch_stat_p_j.textfsm')
            if data:
                for parsed_data in data:
                    tags = {}
                    tags['name'] = parsed_data['physical_port']
                    tags['im.identity-tags'] = 'name'
                    self.write_double_metric('network-interface-admin-state', tags, 'gauge', 1 if parsed_data['int_admin_state'] == 'UP' else 0, True, "Network Interfaces - Admin State", "state", "name")
                    self.write_double_metric('network-interface-state', tags, 'gauge', 1 if parsed_data['int_link_state'] == 'UP' else 0, True, "Network Interfaces - Link State", "state", "name")
                    self.write_complex_metric_string('network-interface-speed', tags, parsed_data['speed'], True, "Network Interfaces - Speed")
                    self.write_complex_metric_string('network-interface-type', tags, parsed_data['type'], True, "Network Interfaces - Type")
                    self.write_double_metric('network-interface-mtu', tags, 'gauge', float(parsed_data['mtu']), True, "Network Interfaces - MTU", "number", "name")
                    self.write_double_metric('network-interface-rx-frame', tags, 'gauge', float(parsed_data['rx_frames']), True, "Network Interfaces - RX Frames", "number", "name")
                    self.write_double_metric('network-interface-tx-frame', tags, 'gauge', float(parsed_data['tx_frames']), True, "Network Interfaces - TX Frames", "number", "name")
        return self.output