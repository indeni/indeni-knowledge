BEGIN {
    FS = "|"
    second = "00"
    resetVars()
}

function resetVars() {
    chassis_and_blade = ""
    current_chassis_number = ""
    current_blade = ""
    delete tags

    policy_name = ""
    delete policy_array
    policy_date = ""

    date_whole = ""
    day = ""
    month = ""
    month_num = ""
    year = ""

    time_whole = ""
    hour = ""
    minute = ""

    policy_status = -1
}

function dumpData(this_chassis_number) { # Passing in current chassis number just to be clear on the purpose of this function.
    if (this_chassis_number != "") {
        if (policy_name == "") {
            policy_status = 0
        } else {
            policy_status = 1
        }

        policy_array[0, "policy-name"] = trim(policy_name)
        writeComplexMetricObjectArray("policy-name", tags, policy_array, "true", "Firewall Policy Name")

        writeDoubleMetric("policy-installed", tags, "gauge", policy_status, "true", "Firewall Policy", "state", "name")

        if (policy_date != "") {
            writeDoubleMetric("policy-install-last-modified", tags, "gauge", datetime(year, month_num, day, hour, minute, second), "true", "Firewall Policy - Last Modified", "date", "name")
        }

        resetVars()
    }
}

#1_1
/^[0-9]+\_[0-9]/ {
    split($0, chassis_arr, "_")
    local_chassis = chassis_arr[1]
}

#|1_01   |CP-R8020SP-CLUSTER |21May19 18:56  |4d53f1d10        |Success |
#|1_02   |CP-R80.20S-FW_Polic|13Sep19 05:50  |ca4abd3be        |Failure |
#|1_03   |CP-R80.20S-FW_Polic|13Sep19 05:50  |ca4abd3be        |FooBar  |
/^\|[0-9]_[0-9]/ {
    if ($0 !~ /Success/) {
        dumpData(current_chassis_number)
    } else {
        # Write previously collected data ONLY if we have some. See checks in dumpData. We're doing it this way so that we
        # can handle long policy names which span multiple lines. See unit tests.
        dumpData(current_chassis_number)

        chassis_and_blade = $2
        split(chassis_and_blade, chassis_blade_arr, "_")
        current_chassis_number = trim(chassis_blade_arr[1])

        if (local_chassis != current_chassis_number) {
            chassis_and_blade = ""
            current_chassis_number = ""   # MUST reset these here. dumpData() depends on this being 'correctly empty'.
        } else {
            current_blade = trim(chassis_blade_arr[2])
            tags["name"] = "Chassis " current_chassis_number " Blade " current_blade

            policy_name = trim($3)
            policy_date = trim($4)
            split(policy_date, date_arr, " ")

            date_whole =  date_arr[1]
            day = substr(date_whole, 1, 2)
            month = substr(date_whole, 3, 3)
            month_num = parseMonthThreeLetter(month)
            year = "20"substr(date_whole, 6, 2)

            time_whole = date_arr[2]
            split(time_whole, time_arr, ":")
            hour = time_arr[1]
            minute = time_arr[2]
        }
    }
}

# The following block handles multi-line policy names. See unit tests.
#|       |yWithALotOf_Extra_W|               |                 |        |
$3 != "" && /^\|\s/ {
    if (current_chassis_number != "") {
        policy_name = policy_name trim($3)
    }
}

END {
    dumpData(current_chassis_number)
}