##### Why we took this approach over others ####
# It's very clear that asg diag can take several minutes for each test
# Having individual scripts that execute each script is dangerous as the Indeni Collector runs commands in sequence.
# Having multiple INDs, each running each asg diag test will significantly delay other IND scripts

#### What this IND Script does ######
# The command "asg diag verify" is actually a script that runs a whole series of tests that execute a series of health checks for the chassis.
# This script will run "asg diag verify" in the background because it takes too long to execute the command and wait for the output.
# Since we have "asg diag verify" running in the background, we have no way of parsing it in real time.
# However, "asg diag verify" outputs the contents of its tests into a text file with the naming convention "verifier_sum.<number of tests> - <date>"
# the parameter <number of tests> is reflective on how "asg diag" is executed.
# If you simply run "asg diag verify", the chassis is prompted to runs all the tests available (starting from test #1).
# Depending on which version of the Chassis, there can be as little as 37 tests (R80.20SP) or as high as 48 tests (R76SP40).
# As a result, the text file name can be something like "verifier_sum.1-37-<date>" and "verifier_sum.1-48-<date>"
# By looking for the pattering "verifier_sum.1-", we will capture any scenario where asg diag verify is executed.
# To run asg diag verify, We use the command "nohup" as it removes the association of this child process to the active terminal session.
# By using "nohup" it allows whatever command thereafter to run after the terminal closes.
# This is really important, because otherwise, the collector will wait for the test to complete.
# Eventually the Collector will timeout for waiting on the input that can take several minutes
# using nohup results in the output being piped into nohup.txt in the home/dir/.
# To avoid any new logs and the output generated form asg diag, we redirect the output to /dev/null which simply throws away the content.
# asg diag is built to generate a separate log under /var/log that generates and appends logs with the content needed to open the latest iteration of the tests.
# asg diag last_run only works for R76SP.50 and newer. asg diag stat runs on the older version. The output syntax does not change. We will run both,
# shortened to 55 minutes as we might have gaps in the data.
# Shorted blacklist as there are conditions for performance and system related issues that can be unrelated to the issues we track for.


# 3/20/19: asg diag verify creates a txt file under /var/log/
# The command "asg diag verify" is actually a command that executes all the available asg scripts that validate the configuration of the device.
# Each script runs a series of commands, parses the information, and comes back with a "Passed" or "Failed" status in a simplified table for asg diag.
# The content of all the commands executed by the scripts, along with the information parses, are stored in a text file called "verified_sum".
# Amongst the commands that "asg diag verify" executes, it also runs the command "asg_version verify -v"
# This command (Validated through CheckMates) is the only way to collect the hotfix-take of a chassis
# Instead of being redundant and running the command "asg_version verify -v" separately, we can find the contents of the output in the textfile parsed here.
# Please note, We should only collect hotfix take for R76SP code base. R80.20 has addressed a new way of collecting information through cpinfo -y all

# 4/25/19: Customer wants each test passed as a separate alert. Thus, each test needs to be passed as a separate metric.
# Additionally, they want to have verbose information from the output. As a result, we now need to parse the metric as a complexmetricstring to store the output.
# ASG Diag can be updated with new tests depending on the version that the customer is on. As a result, we need to capture those new tests in some way.
# As a catch-all for metrics that are not known, we have a metric name asg-test-state-others.

BEGIN {
    FS = "|"
    version = 0
    # Define descriptions for known tests that should be validated.
    asg_test_blacklist["Bond"]
    asg_test_blacklist["Port Speed"]
    hotfix_tags["im.identity-tags"] = ""

    asg_test_unique_metriclist["System Health"]
    asg_test_unique_metriclist["Hardware"]
    asg_test_unique_metriclist["Resources"]
    asg_test_unique_metriclist["Software Versions"]
    asg_test_unique_metriclist["Software Provision"]
    asg_test_unique_metriclist["CPU Type"]
    asg_test_unique_metriclist["Media Details"]
    asg_test_unique_metriclist["Chassis ID"]
    asg_test_unique_metriclist["SSD Health"]
    asg_test_unique_metriclist["Distribution Mode"]
    asg_test_unique_metriclist["DXL Balance"]
    asg_test_unique_metriclist["Policy"]
    asg_test_unique_metriclist["AMW Policy"]
    asg_test_unique_metriclist["SWB Updates"]
    asg_test_unique_metriclist["Installation"]
    asg_test_unique_metriclist["Security Group"]
    asg_test_unique_metriclist["Cores Distribution"]
    asg_test_unique_metriclist["SPI Affinity"]
    asg_test_unique_metriclist["Clock"]
    asg_test_unique_metriclist["Licenses"]
    asg_test_unique_metriclist["Hide NAT range"]
    asg_test_unique_metriclist["LTE"]
    asg_test_unique_metriclist["IPS Enhancement"]
    asg_test_unique_metriclist["Configuration File"]
    asg_test_unique_metriclist["USER KERNEL Dist"]
    asg_test_unique_metriclist["VSX configuration"]
    asg_test_unique_metriclist["HW Utilization"]
    asg_test_unique_metriclist["BMAC VMAC verify"]
    asg_test_unique_metriclist["MAC Setting"]
    asg_test_unique_metriclist["ARP Consistency"]
    asg_test_unique_metriclist["Interfaces"]
    asg_test_unique_metriclist["Bridge"]
    asg_test_unique_metriclist["IPv4 Route"]
    asg_test_unique_metriclist["IPv6 Route"]
    asg_test_unique_metriclist["OS Route Cache"]
    asg_test_unique_metriclist["Dynamic Routing"]
    asg_test_unique_metriclist["Local ARP"]
    asg_test_unique_metriclist["SSM QoS"]
    asg_test_unique_metriclist["IGMP Consistency"]
    asg_test_unique_metriclist["PIM Neighbors"]
    asg_test_unique_metriclist["ACL Filter"]
    asg_test_unique_metriclist["SSM Parity Errors"]
    asg_test_unique_metriclist["SYN Defender"]
    asg_test_unique_metriclist["F2F Quota"]
    asg_test_unique_metriclist["Core Dumps"]
    asg_test_unique_metriclist["Syslog"]
    asg_test_unique_metriclist["Processes"]
    asg_test_unique_metriclist["Performance hogs"]
}

#R76SP_50_JHF take_96
/take_/{
    split($0, take_array, " ")
#split is used to ensure that we only capture the "take".
#take_96
    split(take_array[2], output_array, "_")
# This is to determine if the build is R76. If it is R80, we should ignore the hotfix take here.
    if ($0 ~ /R76SP/) {
        r76version++
    }
}


#| 30 | Interfaces         | Failed (!) | (1)Interface down                     |
/^\|\s+[0-9]+\s+.*(Passed|Failed)/{
    asg_test_name = trim($3)
    asg_test_state = trim($4)
#(1)Interface down
    asg_test_state_reason = trim($5)
    tags["name"] = asg_test_name
    tags["im.identity-tags"] = "name"
    # The below identifies if the value can be found in the Blacklist. If false, then we proceed to parse the metric info.
    if (asg_test_name in asg_test_blacklist == 0) {
        # if the status is Passed (!), it means that it has not fully pased. We therefore confirm that (!) is not in the output
        if (asg_test_state == "Passed") {
            state = asg_test_state
        } else {
            state = asg_test_state " " asg_test_state_reason
        }
        if (asg_test_name in asg_test_unique_metriclist) {
            newnaming = tolower(asg_test_name)
            gsub(/ /, "-", newnaming)
            writeComplexMetricString("asg-test-state-" newnaming, tags, state, "true", "ASG Test Report")
        } else {
            writeComplexMetricString("asg-test-state-other", tags, state, "true", "ASG Test Report")
        }
    }

}

END {
    if (r76version > 0) {
        writeComplexMetricString("hotfix-jumbo-take", hotfix_tags, output_array[2], "true", "Installed Hotfix Take")
    }
}