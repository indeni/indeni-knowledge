BEGIN {
    chassis_state = 0.0
    chassis_active = 0.0
}

#1_1
/^[0-9]+\_+[0-9]/ {
    split($0, chassis_arr, "_")
    local_chassis = chassis_arr[1]
    chassis_name_index = 1 + chassis_arr[1]
}

# 1. Chassis should be recognized as the cluster members as each SGM blade only represent compute resources available for the Security Group to operate. 
# This means that an SGM blade can go down and the chassis will persist as the cluster member
# 2. Chassis should only be considered as being down if the status is not ACTIVE or STANDBY. As a result, Indeni should alert "Chassis Down" only if the chassis is defined as such. 
# The chassis state should be tracked separately from cluster member status
# Live Config display names should be consistent across different devices.
# Clustering should be consider

#--------------------------------------------------------------------------------
#| SGM ID             Chassis 1                          Chassis 2              |
# This section to get the chassis name and Chassis number
# For chassis name , we split the line into array based on more than two space , So chassis_name_index  has the chassis name
# For chassis number , We again split the chassis name into array based on more than one space
/^\| SGM ID/ {
    split($0, split_arr, /[ ]{2,}/)
    chassis_name = split_arr[chassis_name_index]
    split(chassis_name, split_name, /[ ]{1,}/)
    chassis = split_name[2]
}

#|                    STANDBY                            ACTIVE                 |
#--------------------------------------------------------------------------------
# This section is to get the chassis state, Compare and get the chassis_state & chassis_active metrics

/^\|\s+(ACTIVE|STANDBY|DOWN)/ {
    chassis_state_string =  toupper($chassis_name_index)
    if (local_chassis == chassis) {
        chassis_tags["name"] = chassis_name
        chassis_tags["im.identity-tags"] = "name"
        if (chassis_state_string ~ /STANDBY/) {
            chassis_state = 1.0
            chassis_active = 0.0
        } else if (chassis_state_string ~ /ACTIVE/) {
            chassis_state = 1.0
            chassis_active = 1.0
        }
        writeDoubleMetric("chassis-state", chassis_tags, "gauge", chassis_state, "false")
        writeDoubleMetric("cluster-member-active", chassis_tags, "gauge", chassis_active, "false")
        writeComplexMetricString("chkp-cluster-member-active-live-config", chassis_tags, chassis_state_string, "true", "Chassis Cluster Member State")
    } else {
    # TODO: Log error here when we have error logging
        exit
    }
}

# This section is to check SGM status ( blade status ) on chassis
#--------------------------------------------------------------------------------
#|  1 (local)           ACTIVE                             ACTIVE               |
#|  2                   ACTIVE                             ACTIVE               |
#--------------------------------------------------------------------------------
# SGM ID >> Identifier of the SGM. (local) is the SGM on which you ran the command.
# State of the SGM:
#   ACTIVE - The SGM is processing traffic
#   DOWN - The SGM is not processing traffic
#   Detached - No SGM is detected in a slot
/^\|\s+[0-9].*(ACTIVE|DOWN|DETACHED)/ {
    # Remove (local) to not mess up counting columns
    gsub(/\(local\)/, "", $0)
    
    # Adding plus one to Index_value to match correct state column
    state = $(chassis_name_index+1)
    blade = $2

    # The blade needs to be "ACTIVE" to be considered ok.
    if (local_chassis == chassis) {
        tags["name"] = "chassis: " chassis " blade: " blade
        tags["im.identity-tags"] = "name"
        blade_state_description = state
        writeComplexMetricString("blade-state-live-config", tags, blade_state_description, "true", "Blade Status")
        if (state == "ACTIVE") {
            blade_state = 1
        } else {
            blade_state = 0
        }
        writeDoubleMetric("blade-state", tags, "gauge", blade_state, "false")
    } else {
    # TODO: Log error here when we have error logging
        exit
    }
}