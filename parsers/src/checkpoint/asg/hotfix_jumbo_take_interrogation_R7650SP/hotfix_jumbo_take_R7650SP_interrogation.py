from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class HotfixJumboTakeInterrogation_R7650SP(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        # Step 1 : Data Extraction
        if raw_data:
            hf_jumbo_take = helper_methods.parse_data_as_list(raw_data, 'hotfix_jumbo_take_R7650SP_interrogation.textfsm')

            # Step 2 : Data Processing
            # Extracted data would look like this
            # R76SP_50_JHF take_215
            if hf_jumbo_take:
                jumbo_take = hf_jumbo_take[0]['hf_jumbo_take']
                self.write_tag('hotfix-jumbo-take', jumbo_take)

            # Step 3 : Data Reporting
            # The method must return self.output
            return self.output
