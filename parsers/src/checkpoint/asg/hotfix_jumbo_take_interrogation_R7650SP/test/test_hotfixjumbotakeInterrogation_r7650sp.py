import os
import unittest

from checkpoint.asg.hotfix_jumbo_take_interrogation_R7650SP.hotfix_jumbo_take_R7650SP_interrogation import HotfixJumboTakeInterrogation_R7650SP
from parser_service.public.action import WriteTag


class TestLowSevParser1(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = HotfixJumboTakeInterrogation_R7650SP()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_valid_input(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/valid_input.input', {}, {})

        # Assert
        self.assertEqual(1, len(result))

        self.assertTrue(isinstance(result[0], WriteTag))
        self.assertEqual('hotfix-jumbo-take', result[0].key)
        self.assertEqual('215', result[0].value)


if __name__ == '__main__':
    unittest.main()