from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser
import copy
import re

class ChkpChassisDropMonitor(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            # Data extraction:
            raw_data = re.sub(r'\s+\":','\":',raw_data)
            data = helper_methods.parse_data_as_json(raw_data)

            if data:
                keys = list(data['members'])
                total_dict = copy.deepcopy(data)

                # total_dict is initialized to have the same structure and data as the first member (arbitrary)
                for k, v in data['members'].items():
                    if k != keys[0]:
                        total_dict['members'].pop(k)

                # Removing the first member, since total_dict already have its info. We will start aggregation from the 2nd member.
                data['members'].pop(keys[0])
                total_key = keys[0]
                keys.pop(0)

                # Accumulate values from all members:
                for k in keys:
                    walk_dict(data['members'][k], total_dict['members'][total_key])

                # Reporting metrics into the DB:
                for k, v in total_dict['members'][total_key]['VS'].items():
                    if device_tags.get('vsx') == 'true':
                        walk_dict_create_metric_vsx(self, v, '', k)
                    else:
                        walk_dict_create_metric_non_vsx(self, v, '', k)
        return self.output


def walk_dict(member_dict: dict, total_dict: dict):
    for k in member_dict:
        if type(member_dict[k]) == dict:
            walk_dict(member_dict[k], total_dict[k])
        else:
            total_dict[k] = int(total_dict[k]) + int(member_dict[k])


def walk_dict_create_metric_vsx(self, total_dict: dict, name: str, vsid: int):
    for k in total_dict:
        if type(total_dict[k]) == dict:
            walk_dict_create_metric_vsx(self, total_dict[k], '{}_{}'.format(name, k), vsid)
        else:
            tags = {'name': ('VS' + '{}' + ': ' + '{}' + ': ' + '{}').format(vsid, name, k).replace('_', '', 1), 'vs.id': vsid}
            self.write_double_metric('packet-drop-counter', tags, 'counter', int(total_dict[k]), False, 'name', 'vs.id')


def walk_dict_create_metric_non_vsx(self, total_dict: dict, name: str, vsid: int):
    for k in total_dict:
        if type(total_dict[k]) == dict:
            walk_dict_create_metric_non_vsx(self, total_dict[k], '{}_{}'.format(name, k), vsid)
        else:
            tags = {'name': '{}_{}'.format(name, k).replace('_', '', 1), 'vs.id': vsid}
            self.write_double_metric('packet-drop-counter', tags, 'counter', int(total_dict[k]), False, 'name', 'vs.id')
