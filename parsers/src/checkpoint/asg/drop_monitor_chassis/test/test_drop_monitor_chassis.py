import os
import unittest

from checkpoint.asg.drop_monitor_chassis.drop_monitor_chassis import ChkpChassisDropMonitor
from parser_service.public.action import WriteDoubleMetric


class TestChkpChassisDropMonitor(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = ChkpChassisDropMonitor()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_valid_input_R80_20SP_high_values(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/test_valid_input_R80_20SP_high_values.json', {}, {})

        # Assert
        self.assertEqual(37, len(result))

        self.assertTrue(isinstance(result[0], WriteDoubleMetric))
        self.assertEqual(900000000300, result[0].value)
        self.assertEqual('psl_stats_dropped_total', result[0].tags['name'])
        self.assertEqual('counter', result[0].ds_type)

        self.assertTrue(isinstance(result[36], WriteDoubleMetric))
        self.assertEqual(4532, result[36].value)
        self.assertEqual('ppak_stats_clr pkt on vpn', result[36].tags['name'])
        self.assertEqual('counter', result[36].ds_type)


    def test_valid_input_R80_20SP_alert_multiple_vs(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/test_valid_input_R80_20SP_alert_multiple_vs.json', {}, {'vsx': 'true'})

        # Assert
        self.assertEqual(74, len(result))

        self.assertTrue(isinstance(result[0], WriteDoubleMetric))
        self.assertEqual(1414, result[0].value)
        self.assertEqual('VS0: psl_stats: dropped_total', result[0].tags['name'])
        self.assertEqual('0', result[0].tags['vs.id'])
        self.assertEqual('counter', result[0].ds_type)

        self.assertTrue(isinstance(result[48], WriteDoubleMetric))
        self.assertEqual(5036, result[48].value)
        self.assertEqual('VS1: ppak_stats: S2C violation', result[48].tags['name'])
        self.assertEqual('1', result[48].tags['vs.id'])
        self.assertEqual('counter', result[48].ds_type)


if __name__ == '__main__':
    unittest.main()
