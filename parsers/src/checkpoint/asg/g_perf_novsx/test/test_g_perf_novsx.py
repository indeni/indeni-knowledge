import os
import unittest

from checkpoint.asg.g_perf_novsx.g_perf_novsx import GPerfNoVsx
from parser_service.public.action import *


class TestGPerfNoVsx(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = GPerfNoVsx()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_1_chassis_1_blade(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/1_chassis_1_blade.input', {}, {})

        # Assert
        self.assertEqual(4, len(result))
        self.assertEqual('concurrent-connections', result[0].name)
        self.assertEqual(37, result[0].value)
        self.assertEqual('system_wide', result[0].tags['name'])
        self.assertEqual('concurrent-connections-limit', result[1].name)
        self.assertEqual(20000000, result[1].value)
        self.assertEqual('system_wide', result[1].tags['name'])
        self.assertEqual('asg-blade-concurrent-connections', result[2].name)
        self.assertEqual(37, result[2].value)
        self.assertEqual('per_blade', result[2].tags['name'])
        self.assertEqual('1', result[2].tags['chassis'])
        self.assertEqual('01', result[2].tags['blade'])
        self.assertEqual('asg-blade-concurrent-connections-limit', result[3].name)
        self.assertEqual(2000000, result[3].value)
        self.assertEqual('per_blade', result[3].tags['name'])
        self.assertEqual('1', result[3].tags['chassis'])
        self.assertEqual('01', result[3].tags['blade'])

    def test_1_chassis_2_blade(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/1_chassis_2_blade.input', {}, {})

        # Assert
        self.assertEqual(6, len(result))
        self.assertEqual('concurrent-connections', result[0].name)
        self.assertEqual(1600, result[0].value)
        self.assertEqual('system_wide', result[0].tags['name'])
        self.assertEqual('concurrent-connections-limit', result[1].name)
        self.assertEqual(20000000, result[1].value)
        self.assertEqual('system_wide', result[1].tags['name'])
        self.assertEqual('asg-blade-concurrent-connections', result[2].name)
        self.assertEqual(537, result[2].value)
        self.assertEqual('per_blade', result[2].tags['name'])
        self.assertEqual('1', result[2].tags['chassis'])
        self.assertEqual('01', result[2].tags['blade'])
        self.assertEqual('asg-blade-concurrent-connections-limit', result[3].name)
        self.assertEqual(2000000, result[3].value)
        self.assertEqual('per_blade', result[3].tags['name'])
        self.assertEqual('1', result[3].tags['chassis'])
        self.assertEqual('01', result[3].tags['blade'])
        self.assertEqual('asg-blade-concurrent-connections', result[4].name)
        self.assertEqual(1100, result[4].value)
        self.assertEqual('per_blade', result[4].tags['name'])
        self.assertEqual('1', result[4].tags['chassis'])
        self.assertEqual('02', result[4].tags['blade'])
        self.assertEqual('asg-blade-concurrent-connections-limit', result[5].name)
        self.assertEqual(2000000, result[5].value)
        self.assertEqual('per_blade', result[5].tags['name'])
        self.assertEqual('1', result[5].tags['chassis'])
        self.assertEqual('02', result[5].tags['blade'])


if __name__ == '__main__':
    unittest.main()