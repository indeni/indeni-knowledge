from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

units = {
    'G': 1000000000,
    'M': 1000000,
    'K': 1000,
}

class GPerfNoVsx(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            parsed_data = helper_methods.parse_data_as_list(raw_data, 'g_perf_novsx.textfsm')
            if parsed_data:
                tags ={}
                for entry in parsed_data:
                    if len(entry['con_conn_system_value']) > 0:
                        tags['name'] = 'system_wide'
                        self.write_double_metric('concurrent-connections', tags, 'gauge', float(entry['con_conn_system_value'])*(units[entry['con_conn_system_unit']] if len(entry['con_conn_system_unit'])>0 else 1 ), True,'Connections - System (Total)', 'number', 'name')
                        self.write_double_metric('concurrent-connections-limit', tags, 'gauge', float(entry['con_conn_system_limit']), True, 'Connections - System (Limit)', 'number', 'name')
                    elif entry['chassis'] == entry['local_chassis']:
                        tags['name'] = 'per_blade'
                        tags['chassis'] = entry['chassis']
                        tags['blade'] = entry['blade']
                        self.write_double_metric('asg-blade-concurrent-connections', tags, 'gauge', float(entry['con_conn_blade_value'])*(units[entry['con_conn_blade_unit']] if len(entry['con_conn_blade_unit'])>0  else 1), True,'Connections - Blade (Total)', 'number', 'chassis|blade')
                        self.write_double_metric('asg-blade-concurrent-connections-limit', tags, 'gauge', float(entry['con_conn_blade_limit']), True, 'Connections - Blade (Limit)', 'number', 'chassis|blade')
        return self.output
