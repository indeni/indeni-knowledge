from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class ShowMaestroSecurityGroupVerifyNewConfig(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data = helper_methods.parse_data_as_list(raw_data, 'show_maestro_security_group_verify_new_config.textfsm')
            for parsed_data in data:
                tags = {}
                tags['name'] = parsed_data['security_group']
                self.write_double_metric('maestro-sg-verify-new-config', tags, 'gauge', 0 if parsed_data['name'] == 'No changes' else 1, False)
        return self.output