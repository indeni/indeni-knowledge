import os
import unittest
from checkpoint.asg.mho.show_maestro_security_group_verify_new_config.show_maestro_security_group_verify_new_config import ShowMaestroSecurityGroupVerifyNewConfig
from parser_service.public.action import *

class TestShowMaestroSecurityGroupVerifyNewConfig(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = ShowMaestroSecurityGroupVerifyNewConfig()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_maestro_config_file_no_changes(self):
        result = self.parser.parse_file(self.current_dir + '/config_file_no_changes.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'maestro-sg-verify-new-config')
        self.assertEqual(result[0].tags['name'], 'Security Group 1')
        self.assertEqual(result[0].value, 0)

    def test_maestro_config_file_not_exists(self):
        result = self.parser.parse_file(self.current_dir + '/config_file_not_exists.input', {}, {})
        self.assertEqual(0, len(result))

    def test_maestro_config_some_changes(self):
        result = self.parser.parse_file(self.current_dir + '/config_some_changes.input', {}, {})
        self.assertEqual(3, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'maestro-sg-verify-new-config')
        self.assertEqual(result[0].tags['name'], 'Security Group 1')
        self.assertEqual(result[0].value, 0)
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].name, 'maestro-sg-verify-new-config')
        self.assertEqual(result[1].tags['name'], 'Security Group 2')
        self.assertEqual(result[1].value, 1)
        self.assertEqual(result[2].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[2].name, 'maestro-sg-verify-new-config')
        self.assertEqual(result[2].tags['name'], 'Security Group 3')
        self.assertEqual(result[2].value, 1)

if __name__ == '__main__':
    unittest.main()