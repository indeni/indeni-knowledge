from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class ShowMaestroConfigurationOrchestratorSiteAmount(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data = helper_methods.parse_data_as_list(raw_data, 'show_maestro_configuration_orchestrator_site_amount.textfsm')
            if data:
                self.write_double_metric('maestro-site-amount', {}, 'gauge', int(data[0]['site_value']), True, "Maestro site amount", "number")
        return self.output