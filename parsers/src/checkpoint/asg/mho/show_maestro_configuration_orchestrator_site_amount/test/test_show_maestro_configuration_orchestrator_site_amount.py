import os
import unittest
from checkpoint.asg.mho.show_maestro_configuration_orchestrator_site_amount.show_maestro_configuration_orchestrator_site_amount import ShowMaestroConfigurationOrchestratorSiteAmount
from parser_service.public.action import *

class TestShowMaestroConfigurationOrchestratorSiteAmount(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = ShowMaestroConfigurationOrchestratorSiteAmount()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_check_point_single_site(self):
        result = self.parser.parse_file(self.current_dir + '/single_site.input', {}, {})
        self.assertEqual(1, len(result))

    def test_check_point_dual_site(self):
        result = self.parser.parse_file(self.current_dir + '/dual_site.input', {}, {})
        self.assertEqual(1, len(result))

if __name__ == '__main__':
    unittest.main()