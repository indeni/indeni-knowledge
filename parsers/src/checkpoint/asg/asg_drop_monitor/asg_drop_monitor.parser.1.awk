BEGIN {
	ppakDropsSection = 0
}



#IP Stack qdisc drops (Tx):
#general reason                      15    PXL decision                        307
/^[a-zA-Z]/ {
	if (ppakDropsSection != 1) {
		name = $0
	} else if (ppakDropsSection == 1) {
		split($0, splitArr, /[ ]{3,}/)

		tags["name"] = splitArr[1]
		writeDoubleMetric("packet-drop-counter", tags, "counter", splitArr[2], "false")  # Converted to new syntax by change_ind_scripts.py script

		tags["name"] = splitArr[3]
		writeDoubleMetric("packet-drop-counter", tags, "counter", splitArr[4], "false")  # Converted to new syntax by change_ind_scripts.py script
	}
}

#300
/^[0-9]+$/ {
	tags["name"] = name
	writeDoubleMetric("packet-drop-counter", tags, "counter", $1, "false")  # Converted to new syntax by change_ind_scripts.py script
}

#Reason                Value              Reason                Value
/^Reason/ {
	ppakDropsSection = 1
}