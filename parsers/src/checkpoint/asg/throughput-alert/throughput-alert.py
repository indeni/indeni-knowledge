from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser
from collections import defaultdict

class ThroughputParser(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        def reconstruct_interface(_key):
            if _key[2] == "8":
                return f"eth{_key[1]}-Sync"
            else:
                return f"eth{_key[1]}-{_key[2].rjust(2, '0')}"

        # Step 1: Data Extraction
        chassis_local_list = helper_methods.parse_data_as_list(raw_data, 'checkpoint_gaiasp_asg_stat_i_local_id.textfsm')
        basic = helper_methods.parse_data_as_list(raw_data, 'checkpoint_gaiasp_usr_scripts_ssm_monitord_cli_ports.textfsm')
        data = helper_methods.parse_data_as_list(raw_data, 'checkpoint_gaiasp_cat_config_active_grep_interfaces.textfsm')

        # Step 2: Data Processing
        local_chassis = "chassis-"+str(chassis_local_list[0]['chassis_local_id'])
        for entry in data:
            if entry["port_configured"] == "Sync":
                entry["port_configured"] = "08"
        clear_list = [{k: v for k, v in d.items() if v and v.strip()} for d in basic]
        basic_records_interest = []
        relevant_pairs = {(int(record["SSM_ID_configured"]), int(record["port_configured"])) for record in data}

        for record in clear_list:
            if "SSM_id" not in record.keys() or "Port_number" not in record.keys():
                continue
            record_ssm_id = int(record["SSM_id"])
            record_port = int(record["Port_number"])
            if (record_ssm_id, record_port) in relevant_pairs:
                basic_records_interest.append(record)

        round1_list = [d for d in basic_records_interest if d["round"]=="1"]
        round2_list = [d for d in basic_records_interest if d["round"]=="2"]
        round1_dict = defaultdict(lambda: [0, 0])
        round2_dict = defaultdict(lambda: [0, 0])
        speed_dict = {}

        for record in round1_list:
            key = record["Chassis_id"], record["SSM_id"], record["Port_number"]
            value = (int(record["Low_value"]) + int(record["High_value"])) / 5000
            if record.get("incoming") == "in":
                round1_dict[key][0] += value
            elif record.get("outgoing") == "out":
                round1_dict[key][1] += value

        for record in round2_list:
            key = record["Chassis_id"], record["SSM_id"], record["Port_number"]
            value = (int(record["Low_value"]) + int(record["High_value"])) / 5000
            if record.get("incoming") == "in":
                round2_dict[key][0] += value
            elif record.get("outgoing") == "out":
                round2_dict[key][1] += value
            elif record.get("speed") == "speed":
                speed_dict[key] = int(record["Low_value"]) * 1000

        # Step 3: Data Reporting
        for key, value in round1_dict.items():
            if local_chassis in key:
                value2 = round2_dict[key]
                interface_tags = {"name": reconstruct_interface(key)}
                percent_tx_used = ((value2[1] - value[1]) / speed_dict[key]) * 100
                percent_rx_used = ((value2[0] - value[0]) / speed_dict[key]) * 100
                self.write_double_metric("network-interface-tx-util-percentage", interface_tags, "gauge", percent_tx_used, True, "Network Interfaces - Throughput Transmit", "percentage", "name")
                self.write_double_metric("network-interface-rx-util-percentage", interface_tags, "gauge", percent_rx_used, True, "Network Interfaces - Throughput Receive", "percentage", "name")
        return self.output

