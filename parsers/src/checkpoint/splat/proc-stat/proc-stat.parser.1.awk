# SecurePlatform does not have commands such as "mpstat" and does not allow top to run in batch mode.
# The only available option is to parse /proc/stat
# Information in /proc/stat is since last boot, so the output needs to be collected at least twice with a pre determined intervall between
# and then compare the difference.
# Info taken from: https://github.com/Leo-G/DevopsWiki/wiki/How-Linux-CPU-Usage-Time-and-Percentage-is-calculated

BEGIN {
	runCount = 1
	cputags["resource-metric"] = "true"
}

# cpu  17528 282 42758 379482349 10786 3427 56029 0
/^cpu/ {
	# If the name is CPU withouth a number, then its the average of all CPUs
	if ($1 == "cpu") {
		
		# Calculate total CPU time since boot by adding all counters
		split($0, splitArr, " ")
		if (runCount == 1) {
			# Data is collected for the first time, and only stored
			for (id in splitArr) {
				cpuAvgTotal = cpuAvgTotal + splitArr[id]
			}
		} else {
			# Data is collected a second time, and compared with the first data.
			for (id in splitArr) {
				cpuAvgTotalTmp = cpuAvgTotalTmp + splitArr[id]
			}
			cpuAvgTotal = cpuAvgTotalTmp - cpuAvgTotal
		}
		
		
		# Calculate idle CPU time since boot
		if (runCount == 1) {
			# Data is collected for the first time, and only stored
			cpuAvgIdle = $5 + $6
		} else {
			# Data is collected a second time, and compared with the first data.
			cpuAvgIdleTmp = $5 + $6
			cpuAvgIdle = cpuAvgIdleTmp - cpuAvgIdle
		}
		
		
		# Calculate CPU usage time since boot
		if (runCount == 1) {
			# Data is collected for the first time, and only stored
			cpuAvgUsage = cpuAvgTotal - cpuAvgIdle
		} else {
			# Data is collected a second time, and compared with the first data.
			cpuAvgUsageTmp = cpuAvgTotalTmp - cpuAvgIdleTmp
			cpuAvgUsage = cpuAvgUsageTmp - cpuAvgUsage
		}
		
		
		# Calculate CPU usage in percentage
		if (runCount == 2) {
			# Second run, compare data.
			cpuAvgUsagePercent = (cpuAvgUsage / cpuAvgTotal) * 100
			cputags["cpu-is-avg"] = "true"
			cputags["cpu-id"] = "all-average"
			writeDoubleMetric("cpu-usage", cputags, "gauge", cpuAvgUsagePercent, "true", "CPU", "percentage", "cpu-id")  # Converted to new syntax by change_ind_scripts.py script
		}
	} else {
		# If the name of the CPU is not withouth a number, then its a specific CPU.
		
		# Get CPU ID
		split($1, splitArr, "u")
		cpuId = splitArr[2]
		
		
		# Calculate total CPU time since boot by adding all counters
		split($0, splitArr, " ")
		if (runCount == 1) {
			# Data is collected for the first time, and only stored
			for (id in splitArr) {
				cpuTotal[cpuId] = cpuTotal[cpuId] + splitArr[id]
			}
		} else {
			for (id in splitArr) {
				# Data is collected a second time, and compared with the first data.
				cpuTotalTmp[cpuId] = cpuTotalTmp[cpuId] + splitArr[id]
			}
			cpuTotal[cpuId] = cpuTotalTmp[cpuId] - cpuTotal[cpuId]
		}
		
		
		# Calculate idle CPU time since boot
		if (runCount == 1) {
			# Data is collected for the first time, and only stored
			cpuIdle[cpuId] = $5 + $6
		} else {
			# Data is collected a second time, and compared with the first data.
			cpuIdleTmp[cpuId] = $5 + $6
			cpuIdle[cpuId] = cpuIdleTmp[cpuId] - cpuIdle[cpuId]
		}
		
	
		
		# Calculate CPU usage time since boot
		if (runCount == 1) {
			# Data is collected for the first time, and only stored
			cpuUsage[cpuId] = cpuTotal[cpuId] - cpuIdle[cpuId]
		} else {
			# Data is collected a second time, and compared with the first data.
			cpuUsageTmp[cpuId] = cpuTotalTmp[cpuId] - cpuIdleTmp[cpuId]
			cpuUsage[cpuId] = cpuUsageTmp[cpuId] - cpuUsage[cpuId]
		}
		
		
		# Calculate CPU usage in percentage
		if (runCount == 2) {
			cpuUsagePercent[cpuId] = (cpuUsage[cpuId] / cpuTotal[cpuId]) * 100
			cputags["cpu-is-avg"] = "false"
			cputags["cpu-id"] = cpuId
			writeDoubleMetric("cpu-usage", cputags, "gauge", cpuUsagePercent[cpuId], "true", "CPU", "percentage", "cpu-id")  # Converted to new syntax by change_ind_scripts.py script
		}
		
	}
}

/^END/ {
	# Count number of runs.
	runCount++
}