BEGIN {
	FS = ":"
}

# indeni:x:0:0::/home/indeni:/bin/bash
/:x:/ {
	iuser++
	users[iuser, "username"] = $1
}

END {
	writeComplexMetricObjectArray("users", null, users, "false")  # Converted to new syntax by change_ind_scripts.py script
}