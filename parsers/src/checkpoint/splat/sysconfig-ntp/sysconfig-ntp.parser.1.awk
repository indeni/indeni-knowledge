# SERVER1=10.10.1.75
/^SERVER/ {
	split($1, splitArr, "=")
	ipaddress = splitArr[2]
	idns++
	ntps[idns, "ipaddress"] = ipaddress
}


END {
	writeComplexMetricObjectArray("ntp-servers", null, ntps, "true", "NTP Servers")  # Converted to new syntax by change_ind_scripts.py script
}