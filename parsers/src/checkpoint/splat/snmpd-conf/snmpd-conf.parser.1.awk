BEGIN {
	snmpUnencrypted = "false"
	snmpVersion = ""
}

# SNMP service disabled.
# SNMP service enabled and listening on port 161.
/^SNMP service / {
	status = $3
	if (status == "enabled") {
		writeComplexMetricString("snmp-enabled", null, "true", "false")  # Converted to new syntax by change_ind_scripts.py script
	} else if (status == "disabled.") {
		writeComplexMetricString("snmp-enabled", null, "false", "false")  # Converted to new syntax by change_ind_scripts.py script
	}
}



# syslocation  "secret Lab"
/^syslocation|^sysLocation/ {
	snmpLocation = $0
	gsub(/syslocation|sysLocation/, "", snmpLocation)
	gsub(/\"/, "", snmpLocation)
}

#syscontact MrAdmin
/^syscontact|^sysContact/ {
	snmpContact = $0
	gsub(/syscontact|sysContact/, "", snmpContact)
	gsub(/\"/, "", snmpContact)
}

/^rocommunity |rwcommunity / {
	icommunity++
	community = $2
	communities[icommunity, "community"] = community
	snmpUnencrypted = "true"
	if (snmpVersion != "" && snmpVersion != "v2") {
		snmpVersion = snmpVersion "/v2"
	} else {
		snmpVersion = "v2"
	}
	
	if ($1 == "rwcommunity") {
		communities[icommunity, "permissions"] = "read-write"
	} else if ($1 == "rocommunity") {
		communities[icommunity, "permissions"] = "read-only"
	}
}

/^rouser |^rwuser / {
	user = $2
	security = $3
	
	snmpuser[user, "username"] = user
	snmpuser[user, "security"] = security
	
	if (snmpVersion != "" && snmpVersion != "v3") {
		snmpVersion = snmpVersion "/v3"
	} else {
		snmpVersion = "v3"
	}
	
	if ($1 == "rouser") {
		snmpuser[user, "permissions"] = "ReadOnly"
	} else if ($1 == "rwuser") {
		snmpuser[user, "permissions"] = "ReadWrite"
	}

}

END {
	if (status == "enabled") {
		writeComplexMetricObjectArray("snmp-communities", null, communities, "false")  # Converted to new syntax by change_ind_scripts.py script
		writeComplexMetricString("snmp-contact", null, trim(tolower(snmpContact)), "true", "SNMP - Contact")  # Converted to new syntax by change_ind_scripts.py script
		writeComplexMetricString("snmp-location", null, trim(snmpLocation), "true", "SNMP - Location")  # Converted to new syntax by change_ind_scripts.py script
		writeComplexMetricString("unencrypted-snmp-configured", null, snmpUnencrypted, "false")  # Converted to new syntax by change_ind_scripts.py script
		writeComplexMetricString("snmp-version", null, snmpVersion, "false")  # Converted to new syntax by change_ind_scripts.py script
	}
}