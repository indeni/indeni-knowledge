BEGIN {
	priority = 0
}

# 1.1.1.1:3022           superSecret            1  [MyRadiusServer]
/[0-9]/ {
	split($1,splitArr,":")
	host = splitArr[1]
	port = splitArr[2]
	timeout = $3
	
	if (port == "") {
		# No port set, using default
		port = 1812
	}

	servers[priority, "priority"] = priority
	servers[priority, "timeout"] = timeout
	servers[priority, "host"] = host
	servers[priority, "port"] = port
	priority++
}

END {
	writeComplexMetricObjectArray("radius-servers", null, servers, "false")  # Converted to new syntax by change_ind_scripts.py script
}