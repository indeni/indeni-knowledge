BEGIN {
    no_of_cores = 0
}

#  17:32:26 up 1 day, 41 min,  2 users,  load average: 1.00, 1.00, 1.00
/load average/ {
    one_min_average = trim($(NF-2))
    sub(/,/, "", one_min_average)

    five_min_average = trim($(NF-1))
    sub(/,/, "", five_min_average)

    fifteen_min_average = trim($NF)
    sub(/,/, "", fifteen_min_average)
    next
}

#    4
#Cannot use $ - end of line in the regex so adding a loop to avoid the first line
/^\s*[0-9]+/{
    line = $0
    #Adding loop to ignore first line  
    if (line !~ /^\s*[0-9]+\:/ ) {
        no_of_cores = trim(line)
    }
}

END {
    writeDoubleMetric("load-average-one-minute-live-config", null, "gauge", one_min_average, "true", "Load Average (1 Minute)", "number", "")
    writeDoubleMetric("load-average-five-minutes-live-config", null, "gauge", five_min_average, "true", "Load Average (5 Minutes)", "number", "")
    writeDoubleMetric("load-average-fifteen-minutes-live-config", null, "gauge", fifteen_min_average, "true", "Load Average (15 Minutes)", "number", "")

    if (no_of_cores > 0) {
        one_min_normalized = one_min_average / no_of_cores
        five_min_normalized = five_min_average / no_of_cores
        fifteen_min_normalized = fifteen_min_average / no_of_cores
    } else {
        one_min_normalized = one_min_average
        five_min_normalized = five_min_average
        fifteen_min_normalized = fifteen_min_average
    }

    writeDoubleMetric("load-average-one-minute", null, "gauge", one_min_normalized, "false")
    writeDoubleMetric("load-average-five-minutes", null, "gauge", five_min_normalized, "false")
    writeDoubleMetric("load-average-fifteen-minutes", null, "gauge", fifteen_min_normalized, "false")
}