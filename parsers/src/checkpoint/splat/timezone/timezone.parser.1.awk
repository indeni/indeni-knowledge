# GMT+2
/GMT/ {
	writeComplexMetricString("timezone", null, $1, "true", "Timezone")  # Converted to new syntax by change_ind_scripts.py script
}