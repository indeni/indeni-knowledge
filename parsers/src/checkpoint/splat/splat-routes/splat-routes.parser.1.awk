function decimalToBinary(N) {
	r = ""						# initialize result to empty (not 0)
	paddingData = ""
	while (N != 0) {			# as long as number still has a value
		r = ((N%2)?"1":"0") r	# prepend the modulos2 to the result
		N = int(N/2)			# shift right (integer division by 2)
	}

	# Need to pad with zeroes if less than 8 bits
	count = r

	# Count how many digits in count.
	count = gsub(/[0-1]/, "", count)

	if(count < 8) {
		# How many to pad?
		padding = 8 - count
		
		# Start padding
		for(i = 1; i <= padding; i++) {
			paddingData = paddingData 0
		}
		r = paddingData r 
	}

	# Return result
	return r
}

# Function to calculate 8 bits to decimal
function binaryToDecimal(binary) {

	# Reset variables.
	delete bitArr
	totalValue = ""

	# Bit values.
	bitValueArr[1] = 128
	bitValueArr[2] = 64
	bitValueArr[3] = 32
	bitValueArr[4] = 16
	bitValueArr[5] = 8
	bitValueArr[6] = 4
	bitValueArr[7] = 2
	bitValueArr[8] = 1

	# Record each binary value into an array.
	for(i = 1; i <= 8; i++) {
		bitArr[i] = substr(binary, i, 1)
	}

	# For each binary, calculate value.
	for(id in bitArr) {
		if (bitArr[id] == 1) {
			value = bitValueArr[id]
		}
		totalValue = totalValue + value

		value = 0
	}

	# Return result
	return totalValue
}


BEGIN {
	routeSection = 0

	netMaskToCIDR["0.0.0.0"] = 0
	netMaskToCIDR["128.0.0.0"] = 1
	netMaskToCIDR["192.0.0.0"] = 2
	netMaskToCIDR["224.0.0.0"] = 3
	netMaskToCIDR["240.0.0.0"] = 4
	netMaskToCIDR["248.0.0.0"] = 5
	netMaskToCIDR["252.0.0.0"] = 6
	netMaskToCIDR["254.0.0.0"] = 7
	netMaskToCIDR["255.0.0.0"] = 8
	netMaskToCIDR["255.128.0.0"] = 9
	netMaskToCIDR["255.192.0.0"] = 10
	netMaskToCIDR["255.224.0.0"] = 11
	netMaskToCIDR["255.240.0.0"] = 12
	netMaskToCIDR["255.248.0.0"] = 13
	netMaskToCIDR["255.252.0.0"] = 14
	netMaskToCIDR["255.254.0.0"] = 15
	netMaskToCIDR["255.255.0.0"] = 16
	netMaskToCIDR["255.255.128.0"] = 17
	netMaskToCIDR["255.255.192.0"] = 18
	netMaskToCIDR["255.255.224.0"] = 19
	netMaskToCIDR["255.255.240.0"] = 20
	netMaskToCIDR["255.255.248.0"] = 21
	netMaskToCIDR["255.255.252.0"] = 22
	netMaskToCIDR["255.255.254.0"] = 23
	netMaskToCIDR["255.255.255.0"] = 24
	netMaskToCIDR["255.255.255.128"] = 25
	netMaskToCIDR["255.255.255.192"] = 26
	netMaskToCIDR["255.255.255.224"] = 27
	netMaskToCIDR["255.255.255.240"] = 28
	netMaskToCIDR["255.255.255.248"] = 29
	netMaskToCIDR["255.255.255.252"] = 30
	netMaskToCIDR["255.255.255.254"] = 31
	netMaskToCIDR["255.255.255.255"] = 32
}

#                : (route
/\s+: \(route/ {
	routeSection = 1
	iStaticRoute++

	next
}

#                        :dest ("10.1.2.0/24")
#                        :dest (default)
/\s+:dest \(/ {
	networkAndMask = $2

	#("10.1.2.0/24")
	#(default)
	gsub(/\(|\"|\)/, "", networkAndMask)

	if (networkAndMask == "default") {
		destinationNetwork = "0.0.0.0"
		subnetPrefix = 0
	} else {

		split(networkAndMask, networkAdnMaskSplitArr, /\//)

		destinationNetwork = networkAdnMaskSplitArr[1]
		subnetPrefix = networkAdnMaskSplitArr[2]
	}
	
	if (destinationNetwork != "127.0.0.0") {
		staticRoutes[iStaticRoute, "network"] = destinationNetwork
		staticRoutes[iStaticRoute, "mask"] = subnetPrefix
	}

	next
}


#                        :via (192.168.194.254)
/\s+:via \(/ {
	nextHop = $2

	#(192.168.194.254)
	gsub(/\(|\)/, "", nextHop)

	staticRoutes[iStaticRoute, "next-hop"] = nextHop

	next
}

#        )
/\s+\)$/ {
	routeSection = 0
}


#            inet addr:192.168.194.41  Bcast:192.168.194.255  Mask:255.255.255.0
/^\s+inet addr:/ {
	ipAddress = $2
	subnetMask = $4

	
	#addr:192.168.194.41
	gsub(/addr:/, "", ipAddress)

	#Mask:255.255.255.0
	gsub(/Mask:/, "", subnetMask)

	subnetPrefix = netMaskToCIDR[subnetMask]

	# Calculate the subnet address.
	# Get subnet address by filling the host portion of the address with zeroes.
	# Example: 172.16.35.123/20

	# Split in octets.
	#172.16.35.123
	split(ipAddress, ipAddressArr, ".")

	# Translating all octets to binary (no dots between octets)
	for(octet in ipAddressArr) {
		octetBinary = decimalToBinary(ipAddressArr[octet])

		ipBinary = ipBinary octetBinary
	}

	# 10101100000100000010001101111011

	# Remove everything except first 20 bits.
	#10101100000100000010
	keepBits = substr(ipBinary, 1, subnetPrefix)
	
	
	subnetIpBinary = keepBits

	#32-20=12
	addBinary = 32 - subnetPrefix

	# Fill out remaining bits as zeroes until reaching 32 bits.
	for (i = 1; i <= addBinary; i++) {
		subnetIpBinary = subnetIpBinary 0
	}

	#subnetIpBinary = 10101100000100000010000000000000

	# Split binary into octets
	octetArr[1] = substr(subnetIpBinary, 1, 8)
	octetArr[2] = substr(subnetIpBinary, 9, 8)
	octetArr[3] = substr(subnetIpBinary, 17, 8)
	octetArr[4] = substr(subnetIpBinary, 25, 8)

	# Convert från binary to decimal, and combine octets to form the subnet IP address.
	subnetIp = binaryToDecimal(octetArr[1]) "." binaryToDecimal(octetArr[2]) "." binaryToDecimal(octetArr[3]) "." binaryToDecimal(octetArr[4])

	# If the subnet mask is empty, like it is for localhost, ignore.
	if (subnetMask != "") {
		iDirectRoute++
		directRoutes[iDirectRoute, "network"] = subnetIp
		directRoutes[iDirectRoute, "mask"] = subnetPrefix
	}


	# Reset variables
	ipBinary = ""

	next
}

END {
	writeComplexMetricObjectArray("static-routing-table", null, staticRoutes, "true", "Static routes")  # Converted to new syntax by change_ind_scripts.py script
	writeComplexMetricObjectArray("connected-networks-table", null, directRoutes, "true", "Directly Connected Networks")  # Converted to new syntax by change_ind_scripts.py script
}