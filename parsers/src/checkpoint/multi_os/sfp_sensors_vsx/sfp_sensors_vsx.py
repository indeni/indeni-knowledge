from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class ChkpSfpSensorVsx(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        # Step 1 : Data Extraction
        data = helper_methods.parse_data_as_list(raw_data, 'checkpoint_vsx_ethtool_m.textfsm')

        # Step 2 : Data Processing and Reporting
        if data:
            interface_found = []
            for sfp in data:
                if sfp['int_name'] not in interface_found:
                    interface_found.append(sfp['int_name'])
                    tags = {}
                    tags['name'] = sfp['int_name']
                    tags['type'] = 'Laser Bias ({})'.format(sfp['laser_bias_units'])
                    self.write_double_metric("laser-bias-value", tags, "gauge", float(sfp['laser_bias_value']), True, "SFP Sensor", "number", "name|type")
                    self.write_double_metric("laser-bias-high-alarm", tags, "gauge", float(sfp['laser_bias_high_alarm']), False)
                    self.write_double_metric("laser-bias-low-alarm", tags, "gauge", float(sfp['laser_bias_low_alarm']), False)
                    tags['type'] = 'Laser Power In ({})'.format(sfp['laser_in_power_units'])
                    self.write_double_metric("laser-power-in-value", tags, "gauge", float(sfp['laser_in_power_value']), True, "SFP Sensor", "number", "name|type")
                    self.write_double_metric("laser-power-in-high-alarm", tags, "gauge", float(sfp['laser_in_power_high_alarm']), False)
                    self.write_double_metric("laser-power-in-low-alarm", tags, "gauge", float(sfp['laser_in_power_low_alarm']), False)
                    tags['type'] = 'Laser Power Out ({})'.format(sfp['laser_out_power_units'])
                    self.write_double_metric("laser-power-out-value", tags, "gauge", float(sfp['laser_out_power_value']), True, "SFP Sensor", "number", "name|type")
                    self.write_double_metric("laser-power-out-high-alarm", tags, "gauge", float(sfp['laser_out_power_high_alarm']), False)
                    self.write_double_metric("laser-power-out-low-alarm", tags, "gauge", float(sfp['laser_out_power_low_alarm']), False)
                    tags['type'] = 'Temperature ({})'.format(sfp['temperature_units'])
                    self.write_double_metric("laser-temperature-value", tags, "gauge",float( sfp['temperature_value']), True, "SFP Sensor", "number", "name|type")
                    self.write_double_metric("laser-temperature-high-alarm", tags, "gauge", float(sfp['temperature_high_alarm']), False)
                    self.write_double_metric("laser-temperature-low-alarm", tags, "gauge",float( sfp['temperature_low_alarm']), False)
                    tags['type'] = 'Voltage ({})'.format(sfp['voltage_units'])
                    self.write_double_metric("laser-voltage-value", tags, "gauge", float(sfp['voltage_value']), True, "SFP Sensor", "number", "name|type")
                    self.write_double_metric("laser-voltage-high-alarm", tags, "gauge", float(sfp['voltage_high_alarm']), False)
                    self.write_double_metric("laser-voltage-low-alarm", tags, "gauge", float(sfp['voltage_low_alarm']), False)
        return self.output
