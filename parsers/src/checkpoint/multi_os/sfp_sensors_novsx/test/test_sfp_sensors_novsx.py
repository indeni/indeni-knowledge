import os
import unittest
from checkpoint.multi_os.sfp_sensors_novsx.sfp_sensors_novsx import ChkpSfpSensorNoVsx
from parser_service.public.action import *

class TestChkpSfpSensorNoVsx(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = ChkpSfpSensorNoVsx()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_snmp_trap_single_community(self):
        result = self.parser.parse_file(self.current_dir + '/sfp_sensors_novsx.input', {}, {})
        self.assertEqual(45, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'laser-bias-value')
        self.assertEqual(result[0].tags['name'], 'eth1-01')
        self.assertEqual(result[0].tags['type'], 'Laser Bias (mA)')
        self.assertEqual(result[0].tags['live-config'], 'true')
        self.assertEqual(result[0].tags['display-name'], 'SFP Sensor')
        self.assertEqual(result[0].value, 9.104)
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].name, 'laser-bias-high-alarm')
        self.assertEqual(result[1].tags['name'], 'eth1-01')
        self.assertEqual(result[1].tags['type'], 'Laser Bias (mA)')
        self.assertEqual(result[1].value, 13.2)
        self.assertEqual(result[2].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[2].name, 'laser-bias-low-alarm')
        self.assertEqual(result[2].tags['name'], 'eth1-01')
        self.assertEqual(result[2].tags['type'], 'Laser Bias (mA)')
        self.assertEqual(result[2].value, 2.0)
        self.assertEqual(result[3].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[3].name, 'laser-power-in-value')
        self.assertEqual(result[3].tags['name'], 'eth1-01')
        self.assertEqual(result[3].tags['type'], 'Laser Power In (mW)')
        self.assertEqual(result[3].tags['live-config'], 'true')
        self.assertEqual(result[3].tags['display-name'], 'SFP Sensor')
        self.assertEqual(result[3].value, 0.5551)
        self.assertEqual(result[4].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[4].name, 'laser-power-in-high-alarm')
        self.assertEqual(result[4].tags['name'], 'eth1-01')
        self.assertEqual(result[4].tags['type'], 'Laser Power In (mW)')
        self.assertEqual(result[4].value, 1.0)
        self.assertEqual(result[5].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[5].name, 'laser-power-in-low-alarm')
        self.assertEqual(result[5].tags['name'], 'eth1-01')
        self.assertEqual(result[5].tags['type'], 'Laser Power In (mW)')
        self.assertEqual(result[5].value, 0.01)
        self.assertEqual(result[6].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[6].name, 'laser-power-out-value')
        self.assertEqual(result[6].tags['name'], 'eth1-01')
        self.assertEqual(result[6].tags['type'], 'Laser Power Out (mW)')
        self.assertEqual(result[6].tags['live-config'], 'true')
        self.assertEqual(result[6].tags['display-name'], 'SFP Sensor')
        self.assertEqual(result[6].value, 0.5439)
        self.assertEqual(result[7].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[7].name, 'laser-power-out-high-alarm')
        self.assertEqual(result[7].tags['name'], 'eth1-01')
        self.assertEqual(result[7].tags['type'], 'Laser Power Out (mW)')
        self.assertEqual(result[7].value, 1.0)
        self.assertEqual(result[8].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[8].name, 'laser-power-out-low-alarm')
        self.assertEqual(result[8].tags['name'], 'eth1-01')
        self.assertEqual(result[8].tags['type'], 'Laser Power Out (mW)')
        self.assertEqual(result[8].value, 0.1585)
        self.assertEqual(result[9].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[9].name, 'laser-temperature-value')
        self.assertEqual(result[9].tags['name'], 'eth1-01')
        self.assertEqual(result[9].tags['type'], 'Temperature (degrees C)')
        self.assertEqual(result[9].tags['live-config'], 'true')
        self.assertEqual(result[9].tags['display-name'], 'SFP Sensor')
        self.assertEqual(result[9].value, 38.59)
        self.assertEqual(result[10].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[10].name, 'laser-temperature-high-alarm')
        self.assertEqual(result[10].tags['name'], 'eth1-01')
        self.assertEqual(result[10].tags['type'], 'Temperature (degrees C)')
        self.assertEqual(result[10].value, 78.0)
        self.assertEqual(result[11].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[11].name, 'laser-temperature-low-alarm')
        self.assertEqual(result[11].tags['name'], 'eth1-01')
        self.assertEqual(result[11].tags['type'], 'Temperature (degrees C)')
        self.assertEqual(result[11].value, -13.0)
        self.assertEqual(result[12].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[12].name, 'laser-voltage-value')
        self.assertEqual(result[12].tags['name'], 'eth1-01')
        self.assertEqual(result[12].tags['type'], 'Voltage (V)')
        self.assertEqual(result[12].tags['live-config'], 'true')
        self.assertEqual(result[12].tags['display-name'], 'SFP Sensor')
        self.assertEqual(result[12].value, 3.3075)
        self.assertEqual(result[13].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[13].name, 'laser-voltage-high-alarm')
        self.assertEqual(result[13].tags['name'], 'eth1-01')
        self.assertEqual(result[13].tags['type'], 'Voltage (V)')
        self.assertEqual(result[13].value, 3.7)
        self.assertEqual(result[14].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[14].name, 'laser-voltage-low-alarm')
        self.assertEqual(result[14].tags['name'], 'eth1-01')
        self.assertEqual(result[14].tags['type'], 'Voltage (V)')
        self.assertEqual(result[14].value, 2.9)
        self.assertEqual(result[15].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[15].name, 'laser-bias-value')
        self.assertEqual(result[15].tags['name'], 'eth1-02')
        self.assertEqual(result[15].tags['type'], 'Laser Bias (mA)')
        self.assertEqual(result[15].tags['live-config'], 'true')
        self.assertEqual(result[15].tags['display-name'], 'SFP Sensor')
        self.assertEqual(result[15].value, 9.094)
        self.assertEqual(result[16].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[16].name, 'laser-bias-high-alarm')
        self.assertEqual(result[16].tags['name'], 'eth1-02')
        self.assertEqual(result[16].tags['type'], 'Laser Bias (mA)')
        self.assertEqual(result[16].value, 13.2)
        self.assertEqual(result[17].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[17].name, 'laser-bias-low-alarm')
        self.assertEqual(result[17].tags['name'], 'eth1-02')
        self.assertEqual(result[17].tags['type'], 'Laser Bias (mA)')
        self.assertEqual(result[17].value, 2.0)
        self.assertEqual(result[18].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[18].name, 'laser-power-in-value')
        self.assertEqual(result[18].tags['name'], 'eth1-02')
        self.assertEqual(result[18].tags['type'], 'Laser Power In (mW)')
        self.assertEqual(result[18].tags['live-config'], 'true')
        self.assertEqual(result[18].tags['display-name'], 'SFP Sensor')
        self.assertEqual(result[18].value, 0.5678)
        self.assertEqual(result[19].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[19].name, 'laser-power-in-high-alarm')
        self.assertEqual(result[19].tags['name'], 'eth1-02')
        self.assertEqual(result[19].tags['type'], 'Laser Power In (mW)')
        self.assertEqual(result[19].value, 1.0)
        self.assertEqual(result[20].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[20].name, 'laser-power-in-low-alarm')
        self.assertEqual(result[20].tags['name'], 'eth1-02')
        self.assertEqual(result[20].tags['type'], 'Laser Power In (mW)')
        self.assertEqual(result[20].value, 0.01)
        self.assertEqual(result[21].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[21].name, 'laser-power-out-value')
        self.assertEqual(result[21].tags['name'], 'eth1-02')
        self.assertEqual(result[21].tags['type'], 'Laser Power Out (mW)')
        self.assertEqual(result[21].tags['live-config'], 'true')
        self.assertEqual(result[21].tags['display-name'], 'SFP Sensor')
        self.assertEqual(result[21].value, 0.5475)
        self.assertEqual(result[22].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[22].name, 'laser-power-out-high-alarm')
        self.assertEqual(result[22].tags['name'], 'eth1-02')
        self.assertEqual(result[22].tags['type'], 'Laser Power Out (mW)')
        self.assertEqual(result[22].value, 1.0)
        self.assertEqual(result[23].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[23].name, 'laser-power-out-low-alarm')
        self.assertEqual(result[23].tags['name'], 'eth1-02')
        self.assertEqual(result[23].tags['type'], 'Laser Power Out (mW)')
        self.assertEqual(result[23].value, 0.1585)
        self.assertEqual(result[24].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[24].name, 'laser-temperature-value')
        self.assertEqual(result[24].tags['name'], 'eth1-02')
        self.assertEqual(result[24].tags['type'], 'Temperature (degrees C)')
        self.assertEqual(result[24].tags['live-config'], 'true')
        self.assertEqual(result[24].tags['display-name'], 'SFP Sensor')
        self.assertEqual(result[24].value, 37.93)
        self.assertEqual(result[25].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[25].name, 'laser-temperature-high-alarm')
        self.assertEqual(result[25].tags['name'], 'eth1-02')
        self.assertEqual(result[25].tags['type'], 'Temperature (degrees C)')
        self.assertEqual(result[25].value, 78.0)
        self.assertEqual(result[26].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[26].name, 'laser-temperature-low-alarm')
        self.assertEqual(result[26].tags['name'], 'eth1-02')
        self.assertEqual(result[26].tags['type'], 'Temperature (degrees C)')
        self.assertEqual(result[26].value, -13.0)
        self.assertEqual(result[27].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[27].name, 'laser-voltage-value')
        self.assertEqual(result[27].tags['name'], 'eth1-02')
        self.assertEqual(result[27].tags['type'], 'Voltage (V)')
        self.assertEqual(result[27].tags['live-config'], 'true')
        self.assertEqual(result[27].tags['display-name'], 'SFP Sensor')
        self.assertEqual(result[27].value, 3.3046)
        self.assertEqual(result[28].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[28].name, 'laser-voltage-high-alarm')
        self.assertEqual(result[28].tags['name'], 'eth1-02')
        self.assertEqual(result[28].tags['type'], 'Voltage (V)')
        self.assertEqual(result[28].value, 3.7)
        self.assertEqual(result[29].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[29].name, 'laser-voltage-low-alarm')
        self.assertEqual(result[29].tags['name'], 'eth1-02')
        self.assertEqual(result[29].tags['type'], 'Voltage (V)')
        self.assertEqual(result[29].value, 2.9)
        self.assertEqual(result[30].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[30].name, 'laser-bias-value')
        self.assertEqual(result[30].tags['name'], 'eth1-03')
        self.assertEqual(result[30].tags['type'], 'Laser Bias (mA)')
        self.assertEqual(result[30].tags['live-config'], 'true')
        self.assertEqual(result[30].tags['display-name'], 'SFP Sensor')
        self.assertEqual(result[30].value, 9.032)
        self.assertEqual(result[31].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[31].name, 'laser-bias-high-alarm')
        self.assertEqual(result[31].tags['name'], 'eth1-03')
        self.assertEqual(result[31].tags['type'], 'Laser Bias (mA)')
        self.assertEqual(result[31].value, 13.2)
        self.assertEqual(result[32].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[32].name, 'laser-bias-low-alarm')
        self.assertEqual(result[32].tags['name'], 'eth1-03')
        self.assertEqual(result[32].tags['type'], 'Laser Bias (mA)')
        self.assertEqual(result[32].value, 2.0)
        self.assertEqual(result[33].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[33].name, 'laser-power-in-value')
        self.assertEqual(result[33].tags['name'], 'eth1-03')
        self.assertEqual(result[33].tags['type'], 'Laser Power In (mW)')
        self.assertEqual(result[33].tags['live-config'], 'true')
        self.assertEqual(result[33].tags['display-name'], 'SFP Sensor')
        self.assertEqual(result[33].value, 0.602)
        self.assertEqual(result[34].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[34].name, 'laser-power-in-high-alarm')
        self.assertEqual(result[34].tags['name'], 'eth1-03')
        self.assertEqual(result[34].tags['type'], 'Laser Power In (mW)')
        self.assertEqual(result[34].value, 1.0)
        self.assertEqual(result[35].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[35].name, 'laser-power-in-low-alarm')
        self.assertEqual(result[35].tags['name'], 'eth1-03')
        self.assertEqual(result[35].tags['type'], 'Laser Power In (mW)')
        self.assertEqual(result[35].value, 0.01)
        self.assertEqual(result[36].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[36].name, 'laser-power-out-value')
        self.assertEqual(result[36].tags['name'], 'eth1-03')
        self.assertEqual(result[36].tags['type'], 'Laser Power Out (mW)')
        self.assertEqual(result[36].tags['live-config'], 'true')
        self.assertEqual(result[36].tags['display-name'], 'SFP Sensor')
        self.assertEqual(result[36].value, 0.5737)
        self.assertEqual(result[37].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[37].name, 'laser-power-out-high-alarm')
        self.assertEqual(result[37].tags['name'], 'eth1-03')
        self.assertEqual(result[37].tags['type'], 'Laser Power Out (mW)')
        self.assertEqual(result[37].value, 1.0)
        self.assertEqual(result[38].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[38].name, 'laser-power-out-low-alarm')
        self.assertEqual(result[38].tags['name'], 'eth1-03')
        self.assertEqual(result[38].tags['type'], 'Laser Power Out (mW)')
        self.assertEqual(result[38].value, 0.1585)
        self.assertEqual(result[39].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[39].name, 'laser-temperature-value')
        self.assertEqual(result[39].tags['name'], 'eth1-03')
        self.assertEqual(result[39].tags['type'], 'Temperature (degrees C)')
        self.assertEqual(result[39].tags['live-config'], 'true')
        self.assertEqual(result[39].tags['display-name'], 'SFP Sensor')
        self.assertEqual(result[39].value, 36.25)
        self.assertEqual(result[40].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[40].name, 'laser-temperature-high-alarm')
        self.assertEqual(result[40].tags['name'], 'eth1-03')
        self.assertEqual(result[40].tags['type'], 'Temperature (degrees C)')
        self.assertEqual(result[40].value, 78.0)
        self.assertEqual(result[41].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[41].name, 'laser-temperature-low-alarm')
        self.assertEqual(result[41].tags['name'], 'eth1-03')
        self.assertEqual(result[41].tags['type'], 'Temperature (degrees C)')
        self.assertEqual(result[41].value, -13.0)
        self.assertEqual(result[42].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[42].name, 'laser-voltage-value')
        self.assertEqual(result[42].tags['name'], 'eth1-03')
        self.assertEqual(result[42].tags['type'], 'Voltage (V)')
        self.assertEqual(result[42].value, 3.317)
        self.assertEqual(result[43].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[43].name, 'laser-voltage-high-alarm')
        self.assertEqual(result[43].tags['name'], 'eth1-03')
        self.assertEqual(result[43].tags['type'], 'Voltage (V)')
        self.assertEqual(result[43].value, 3.7)
        self.assertEqual(result[44].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[44].name, 'laser-voltage-low-alarm')
        self.assertEqual(result[44].tags['name'], 'eth1-03')
        self.assertEqual(result[44].tags['type'], 'Voltage (V)')
        self.assertEqual(result[44].value, 2.9)

if __name__ == '__main__':
    unittest.main()