from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class ChkpOsFileTimestamp(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            parsed_data = helper_methods.parse_data_as_list(raw_data, 'chkp_os_file_timestamp.textfsm')
            if parsed_data:
                for entry in parsed_data :
                    tags = {}
                    tags['name'] = entry['filename']
                    self.write_double_metric('file-modifed-last-day', tags, 'gauge', 1 if (int(entry['sys_timestamp'])-int(entry['mod_timestamp'])) > 86400 else 0, False)
        return self.output