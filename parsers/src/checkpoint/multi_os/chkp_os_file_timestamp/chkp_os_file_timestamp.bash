echo -n "__CURRENT_TIME:";date +%s
ls -1 $FWDIR/conf/*.def| xargs -I {} sh -c 'echo "__FILE:"{}; echo -n "__LAST_MOD:";date -r {} +%s'
file="/etc/sysctl.conf"; echo "__FILE:$file";  echo -n "__LAST_MOD:";date -r $file +%s
file="/etc/modprobe.conf"; echo "__FILE:$file";  echo -n "__LAST_MOD:";date -r $file +%s
file="/etc/rc.local"; echo "__FILE:$file";  echo -n "__LAST_MOD:";date -r $file +%s
file="/etc/rc.sysinit"; echo "__FILE:$file";  echo -n "__LAST_MOD:";date -r $file +%s
file="/etc/scpusers"; echo "__FILE:$file";  echo -n "__LAST_MOD:";date -r $file +%s
file="/etc/grub.conf"; echo "__FILE:$file";  echo -n "__LAST_MOD:";date -r $file +%s
file="/etc/resolv.conf"; echo "__FILE:$file";  echo -n "__LAST_MOD:";date -r $file +%s
file="/etc/syslog.conf"; echo "__FILE:$file";  echo -n "__LAST_MOD:";date -r $file +%s
file="$FWDIR/boot/modules/fwkern.conf"; echo "__FILE:$file";  echo -n "__LAST_MOD:";date -r $file +%s
file="$FWDIR/conf/local.arp"; echo "__FILE:$file";  echo -n "__LAST_MOD:";date -r $file +%s
