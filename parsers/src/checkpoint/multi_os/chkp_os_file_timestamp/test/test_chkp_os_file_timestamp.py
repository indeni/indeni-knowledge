import os
import unittest

from checkpoint.multi_os.chkp_os_file_timestamp.chkp_os_file_timestamp import ChkpOsFileTimestamp
from parser_service.public.action import *

expected_some_files_missing = [{'metric': 'file-modifed-last-day', 'tags': {'name': '/opt/CPsuite-R81/fw1/conf/updates.def'}, 'value': 1},
    {'metric': 'file-modifed-last-day', 'tags': {'name': '/etc/sysctl.conf'}, 'value': 1},
    {'metric': 'file-modifed-last-day', 'tags': {'name': '/etc/modprobe.conf'}, 'value': 1},
    {'metric': 'file-modifed-last-day', 'tags': {'name': '/etc/rc.local'}, 'value': 1},
    {'metric': 'file-modifed-last-day', 'tags': {'name': '/etc/rc.sysinit'}, 'value': 1},
    {'metric': 'file-modifed-last-day', 'tags': {'name': '/etc/grub.conf'}, 'value': 0},
    {'metric': 'file-modifed-last-day', 'tags': {'name': '/etc/resolv.conf'}, 'value': 0},
    {'metric': 'file-modifed-last-day', 'tags': {'name': '/etc/syslog.conf'}, 'value': 0}]


class TestChkpOsFileTimestamp(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = ChkpOsFileTimestamp()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_some_files_missing(self):
        result = self.parser.parse_file(self.current_dir + '/some_files_missing.input', {}, {})
        self.assertEqual(8, len(result))

        for i in range(len(expected_some_files_missing)):
            self.assertEqual(expected_some_files_missing[i]['tags'], result[i].tags)
            self.assertEqual(expected_some_files_missing[i]['metric'], result[i].name)
            self.assertEqual(expected_some_files_missing[i]['value'], result[i].value)


if __name__ == '__main__':
    unittest.main()