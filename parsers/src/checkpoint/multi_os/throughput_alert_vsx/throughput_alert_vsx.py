from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class ChkpThroughputAlertVsx(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):

        speed_units = {
            "Mb/s": 1000000,
            "Gb/s": 1000000000
        }
        # Step 1 : Data Extraction
        data = helper_methods.parse_data_as_list(raw_data, 'throughput_alert_vsx.textfsm')

        # Step 2 : Data Processing and Reporting
        if data:
            interfaces_list=list(dict.fromkeys([entry['int_name'] for entry in data]))
            if len(interfaces_list) > 0:
                for interface in interfaces_list:
                    interface_stats_tx=[entry['tx_bytes'] for entry in data if entry['int_name']==interface]
                    interface_stats_rx=[entry['rx_bytes'] for entry in data if entry['int_name']==interface]
                    interface_speed_value=int([entry['int_speed_value'] for entry in data if entry['int_name']==interface][0])
                    interface_speed_unit=str([entry['int_speed_unit'] for entry in data if entry['int_name']==interface][0])
                    tags = {
                        'vs.id': str([entry['vsid'] for entry in data if entry['int_name']==interface][0]),
                        'vs.name': str([entry['vsname'] for entry in data if entry['int_name']==interface][0]),
                    }
                    tags['name'] = 'VSID:{}_{}_{}{}'.format(tags['vs.id'],str(interface), interface_speed_value, interface_speed_unit.rstrip('/s'))
                    percentage_rx = percentage_tx = 0
                    if len(interface_stats_tx) == 2 and len(interface_stats_rx) == 2:
                        percentage_tx = (abs(int(interface_stats_tx[0])-int(interface_stats_tx[1]))*8/10)/(interface_speed_value*speed_units[interface_speed_unit])*100
                        if percentage_tx < 120:
                            self.write_double_metric('network-interface-tx-util-percentage',tags,'gauge', percentage_tx, True, 'Network Interfaces - Throughput Transmit', 'percentage', 'vs.id|vs.name|name')
                        percentage_rx = (abs(int(interface_stats_rx[0])-int(interface_stats_rx[1]))*8/10)/(interface_speed_value*speed_units[interface_speed_unit])*100
                        if percentage_rx < 120:
                            self.write_double_metric('network-interface-rx-util-percentage',tags,'gauge', percentage_rx, True, 'Network Interfaces - Throughput Receive', 'percentage', 'vs.id|vs.name|name')
                    interface_stats_packets_tx=[entry['tx_packets'] for entry in data if entry['int_name']==interface]
                    interface_stats_packets_rx=[entry['rx_packets'] for entry in data if entry['int_name']==interface]
                    if len(interface_stats_packets_tx) == 2 and len(interface_stats_packets_rx) == 2:
                        interface_stats_errors_tx=[entry['tx_errors'] for entry in data if entry['int_name']==interface]
                        interface_stats_errors_rx=[entry['rx_errors'] for entry in data if entry['int_name']==interface]
                        if len(interface_stats_errors_tx) == 2 and len(interface_stats_errors_rx) == 2:
                            tags['type'] = 'Errors percentage'
                            if abs(int(interface_stats_packets_tx[0])-int(interface_stats_packets_tx[1])) != 0  and percentage_tx > 0.1:
                                percentage_errors_tx = (abs(int(interface_stats_errors_tx[0])-int(interface_stats_errors_tx[1])))/(abs(int(interface_stats_packets_tx[0])-int(interface_stats_packets_tx[1])))*100
                            else:
                                percentage_errors_tx = 0
                            self.write_double_metric('network-interface-tx-error-percentage',tags,'gauge', percentage_errors_tx, True, 'Network Interfaces - Throughput Transmit', 'percentage', 'vs.id|vs.name|name|type')
                            if abs(int(interface_stats_packets_rx[0])-int(interface_stats_packets_rx[1])) != 0 and percentage_rx > 0.1:
                                percentage_errors_rx = (abs(int(interface_stats_errors_rx[0])-int(interface_stats_errors_rx[1])))/(abs(int(interface_stats_packets_rx[0])-int(interface_stats_packets_rx[1])))*100
                            else:
                                percentage_errors_rx = 0
                            self.write_double_metric('network-interface-rx-error-percentage',tags,'gauge', percentage_errors_rx, True, 'Network Interfaces - Throughput Receive', 'percentage', 'vs.id|vs.name|name|type')
                        interface_stats_dropped_tx=[entry['tx_dropped'] for entry in data if entry['int_name']==interface]
                        interface_stats_dropped_rx=[entry['rx_dropped'] for entry in data if entry['int_name']==interface]
                        if len(interface_stats_dropped_tx) == 2 and len(interface_stats_dropped_rx) == 2:
                            tags ['type'] = 'Dropped percentage'
                            if abs(int(interface_stats_packets_tx[0])-int(interface_stats_packets_tx[1])) != 0 and percentage_tx > 0.1:
                                percentage_dropped_tx = (abs(int(interface_stats_dropped_tx[0])-int(interface_stats_dropped_tx[1])))/(abs(int(interface_stats_packets_tx[0])-int(interface_stats_packets_tx[1])))*100
                            else:
                                percentage_dropped_tx = 0
                            self.write_double_metric('network-interface-tx-dropped-percentage',tags,'gauge', percentage_dropped_tx, True, 'Network Interfaces - Throughput Transmit', 'percentage', 'vs.id|vs.name|name|type')
                            if abs(int(interface_stats_packets_rx[0])-int(interface_stats_packets_rx[1])) != 0 and percentage_rx > 0.1:
                                percentage_dropped_rx = (abs(int(interface_stats_dropped_rx[0])-int(interface_stats_dropped_rx[1])))/(abs(int(interface_stats_packets_rx[0])-int(interface_stats_packets_rx[1])))*100
                            else:
                                percentage_dropped_rx = 0
                            self.write_double_metric('network-interface-rx-dropped-percentage',tags,'gauge', percentage_dropped_rx, True, 'Network Interfaces - Throughput Receive', 'percentage', 'vs.id|vs.name|name|type')
        return self.output
