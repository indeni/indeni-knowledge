import os
import unittest

from checkpoint.multi_os.throughput_alert_vsx.throughput_alert_vsx import ChkpThroughputAlertVsx


class TestChkpThroughputAlertVsx(unittest.TestCase):

    def setUp(self):
        self.parser = ChkpThroughputAlertVsx()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_2vs_3int_and_no_file_stats(self):
        expected_results =[
            {'metric': 'network-interface-tx-util-percentage', 'name': 'VSID:0_eth0_1000Mb', 'vs.id':'0', 'vs.name': 'VSX-1', 'value': 5.86416e-03},
            {'metric': 'network-interface-rx-util-percentage', 'name': 'VSID:0_eth0_1000Mb', 'vs.id':'0', 'vs.name': 'VSX-1', 'value': 1.15344e-03},
            {'metric':'network-interface-tx-error-percentage', 'name': 'VSID:0_eth0_1000Mb', 'vs.id':'0', 'vs.name': 'VSX-1', 'value': 0.0},
            {'metric':'network-interface-rx-error-percentage', 'name': 'VSID:0_eth0_1000Mb', 'vs.id':'0', 'vs.name': 'VSX-1', 'value': 0.0},
            {'metric':'network-interface-tx-dropped-percentage', 'name': 'VSID:0_eth0_1000Mb', 'vs.id':'0', 'vs.name': 'VSX-1', 'value': 0.0},
            {'metric':'network-interface-rx-dropped-percentage', 'name': 'VSID:0_eth0_1000Mb', 'vs.id':'0', 'vs.name': 'VSX-1', 'value': 0.0},
            {'metric': 'network-interface-tx-util-percentage', 'name': 'VSID:0_eth1_1000Mb',  'vs.id':'0', 'vs.name': 'VSX-1', 'value': 0.0113096},
            {'metric': 'network-interface-rx-util-percentage', 'name': 'VSID:0_eth1_1000Mb',  'vs.id':'0', 'vs.name': 'VSX-1', 'value': 0.015176479999999998},
            {'metric':'network-interface-tx-error-percentage', 'name': 'VSID:0_eth1_1000Mb', 'vs.id':'0', 'vs.name': 'VSX-1', 'value': 0.0},
            {'metric':'network-interface-rx-error-percentage', 'name': 'VSID:0_eth1_1000Mb', 'vs.id':'0', 'vs.name': 'VSX-1', 'value': 0.0},
            {'metric':'network-interface-tx-dropped-percentage', 'name': 'VSID:0_eth1_1000Mb', 'vs.id':'0', 'vs.name': 'VSX-1', 'value': 0.0},
            {'metric':'network-interface-rx-dropped-percentage', 'name': 'VSID:0_eth1_1000Mb', 'vs.id':'0', 'vs.name': 'VSX-1', 'value': 0.0},
        ]

        result = self.parser.parse_file(self.current_dir + '/2vs_3int_and_no_file_stats.input', {}, {})
        self.assertEqual(12, len(result))

        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i]['name'], result[i].tags.get('name'))
            self.assertEqual(expected_results[i]['metric'], result[i].name)
            self.assertEqual(expected_results[i]['value'], result[i].value)


if __name__ == '__main__':
    unittest.main()