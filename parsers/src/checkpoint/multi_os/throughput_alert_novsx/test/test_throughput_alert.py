import os
import unittest

from checkpoint.multi_os.throughput_alert_novsx.throughput_alert_novsx import ChkpThroughputAlertNoVsx


class TestChkpThroughputAlertNoVsx(unittest.TestCase):

    def setUp(self):
        self.parser = ChkpThroughputAlertNoVsx()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_8int(self):

        expected_results =[
            {'metric': 'network-interface-tx-util-percentage', 'name': 'eth0_1000Mb', 'value': 0.06452808000000001},
            {'metric': 'network-interface-rx-util-percentage', 'name': 'eth0_1000Mb', 'value': 8.882080000000001e-03},
            {'metric':'network-interface-tx-error-percentage', 'name': 'eth0_1000Mb', 'value': 0.0},
            {'metric':'network-interface-rx-error-percentage', 'name': 'eth0_1000Mb', 'value': 0.0},
            {'metric':'network-interface-tx-dropped-percentage', 'name': 'eth0_1000Mb', 'value': 0.0},
            {'metric':'network-interface-rx-dropped-percentage', 'name': 'eth0_1000Mb', 'value': 0.0},
            {'metric': 'network-interface-tx-util-percentage', 'name': 'eth1_1000Mb', 'value': 0.0},
            {'metric': 'network-interface-rx-util-percentage', 'name': 'eth1_1000Mb', 'value': 3.74512e-03},
            {'metric':'network-interface-tx-error-percentage', 'name': 'eth1_1000Mb', 'value': 0.0},
            {'metric':'network-interface-rx-error-percentage', 'name': 'eth1_1000Mb', 'value': 0.0},
            {'metric':'network-interface-tx-dropped-percentage', 'name': 'eth1_1000Mb', 'value': 0.0},
            {'metric':'network-interface-rx-dropped-percentage', 'name': 'eth1_1000Mb', 'value': 0.0},
            {'metric': 'network-interface-tx-util-percentage', 'name': 'eth2_1000Mb', 'value': 3.62128e-03},
            {'metric': 'network-interface-rx-util-percentage', 'name': 'eth2_1000Mb', 'value': 3.48992e-03},
            {'metric':'network-interface-tx-error-percentage', 'name': 'eth2_1000Mb', 'value': 0.0},
            {'metric':'network-interface-rx-error-percentage', 'name': 'eth2_1000Mb', 'value': 0.0},
            {'metric':'network-interface-tx-dropped-percentage', 'name': 'eth2_1000Mb', 'value': 0.0},
            {'metric':'network-interface-rx-dropped-percentage', 'name': 'eth2_1000Mb', 'value': 0.0},
            {'metric': 'network-interface-tx-util-percentage', 'name': 'eth3_1000Mb', 'value': 3.2431999999999997e-04},
            {'metric': 'network-interface-rx-util-percentage', 'name': 'eth3_1000Mb', 'value': 0.00028671999999999997},
            {'metric':'network-interface-tx-error-percentage', 'name': 'eth3_1000Mb', 'value': 0.0},
            {'metric':'network-interface-rx-error-percentage', 'name': 'eth3_1000Mb', 'value': 0.0},
            {'metric':'network-interface-tx-dropped-percentage', 'name': 'eth3_1000Mb', 'value': 0.0},
            {'metric':'network-interface-rx-dropped-percentage', 'name': 'eth3_1000Mb', 'value': 0.0},
            {'metric': 'network-interface-tx-util-percentage', 'name': 'eth4_1000Mb', 'value': 0.00020831999999999998},
            {'metric': 'network-interface-rx-util-percentage', 'name': 'eth4_1000Mb', 'value': 2.5504e-04},
            {'metric':'network-interface-tx-error-percentage', 'name': 'eth4_1000Mb', 'value': 0.0},
            {'metric':'network-interface-rx-error-percentage', 'name': 'eth4_1000Mb', 'value': 0.0},
            {'metric':'network-interface-tx-dropped-percentage', 'name': 'eth4_1000Mb', 'value': 0.0},
            {'metric':'network-interface-rx-dropped-percentage', 'name': 'eth4_1000Mb', 'value': 0.0},
            {'metric': 'network-interface-tx-util-percentage', 'name': 'eth5_1000Mb', 'value': 0},
            {'metric': 'network-interface-rx-util-percentage', 'name': 'eth5_1000Mb', 'value': 0},
            {'metric':'network-interface-tx-error-percentage', 'name': 'eth5_1000Mb', 'value': 0.0},
            {'metric':'network-interface-rx-error-percentage', 'name': 'eth5_1000Mb', 'value': 0.0},
            {'metric':'network-interface-tx-dropped-percentage', 'name': 'eth5_1000Mb', 'value': 0.0},
            {'metric':'network-interface-rx-dropped-percentage', 'name': 'eth5_1000Mb', 'value': 0.0},
            {'metric': 'network-interface-tx-util-percentage', 'name': 'eth6_1000Mb', 'value': 0.00021791999999999997},
            {'metric': 'network-interface-rx-util-percentage', 'name': 'eth6_1000Mb', 'value': 2.08e-04},
            {'metric':'network-interface-tx-error-percentage', 'name': 'eth6_1000Mb', 'value': 0.0},
            {'metric':'network-interface-rx-error-percentage', 'name': 'eth6_1000Mb', 'value': 0.0},
            {'metric':'network-interface-tx-dropped-percentage', 'name': 'eth6_1000Mb', 'value': 0.0},
            {'metric':'network-interface-rx-dropped-percentage', 'name': 'eth6_1000Mb', 'value': 0.0},
            {'metric': 'network-interface-tx-util-percentage', 'name': 'bond0_2000Mb', 'value': 0},
            {'metric': 'network-interface-rx-util-percentage', 'name': 'bond0_2000Mb', 'value': 1.88808e-03},
            {'metric':'network-interface-tx-error-percentage', 'name': 'bond0_2000Mb', 'value': 0.0},
            {'metric':'network-interface-rx-error-percentage', 'name': 'bond0_2000Mb', 'value': 0.0},
            {'metric':'network-interface-tx-dropped-percentage', 'name': 'bond0_2000Mb', 'value': 0.0},
            {'metric':'network-interface-rx-dropped-percentage', 'name': 'bond0_2000Mb', 'value': 0.0},
        ]

        result = self.parser.parse_file(self.current_dir + '/8int.input', {}, {})
        self.assertEqual(48, len(result))

        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i]['name'], result[i].tags.get('name'))
            self.assertEqual(expected_results[i]['metric'], result[i].name)
            self.assertEqual(expected_results[i]['value'], result[i].value)

    def test_production(self):

        expected_results =[
            {'metric': 'network-interface-tx-util-percentage', 'name': 'eth3-02_10000Mb', 'value': 1.994524976},
            {'metric': 'network-interface-rx-util-percentage', 'name': 'eth3-02_10000Mb', 'value': 0.49476797600000005},
            {'metric': 'network-interface-tx-error-percentage', 'name': 'eth3-02_10000Mb', 'value': 0.0},
            {'metric': 'network-interface-rx-error-percentage', 'name': 'eth3-02_10000Mb', 'value': 0.0},
            {'metric': 'network-interface-tx-dropped-percentage', 'name': 'eth3-02_10000Mb', 'value': 0.0},
            {'metric': 'network-interface-rx-dropped-percentage', 'name': 'eth3-02_10000Mb', 'value': 0.0},
            {'metric': 'network-interface-tx-util-percentage', 'name': 'eth2-01_10000Mb', 'value': 1.497089184},
            {'metric': 'network-interface-rx-util-percentage', 'name': 'eth2-01_10000Mb', 'value': 3.1590624080000005},
            {'metric': 'network-interface-tx-error-percentage', 'name': 'eth2-01_10000Mb', 'value': 0.0},
            {'metric': 'network-interface-rx-error-percentage', 'name': 'eth2-01_10000Mb', 'value': 0.0},
            {'metric': 'network-interface-tx-dropped-percentage', 'name': 'eth2-01_10000Mb', 'value': 0.0},
            {'metric': 'network-interface-rx-dropped-percentage', 'name': 'eth2-01_10000Mb', 'value': 0.0},
            {'metric': 'network-interface-tx-util-percentage', 'name': 'eth1-04_1000Mb', 'value': 0.00541176},
            {'metric': 'network-interface-rx-util-percentage', 'name': 'eth1-04_1000Mb', 'value': 0.005605199999999999},
            {'metric': 'network-interface-tx-error-percentage', 'name': 'eth1-04_1000Mb', 'value': 0},
            {'metric': 'network-interface-rx-error-percentage', 'name': 'eth1-04_1000Mb', 'value': 0},
            {'metric': 'network-interface-tx-dropped-percentage', 'name': 'eth1-04_1000Mb', 'value': 0},
            {'metric': 'network-interface-rx-dropped-percentage', 'name': 'eth1-04_1000Mb', 'value': 0},
        ]

        result = self.parser.parse_file(self.current_dir + '/production.input', {}, {})
        self.assertEqual(18, len(result))

        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i]['name'], result[i].tags.get('name'))
            self.assertEqual(expected_results[i]['metric'], result[i].name)
            self.assertEqual(expected_results[i]['value'], result[i].value)

    def test_production_error(self):

        expected_results =[
            {'metric': 'network-interface-tx-error-percentage', 'name': 'eth2-01_10000Mb', 'value': 0.0},
            {'metric': 'network-interface-rx-error-percentage', 'name': 'eth2-01_10000Mb', 'value': 0.0},
            {'metric': 'network-interface-tx-dropped-percentage', 'name': 'eth2-01_10000Mb', 'value': 0.0},
            {'metric': 'network-interface-rx-dropped-percentage', 'name': 'eth2-01_10000Mb', 'value': 0.0},
            {'metric': 'network-interface-tx-util-percentage', 'name': 'eth1-04_1000Mb', 'value': 0.00541176},
            {'metric': 'network-interface-rx-util-percentage', 'name': 'eth1-04_1000Mb', 'value': 0.005605199999999999},
            {'metric': 'network-interface-tx-error-percentage', 'name': 'eth1-04_1000Mb', 'value': 0},
            {'metric': 'network-interface-rx-error-percentage', 'name': 'eth1-04_1000Mb', 'value': 0},
            {'metric': 'network-interface-tx-dropped-percentage', 'name': 'eth1-04_1000Mb', 'value': 0},
            {'metric': 'network-interface-rx-dropped-percentage', 'name': 'eth1-04_1000Mb', 'value': 0},
        ]

        result = self.parser.parse_file(self.current_dir + '/production_error.input', {}, {})
        self.assertEqual(10, len(result))

        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i]['name'], result[i].tags.get('name'))
            self.assertEqual(expected_results[i]['metric'], result[i].name)
            self.assertEqual(expected_results[i]['value'], result[i].value)

    def test_unknown_throughput(self):

        expected_results =[
            {'metric': 'network-interface-tx-util-percentage', 'name': 'Mgmt_1000Mb', 'value': 0.01479408}, 
            {'metric': 'network-interface-rx-util-percentage', 'name': 'Mgmt_1000Mb', 'value': 0.0002368}, 
            {'metric': 'network-interface-tx-error-percentage', 'name': 'Mgmt_1000Mb', 'value': 0}, 
            {'metric': 'network-interface-rx-error-percentage', 'name': 'Mgmt_1000Mb', 'value': 0}, 
            {'metric': 'network-interface-tx-dropped-percentage', 'name': 'Mgmt_1000Mb', 'value': 0}, 
            {'metric': 'network-interface-rx-dropped-percentage', 'name': 'Mgmt_1000Mb', 'value': 0}, 
            {'metric': 'network-interface-tx-util-percentage', 'name': 'eth1_1000Mb', 'value': 4.1845412799999995}, 
            {'metric': 'network-interface-rx-util-percentage', 'name': 'eth1_1000Mb', 'value': 16.5140392}, 
            {'metric': 'network-interface-tx-error-percentage', 'name': 'eth1_1000Mb', 'value': 0.0}, 
            {'metric': 'network-interface-rx-error-percentage', 'name': 'eth1_1000Mb', 'value': 0.0}, 
            {'metric': 'network-interface-tx-dropped-percentage', 'name': 'eth1_1000Mb', 'value': 0.0}, 
            {'metric': 'network-interface-rx-dropped-percentage', 'name': 'eth1_1000Mb', 'value': 0.0}, 
            {'metric': 'network-interface-tx-util-percentage', 'name': 'eth5_1000Mb', 'value': 0.16475711999999998}, 
            {'metric': 'network-interface-rx-util-percentage', 'name': 'eth5_1000Mb', 'value': 0.02975208}, 
            {'metric': 'network-interface-tx-error-percentage', 'name': 'eth5_1000Mb', 'value': 0.0}, 
            {'metric': 'network-interface-rx-error-percentage', 'name': 'eth5_1000Mb', 'value': 0}, 
            {'metric': 'network-interface-tx-dropped-percentage', 'name': 'eth5_1000Mb', 'value': 0.0}, 
            {'metric': 'network-interface-rx-dropped-percentage', 'name': 'eth5_1000Mb', 'value': 0}, 
            {'metric': 'network-interface-tx-util-percentage', 'name': 'eth2_1000Mb', 'value': 16.608640480000002}, 
            {'metric': 'network-interface-rx-util-percentage', 'name': 'eth2_1000Mb', 'value': 4.2135828}, 
            {'metric': 'network-interface-tx-error-percentage', 'name': 'eth2_1000Mb', 'value': 0.0}, 
            {'metric': 'network-interface-rx-error-percentage', 'name': 'eth2_1000Mb', 'value': 0.0}, 
            {'metric': 'network-interface-tx-dropped-percentage', 'name': 'eth2_1000Mb', 'value': 0.0}, 
            {'metric': 'network-interface-rx-dropped-percentage', 'name': 'eth2_1000Mb', 'value': 0.0}, 
            {'metric': 'network-interface-tx-util-percentage', 'name': 'eth6_1000Mb', 'value': 0.0171356}, 
            {'metric': 'network-interface-rx-util-percentage', 'name': 'eth6_1000Mb', 'value': 0.00662808}, 
            {'metric': 'network-interface-tx-error-percentage', 'name': 'eth6_1000Mb', 'value': 0}, 
            {'metric': 'network-interface-rx-error-percentage', 'name': 'eth6_1000Mb', 'value': 0}, 
            {'metric': 'network-interface-tx-dropped-percentage', 'name': 'eth6_1000Mb', 'value': 0}, 
            {'metric': 'network-interface-rx-dropped-percentage', 'name': 'eth6_1000Mb', 'value': 0}, 
            {'metric': 'network-interface-tx-util-percentage', 'name': 'eth3_1000Mb', 'value': 0.00969736}, 
            {'metric': 'network-interface-rx-util-percentage', 'name': 'eth3_1000Mb', 'value': 0.03256344}, 
            {'metric': 'network-interface-tx-error-percentage', 'name': 'eth3_1000Mb', 'value': 0}, 
            {'metric': 'network-interface-rx-error-percentage', 'name': 'eth3_1000Mb', 'value': 0}, 
            {'metric': 'network-interface-tx-dropped-percentage', 'name': 'eth3_1000Mb', 'value': 0}, 
            {'metric': 'network-interface-rx-dropped-percentage', 'name': 'eth3_1000Mb', 'value': 0}, 
            {'metric': 'network-interface-tx-util-percentage', 'name': 'eth7_1000Mb', 'value': 0.00020271999999999998}, 
            {'metric': 'network-interface-rx-util-percentage', 'name': 'eth7_1000Mb', 'value': 0.0001984}, 
            {'metric': 'network-interface-tx-error-percentage', 'name': 'eth7_1000Mb', 'value': 0}, 
            {'metric': 'network-interface-rx-error-percentage', 'name': 'eth7_1000Mb', 'value': 0}, 
            {'metric': 'network-interface-tx-dropped-percentage', 'name': 'eth7_1000Mb', 'value': 0}, 
            {'metric': 'network-interface-rx-dropped-percentage', 'name': 'eth7_1000Mb', 'value': 0}, 
            {'metric': 'network-interface-tx-util-percentage', 'name': 'eth4_1000Mb', 'value': 0.07024808}, 
            {'metric': 'network-interface-rx-util-percentage', 'name': 'eth4_1000Mb', 'value': 0.07866752}, 
            {'metric': 'network-interface-tx-error-percentage', 'name': 'eth4_1000Mb', 'value': 0}, 
            {'metric': 'network-interface-rx-error-percentage', 'name': 'eth4_1000Mb', 'value': 0}, 
            {'metric': 'network-interface-tx-dropped-percentage', 'name': 'eth4_1000Mb', 'value': 0}, 
            {'metric': 'network-interface-rx-dropped-percentage', 'name': 'eth4_1000Mb', 'value': 0}, 
            {'metric': 'network-interface-tx-util-percentage', 'name': 'eth4.993_1000Mb', 'value': 0.0}, 
            {'metric': 'network-interface-rx-util-percentage', 'name': 'eth4.993_1000Mb', 'value': 0.00016096}, 
            {'metric': 'network-interface-tx-error-percentage', 'name': 'eth4.993_1000Mb', 'value': 0}, 
            {'metric': 'network-interface-rx-error-percentage', 'name': 'eth4.993_1000Mb', 'value': 0}, 
            {'metric': 'network-interface-tx-dropped-percentage', 'name': 'eth4.993_1000Mb', 'value': 0}, 
            {'metric': 'network-interface-rx-dropped-percentage', 'name': 'eth4.993_1000Mb', 'value': 0}, 
            {'metric': 'network-interface-tx-util-percentage', 'name': 'eth4.994_1000Mb', 'value': 0.00021368}, 
            {'metric': 'network-interface-rx-util-percentage', 'name': 'eth4.994_1000Mb', 'value': 0.0006772799999999999}, 
            {'metric': 'network-interface-tx-error-percentage', 'name': 'eth4.994_1000Mb', 'value': 0}, 
            {'metric': 'network-interface-rx-error-percentage', 'name': 'eth4.994_1000Mb', 'value': 0}, 
            {'metric': 'network-interface-tx-dropped-percentage', 'name': 'eth4.994_1000Mb', 'value': 0}, 
            {'metric': 'network-interface-rx-dropped-percentage', 'name': 'eth4.994_1000Mb', 'value': 0}, 
            {'metric': 'network-interface-tx-util-percentage', 'name': 'eth4.991_1000Mb', 'value': 0.0341052}, 
            {'metric': 'network-interface-rx-util-percentage', 'name': 'eth4.991_1000Mb', 'value': 0.03765648}, 
            {'metric': 'network-interface-tx-error-percentage', 'name': 'eth4.991_1000Mb', 'value': 0}, 
            {'metric': 'network-interface-rx-error-percentage', 'name': 'eth4.991_1000Mb', 'value': 0}, 
            {'metric': 'network-interface-tx-dropped-percentage', 'name': 'eth4.991_1000Mb', 'value': 0}, 
            {'metric': 'network-interface-rx-dropped-percentage', 'name': 'eth4.991_1000Mb', 'value': 0}, 
            {'metric': 'network-interface-tx-util-percentage', 'name': 'eth4.997_1000Mb', 'value': 0.03572272}, 
            {'metric': 'network-interface-rx-util-percentage', 'name': 'eth4.997_1000Mb', 'value': 0.03489352}, 
            {'metric': 'network-interface-tx-error-percentage', 'name': 'eth4.997_1000Mb', 'value': 0}, 
            {'metric': 'network-interface-rx-error-percentage', 'name': 'eth4.997_1000Mb', 'value': 0}, 
            {'metric': 'network-interface-tx-dropped-percentage', 'name': 'eth4.997_1000Mb', 'value': 0}, 
            {'metric': 'network-interface-rx-dropped-percentage', 'name': 'eth4.997_1000Mb', 'value': 0}, 
        ]

        result = self.parser.parse_file(self.current_dir + '/unknown_throughput.input', {}, {})
        self.assertEqual(72, len(result))

        for i in range(len(expected_results)):
            self.assertEqual(expected_results[i]['name'], result[i].tags.get('name'))
            self.assertEqual(expected_results[i]['metric'], result[i].name)
            self.assertEqual(expected_results[i]['value'], result[i].value)


if __name__ == '__main__':
    unittest.main()