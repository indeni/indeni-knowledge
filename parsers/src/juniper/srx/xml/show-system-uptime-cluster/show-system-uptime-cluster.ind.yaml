name: junos-show-system-uptime-cluster
description: Retrieve system uptime, timezone and current date and time
type: monitoring
monitoring_interval: 5 minute
requires:
    vendor: juniper
    os.name: junos
    high-availability: true
comments:
    uptime-seconds:
        why: "Capture the uptime of the device. If the uptime is lower than the previous\
            \ sample, the device must have reloaded. \n"
        how: |
            This script logs into the Juniper JUNOS-based device using SSH and retrieves the output of the "show system uptime" command. The output includes the device's uptime as well as additional information.
        can-with-snmp: true
        can-with-syslog: true
    current-datetime:
        why: "Capture the current date and time of the device. Device current date\
            \ and time should never be more than 24 hours away from date and time\
            \ of the device polling the data, otherwise date and time are not correctly\
            \ set on device. \n"
        how: |
            This script logs into the Juniper JUNOS-based device using SSH and retrieves the current time using the output of the "show system uptime" command. The output includes the device's current date and time as well as configured time zone.
        can-with-snmp: true
        can-with-syslog: false
    timezone:
        why: "Capture the current time zone of the device. The time zone information\
            \ is useful for display purposes. \n"
        how: |
            This script logs into the Juniper JUNOS-based device using SSH and retrieves the configured time zone using the output of the "show system uptime" command. The output includes the device's current date and time as well as configured time zone.
        can-with-snmp: false
        can-with-syslog: false
steps:
-   run:
        type: SSH
        command: show chassis hardware node local | display xml
    parse:
        type: XML
        file: show-system-uptime-cluster.parser.1.xml.yaml
-   run:
        type: SSH
        command: show system uptime node ${node} | display xml
    parse:
        type: XML
        file: show-system-uptime-cluster.parser.2.xml.yaml
