BEGIN {
    node0 = 0
    node1 = 0
    feature_supported = 1
    node_sync_idx = 0
}

#error: syntax error, expecting <command>: informationconfiguration-synchronization
#the firmware is below 12.1X47
/^(errors:\s+syntax\s+error)/ {
    feature_supported = 0
}

#node0:
/^node0/ {
    node0++ 
}

#        Last sync result: Succeeded
#        Last sync result: Not needed 
/(Last sync result:)/ {
    split($0, get_status, ": ")
    if ( get_status[2] == "Succeeded" || get_status[2] == "Not needed" ){ 
        SyncStatus = 1
    } else {
        SyncStatus = 0
    }
    node_sync_status[node_sync_idx] = SyncStatus 
    node_sync_idx++
}

END {
    if ( feature_supported == 1 ) {
        if ( node0 == 2) {
            node_sync_idx = 0
            cluster_node["node"] = "node0"
        } else {
            node_sync_idx = 1
            cluster_node["node"] = "node1"
        }
        writeDoubleMetric("cluster-config-synced", cluster_node, "gauge", node_sync_status[node_sync_idx], "false")  # Converted to new syntax by change_ind_scripts.py script
    }
}