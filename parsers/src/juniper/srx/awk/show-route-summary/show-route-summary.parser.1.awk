BEGIN {
  total_routes = 0
}

#Limit/Threshold: 100/100 destinations 200/200 routes
/^(Limit\/Threshold:)/ {
    PrefixLimit = $2
    split(PrefixLimit, PrefixLimitArray, "/") 
    writeDoubleMetric("routes-limit", null, "gauge", PrefixLimitArray[1], "false")  # Converted to new syntax by change_ind_scripts.py script
}

#inet.0: 15 destinations, 22 routes (15 active, 0 holddown, 0 hidden)
/inet/ {
   inet_table = $1
   gsub("\:", "", inet_table )
   name_tag["name"] = inet_table 
   total_for_table = $4
   writeDoubleMetric("routes-usage", name_tag, "gauge", total_for_table, "false")  # Converted to new syntax by change_ind_scripts.py script
   total_routes = total_routes + total_for_table 
}

END {
   name_tag["name"] = "total-routes" 
   writeDoubleMetric("routes-usage", name_tag, "gauge", total_routes, "false")  # Converted to new syntax by change_ind_scripts.py script

}