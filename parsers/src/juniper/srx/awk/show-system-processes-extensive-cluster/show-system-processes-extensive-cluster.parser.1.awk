/^node/ {
    node_name = $1
    if ( node_name ~ /node0/ ) {
        node_idx = 0
    } else {
        node_idx = 1
    }
}

END {
    printf("VAR:node=%d",node_idx)
}