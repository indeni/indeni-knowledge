BEGIN {
    node0 = 0
    cluster = 0
    node_ukern = 0
    node_urt = 0
    node_mem_heap = 0
    node_mem_buffer = 0
}

#node0:
/^node0/ {
    node0++ 
    cluster = 1
}

# Microkernel CPU utilization        15 percent
/(Microkernel CPU utilization)/ {
    cpu_kernel[node_ukern] = $(NF-1)
    node_ukern++
}

# Real-time threads CPU utilization   0 percent
/(Real-time threads CPU utilization)/ {
    cpu_real_time[node_urt] = $(NF-1)
    node_urt++
}

# Heap utilization                   62 percent
/(Heap utilization)/ {
    memory_heap[node_mem_heap] = $(NF-1) 
    node_mem_heap++
}

# Buffer utilization                  1 percent 
/(Buffer utilization)/ {
    memory_buffer[node_mem_buffer] = $(NF-1)
    node_mem_buffer++
}

END {
    if ( cluster == 0 ) {
        node_idx = 0 
    } else {
        if (node0 == 2) {
            node_idx = 0
        } else {
            node_idx = 1
        }  
    }
    cpu_total_usage = cpu_kernel[node_idx] + cpu_real_time[node_idx]
    cpu_tag["name"] = "FWDD"
    cpu_tag["cpu-is-avg"] = "false"
    cpu_tag["resource-metric"] = "true"
    cpu_tag["im.dstype.displayType"] = "percentage"
    writeDoubleMetric("cpu-usage", cpu_tag, "gauge", cpu_kernel[node_idx], "false")  # Converted to new syntax by change_ind_scripts.py script

    mem_heap_tag["name"] = "FWDD Heap"
    mem_heap_tag["resource-metric"] = "true"
    mem_heap_tag["im.dstype.displayType"] = "percentage"
    writeDoubleMetric("memory-usage", mem_heap_tag, "gauge", memory_heap[node_idx], "false")  # Converted to new syntax by change_ind_scripts.py script

    mem_buffer_tag["name"] = "FWDD Buffer"
    mem_buffer_tag["resource-metric"] = "true"
    mem_buffer_tag["im.dstype.displayType"] = "percentage"
    writeDoubleMetric("memory-usage", mem_buffer_tag, "gauge", memory_buffer[node_idx], "false")  # Converted to new syntax by change_ind_scripts.py script
}