#domain-name gongya.net;
/^(domain-name \S+;)/ {
    domainName = $2
    sub(/;/, "", domainName)
    writeComplexMetricString("domain", null, domainName, "true", " Overview")  # Converted to new syntax by change_ind_scripts.py script
}