#####
# We first get the IKE gateways - so we can translate gateway name to IP
#####

#gateway srx220 {
/^gateway .* \{$/ {
    ikeGatewayName = $2
    inIkeGateway = "true"
}

#    address 192.168.1.20;
/^\s+address [0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3};/ {
    ipAddress = $2
    sub(/;/, "", ipAddress)

    if (inIkeGateway == "true") {
        gatewaysIP[ikeGatewayName] = ipAddress
        gatewaysMonitored[ikeGatewayName] = "false"
        gatewaysState[ikeGatewayName] = "0"
    }
}

#}
/^}$/ {
    inIkeGateway = "false"
}

######
# Parsing of ipsec, matching to IKE data.
######

#vpn ipsec-vpn-cfgr {
/vpn .* \{/ {
    inVpnIPsec = "true"
}

#vpn-monitor-options
/\s+vpn-monitor/ {
    vpnMonitorEnabled = "true"
}

#        gateway ike-gate-cfgr;
/\s+gateway \S+;/ {
    ikeGatewayRef = $2
    sub(/;/, "", ikeGatewayRef)
}

#}
/^}$/ {
    if (inVpnIPsec == "true" && vpnMonitorEnabled == "true") {
        gatewaysMonitored[ikeGatewayRef] = "true"
    }
    vpnMonitorEnabled = "false"
    inVpnIPsec = "false"
}

#####
# Parsing actual Phasee II tunnel status
#####
# <2    ESP:3des/md5    e12196b8 68188/unlim   -   root 500   128.208.90.2
# >2    ESP:3des/md5    9dfde7c2 68188/unlim   -   root 500   128.208.90.2
/(500\s+)[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}/ {
    peer = $(NF)
    for (gw in gatewaysIP){
         if ( gatewaysIP[gw] == peer ) {
             gatewaysState[gw] = "1" 
         }
    }  
}

END { 
    for (vpnPeer in gatewaysIP) {
        state = gatewaysState[vpnPeer]
        vpntags["name"] = "gateway " vpnPeer 
        vpntags["peerip"] = gatewaysIP[vpnPeer] 
        vpntags["always-on"] = gatewaysMonitored[vpnPeer]

        writeDoubleMetric("vpn-tunnel-state", vpntags, "gauge", state, "true", "VPN Tunnels - State", "state", "name")  # Converted to new syntax by change_ind_scripts.py script
    }
}