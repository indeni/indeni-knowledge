BEGIN {
    node0 = 0
    cluster = 0
    node_hostname = 0
    node_model = 0
    node_software = 0

    hardware_eos["srx110h2"] = date(2022, 03, 31)
    hardware_eos["srx110h2-vb"] = date(2022, 03, 31)
    hardware_eos["srx100"] = date(2021, 05, 01)
    hardware_eos["srx210"] = date(2021, 05, 01)
    hardware_eos["srx240"] = date(2021, 05, 01)
    hardware_eos["srx650"] = date(2021, 05, 01)
    hardware_eos["srx110h"] = date(2020, 11, 30)
    hardware_eos["srx110h-taa"] = date(2020, 11, 30)
    hardware_eos["srx210he"] = date(2020, 11, 30)
    hardware_eos["srx210he-taa"] = date(2020, 11, 30)
    hardware_eos["srx210he-poe-taa"] = date(2020, 11, 30)
    hardware_eos["srx240h-taa"] = date(2020, 11, 30)
    hardware_eos["srx240h-poe-taa"] = date(2020, 11, 30)
    hardware_eos["srx240h"] = date(2020, 11, 30)
    hardware_eos["srx100b"] = date(2019, 05, 10)
    hardware_eos["srx100h"] = date(2019, 05, 10)
    hardware_eos["srx110h-va"] = date(2019, 05, 10)
    hardware_eos["srx110h-vb"] = date(2019, 05, 10)
    hardware_eos["srx210be"] = date(2019, 05, 10)
    hardware_eos["srx210he"] = date(2019, 05, 10)
    hardware_eos["srx210he-poe"] = date(2019, 05, 10)
    hardware_eos["srx220h"] = date(2019, 05, 10)
    hardware_eos["srx220h-poe"] = date(2019, 05, 10)
    hardware_eos["srx240b"] = date(2019, 05, 10)
    hardware_eos["srx240b2"] = date(2019, 05, 10)
    hardware_eos["srx240h"] = date(2019, 05, 10)
    hardware_eos["srx240h-poe"] = date(2019, 05, 10)
    hardware_eos["srx240h-dc"] = date(2019, 05, 10)
    hardware_eos["srx210b"] = date(2017, 08, 31)
    hardware_eos["srx210h"] = date(2017, 08, 31)
    hardware_eos["srx210h-poe"] = date(2017, 08, 31)
    hardware_eos["srx210h-p-mgw"] = date(2011, 01, 24)
    hardware_eos["srx220h-p-mgw"] = date(2011, 01, 24)
    hardware_eos["srx240h-p-mgw"] = date(2011, 01, 24)

    software_eos["22.1"] = date(2024, 09, 24)
    software_eos["21.4"] = date(2025, 06, 25)
    software_eos["21.3"] = date(2024, 03, 23)
    software_eos["21.2"] = date(2024, 12, 29)
    software_eos["21.1"] = date(2023, 09, 25)
    software_eos["20.4"] = date(2024, 06, 25)
    software_eos["20.3"] = date(2023, 03, 29)
    software_eos["20.2"] = date(2023, 12, 30)
    software_eos["20.1"] = date(2022, 09, 27)
    software_eos["19.4"] = date(2023, 06, 26)
    software_eos["19.3"] = date(2023, 03, 26)
    software_eos["19.2"] = date(2022, 12, 26)
    software_eos["19.1"] = date(2022, 09, 27)
    software_eos["18.4"] = date(2022, 06, 22)
    software_eos["18.3"] = date(2022, 03, 26)
    software_eos["18.2"] = date(2021, 12, 29)
    software_eos["18.1"] = date(2021, 09, 28)
    software_eos["17.4R3"] = date(2022, 02, 25)
    software_eos["17.4R2"] = date(2022, 02, 25)
    software_eos["17.4"] = date(2021, 06, 21)
    software_eos["17.3R3"] = date(2022, 02, 25)
    software_eos["17.3"] = date(2021, 02, 25)
    software_eos["17.2"] = date(2020, 12, 06)
    software_eos["17.1"] = date(2020, 09, 03)
    software_eos["16.1R7"] = date(2021, 01, 28)
    software_eos["16.1"] = date(2020, 01, 28)
    software_eos["15.1X49"] = date(2020, 05, 01) 
    software_eos["15.1"] = date(2018, 12, 05) 
    software_eos["14.2"] = date(2018, 05, 05) 
    software_eos["14.1X5"] = date(2019, 06, 30) 
    software_eos["14.1"] = date(2018, 06, 13) 
    software_eos["13.3"] = date(2017, 07, 22) 
    software_eos["13.2X5"] = date(2017, 06, 30) 
    software_eos["13.2"] = date(2016, 02, 29) 
    software_eos["13.1X5"] = date(2015, 12, 30) 
    software_eos["13.1"] = date(2015, 09, 15) 
    software_eos["12.3X54"] = date(2018, 07, 18) 
    software_eos["12.3X52"] = date(2016, 02, 23) 
    software_eos["12.3X51"] = date(2015, 09, 15) 
    software_eos["12.3X50"] = date(2016, 07, 31) 
    software_eos["12.3X48"] = date(2022, 06, 30) 
    software_eos["12.31"] = date(2016, 07, 31) 
    software_eos["12.2X5"] = date(2015, 07, 31) 
    software_eos["12.2"] = date(2015, 03, 05)  
    software_eos["12.1X4"] = date(2015, 06, 30) 
    software_eos["12.1X47"] = date(2017, 02, 18) 
    software_eos["12.1X46"] = date(2017, 06, 30) 
    software_eos["12.1X45"] = date(2015, 01, 17) 
    software_eos["12.1X44"] = date(2016, 07, 18) 
    software_eos["12.1"] = date(2014, 09, 28) 
    software_eos["11.4"] = date(2015, 06, 21) 
    software_eos["11.3"] = date(2013, 03, 15) 
    software_eos["11.2"] = date(2013, 02, 15) 
    software_eos["11.1"] = date(2012, 05, 15) 
    software_eos["10.4"] = date(2014, 06, 08) 
    software_eos["10.3"] = date(2011, 12, 21) 
    software_eos["10.2"] = date(2011, 11, 15) 
    software_eos["10.1"] = date(2011, 05, 15) 
    software_eos["10.0"] = date(2013, 05, 15) 
    software_eos["9.6"] = date(2010, 11, 06) 
    software_eos["9.5"] = date(2010, 08, 15) 
    software_eos["9.4"] = date(2010, 05, 11) 
    software_eos["9.3"] = date(2012, 05, 15) 
    software_eos["9.2"] = date(2009, 11, 12) 
    software_eos["9.1"] = date(2009, 07, 28) 
    software_eos["9.0"] = date(2009, 05, 15) 
    software_eos["8.5"] = date(2011, 05, 16) 
    software_eos["8.4"] = date(2008, 11, 09) 
    software_eos["8.3"] = date(2008, 07, 18) 
    software_eos["8.2"] = date(2008, 05, 15) 
    software_eos["8.1"] = date(2010, 05, 06) 
    software_eos["8.0"] = date(2007, 11, 15) 
    software_eos["7.6"] = date(2007, 08, 15) 
    software_eos["7.5"] = date(2007, 05, 08) 
    software_eos["7.4"] = date(2007, 02, 15) 
    software_eos["7.3"] = date(2006, 11, 16) 
    software_eos["7.2"] = date(2006, 08, 14) 
    software_eos["7.1"] = date(2006, 05, 14) 
    software_eos["7.0"] = date(2006, 02, 15) 
    software_eos["6.4"] = date(2005, 11, 12) 
    software_eos["6.3"] = date(2005, 08, 15) 
    software_eos["6.2"] = date(2005, 05, 15) 
    software_eos["6.1"] = date(2005, 02, 15) 
    software_eos["6.0"] = date(2004, 11, 15) 
    software_eos["5.7"] = date(2004, 08, 15) 
    software_eos["5.6"] = date(2004, 05, 15) 
    software_eos["5.5"] = date(2004, 02, 15) 
    software_eos["5.4"] = date(2003, 11, 15) 
    software_eos["5.3"] = date(2003, 08, 15) 
    software_eos["5.2"] = date(2003, 05, 15) 
    software_eos["5.1"] = date(2003, 02, 15) 
    software_eos["5.0"] = date(2002, 11, 15) 
    software_eos["4.4"] = date(2002, 08, 15) 
    software_eos["4.3"] = date(2002, 05, 15) 
    software_eos["4.2"] = date(2002, 02, 15) 
    software_eos["4.1"] = date(2001, 11, 15) 
    software_eos["4.0"] = date(2001, 08, 15) 
}

#node0:
/^node0/ {
    node0++ 
    cluster = 1
}

#Hostname: SRX02
/^Hostname/ {
    hostname[node_hostname] = $2 
    node_hostname++
}

#Model: srx100b
/^Model/ {
    model[node_model] = $2
    node_model++
}

#JUNOS Software Release [12.1X46-D55.3]
/^(JUNOS Software Release)/ {
    software[node_software] = $4
    node_software++
}

END {
    if ( cluster == 0 ) {
        node_idx = 0 
    } else {
        if (node0 == 2) {
            node_idx = 0
        } else {
            node_idx = 1
        }  
    }
    gsub(/\[|\]/,"", software[node_idx]) 
    split(software[node_idx], software_version, "-")
    writeComplexMetricString("vendor", null, "Juniper", "false")  # Converted to new syntax by change_ind_scripts.py script
    writeComplexMetricString("os-name", null, "JUNOS", "false")  # Converted to new syntax by change_ind_scripts.py script
    writeComplexMetricString("model", null, model[node_idx], "false")  # Converted to new syntax by change_ind_scripts.py script
    writeComplexMetricString("hostname", null, hostname[node_idx], "false")  # Converted to new syntax by change_ind_scripts.py script
    writeComplexMetricString("os-version", null, software[node_idx], "false")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("software-eos-date", null, "gauge", software_eos[software_version[1]], "false")  # Converted to new syntax by change_ind_scripts.py script
    if ( model[node_idx] != "vsrx") {
        writeDoubleMetric("hardware-eos-date", null, "gauge", hardware_eos[model[node_idx]], "false")  # Converted to new syntax by change_ind_scripts.py script
    }
}