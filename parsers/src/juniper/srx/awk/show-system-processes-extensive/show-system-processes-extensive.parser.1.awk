#The function converts memory unit into KiloBytes
function parseBytes(_value) {
    _multiplier = 1
    if (_value ~ /M$/) {
        _multiplier = 1024 * 1024
    } else if (_value ~ /G$/) {
        _multiplier = 1024 * 1024 * 1024
    } else {
        _multiplier = 1024
    }

    _valueNumber = _value
    sub(/(M|G|K)/, "", _valueNumber)

    return _valueNumber * _multiplier
}

BEGIN { 
    # The management daemon (MGD) serves a central role in the user-interface component of JUNOS
    criticalprocess["mgd"] = "false"
    processdescription["mgd"] = "This process serves a centrol role in the user-interface component."
    # the routing protocols processes on a machine running logical routers
    criticalprocess["rpd"] = "false"
    processdescription["rpd"] = "This process serves a machine running logical routers." 
    # The chassis daemon (chassisd) supports all chassis, alarm, and environmental processes.
    criticalprocess["chassisd"] = "false"
    processdescription["chassisd"] = "This process supports all chassis, alarm, and environmental processes." 
    # This process is responsible for exchanging messages and doing failover between devices. 
    criticalprocess["jsrpd"] = "false"
    processdescription["jsrpd"] = "This process is responsible for exchanging messages and doing failover between devices." 
    # Junos OS runs PKId for certificate validation.
    criticalprocess["pkid"] = "false"
    processdescription["pkid"] = "This process verifies certificates." 
    
}

#Mem: 168M Active, 61M Inact, 224M Wired, 12M Cache, 61M Buf, 13M Free
/^Mem:/ {
    totalMem = 0
    for (i = 1; i <= NF; i++) {
        if ($i ~ /[0-9]+(M|G|K)/) {
            totalMem = totalMem + parseBytes($i)
        }
    }
}

#   PID USERNAME  THR PRI NICE   SIZE    RES STATE  C   TIME   WCPU COMMAND
/(PID|USERNAME|COMMAND)/ {
    getColumns(trim($0), "[ \t]+", columns)
}

# 1449 root        4  76    0   214M 45968K select 0 269:40 99.17% flowd_octeon
/root.*%/ {
    wcpuCol = getColId(columns, "WCPU")
    pidCol = getColId(columns, "PID")
    resCol = getColId(columns, "RES")
    processnameCol = getColId(columns, "COMMAND")

    cpu = $wcpuCol
    sub(/%/, "", cpu)

    memory = parseBytes($resCol) / totalMem * 100
    pid = $pidCol
    processname = $processnameCol
    command = $processnameCol
    for (i = processnameCol + 1; i <= NF; i++) {
        command = command " " $i
    }

    if (processname in criticalprocess && criticalprocess[processname] == "false") {
        criticalprocess[processname] = "true"
    }

    if (command !~ /^(flowd_octeon|idle.*)$/) {
        pstags["name"] = pid
        pstags["process-name"] = processname
        gsub(/,/, "", command)
        pstags["command"] = command

        writeDoubleMetric("process-cpu", pstags, "gauge", cpu, "false")  # Converted to new syntax by change_ind_scripts.py script
        writeDoubleMetric("process-memory", pstags, "gauge", memory, "false")  # Converted to new syntax by change_ind_scripts.py script
    }
} 

END {
    for (processname in criticalprocess) {
        if (criticalprocess[processname] == "false") {
            status = 0
        } else {
            status = 1
        }
        criticalpstags["process-name"] = processname
        criticalpstags["command"] = processname
        criticalpstags["description"] = processdescription[processname] 

        writeDoubleMetric("process-state", criticalpstags, "gauge", status, "false")  # Converted to new syntax by change_ind_scripts.py script
    }
}