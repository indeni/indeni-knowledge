BEGIN{
    telnet_enabled = "false"
    telnet_deactivated = 0
    http_enabled = "false"
    http_deactivated = 0
}

#set system services telnet
#deactivate system services telnet
/^(set|deactivate)(\s+system\s+services\s+telnet)/ { 
    telnet_service = $1
    if (telnet_service == "deactivate") {
        telnet_enabled = "false"
        telnet_deactivated = 1
    } else if (telnet_deactivated == 0) {
        telnet_enabled = "true"
    }
}

#set system services web-management http interface vlan.0
#deactivate system services web-management http interface vlan.0 
/^(set|deactivate)(\s+system\s+services\s+web-management\s+http)/ {
    http_service = $1
    if (http_service == "deactivate") {
        http_enabled = "false"
        http_deactivated = 1
    } else if (http_deactivated == 0) {
        http_enabled = "true"
    }
}


END {
   writeComplexMetricString("telnet-enabled", null, telnet_enabled, "false")  # Converted to new syntax by change_ind_scripts.py script
   writeComplexMetricString("http-server-enabled", null, http_enabled, "false")  # Converted to new syntax by change_ind_scripts.py script
}