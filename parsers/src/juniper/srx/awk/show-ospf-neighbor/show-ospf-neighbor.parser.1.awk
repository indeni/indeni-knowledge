# Address          Interface              State     ID               Pri  Dead
# 10.222.2.2       ge-0/0/11.0            Full      192.168.36.1     128    36

# Make sure line starts with some ID
/^[0-9\.]{4}/ {
    # Verify we have the number of columns we thought we would
    if (NF == 6) {
        name_tag["name"] = $4 " priority: " $5 " address: " $1
        state_desc = $3
        up = 0
        if (match(toupper(state_desc), "FULL|2WAY")) {
            up = 1
        }
        writeDoubleMetric("ospf-state", name_tag, "gauge", up, "true", "OSPF Neighbors", "state", "name")  # Converted to new syntax by change_ind_scripts.py script
    }
}