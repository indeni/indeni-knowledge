BEGIN {
    node0 = 0
    cluster = 0
    node_hostname = 0
    node_model = 0
    node_software = 0
    SRX = 0
}

#node0:
/^node0/ {
    node0++ 
    cluster = 1
}

#node1:
/^node1/ {
    cluster = 1
}

#Hostname: SRX02
/^Hostname/ {
    hostname[node_hostname] = $2 
    node_hostname++
}

#Model: srx100b
/^Model/ {
    model[node_model] = $2
    node_model++
    if ($2 ~ /srx/){
       SRX = 1 
    }
}

#JUNOS Software Release [12.1X46-D55.3]
/^(JUNOS Software Release)/ {
    software[node_software] = $4
    node_software++
}

END {
    if ( cluster == 0 ) {
        node_idx = 0 
    } else {
        if ( node0 == 2 ) {
            node_idx = 0
        } else {
            node_idx = 1
        }  
    } 
    if (SRX == 1)
    {
        gsub(/\[|\]/,"", software[node_idx]) 
        writeTag("hostname", hostname[node_idx]) 
        writeTag("model", model[node_idx]) 
        writeTag("os.name", "junos") 
        writeTag("product", "firewall") 
        writeTag("vendor", "juniper" ) 
        writeTag("os.version", software[node_idx]) 
    }
}