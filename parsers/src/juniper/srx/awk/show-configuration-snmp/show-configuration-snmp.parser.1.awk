BEGIN{
    snmp_enabled = "enabled"
    snmp_version = ""
}

#deactivate snmp
/^(deactivate snmp)$/ {
   snmp_enabled = "disabled"
}

#set snmp community admin authorization read-write
/^(set\s+snmp\s+community)/ {
   community_string[$4] = $NF
}

#deactivate snmp community public
/^(deactivate\s+snmp\s+community)/ {
   deactivated_community[$NF] = 1
}

#set snmp location "indeni lab"
/^(set\s+snmp\s+location)/ {
    split($0,a,"\"");
    snmp_location = a[2]
    writeComplexMetricString("snmp-location", null, snmp_location, "false")  # Converted to new syntax by change_ind_scripts.py script
}

#set snmp contact "indeni@indeni.org"
/^(set\s+snmp\s+contact)/ {
    split($0,a,"\"");
    snmp_contact = a[2]
    writeComplexMetricString("snmp-contact", null, snmp_contact, "false")  # Converted to new syntax by change_ind_scripts.py script
}

#set snmp v3 usm local-engine user gongya privacy-des privacy-key
/^(set\s+snmp\s+v3\s+usm\s+local-engine\s+user)/ {
   snmpv3_user[$7] = $7
}

#set snmp v3 usm remote-engine r-server user tom privacy-des privacy-key
/^(set\s+snmp\s+v3\s+usm\s+remote-engine\s+.*user)/ {
   snmpv3_user[$8] = $8
}

#set snmp v3 vacm security-to-group security-model usm security-name  gongya group v3group
/^(set\s+snmp\s+v3\s+vacm\s+security-to-group\s+security-model\s+usm\s+security-name)/ {
   snmpv3_group[$9] = $NF
}

#set snmp v3 vacm access group v3group default-context-prefix security-model any security-level privacy read-view snmpview1
/^(set\s+snmp\s+v3\s+vacm\s+access\s+group)/ {
   snmpv3_level[$7] = $12
}

#set snmp trap-group trap-server targets 192.168.1.225
/^(set\s+snmp\s+trap-group\s+.*targets)/ {
   if (targets_state[$NF] != "disabled") {
       targets_state[$NF] = "enabled"
       targets[$NF] = $4
   }
}

#deactivate snmp trap-group trap-server targets 192.168.1.225
/^(deactivate\s+snmp\s+trap-group\s+.*targets)/ {
   targets_state[$NF] = "disabled"
}

#set snmp trap-group trap-auth version v2
/^(set\s+snmp\s+trap-group\s+.*version)/ {
   snmp_trap_version[$4] = $NF
}

/^(set snmp trap-group\s+.*categories)/ {
   if (snmp_trap_item[$NF] != "disabled") {
      snmp_trap_item[$NF] = "enabled"
   }
}

/^(deactivate snmp trap-group\s+.*categories)/ {
   snmp_trap_item[$NF] = "disabled"
}

END{
    #snmp-enabled
    writeComplexMetricString("snmp-enabled", null, snmp_enabled, "false")  # Converted to new syntax by change_ind_scripts.py script

    #snmp-communities
    community_index = 1
    community_exist = 0
    for (var in community_string) {
        if (deactivated_community[var] != 1){
           community[community_index, "permissions"] = community_string[var]
           community[community_index, "community"] = var
           community_index++
           community_exist = 1
        }
    }

    unencrypted_snmp_configured = "false"
    if (community_exist == 1) {
        writeComplexMetricObjectArray("snmp-communities", null, community, "false")  # Converted to new syntax by change_ind_scripts.py script
        snmp_v1 = "v1"
        snmp_v2 = "v2"
        snmp_enabled = "enabled"
        unencrypted_snmp_configured = "true"
        writeComplexMetricString("unencrypted-snmp-configured", null, unencrypted_snmp_configured, "false")  # Converted to new syntax by change_ind_scripts.py script
    }

    # snmp_users
    user_index = 1
    for (v_user in snmpv3_user){
         snmpv3_users[user_index, "permissions"] = snmpv3_level[snmpv3_group[v_user]]
         snmpv3_users[user_index, "community"] = v_user
         snmpv3_enabled = 1
         user_index++
    }

    if (snmpv3_enabled == 1){
        snmp_v3 = "v3"
        writeComplexMetricObjectArray("snmp-users", null, snmpv3_users, "false")  # Converted to new syntax by change_ind_scripts.py script
    }

    #snmp-traps-receiver
    target_index = 1
    for (target in targets) {
        snmp_trap_target[target_index, "community"] = targets[target]
        snmp_trap_target[target_index, "ip"] = target
        snmp_trap_target[target_index, "version"] = snmp_trap_version[targets[target]]
        target_index++
    }

    if (target_index > 1){
        writeComplexMetricObjectArray("snmp-traps-receiver", null, snmp_trap_target, "false")  # Converted to new syntax by change_ind_scripts.py script
    }

    #snmp_trap_status
    item_index = 1
    for (item in snmp_trap_item) {
         snmp_trap_status[item_index, item] = snmp_trap_item[item]
         item_index++
         snmp_trap_enabled = 1
    }

    if (snmp_trap_enabled == 1) {
        writeComplexMetricObjectArray("snmp-traps-status", null, snmp_trap_status, "false")  # Converted to new syntax by change_ind_scripts.py script
    }

    #snmp_version
    if (snmp_enabled == "enabled" or snmpv3_enabled == 1){
        if (snmp_enabled == "enabled"){
            snmp_version = sprintf("%s|%s", snmp_v1,snmp_v2)
        }
        if (snmpv3_enabled == 1){
            if (snmp_enabled == "enabled"){
                snmp_version = sprintf("%s|%s", snmp_version,snmp_v3)
            }else{
                snmp_version = sprintf("%s", snmp_v3)
            }
        }
        writeComplexMetricString("snmp-version", null, snmp_version, "false")  # Converted to new syntax by change_ind_scripts.py script
    }
}