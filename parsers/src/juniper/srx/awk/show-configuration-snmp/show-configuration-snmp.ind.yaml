name: junos-show-configuration-snmp
description: JUNOS SRX retrieving snmp configuration information
type: monitoring
monitoring_interval: 60 minute
requires:
    vendor: juniper
    os.name: junos
    product: firewall
comments:
    snmp-enabled:
        why: |
            Capture whether SNMP is enabled on the device.
        how: |
            This script retrieves how the snmp is configured on the SRX device by running the command "show configuration snmp" via SSH connection to a device.
        can-with-snmp: false
        can-with-syslog: false
    snmp-version:
        why: |
            Capture the SNMP version enabled on the device.
        how: |
            This script retrieves how the snmp is configured on the SRX device by running the command "show configuration snmp" via SSH connection to a device.
        can-with-snmp: false
        can-with-syslog: false
    snmp-location:
        why: |
            Capture the SNMP location information. This field can be used to store real location information for the device.
        how: |
            This script retrieves how the snmp is configured on the SRX device by running the command "show configuration snmp" via SSH connection to a device.
        can-with-snmp: false
        can-with-syslog: false
    snmp-communities:
        why: |
            Capture the SNMP communities.If the default SNMP communities are configured, like "public" or "private" it could allow unauthorized clients to poll the device.
        how: |
            This script retrieves how the snmp is configured on the SRX device by running the command "show configuration snmp" via SSH connection to a device.
        can-with-snmp: false
        can-with-syslog: false
    snmp-traps-status:
        why: |
            Capture whether SNMP Traps are enabled or not.
        how: |
            This script retrieves how the snmp is configured on the SRX device by running the command "show configuration snmp" via SSH connection to a device.
        can-with-snmp: false
        can-with-syslog: false
    snmp-traps-receiver:
        why: |
            Capture SNMP Traps configuration.
        how: |
            This script retrieves how the snmp is configured on the SRX device by running the command "show configuration snmp" via SSH connection to a device.
        can-with-snmp: false
        can-with-syslog: false
    snmp-users:
        why: |
            Capture the SNMP users and permissions. SNMPv3 is the recommended SNMP version because of the additional security authentication and encryption mechanisms.
        how: |
            This script retrieves how the snmp is configured on the SRX device by running the command "show configuration snmp" via SSH connection to a device.
        can-with-snmp: false
        can-with-syslog: false
    unencrypted-snmp-configured:
        why: |
            SNMPv2c is an unsecure protocol and should not be used. Users should prefer the more secure SNMPv3.
        how: |
            This script retrieves how the snmp is configured on the SRX device by running the command "show configuration snmp" via SSH connection to a device.
        can-with-snmp: false
        can-with-syslog: false
    snmp-contact:
        why: |
            Capture contact details. If the wrong contact is specified in the SNMP settings, the network monitoring team might contact the wrong person or team when there is an issue.
        how: |
            This script retrieves how the snmp is configured on the SRX device by running the command "show configuration snmp" via SSH connection to a device.
        can-with-snmp: false
        can-with-syslog: false
steps:
    -   run:
            type: SSH
            command: show configuration snmp | display set
        parse:
            type: AWK
            file: show-configuration-snmp.parser.1.awk
