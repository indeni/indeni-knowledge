BEGIN {
    node0 = 0
    cluster = 0
    node_idx = 0
    file_idx = 1
    node_core_files[0] = 0
    node_core_files[1] = 0
    month["Jan"] = 1
    month["Feb"] = 2
    month["Mar"] = 3
    month["Apr"] = 4
    month["May"] = 5
    month["Jun"] = 6
    month["Jul"] = 7
    month["Aug"] = 8
    month["Sep"] = 9
    month["Oct"] = 10
    month["Nov"] = 11
    month["Dec"] = 12
}

#Current time: 2017-06-10 06:29:10 UTC
/^Current/ {
    cur_date = $3
    split(cur_date, YMD, "-")
    created_year = YMD[1]
}

#node0:
/^node0/ {
    node0++ 
    cluster = 1
}

#node1:
/^node1/ {
    if ( node0 > 0 ) {
        node_idx++
        file_idx = 1
    }
    cluster = 1
}

#-rw-r--r--  1 root  wheel          0 May 21 16:58 /var/tmp/vmcore.0
/^(-r)/ {
    core_file = $NF
    created_month = month[$(NF-3)]
    created_date  = $(NF-2)
    created_time  = $(NF-1)
    if ( created_time !~ /:/ ) {
        created_year = created_time
        created_hour = 0
        created_minute = 0
    } else {
        split(created_time, HM, ":")
        created_hour = HM[1]
        created_minute = HM[2]
    }

    created_time = datetime(created_year, created_month, created_date, created_hour, created_minute, 0)
    if ( core_file !~ /\// ) {
        core_file = "/var/crash/corefiles/"core_file
    }
    if ( node_idx == 0 ) {
        node0_core_files[file_idx,"path"] = core_file
        node0_core_files[file_idx,"created"] = created_time 
        
    } else {
        node1_core_files[file_idx,"path"] = core_file
        node1_core_files[file_idx,"created"] = created_time 
    }
    file_idx++
}

END {
    if ( file_idx > 1) { 
        if ( cluster == 1 ) {
            if ( node0 == 2 ) {
                node_idx = 0
                cluster_node["node"] = "node0"
                writeComplexMetricObjectArray("core-dumps", null, node0_core_files, "false")  # Converted to new syntax by change_ind_scripts.py script
            } else {
                node_idx = 1
                cluster_node["node"] = "node1"
                writeComplexMetricObjectArray("core-dumps", null, node1_core_files, "false")  # Converted to new syntax by change_ind_scripts.py script
            }
        } else {
            node_idx = 0
            cluster_node["node"] = "standalone"
            writeComplexMetricObjectArray("core-dumps", null, node0_core_files, "false")  # Converted to new syntax by change_ind_scripts.py script
        }
    }
}