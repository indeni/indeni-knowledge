/^(message ")/ {
    message = trim($0)
    sub(/^(message )/, "", message)
    sub(/;$/, "", message)
}

END {
  gsub(/["]/, "", message)
  writeComplexMetricString("login-banner", null, message, "false")  # Converted to new syntax by change_ind_scripts.py script
}