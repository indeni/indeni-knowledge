BEGIN{
    ntp_server_index = 1
}
#Match lines that contains digits
#*10.10.10.144  193.204.114.233  2 u   42   64   37    0.193   -7.375   9.959
/^[\#\+\*o][1-9]|^(\s[0-9])/ {
    serverIP = $1
	
    if ( serverIP ~ /^[\*]/ ) {
        ntp_servers[ntp_server_index, "type"] = "primary" 
    } else if ( serverIP ~ /^[\+]/ ) {
        ntp_servers[ntp_server_index, "type"] = "secondary" 
    } else {
        ntp_servers[ntp_server_index, "type"] = "other" 
    }
   
    #Remove the first character if it's not a number
    sub(/^[\#\+\*o]/, "", serverIP)
    ntp_servers[ntp_server_index, "ipaddress"] = serverIP
    ntp_server_index++	
    #Extract the ipaddress
    ntpServerTags["name"] = serverIP
        	
    #Rows that starts with the following contains failed ntp servers
    #" "	non-communicating remote machines,
    #	"LOCAL" for this local host,
    #	(unutilised) high stratum servers,
    #	remote machines that are themselves using this host as their synchronisation reference;
    #Rows that starts with the following contains functioning ntp servers
    #"#" 	Good remote peer or server but not utilised (not among the first six peers sorted by synchronization distance, ready as a backup source);
    #"+"	Good and a preferred remote peer or server (included by the combine algorithm);
    #"*"	The remote peer or server presently used as the primary reference;
    #"o"	PPS peer (when the prefer peer is valid). The actual system synchronization is derived from a pulse-per-second (PPS) signal, either indirectly via the PPS reference clock driver or directly via kernel interface.
    #Source:
    #http://nlug.ml1.co.uk/2012/01/ntpq-p-output/831
	
    if( match($0, /^[\#\+\*o]/) ) {
        state = 1
    } else {
        state = 0
    }	
    writeDoubleMetric("ntp-server-state", ntpServerTags, "gauge", state, "true", "NTP Servers", "state", "name")  # Converted to new syntax by change_ind_scripts.py script
}

#set system ntp server 140.142.1.9
/^(set\s+system\s+ntp\s+server)/ {
    configured_line = $0
    ntp_server = $5
    if (configured_line ~ /version/) {
        ntp_server_version[ntp_server] = $NF 
    } else {  
        ntp_server_version[ntp_server] = "4" 
    }
}

END{
    if ( ntp_server_index > 1 ) {
        for ( i=1; i < ntp_server_index; i++ ) {
            ntp_servers[i, "version"] = ntp_server_version[ntp_servers[i, "ipaddress"]]
        }
        writeComplexMetricObjectArray("ntp-servers", null, ntp_servers, "false")  # Converted to new syntax by change_ind_scripts.py script
    }
}