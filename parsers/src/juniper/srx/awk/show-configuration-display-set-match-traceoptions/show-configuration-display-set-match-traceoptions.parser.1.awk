#set security ike traceoptions flag all
#deactivate security ike traceoptions
/^(set|deactivate)/{

    line = $0
    split(line, words, " traceoptions")
    if (words[1] ~ /deactivate/) {
           debug_status = 0
           gsub("deactivate ", "", words[1])
    } else {
           debug_status = 1
           gsub("set ", "", words[1])
    }
    if (debug_component[words[1]] != "0") {
          debug_component[words[1]] = debug_status 
    }
}

END{
     for (var in debug_component){
         debug_item["name"] = var
         debug_new = debug_component[var]
         writeDoubleMetric("debug-status", debug_item, "gauge", debug_new, "false")  # Converted to new syntax by change_ind_scripts.py script
     }
}