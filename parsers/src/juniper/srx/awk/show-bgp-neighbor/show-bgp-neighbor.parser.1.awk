#128.208.95.1          65007        536        528       0       0     3:55:44 0/6/5/0              0/0/0/0
/^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}/ {

    nameTag["name"]  = $1;
    if ($0 ~ /Active/ || $0 ~ /Connect/ || $0 ~ /Idle/) {
        peerState = "0"
    } else {
        peerState = "1"
    }
    writeDoubleMetric("bgp-state", nameTag, "gauge", peerState, "true", "BGP Neighbor", "state", "name")  # Converted to new syntax by change_ind_scripts.py script

    #99.99.99.2              555         35         38       0       0       12:05 Establ
    bgpState = $(NF)
    if (peerState == "1" && bgpState !~ /Establ/) {
        routes = $(NF-1)
        split (routes, route, "/")
        writeDoubleMetric("bgp-received-routes", nameTag, "number", route[2], "true", "BGP Neighbor", "number", "name")  # Converted to new syntax by change_ind_scripts.py script
    } 
}

#  bgp.evpn.0: 9/9/9/0
/^(\s \w)/ {
    bgpTable = $1
    gsub(/\:/, "", bgpTable)
    logicalSystemNameTag["name"] = nameTag["name"]
    logicalSystemNameTag["name"] = logicalSystemNameTag["name"] ":" bgpTable
    routes = $(NF)
    split (routes, route, "/")
    writeDoubleMetric("bgp-received-routes", logicalSystemNameTag, "number", route[2], "true", "BGP Neighbor", "number", "name")  # Converted to new syntax by change_ind_scripts.py script
}