BEGIN {
    node0 = 0
    node1 = 0
    cluster = 0 
    node_tag["name"] = "standalone"
}
 
#node0:
/^(node0)/{
   node0++
   cluster = 1
   if (node0 == 2) {
       node_tag["name"] = "node0"
   }
}

#node1:
/^(node1)/{
   node1++
   if (node0 == 2) {
       node0 = 1
   } else {
       node_tag["name"] = "node1"
   }
}

#IPv4  Session Creation Per Second (for last 96 seconds on average):  128
/^(IPv4\s+Session\s+Creation\s+Per\s+Second)/{
    if (cluster == 0 || node0 == 2 || node1 == 2) {
        CPS = $NF 
        writeDoubleMetric("connections-per-second", node_tag, "gauge", CPS, "false")  # Converted to new syntax by change_ind_scripts.py script
    }
}