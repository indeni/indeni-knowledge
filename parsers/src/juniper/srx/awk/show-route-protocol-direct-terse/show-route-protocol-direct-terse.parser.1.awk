#inet.0: 12 destinations, 12 routes (12 active, 0 holddown, 0 hidden)
/inet/ {
  table_name = $1
}

#* 20.20.20.0/24      D   0                       >lt-0/0/0.4
/^(\*\s+[0-9]|\+\s+[0-9]|\-\s+[0-9])/ {
    line++
    network = $2
    split(network, network_prefix, "/")
    route[line, "table-name"] = table_name
    route[line, "network"] = network_prefix[1]
    route[line, "mask"] = network_prefix[2]  
}

END{
    writeComplexMetricObjectArray("connected-networks-table", null, route, "false")  # Converted to new syntax by change_ind_scripts.py script
}