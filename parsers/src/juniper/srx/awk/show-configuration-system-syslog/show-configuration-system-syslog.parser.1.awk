#set system syslog host 192.168.1.56 any critical
/^(set\s+system\s+syslog\s+host)/{
    host = $(NF - 2)
    facility = $(NF-1)
    severity = $NF 
    syslog_server[idx_1,"host"] = host 
    syslog_server[idx_1,"severity"] = severity 
    idx_1++
}

END{
    writeComplexMetricObjectArray("syslog-servers", null, syslog_server, "false")  # Converted to new syntax by change_ind_scripts.py script
}