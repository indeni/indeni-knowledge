# 18:37:53 up 18 days,  8:54,  2 users,  load average: 0.35, 0.28, 0.20
#USER     TTY      FROM                              LOGIN@  IDLE WHAT
#indeni   u0       -                                1:47PM   3:55 -cli (cli)    
#indeni   p0       172.16.16.38                     5:04PM      - -cli (cli)    
#indeni   p1       172.16.16.38                     3:38PM   2:03 -cli (cli)    
/^(?!\s|USER\s+TTY\s+FROM\s+LOGIN@\s+IDLE\s+WHAT)/ {

	username = $1
	from = $3
	idle = $5
	tty = $2	
	iuser++
	users[iuser, "username"] = username
	users[iuser, "from"] = from
	if ( from == "-" ) {
		users[iuser, "from"] = "console"
	}
	# 2days
	secondsIdle = 0
	if ( idle ~ /days/ ) {
		split (idle, idleArr, "days")
		secondsIdle = idleArr[1] * 86400
	} else if ( idle ~ /:/ ) {
		split (idle, idleArr, ":")
		secondsIdle = idleArr[1] * 60 + idleArr[2]
	} else {
		secondsIdle = idleArr[1]

	} 
	users[iuser, "idle"] = secondsIdle
}

END {
    writeComplexMetricObjectArray("logged-in-users", null, users, "false")  # Converted to new syntax by change_ind_scripts.py script
}