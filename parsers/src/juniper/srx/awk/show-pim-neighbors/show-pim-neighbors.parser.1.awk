# Interface           IP V Mode        Option      Uptime Neighbor addr
# so-1/0/0.0           4 2             HPLG      00:07:10 10.111.10.2

# Make sure line ends with some ID
/[0-9\.]{4}$/ {
    # Verify we have the number of columns we thought we would
    if (NF == 6) {
        name_tag["name"] = $6

        up = 1  # if it's in the list, it's up

        writeDoubleMetric("pim-state", name_tag, "gauge", up, "true", "PIM Neighbors", "state", "name")  # Converted to new syntax by change_ind_scripts.py script
    }
}