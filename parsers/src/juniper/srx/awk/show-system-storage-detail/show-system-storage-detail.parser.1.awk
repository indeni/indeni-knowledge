BEGIN {
    node0 = 0
    node1 = 0
    cluster = 0
    # List of uninteresting mount points for JUNOS specifically:
    uninterestingmounts["/dev"] = "true"
    uninterestingmounts["/jail/dev"] = "true"
    uninterestingmounts["/junos"] = "true"
    uninterestingmounts["/junos/cf/dev"] = "true"
    uninterestingmounts["/junos/dev/"] = "true"
    uninterestingmounts["/proc"] = "true"
}

#node0:
/^node0/ {
    node0++ 
    cluster = 1
}

#node1:
/^node1/ {
    node1++
    cluster = 1
    if (node0 == 2) {
        node0 = 1
    }
}

#/dev/sda1  295561     24017    256284   9% /boot
/(\d+)%/ {
    mount = trim($NF)

    if (cluster == 0 || node0 == 2 || node1 == 2) {
        if (!uninterestingmounts[mount]) {
            usage = $(NF-1)
            sub(/%/, "", usage)
            available = $(NF-2)
            used = $(NF-3)
    	    total = $(NF-4)

            mounttags["file-system"] = mount

            writeDoubleMetric("disk-usage-percentage", mounttags, "gauge", usage, "true", "Mount Points - Usage", "percentage", "file-system")  # Converted to new syntax by change_ind_scripts.py script
            writeDoubleMetric("disk-used-kbytes", mounttags, "gauge", used, "true", "Mount Points - Used", "kbytes", "file-system")  # Converted to new syntax by change_ind_scripts.py script
            writeDoubleMetric("disk-total-kbytes", mounttags, "gauge", total, "true", "Mount Points - Total", "kbytes", "file-system")  # Converted to new syntax by change_ind_scripts.py script
        }  
    }
}