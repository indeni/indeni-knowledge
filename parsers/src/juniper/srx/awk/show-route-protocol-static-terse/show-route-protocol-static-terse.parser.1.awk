#inet.0: 13 destinations, 13 routes (13 active, 0 holddown, 0 hidden)
/inet/ {
  table_name = $1
}

#* 30.30.30.0/24      D   0                       >lt-0/0/0.1
/^(\*\s+[0-9]|\+\s+[0-9]|\-\s+[0-9])/ {
    line++
    network = $2
    next_hop = $NF
    split(network, network_prefix, "/")
    route[line, "table-name"] = table_name
    route[line, "network"] = network_prefix[1]
    route[line, "mask"] = network_prefix[2]  
    gsub(/\>/, "", next_hop)
    route[line, "next-hop"] = next_hop
}

END{
    writeComplexMetricObjectArray("static-routing-table", null, route, "false")  # Converted to new syntax by change_ind_scripts.py script
}