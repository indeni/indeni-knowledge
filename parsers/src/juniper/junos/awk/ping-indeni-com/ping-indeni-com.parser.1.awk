BEGIN {
    dns_success = 0
    cluster = 0
    node_local = 0
    run_ping = 0
}

#node0 
##node0  255      primary        no      yes      None
/^node0/ {
    cluster = 1
    node = $1
    if ( node_local == 1 ) {
        node_role[0] = $3
    }

    if ( node_local == 0 && node ~ /node0/ ){
        node_local = 1
        myself = 0
    } 
}

#node1
#node1  1        secondary      no      yes      None
/^node1/ {
    node = $1
    if ( node_local == 1 ) {
        node_role[1] = $3
    }

    if ( node_local == 0 && node ~ /node1/ ) {
        node_local = 1
        myself = 1
    } 
}

# Let's see if it managed to translate the domain to IP:
# PING indeni.com (109.199.106.156) 56(84) bytes of data.
/PING indeni.com \([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\)/ {
    dns_success = 1 
}

END {
    if ( cluster == 0 ) {
        run_ping = 1
    } else if (cluster == 1 && node_role[myself] == "primary") {
         run_ping = 1
    }
    #if the node is secondary, do not write metric to avoid alerts
    if ( run_ping == 1 ) {
        dnstags["name"] = "configured DNS server"
        writeDoubleMetric("dns-server-state", dnstags, "gauge", dns_success, "true", "DNS Servers - State", "state", "name")  # Converted to new syntax by change_ind_scripts.py script
    }
}