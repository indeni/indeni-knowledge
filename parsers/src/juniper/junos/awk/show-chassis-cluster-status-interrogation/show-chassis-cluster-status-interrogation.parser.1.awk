BEGIN {
    iscluster = "false"
}

#Redundancy group: 0 , Failover count: 1
/Redundancy group/ {
    iscluster = "true"
}

END {
    writeTag("high-availability", iscluster)
}