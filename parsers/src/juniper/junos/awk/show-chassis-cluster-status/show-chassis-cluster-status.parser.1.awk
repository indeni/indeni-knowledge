BEGIN {
    RG = 0
}

#Node   Priority Status         Preempt Manual   Monitor-failures
/^Node.*Priority*/ {
    getColumns(trim($0), "[ \t]+", columns)
}

#Redundancy group: 0 , Failover count: 1
/^Redundancy group/ {
    regroup = $3
    group_state[regroup] = 0
    group_preempt [regroup] = 0
    RG = 1
    node_idx = 0
    cluster_tags["name"] = "redundancy group "regroup
}

#node0  1        primary        no      no       None           
/^node.*/ {
    if (RG == 0) {
       node_local = $1
       if (node_local ~ /node0/){
           myself = 0
       } else {
           myself = 1
       }
    } else {
        node = $getColId(columns, "Node")
        if (node == "node0") {
            node_idx == 0
        }else {
            node_idx = 1
        }

        statusDesc = $getColId(columns, "Status")
        monitor_failures = $getColId(columns, "Monitor-failures")

        if ( node_idx == myself ) {
            if ((statusDesc == "primary" && monitor_failures == "None") || (statusDesc == "secondary" && monitor_failures == "None")) {
                node_status[node_idx] = 1
            } else {
                node_status[node_idx] = 0
            }
            writeDoubleMetric("cluster-member-active", cluster_tags, "gauge", node_status[myself], "true", "Cluster Member Active", "state", "name")  # Converted to new syntax by change_ind_scripts.py script
        }
        node_idx++

        if (statusDesc == "primary" && monitor_failures == "None") {
            # either of nodes is primary, the state for this redundancy group is up
            group_state[regroup] = 1 
        }

        preempt = $getColId(columns, "Preempt")
        if (preempt == "yes") {
            group_preempt[regroup] = 1
        }
    }
}

END {
        for (regroup in group_state) {
            cluster_tags["name"] = "redundancy group "regroup
            writeDoubleMetric("cluster-state", cluster_tags, "gauge", group_state[regroup], "true", "Cluster State", "state", "name")  # Converted to new syntax by change_ind_scripts.py script
            writeDoubleMetric("cluster-preemption-enabled", cluster_tags, "gauge", group_preempt[regroup], "true", "Cluster Preemption Enabled", "boolean", "name")  # Converted to new syntax by change_ind_scripts.py script
        }
}