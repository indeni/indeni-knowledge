import os
import unittest

from zscaler.application_reachability_monitor.application_reachability_monitor import ApplicationReachabilityMonitor
from parser_service.public.action import *

expected_result_alarmed = [{'metric': 'zpa-registered-apps', 'tags': {'im.identity-tags': 'name', 'name': 'Registered Apps', 'live-config': 'true', 'display-name': 'ZPA Connector - Application Reachability'}, 'value': 1},
    {'metric': 'zpa-alive-apps', 'tags': {'im.identity-tags': 'name', 'name': 'Alive Apps', 'live-config': 'true', 'display-name': 'ZPA Connector - Application Reachability'}, 'value': 1},
    {'metric': 'zpa-apps-on-access-health', 'tags': {'im.identity-tags': 'name', 'name': 'On-Access Health Apps', 'live-config': 'true', 'display-name': 'ZPA Connector - Application Reachability'}, 'value': 1},
    {'metric': 'zpa-service-count', 'tags': {'im.identity-tags': 'name', 'name': 'Service Count', 'live-config': 'true', 'display-name': 'ZPA Connector - Application Reachability'}, 'value': 0},
    {'metric': 'zpa-target-count', 'tags': {'im.identity-tags': 'name', 'name': 'Target Count', 'live-config': 'true', 'display-name': 'ZPA Connector - Application Reachability'}, 'value': 6002},
    {'metric': 'zpa-target-alive', 'tags': {'im.identity-tags': 'name', 'name': 'Target Alive', 'live-config': 'true', 'display-name': 'ZPA Connector - Application Reachability'}, 'value': 3466}]

expected_result_not_alarmed = [{'metric': 'zpa-registered-apps', 'tags': {'im.identity-tags': 'name', 'name': 'Registered Apps', 'live-config': 'true', 'display-name': 'ZPA Connector - Application Reachability'}, 'value': 1},
    {'metric': 'zpa-alive-apps', 'tags': {'im.identity-tags': 'name', 'name': 'Alive Apps', 'live-config': 'true', 'display-name': 'ZPA Connector - Application Reachability'}, 'value': 1},
    {'metric': 'zpa-apps-on-access-health', 'tags': {'im.identity-tags': 'name', 'name': 'On-Access Health Apps', 'live-config': 'true', 'display-name': 'ZPA Connector - Application Reachability'}, 'value': 1},
    {'metric': 'zpa-service-count', 'tags': {'im.identity-tags': 'name', 'name': 'Service Count', 'live-config': 'true', 'display-name': 'ZPA Connector - Application Reachability'}, 'value': 0},
    {'metric': 'zpa-target-count', 'tags': {'im.identity-tags': 'name', 'name': 'Target Count', 'live-config': 'true', 'display-name': 'ZPA Connector - Application Reachability'}, 'value': 3466},
    {'metric': 'zpa-target-alive', 'tags': {'im.identity-tags': 'name', 'name': 'Target Alive', 'live-config': 'true', 'display-name': 'ZPA Connector - Application Reachability'}, 'value': 3466}]

class TestApplicationReachabilityMonitor(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = ApplicationReachabilityMonitor()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_alarmed(self):
        result = self.parser.parse_file(self.current_dir + '/alarmed.input', {}, {})
        self.assertEqual(6, len(result))

        for i in range(len(expected_result_alarmed)):
            self.assertEqual(expected_result_alarmed[i]['tags'], result[i].tags)
            self.assertEqual(expected_result_alarmed[i]['metric'], result[i].name)
            self.assertEqual(expected_result_alarmed[i]['value'], result[i].value)

    def test_not_alarmed(self):
        result = self.parser.parse_file(self.current_dir + '/not_alarmed.input', {}, {})
        self.assertEqual(6, len(result))

        for i in range(len(expected_result_not_alarmed)):
            self.assertEqual(expected_result_not_alarmed[i]['tags'], result[i].tags)
            self.assertEqual(expected_result_not_alarmed[i]['metric'], result[i].name)
            self.assertEqual(expected_result_not_alarmed[i]['value'], result[i].value)

if __name__ == '__main__':
    unittest.main()