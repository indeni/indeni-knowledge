from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class ApplicationReachabilityMonitor(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            reachability_stats = helper_methods.parse_data_as_list(raw_data, 'application_reachability_monitor.textfsm')
            if reachability_stats:
                tags = {}
                tags['im.identity-tags'] = 'name'
                tags['name'] = 'Registered Apps'
                self.write_double_metric('zpa-registered-apps', tags, 'gauge', int(reachability_stats[0]['registered_apps']), True,
                                                                'ZPA Connector - Application Reachability')
                tags['name'] = 'Alive Apps'
                self.write_double_metric('zpa-alive-apps', tags, 'gauge', int(reachability_stats[0]['alive_apps']), True,
                                                                'ZPA Connector - Application Reachability')
                tags['name'] = 'On-Access Health Apps'
                self.write_double_metric('zpa-apps-on-access-health', tags, 'gauge', int(reachability_stats[0]['apps_on_access_health']), True,
                                                                'ZPA Connector - Application Reachability')
                tags['name'] = 'Service Count'
                self.write_double_metric('zpa-service-count', tags, 'gauge', int(reachability_stats[0]['service_count']), True,
                                                                'ZPA Connector - Application Reachability')
                tags['name'] = 'Target Count'
                self.write_double_metric('zpa-target-count', tags, 'gauge', int(reachability_stats[0]['target_count']), True,
                                                                'ZPA Connector - Application Reachability')
                tags['name'] = 'Target Alive'
                self.write_double_metric('zpa-target-alive', tags, 'gauge', int(reachability_stats[0]['target_alive']), True,
                                                                'ZPA Connector - Application Reachability')

        return self.output