import os
import unittest

from zscaler.zpa_connector_cpu_memory.zpa_connector_cpu_memory import ZpaConnectorCpuMemory
from parser_service.public.action import *

expected_result_2cores_cpu_not_alerted = [{'metric': 'zpa-connector-cpu-usage-percentage', 'tags': {'im.identity-tags': 'pid|binary|name', 'pid': '31912', 'binary': '/opt/zscaler/bin/zpa-connector', 'name': 'cpu', 'live-config': 'true', 'display-name': 'ZPA Connector - Resources Usage', 'im.dstype.displayType': 'percentage'}, 'value': 0.0},
{'metric': 'zpa-connector-memory-usage-percentage', 'tags': {'im.identity-tags': 'pid|binary|name', 'pid': '31912', 'binary': '/opt/zscaler/bin/zpa-connector', 'name': 'memory', 'live-config': 'true', 'display-name': 'ZPA Connector - Resources Usage', 'im.dstype.displayType': 'percentage'}, 'value': 0.2}]

expected_result_4cores_cpu_alerted = [{'metric': 'zpa-connector-cpu-usage-percentage', 'tags': {'im.identity-tags': 'pid|binary|name', 'pid': '31912', 'binary': '/opt/zscaler/bin/zpa-connector', 'name': 'cpu', 'live-config': 'true', 'display-name': 'ZPA Connector - Resources Usage', 'im.dstype.displayType': 'percentage'}, 'value': 0.0},
{'metric': 'zpa-connector-memory-usage-percentage', 'tags': {'im.identity-tags': 'pid|binary|name', 'pid': '31912', 'binary': '/opt/zscaler/bin/zpa-connector', 'name': 'memory', 'live-config': 'true', 'display-name': 'ZPA Connector - Resources Usage', 'im.dstype.displayType': 'percentage'}, 'value': 0.2}]
class TestZpaConnectorCpuMemory(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = ZpaConnectorCpuMemory()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_2cores_cpu_not_alerted(self):
        result = self.parser.parse_file(self.current_dir + '/2cores_cpu_not_alerted.input', {}, {})
        self.assertEqual(2, len(result))

        for i in range(len(expected_result_2cores_cpu_not_alerted)):
            self.assertEqual(expected_result_2cores_cpu_not_alerted[i]['tags'], result[i].tags)
            self.assertEqual(expected_result_2cores_cpu_not_alerted[i]['metric'], result[i].name)
            self.assertEqual(expected_result_2cores_cpu_not_alerted[i]['value'], result[i].value)

    def test_4cores_cpu_alerted(self):
        result = self.parser.parse_file(self.current_dir + '/4cores_cpu_alerted.input', {}, {})
        self.assertEqual(2, len(result))

        for i in range(len(expected_result_4cores_cpu_alerted)):
            self.assertEqual(expected_result_4cores_cpu_alerted[i]['tags'], result[i].tags)
            self.assertEqual(expected_result_4cores_cpu_alerted[i]['metric'], result[i].name)
            self.assertEqual(expected_result_4cores_cpu_alerted[i]['value'], result[i].value)

if __name__ == '__main__':
    unittest.main()