from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class ZpaConnectorCpuMemory(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            connector_resources_usage = helper_methods.parse_data_as_list(raw_data, 'zpa_connector_cpu_memory.textfsm')
            if len(connector_resources_usage) > 0:
                for process in connector_resources_usage:
                    tags = {}
                    tags['im.identity-tags'] = 'pid|binary|name'
                    tags['pid'] = process['pid']
                    tags['binary'] = process['binary_path']
                    tags['name'] = 'cpu'
                    self.write_double_metric('zpa-connector-cpu-usage-percentage', tags, 'gauge', float(process['cpu_usage']) / float(process['cpu_count']), True,
                                                                    'ZPA Connector - Resources Usage', 'percentage')
                    tags['name'] = 'memory'
                    self.write_double_metric('zpa-connector-memory-usage-percentage', tags, 'gauge', float(process['mem_usage']), True,
                                                                    'ZPA Connector - Resources Usage', 'percentage')


        return self.output