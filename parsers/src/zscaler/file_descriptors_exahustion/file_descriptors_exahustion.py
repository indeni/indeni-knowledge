from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class FileDescriptorsExhaustion(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            file_handles_usage = helper_methods.parse_data_as_list(raw_data, 'file_descriptors_exahustion.textfsm')
            if file_handles_usage:
                tags = {}
                tags['im.identity-tags'] = 'name'
                tags['name'] = 'Allocated File Handles'
                self.write_double_metric('allocated-file-handles', tags, 'gauge', float(file_handles_usage[0]['allocated_file_handles']), True,
                                                                'ZPA Connector - File Descriptors')
                tags['name'] = 'Unused File Handles'
                self.write_double_metric('unused-file-handles', tags, 'gauge', float(file_handles_usage[0]['unused_file_handles']), True,
                                                                'ZPA Connector - File Descriptors')
                tags['name'] = 'Max File Handles'
                self.write_double_metric('allocated-file-handles-max', tags, 'gauge', float(file_handles_usage[0]['max_file_handles']), True,
                                                                'ZPA Connector - File Descriptors')

        return self.output