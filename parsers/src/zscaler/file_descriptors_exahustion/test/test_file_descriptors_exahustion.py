import os
import unittest

from zscaler.file_descriptors_exahustion.file_descriptors_exahustion import FileDescriptorsExhaustion
from parser_service.public.action import *

expected_result_alarmed = [{'metric': 'allocated-file-handles', 'tags': {'im.identity-tags': 'name', 'name': 'Allocated File Handles', 'live-config': 'true', 'display-name': 'ZPA Connector - File Descriptors'}, 'value': 374901.0},
    {'metric': 'unused-file-handles', 'tags': {'im.identity-tags': 'name', 'name': 'Unused File Handles', 'live-config': 'true', 'display-name': 'ZPA Connector - File Descriptors'}, 'value': 0.0},
    {'metric': 'allocated-file-handles-max', 'tags': {'im.identity-tags': 'name', 'name': 'Max File Handles', 'live-config': 'true', 'display-name': 'ZPA Connector - File Descriptors'}, 'value': 374973.0}]

expected_result_not_alarmed =[{'metric': 'allocated-file-handles', 'tags': {'im.identity-tags': 'name', 'name': 'Allocated File Handles', 'live-config': 'true', 'display-name': 'ZPA Connector - File Descriptors'}, 'value': 6624.0},
    {'metric': 'unused-file-handles', 'tags': {'im.identity-tags': 'name', 'name': 'Unused File Handles', 'live-config': 'true', 'display-name': 'ZPA Connector - File Descriptors'}, 'value': 0.0},
    {'metric': 'allocated-file-handles-max', 'tags': {'im.identity-tags': 'name', 'name': 'Max File Handles', 'live-config': 'true', 'display-name': 'ZPA Connector - File Descriptors'}, 'value': 374973.0}]
class TestFileDescriptorsExhaustion(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = FileDescriptorsExhaustion()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_alarmed(self):
        result = self.parser.parse_file(self.current_dir + '/alarmed.input', {}, {})
        self.assertEqual(3, len(result))

        for i in range(len(expected_result_alarmed)):
            self.assertEqual(expected_result_alarmed[i]['tags'], result[i].tags)
            self.assertEqual(expected_result_alarmed[i]['metric'], result[i].name)
            self.assertEqual(expected_result_alarmed[i]['value'], result[i].value)

    def test_not_alarmed(self):
        result = self.parser.parse_file(self.current_dir + '/not_alarmed.input', {}, {})
        self.assertEqual(3, len(result))

        for i in range(len(expected_result_not_alarmed)):
            self.assertEqual(expected_result_not_alarmed[i]['tags'], result[i].tags)
            self.assertEqual(expected_result_not_alarmed[i]['metric'], result[i].name)
            self.assertEqual(expected_result_not_alarmed[i]['value'], result[i].value)

if __name__ == '__main__':
    unittest.main()