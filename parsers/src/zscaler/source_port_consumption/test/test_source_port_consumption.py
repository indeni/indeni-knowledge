import os
import unittest

from zscaler.source_port_consumption.source_port_consumption import SourcePortConsumption
from parser_service.public.action import *

expected_result_alarmed = [{'metric': 'port-usage', 'tags': {'im.identity-tags': 'protocol', 'protocol': 'TCP', 'live-config': 'true', 'display-name': 'ZPA Connector - Port Usage'}, 'value': 5.0},
    {'metric': 'total-available-ports', 'tags': {'im.identity-tags': 'protocol', 'protocol': 'TCP', 'live-config': 'true', 'display-name': 'ZPA Connector - Port Usage Limit'}, 'value': 28231.0},
    {'metric': 'port-usage', 'tags': {'im.identity-tags': 'protocol', 'protocol': 'UDP', 'live-config': 'true', 'display-name': 'ZPA Connector - Port Usage'}, 'value': 26235.0},
    {'metric': 'total-available-ports', 'tags': {'im.identity-tags': 'protocol', 'protocol': 'UDP', 'live-config': 'true', 'display-name': 'ZPA Connector - Port Usage Limit'}, 'value': 28231.0}]

expected_result_not_alarmed =[{'metric': 'port-usage', 'tags': {'im.identity-tags': 'protocol', 'protocol': 'TCP', 'live-config': 'true', 'display-name': 'ZPA Connector - Port Usage'}, 'value': 5.0},
{'metric': 'total-available-ports', 'tags': {'im.identity-tags': 'protocol', 'protocol': 'TCP', 'live-config': 'true', 'display-name': 'ZPA Connector - Port Usage Limit'}, 'value': 28231.0},
{'metric': 'port-usage', 'tags': {'im.identity-tags': 'protocol', 'protocol': 'UDP', 'live-config': 'true', 'display-name': 'ZPA Connector - Port Usage'}, 'value': 13.0},
{'metric': 'total-available-ports', 'tags': {'im.identity-tags': 'protocol', 'protocol': 'UDP', 'live-config': 'true', 'display-name': 'ZPA Connector - Port Usage Limit'}, 'value': 28231.0}]

class TestSourcePortConsumption(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = SourcePortConsumption()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_alarmed(self):
        result = self.parser.parse_file(self.current_dir + '/alarmed.input', {}, {})
        self.assertEqual(4, len(result))

        for i in range(len(expected_result_alarmed)):
            self.assertEqual(expected_result_alarmed[i]['tags'], result[i].tags)
            self.assertEqual(expected_result_alarmed[i]['metric'], result[i].name)
            self.assertEqual(expected_result_alarmed[i]['value'], result[i].value)

    def test_not_alarmed(self):
        result = self.parser.parse_file(self.current_dir + '/not_alarmed.input', {}, {})
        self.assertEqual(4, len(result))

        for i in range(len(expected_result_not_alarmed)):
            self.assertEqual(expected_result_not_alarmed[i]['tags'], result[i].tags)
            self.assertEqual(expected_result_not_alarmed[i]['metric'], result[i].name)
            self.assertEqual(expected_result_not_alarmed[i]['value'], result[i].value)

if __name__ == '__main__':
    unittest.main()