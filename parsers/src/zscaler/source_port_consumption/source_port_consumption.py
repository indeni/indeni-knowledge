from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class SourcePortConsumption(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            source_port_usage = helper_methods.parse_data_as_list(raw_data, 'source_port_consumption.textfsm')
            if source_port_usage:
                tags = {}
                tags['im.identity-tags'] = 'protocol'
                tags['protocol'] = 'TCP'
                self.write_double_metric('port-usage', tags, 'gauge', float(source_port_usage[0]['tcp_ports_in_use']), True,
                                                                'ZPA Connector - Port Usage')
                tags['protocol'] = 'TCP'
                self.write_double_metric('total-available-ports', tags, 'gauge', float(source_port_usage[0]['port_high']) - float(source_port_usage[0]['port_low']), True,
                                                                'ZPA Connector - Port Usage Limit')
                tags['protocol'] = 'UDP'
                self.write_double_metric('port-usage', tags, 'gauge', float(source_port_usage[0]['udp_ports_in_use']), True,
                                                                'ZPA Connector - Port Usage')
                tags['protocol'] = 'UDP'
                self.write_double_metric('total-available-ports', tags, 'gauge', float(source_port_usage[0]['port_high']) - float(source_port_usage[0]['port_low']), True,
                                                                'ZPA Connector - Port Usage Limit')

        return self.output