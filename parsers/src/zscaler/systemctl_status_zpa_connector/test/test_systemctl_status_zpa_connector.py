import os
import unittest

from zscaler.systemctl_status_zpa_connector.systemctl_status_zpa_connector import SystemCtlStatusZpaConnector
from parser_service.public.action import *

expected_result_status_active_config_loaded_boot_disabled = [{'metric': 'systemctl-service-description', 'tags': {'im.identity-tags': 'name|type', 'name': 'zpa-connector', 'type': 'description', 'live-config': 'true', 'display-name': 'ZPA Connector - Details'}, 'value': {'value': 'Zscaler Private Access Connector'}},
    {'metric': 'systemctl-service-source-file', 'tags': {'im.identity-tags': 'name|type', 'name': 'zpa-connector', 'type': 'source file', 'live-config': 'true', 'display-name': 'ZPA Connector - Details'}, 'value': {'value': '/usr/lib/systemd/system/zpa-connector.service'}},
    {'metric': 'systemctl-zpa-connector-service-state', 'tags': {'im.identity-tags': 'name|type', 'name': 'zpa-connector', 'type': 'state status', 'live-config': 'true', 'display-name': 'ZPA Connector - Status', 'im.dstype.displayType': 'state'}, 'value': 1},
    {'metric': 'systemctl-zpa-connector-service-state-detail', 'tags': {'im.identity-tags': 'name|type', 'name': 'zpa-connector', 'type': 'state status detail', 'live-config': 'true', 'display-name': 'ZPA Connector - Details'}, 'value': {'value': 'running'}},
    {'metric': 'systemctl-zpa-connector-service-memory-load', 'tags': {'im.identity-tags': 'name|type', 'name': 'zpa-connector', 'type': 'load in memory', 'live-config': 'true', 'display-name': 'ZPA Connector - Status', 'im.dstype.displayType': 'state'}, 'value': 1},
    {'metric': 'systemctl-zpa-connector-service-boot-start', 'tags': {'im.identity-tags': 'name|type', 'name': 'zpa-connector', 'type': 'start at boot', 'live-config': 'true', 'display-name': 'ZPA Connector - Status', 'im.dstype.displayType': 'state'}, 'value': 0},
    {'metric': 'systemctl-service-status-since', 'tags': {'im.identity-tags': 'name|type', 'name': 'zpa-connector', 'type': 'state status since', 'live-config': 'true', 'display-name': 'ZPA Connector - Details'}, 'value': {'value': 'mié 2021-12-08 01:23:31 IST'}}]

expected_result_status_active_config_loaded_boot_enabled = [{'metric': 'systemctl-service-description', 'tags': {'im.identity-tags': 'name|type', 'name': 'zpa-connector', 'type': 'description', 'live-config': 'true', 'display-name': 'ZPA Connector - Details'}, 'value': {'value': 'Zscaler Private Access Connector'}},
    {'metric': 'systemctl-service-source-file', 'tags': {'im.identity-tags': 'name|type', 'name': 'zpa-connector', 'type': 'source file', 'live-config': 'true', 'display-name': 'ZPA Connector - Details'}, 'value': {'value': '/usr/lib/systemd/system/zpa-connector.service'}},
    {'metric': 'zpa-connector-memory-usage-size', 'tags': {'im.identity-tags': 'name|type', 'name': 'zpa-connector', 'type': 'memory usage size', 'live-config': 'true', 'display-name': 'ZPA Connector - Details'}, 'value': {'value': '15.1M'}},
    {'metric': 'systemctl-zpa-connector-service-state', 'tags': {'im.identity-tags': 'name|type', 'name': 'zpa-connector', 'type': 'state status', 'live-config': 'true', 'display-name': 'ZPA Connector - Status', 'im.dstype.displayType': 'state'}, 'value': 1},
    {'metric': 'systemctl-zpa-connector-service-state-detail', 'tags': {'im.identity-tags': 'name|type', 'name': 'zpa-connector', 'type': 'state status detail', 'live-config': 'true', 'display-name': 'ZPA Connector - Details'}, 'value': {'value': 'running'}},
    {'metric': 'systemctl-zpa-connector-service-memory-load', 'tags': {'im.identity-tags': 'name|type', 'name': 'zpa-connector', 'type': 'load in memory', 'live-config': 'true', 'display-name': 'ZPA Connector - Status', 'im.dstype.displayType': 'state'}, 'value': 1},
    {'metric': 'systemctl-zpa-connector-service-boot-start', 'tags': {'im.identity-tags': 'name|type', 'name': 'zpa-connector', 'type': 'start at boot', 'live-config': 'true', 'display-name': 'ZPA Connector - Status', 'im.dstype.displayType': 'state'}, 'value': 1},
    {'metric': 'systemctl-service-status-since', 'tags': {'im.identity-tags': 'name|type', 'name': 'zpa-connector', 'type': 'state status since', 'live-config': 'true', 'display-name': 'ZPA Connector - Details'}, 'value': {'value': 'Mon 2021-12-06 02:52:01 EST'}}]

expected_result_status_active_config_not_found_boot_no_data = [{'metric': 'zpa-connector-memory-usage-size', 'tags': {'im.identity-tags': 'name|type', 'name': 'zpa-connector', 'type': 'memory usage size', 'live-config': 'true', 'display-name': 'ZPA Connector - Details'}, 'value': {'value': '5.5M'}},
    {'metric': 'systemctl-zpa-connector-service-state', 'tags': {'im.identity-tags': 'name|type', 'name': 'zpa-connector', 'type': 'state status', 'live-config': 'true', 'display-name': 'ZPA Connector - Status', 'im.dstype.displayType': 'state'}, 'value': 1},
    {'metric': 'systemctl-zpa-connector-service-state-detail', 'tags': {'im.identity-tags': 'name|type', 'name': 'zpa-connector', 'type': 'state status detail', 'live-config': 'true', 'display-name': 'ZPA Connector - Details'}, 'value': {'value': 'running'}},
    {'metric': 'systemctl-zpa-connector-service-memory-load', 'tags': {'im.identity-tags': 'name|type', 'name': 'zpa-connector', 'type': 'load in memory', 'live-config': 'true', 'display-name': 'ZPA Connector - Status', 'im.dstype.displayType': 'state'}, 'value': 0},
    {'metric': 'systemctl-service-status-since', 'tags': {'im.identity-tags': 'name|type', 'name': 'zpa-connector', 'type': 'state status since', 'live-config': 'true', 'display-name': 'ZPA Connector - Details'}, 'value': {'value': 'Tue 2021-12-07 16:55:48 EST'}}]

expected_result_status_failed_config_not_found_boot_no_data = [{'metric': 'systemctl-zpa-connector-service-state', 'tags': {'im.identity-tags': 'name|type', 'name': 'zpa-connector', 'type': 'state status', 'live-config': 'true', 'display-name': 'ZPA Connector - Status', 'im.dstype.displayType': 'state'}, 'value': 0},
    {'metric': 'systemctl-zpa-connector-service-state-detail', 'tags': {'im.identity-tags': 'name|type', 'name': 'zpa-connector', 'type': 'state status detail', 'live-config': 'true', 'display-name': 'ZPA Connector - Details'}, 'value': {'value': 'Result: start-limit'}},
    {'metric': 'systemctl-zpa-connector-service-memory-load', 'tags': {'im.identity-tags': 'name|type', 'name': 'zpa-connector', 'type': 'load in memory', 'live-config': 'true', 'display-name': 'ZPA Connector - Status', 'im.dstype.displayType': 'state'}, 'value': 0},
    {'metric': 'systemctl-service-status-since', 'tags': {'im.identity-tags': 'name|type', 'name': 'zpa-connector', 'type': 'state status since', 'live-config': 'true', 'display-name': 'ZPA Connector - Details'}, 'value': {'value': 'mié 2021-12-08 00:11:10 IST'}}]

expected_result_status_inactive_config_loaded_boot_enabled = [{'metric': 'systemctl-service-description', 'tags': {'im.identity-tags': 'name|type', 'name': 'zpa-connector', 'type': 'description', 'live-config': 'true', 'display-name': 'ZPA Connector - Details'}, 'value': {'value': 'Zscaler Private Access Connector'}},
    {'metric': 'systemctl-service-source-file', 'tags': {'im.identity-tags': 'name|type', 'name': 'zpa-connector', 'type': 'source file', 'live-config': 'true', 'display-name': 'ZPA Connector - Details'}, 'value': {'value': '/usr/lib/systemd/system/zpa-connector.service'}},
    {'metric': 'systemctl-zpa-connector-service-state', 'tags': {'im.identity-tags': 'name|type', 'name': 'zpa-connector', 'type': 'state status', 'live-config': 'true', 'display-name': 'ZPA Connector - Status', 'im.dstype.displayType': 'state'}, 'value': 0},
    {'metric': 'systemctl-zpa-connector-service-state-detail', 'tags': {'im.identity-tags': 'name|type', 'name': 'zpa-connector', 'type': 'state status detail', 'live-config': 'true', 'display-name': 'ZPA Connector - Details'}, 'value': {'value': 'dead'}},
    {'metric': 'systemctl-zpa-connector-service-memory-load', 'tags': {'im.identity-tags': 'name|type', 'name': 'zpa-connector', 'type': 'load in memory', 'live-config': 'true', 'display-name': 'ZPA Connector - Status', 'im.dstype.displayType': 'state'}, 'value': 1},
    {'metric': 'systemctl-zpa-connector-service-boot-start', 'tags': {'im.identity-tags': 'name|type', 'name': 'zpa-connector', 'type': 'start at boot', 'live-config': 'true', 'display-name': 'ZPA Connector - Status', 'im.dstype.displayType': 'state'}, 'value': 1},
    {'metric': 'systemctl-service-status-since', 'tags': {'im.identity-tags': 'name|type', 'name': 'zpa-connector', 'type': 'state status since', 'live-config': 'true', 'display-name': 'ZPA Connector - Details'}, 'value': {'value': 'Mon 2021-12-06 02:48:00 EST'}}]
class TestSystemCtlStatusZpaConnector(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = SystemCtlStatusZpaConnector()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_status_active_config_loaded_boot_disabled(self):
        result = self.parser.parse_file(self.current_dir + '/status_active_config_loaded_boot_disabled.input', {}, {})
        self.assertEqual(7, len(result))

        for i in range(len(expected_result_status_active_config_loaded_boot_disabled)):
            self.assertEqual(expected_result_status_active_config_loaded_boot_disabled[i]['tags'], result[i].tags)
            self.assertEqual(expected_result_status_active_config_loaded_boot_disabled[i]['metric'], result[i].name)
            self.assertEqual(expected_result_status_active_config_loaded_boot_disabled[i]['value'], result[i].value)

    def test_status_active_config_loaded_boot_enabled(self):
        result = self.parser.parse_file(self.current_dir + '/status_active_config_loaded_boot_enabled.input', {}, {})
        self.assertEqual(8, len(result))

        for i in range(len(expected_result_status_active_config_loaded_boot_enabled)):
            self.assertEqual(expected_result_status_active_config_loaded_boot_enabled[i]['tags'], result[i].tags)
            self.assertEqual(expected_result_status_active_config_loaded_boot_enabled[i]['metric'], result[i].name)
            self.assertEqual(expected_result_status_active_config_loaded_boot_enabled[i]['value'], result[i].value)

    def test_status_active_config_not_found_boot_no_data(self):
        result = self.parser.parse_file(self.current_dir + '/status_active_config_not_found_boot_no_data.input', {}, {})
        self.assertEqual(5, len(result))

        for i in range(len(expected_result_status_active_config_not_found_boot_no_data)):
            self.assertEqual(expected_result_status_active_config_not_found_boot_no_data[i]['tags'], result[i].tags)
            self.assertEqual(expected_result_status_active_config_not_found_boot_no_data[i]['metric'], result[i].name)
            self.assertEqual(expected_result_status_active_config_not_found_boot_no_data[i]['value'], result[i].value)

    def test_status_failed_config_not_found_boot_no_data(self):
        result = self.parser.parse_file(self.current_dir + '/status_failed_config_not_found_boot_no_data.input', {}, {})
        self.assertEqual(4, len(result))

        for i in range(len(expected_result_status_failed_config_not_found_boot_no_data)):
            self.assertEqual(expected_result_status_failed_config_not_found_boot_no_data[i]['tags'], result[i].tags)
            self.assertEqual(expected_result_status_failed_config_not_found_boot_no_data[i]['metric'], result[i].name)
            self.assertEqual(expected_result_status_failed_config_not_found_boot_no_data[i]['value'], result[i].value)

    def test_status_inactive_config_loaded_boot_enabled(self):
        result = self.parser.parse_file(self.current_dir + '/status_inactive_config_loaded_boot_enabled.input', {}, {})
        self.assertEqual(7, len(result))

        for i in range(len(expected_result_status_inactive_config_loaded_boot_enabled)):
            self.assertEqual(expected_result_status_inactive_config_loaded_boot_enabled[i]['tags'], result[i].tags)
            self.assertEqual(expected_result_status_inactive_config_loaded_boot_enabled[i]['metric'], result[i].name)
            self.assertEqual(expected_result_status_inactive_config_loaded_boot_enabled[i]['value'], result[i].value)

if __name__ == '__main__':
    unittest.main()