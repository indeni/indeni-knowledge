from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

states_ok = ['active','reloading', 'activating']
states_nok = ['inactive', 'maintenance','deactivating','failed','error']
load_ok = ['loaded']
load_nok = ['error', 'not-found', 'bad-setting', 'masked']
is_enabled_ok = ['enabled', 'enabled-runtime', 'linked', 'linked-runtime']
is_enabled_nok = ['masked', 'masked-runtime', 'static', 'indirect', 'disabled', 'bad']
is_enabled_not_defined = ['alias', 'generated', 'transient']

class SystemCtlStatusZpaConnector(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            connector_data = helper_methods.parse_data_as_list(raw_data, 'systemctl_status_zpa_connector.textfsm')
            if connector_data:
                tags = {}
                tags['im.identity-tags'] = 'name|type'
                tags['name'] = connector_data[0]['service_name']

                if connector_data[0].get('service_description'):
                    tags['type'] = 'description'
                    self.write_complex_metric_string('systemctl-service-description', tags, connector_data[0]['service_description'], True, 'ZPA Connector - Details')

                if connector_data[0].get('unit_file_source'):
                    tags['type'] = 'source file'
                    self.write_complex_metric_string('systemctl-service-source-file', tags, connector_data[0]['unit_file_source'], True, 'ZPA Connector - Details')

                if connector_data[0].get('unit_memory_usage_size'):
                    tags['type'] = 'memory usage size'
                    self.write_complex_metric_string('zpa-connector-memory-usage-size', tags, connector_data[0]['unit_memory_usage_size'], True, 'ZPA Connector - Details')

                if connector_data[0].get('unit_activation_state'):
                    tags['type'] = 'state status'
                    self.write_double_metric('systemctl-zpa-connector-service-state', tags, 'gauge', 1 if connector_data[0]['unit_activation_state'].lower() in states_ok else 0, True,
                                                                    'ZPA Connector - Status', 'state', 'name|type')

                if connector_data[0].get('unit_activation_detail'):
                    tags['type'] = 'state status detail'
                    self.write_complex_metric_string('systemctl-zpa-connector-service-state-detail', tags, connector_data[0]['unit_activation_detail'], True, 'ZPA Connector - Details')

                if connector_data[0].get('unit_memory_load'):
                    tags['type'] = 'load in memory'
                    self.write_double_metric('systemctl-zpa-connector-service-memory-load', tags, 'gauge', 1 if connector_data[0]['unit_memory_load'].lower() in load_ok else 0, True,
                                                                    'ZPA Connector - Status', 'state', 'name|type')

                if connector_data[0].get('unit_start_at_boot'):
                    tags['type'] = 'start at boot'
                    self.write_double_metric('systemctl-zpa-connector-service-boot-start', tags, 'gauge', 1 if connector_data[0]['unit_start_at_boot'].lower() in is_enabled_ok else 0, True,
                                                                    'ZPA Connector - Status', 'state', 'name|type')

                if connector_data[0].get('unit_activation_date'):
                    tags['type'] = 'state status since'
                    self.write_complex_metric_string('systemctl-service-status-since', tags, connector_data[0]['unit_activation_date'], True, 'ZPA Connector - Details')

        return self.output