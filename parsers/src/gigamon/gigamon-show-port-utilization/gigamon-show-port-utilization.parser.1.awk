#1/1/x1    hybrid     10000    0.00   0.00        0    0   -                    -
/^[0-9]+\// {
    tags["name"] = $1
    speed = $3
    tx_utilization_percentage = $4
    rx_utilization_percentage = $5
    writeDoubleMetric("network-interface-tx-rate-mbps", tags, "gauge", speed * tx_utilization_percentage / 100.0, "true", "Network Interface Output Rate - Mbps", "number", "name")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("network-interface-rx-rate-mbps", tags, "gauge", speed * rx_utilization_percentage / 100.0, "true", "Network Interface Input Rate - Mbps", "number", "name")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("network-interface-bandwidth-mbps", tags, "gauge", speed, "true", "Network Interface Speed - Mbps", "number", "name")  # Converted to new syntax by change_ind_scripts.py script
    next
}