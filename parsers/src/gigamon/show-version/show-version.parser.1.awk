#CPU load averages: 1.08 / 1.04 / 1.06
/^CPU load averages:/ {
    avg = $4 * 100
}

#Number of CPUs:    4
/^Number of CPUs:/ {
    cpu_count = $4
    tags["cpu-id"] = "average"
    tags["cpu-is-avg"] = "true"
    tags["resource-metric"] = "true"
    writeDoubleMetric("cpu-usage", tags, "gauge", avg / cpu_count, "true", "CPU Usage", "percentage", "cpu-id")  # Converted to new syntax by change_ind_scripts.py script
}