BEGIN {
    # Initializing default values for interrogation tags
    is_gigamon = 0
    os_name = ""
    os_version = ""
    model = ""
    host_id = ""
}

# Extract 'Product model'
#Product model:     GigaVUE-HC2
/^Product model:/ {
    #Retrieve model
    if ($3 ~ /Giga/) {
        model = $3
        is_gigamon = 1
    }
    next
}

# Extract 'Product edition'
#Product name:      GigaVUE-OS
/^Product name:/ {
    os_name = $3
    next
}

# Extract 'version'
#Product release:   4.7.01
/^Product release:/ {
    os_version = $3
    next
}

# Extract 'Host ID'
#Host ID:           8690de621fe9
/^Host ID:/ {
    host_id = $3
    next
}

END {
    #Only publish tags if Gigamon
    if (is_gigamon == 1) {
        writeTag("vendor", "gigamon")
        if (os_name != "") {
            writeTag("os.name", os_name)
        }

        writeTag("product", "network-tap")

        if (model != "") {
            writeTag("model", model);
        }

        if (os_version != "") {
            writeTag("os.version", os_version)
        }

        if (host_id != "") {
            writeTag("host-id", host_id)
        }
    }
}