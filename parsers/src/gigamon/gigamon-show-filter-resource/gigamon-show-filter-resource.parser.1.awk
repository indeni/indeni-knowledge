#1/1   98%                  100%
/\s+[0-9]/ {
    box_name = $1
    drop_pass_avail = $2
    tool_port_avail = $3

    sub(/%/, "", drop_pass_avail)
    sub(/%/, "", tool_port_avail)
    tags["name"] = box_name

    writeDoubleMetric("filter-drop-pass-available", tags, "gauge", drop_pass_avail, "true", "Filter Resources Available - Drop & Pass", "percentage", "name")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("filter-tool-port-available", tags, "gauge", tool_port_avail, "true", "Filter Resources Available - Tool Port Filter", "percentage", "name")  # Converted to new syntax by change_ind_scripts.py script
}