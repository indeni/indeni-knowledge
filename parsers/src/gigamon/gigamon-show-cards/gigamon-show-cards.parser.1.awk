#Box ID: 1
/^Box ID:/ {
    box_id = $NF
    next
}

#cc1   yes     up           HC2-Main-Board  132-00AN      1AN0-009C   B1-25
/[^\s]+\s{2,}[^\s]+\s{2,}[^\s]+\s{2,}[^\s]+\s{2,}[^\s]+\s{2,}[^\s]+\s+[^\s]+/ {
    slot = $1
    config = $2
    oper_status = $3

    hardware_status = 1.0
    if (config != "yes") {
        hardware_status = 0.0
    } else {
        if (oper_status != "up") {
            hardware_status = 0.0
        }
    }

    tags["name"] = "Card: Box " box_id " / Slot " slot
    tags["type"] = $4
    writeDoubleMetric("hardware-element-status", tags, "gauge", hardware_status, "true", "Card status", "state", "name|type")
    next
}