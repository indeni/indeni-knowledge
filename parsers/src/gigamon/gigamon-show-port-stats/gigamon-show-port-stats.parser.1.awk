#Check if the given data is valid or not. Returns 1 if it's valid, 0 otherwise
function isValidData(data) {
    if (data != "N/A") {
        return 1
    }
    return 0
}

#Iterate through each of the interface columns and writes the value out as double metric
function writeCounterMetric(metric, display_name, unit) {
    for (name in name_to_index_array) {
        idx = name_to_index_array[name]
        if (isValidData($idx)) {
            tags["name"] = name
            writeDoubleMetric(metric, tags, "gauge", $idx, "true", display_name, unit, "name")
        }
    }
}

#Iterate through the current row and adds the value to total rx pkts array for each interface
function updateTotalRxPkts() {
    for (name in name_to_index_array) {
        idx = name_to_index_array[name]
        if (isValidData($idx)) {
           name_to_total_rx_pkts_array[name] = name_to_total_rx_pkts_array[name] + $idx
        }
    }
}

#Iterate through the total rx pkts array and write network interface rx packets metric for each interface
function writeTotalRxPktsIfExist() {
    for (name in name_to_total_rx_pkts_array) {
        tags["name"] = name
        total_rx_pkts = name_to_total_rx_pkts_array[name]
        writeDoubleMetric("network-interface-rx-packets", tags, "gauge", total_rx_pkts, "true", "Network Interfaces - RX Packets", "number", "name")
    }
}

#        Counter Name     Port: 1/1/e1     Port: 1/1/x1     Port: 1/1/x2     Port: 1/1/x3
/^\s+Counter Name/ {

    writeTotalRxPktsIfExist()

    split("", name_to_index_array)
    split("", name_to_total_rx_pkts_array)
    col = 1
    for (i = 3; i <= NF; i++) {
        if ($i == "Port:") {
            col++
            i++
            name_to_index_array[$i] = col
            name_to_total_rx_pkts_array[$i] = 0
        }
    }
    next
}

#       IfInPktDrops:              N/A                0                0                0
/^\s+IfInPktDrops:/ {
    writeCounterMetric("network-interface-rx-dropped", "Network Interfaces - RX Dropped", "number")
    next
}

#      IfInUcastPkts:                0                0                0                0
/^\s+IfInUcastPkts:/ {
    updateTotalRxPkts()
    next
}

#     IfInNUcastPkts:                0                0                0                0
/^\s+IfInNUcastPkts:/ {
    updateTotalRxPkts()
    next
}

#This is needed to write the remaining data
END {
    writeTotalRxPktsIfExist()
}
