#1/1/e1    gs          N/A          enabled      up                   -            N/A          N/A        40000  full    off    N/A            
/^[0-9]/ {
    # We are only interested in ports that are enabled
    if (trim($4) != "enabled") {
        next
    }

    tags["name"] = $1
    state = 0.0
    if ($5 == "up") {
        state = 1.0
    }

    writeDoubleMetric("network-interface-state", tags, "gauge", state, "true", "Network interface state", "boolean", "name")  # Converted to new syntax by change_ind_scripts.py script
    next
}