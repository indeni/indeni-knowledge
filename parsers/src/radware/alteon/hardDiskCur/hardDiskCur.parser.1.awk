BEGIN{
    FS=":"
}
/hardDiskCur/ {
    #  Disk usage is in GB
    usageinkb = cleanJsonValue($NF) * 1024 * 1024
    disktags["file-system"] = "base"
    writeDoubleMetric("disk-used-kbytes", disktags, "gauge", usageinkb, "true", "Disk - Used", "kilobytes")  # Converted to new syntax by change_ind_scripts.py script
}