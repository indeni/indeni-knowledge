BEGIN{
    FS=":"
}
/Indx/ {
    indx = cleanJsonValue($NF)
}
/State/ {
    #  {2=ENABLED, 3=DISABLED} 
    state = cleanJsonValue($NF)
    if (state == 2) {
        state = 1.0
    } else {
        state = 0
    }
    nictags["name"] = indx
    writeDoubleMetric("network-interface-admin-state", nictags, "gauge", state, "true", "Network Interfaces - Admin State", "state", "name")  # Converted to new syntax by change_ind_scripts.py script
}