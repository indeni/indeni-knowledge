BEGIN{
    FS=":"
    tags["name"] = "System Ambient Temperature"
}

# Temperature states:
# ok(1),
# fail(2)
# There are limitations with what we can gather from the device itself.
# We can only know when the temperature is either in "OK" shape.
# Should the temperature reach critical levels, the output would be 2.
# This script relies on the impression that Indeni should trigger an alert when it reaches critical levels.
# This is done by assuming that the maximum value of the sensor is 2. The output captures current value.
# The scala rule triggers an alert when the the current value reaches 90% of the maximum.
# This means Indeni will always alert when the current value is 2.

# "hwTemperatureStatus": 1
/"hwTemperatureStatus"/ {
    current_state = cleanJsonValue($NF)
}


END {
writeDoubleMetric("temperature-sensor-max", tags, "gauge", 2, "false")  # Converted to new syntax by change_ind_scripts.py script
writeDoubleMetric("temperature-sensor-current", tags, "gauge", current_state, "false")  # Converted to new syntax by change_ind_scripts.py script
}