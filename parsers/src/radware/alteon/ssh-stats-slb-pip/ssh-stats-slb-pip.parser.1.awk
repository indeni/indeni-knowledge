#10.10.10.10                              2031584        0          0
$1 ~ /[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+/ {
    proxy_ip = $1
    free_count = $2
    used_count = $3
    total_count = free_count + used_count

    metricTags["name"] = proxy_ip
    writeDoubleMetric("lb-snatpool-limit", metricTags, "gauge", total_count, "false")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("lb-snatpool-usage", metricTags, "gauge", used_count, "false")  # Converted to new syntax by change_ind_scripts.py script
}