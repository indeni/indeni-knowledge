BEGIN{
    FS=":"
}
/Index/ {
    indx = cleanJsonValue($NF)
}
/IpAddr/ {
    ipaddr = cleanJsonValue($NF)
}
/State/ {
    state = cleanJsonValue($NF)
    if (state == 2) {
        state = 1.0
    } else {
        state = 0
    }
    servertags["name"] = indx " (" ipaddr ")"
    writeDoubleMetric("lb-server-state", servertags, "gauge", state, "true", "Real Servers - Operational State", "state", "name")  # Converted to new syntax by change_ind_scripts.py script
}