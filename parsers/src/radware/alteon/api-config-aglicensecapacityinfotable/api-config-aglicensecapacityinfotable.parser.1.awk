BEGIN {
    FS = ":"
    idx = ""
}

# "LicenseCapacityInfoIdx": 9,
/LicenseCapacityInfoIdx/ {
    idx = cleanJsonValue($NF)
}

# "LicenseCapacitySize": 10000,
/LicenseCapacitySize/ {
    size_string = cleanJsonValue($NF)  
    limit = size_string
}

# "LicenseCapacityCurrUsage": "880.65 Mbps",
/LicenseCapacityCurrUsage/ {
    current_string = cleanJsonValue($NF)
    # example: current_string = "880.65 Mbps"
    split(current_string, vals, " ")
    current = vals[1]
    # example: current = 880.65
    if (arraylen(vals) == 2) {
        currentunit = vals[2]
        # unify the throughput unit to Mbps
        if (currentunit == "Gbps") {
            current = current * 1000
            currentunit = "Mbps"
        } else if (currentunit == "bps") {
            current = current / 1000000
            currentunit = "Mbps"
        }
    } else {
        currentunit = ""
    }
}

# },
/\}/ {
    if ( idx == "8" ) {
        licensetags["name"] = "vADC"
    } else if (idx == "9") {
        licensetags["name"] = "Throughput (Mbps)"
    } else if (idx == "10") {
        licensetags["name"] = "SSL (CPS)"
    } else if (idx == "11") {
        licensetags["name"] = "Compression (Mbps)"
    } else if (idx == "12") {
        licensetags["name"] = "APM (PgPM)"
    } else if (idx == "101") {
        licensetags["name"] = "Fastview (PgPS)"
    } else if (idx == "16") {
        licensetags["name"] = "AppWall (Mbps)"
    } else if (idx == "17") {
        licensetags["name"] = "Authentication (users)"
    } else {
        # next line
        next
    }

    if (current != "N/A") {
        writeDoubleMetric("license-elements-used", licensetags, "gauge", current, "true", "Licenses Usage", "number", "name")  # Converted to new syntax by change_ind_scripts.py script
    }
    if (limit != "N/A") {
        writeDoubleMetric("license-elements-limit", licensetags, "gauge", limit, "true", "Licenses Limit", "number", "name")  # Converted to new syntax by change_ind_scripts.py script
    }
}