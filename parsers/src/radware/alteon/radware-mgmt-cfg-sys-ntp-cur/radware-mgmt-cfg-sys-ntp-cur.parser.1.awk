#Current primary NTP server: 173.71.73.207
#Current secondary NTP server: 2.2.2.0
/NTP server/ {
    iEntry++

    servers[iEntry, "ipaddress"] = $5
    servers[iEntry, "type"] = $2 
} 

END {
    writeComplexMetricObjectArray("ntp-servers", null, servers, "true", "NTP Servers")  # Converted to new syntax by change_ind_scripts.py script
}