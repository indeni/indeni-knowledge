BEGIN{
    FS=":"
}
/hwTemperatureStatus/ {
# Temperature states:
# sok(1),
# fail(2)
    orig_state = cleanJsonValue($NF)
    if (orig_state == "1") {
        state = 1.0
    } else if (orig_state == "2") {
        state = 0.0
    }

    if (state != "") {
        hwelementtags["name"] = "Temperature"
        writeDoubleMetric("hardware-element-status", hwelementtags, "gauge", state, "true", "Hardware", "state", "name")  # Converted to new syntax by change_ind_scripts.py script
    }
}