BEGIN{
    FS=":"
}
/Indx/ {
    indx = cleanJsonValue($NF)
    nictags["name"] = indx
}
/Speed/ {
    # {2=MBS10, 3=MBS100, 4=MBS1000, 5=ANY, 6=MBS10000, 7=MBS40000, 8=AUTO} 
    orig_speed = cleanJsonValue($NF)
    if (orig_speed == 2) {
        speed = "10M"
    } else if (orig_speed == 3) {
        speed = "100M"
    } else if (orig_speed == 4) {
        speed = "1000M"
    } else if (orig_speed == 6) {
        speed = "10G"
    } else if (orig_speed == 7) {
        speed = "40G"
    } else {
        speed = "unknown"
    }
    speed_tags["name"] = nictags["name"]
    writeComplexMetricString("network-interface-speed", speed_tags, speed, "false")  # Converted to new syntax by change_ind_scripts.py script
}
/PhyIfDesc/ {
    writeComplexMetricString("network-interface-type", nictags, cleanJsonValue($NF), "false")  # Converted to new syntax by change_ind_scripts.py script
}
/Mode/ {
    # {2=FULL_DUPLEX, 3=HALF_DUPLEX, 4=ANY} 
    orig_duplex = cleanJsonValue($NF)
    if (orig_duplex == 2) {
        duplex = "full"
    } else if (orig_duplex == 3) {
        duplex = "half"
    } else {
        duplex = "unknown"
    }
    writeComplexMetricString("network-interface-duplex", nictags, duplex, "false")  # Converted to new syntax by change_ind_scripts.py script
}
/PhyIfMtu/ {
    writeComplexMetricString("network-interface-mtu", nictags, cleanJsonValue($NF), "false")  # Converted to new syntax by change_ind_scripts.py script
}
/PhyIfPhysAddress/ {
    #   "PhyIfPhysAddress":"00:03:b2:80:00:40",
    # Because FS = ":", it breaks the MAC address
    value = substr($0, index($0, ":") + 1)
    writeComplexMetricString("network-interface-mac", nictags, cleanJsonValue(value), "false")  # Converted to new syntax by change_ind_scripts.py script
}
/State/ {
    #  {1=UP, 2=DOWN, 3=TESTING} 
    state = cleanJsonValue($NF)
    if (state == 1) {
        state = 1.0
    } else {
        state = 0
    }
    writeDoubleMetric("network-interface-state", nictags, "gauge", state, "true", "Network Interfaces - Operational State", "state", "name")  # Converted to new syntax by change_ind_scripts.py script
}