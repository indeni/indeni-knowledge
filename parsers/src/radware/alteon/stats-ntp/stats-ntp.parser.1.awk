#Current system time:  14:07:06 Fri Jun  9, 2017 (DST)
/Current system time/ {
	split($4, clockArr, ":")
	hour = clockArr[1]
	minute = clockArr[2]
	second = clockArr[3]

	month = parseMonthThreeLetter($6)
	
	day = $7
	#9,
	gsub (/,/," ", day)

	year = $8
	currentSinceEpoch = datetime(year, month, day, hour, minute, second)
}


#Last update time:     11:15:18 Thu Jun  8, 2017 (DST)
/Last update time/ {
	split($4, clockArr, ":")
	hour = clockArr[1]
	minute = clockArr[2]
	second = clockArr[3]

	month = parseMonthThreeLetter($6)

	day = $7
	#8,
	gsub (/,/," ", day)

	year = $8
	lastUpdateSinceEpoch = datetime(year, month, day, hour, minute, second)
}



END {
	differencesInSeconds = currentSinceEpoch - lastUpdateSinceEpoch
	
	if (differencesInSeconds < 600) {
		serverState = 1
	} else {
		serverState = 0
	}
	
	writeDoubleMetric("ntp-server-state", null, "gauge", serverState, "false")  # Converted to new syntax by change_ind_scripts.py script
}