BEGIN {
	fs = ":"
}
# Utilizing the Group ID will be very difficult here as many Radware users are instructed to use numerical indexes. 
# Relying on the index would be inaccurate to determine cluster groups. Instead, we rely on floating IPs
# Most Radware devices rely on an external and internal floating IP for dealing with high-availability.
# This script relies a quick sort of the 2nd octect within each ip address and arranges them in order of smaller octet to larger.

# WAN floating IP 69.187.24.140
# LAN floating IP 10.237.101.74

# "IpAddr": "69.187.24.140"
# "IpAddr": "10.237.101.74"
/"IpAddr"/{
	i++
	ipAddr[i] = $2
	#Basic sort below before concatenating the floating IP in case the index are in different order across devices.
	#"69.187.24.140"
	split(ipAddr[i],octetArr,".")
	# 187
	secondOctet[i] = octetArr[2]
	# In the example below, this logic would compare the second octet of the current value and the value previous.
	# 237 > 187
	if (i == 2) {
		if (secondOctet[i] > secondOctet[i-1]){
			firstEntry = ipAddr[1]
			secondEntry = ipAddr[2]
		} else {
			firstEntry = ipAddr[2]
			secondEntry = ipAddr[1]
		}
	}
	# "69.187.24.140""10.237.101.74"
	value = firstEntry "" secondEntry
}


END {
	writeTag("cluster-id", value)
}