#HA State:    NONE
/HA State:/ {
    sHaState = tolower($3)
    if (sHaState == "active") {
        dHaState = 1
    } else {
        dHaState = 0
    }
}

#vADC 1
/^vADC [0-9]+$/ {
    vADC = $0
}

END {
    if (length(vADC) > 0) {
        clusterTag["name"] = vADC
        writeDoubleMetric("cluster-member-active", clusterTag, "gauge", dHaState, "false")  # Converted to new syntax by change_ind_scripts.py script
    }
}