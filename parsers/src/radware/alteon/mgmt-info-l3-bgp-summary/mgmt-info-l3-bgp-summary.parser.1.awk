#  1: 10.1.1.10       0       32        0        0 never     connect  
#  2: 10.1.1.10       0       32        0        0 up     connect

/[0-9]+\:/ {
    tags["name"] = $2
    if ($7 !~ /[U|u]p/) {
        state = 0.0
    } else {
        state = 1.0
    }
    writeDoubleMetric("bgp-state", tags, "gauge", state, "false")  # Converted to new syntax by change_ind_scripts.py script
}