BEGIN{
    FS=":"
}
/hardDiskMax/ {
    #  Disk max is in GB
    totalinkb = cleanJsonValue($NF) * 1024 * 1024
    disktags["file-system"] = "base"
    writeDoubleMetric("disk-total-kbytes", disktags, "gauge", totalinkb, "true", "Disk - Total", "kilobytes")  # Converted to new syntax by change_ind_scripts.py script
}