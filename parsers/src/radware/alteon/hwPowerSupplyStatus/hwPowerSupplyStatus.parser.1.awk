BEGIN{
    FS=":"
}

/hwPowerSupplyStatus/ {
# Power supply states:
# singlePowerSupplyOk(1)
# firstPowerSupplyFailed(2)
# secondPowerSupplyFailed(3)
# doublePowerSupplyOk(4)
# unknownPowerSupplyFailed(5)
# singlePowerSupplyFailed(6)
# singlePowerSupplyConnected (7)
    orig_state = cleanJsonValue($NF)
    if (orig_state == "1" || orig_state == "4") {
        state = 1.0
    } else if (orig_state != "0") {
        state = 0.0
    }

    if (state != "") {
        hwelementtags["name"] = "Power Supplies"
        writeDoubleMetric("hardware-element-status", hwelementtags, "gauge", state, "true", "Hardware", "state", "name")  # Converted to new syntax by change_ind_scripts.py script
    }
}