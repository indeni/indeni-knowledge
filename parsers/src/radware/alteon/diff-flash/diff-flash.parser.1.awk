BEGIN {
}

/Pending/ {
	writeDoubleMetric("config-unsaved", null, "gauge", 1.0, "true", "Configuration Left Unsaved", "boolean", "")  # Converted to new syntax by change_ind_scripts.py script
}

/identical/ {
	writeDoubleMetric("config-unsaved", null, "gauge", 0.0, "true", "Configuration Left Unsaved", "boolean", "")  # Converted to new syntax by change_ind_scripts.py script
}