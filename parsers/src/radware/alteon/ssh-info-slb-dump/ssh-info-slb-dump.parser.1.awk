BEGIN{
    FS = ","
}

#1: 100.100.100.1, 123, 00:00:00:00:00:00,  vlan , port , health inherit, FAILED
/FAILED|RUNNING|DISABLED/ {
    #1: 100.100.100.1
    split($1, arr, ":")
    srvIndex = trim(arr[1])
    runtimeStatus = trim($NF)
    #"runtimeStatus -> srvStatus mapping": {RUNNING->1, FAILED|DISABLED->0}
    if (runtimeStatus == "RUNNING") {
        srvStatus = 1
    } else {
        srvStatus = 0
    }
}

#    Real Server Group 1, health tcp (runtime ICMP)
/Real Server Group/ {
    #"   Real Server Group 1"
    split(ltrim($1), arr, " ")
    groupIndex = arr[4]
    if (! (groupIndex in groupSrvCount)) {
        # groupIndex not in the list, add it and initilize it
        groupSrvCount[groupIndex] = 0
        groupRunningSrvCount[groupIndex] = 0
    }
    groupSrvCount[groupIndex]++
    if (srvStatus == 1) {
        groupRunningSrvCount[groupIndex]++
    }
   
    # server state metric per each group 
    serverTags["name"] = srvIndex
    serverTags["pool-name"] = groupIndex
    writeDoubleMetric("lb-pool-member-state", serverTags, "gauge", srvStatus, "false")  # Converted to new syntax by change_ind_scripts.py script
}

END {
    for (groupIndex in groupSrvCount) {
        groupTags["name"] = groupIndex
        percentile = 100.0 * groupRunningSrvCount[groupIndex] / groupSrvCount[groupIndex]
        writeDoubleMetric("lb-pool-capacity", groupTags, "gauge", percentile, "false")  # Converted to new syntax by change_ind_scripts.py script
    }
}