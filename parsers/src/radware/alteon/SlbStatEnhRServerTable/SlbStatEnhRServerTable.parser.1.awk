BEGIN{
    FS=":"
}
/Index/ {
    indx = cleanJsonValue($NF)
    servertags["name"] = indx
}
/CurrSessions/ {
    cursessions = cleanJsonValue($NF)
    writeDoubleMetric("lb-server-concurrent-connections", servertags, "gauge", cursessions, "true", "Real Servers - Concurrent Connections", "number", "name")  # Converted to new syntax by change_ind_scripts.py script
}