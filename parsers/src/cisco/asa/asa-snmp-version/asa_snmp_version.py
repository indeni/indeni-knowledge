from parser_service.public.helper_methods import *
from parser_service.public.base_parser import BaseParser


class AsaSnmpVersion(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        data = parse_data_as_list(raw_data, 'asa_snmp_version.textfsm')
        is_snmp_v1_v2 = 'false'
        for entry in data:
            if entry['security'] in ['v1', 'v2c']:
                is_snmp_v1_v2 = 'true'
                break

        self.write_complex_metric_string('unencrypted-snmp-configured', {}, is_snmp_v1_v2, True, 'SNMP unencrypted version')

        return self.output
