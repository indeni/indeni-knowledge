from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser
import re


class Coredump(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):

        # Loop For each line of the input
        parent_dir = ''
        tags = {}
        dumpfiles = []
        is_coredumping_enabled = 0.0

        for line in iter(raw_data.splitlines()):

            if line.startswith('Coredump Filesystem Size is'):
                is_coredumping_enabled = 1.0
                continue

            # Directory of disk0:/coredumpfsys/
            # Split line into columns and read parent directory
            arr_line_columns = line.split()
            if len(arr_line_columns) == 3 and arr_line_columns[0] == "Directory":
                parent_dir = arr_line_columns[2]
                if not parent_dir.endswith('/'):
                    parent_dir = parent_dir + '/'
                continue

            # 246    -rwx  20205386    19:16:44 Nov 26 2008  core_lina.1227726922.258.11.gz
            # -rw------- 1 root root 1230112689 May  2 10:20 core.xxxxx.xxxx.xxxxxxxxxxxx.gz
            result = re.search('.*\s(?P<name>.*\.gz)', line)
            if result is not None:
                dumpfiles.append({'path': parent_dir + result.group('name')})
                tags['path'] = parent_dir + result.group('name')
                self.write_double_metric('asa-core-dumps', tags, 'gauge', 0, False)

        if len(dumpfiles) == 0:
            tags['path'] = ''
            self.write_double_metric('asa-core-dumps', tags, 'gauge', 1, False)

        self.write_double_metric('total-core-dumps', {}, 'gauge', len(dumpfiles), True, 'Core Dump total files', 'number', None)
        self.write_double_metric('coredumping-enabled', {}, 'gauge', is_coredumping_enabled, True, 'Core Dump configuration status', 'state', None)

        return self.output
