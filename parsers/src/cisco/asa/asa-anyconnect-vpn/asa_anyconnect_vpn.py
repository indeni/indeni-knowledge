from parser_service.public.base_parser import BaseParser
from parser_service.public.helper_methods import *


class AsaAnyconnectVpn(BaseParser):

    def _get_key_value(self, key: str, data: dict):
        if key in data and data[key] != '':
            return data[key]
        return None

    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        data = parse_data_as_list(raw_data, 'asa_anyconnect_vpn.textfsm')

        # Dictionary, contain the number of connection per username
        map_connection_info = {}
        total_connected_users = 0

        # First iterate through them entries to COUNT the total number of connection per-username
        for entry in data:
            username = self._get_key_value('username', entry)
            total_connected_users = total_connected_users + 1
            prefix = ''
            if username in map_connection_info:
                prefix = ' and '
            map_connection_info[username] = map_connection_info.get(username, 'connected from ') + prefix + 'Remote IP:' + self._get_key_value('ip_remote', entry) + \
                                            ' Local IP:' + self._get_key_value('ip_local', entry) + ' at ' + self._get_key_value('login_time', entry)
        # Publish the count of the connected users
        self.write_double_metric('total-anyconnect-vpn-users', {}, 'gauge', total_connected_users, True, "Total anyconnect users")

        # Publish values using dictionaries
        if total_connected_users == 0:
            self.write_complex_metric_string('total-anyconnect-vpn-users-liveconfig', {}, 'No users found', True, 'Anyconnect Users')
        else:
            for username in map_connection_info:
                tags = {'name':'User: '+username, 'im.identity-tags':'name'}
                self.write_complex_metric_string('total-anyconnect-vpn-users-liveconfig', tags, map_connection_info[username], True, 'Anyconnect Users')
        return self.output


