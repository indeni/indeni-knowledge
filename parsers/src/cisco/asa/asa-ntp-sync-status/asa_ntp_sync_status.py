import re

from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class AsaNtpSyncStatus(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):

        # Loop For each line of the input
        for line in iter(raw_data.splitlines()):

            # Split line into column
            arr_line_columns = line.split()

            # Ignore header line
            if len(arr_line_columns) != 9 or (len(arr_line_columns) > 0 and arr_line_columns[0] == 'address'):
                continue

            # Read server-name (column-0) and value (column-5)
            server_name = arr_line_columns[0]
            is_reach_not_zero = 1.0
            if float(arr_line_columns[5]) == 0:
                is_reach_not_zero = 0.0

            # Remove not needed character from server_name example: +8.8.8.8 -> 8.8.8.8
            server_name_clear = re.search('[*#+\-~]*(?P<name>.*)', server_name).group('name')

            tags = {'name': server_name_clear}

            # Publish value in live-config, under category 'NTP Server'
            self.write_double_metric('ntp-server-state', tags, 'gauge', is_reach_not_zero, True, 'NTP Servers', 'state',
                                     'name')

        return self.output
