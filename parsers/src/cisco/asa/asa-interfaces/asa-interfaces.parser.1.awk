#interface name
/^1\.3\.6\.1\.2\.1\.2\.2\.1\.2\./ {
   key = getSnmpOid($0)
   value = getSnmpValue($0)
   interface_index = extractTrailingOid(key, 11)
   if (value ~ /'/) {
      sub(/^[^']*'/,"", value)
      sub(/'.*$/,"", value)
   }
   if (interface_name[value] == 1) {
      value = value "-" interface_index
   }
   interface_name[value] = 1
   interface_index_to_name[interface_index] = value
   next
}

# Network-interface-type - we need to monitor type 6 (Ethernet)
/^1\.3\.6\.1\.2\.1\.2\.2\.1\.3\./ {
   key_mtu = getSnmpOid($0)
   value = getSnmpValue($0)
   interface_index = extractTrailingOid(key_mtu, 11)
   interface_type[interface_index] = value
   next
}

# check if the interface type is ethernet
# if it's not then skip to the next entry
/^1\.3\.6\.1\.2\.1\.2\.2\.1\./ {
   key_mtu = getSnmpOid($0)
   interface_index = extractTrailingOid(key_mtu, 11)
   if (interface_type[interface_index] != 6) {
      next
   }
}

#network-interface-mtu
/^1\.3\.6\.1\.2\.1\.2\.2\.1\.4\./ {
   key_mtu = getSnmpOid($0)
   network_interface_mtu = getSnmpValue($0)
   interface_index = extractTrailingOid(key_mtu, 11)
   mtu_tags["name"] = interface_index_to_name[interface_index]
   mtu_tags["im.identity-tags"] = "name"
   writeComplexMetricString("network-interface-mtu", mtu_tags, network_interface_mtu, "true", "Network Interface MTU" )
}

#network-interface-speed
/^1\.3\.6\.1\.2\.1\.2\.2\.1\.5\./ {
   key_speed = getSnmpOid($0)
   network_interface_speed = getSnmpValue($0) / 1000000 "M"
   interface_index = extractTrailingOid(key_speed, 11)
   speed_tags["name"] = interface_index_to_name[interface_index]
   speed_tags["im.identity-tags"] = "name"
   writeComplexMetricString("network-interface-speed", speed_tags, network_interface_speed, "true", "Network Interface Speed" )
}

#network-interface-mac
/^1\.3\.6\.1\.2\.1\.2\.2\.1\.6\./ {
   key_mac = getSnmpOid($0)
   network_interface_mac = getSnmpValue($0)
   interface_index = extractTrailingOid(key_mac, 11)
   mac_tags["name"] = interface_index_to_name[interface_index]
   mac_tags["im.identity-tags"] = "name" 
   writeComplexMetricString("network-interface-mac", mac_tags, network_interface_mac, "true", "Network Interface MAC Address" )
}


#network-interface-admin-state
/^1\.3\.6\.1\.2\.1\.2\.2\.1\.7\./ {
   key = getSnmpOid($0)
   value = getSnmpValue($0)
   interface_index = extractTrailingOid(key, 11)
   tags["name"] = interface_index_to_name[interface_index]
   if (value == "1" || value == "3") {
        network_interface_admin_state = 1
   } else {
        network_interface_admin_state = 0
   }
   writeDoubleMetric("network-interface-admin-state", tags, "gauge", network_interface_admin_state, "true", "Network Interface Admin State", "state", "name")
}

#network-interface-state
/^1\.3\.6\.1\.2\.1\.2\.2\.1\.8\./ {
   key = getSnmpOid($0)
   value = getSnmpValue($0)
   interface_index = extractTrailingOid(key, 11)
   tags["name"] = interface_index_to_name[interface_index]
   if (value == "1" || value == "3" || value == "5" || value == "6") {
        network_interface_state = 1
   } else {
        network_interface_state = 0
   }
   writeDoubleMetric("network-interface-state", tags, "gauge", network_interface_state, "true", "Network Interface State", "state", "name")
}

#/^1.3.6.1.2.1.2.2.1.9./ Interface Last change

#network-interface-rx-bits
/^1\.3\.6\.1\.2\.1\.2\.2\.1\.10\./ {
   key = getSnmpOid($0)
   value = getSnmpValue($0)
   interface_index = extractTrailingOid(key, 11)
   tags["name"] = interface_index_to_name[interface_index]
   tags["im.dstype.displayType"] = "bits"
   network_interface_rx_bits = value * 8
   writeDoubleMetric("network-interface-rx-bits", tags, "counter", network_interface_rx_bits, "true", "Network Interfaces - RX Bits", "bits", "name")
}


#network-interface-rx-packets (ifInUcastPkts + ifInNUcastPkts)
/^1\.3\.6\.1\.2\.1\.2\.2\.1\.11\./ {
     key = getSnmpOid($0)
     value = getSnmpValue($0)
     interface_index = extractTrailingOid(key, 11)
     interface_index_to_incast_packets[interface_index] = value
}

/^1\.3\.6\.1\.2\.1\.2\.2\.1\.12\./ {
   key = getSnmpOid($0)
   value = getSnmpValue($0)
   interface_index = extractTrailingOid(key, 11)
   incast_packets = interface_index_to_incast_packets[interface_index]
   tags["name"] = interface_index_to_name[interface_index]
   tags["im.dstype.displayType"] = "number"
   network_interface_rx_packets = value + incast_packets
   writeDoubleMetric("network-interface-rx-packets", tags, "counter", network_interface_rx_packets, "true", "Network Interfaces - RX Packets", "number", "name")
}



#network-interface-rx-dropped
/^1\.3\.6\.1\.2\.1\.2\.2\.1\.13\./ {
   key = getSnmpOid($0)
   value = getSnmpValue($0)
   interface_index = extractTrailingOid(key, 11)
   tags["name"] = interface_index_to_name[interface_index]
   tags["im.dstype.displayType"] = "number"
   network_interface_rx_dropped = value
   writeDoubleMetric("network-interface-rx-dropped", tags, "counter", network_interface_rx_dropped, "true", "Network Interfaces - RX Dropped", "number", "name")
}


#network-interface-rx-errors
/^1\.3\.6\.1\.2\.1\.2\.2\.1\.14\./ {
   key = getSnmpOid($0)
   value = getSnmpValue($0)
   interface_index = extractTrailingOid(key, 11)
   tags["name"] = interface_index_to_name[interface_index]
   tags["im.dstype.displayType"] = "number"
   network_interface_rx_errors = value
   writeDoubleMetric("network-interface-rx-errors", tags, "counter", network_interface_rx_errors, "true", "Network Interfaces - RX Errors", "number", "name")
}


#/^1.3.6.1.2.1.2.2.1.15./ ifInUnknownProtos N/A in table

#network-interface-tx-bits
/^1\.3\.6\.1\.2\.1\.2\.2\.1\.16\./ {
   key = getSnmpOid($0)
   value = getSnmpValue($0)
   interface_index = extractTrailingOid(key, 11)
   tags["name"] = interface_index_to_name[interface_index]
   tags["im.dstype.displayType"] = "bits"
   network_interface_tx_bits = value * 8
   writeDoubleMetric("network-interface-tx-bits", tags, "counter", network_interface_tx_bits, "true", "Network Interfaces - TX Bits", "bits", "name")
}

#network-interface-tx-packets  (ifOutUcastPkts + ifOutNUcastPkts)
/^1\.3\.6\.1\.2\.1\.2\.2\.1\.17\./ {
     key = getSnmpOid($0)
     value = getSnmpValue($0)
     interface_index = extractTrailingOid(key, 11)
     interface_index_to_outcast_packets[interface_index] = value
}

/^1\.3\.6\.1\.2\.1\.2\.2\.1\.18\./ {
   key = getSnmpOid($0)
   value = getSnmpValue($0)
   interface_index = extractTrailingOid(key, 11)
   outcast_packets = interface_index_to_outcast_packets[interface_index]
   tags["name"] = interface_index_to_name[interface_index]
   tags["im.dstype.displayType"] = "number"
   network_interface_tx_packets = value + outcast_packets
   writeDoubleMetric("network-interface-tx-packets", tags, "counter", network_interface_tx_packets, "true", "Network Interfaces - TX Packets", "number", "name")
}

#network-interface-tx-dropped
/^1\.3\.6\.1\.2\.1\.2\.2\.1\.19\./ {
   key = getSnmpOid($0)
   value = getSnmpValue($0)
   interface_index = extractTrailingOid(key, 11)
   tags["name"] = interface_index_to_name[interface_index]
   tags["im.dstype.displayType"] = "number"
   network_interface_tx_dropped = value
   writeDoubleMetric("network-interface-tx-dropped", tags, "counter", network_interface_tx_dropped, "true", "Network Interfaces - TX Dropped", "number", "name")
}

#network-interface-tx-errors
/^1\.3\.6\.1\.2\.1\.2\.2\.1\.20\./ {
   key = getSnmpOid($0)
   value = getSnmpValue($0)
   interface_index = extractTrailingOid(key, 11)
   tags["name"] = interface_index_to_name[interface_index]
   tags["im.dstype.displayType"] = "number"
   network_interface_tx_errors = value
   writeDoubleMetric("network-interface-tx-errors", tags, "counter", network_interface_tx_errors, "true", "Network Interfaces - TX Errors", "number", "name")
}