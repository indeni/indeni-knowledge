#cpmCPUTotalPhysicalIndex
/^1.3.6.1.4.1.9.9.109.1.1.1.1.2./ {
    key = getSnmpOid($0)
    value = getSnmpValue($0)
    current_index = extractTrailingOid(key, 15)
    cpm_cpu_total_physical_index[current_index] = value
}

#cpmCPUTotal1minRev
/^1.3.6.1.4.1.9.9.109.1.1.1.1.7./ {
    key = getSnmpOid($0)
    value = getSnmpValue($0)
    current_index = extractTrailingOid(key, 15)
    cpm_cpu_total_1_min_rev[current_index] = value
}

END {
    number_of_cores = 0
    total_core_percentage = 0
    for (oid in cpm_cpu_total_physical_index){
        core_index = int(cpm_cpu_total_physical_index[oid])
        core_percentage = int(cpm_cpu_total_1_min_rev[oid])
        if (core_index > 0) {
            number_of_cores = number_of_cores + 1
            total_core_percentage = total_core_percentage + core_percentage
            cputags["cpu-id"] = core_index
            cputags["resource-metric"] = "false"
            cputags["cpu-is-avg"] = "false"
            writeDoubleMetric("cpu-usage", cputags, "gauge", core_percentage, "true", "CPU Usage", "percentage", "cpu-id")
        }
    }
    if (number_of_cores > 0) {
        cputags["cpu-is-avg"] = "true"
        cputags["cpu-id"] = "all-average"
        cputags["resource-metric"] = "true"
        average_cpu_percentage = int(total_core_percentage/number_of_cores)
        writeDoubleMetric("cpu-usage", cputags, "gauge", average_cpu_percentage, "true", "CPU Usage", "percentage", "cpu-id")
    }
}

