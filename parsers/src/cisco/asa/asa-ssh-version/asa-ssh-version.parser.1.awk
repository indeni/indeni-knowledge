BEGIN {
    version_1_enabled = 0
}

/^Version(s?) allowed:/ {
    if (match($0,"1")) {
        version_1_enabled = 1
    }
}

END {
    tags["name"] = "SSH Version"
    writeDoubleMetric("ssh-version-1-enabled", tags, "gauge", version_1_enabled, "false", "SSH Version 1 Enabled", "state", "name")
}
