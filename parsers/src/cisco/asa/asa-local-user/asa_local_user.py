from parser_service.public.base_parser import BaseParser


class LocalUser(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):

        usernames = []
        # Loop For each line of the input
        for line in iter(raw_data.splitlines()):

            # Split line into column
            arr_line_columns = line.split()

            # Ignore header line
            # Lock-time  Failed-attempts      Locked  User
            if (len(arr_line_columns) != 4 or (len(arr_line_columns) > 0 and arr_line_columns[0] == 'Lock-time')):
                continue

            # Read user-name (column-3)
            #    -                   0       N       user2
            user_name = arr_line_columns[3]
            usernames.append({'username': user_name})

        # Publish value with name "users"
        self.write_complex_metric_object_array('users', {}, usernames, False)

        return self.output
