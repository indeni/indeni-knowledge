import re
from parser_service.public.base_parser import BaseParser


class ResourceUsage(BaseParser):

    # Parse a line that contains column[2] = 'current' and  column[4] = 'limit'
    # And return the result of  (current / limit) * 100 (if applicable)
    def calculate_usage(self, prefix, line):

        # The prefix + \s{2,} is needed in order to avoid any 'prefix [rate]' lines. For example, i only need the first line and avoid the second:
        # Conns   4 36 100 0 System
        # Conns [rate] 0 3 N/A 0 System
        result = re.search(prefix + '\s{2,}(?P<current>\S*)\s+\S*\s+(?P<limit>\S*)', line)
        if result is not None:
            current = result.group('current')
            limit = result.group('limit')
            try:
                current = int(current)
                limit = int(limit)
            except ValueError:
                current = None
                limit = None
            if current is not None and limit is not None and limit > 0:
                return 100 * (current / limit)
        return None

    # Parse number or return None
    def parse_number(self, number_str):
        value = None
        if number_str is not None:
            try:
                value = int(number_str)
            except ValueError:
                value = None
        return value

    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):

        # Cache the parsed data, for optimization (avoid searching again if we have already process the metric)
        asdm_usage = None
        ssh_usage = None
        nat_usage = None

        for line in iter(raw_data.splitlines()):

            # Parse 'routes' related metrics
            # Routes                        13          13  unlimited             0 System
            result = re.search('Routes\s+(?P<routes_usage>\S*)\s+\S*\s+(?P<routes_limit>\S*)', line)
            if result is not None:
                routes_usage = self.parse_number(result.group('routes_usage'))
                routes_limit = self.parse_number(result.group('routes_limit'))
                if routes_usage is not None:
                    self.write_double_metric('routes-usage', {}, 'gauge', routes_usage, True, 'Routes Usage', 'number', None)
                if routes_limit is not None:
                    self.write_double_metric('routes-limit', {}, 'gauge', routes_limit, True, 'Routes Limit', 'number', None)

            # Parse 'connection' related metrics
            # Conns                          1           5     250000             0 System
            # Avoid This one:
            # Conns [rate]                  12        2817        N/A             0 System
            result = re.search('Conns\s{2,}(?P<connections_concurrent>\S*)\s+\S*\s+(?P<connections_limit>\S*)', line)
            if result is not None:
                connections_concurrent = self.parse_number(result.group('connections_concurrent'))
                connections_limit = self.parse_number(result.group('connections_limit'))
                if connections_concurrent is not None:
                    self.write_double_metric('concurrent-connections', {}, 'gauge', connections_concurrent, True, 'Current Connections Usage', 'number', None)
                if connections_limit is not None:
                    self.write_double_metric('concurrent-connections-limit', {}, 'gauge', connections_limit, True, 'Current Connections Limit', 'number', None)

            # Parse 'ASDM' Usage
            # ASDM                           0           3         30             0 System
            if asdm_usage is None:
                asdm_usage = self.calculate_usage('ASDM', line)
                if asdm_usage is not None:
                    self.write_double_metric('asdm-usage', {}, 'gauge', asdm_usage, True, 'ASDM Usage', 'percentage', None)

            # Parse 'SSH' Usage
            # SSH Server                     1           5          5             0 System
            if ssh_usage is None:
                ssh_usage = self.calculate_usage('SSH Server', line)
                if ssh_usage is not None:
                    self.write_double_metric('ssh-usage', {}, 'gauge', ssh_usage, True, 'SSH Usage', 'percentage', None)

            # Parse 'NAT' Usage
            # Xlates                         2           2        N/A             0 System
            if nat_usage is None:
                nat_usage = self.calculate_usage('Xlates', line)
                if nat_usage is not None:
                    self.write_double_metric('nat-usage', {}, 'gauge', nat_usage, True, 'NAT Usage', 'percentage', None)

        return self.output
