
/cn=/ {
    name_index = match($0, /=/) + 1
	tags["name"] = substr($0, name_index)
}

/end\s+date:/ {
    split($3,time,":")
    hour = time[1]
    minute = time[2]
    second = time[3]
    zone = $4
    month = (match("JanFebMarAprMayJunJulAugSepOctNovDec",$5)+2)/3
    day = $6
    year = $7
    exp_date = datetime(year, month, day, hour, minute, second)
    writeDoubleMetric("certificate-expiration", tags, "gauge", exp_date, "true", "Certificate Expiration Date", "date", "name")
}
