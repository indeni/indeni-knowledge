BEGIN {
    array_size = -1
}

#    Crypto map tag: outside_map, seq num: 1, local addr: 9.187.120.234
/Crypto map/ {
    array_size++
    array_ipsec[array_size,"local-addr"] = $NF
}

#      current_peer: 2.23.139.254
/current_peer:/{
    array_ipsec[array_size,"current-peer"] = $NF
}

#      spi: 0xFCD38A1F (4241721887)
#    spi: 0x136A010F(325714191)
/^\s*spi:/{
    #store the '0xFCD38A1F'. Always the first is the inbound-spi
    count = split($0, key_value, " ")
    if(count == 2){
        split(key_value[2], key_value, "(")
        value = key_value[1]
    } else {
        value = key_value[2]
    }

    if(array_ipsec[array_size,"inbound-spi"] == "")
        array_ipsec[array_size,"inbound-spi"] = value
    else
        array_ipsec[array_size,"outbound-spi"] = value
}

#      #pkts encaps: 28, #pkts encrypt: 28, #pkts digest: 28
/#pkts encrypt:/{
    split($0, pkts_types, ", ")
    split(pkts_types[2], pkts_encrypt, ":")
    array_ipsec[array_size,"pkts-encrypt"] = pkts_encrypt[2]
}

#      #pkts decaps: 28, #pkts decrypt: 28, #pkts verify: 28
/#pkts decrypt:/{
    split($0, pkts_types, ", ")
    split(pkts_types[2], pkts_decrypt, ":")
    array_ipsec[array_size,"pkts-decrypt"] = pkts_decrypt[2]
}


END {
    for (i = 0; i <= array_size ; i++) {
        tags["local-addr"] = array_ipsec[i,"local-addr"]
        tags["current-peer"] = array_ipsec[i,"current-peer"]
        tags["inbound-spi"] = array_ipsec[i,"inbound-spi"]
        tags["outbound-spi"] = array_ipsec[i,"outbound-spi"]
        tags["name"] = "IP sec tunnel Local IP " tags["local-addr"] " - Remote IP " tags["current-peer"] " with SPI-Inbound " tags["inbound-spi"] " SPI-Outbound " tags["outbound-spi"]
        writeDoubleMetric("vpn-ipsec-pkt-encrypted", tags, "gauge",  array_ipsec[i,"pkts-encrypt"], "false")
        writeDoubleMetric("vpn-ipsec-pkt-decrypted", tags, "gauge",  array_ipsec[i,"pkts-decrypt"], "false")
    }
}