import re
from parser_service.public.base_parser import BaseParser

class AsaOsfpLsaCve(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict,  device_tags: dict):

        # Find All process id's (if any)
        # Example: Routing Process "ospf 1"
        list_of_ids = re.findall('Routing Process "ospf ([\w-]+)"', raw_data)
        string_ids = ''
        is_state_alert = 0
        if len(list_of_ids) > 0:
            is_state_alert = 1
            for idx in range(len(list_of_ids)):
                string_ids += str(list_of_ids[idx])
                if idx != (len(list_of_ids)-1):
                    string_ids += ','
        else:
            string_ids = 'no processes'

        # Needed for Live Config
        self.write_complex_metric_string('asa-ospf-v2-processes', {}, string_ids, True, 'OSPF V2 Processes')

        # Needed for rule generation
        self.write_double_metric('asa-ospf-v2-state', {}, 'gauge', is_state_alert, False)

        return self.output

