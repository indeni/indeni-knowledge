BEGIN {
    dns_server_index = 0
}

/name-server/ {
    dns_server_index++
    ip = $2
    if (ips[ip] != "true"){
        dns_servers[dns_server_index, "ipaddress"] = ip
        ips[ip] = "true"
    }
}

END {
	writeComplexMetricObjectArray("dns-servers", null, dns_servers, "true", "DNS Servers")  # Converted to new syntax by change_ind_scripts.py script
}


