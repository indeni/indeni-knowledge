name: cisco-asa-memory-usage
description: Fetch memory usage for ASA device
type: monitoring
monitoring_interval: 1 minute
includes_resource_data: true
requires:
  vendor: cisco
  os.name: asa
  snmp: true
comments:
  memory-usage:
    why: |
      Capture the total utilization of the system's memory (RAM) resources. This information is critical for monitoring the system's health and make sure memory is not over utilized. If memory utilization goes beyond a certain threshold an alert will be triggered.
    how: |
      This script polls the ciscoMemoryPoolTable via SNMP to calculate memory utilization.
    can-with-snmp: true
    can-with-syslog: false
  memory-free-kbytes:
    why: |
      Capture the free amount of the system's memory (RAM) resources. This information is critical for monitoring the system's health and make sure memory is not over utilized. If memory utilization goes beyond a certain threshold an alert is triggered.
    how: |
      This script polls the ciscoMemoryPoolTable via SNMP to calculate the free kbytes of the memory. This metric indicates the number of bytes from the memory pool that are currently available on the managed device.
    can-with-snmp: true
    can-with-syslog: false
  memory-total-kbytes:
    why: |
      Capture the total amount of the system's memory (RAM) resources. This information is critical for monitoring the system's health and make sure memory is not over utilized. If memory utilization goes beyond a certain threshold an alert is triggered.
    how: |
      This script polls the ciscoMemoryPoolTable via SNMP to calculate the total kbytes of the memory. The sum of ciscoMemoryPoolUsed and ciscoMemoryPoolFree is the total amount of memory in the pool.
    can-with-snmp: true
    can-with-syslog: false
  memory-used-kbytes:
    why: |
      Capture the used amount of the system's memory (RAM) resources. This information is critical for monitoring the system's health and make sure memory is not over utilized. If memory utilization goes beyond a certain threshold an alert is triggered.
    how: |
      This script polls the ciscoMemoryPoolTable via SNMP to calculate the used kbytes of the memory. This metric indicates the number of bytes from the memory pool that are currently in use on the managed device.
    can-with-snmp: true
    can-with-syslog: false
steps:
-   run:
      type: SNMP
      command: GETBULK --columns 1.3.6.1.4.1.9.9.48.1.1.1.2 1.3.6.1.4.1.9.9.48.1.1.1.5 1.3.6.1.4.1.9.9.48.1.1.1.6
    parse:
      type: AWK
      file: asa-memory-usage.parser.1.awk
