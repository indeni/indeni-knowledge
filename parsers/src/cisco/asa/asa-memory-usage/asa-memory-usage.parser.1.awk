function bytes_to_kilobytes(value) {
    return int(value / 1024)
}

BEGIN {
    system_memory_pool_index = "NaN"
}

#ciscoMemoryPoolName
/^1.3.6.1.4.1.9.9.48.1.1.1.2./ {
    key = getSnmpOid($0)
    value = getSnmpValue($0)
    current_index = extractTrailingOid(key, 14)
    if (value == "System memory") {
        system_memory_pool_index = current_index
    }
}

#ciscoMemoryPoolUsed
/^1.3.6.1.4.1.9.9.48.1.1.1.5./ {
    key = getSnmpOid($0)
    value = getSnmpValue($0)
    current_index = extractTrailingOid(key, 14)
    memory_pool_used[current_index] = value
}

#ciscoMemoryPoolFree
/^1.3.6.1.4.1.9.9.48.1.1.1.6./ {
    key = getSnmpOid($0)
    value = getSnmpValue($0)
    current_index = extractTrailingOid(key, 14)
    memory_pool_free[current_index] = value
}

END {
    if (system_memory_pool_index != "NaN") {
        used_ram = bytes_to_kilobytes(memory_pool_used[system_memory_pool_index])
        free_ram = bytes_to_kilobytes(memory_pool_free[system_memory_pool_index])
        total_ram = used_ram + free_ram
        usage_ram = int((used_ram / total_ram) * 100)

        ramtag["name"] = "RAM"
        writeDoubleMetric("memory-free-kbytes", ramtag, "gauge", free_ram, "true", "Free Memory", "kilobytes", "name")
        writeDoubleMetric("memory-used-kbytes", ramtag, "gauge", used_ram, "true", "Used Memory", "kilobytes", "name")
        writeDoubleMetric("memory-total-kbytes", ramtag, "gauge", total_ram, "true", "Total Memory", "kilobytes", "name")

        ramtag_memory_usage["name"] = "RAM"
        ramtag_memory_usage["resource-metric"] = "true"
        writeDoubleMetric("memory-usage", ramtag_memory_usage, "gauge", usage_ram, "true", "Memory Usage", "percentage", "name")
    }
}
