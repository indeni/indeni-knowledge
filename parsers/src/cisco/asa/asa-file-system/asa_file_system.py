from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser
import re


class FileSystem(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        for line in iter(raw_data.splitlines()):
            # * 8571076608    8549339136    disk      rw      disk0: flash:
            result = re.search(r'(?P<size>[0-9]+)\s+(?P<free>[0-9]+)\s+\S*\s+\S*\s+(?P<prefixes>.*$)', line)
            if result is not None:
                bytes_size = int(result.group('size'))
                bytes_free = int(result.group('free'))
                fs_prefixes = result.group('prefixes')

                out_disk_used = bytes_size - bytes_free

                if bytes_size != 0:
                    out_disk_usage = (out_disk_used / bytes_size) * 100
                    self.write_double_metric('disk-usage-percentage', {'name': 'Utilization mount point ' + fs_prefixes, 'file-system': fs_prefixes},
                                             'gauge', out_disk_usage, True, 'File Systems Usage', 'percentage', 'name')

                self.write_double_metric('disk-used-kbytes', {'name': 'Total used size for ' + fs_prefixes, 'file-system': fs_prefixes},
                                         'gauge', out_disk_used / 1024, True, 'File Systems Usage', 'kbytes', 'name')
                self.write_double_metric('disk-total-kbytes', {'name': 'Total size for ' + fs_prefixes, 'file-system': fs_prefixes}, 'gauge',
                                         bytes_size / 1024, True, 'File Systems Usage', 'kbytes', 'name')

        return self.output
