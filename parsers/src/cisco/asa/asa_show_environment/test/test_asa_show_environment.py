import os
import unittest

from cisco.asa.asa_show_environment.show_environment import CiscoAsaShowEnvironment


class TestCiscoAsaShowEnvironment(unittest.TestCase):

    def setUp(self):
        self.parser = CiscoAsaShowEnvironment()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_all_sensors_ok(self):
        data_parsed = self.parser.parse_file(self.current_dir + '/all_sensors_ok.input', {},{})
        self.assertEqual(44,len(data_parsed))


        self.assertEqual(data_parsed[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(data_parsed[0].name, 'hardware-element-status')
        self.assertEqual(data_parsed[0].value, 1)
        self.assertEqual(data_parsed[0].tags['name'], 'Chassis Fans - Cooling Fan 1')        
        self.assertEqual(data_parsed[0].tags['display-name'], 'Fan Status')

        self.assertEqual(data_parsed[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(data_parsed[1].name, 'fan-rotation-speed')
        self.assertEqual(data_parsed[1].value, 5632)
        self.assertEqual(data_parsed[1].tags['name'], 'Chassis Fans - Cooling Fan 1')        
        self.assertEqual(data_parsed[1].tags['display-name'], 'Fan RPM')

        self.assertEqual(data_parsed[2].action_type, 'WriteDoubleMetric')
        self.assertEqual(data_parsed[2].name, 'hardware-element-status')
        self.assertEqual(data_parsed[2].value, 1)
        self.assertEqual(data_parsed[2].tags['name'], 'Chassis Fans - Cooling Fan 2')        
        self.assertEqual(data_parsed[2].tags['display-name'], 'Fan Status')

        self.assertEqual(data_parsed[3].action_type, 'WriteDoubleMetric')
        self.assertEqual(data_parsed[3].name, 'fan-rotation-speed')
        self.assertEqual(data_parsed[3].value, 5632)
        self.assertEqual(data_parsed[3].tags['name'], 'Chassis Fans - Cooling Fan 2')        
        self.assertEqual(data_parsed[3].tags['display-name'], 'Fan RPM')

        self.assertEqual(data_parsed[4].action_type, 'WriteDoubleMetric')
        self.assertEqual(data_parsed[4].name, 'hardware-element-status')
        self.assertEqual(data_parsed[4].value, 1)
        self.assertEqual(data_parsed[4].tags['name'], 'Chassis Fans - Cooling Fan 3')        
        self.assertEqual(data_parsed[4].tags['display-name'], 'Fan Status')

        self.assertEqual(data_parsed[5].action_type, 'WriteDoubleMetric')
        self.assertEqual(data_parsed[5].name, 'fan-rotation-speed')
        self.assertEqual(data_parsed[5].value, 5888)
        self.assertEqual(data_parsed[5].tags['name'], 'Chassis Fans - Cooling Fan 3')        
        self.assertEqual(data_parsed[5].tags['display-name'], 'Fan RPM')

        self.assertEqual(data_parsed[14].action_type, 'WriteDoubleMetric')
        self.assertEqual(data_parsed[14].name, 'hardware-element-status')
        self.assertEqual(data_parsed[14].value, 1)
        self.assertEqual(data_parsed[14].tags['name'], 'Left Slot (PS0)')        
        self.assertEqual(data_parsed[14].tags['display-name'], 'Power Supply Status')

        self.assertEqual(data_parsed[15].action_type, 'WriteDoubleMetric')
        self.assertEqual(data_parsed[15].name, 'hardware-element-status')
        self.assertEqual(data_parsed[15].value, 1)
        self.assertEqual(data_parsed[15].tags['name'], 'Right Slot (PS1)')        
        self.assertEqual(data_parsed[15].tags['display-name'], 'Power Supply Status')

        self.assertEqual(data_parsed[16].action_type, 'WriteDoubleMetric')
        self.assertEqual(data_parsed[16].name, 'hardware-element-status')
        self.assertEqual(data_parsed[16].value, 0)
        self.assertEqual(data_parsed[16].tags['name'], 'Left Slot (PS0) - ')        
        self.assertEqual(data_parsed[16].tags['display-name'], 'Temperature Status')

        self.assertEqual(data_parsed[17].action_type, 'WriteDoubleMetric')
        self.assertEqual(data_parsed[17].name, 'temperature-sensor-value')
        self.assertEqual(data_parsed[17].value, '27')
        self.assertEqual(data_parsed[17].tags['name'], 'Left Slot (PS0) - ')        
        self.assertEqual(data_parsed[17].tags['display-name'], 'Temperature Sensor (C)')

if __name__ == '__main__':
    unittest.main()