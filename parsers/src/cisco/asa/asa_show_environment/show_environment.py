from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class CiscoAsaShowEnvironment(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):

        fan_data = helper_methods.parse_data_as_list(raw_data, 'show_environment_fan.textfsm')
        power_status_data = helper_methods.parse_data_as_list(raw_data, 'show_environment_power_supply_status.textfsm')
        power_insert_data = helper_methods.parse_data_as_list(raw_data, 'show_environment_power_supply_insert.textfsm')
        temperature_data = helper_methods.parse_data_as_list(raw_data, 'show_environment_temperature.textfsm')
        voltage_data = helper_methods.parse_data_as_list(raw_data, 'show_environment_voltage.textfsm')


        # Step 2 : Data Processing and Reporting
        if fan_data:
            for fan in fan_data:
                tags = {}
                tags['name'] = '{} - {}'.format(fan['location'], fan['name'])
                self.write_double_metric('hardware-element-status', tags, 'state', 1 if fan['status'] in ['OK','none'] else 0, True, 'Fan Status', 'name')
                self.write_double_metric('fan-rotation-speed', tags, 'gauge', int(fan['rpm']), True, 'Fan RPM', 'name')

            for psu_unit in power_status_data:
                tags = {}
                tags['name'] = '{}'.format(psu_unit['name'])
                if psu_unit['status'] in ['ok', 'OK']:
                    self.write_double_metric('hardware-element-status', tags, 'state', 1, True, 'Power Supply Status', 'name')
                else:
                    for psu_unit_2 in power_insert_data:
                        if psu_unit_2['name'] == psu_unit['name']:
                            self.write_double_metric('hardware-element-status', tags, 'state', 1 if psu_unit_2['inserted'] in ['absent'] else 0, True, 'Power Supply Status', 'name')
            
            for temp_sensor in temperature_data:
                tags = {}
                tags['name'] = '{} - {}'.format(temp_sensor['name'], temp_sensor['name_secondary'])
                self.write_double_metric('hardware-element-status', tags, 'state', 1 if temp_sensor['status'] in ['OK','none'] else 0, True, 'Temperature Status', 'name')
                self.write_double_metric('temperature-sensor-value', tags, 'gauge', temp_sensor['value'].split(' ' )[0], True, 'Temperature Sensor ({})'.format(temp_sensor['value'].split(' ')[1]), 'name')

            for volt_sensor in voltage_data:
                tags = {}
                tags['name'] = '{} - {}'.format(volt_sensor['name'], volt_sensor['detail'])
                self.write_double_metric('hardware-element-status', tags, 'state', 1 if volt_sensor['status'] in ['OK','none'] else 0, True, 'Temperature Status', 'name')
                self.write_double_metric('voltage-sensor-value', tags, 'gauge', volt_sensor['voltage'], True, 'Temperature Sensor (V)', 'name')

        return self.output
