BEGIN {
    array_size = -1
}

/^\s?[1-9]\s/ {
    array_size++
    array_tunnel[array_size,"ip"] = $NF
}

#    Type    : L2L             Role    : initiator
/Type/{
    split($0, key_value, ":")
    split(key_value[2], values, " ")
    array_tunnel[array_size, "type"] = values[1]
}

#    Rekey   : no              State   : MM_ACTIVE
/State/{
    array_tunnel[array_size, "state"] = $NF
}

END {
    for (i = 0; i <= array_size ; i++) {
        tags["IKE-peer-ip"] = array_tunnel[i,"ip"]
        tags["type"] = array_tunnel[i,"type"]
        tags["state"] = array_tunnel[i,"state"]
        tags["name"] = "Peer IP " tags["IKE-peer-ip"] " IPsec type " tags["type"] " IKE state " tags["state"]
        value = (tags["state"] == "MM_ACTIVE" || tags["state"] == "AM_ACTIVE" ) ? 1 : 0
        writeDoubleMetric("vpn-ike-state", tags, "gauge", value, "true", "ISAKMP SA Tunnel State", "state" , "name")
    }
}