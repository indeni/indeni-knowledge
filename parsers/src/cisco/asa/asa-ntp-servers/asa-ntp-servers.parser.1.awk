BEGIN {
intp = 0
}
/(?:[0-9]{1,3}\.){3}[0-9]{1,3}/ {
	intp++
	dirty_ip = $1
	start_ip_index = match($1, /[0-9]/)
	clean_ip = substr($1, start_ip_index)
	ntps[intp, "ipaddress"] = clean_ip
}

END {
	writeComplexMetricObjectArray("ntp-servers", null, ntps, "true", "NTP Servers")  # Converted to new syntax by change_ind_scripts.py script
}