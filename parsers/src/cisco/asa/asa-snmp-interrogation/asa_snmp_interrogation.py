from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

# 1.3.6.1.2.1.47.1.1.1.1.2.1 = ASA 5516-X with FirePOWER services, 8GE, AC, DES
# 1.3.6.1.2.1.47.1.1.1.1.10.1 = 9.13(1)
# 1.3.6.1.2.1.47.1.1.1.1.11.1 = JMX1943Z10U
# 1.3.6.1.2.1.47.1.1.1.1.12.1 = cisco Systems Inc.
# 1.3.6.1.2.1.47.1.1.1.1.13.1 = ASA5516


class ASAInterrogation(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        entPhysicalMfgName = ''
        entPhysicalDescr = ''
        lines = raw_data.splitlines()
        for line in lines:
            if line.startswith('1.3.6.1.2.1.47.1.1.1.1.2.1 '):  # entPhysicalDescr
                entPhysicalDescr = line.split("= ")[1]
            elif line.startswith('1.3.6.1.2.1.47.1.1.1.1.10.1 '):  # entPhysicalSoftwareRev
                entPhysicalSoftwareRev = line.split("= ")[1]
            elif line.startswith('1.3.6.1.2.1.47.1.1.1.1.11.1 '):  # entPhysicalSerialNum
                entPhysicalSerialNum = line.split("= ")[1]
            elif line.startswith('1.3.6.1.2.1.47.1.1.1.1.12.1 '):  # entPhysicalMfgName
                entPhysicalMfgName = line.split("= ")[1]
            elif line.startswith('1.3.6.1.2.1.47.1.1.1.1.13.1 '):  # entPhysicalModelName
                entPhysicalModelName = line.split("= ")[1]

        if 'cisco Systems Inc.' in entPhysicalMfgName and ('ASA' or 'Firepower' in entPhysicalDescr):
            self.write_tag('model', entPhysicalModelName)
            self.write_tag('vendor', 'cisco')
            self.write_tag('product', 'firewall')
            self.write_tag('os.name', 'asa')
            self.write_tag('os.version', entPhysicalSoftwareRev)
            self.write_tag('serial', entPhysicalSerialNum)

        return self.output
