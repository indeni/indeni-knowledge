BEGIN {
    now_date= now()
}

#for tests
/Current Date:/ {
    now_date = datetime($3, $4, $5, $6, $7, $8)
}


/.+\s+:\s+.+\s+[0-9]+\s+days/ {
    split($0, key_value, ":")
    tags["name"] = trim(key_value[1])
    split(key_value[2], values, " ")
    expiration_date = now_date + int(values[2])*24*60*60
    writeDoubleMetric("license-expiration", tags, "gauge", expiration_date, "false")
}