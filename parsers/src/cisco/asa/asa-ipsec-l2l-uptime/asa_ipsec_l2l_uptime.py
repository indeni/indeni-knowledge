from parser_service.public import helper_methods
from parser_service.public.helper_methods import *
from parser_service.public.base_parser import BaseParser
import re


class IpsecL2lUptime(BaseParser):

    def _get_key_value(self, key: str, data: dict):
        if key in data and data[key] != '':
            return data[key]
        return None

    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        data = parse_data_as_list(raw_data, 'asa_ipsec_l2l_uptime.textfsm')

        for entry in data:
            # Duration in string is: [1d 3h:41m:56s] or [3h:41m:56s]
            # The days is optional field
            duration = self._get_key_value('duration', entry)
            if duration is not None:
                regex = re.search('((?P<duration_days>.*)d\s)?(?P<duration_hours>.*)h:(?P<duration_mins>.*)m:(?P<duration_secs>.*)s', duration)
            else:
                regex = None

            if regex is not None:
                duration_days = 0
                if regex.group('duration_days') is not None:
                    duration_days = int(regex.group('duration_days'))
                duration_hours = int(regex.group('duration_hours'))
                duration_mins = int(regex.group('duration_mins'))
                duration_secs = int(regex.group('duration_secs'))

                tags = {'name': 'Peer-ip ' + entry['ip_address'], 'peer-ip': entry['ip_address']}

                # Live config entry in 'IP sec L2L Uptime' category
                self.write_double_metric('vpn-ipsec-l2l-uptime', tags, 'gauge',
                                         (((duration_days * 24 * 60) + (duration_hours * 60) + duration_mins) * 60 + duration_secs) * 1000, True,
                                         'IP sec L2L Uptime', 'duration', 'name')

        return self.output
