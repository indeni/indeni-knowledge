# Evaluate the input text
# return 0 if the text is 'disabled' or 'none'
function getZeroIfDisabled(text) {
    if (tolower(trim(text)) == "disabled" || tolower(trim(text)) == "none")
        return 0;
    return 1;
}

BEGIN {
    # Store in table all the listed interfaces with the needed properties
    table_index = 0;

    # The VRF is not in table format
    value_vrf = "";
}

# Read the VRF (note that we don't increase the table index)
#IP Interface Status for VRF "default"
/IP Interface Status for VRF /{
    value_vrf = $NF;
}

# Read the interface name
#Vlan10, Interface status: protocol-up/link-up/admin-up, iod: 5,
/, Interface status:/ {
    table_index++;

    # Remove ',' character
    interface_name = $1;
    gsub(/,/, "", interface_name);

    table_interface[table_index, "name"] = interface_name;
    table_interface[table_index, "vrf"] = value_vrf;
}

#  IP proxy ARP : disabled
/\sIP proxy ARP :/{
    table_interface[table_index, "proxy"] = $NF;
}

#  IP Local Proxy ARP : disabled
/\sIP Local Proxy ARP :/{
    table_interface[table_index, "local-proxy"] = $NF;
}

#  IP directed-broadcast: disabled
/\sIP directed-broadcast:/{
    table_interface[table_index, "d-broadcast"] = $NF;
}

#  IP icmp redirects: disabled
/\sIP icmp redirects:/{
    table_interface[table_index, "icmp-r"] = $NF;
}

#  IP icmp unreachables (except port): disabled
/\sIP icmp unreachables \(except port\):/{
    table_interface[table_index, "icmp-un"] = $NF;
}

#  IP unicast reverse path forwarding: none
/\sIP unicast reverse path forwarding:/{
    table_interface[table_index, "ip-urpf"] = $NF;
}

END{
    # For each interface publish the stored metrics
    for (idx = 1; idx <= table_index ; idx++ ) {
        tags["name"] = table_interface[idx, "name"] " for VRF " table_interface[idx, "vrf"] ;

        writeDoubleMetric("interface-proxy-arp-status", tags, "gauge", getZeroIfDisabled(table_interface[idx, "proxy"]), "false")  # Converted to new syntax by change_ind_scripts.py script
        writeDoubleMetric("interface-local-proxy-arp-status", tags, "gauge", getZeroIfDisabled(table_interface[idx, "local-proxy"]), "false")  # Converted to new syntax by change_ind_scripts.py script
        writeDoubleMetric("interface-directed-broadcasts-status", tags, "gauge", getZeroIfDisabled(table_interface[idx, "d-broadcast"]), "false")  # Converted to new syntax by change_ind_scripts.py script
        writeDoubleMetric("interface-icmp-redirect-status", tags, "gauge", getZeroIfDisabled(table_interface[idx, "icmp-r"]), "false")  # Converted to new syntax by change_ind_scripts.py script
        writeDoubleMetric("interface-icmp-unreachable-status", tags, "gauge", getZeroIfDisabled(table_interface[idx, "icmp-un"]), "false")  # Converted to new syntax by change_ind_scripts.py script
        writeDoubleMetric("interface-urpf-status", tags, "gauge", getZeroIfDisabled(table_interface[idx, "ip-urpf"]), "false")  # Converted to new syntax by change_ind_scripts.py script
    }
}