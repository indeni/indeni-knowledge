#Publish the 'cpu-usage' (average and per cpu-id)
#     CPU0 states  :   0.0% user,   0.0% kernel,   100.0% idle
#CPU states  :   0.1% user,   0.1% kernel,   99.8% idle
/CPU.*states/ {
    cpu_id = $1
    idle = $8
    sub(/%/, "", idle)

    # if contains 'CPU states' then the line has the average values
    is_average = $0 ~ /CPU states/

    tags["cpu-id"] = cpu_id
    tags["cpu-is-avg"] = is_average ? "true" : "false"
    tags["resource-metric"] = "true"
    writeDoubleMetric("cpu-usage", tags, "gauge", 100 - idle, "true", "CPU Usage", "percentage", "cpu-id")  # Converted to new syntax by change_ind_scripts.py script
}

#Publish the memory metrics
#Memory usage:   8243096K total,   2726536K used,   5516560K free
/^Memory/ {

    total = $3
    used = $5
    free = $7

    sub(/K/, "", total)
    sub(/K/, "", used)
    sub(/K/, "", free)

    tags_memory["name"] = "Memory Free"
    writeDoubleMetric("memory-free-kbytes", tags_memory, "gauge", free, "true", "Memory Usage", "kilobytes", "name")  # Converted to new syntax by change_ind_scripts.py script
    tags_memory["name"] = "Memory Total"
    writeDoubleMetric("memory-total-kbytes", tags_memory, "gauge", total, "true", "Memory Usage", "kilobytes", "name")  # Converted to new syntax by change_ind_scripts.py script
    tags_memory["name"] = "Memory Used"
    writeDoubleMetric("memory-used-kbytes", tags_memory, "gauge", used, "true", "Memory Usage", "kilobytes", "name")  # Converted to new syntax by change_ind_scripts.py script

    tags["resource-metric"] = "true"
    mem_usage = (used / total) * 100
    tags_memory["name"] = "Memory Usage (%)"
    writeDoubleMetric("memory-usage", tags_memory, "gauge", mem_usage, "true", "Memory Usage", "percentage", "name")  # Converted to new syntax by change_ind_scripts.py script
}