BEGIN {

    # The column-name used for CPU (1Sec or 5Sec)
    columnCPU = ""

    # Index of Process List
    processListIndex = -1

    # A list with the monitored/critical processes.
    # Initialized them with '-1' meaning that the process is not found / not-set
    processListCritical["adjmgr"] = -1
    processListCritical["arp"] = -1
    processListCritical["bootvar"] = -1
    processListCritical["cisco"] = -1
    processListCritical["confcheck"] = -1
    processListCritical["core-dmon"] = -1
    processListCritical["dcos-xinetd"] = -1
    processListCritical["feature-mgr"] = -1
    processListCritical["fs-daemon"] = -1
    processListCritical["ifmgr"] = -1
    processListCritical["init"] = -1
    processListCritical["klogd"] = -1
    processListCritical["licmgr"] = -1
    processListCritical["pktmgr"] = -1
    processListCritical["rpc.statd"] = -1
    processListCritical["securityd"] = -1
    processListCritical["sysinfo"] = -1
    processListCritical["syslogd"] = -1
    processListCritical["sysmgr"] = -1
    processListCritical["ttyd"] = -1
    processListCritical["xinetd"] = -1
}

# Table Option 1 (Header with '1Sec')
#PID    Runtime(ms)  Invoked   uSecs  1Sec    Process
#
# Table Option 2 (Header with '5Sec')
#PID    Runtime(ms)  Invoked   uSecs  5Sec    1Min    5Min    TTY  Process
/(PID|Runtime)/ {

    # Parse the line into a column array. The second parameter is the separator between column names.
    # The last parameter is the array to save the data into.
    getColumns(trim($0), "[ \t]+", columns)

    # Store the column-name used to store the "cpu" (1Sec OR 5Sec)
    if ($0 ~ "1Sec") {
        columnCPU = "1Sec"
    } else if ($0 ~ "5Sec") {
        columnCPU = "5Sec"
    }
}

# Table Option 1 (cpu column is stored in 1Sec)
#PID    Runtime(ms)  Invoked   uSecs  1Sec    Process
#-----  -----------  --------  -----  ------  -----------
#    2            9      1052      8    0.0%  kthreadd
#
#Table Option 2 (cpu column is stored in 5Sec)
# CPU utilization for five seconds: 17%/3%; one minute: 18%; five minutes: 19%
#PID    Runtime(ms)  Invoked   uSecs  5Sec    1Min    5Min    TTY  Process
#-----  -----------  --------  -----  ------  ------  ------  ---  -----------
#    1        18510    369097      0   0.00%   0.00%  0.00%   -    init
# 3266            0         5      0   0.00%   0.00%  0.00%   -    portmap
/^\s*[0-9]+.*%/ {

    # Read input line and trim
    trimmedLine = trim($0)

    # Parse CPU. Note that the value is stored in column-name '1Sec' or '5Sec'.
    cpu = getColData(trimmedLine, columns, columnCPU)
    sub(/%/, "", cpu)

    # Read Process Name
    processName = getColData(trimmedLine, columns, "Process")

    # Increase List Index
    processListIndex++

    # Store Process Info in List
    processList[processListIndex, "name"] = processName
    processList[processListIndex, "pid"] = getColData(trimmedLine, columns, "PID")
    processList[processListIndex, "cpu"] = cpu

    # Check if the process is Critical. Register the index
    if (processName in processListCritical) {
        processListCritical[processName] = processListIndex
    }
}


END {

  # A. Publish for ALL running processes the cpu
  for (indexList = 0; indexList < processListIndex+1; indexList++){
    tags["process-name"] = processList[indexList, "name"]
    tags["name"] = processList[indexList, "pid"]
    writeDoubleMetric("process-cpu", tags, "gauge", processList[indexList, "cpu"], "false")  # Converted to new syntax by change_ind_scripts.py script
  }


  # B. Publish for ALL processes in critical-list '1' if it is running '0' if it is not running
  for (pNameCritical in processListCritical) {
    tags["process-name"] = pNameCritical
    tags["description"] = "Process " pNameCritical " is required for normal system operation"
    tags["name"] = 0
    isProcessRunning = 0
    if (processListCritical[pNameCritical] != -1) {
       # The process is running because the index is set
       tags["name"] = processList[ processListCritical[pNameCritical], "pid"]
       # mark process as running
       isProcessRunning = 1
    }
    writeDoubleMetric("process-state", tags, "gauge", isProcessRunning, "false")  # Converted to new syntax by change_ind_scripts.py script
  }

}