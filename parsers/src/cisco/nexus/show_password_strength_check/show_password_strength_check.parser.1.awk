BEGIN {
    is_enabled = 0
}

#Password strength check enabled
#Password strength check not enabled
#Password strength check is disabled
#Password strength check is enabled
/Password strength check/ {

    is_enabled = !(($0 ~ /not enabled/) || ($0 ~ /disabled/))

}

END {
    tags["name"] = "Status"
    writeDoubleMetric("password-strength-status", tags, "gauge", is_enabled, "true", "Password Strength Policy", "state", "name")  # Converted to new syntax by change_ind_scripts.py script
}