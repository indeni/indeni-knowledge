#Neighbor ID     Pri State            Up Time  Address         Interface
#10.28.0.34        1 FULL/DR          40w0d    10.28.1.21      Eth1/2
/^\s*\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/ {
    neighbor_id = $1
    state = $3
    interface = $6

    if (state ~ /FULL/) {
        ospf_state = "1.0"
    } else {
        ospf_state = "0.0"
    }

    ospftags["name"] = neighbor_id
    ospftags["interface"] = $NF
    writeDoubleMetric("ospf-state", ospftags, "gauge", ospf_state, "false")  # Converted to new syntax by change_ind_scripts.py script
}