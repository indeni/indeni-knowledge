BEGIN {
    syslogIndex = 0
    serverIP = ""
    serverSeverity = ""
}

#{1.1.1.1}
/\{[0-9a-f.:]+\}/ {
    serverIP = $1
    # remove characters '{' and '}'
    gsub(/\{|\}/, "", serverIP)
}

#         server severity:        errors
/server severity/ {
    split($0, arrayLine, ":")

    serverSeverity = trim(arrayLine[2])

    if ((serverSeverity == "") || (serverIP == "")) {
        next
    }

    syslogIndex++
    syslogArray[syslogIndex, "host"] = serverIP
    syslogArray[syslogIndex, "severity"] = serverSeverity

    serverIP = ""
    serverSeverity = ""
}

END {
    writeComplexMetricObjectArray("syslog-servers", null, syslogArray, "true", "Syslog Servers")  # Converted to new syntax by change_ind_scripts.py script
}