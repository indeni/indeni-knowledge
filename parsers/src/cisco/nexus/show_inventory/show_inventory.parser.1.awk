BEGIN {
    i = 0
}

# PID: N77-C7706           ,  VID: V01 ,  SN: FXS1123Q333
/PID:.*SN:/ {
    split($0, r, ",")

    split(r[1], p, ":")
    pid = p[2]
    split(r[3], s, ":")
    sn = s[2]

    if ((sn == "") || (sn ~ "N/A")) {
        next
    }

    i++
    sn_list[i, "name"] = trim(pid)
    sn_list[i, "serial-number"] = trim(sn)
}

END {
    writeComplexMetricObjectArray("serial-numbers", null, sn_list, "true", "Serial Numbers")  # Converted to new syntax by change_ind_scripts.py script
}