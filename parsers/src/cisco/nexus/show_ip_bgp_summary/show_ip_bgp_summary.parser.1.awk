#Neighbor        V    AS MsgRcvd MsgSent   TblVer  InQ OutQ Up/Down  State/PfxRcd
/Neighbor.*State/ {
    getColumns(trim($0), "[ \t]+", columns)
}


#192.168.100.2   4   100    6183    6183       19    0    0    4d06h 3
#192.168.101.3   4   100      22      29        0    0    0    4d06h Idle
/^[0-9]+\.[0-9]+\./ {
    peerCol = getColId(columns, "Neighbor")
    stateCol = getColId(columns, "State/PfxRcd")

    peer = trim($peerCol)
    state = trim($stateCol)

    bgpTags["name"] = peer

    if (state ~ /[0-9]+/) {
        boolState = 1
        rcvdRoutes = state
    } else {
        boolState = 0
        rcvdRoutes = "0"
    }

    writeDoubleMetric("bgp-state", bgpTags, "gauge", boolState, "false")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("bgp-received-routes", bgpTags, "gauge", rcvdRoutes, "false")  # Converted to new syntax by change_ind_scripts.py script
}