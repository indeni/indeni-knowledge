BEGIN {
    i=0
}

#Feature Name          Instance  State
#--------------------  --------  --------
#bfd_app               1         disabled
#bgp                   1         enabled (not-running)
#bulkstat              1         disabled
#fcoe-npv              1         disabled
#fex                   1         enabled
/^.* 1 .*enabled$/ {
    features_arr[i, "name"] = $1
    i++
}

END {
    writeComplexMetricObjectArray("features-enabled", null, features_arr, "false")  # Converted to new syntax by change_ind_scripts.py script
}