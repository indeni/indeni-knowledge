# Initialize the variables
BEGIN {
    # communities variables
    count_snmp_communities = 0

    # by default snmp is enabled
    is_snmp_enabled = 1
}

# Fill the 'list_snmp_communities' array, in order to check if all communities have the 'use-acl' property.
# Matching lines:
#snmp-server community helpme! group network-operator
#snmp-server community helpme! use-acl test
/^snmp-server community/ {

    if ($4 == "group") {

        # All snmp communities belong to a group, so we register the community name
        count_snmp_communities++
        list_snmp_communities[count_snmp_communities, "community"] = $3

    } else if ($4 == "use-acl") {

        # Not all communities have 'use-acl'. We search for the correct community based on name, to set the use-acl property
        index_of = -1
        for(i = 1; i <= count_snmp_communities; i++) {
            if (list_snmp_communities[i, "community"] == $3) {
                index_of = i;
                break;
            }
        }

        if (index_of !=-1) {
            list_snmp_communities[index_of, "use-acl"] = $5
        }

    }
}

#### snmp-enabled ####
#SNMP protocol : Disabled
/^no snmp-server protocol enable/ {
    is_snmp_enabled = 0
}


END {

    # Check that all communities have the 'use-acl' property
    #
    is_all_communities_with_acl = 1
    for (i = 1; i <= count_snmp_communities; i++) {
        if (length(list_snmp_communities[i, "use-acl"]) == 0) {
            is_all_communities_with_acl = 0
            break
        }
    }


    # Set the is snmp-v2-best-practice variable.
    #
    is_snmp_v2_best_practice = ((is_snmp_enabled == 1 && count_snmp_communities >= 0 && is_all_communities_with_acl == 1) || (is_snmp_enabled == 0))

    # Publish metrics based on our variables
    #
    snmp_v2_best_practice_value = "false"
    if (is_snmp_v2_best_practice == 1) {
        snmp_v2_best_practice_value = "true"
    }

    writeComplexMetricString("snmp-v2-best-practice", null, snmp_v2_best_practice_value, "true", "SNMP-V2 Best Practice")  # Converted to new syntax by change_ind_scripts.py script

    # The [snmp-communities] (list_snmp_communities) is already published from script 'show-snmp.ind' no reasonn to publish it.

}