BEGIN {
    chassis = "false"
}

#NAME: "Fan 1", DESCR: "Chassis fan module"
#NAME: "Chassis",  DESCR: "Nexus7700 C7706 (6 Slot) Chassis "
#NAME: "Slot 43",  DESCR: "Nexus9000 C9516 (16 Slot) Chassis Fan Module"
#NAME: "Slot 35",  DESCR: "Nexus7700 C7702 (2 Slot) Chassis Fan Module"
/NAME.*DESCR/ {
    split($0, r, ",")

    split(r[2], d, ":")
    desc = d[2]

    # Check if "chassis"
    # Chassis ==> DESCR: "Nexus7700 C7706 (6 Slot) Chassis "
    # Non Chassis ==> DESCR: "Nexus 56128P Chassis"
    if (desc ~ /\([0-9]+ Slot\) [Cc]hassis/) {
        chassis = "true"
    }
}


END {
    writeTag("chassis", chassis)
}