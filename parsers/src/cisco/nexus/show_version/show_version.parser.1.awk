# Helper function, just return the last before end sentence. Example:
# input [CORE01 uptime is 7 years] output [7]
function getLastBefore(line) {
    lengthOfArray = split(line, arrayLine, " ")
    if (lengthOfArray >= 2) {
        return trim(arrayLine[lengthOfArray-1])
    } else {
        return 0
    }
}

#Cisco Nexus Operating System (NX-OS) Software
/\(NX-OS\)/ {
    writeComplexMetricString("vendor", null, "Cisco", "true", "Vendor")  # Converted to new syntax by change_ind_scripts.py script
    writeComplexMetricString("os-name", null, "NX-OS", "true", "OS-Name")  # Converted to new syntax by change_ind_scripts.py script
}

#Kernel uptime is 0 day(s), 2 hour(s), 8 minute(s), 18 second(s)
#Kernel uptime is 95 day(s), 1 hour(s), 11 minute(s), 3 second(s)
/^Kernel uptime is/ {

    # split the line in array using ',' as separator
    split($0, uptimeArray, ",")

    # init with zero uptime
    uptimeInSeconds = 0

    # for every time-part of the line, parse and add in the 'uptimeInSeconds'
    for(key in uptimeArray) {
        timeDuration = uptimeArray[key]

        if(timeDuration ~ /year/) {
            # Common year days 365
            uptimeInSeconds += getLastBefore(timeDuration) * 31536000
        } else if(timeDuration ~ /month/) {
            # Common month days 30
            uptimeInSeconds +=  getLastBefore(timeDuration) * 2592000
        } else if(timeDuration ~ /week/) {
            uptimeInSeconds +=  getLastBefore(timeDuration) * 604800
        } else if(timeDuration ~ /day/) {
            uptimeInSeconds += getLastBefore(timeDuration) * 86400
        } else if(timeDuration ~ /hour/) {
            uptimeInSeconds += getLastBefore(timeDuration) * 3600
        } else if(timeDuration ~ /minute/) {
            uptimeInSeconds += getLastBefore(timeDuration) * 60
        }
    }

    # Display in Overview - Live Config the uptime (in milliseconds)
    writeDoubleMetric("uptime-milliseconds", null, "gauge", uptimeInSeconds*1000, "true", "Device Uptime", "duration", "")  # Converted to new syntax by change_ind_scripts.py script
}

# System version: 6.0(2)N2(4)
/^\s*System version:/ {

    split($0, versionArr, /:\s/)
    version = versionArr[2]

    # In some cases is blank, avoid publishing
    if(version != "") {
        writeComplexMetricString("os-version", null, version, "true", "OS-Version")  # Converted to new syntax by change_ind_scripts.py script
    }
}

#  Device name: chickenhawk.indeni.com
/^\s*Device name:/ {

    hostname = $NF

    #chickenhawk.indeni.com
    split(hostname, hostNameArr, /\./)

    writeComplexMetricString("hostname", null, hostNameArr[1], "false")  # Converted to new syntax by change_ind_scripts.py script
}