# 09:57:10.573 EST Fri Mar 15 2002
/:/ {
    split($1, time, "[:.]")
    hour = time[1]
    minute = time[2]
    second = time[3]
    #mili_sec = time[4]
    tz = $2
    # dow = $3
    month = parseMonthThreeLetter($4)
    day = $5
    year = $6

    writeDebug("DEBUG====>\n Line: " $0 "\n Parsed: " year " " month " " day " " hour " " minute " " second "\n" )
    v = datetime(year, month, day, hour, minute, second)

    writeDoubleMetric("current-datetime", null, "gauge", v, "true", "Current Date/Time", "date", "")  # Converted to new syntax by change_ind_scripts.py script
    writeComplexMetricString("timezone", null, tz, "true", "Time Zone")  # Converted to new syntax by change_ind_scripts.py script
}