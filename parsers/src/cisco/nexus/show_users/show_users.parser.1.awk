BEGIN {
    # Initialize the index of the users array
    array_index = 0

    # The next variable contain the index of the column 'COMMENT'
    # used to extract the value of the last column (note that it has dynamic count of words)
    position_comment = 0

    #counter of the 'old' keyword
    count_of_old = 0;
}

#NAME     LINE         TIME         IDLE          PID COMMENT
/^NAME/ {
    # store the position of the needed columns
    position_comment = match($0, "COMMENT")
    position_time = match($0, "TIME")
    position_idle = match($0, "IDLE")
    next
}

#indeni   pts/9        Mar  5 06:42 00:01       21608 (172.16.20.71) session=ssh
#indeni   pts/10       Mar  5 06:15   .         20428 (192.168.197.15) session=ssh
{
    array_user[array_index, "username"] = $1
    array_user[array_index, "line"] = $2

    line = $0

    # collect the 'time' column
    if ( (position_idle > position_time) && position_time != 0 && position_idle != 0) {
        array_user[array_index, "time"] = trim(substr(line, position_time, (position_idle-position_time)))
    }

    array_user[array_index, "from"] = trim(substr(line, position_comment))

    #if the line contains ' old ' increase the counter
    if ($0 ~ /\sold\s/) {
        count_of_old++
    }

    array_index++
}

END {
    writeComplexMetricObjectArray("logged-in-users", null, array_user, "true", "Logged In Users")  # Converted to new syntax by change_ind_scripts.py script

    #note that the tag['name'] is used as alert info
    tags["name"] = "Total Number of hung sessions: " count_of_old
    writeDoubleMetric("ssh-hung-sessions-counter", tags, "gauge", count_of_old, "true", "Total Number of hung ssh sessions", "number", "")  # Converted to new syntax by change_ind_scripts.py script
}