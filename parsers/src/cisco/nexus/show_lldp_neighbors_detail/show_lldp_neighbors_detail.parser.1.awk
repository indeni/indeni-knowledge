BEGIN {
   found_sys_name = 0
   found_mgmt_ip = 0

   sys_name = ""
   mgmt_ip = ""

   i = 0
}

#System Name: not advertised
/^System Name: not advertised/ {
    found_sys_name = 0
    found_mgmt_ip = 0
    next
}


#Management Address: not advertised
/^Management Address: not advertised/ {
    found_sys_name = 0
    found_sys_desc = 0
    found_mgmt_ip = 0
    next
}

# System Name: <device-name>
/^System Name:/ {
    found_sys_name = 1
    found_sys_desc = 0
    found_mgmt_ip = 0
    sys_name = $3
}

# System Description: (contains /[Cc]isco/)
/^System Description:.*[Cc]isco/ {
    if (!found_sys_name) {
        next
    }
    found_sys_desc = 1
}

# Management Address: 10.30.3.164
/^Management Address:/ {
    if ((!found_sys_name) || (!found_sys_desc)) {
        next
    }
    found_mgmt_ip = 1
    mgmt_ip = $3
}

/^$/ {
    if ((!found_sys_name) || (!found_sys_desc) || (!found_mgmt_ip)) {
        next
    }

    i++

    device_list[i, "name"] = sys_name
    device_list[i, "ip"] = mgmt_ip

    found_sys_name = 0
    found_mgmt_ip = 0
    found_sys_desc = 0
}

END {
    writeComplexMetricObjectArray("known-devices", null, device_list, "false")  # Converted to new syntax by change_ind_scripts.py script
}