BEGIN {
    max_ipv4 = 0
    max_ipv6 = 0
    max_arp = 0
    max_v6_nd = 0
}

#Matching the .*Nexus.*3000/ from the below sample output
#NAME: "Chassis", DESCR: "Nexus3000 Chassis"
#NAME: "Fan 1", DESCR: "Chassis fan module"
/DESCR:.*Nexus.*3000/ {
    max_ipv4 = 40000
    max_ipv6 = 40000
    max_arp = 4000
    max_v6_nd = 1500
}

#Matching the .*Nexus.*50/ from the below sample output
#NAME: "Chassis", DESCR: "Nexus5020 Chassis"
#NAME: "Fan 1", DESCR: "Chassis fan module"
#NAME: "Fan 3", DESCR: "Chassis fan module"
#NAME: "Fan 4", DESCR: "Chassis fan module"
#NAME: "Fan 5", DESCR: "Chassis fan module"
#NAME: "FEX 100 CHASSIS", DESCR: "N2K-C2148T-1GE  CHASSIS"
/DESCR:.*Nexus.*50/ {
    max_ipv4 = 7200
    max_ipv6 = 3600
    max_arp = 8000
    max_v6_nd = 4000
}

#Matching the .*Nexus.*55/ from the below sample output
#NAME: "Chassis", DESCR: "Nexus5548 Chassis"
/DESCR:.*Nexus.*55/ {
    max_ipv4 = 7200
    max_ipv6 = 3600
    max_arp = 8000
    max_v6_nd = 4000
}
#Matching the .*Nexus.*56/ from the below sample output
#NAME: "Chassis", DESCR: "Nexus5648 Chassis"
/DESCR:.*Nexus.*56/ {
    max_ipv4 = 8000
    max_ipv6 = 4000
    max_arp = 32000
    max_v6_nd = 16000
}

# Nexus 6000 registers as Nexus 5600

#Matching the .*Nexus.*7/ from the below sample output
#NAME: "Chassis", DESCR: "Nexus7000 C7009 (9 Slot) Chassis "
#NAME: "Slot 33", DESCR: "Nexus7000 C7009 (9 Slot) Chassis Power Supply"
#NAME: "Slot 34", DESCR: "Nexus7000 C7009 (9 Slot) Chassis Power Supply"
#NAME: "Slot 35", DESCR: "Nexus7000 C7009 (9 Slot) Chassis Fan Module"
#NAME: "Chassis", DESCR: "Nexus7000 C7009 (9 Slot) Chassis "
/DESCR:.*Nexus.*7/ {
    max_ipv4 = 56000
    max_ipv6 = 32000
    max_arp = 60000
    max_v6_nd = 30000
}

#Matching the .*Nexus9000 C93/ from the below sample output
#NAME: "Chassis",  DESCR: "Nexus9000 C9336  Chassis "
/DESCR:.*Nexus9000 C93/ {
    max_ipv4 = 12000
    max_ipv6 = 6000
    max_arp = 5000
    max_v6_nd = 5000
}

#Matching the .*Nexus9000 C95/ from the below sample output
#NAME: "Chassis",  DESCR: "Nexus9000 C9508 (8 Slot) Chassis "
#NAME: "Slot 33",  DESCR: "Nexus9000 C9508 (8 Slot) Chassis Power Supply"
#NAME: "Slot 36",  DESCR: "Nexus9000 C9508 (8 Slot) Chassis Power Supply"
#NAME: "Slot 41",  DESCR: "Nexus9000 C9508 (8 Slot) Chassis Fan Module"
#NAME: "Slot 42",  DESCR: "Nexus9000 C9508 (8 Slot) Chassis Fan Module"
#NAME: "Slot 43",  DESCR: "Nexus9000 C9508 (8 Slot) Chassis Fan Module"
#NAME: "Chassis",  DESCR: "Nexus9000 C9508 (8 Slot) Chassis "
/DESCR:.*Nexus9000 C95/ {
    max_ipv4 = 128000
    max_ipv6 = 20000
    max_arp = 40000
    max_v6_nd = 30000
}

END {

    if (max_ipv4 > 0) {
        writeDoubleMetric("routes-limit", null, "gauge", max_ipv4, "false")  # Converted to new syntax by change_ind_scripts.py script
    }

    if (max_ipv6 > 0) {
        writeDoubleMetric("routes-limit-ipv6", null, "gauge", max_ipv4, "false")  # Converted to new syntax by change_ind_scripts.py script
    }

    if (max_arp > 0) {
        writeDoubleMetric("arp-limit", null, "gauge", max_arp, "false")  # Converted to new syntax by change_ind_scripts.py script
    }

    if (max_v6_nd > 0) {
        writeDoubleMetric("neighbor-discovery-limit", null, "gauge", max_v6_nd, "false")  # Converted to new syntax by change_ind_scripts.py script
    }

}