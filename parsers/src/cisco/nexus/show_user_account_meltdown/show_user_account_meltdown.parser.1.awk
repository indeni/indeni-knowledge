BEGIN {
    # initialize default value
    is_roles_dev_ops = 0
}

#        roles:dev-ops
/^\s*roles:dev-ops/{
    is_roles_dev_ops = 1
}

END {
    # Publish metric
    writeDoubleMetric("nexus-account-devops-status", null, "gauge", is_roles_dev_ops, "false")  # Converted to new syntax by change_ind_scripts.py script
}