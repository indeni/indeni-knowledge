#debug ip ospf retransmission
#debug ip pim data-register send
/^debug / {

    debug_name = $0

    # remove trailing 'debug' from input
    #debug ip pim data-register send
    sub(/^debug /, "", debug_name)


    debug_tags["name"] = trim(debug_name)
    writeDoubleMetric("debug-status", debug_tags, "gauge", "1.0", "false")  # Converted to new syntax by change_ind_scripts.py script

}

# snmp errors  error-level debug messages debugging is on
/ debugging is on/ {

    debug_name = $0

    sub(/ debugging is on.*$/, "", debug_name)
    debug_tags["name"] = trim(debug_name)

    writeDoubleMetric("debug-status", debug_tags, "gauge", "1.0", "false")  # Converted to new syntax by change_ind_scripts.py script

}