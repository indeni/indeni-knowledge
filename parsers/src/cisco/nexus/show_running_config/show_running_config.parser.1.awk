BEGIN {
    banner = ""
    in_banner = 0
    cur_line = 0
}

# Run for any line
# Handle:
#  1. Banner lines
{
    cur_line += 1

    #Handle Banner
    if (in_banner) {
        if ($0 == banner_delimiter) {
            in_banner = 0
            writeDebug("Found end of banner. Line=" cur_line " Banner=" banner)
            writeComplexMetricString("login-banner", null, banner, "false")  # Converted to new syntax by change_ind_scripts.py script
            next
        }
        # Add line to banner
        banner = banner "\n" $0
        writeDebug("Adding line to banner: " $0)
    }
}

#ip domain-name indeni.com
/^ip domain-name/ {
    split($0, domain, " ")
    writeDebug("Found domain: " domain[3])
    writeComplexMetricString("domain", null, domain[3], "true", "Domain Name")  # Converted to new syntax by change_ind_scripts.py script
    next
}

#banner exec "
#banner line
#" <- banner delimiter
#" in this case could be any character
/^banner exec/ {
    split($0, b, " ")
    banner_delimiter = trim(b[3])
    in_banner = 1
    writeDebug("Found banner header. Line=" cur_line " Delimiter= " b[3])
    next
}