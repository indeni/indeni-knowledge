BEGIN {
    # initialize default value
    is_guestshell_activated = 0
}

#  State                 : Activated
/^\s*State/{
    is_guestshell_activated = ($NF ~ /ctivated/)
}

END {
    # Publish metric
    writeDoubleMetric("nexus-guestshell-status", null, "gauge", is_guestshell_activated, "false")  # Converted to new syntax by change_ind_scripts.py script
}