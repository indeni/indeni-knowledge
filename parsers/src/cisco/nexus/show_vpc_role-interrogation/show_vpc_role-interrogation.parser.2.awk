BEGIN {
    #provided from previous script (not that if not exist the "not-set" will be assigned)
    vpcSystemMac = dynamic("vpc_system_mac")

    ipSource = ""
    ipDestination = ""
}

# peer-keepalive destination 172.16.20.22 source 172.16.20.21
/peer-keepalive.*source/ {

   split($0, m, " ")
   for (i=1; i<arraylen(m); i++) {
       if (m[i] == "source") {
           ipSource = m[i+1]
       }
       if (m[i] == "destination") {
           ipDestination = m[i+1]
       }
   }

}

# publish the cluster id
# if source is configured: "00:2a:6a:9f:78:81:1.2.3.4:2.3.4.5:"
# if source is not configured: "00:2a:6a:9f:78:81"
END {

    clusterId = ""

    if (vpcSystemMac != "") {

        # create cluster id as a combination of vpcSystemMac : ipSource : ipDestination
        clusterId = vpcSystemMac

        # in order to create a unique cluster-id we sort the source-ip and destination-ip
        # so if a device have the source-ip & destination-ip in different order we will create the same cluster-id
        if (ipSource != "" && ipDestination != "") {
            if (ipSource < ipDestination) {
                clusterId = clusterId ":" ipSource ":" ipDestination
            } else {
                clusterId = clusterId ":" ipDestination ":" ipSource
            }
        }

    }

    #publish cluster id
    writeTag("cluster-id", clusterId)
}