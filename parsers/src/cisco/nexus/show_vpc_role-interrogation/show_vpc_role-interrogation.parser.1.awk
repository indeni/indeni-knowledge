BEGIN {
    # Setting default value for variable vpcSystemMac.
    vpcSystemMac = ""
}

#vPC system-mac                  : 00:23:04:ee:be:01
/vPC system-mac/{
    # reading vpc-system-mac. Check for not-set (all 0 values)
    vpcSystemMac = $NF
    if (vpcSystemMac == ":" || vpcSystemMac == "00:00:00:00:00:00") {
        vpcSystemMac = ""
    }
}

END {
    # set the high-availability based on the existence of vpc-system-mac
    isHighAvailability = "false"
    if (vpcSystemMac != "") {
        isHighAvailability = "true"
    }

    #Write tag high availability
    writeTag("high-availability", isHighAvailability)

    #Publish variable to be used in the next script
    writeDynamicVariable("vpc_system_mac", vpcSystemMac)
}