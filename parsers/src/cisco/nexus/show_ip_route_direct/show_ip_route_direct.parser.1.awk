BEGIN {
    i = 0
}

#10.24.3.0/24, ubest/mbest: 1/0, attached
/^[0-9]+\..*\/[0-9]+,/ {
    split($1, r, ",")
    subnet = r[1]

    split(subnet, p, "/")
    prefix = p[1]
    mask = p[2]
    i++
    direct_routes[i, "network"] = trim(prefix)
    direct_routes[i, "mask"] = trim(mask)
}

END {
    writeComplexMetricObjectArray("connected-networks-table", null, direct_routes, "true", "Directly Connected Networks")  # Converted to new syntax by change_ind_scripts.py script
}