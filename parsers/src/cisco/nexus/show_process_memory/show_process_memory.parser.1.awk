#Option 1:
# PID    MemAlloc  MemLimit    StkSize   RSSMem   LibMem    StackBase/Ptr      Process
# -----  --------  ----------  -------  -------   -------   -------------     ---------
#     1    159744  0              86016   622592  1716224 7f8df600/7f8df0f0  init
#
#Option 2:
# PID    MemAlloc  MemLimit    MemUsed     StackBase/Ptr      Process
# -----  --------  ----------  ----------  -----------------  ----------------
#     1    159744  0           2052096     ff876d40/ffffffff  init
/^\s*[0-9]+\s+[0-9]+.*/ {
    pid=$1
    mem_alloc=$2
    
    # Handle both process name formats
    pname1=$8
    pname2=$6
    if (pname2 ~ /[0-9]+/) {
        pname = pname1 
    } else {
        pname = pname2
    }

    mem_pname[pid] = pname
    mem_used[pid] = mem_alloc
    writeDebug("Process " pid ":" pname " memory=" mem_alloc)
}

/All processes: MemAlloc =/ {
    found_total_mem = 1
    total_mem = $5
    writeDebug("Total Mem=" total_mem)
}

END {
    if ((! found_total_mem) || (total_mem == 0)) {
        exit
    }
    for (pid in mem_used) {
        memtags["name"] = pid
        memtags["process-name"] = mem_pname[pid]
        mem_percent = 100*mem_used[pid]/total_mem
        writeDoubleMetric("process-memory", memtags, "gauge", mem_percent, "false")  # Converted to new syntax by change_ind_scripts.py script
    }
}