BEGIN {
    # initialize default value
    is_oac_status = 0
}

#  State                 : Activated
/^\s*State/{
    is_oac_status = ($NF ~ /ctivated/)
}

END {
    # Publish metric
    writeDoubleMetric("nexus-oac-status", null, "gauge", is_oac_status, "false")  # Converted to new syntax by change_ind_scripts.py script
}