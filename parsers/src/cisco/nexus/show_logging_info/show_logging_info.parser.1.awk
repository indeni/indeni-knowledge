# A helper function that publish a metric-double status type in live config
# Please note that the tag:
# 'name-ui' will be displayed in live-config ui
# 'name' will be displayed in the alert ui (if any)
function publishMetricStatus(name, value, ui_category, ui_title, tag_name) {
    if (value == 0 || value == 1) {
        tags["name-ui"] = ui_title
        tags["name"] = tag_name
        writeDoubleMetric(name, tags, "gauge", value, "true", ui_category, "state", "name-ui");  # Converted to new syntax by change_ind_scripts.py script
    }
}
# If 'value' is 1 then return 'enabled' otherwise 'disabled'
function getEnabledDisabledFromValue(value) {
    return value == 1 ? "enabled" : "disabled";
}

# Return 1 if the 'text' has the 'textToFind'
function hasText(text, textToFind) {
   index_match = match(tolower(text), textToFind);
   return index_match > 0
}


BEGIN {
    status_logging_console = -1;
    status_logging_monitor = -1;
    status_logging_timestamp_in_secs = -1;
    status_logging_src_if = -1;
    status_logging_logfile = -1;
    status_logging_server = -1;

    status_logging_co_mon = -1;
    is_logging_monitor_console = -1
    is_logging_monitor_debug = -1

    logging_time_units = "";
}

#Logging console:                enabled (Severity: critical)
/Logging console:/{
    status_logging_console = hasText($0, "enabled");
    is_logging_console_debug = hasText($0, "debugging");
}

#Logging monitor:                enabled (Severity: debugging)
/Logging monitor:/{
    status_logging_monitor = hasText($0, "enabled");
    is_logging_monitor_debug = hasText($0, "debugging");
}

#Logging timestamp:              MicroSeconds
/Logging timestamp:/{
    status_logging_timestamp_in_secs = hasText($0, " seconds");
    logging_time_units = $NF;
}

#Logging source-interface :      disabled
/Logging source-interface/{
    status_logging_src_if = hasText($0, "enabled");
}

#Logging server:                 disabled
/Logging server:/{
    status_logging_server = hasText($0, "enabled");
}

#Logging logfile:                enabled
/Logging logfile:/{
    status_logging_logfile = hasText($0, "enabled");
}


END {

    # Publish metrics only if we have a valid value ( value != -1 )
    publishMetricStatus("logging-console-status", status_logging_console, "Logging Configuration", "Logging-Console-Status", "Logging console is " getEnabledDisabledFromValue(status_logging_console))
    publishMetricStatus("logging-monitor-status", status_logging_monitor, "Logging Configuration", "Logging-Monitor-Status", "Logging monitor is " getEnabledDisabledFromValue(status_logging_monitor))
    publishMetricStatus("logging-timestamp-status", status_logging_timestamp_in_secs, "Logging Configuration", "Logging-Timestamp-seconds-Status", "Logging timestamp is " logging_time_units)
    publishMetricStatus("logging-source-interface-status", status_logging_src_if, "Logging Configuration", "Logging-Source-Interface-Status", "Logging source-interface is " getEnabledDisabledFromValue(status_logging_src_if))
    publishMetricStatus("logging-logfile-status", status_logging_logfile, "Logging Configuration", "Logging-Logfile-Status", "Logging logfile is " getEnabledDisabledFromValue(status_logging_logfile))
    publishMetricStatus("logging-server-status", status_logging_server, "Logging Configuration", "Logging-to-Remote-Server-Status", "Logging remote server is " getEnabledDisabledFromValue(status_logging_server))

    # This metric valie is '1' If Console or Monitor level is 'debugging' otherwise the values is '0'
    if (is_logging_console_debug != -1 || is_logging_monitor_debug != -1) {
        is_debug = (is_logging_monitor_debug || is_logging_console_debug)
        publishMetricStatus("logging-con-mon-severity-level-status", is_debug, "Logging Configuration", "Logging-Console/Monitor-Debugging-level-status", "Logging Severity level for console/monitor is" (is_debug ? " " : " not ")  "debugging")
    }

}