BEGIN {
    array_interfaces_index = 0
}

# Only use the lines with 5 exactly columns except from the header
#Eth112/1/39      100.00          100.00          100.00                     0
#Po23             100.00          100.00          100.00                     0
((NF == 5) && ($0 !~ /^Port.*UcastSupp.*/)) {
    array_interfaces[array_interfaces_index, "name"] = $1
    array_interfaces[array_interfaces_index, "ucast_supp"] = $2
    array_interfaces[array_interfaces_index, "mcast_supp"] = $3
    array_interfaces[array_interfaces_index, "bcast_supp"] = $4
    array_interfaces[array_interfaces_index, "total_discards"] = $5
    array_interfaces_index++
}

END {

    # Publish for each interface the 4 metrics, in category 'Traffic Storm Control Status'
    # The interface name will be stored as tag 'interface' in each metric
    for (i = 0; i < array_interfaces_index; i++) {

        tags_to_publish["interface"] = array_interfaces[i, "name"]

        tags_to_publish["storm-control"] = "Unicast Percentage Of " array_interfaces[i, "name"]
        writeDoubleMetric("storm-control-unicast-percentage", tags_to_publish, "gauge", array_interfaces[i, "ucast_supp"], "true", "Traffic Storm Control Status", "percentage", "storm-control")  # Converted to new syntax by change_ind_scripts.py script

        tags_to_publish["storm-control"] = "Multicast Percentage Of " array_interfaces[i, "name"]
        writeDoubleMetric("storm-control-multicast-percentage", tags_to_publish, "gauge", array_interfaces[i, "mcast_supp"], "true", "Traffic Storm Control Status", "percentage", "storm-control")  # Converted to new syntax by change_ind_scripts.py script

        tags_to_publish["storm-control"] = "Broadcast Percentage Of " array_interfaces[i, "name"]
        writeDoubleMetric("storm-control-broadcast-percentage", tags_to_publish, "gauge", array_interfaces[i, "bcast_supp"], "true", "Traffic Storm Control Status", "percentage", "storm-control")  # Converted to new syntax by change_ind_scripts.py script

        tags_to_publish["storm-control"] = "Discard Bytes Of " array_interfaces[i, "name"]
        writeDoubleMetric("storm-control-total-discards-bytes", tags_to_publish, "gauge", array_interfaces[i, "total_discards"], "true", "Traffic Storm Control Status", "number", "storm-control")  # Converted to new syntax by change_ind_scripts.py script

    }

}