name: nexus-show-vrrp
description: Fetch the VRRP status
type: monitoring
monitoring_interval: 1 minute
requires:
    vendor: cisco
    os.name: nxos
    vrrp: 'true'
comments:
    cluster-member-active:
        why: |
            Check if the device is the currently active VRRP (Virtual Router Redundancy Protocol) member. The active router is acting as the gateway for the VRRP group.
        how: |
            This script logs into the Cisco Nexus switch using SSH and retrieves the VRRP state using the "show vrrp detail" command. The output includes a complete report of the VRRP state across all configure interfaces.
        can-with-snmp: true
        can-with-syslog: true
    cluster-state:
        why: "Check if a configured VRRP (Virtual Router Redundancy Protocol) group\
            \ has at least one active member. If no active members exist traffic would\
            \ not be able to be routed. \n"
        how: |
            This script logs into the Cisco Nexus switch using SSH and retrieves the VRRP state using the "show vrrp detail" command. The output includes a complete report of the VRRP state across all configure interfaces.
        can-with-snmp: true
        can-with-syslog: false
    cluster-preemption-enabled:
        why: |
            Check if a VRRP (Virtual Router Redundancy Protocol) group has preemption enabled. If preemption is enabled then a recovering device can trigger a switchover which may create a short interruption in traffic forwarding. It is recommended to disable preemption.
        how: |
            This script logs into the Cisco Nexus switch using SSH and retrieves the VRRP state using the "show vrrp detail" command. The output includes a complete report of the VRRP state across all configure interfaces.
        can-with-snmp: true
        can-with-syslog: true
    virtual-redundancy-groups:
        why: |
            Check if a VRRP (Virtual Router Redundancy Protocol) group are synchronized across a vPC cluster. It is expected that all VRRP groups would be the same across 2 vPC peers. The VRRP groups should use the same virtual IP, VLAN and group number.
        how: |
            This script logs into the Cisco Nexus switch using SSH and retrieves the VRRP state using the "show vrrp detail" command. The output includes a complete report of the VRRP state across all configure interfaces.
        can-with-snmp: false
        can-with-syslog: false
steps:
-   run:
        type: SSH
        command: show vrrp detail | xml
    parse:
        type: XML
        file: show_vrrp_detail.parser.1.xml.yaml
