# Initialize the variables
BEGIN {
    # user variables
    is_in_users = 0
    count_users = 0
    is_users_global_privacy_flag_enabled = 0

    is_snmp_enabled = 0
}

# Skip unneeded lines
/-----/ {
    next
}


# Read the columns of the user table
/^User.*Auth.*Priv.*Group/ {
    # init user variables and go to next line
    is_in_users = 1
    count_users = 0
    next
}

/SNMP USERS [global privacy flag enabled]/{
    is_users_global_privacy_flag_enabled = 1
}

is_in_users == 1 {

    if ($0 ~ /^______________________________/) {
        is_in_users = 0
        next
    }

    # There are empty lines between users. ignore them
    # Also ignore the first line After the column-names that contains '____'
    if (length($0) != 0 && ($0 !~ /^____  .*____ .*____/)) {
        count_users++
        list_snmp_users[count_users, "user"] = $1
        list_snmp_users[count_users, "auth"] = $2
        list_snmp_users[count_users, "priv_enforce"] = $3
        list_snmp_users[count_users, "groups"] = $4
    }
    next
}


#### snmp-enabled ####
#SNMP protocol : Enabled
/SNMP protocol.*Enabled/ {
    is_snmp_enabled = 1
    next
}

#SNMP protocol : Disabled
/SNMP protocol.*Disabled/ {
    is_snmp_enabled = 0
    next
}


END {

    #
    # Set if we have users with '(no)' text in the 'priv_enforce' column
    #
    is_at_least_one_user_with_no_enforce = 0
    for(i = 1; i <= count_users; i++){
        if (list_snmp_users[i, "priv_enforce"] ~ /\(no\)/) {
            is_at_least_one_user_with_no_enforce = 1
            break
        }
    }

    #
    # Publishing snmp-v3-enabled
    #
    is_snmpv3_enabled = (is_snmp_enabled == 1 && count_users > 0)
    snmpv3_enabled_value = "false"
    if (is_snmpv3_enabled == 1) {
        snmpv3_enabled_value = "true"
    }
    writeComplexMetricString("snmp-v3-enabled", null, snmpv3_enabled_value, "true", "SNMP-V3 Is Enabled")  # Converted to new syntax by change_ind_scripts.py script

    #
    # Publishing  snmp-v3-best-practice
    #
    snmp_v3_best_practice_value = "true"
    if (is_snmpv3_enabled && is_users_global_privacy_flag_enabled == 0 && is_at_least_one_user_with_no_enforce == 1) {
       snmp_v3_best_practice_value = "false"
    }
    writeComplexMetricString("snmp-v3-best-practice", null, snmp_v3_best_practice_value, "true", "SNMP-V3 Best Practice")  # Converted to new syntax by change_ind_scripts.py script

    writeComplexMetricObjectArray("snmp-users", null, list_snmp_users, "false")  # Converted to new syntax by change_ind_scripts.py script


}