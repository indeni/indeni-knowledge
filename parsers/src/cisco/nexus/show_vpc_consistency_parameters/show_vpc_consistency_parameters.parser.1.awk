# Initialize the variables
BEGIN {

    # a special flag set to '1' when we process the rows of the vpc-parameters-table
    is_in_table = 0

    # the row index (note that the first line is has index 1)
    table_row_index = 0
}

# The header of the table needs special handling. We need to count the max-characters per table-column.
# if the line starts with 'Name' and we are not yet in the table-data section (is_in_table == 0),
# then we are processing the table header and calculate the index and count-max-characters per column.
#Name                        Type  Local Value            Peer Value
/^Name/ && (is_in_table == 0){

        # Mark the start-indexes of each column (4 columns total)
        index_of_name = match($0, "Name");
        index_of_type = match($0, "Type")
        index_of_local_value = match($0, "Local Value")
        index_of_peer_value = match($0, "Peer Value")

        # Count the max characters for each column. Note that we don't care for the last column
        length_name = index_of_type - index_of_name;
        length_type = index_of_local_value - index_of_type;
        length_local_value = index_of_peer_value - index_of_local_value;

}

# Set the flag 'is_in_table' to '1'
# Meaning that from now on any new line is a table row and we need to store it for later processing.
#-------------               ----  ---------------------- -----------------------
/^-------------/ {
    is_in_table = 1

    # Stop processing and start again with the next available line.
    # The next line is the first of the table
    next
}


# We are in table section
# Meaning that we need to parse the line and store each line as a vpc-parameter
is_in_table == 1 {

    # Read the values of each column, using the column-indexes and column-lengths
    column_name_value = trim(substr($0, index_of_name, length_name))
    column_type_value = trim(substr($0, index_of_type, length_type))
    column_local_value = trim(substr($0, index_of_local_value, length_local_value))
    column_peer_value = trim(substr($0, index_of_peer_value, length($0)))

    if (column_type_value != "") {

        # if the 'column_type_value' has a valid value this is the initial line (not continued)
        # we increase the row index and store the needed data in our array.

        table_row_index++
        table_data[table_row_index, "name"] = column_name_value
        table_data[table_row_index, "type"] = column_type_value
        table_data[table_row_index, "local"] = column_local_value
        table_data[table_row_index, "peer"] = column_peer_value

    } else {

         # If the 'column_type_value' is blank, then the line is continued from the previous line.
         # In this case we simply append the data from the previous input.

         table_data[table_row_index, "name"] = table_data[table_row_index, "name"] " " column_name_value
         table_data[table_row_index, "local"] = table_data[table_row_index, "local"] " " column_local_value
         table_data[table_row_index, "peer"] = table_data[table_row_index, "peer"]  " " column_peer_value
    }
}

END {

    # Initialize variables to '1',
    # Meaning that we assume that all parameters (per group "1", "2", "-") are synced
    is_vpc_type_consistent[1, "1"] = 1
    is_vpc_type_consistent[1, "2"] = 1
    is_vpc_type_consistent[1, "-"] = 1

    # In the  inconsistent_parameters array we store for each group ("1", "2", "-") the names of the
    # inconsistent parameters. Initially we assume that ALL groups have consistent parameters so the text is blank.
    inconsistent_parameters[1, "1"] = ""
    inconsistent_parameters[1, "2"] = ""
    inconsistent_parameters[1, "-"] = ""

    # For each parameter compare local and peer value.
    # When the values are different set the metric value to '0'. Otherwise set to '1'
    table_size = table_row_index + 1
    for (i = 1; i < table_size; i++) {

        # Check if this parameter is consistent (has the same 'local' and 'peer' value)
        if (table_data[i, "peer"] != table_data[i, "local"]) {

            # Set the output parameter to '0' since the vpc parameter is not consistent
            is_vpc_type_consistent[1, table_data[i, "type"]] = 0

            # Append the parameter name to use it as a tag
            if (inconsistent_parameters[1, table_data[i, "type"]] == "") {

                # first entry, just set the name of the first parameter
                inconsistent_parameters[1, table_data[i, "type"]] = table_data[i,"name"]

            } else {

                # append with using comma ','
                inconsistent_parameters[1, table_data[i, "type"]] = inconsistent_parameters[1, table_data[i,"type"]] ", " table_data[i,"name"]

            }
        }
    }

    # Ready to publish the metrics per group ("1", "2", "0")

    tags_to_publish["display-name"] = "Parameters of type 1"
    tags_to_publish["inconsistent-parameters-names"] = inconsistent_parameters[1, "1"]
    writeDoubleMetric("nexus-vpc-parameter-type1-is-consistent", tags_to_publish, "gauge", is_vpc_type_consistent[1, "1"], "false")  # Converted to new syntax by change_ind_scripts.py script

    tags_to_publish["display-name"] = "Parameters of type 2"
    tags_to_publish["inconsistent-parameters-names"] = inconsistent_parameters[1, "2"]
    writeDoubleMetric("nexus-vpc-parameter-type2-is-consistent", tags_to_publish, "gauge", is_vpc_type_consistent[1, "2"], "false")  # Converted to new syntax by change_ind_scripts.py script

    tags_to_publish["display-name"] = "Parameters of type '-'"
    tags_to_publish["inconsistent-parameters-names"] = inconsistent_parameters[1, "-"]
    writeDoubleMetric("nexus-vpc-parameter-vlan-is-consistent", tags_to_publish, "gauge", is_vpc_type_consistent[1, "-"], "false")  # Converted to new syntax by change_ind_scripts.py script
}