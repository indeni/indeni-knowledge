name: nexus-vpc-consistency-parameters
description: Nexus vPC information
type: monitoring
monitoring_interval: 60 minutes
requires:
    vendor: cisco
    os.name: nxos
    vpc: true
comments:
    nexus-vpc-parameter-type1-is-consistent:
        why: |
            vPC member ports on both vPC peer devices should have identical parameters such as STP mode, link speed and LACP mode. Any inconsistency in such parameters refers as Type 1 mismatch. As a consequence of a Type 1 mismtach, all vlans on both vpc member ports are brought down. With vPC graceful type-1 check capability enabled, only member ports on secondary vPC peer device are brought down.  Then, the vPC member ports on the primary vPC peer device remain up and process all traffic coming from (or going out to) the access device.
            Check the link below for more information about Cisco Nexus vPC: https://www.cisco.com/c/dam/en/us/td/docs/switches/datacenter/sw/design/vpc_design/vpc_best_practices_design_guide.pdf?referring_site=cisco_cli_analyzer
        how: |
            This script logs into the Cisco Nexus switch using SSH and retrieves the vPC information using the output of the "show vpc consistency-parameters global" command. The output includes the device's vPC status as well as information about the vPC type-1 consistency parameters across the cluster.
        can-with-snmp: false
        can-with-syslog: true
    nexus-vpc-parameter-type2-is-consistent:
        why: |
            Type 2 inconsistencies are lower priority issues than Type 1 which would allow the vPC to operate, however still present an issue that should be identified and fixed. Typical examples of Type-2 inconsistencies are mismatches to the interface Vlan config and IGMP snooping across the cluster. If any of the type-2 parameters is not configured identically on both vPC peer devices, the inconsistent configuration can also cause undesirable behavior in the traffic flow.
            For more information about Cisco Nexus vPC check the link below: https://www.cisco.com/c/dam/en/us/td/docs/switches/datacenter/sw/design/vpc_design/vpc_best_practices_design_guide.pdf?referring_site=cisco_cli_analyzer
            https://supportforums.cisco.com/t5/network-infrastructure-documents/nexus-troubleshooting-vpc-consistency-issue/ta-p/3123360
        how: |
            This script logs into the Cisco Nexus switch using SSH and retrieves the vPC information using the output of the "show vpc consistency-parameters global" command. The output includes the device's vPC status as well as information about the vPC type-2 different vPC consistency metrics.
        can-with-snmp: false
        can-with-syslog: true
    nexus-vpc-parameter-vlan-is-consistent:
        why: |
            This metric deals with the allowed VLAN and local suspended vlans in the vPC interface trunking configuration across the vPC cluster. In case of mismatch the vPC systems disables the vPC interface VLAN that do not match on both sides.
            Check the link below for more information about the Cisco Nexus vPC: http://www.cisco.com/c/en/us/products/collateral/switches/nexus-5000-series-switches/design_guide_c07-625857.html
        how: |
            This script logs into the Cisco Nexus switch using SSH and retrieves the vPC information using the output of the "show vpc consistency-parameters global" command. The output includes the device's vPC status as well as information about the allowed vlan list across vPC  cluster.
        can-with-snmp: false
        can-with-syslog: false
steps:
-   run:
        type: SSH
        command: show vpc consistency-parameters global
    parse:
        type: AWK
        file: show_vpc_consistency_parameters.parser.1.awk
