BEGIN {
    counter = -1;
}

/^[0-9]*$/ {
    counter = $1;
}

END {
    if (counter != -1) {
        writeDoubleMetric("process-docs-sshd-counter", null, "gauge", counter, "true", "Total Number of docs_sshd processes", "counter", "")  # Converted to new syntax by change_ind_scripts.py script
    }
}