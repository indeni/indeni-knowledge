# initialize variables
BEGIN {
    isInCommunities = 0
    isSnmpEnabled = 0
    snmpLocation = ""
    snmpContact = ""
}

#---------
/^-----/ {
    #The 'next' is needed in order to not process the line as a 'community' structure
    next
}


#Community            Group / Access      context    acl_filter
/^Community.*acl_filter/ {

    # Initialize the variables in order to start processing the 'community' table/array
    isInCommunities = 1
    snmpCommunitiesCount = 0

    # Next is needed in order to not process the header as a 'community' structure
    next
}

(isInCommunities == 1) {

    #sys contact: who@where
    if ($0 ~ /^sys contact/) {

        # Found end of communities block,
        isInCommunities = 0

        # Set the 'snmp-contact'
        #sys contact: who@where
        split($0, array_contact, ":")
        snmpContact = trim(array_contact[2])
        writeComplexMetricString("snmp-contact", null, snmpContact, "false")  # Converted to new syntax by change_ind_scripts.py script

    } else {

        # Process a community line. Store the community and permission in the array
        #really-secure         network-admin                  ACL1
        snmpCommunitiesCount++
        snmpCommunities[snmpCommunitiesCount, "community"] = $1
        snmpCommunities[snmpCommunitiesCount, "permissions"] = $2

    }
}

#SNMP protocol : Enabled
/SNMP protocol.*Enabled/ {
    isSnmpEnabled = 1
    next
}

#SNMP protocol : Disabled
/SNMP protocol.*Disabled/ {
    isSnmpEnabled = 0
    next
}

#sys location: snmplocation
/sys location:/ {
    split($0, arrayLocation, ":")
    snmpLocation = trim(arrayLocation[2])
    next
}


END {

    # Publish snmp-enabled
    snmpEnabledValue = "false"
    if (isSnmpEnabled == 1) {
        snmpEnabledValue = "true"
    }
    writeComplexMetricString("snmp-enabled", null, snmpEnabledValue, "false")  # Converted to new syntax by change_ind_scripts.py script

    # Publish snmp-communities
    writeComplexMetricObjectArray("snmp-communities", null, snmpCommunities, "false")  # Converted to new syntax by change_ind_scripts.py script

    # Publish snmp-location
    writeComplexMetricString("snmp-location", null, snmpLocation, "false")  # Converted to new syntax by change_ind_scripts.py script

    # Publish snmp-contact
    writeComplexMetricString("snmp-contact", null, snmpContact, "false")  # Converted to new syntax by change_ind_scripts.py script
}