BEGIN {
    fn64251_pn["N9K-C9504-FM-E"] = 1
    fn64251_pn["N9K-C9508-FM-E"] = 1
    fn64251_pn["N9K-X9732C-EX"] = 1
}

#NAME: "Slot 43", DESCR: "Nexus9000 C9508 (8 Slot) Chassis Fan Module"
/NAME:.*DESCR:/ {
    split($0, arrayLine, ",")

    split(arrayLine[1], arrayName, ":")
    name = trim(arrayName[2])

    # Remove from variable name the " character
    gsub(/\"/, "", name)

}

#PID: N9k-X9732C-EX , VID: , SN: SAL17257AQQ
#PID: N77-C7706           ,  VID: V01 ,  SN: FXS1123Q333
/PID:.*SN:/ {
    split($0, arrayLine, ",")

    split(arrayLine[1], arrayPID, ":")
    pid = toupper(trim(arrayPID[2]))

    split(arrayLine[2], arrayVID, ":")
    vid = toupper(trim(arrayVID[2]))

    split(arrayLine[3], arraySN, ":")
    sn = trim(arraySN[2])

    # Check for fn-64251 "Nexus 9000 Series N9K-C9504-FM-E/N9K-C9508-FM-E/N9K-X9732C-EX Might Fail After 18 Months or Longer Due to Clock Signal Component Failure"
    if (vid == "") {
        vid = "N/A"
    }

    fn_tags["name"] = name " " pid " VID:" vid " (SN:" sn ")"
    fn64251_found = "false"
    if (pid in fn64251_pn) {
        if ((vid == "N/A") || (vid == "V01")) {
            fn64251_found = "true"
        }
    }

    writeComplexMetricString("cisco-nexus-fn64251", fn_tags, fn64251_found, "false")  # Converted to new syntax by change_ind_scripts.py script
}