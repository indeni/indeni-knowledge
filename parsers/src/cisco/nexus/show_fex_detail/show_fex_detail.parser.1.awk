# -------------------------------------------------------------------------------------------------
# The script publish the 'fex-status' for each CISCO Nexus 2k Series switches (FEX) based on the output of the 'show fex detail' command
# The value depends from the fex status. If the status is not "online" then the value is 0.
#
# Apart from the alarm value, the following tags are published.
#   1. FEX name (is 'FEX-XXX' where XXX is the fex number)
#   2. FEX status (Online | Offline | Discovered | AA Version Mismatch |... )
#   3. FEX model
#   4. FEX serial
# -------------------------------------------------------------------------------------------------


# ------------------------------------------------------------------
# A utility function that returns the part of the string "stringToReadFrom"
# that is between the parameter strings "startStart" and "stringEnd"
# Example:
#   param1 = 'FEX: 112 Description: FEX2k - under N5kUP'
#   param2 = 'FEX: '
#   param3 = ' Description:'
#   result = '112'
# ------------------------------------------------------------------
function getStringBetween(stringToReadFrom, stringStart, stringEnd) {

    index_start = match(stringToReadFrom, stringStart)
    index_start = index_start + length(stringStart)

    if (stringEnd == "") {
        index_end = length(stringToReadFrom) + 1
    } else {
        index_end = match(stringToReadFrom, stringEnd)
    }

     if (index_start == 0 || index_end == 0 || index_end < index_start) {

            # The "stringStart" OR "stringEnd" is not part of the "stringToReadFrom".
            # Or invalid positions "stringStart" is after "stringBefore"
            return ""

     } else {

        length_of_string = (index_end - index_start)
        return trim(substr(stringToReadFrom, index_start, length_of_string))

    }
}


BEGIN {
    # An array contains all the needed fex data.
    # Initialize the index of the array
    array_index = 0
}

#FEX: 100 Description: FEX0100   state: Online
#FEX: 112 Description: FEX2k - under N5kUP   state: AA Version Mismatch
/^FEX:/ {
    # The beginning of a FEX structure. We will parse number, description, state
    array_index++
    array_fex_info[array_index, "number"] = $2
    array_fex_info[array_index, "description"] = getStringBetween($0, " Description: ", " state: ")
    array_fex_info[array_index, "state"] = getStringBetween($0, " state: ", "")
}

#  Extender Serial: FOX1842GGY3
/Extender Serial:/ {
    array_fex_info[array_index, "serial"] = $NF
}

#  Extender Model: N2K-C2248TP-1GE,  Part No: 73-13232-02
/Extender Model:/ {
    array_fex_info[array_index, "model"] = getStringBetween($0, " Model:", ",")
}

END {

    # For each FEX publish the alarm
    array_size = array_index + 1

    for (i = 1; i < array_size; i++) {

        tags_to_publish["name"] = "FEX-" array_fex_info[i, "number"]
        tags_to_publish["state"] = array_fex_info[i, "state"]
        tags_to_publish["serial"] = array_fex_info[i, "serial"]
        tags_to_publish["model"] = array_fex_info[i, "model"]

        # The value of the alarm depends from the value of the fex-state
        isOnline = (array_fex_info[i, "state"] == "Online")

        writeDoubleMetric("fex-status", tags_to_publish, "gauge", isOnline, "true", "FEX Connectivity State", "state", "name")  # Converted to new syntax by change_ind_scripts.py script
    }
}