BEGIN {
    policy_map = "";
}

/Policy-map attached to the control-plane/ {
    policy_map = $NF
}

END {
   tags["name"] = "Control Plane (CoPP)"
   is_policy_map_none = (policy_map != "None")
   writeDoubleMetric("copp-status", tags, "gauge", is_policy_map_none, "true", "Policy Map Status", "state", "name")  # Converted to new syntax by change_ind_scripts.py script
}