BEGIN {
    # All the data per fex-item is stored in the diagnostics_fex_list array.
    diagnostics_fex_count = 0

    # Special flag, enabled when we process the 'TestFabricPorts:' section. Otherwise is 0
    is_in_ports_fabric = 0

    # Special flag, enabled when we process the 'TestForwardingPorts:' section. Otherwise is 0
    is_in_ports_forward = 0

    # Used as a prefix for all generated alarms
    diagnosticAlarmPrefix = "fex-diagnostic-status-"

    # Store the names of the fex-diagnostic-titles based on the index
    # We will use then in the END section during the publishing of the alarms
    diagnostic_tag["value-0"] = "sprom"
    diagnostic_tag["value-1"] = "inband-interface"
    diagnostic_tag["value-2"] = "fan"
    diagnostic_tag["value-3"] = "power-supply"
    diagnostic_tag["value-4"] = "temperature-sensor"
}


# Start the reading of a fex device. Store the name and the serial
#FEX-112: Fabric Extender 48x1GE + 4x10G Module  SerialNo   : FOX1938GPUY
/SerialNo   : / {
    is_in_ports_fabric = 0
    is_in_ports_forward = 0

    diagnostics_fex_count++

    #FOX1938GPUY
    diagnostics_fex_list[diagnostics_fex_count, "serial"] = $NF

    #Remove from the title 'FEX-112:' the last character ':'
    #FEX-112
    diagnostics_fex_list[diagnostics_fex_count, "name"] = substr($1, 1, (length($1) - 1))

    #Store the FEX model. It is between character ':' and ' SerialNo'
    #Fabric Extender 48x1GE + 4x10G Module
    model = substr($0, index($0,":") + 1)
    model = substr(model, 1, index(model, " SerialNo"))
    diagnostics_fex_list[diagnostics_fex_count, "model"] = model
}

# Mark the start of the 'Fabric Port' section
#TestFabricPorts:
/^TestFabricPorts:/ {
    is_in_ports_fabric = 1
    is_in_ports_forward = 0
}

# Mark the start of the 'Forward Port' section
#TestForwardingPorts:
/^TestForwardingPorts:/ {
    is_in_ports_fabric = 0
    is_in_ports_forward = 1
}

# Read the per the state of each port. If the state has 'U' or 'F' then we trigger alarm
#       .  .  .  .  .  .  .  F  .  .  .  .  .  .  .  .  .  .  U  .  .  .  .  .
(is_in_ports_fabric == 1 || is_in_ports_forward == 1) {
   if ($0 ~ / F / || $0 ~ / U /) {
    if(is_in_ports_fabric == 1){
       diagnostics_fex_list[diagnostics_fex_count, "port-fabric"] = "F"
    }
    if(is_in_ports_forward == 1){
       diagnostics_fex_list[diagnostics_fex_count, "port-forward"] = "F"
    }
   }
}


#Overall Diagnostic Result for FEX-112  : OK
/^Overall Diagnostic Result for / {
    # Store the overall status. During the END section we will store the double-value (0|1) based on the value of "status"
    #OK
    diagnostics_fex_list[diagnostics_fex_count, "status"] = $NF
}

# Store status of hard-coded alarms.
# Examples:
#0)              SPROM: ---------------> .
#1)   Inband interface: ---------------> .
#2)                Fan: ---------------> .
#3)       Power Supply: ---------------> F
#4) Temperature Sensor: ---------------> .
/[0-4]\).*---------------> / {

    #convert index from "1)" to "1"
    indexOfTag = substr($1, 1, 1)

    # store the property-name as 'value-1'
    propertyNameOfTag = "value-"  indexOfTag

    # store the property-value as the last character of the line
    valueOfTag = $NF

    #store the property value in the array
    diagnostics_fex_list[diagnostics_fex_count, propertyNameOfTag] = valueOfTag

}

# Process all the stored data and trigger an alarm when needed.
END {

    metric_category = "FEX Diagnostics Status"

    # Loop for each FEX device
    for (i = 1; i <= diagnostics_fex_count; i++) {

        # Set extra tags to be stored with the every alarm
        tags_to_publish["serial"] = diagnostics_fex_list[i, "serial"]
        tags_to_publish["device-description"] = diagnostics_fex_list[i, "model"]

        # Metric will be '1' ONLY IF status is not "OK"
        is_diagnostic_overall_ok = (diagnostics_fex_list[i, "status"] == "OK")

        tags_to_publish["name"] = diagnostics_fex_list[i, "name"] " Overall Result"
        writeDoubleMetric(diagnosticAlarmPrefix  "overall", tags_to_publish, "gauge", is_diagnostic_overall_ok, "true", metric_category, "state", "name")  # Converted to new syntax by change_ind_scripts.py script

        # For the specific FEX read each diagnostic (0 to 4) and create a metric based on the value AND the diagnostic name
        for (tag_index = 0; tag_index <= 4; tag_index++) {
            key_of_tag = "value-" tag_index
            value_of_tag = diagnostics_fex_list[i,key_of_tag]
            is_status_ok_of_diagnostic = 1

            # if is not ok then value is false (including failed and untested)
            if (value_of_tag != "" && value_of_tag != "."){
                is_status_ok_of_diagnostic = 0
            }

            tags_to_publish["name"] = diagnostics_fex_list[i, "name"] " " diagnostic_tag[key_of_tag]
            writeDoubleMetric(diagnosticAlarmPrefix diagnostic_tag[key_of_tag], tags_to_publish, "gauge", is_status_ok_of_diagnostic, "true", metric_category, "state", "name")  # Converted to new syntax by change_ind_scripts.py script
        }

        # Check if we have fail state in Port Fabric
        is_port_fabric_status_ok = (diagnostics_fex_list[i, "port-fabric"] != "F")

        # Check if we have fail state in Port Forward
        is_port_forward_status_ok = diagnostics_fex_list[i, "port-forward"] != "F"

        # Publish Alarms for ports fabric and forward
        tags_to_publish["name"] = diagnostics_fex_list[i, "name"] " Port Fabric"
        writeDoubleMetric(diagnosticAlarmPrefix "port-fabric", tags_to_publish, "gauge", is_port_fabric_status_ok, "true", metric_category, "state", "name")  # Converted to new syntax by change_ind_scripts.py script

        tags_to_publish["name"] = diagnostics_fex_list[i, "name"] " Port Forward"
        writeDoubleMetric(diagnosticAlarmPrefix "port-forward", tags_to_publish, "gauge", is_port_forward_status_ok, "true", metric_category, "state", "name")  # Converted to new syntax by change_ind_scripts.py script
    }
}