BEGIN {
    # initialize default value
    is_bash_shell_enabled = 0
}

#bash-shell             1          enabled
/bash-shell/{
    is_bash_shell_enabled = ($NF == "enabled")
}

END {
    # Publish metric
    writeDoubleMetric("nexus-feature-bash-status", null, "gauge", is_bash_shell_enabled, "false")  # Converted to new syntax by change_ind_scripts.py script
}