# telnet service enabled
/telnet service enabled/ {
    writeComplexMetricString("telnet-enabled", null, "true", "true", "Telnet Enabled")  # Converted to new syntax by change_ind_scripts.py script
}

# telnet service not enabled
/telnet service not enabled/ {
    writeComplexMetricString("telnet-enabled", null, "false", "true", "Telnet Enabled")  # Converted to new syntax by change_ind_scripts.py script
}