#http-server enabled
/http-server enabled/ {
    writeComplexMetricString("http-server-enabled", null, "true", "true", "HTTP Server Enabled")  # Converted to new syntax by change_ind_scripts.py script
}

#http-server not enabled
/http-server not enabled/ {
    writeComplexMetricString("http-server-enabled", null, "false", "true", "HTTP Server Enabled")  # Converted to new syntax by change_ind_scripts.py script
}