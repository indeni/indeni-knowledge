# will be a number. 0: no diff, >0 diff
/^[0-9]+/ {

    diff_lines = $1
    if (diff_lines > 0) {
        unsaved = "1.0"
    } else {
        unsaved = "0.0"
    }
    writeDoubleMetric("config-unsaved", null, "gauge", unsaved, "true", "Configuration Unsaved?", "boolean", "")  # Converted to new syntax by change_ind_scripts.py script
}