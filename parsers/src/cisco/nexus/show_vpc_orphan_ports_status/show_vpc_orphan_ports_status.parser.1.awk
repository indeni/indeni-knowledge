BEGIN {
  # The variable is 1 if we are processing the table content
  is_in_body = 0

  #the table size (contains unique port)
  table_unique_ports_size = 0;
}

#10             Eth1/2
#100            Eth1/2
#101            Eth1/2
(is_in_body == 1 && NF == 2) {

    # Store only if not exist
    port = $2;
    is_exist = 0;
    for (row_index = 1; row_index <= table_unique_ports_size; row_index++) {
        if (table_ports_unique[row_index, "port"] == port) {
            is_exist = 1;
            break;
        }
    }

    if (is_exist == 0) {
        table_unique_ports_size++;
        table_ports_unique[table_unique_ports_size, "port"] = port
    }
}


#-------        -------------------------
/^-------        --/{
    is_in_body = 1;
}

END {
    has_orphan_ports = table_unique_ports_size > 0 ? 1 : 0;

    if (has_orphan_ports == 1) {
        title = "Interface(s) ";
        for (row_index = 1; row_index <= table_unique_ports_size; row_index++) {
            title = title " " table_ports_unique[row_index, "port"];
            if (row_index != table_unique_ports_size)
                title = title ", ";
        }
    } else {
        title = "None";
    }

    # Publish metric - status
    writeDoubleMetric("orphan-port-status", null, "gauge", has_orphan_ports, "false")  # Converted to new syntax by change_ind_scripts.py script

    # Publish metric - for live config
    writeComplexMetricString("orphan-port-status-ui", null, title, "true", "Orphan ports vPC")  # Converted to new syntax by change_ind_scripts.py script
}