#-----------------------------------------------------------------------
# Helper function.
# Retrieve 0 or 1 based on the stringValue input.
#-----------------------------------------------------------------------
function getDoubleFromStatus(value) {
 if (value == "Y" || value == "y")
    return 1;
 return 0;
}


BEGIN {
    is_n5k_format = 0;
    is_n7k_format = 0;
    table_index = 0;
}

#Process a table line. Ignore header, lines with '---' and empty lines
# Example N9k
#  1 arp              3870              N      Y      N  Sat Dec 22 12:02:46 2007
# Example N5k
#clis 4023 N Y Y Sat Nov 6 15:14:53 2010
(is_n5k_format == 1 || is_n7k_format == 1) && !/^---/ {
    table_index++;

    if (is_n5k_format) {
        # basic info for n5k
        table_process[table_index, "vdc"] = "default"
        table_process[table_index, "name"] =  $1
        table_process[table_index, "pid"] =  $2

        # process statuses for n5k
        table_process[table_index, "exit"] = $3
        table_process[table_index, "stack"] = $4
        table_process[table_index, "core"] = $5
   } else {
        # basic info for n7k
        table_process[table_index, "vdc"] = $1
        table_process[table_index, "name"] = $2
        table_process[table_index, "pid"] = $3

        # process statuses for n7k
        table_process[table_index, "exit"] = $4
        table_process[table_index, "stack"] = $5
        table_process[table_index, "core"] = $6
   }
}

#n5k identifier
#Process PID Normal-exit Stack Core Log-create-time
/^Process\sPID/ && NF == 6 {
    is_n5k_format = 1;
}

#n7k identifier
#VDC Process          PID     Normal-exit  Stack  Core   Log-create-time
/^VDC\sProcess/ && NF == 7 {
    is_n7k_format = 1;
}


END {
    # Publish 3 metrics for each row
    for (row_index = 1; row_index <= table_index; row_index++) {

        tags["name"] = "Process '"  table_process[row_index, "name"] "' with PID "  table_process[row_index, "pid"] " at VDC "  table_process[row_index, "vdc"]

        status_double =  getDoubleFromStatus(table_process[row_index, "exit"]);
        writeDoubleMetric("process-normal-exit-status", tags, "gauge", status_double, "true", "Process-Normal-Exit-State", "state", "name")  # Converted to new syntax by change_ind_scripts.py script

        status_double =  getDoubleFromStatus(table_process[row_index, "stack"]);
        writeDoubleMetric("process-stack-status", tags, "gauge", status_double, "true", "Process-Stack-Trace-State", "state", "name")  # Converted to new syntax by change_ind_scripts.py script

        status_double =  getDoubleFromStatus(table_process[row_index, "core"]);
        writeDoubleMetric("process-core-status", tags, "gauge", status_double, "true", "Process-Core-File-State", "state", "name")  # Converted to new syntax by change_ind_scripts.py script

    }
}