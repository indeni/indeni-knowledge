BEGIN {
    i = 0
}

#10.100.100.0/24, ubest/mbest: 1/0
/^[0-9]+\..*\/[0-9]+,/ {
    split($1, r, ",")
    subnet = r[1]

    split(subnet, p, "/")
    prefix = p[1]
    mask = p[2]
}

#    *via 10.28.1.16, Eth1/20, [1/0], 43w1d, static
#     via 10.28.1.21, Eth1/2, [200/0], 43w1d, static
/^.*via.*static/ {
    split($2, r, ",")
    next_hop = r[1]

    i++
    static_routes[i, "network"] = trim(prefix)
    static_routes[i, "mask"] = trim(mask)
    static_routes[i, "next-hop"] = trim(next_hop)
}

END {
    writeComplexMetricObjectArray("static-routing-table", null, static_routes, "true", "Static Routing Table")  # Converted to new syntax by change_ind_scripts.py script
}