BEGIN {
    counter = -1;
}

/^[0-9]*$/ {
    counter = $1;
}

END {
    if (counter != -1) {
        tags["name"] = "Total Number of Checkpoints"
        writeDoubleMetric("checkpoint-status", tags, "gauge", counter, "true", "Configuration Checkpoints", "counter", "name")  # Converted to new syntax by change_ind_scripts.py script
    }
}