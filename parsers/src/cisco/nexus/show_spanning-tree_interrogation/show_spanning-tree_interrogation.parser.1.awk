/[0-9]+/ {
    if ($0 > 0) {
        writeTag("spanning-tree", "true")
    } else {
        writeTag("spanning-tree", "false")
    }
}