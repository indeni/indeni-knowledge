#Cisco IOS Software, C3900 Software (C3900-UNIVERSALK9-M), Version 15.4(3)M3, RELEASE SOFTWARE (fc2)
#Cisco IOS Software, Catalyst 4500 L3 Switch Software (cat4500-ENTSERVICESK9-M), Version 12.2(31)SG, RELEASE SOFTWARE (fc2)
#Cisco IOS Software, C800 Software (C800-UNIVERSALK9-M), Version 15.4(3)M3, RELEASE SOFTWARE (fc2)
#Cisco IOS Software, C2951 Software (C2951-UNIVERSALK9-M), Version 15.4(3)M4, RELEASE SOFTWARE (fc1)
/^Cisco IOS [S|s]oftware/ {

    isIOS = 1

    # retrieve version
    split($0, header, ",")
    split(trim(header[3]), versionArray, " ")
    version = trim(versionArray[2])

}

# Extra handling to avoid to interrogate as 'ios' an 'ios-xe' device
#Cisco IOS-XE software, Copyright (c) 2005-2016 by cisco Systems, Inc.
/^Cisco IOS-XE [S|s]oftware/ {
    isIOSXE = 1
}


#Cisco CISCO2901/K9 (revision 1.0) with 479232K/45056K bytes of memory.
#Cisco 886VAG2 (MPC8300) processor (revision 1.0) with 472064K/52224K bytes of memory.
#Cisco C881-K9 (revision 1.0) with 488524K/35763K bytes of memory.
#cisco WS-C2960-24PC-L (PowerPC405) processor (revision L0) with 65536K bytes of memory.
/^[C|c]isco.*(processor|revis).*memory/ {
    model = trim($2)
}

#Processor board ID FGL210610F0
/^Processor board ID/ {
    serial = trim($4)
}

#DEVICE-NAME uptime is 8 weeks, 15 hours, 32 minutes
#chickenhawk.indeni.com uptime is 8 weeks, 15 hours, 32 minutes
/uptime is/ {
    # Read the hostname (first field)
    split($1, hostNameArr, /\./)
    hostname = hostNameArr[1]
}

END {
    #Only publish tags if IOS and NOT IOS-XE
    if (isIOS == 1 && isIOSXE != 1) {
        writeTag("vendor", "cisco")
        writeTag("os.name", "ios")

        writeTag("os.version", version)
        writeTag("serial", serial)
        writeTag("model", model)

        if (hostname != "") {
            writeTag("hostname", hostname)
        }
    }
}