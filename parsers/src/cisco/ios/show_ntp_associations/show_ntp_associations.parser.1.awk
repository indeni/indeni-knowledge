# SAMPLE #############################################################################
#
#  address         ref clock       st   when   poll reach  delay  offset   disp
# ~127.127.1.1     .LOCL.           9      7     16   377  0.000   0.000  1.204
# ~129.250.35.250  .STEP.          16      -    512     0  0.000   0.000 15937.
#*~10.81.254.202   .GPS.            1     48     64     3 69.827 -31.137  2.011
# ~10.81.254.131   .STEP.          16      -     64     0  0.000   0.000 15937.
# * sys.peer, # selected, + candidate, - outlyer, x falseticker, ~ configured

/(address|poll|reach)/ {
next
}

/.*[1-9][0-9]*\.[1-9]/ {
    remote = $1
    reach = $6
    if (reach == 0) {
        state = "0.0"
    } else {
        state = "1.0"
    }

    gsub(/[\*\+\-\=]/, "", remote)
    trim(remote)

    ntptags["name"] = remote
    writeDoubleMetric("ntp-server-state", ntptags, "gauge", state, "false")  # Converted to new syntax by change_ind_scripts.py script
}