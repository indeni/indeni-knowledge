#SAMPLE
#mA: milliamperes, dBm: decibels (milliwatts), NA or N/A: not applicable.
#++ : high alarm, +  : high warning, -  : low warning, -- : low alarm.
#A2D readouts (if they differ), are reported in parentheses.
#The threshold values are calibrated.
#
#                              High Alarm  High Warn  Low Warn   Low Alarm
#          Temperature         Threshold   Threshold  Threshold  Threshold
#Port       (Celsius)          (Celsius)   (Celsius)  (Celsius)  (Celsius)
#--------- ------------------  ----------  ---------  ---------  ---------
#Gi1/0/25    39.6                90.0        85.0        -5.0      -10.0
#Gi1/0/26    42.9                90.0        85.0        -5.0      -10.0
#
#                              High Alarm  High Warn  Low Warn   Low Alarm
#           Voltage            Threshold   Threshold  Threshold  Threshold
#Port       (Volts)            (Volts)     (Volts)    (Volts)    (Volts)
#---------  ---------------    ----------  ---------  ---------  ---------
#Gi1/0/25   3.29                  3.60        3.50        3.10       3.00
#Gi1/0/26   3.26                  3.60        3.50        3.10       3.00


#The threshold values are calibrated.
/The threshold values are calibrated/ {
    next
}


#           Optical            High Alarm  High Warn  Low Warn   Low Alarm
#           Transmit Power     Threshold   Threshold  Threshold  Threshold
/High Alarm/ {
    element = ""
    if ( NF > 8) {
        for (i = 1; i <= NF - 8; i++) {
            element = element " " trim($i)
        } 
    }
    getline
    if ( NF > 4) {
        for (i = 1; i <= NF - 4; i++) {
            element = element " " trim($i)
        } 
    }
}


#Port       (Celsius)          (Celsius)   (Celsius)  (Celsius)  (Celsius)
/Port/ {
    element = element " " trim($2)
    element = trim(element)
}


#Gi1/0/25    39.3                90.0        85.0        -5.0      -10.0
/^[Ff]|^[Gg]|^[Hh]|^[Tt]/ {

    interface = trim($1)
    current_value = trim($2)
    high_alarm = trim($3)
    high_warn = trim($4)
    low_alarm = trim($5)
    low_warn = trim($NF)
    
    transceiver["name"] = element "  " interface
    transceiver["im.dstype.displayType"] = "state"

    if (current_value > high_alarm || current_value < low_alarm) {
        state = 0
    }
    else {
        state = 1
    }
    writeDoubleMetric("hardware-element-status", transceiver, "gauge", state, "false")  # Converted to new syntax by change_ind_scripts.py script
    
}