# Helper function, just return the last before end sentence. Example:
# input [CORE01 uptime is 7 years] output [7]
function getLastBefore(line) {
    length_of_array = split(line, array_line, " ")
    if(length_of_array >= 2) {
        return trim(array_line[length_of_array-1])
    } else {
        return 0
    }
}

#Cisco IOS Software, C2951 Software (C2951-UNIVERSALK9-M), Version 15.4(3)M4, RELEASE SOFTWARE (fc1)
#Cisco IOS Software, ISR Software (X86_64_LINUX_IOSD-UNIVERSALK9-M), Version 15.5(3)S2, RELEASE SOFTWARE (fc2)
/^Cisco IOS/ {

    split($0, lineArr, /,\s/)
    versionElement = lineArr[3]

    #Version 15.2(4)S5 (replace the Version with "")
    sub(/^[Vv]ersion\s/, "", versionElement)

    writeComplexMetricString("os-version", null, versionElement, "true", "OS-Version")  # Converted to new syntax by change_ind_scripts.py script
    writeComplexMetricString("os-name", null, "ios", "true", "OS-Name")  # Converted to new syntax by change_ind_scripts.py script
    writeComplexMetricString("vendor", null, "cisco", "true", "Vendor")  # Converted to new syntax by change_ind_scripts.py script
}

#ISR-DC-LAB uptime is 2 minutes
#ISR-DC-LAB uptime is 2 hours, 29 minutes
#ISR-DC-LAB uptime is 4 days, 1 hour, 5 minutes
#ISR-DC-LAB uptime is 4 weeks, 4 days, 1 hour, 5 minutes
#CORE01 uptime is 7 years, 32 weeks, 8 hours, 58 minutes
/^.+\suptime is/ {

    # split the line in array using ',' as separator
    split($0, uptime_array, ",")

    # init with zero uptime
    uptime_in_seconds = 0

    # for every time-part of the line, parse and add in the 'uptime_in_seconds'
    for(key in uptime_array) {
        time_duration = uptime_array[key]

        if(time_duration ~ /year/) {
            # Common year days 365
            uptime_in_seconds += getLastBefore(time_duration) * 31536000
        } else if(time_duration ~ /month/) {
            # Common month days 30
            uptime_in_seconds +=  getLastBefore(time_duration) * 2592000
        } else if(time_duration ~ /week/) {
            uptime_in_seconds +=  getLastBefore(time_duration) * 604800
        } else if(time_duration ~ /day/) {
            uptime_in_seconds += getLastBefore(time_duration) * 86400
        } else if(time_duration ~ /hour/) {
            uptime_in_seconds += getLastBefore(time_duration) * 3600
        } else if(time_duration ~ /minute/) {
            uptime_in_seconds += getLastBefore(time_duration) * 60
        }
    }

    writeDoubleMetric("uptime-milliseconds", null, "gauge", (uptimeInSeconds*1000), "false")  # Converted to new syntax by change_ind_scripts.py script
    # Display in Overview - Live Config the uptime (in seconds)
    writeDoubleMetric("live-config-uptime", null, "gauge", uptimeInSeconds, "true", "Device Uptime", "duration", "")  # Converted to new syntax by change_ind_scripts.py script

    #R1.indeni.com (Resolve the Hostname from the FQDN)
    hostname = $1
    split(hostname, hostNameArr, /\./)
    hostname = hostNameArr[1]

    if ( hostname != "") {
        writeComplexMetricString("hostname", null, hostname, "true", "Hostname")  # Converted to new syntax by change_ind_scripts.py script
    }
}