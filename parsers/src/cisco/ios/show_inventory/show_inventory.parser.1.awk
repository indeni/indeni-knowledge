BEGIN {
    entry = 0
}

# SAMPLE Output
#NAME: "Chassis", DESCR: "Cisco 7206VXR, 6-slot chassis"
#PID: CISCO7206VXR      , VID:    , SN: 4279256517 
#NAME: "NPE400 0", DESCR: "Cisco 7200VXR Network Processing Engine NPE-400"
#PID: NPE-400           , VID:    , SN: 11111111   
#NAME: "module 0", DESCR: "I/O FastEthernet (TX-ISL)"
#PID: C7200-IO-FE-MII/RJ, VID:    , SN: 4294967295 
#NAME: "module 1", DESCR: "Dual Port FastEthernet (RJ45)"
#PID: PA-2FE-FX         , VID:    , SN: XXX00000000
#NAME: "module 2", DESCR: "Dual Port FastEthernet (RJ45)"
#PID: PA-2FE-FX         , VID:    , SN: XXX00000000
#NAME: "module 3", DESCR: "Dual Port FastEthernet (RJ45)"
#PID: PA-2FE-FX         , VID:    , SN: XXX00000000
#NAME: "Power Supply 0", DESCR: "Cisco 7200 AC Power Supply"
#PID: PWR-7200-AC       , VID:    , SN:            
#NAME: "Power Supply 1", DESCR: "Cisco 7200 AC Power Supply"
#PID: PWR-7200-AC       , VID:    , SN:


/PID:.*SN:/ {

    split($0, r, ",")

    split(r[1], p, ":")
    pid = trim(p[2])

    split(r[3], s, ":")
    sn = trim(s[2])

    if (length(sn)==0 || sn == "" || sn ~ "N/A") { next }

    entry++
    debugEntries()
    
    sn_list[entry, "name"] = trim(pid)
    sn_list[entry, "serial-number"] = trim(sn)
    
}

function debugEntries () {
    writeDebug("Processing entry " entry " for PID " pid " with serial # " sn)
}

function writeMetrics () {
    writeComplexMetricObjectArray("serial-numbers", null, sn_list, "true", "Serial Numbers")  # Converted to new syntax by change_ind_scripts.py script
}

END {
    writeMetrics()
}