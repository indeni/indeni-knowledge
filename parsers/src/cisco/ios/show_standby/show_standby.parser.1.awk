function resetVars () {
    interface = ""
    group = ""
    state = ""
    vip = ""
    mac = ""
    preempt = 0
    master = 0
}

BEGIN {
    # Initialize array indexes
    location = 0
    new_rec=0
    first_time=1
    metric_tags["live-config"] = "true"
}

# SAMPLE ####################################################
# FastEthernet3/0.1 - Group 31
#   State is Active
#   Virtual IP address is 192.168.31.254
#   Active virtual MAC address is 0000.0c07.ac1f (MAC In Use)
#   Preemption disabled
#   Active router is local
#   Standby router is 192.168.31.2, priority 190 (expires in 9.296 sec)
#   Priority 210 (configured 210)
#   Group name is "hsrp-Fa3/0.1-31" (default)
# FastEthernet3/0.2 - Group 32
#   State is Standby
#   Virtual IP address is 192.168.32.254
#   Active virtual MAC address is 0000.0c07.ac20 (MAC Not In Use)
#   Preemption enabled
#   Active router is 192.168.32.1, priority 210 (expires in 9.680 sec)
#   Standby router is local
#   Priority 190 (configured 190)
#   Group name is "hsrp-Fa3/0.2-32" (default)
# FastEthernet3/0.3 - Group 33
#   State is Listen
#   Virtual IP address is 192.168.33.254
#   Active virtual MAC address is 0000.0c07.ac21 (MAC Not In Use)
#   Preemption enabled
#   Active router is 192.168.33.1, priority 210 (expires in 10.224 sec)
#   Standby router is 192.168.33.2, priority 190 (expires in 10.656 sec)
#   Priority 170 (configured 170)
#   Group name is "hsrp-Fa3/0.3-33" (default)


# Line for HA entry, search for interface type at start of line
# Ethernet # FastEthernet # Forty # Gigabit # Hundred # Serial # Ten
# FastEthernet3/0.3 - Group 33
/^[Ee]ther|^[Ff]ast|^[Ff]or|^[Gg]iga|^[Hh]un|^[Ss]er|^[Tt]en/ {
    new_rec = 1
    location++
    
    if (first_time = 1) {first_time=0} else {resetVars()}

    interface = trim($1)    
    split($0, parts, "-")
    split(trim(parts[2]), grp, " ")
    group = trim(grp[2])
    
    ha_entries[location, "interface"] = interface
    ha_entries[location, "group"] = group
}

/State is/ {
    state = trim($3)
    if (state == "Active") {state = 1}
    if (state == "Standby") {state = 0}
    if (state == "Listen") {state = 0}
    ha_entries[location, "state"] = state
}

/Virtual IP/ {
    vip = trim($5)
    ha_entries[location, "vip"] = vip
}

/Active virtual/ {
    mac = trim($6)
    ha_entries[location, "mac"] = mac
}

/[Pp]reemption/ {
    if (trim($2) == "enabled") {preempt = 1} else {preempt = 0}
    ha_entries[location, "preempt"] = preempt
}

/[Aa]ctive [Rr]outer/ {
    if (trim($4) == "local") {ha_entries[location, "master"]=1} else {ha_entries[location, "master"]=0}
}

/Group name/ {
}

END {
    for (i=1; i <= location; i++) {
        # writeDebug(ha_entries[i,"interface"])
        
        metric_tags["live-config"] = "true"
        metric_tags["name"] = "hsrp:" ha_entries[i, "interface"] ":"  ha_entries[i, "group"] ":" ha_entries[i, "vip"]
        metric_tags["im.identity-tags"] = "name"

        # Member State
        metric_tags["display-name"] = "HSRP - This Member State"
        metric_tags["im.dstype.displaytype"] = "state"
        if (state = "local") {state = 1} else {state =0}
        writeDoubleMetric("cluster-member-active", metric_tags, "gauge", ha_entries[i, "master"], "false")  # Converted to new syntax by change_ind_scripts.py script

        # Cluster State
        # 1.0 if at least one member in the cluster is active and handling traffic OK, 0.0 otherwise. Must have a "name" tag.
        metric_tags["display-name"] = "HSRP - Cluster State"
        metric_tags["im.dstype.displaytype"] = "state"
        # Ask Yoni / Arie about this one - three possible states : Active, Standby, Listen
        writeDoubleMetric("cluster-state", metric_tags, "gauge", ha_entries[i, "state"], "false")  # Converted to new syntax by change_ind_scripts.py script

        # Preemption
        metric_tags["display-name"] = "HSRP - Preemption"
        metric_tags["im.dstype.displaytype"] = "boolean"
        writeDoubleMetric("cluster-preemption-enabled", metric_tags, "gauge", ha_entries[i, "preempt"], "false")  # Converted to new syntax by change_ind_scripts.py script
	delete metric_tags
    }
}