#VRRP:                                                  <<< SAMPLE Router
#  VRRP Events debugging is on
#HSRP:
#  HSRP Events debugging is on
#GLBP:
#  GLBP Errors debugging is on
#  GLBP Events debugging is on
#General Ethernet:                                      <<< SAMPLE Switch
#  Ethernet network interface debugging is on
#IP-STATIC:
#  IP static routing detail debugging is on
#SSH:
#  SSH Client debugging is on
#fastethernet:
#  Fast Ethernet events debugging is on

BEGIN {
    entry = 0
}

/ debugging is/ {

    line = trim($0)
    fields = NR
    entry++
    split(line, p1, " debugging is")
    
    name=p1[1]
    state=p1[2]
    
    writeDebug("@ " NR ":" NF " debug of : ["  name "] with state [" state "]")   
    debug_tags["name"] = trim(name)
    writeDoubleMetric("debug-status", debug_tags, "gauge", "1.0", "false")  # Converted to new syntax by change_ind_scripts.py script
}

END {
}