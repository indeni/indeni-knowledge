function resetVars () {
    interface = ""
    group = ""
    state = ""
    vip = ""
    mac = ""
    preempt = 0
    master = 0
}

BEGIN {
    # Initialize array indexes
    location = 0
    new_rec=0
    first_time=1
    metric_tags["live-config"] = "true"
}

# SAMPLE Input ##############################################
#
# FastEthernet1/0.1 - Group 31
#   State is Active
#   Virtual IP address is 172.16.31.254
#   Redirect time 600 sec, forwarder time-out 14400 sec
#   Preemption disabled
#   Active is local
#   Standby is 172.16.31.2, priority 190 (expires in 19.264 sec)
#   Priority 210 (configured)
#   Weighting 100 (default 100), thresholds: lower 1, upper 100
#   Load balancing: round-robin
# FastEthernet1/0.2 - Group 32
#   State is Standby
#   Virtual IP address is 172.16.32.254
#   Redirect time 600 sec, forwarder time-out 14400 sec
#   Preemption enabled, min delay 0 sec
#   Active is 172.16.32.1, priority 210 (expires in 6.912 sec)
#   Standby is local
#   Priority 190 (configured)
#   Weighting 100 (default 100), thresholds: lower 1, upper 100
#   Load balancing: round-robin
# FastEthernet1/0.3 - Group 33
#   State is Listen
#   Virtual IP address is 172.16.33.254
#   Redirect time 600 sec, forwarder time-out 14400 sec
#   Preemption enabled, min delay 0 sec
#   Active is 172.16.33.1, priority 210 (expires in 10.048 sec)
#   Standby is 172.16.33.2, priority 190 (expires in 16.512 sec)
#   Priority 170 (configured)
#   Weighting 100 (default 100), thresholds: lower 1, upper 100
#   Load balancing: round-robin


# Line for HA entry, search for interface type at start of line
# Ethernet # FastEthernet # Forty # Gigabit # Hundred # Serial # Ten
# FastEthernet1/0.1 - Group 31
/^[Ee]ther|^[Ff]ast|^[Ff]or|^[Gg]iga|^[Hh]un|^[Ss]er|^[Tt]en/ {
    new_rec = 1; forwarder = 0
    location++
    
    if (first_time = 1) {first_time=0} else {resetVars()}

    interface = trim($1)    
    split($0, parts, "-")
    split(trim(parts[2]), grp, " ")
    group = trim(grp[2])
    
    ha_entries[location, "interface"] = interface
    ha_entries[location, "group"] = group
}

/State is/ {
    # if in Forwarder section move to next record
    if (forwarder == 1) {next}
    state = trim($3)
    if (state == "Active") {ha_entries[location, "state"] = 1}
    if (state == "Standby") {ha_entries[location, "state"] = 0}
    if (state == "Listen") {ha_entries[location, "state"] = 0}
}

/Virtual IP/ {
    vip = trim($5)
    ha_entries[location, "vip"] = vip
}

/Virtual MAC/ {
    mac = trim($6)
    ha_entries[location, "mac"] = mac
}

/..[Pp]reemption/ {
    # if in Forwarder section move to next record
    if (forwarder == 1) {next}
    if (trim($2) ~ /enabled/) {preempt = 1} else {preempt = 0}
    ha_entries[location, "preempt"] = preempt
}

# needed to determine if master
/Active is/ {
    active_ip = trim($3)
}

# compare local with active 
/\) local/ {
    if (NF = 3 && trim($3) == "local") {local_ip = trim($2); gsub(/\(/, "", local_ip); gsub(/\)/, "", local_ip)}
    if (active_ip == "local") {ha_entries[location, "master"] = 1} else {ha_entries[location, "master"] = 0}
}

# flag to ignore entries in Forwarder sections
/Forwarder/{
    forwarder=1
}

END {
    for (i=1; i <= location; i++) {
        metric_tags["live-config"] = "true"
        metric_tags["name"] = "glbp:" ha_entries[i, "interface"] ":"  ha_entries[i, "group"] ":" ha_entries[i, "vip"]
        metric_tags["im.identity-tags"] = "name"

        # Member State
        metric_tags["display-name"] = "glbp - This Member State"
        metric_tags["im.dstype.displaytype"] = "state"
        writeDoubleMetric("cluster-member-active", metric_tags, "gauge", ha_entries[i, "master"], "false")  # Converted to new syntax by change_ind_scripts.py script

        # Cluster State
        # 1.0 if at least one member in the cluster is active and handling traffic OK, 0.0 otherwise. Must have a "name" tag.
        metric_tags["display-name"] = "glbp - Cluster State"
        metric_tags["im.dstype.displaytype"] = "state"
        writeDoubleMetric("cluster-state", metric_tags, "gauge", ha_entries[i, "state"], "false")  # Converted to new syntax by change_ind_scripts.py script

        # Preemption
        metric_tags["display-name"] = "glbp - Preemption"
        metric_tags["im.dstype.displaytype"] = "boolean"
        writeDoubleMetric("cluster-preemption-enabled", metric_tags, "gauge", ha_entries[i, "preempt"], "false")  # Converted to new syntax by change_ind_scripts.py script
	delete metric_tags
    }
}