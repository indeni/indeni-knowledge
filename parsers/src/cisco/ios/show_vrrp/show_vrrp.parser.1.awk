function resetVars () {
    interface = ""
    group = ""
    state = ""
    vip = ""
    mac = ""
    preempt = 0
    master = 0
}

BEGIN {
    # Initialize array indexes
    location = 0
    new_rec=0
    first_time=1
    metric_tags["live-config"] = "true"
}

# SAMPLE ###################################################
# FastEthernet2/0.1 - Group 31
#   State is Master
#   Virtual IP address is 192.145.31.254
#   Virtual MAC address is 0000.5e00.011f
#   Advertisement interval is 10.000 sec
#   Preemption disabled
#   Priority is 210
#   Master Router is 192.145.31.1 (local), priority is 210
#   Master Advertisement interval is 10.000 sec
#   Master Down interval is 30.179 sec
# FastEthernet2/0.2 - Group 32
#   State is Backup
#   Virtual IP address is 192.145.32.254
#   Virtual MAC address is 0000.5e00.0120
#   Advertisement interval is 10.000 sec
#   Preemption enabled
#   Priority is 190
#   Master Router is 192.145.32.1, priority is 210
#   Master Advertisement interval is 10.000 sec
#   Master Down interval is 30.257 sec (expires in 12.609 sec) Learning
# FastEthernet2/0.3 - Group 33
#   State is Backup
#   Virtual IP address is 192.145.33.254
#   Virtual MAC address is 0000.5e00.0121
#   Advertisement interval is 10.000 sec
#   Preemption enabled
#   Priority is 170
#   Master Router is 192.145.33.1, priority is 210
#   Master Advertisement interval is 10.000 sec
#   Master Down interval is 30.335 sec (expires in 24.283 sec) Learning


# Line for HA entry, search for interface type at start of line
# Ethernet # FastEthernet # Forty # Gigabit # Hundred # Serial # Ten
# FastEthernet2/0.1 - Group 31
/^[Ee]ther|^[Ff]ast|^[Ff]or|^[Gg]iga|^[Hh]un|^[Ss]er|^[Tt]en/ {
    new_rec = 1
    location++
    
    if (first_time = 1) {first_time=0} else {resetVars()}

    interface = trim($1)    
    split($0, parts, "-")
    split(trim(parts[2]), grp, " ")
    group = trim(grp[2])
    
    ha_entries[location, "interface"] = interface
    ha_entries[location, "group"] = group
}

/State is/ {
    state = trim($3)
    if (state == "Master") {ha_entries[location, "state"] = 1}
    if (state == "Backup") {ha_entries[location, "state"] = 0}
}

/Virtual IP/ {
    vip = trim($5)
    ha_entries[location, "vip"] = vip
}

/Virtual MAC/ {
    mac = trim($6)
    ha_entries[location, "mac"] = mac
}

/[Pp]reemption/ {
    if (trim($2) == "enabled") {preempt = 1} else {preempt = 0}
    ha_entries[location, "preempt"] = preempt
}

/[Mm]aster [Rr]outer/ {
    if ($0 ~ /local/) {
        ha_entries[location, "master"] = 1
    } 
    else {
        ha_entries[location, "master"] = 0
    }
}

END {
    for (i=1; i <= location; i++) {
        # writeDebug(ha_entries[i,"interface"])
        
        metric_tags["live-config"] = "true"
        metric_tags["name"] = "vrrp:" ha_entries[i, "interface"] ":"  ha_entries[i, "group"] ":" ha_entries[i, "vip"]
        metric_tags["im.identity-tags"] = "name"

        # Member State
        metric_tags["display-name"] = "vrrp - This Member State"
        metric_tags["im.dstype.displaytype"] = "state"
        writeDoubleMetric("cluster-member-active", metric_tags, "gauge", ha_entries[i, "master"], "false")  # Converted to new syntax by change_ind_scripts.py script

        # Cluster State
        # 1.0 if at least one member in the cluster is active and handling traffic OK, 0.0 otherwise. Must have a "name" tag.
        metric_tags["display-name"] = "vrrp - Cluster State"
        metric_tags["im.dstype.displaytype"] = "state"
        writeDoubleMetric("cluster-state", metric_tags, "gauge", ha_entries[i, "state"], "false")  # Converted to new syntax by change_ind_scripts.py script

        # Preemption
        metric_tags["display-name"] = "vrrp - Preemption"
        metric_tags["im.dstype.displaytype"] = "boolean"
        writeDoubleMetric("cluster-preemption-enabled", metric_tags, "gauge", ha_entries[i, "preempt"], "false")  # Converted to new syntax by change_ind_scripts.py script
	delete metric_tags
    }
}