# SAMPLE 'show clock' output ...
#.02:49:52.032 PST Thu Feb 23 2017
#*02:39:53.055 UTC Fri Mar 1 2002
 
// {

    line = trim($0)
 
    # Remove leading non-integer characters
    sub(/^[^0-9]*/, "", line)
    split(line, field, " +")
    
    # split date information
    tz = field[2]
    weekday = field[3]
    month = parseMonthThreeLetter(field[4])
    day = field[5]
    year = field[6]
    
        # split clock by colon field
    split(field[1], clock, ":")
    hour = clock[1]
    min = clock[2]
    sec = clock[3]
    
    # remove decimal from 'seconds' 
    split(sec, dec, "\\.")
    
    # Get time from begining of epoch
    d0 = datetime(year, month, day, hour, min, dec[1])
    
    # write metrics
    writeDoubleMetric("current-datetime", null, "gauge", d0, "true", "Current Date/Time", "date", "")  # Converted to new syntax by change_ind_scripts.py script
    writeComplexMetricString("timezone", null, tz, "false")  # Converted to new syntax by change_ind_scripts.py script

}