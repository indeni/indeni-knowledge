BEGIN {
    preferred = "0.0"
    entry = 0
}

# SAMPLE output
#Codes: L - local, C - connected, S - static, R - RIP, M - mobile, B - BGP
#       D - EIGRP, EX - EIGRP external, O - OSPF, IA - OSPF inter area
#       N1 - OSPF NSSA external type 1, N2 - OSPF NSSA external type 2
#       E1 - OSPF external type 1, E2 - OSPF external type 2
#       i - IS-IS, su - IS-IS summary, L1 - IS-IS level-1, L2 - IS-IS level-2
#       ia - IS-IS inter area, * - candidate default, U - per-user static route
#       o - ODR, P - periodic downloaded static route, H - NHRP, l - LISP
#       a - application route
#       + - replicated route, % - next hop override
#
#Gateway of last resort is 174.44.208.125 to network 0.0.0.0
#
#S*    0.0.0.0/0 [1/0] via 174.44.208.125
#      10.0.0.0/8 is variably subnetted, 43 subnets, 2 masks
#S        10.10.10.0/24 [1/0] via 10.1.102.201
#      172.8.0.0/24 is subnetted, 1 subnets
#S        172.8.0.0 [1/0] via 10.1.200.50
#S     192.168.0.0/24 [1/0] via 10.1.200.121
#S     192.168.5.0/24 [1/0] via 10.1.200.22
#S     192.168.200.0/24 [1/0] via 10.1.200.155

/[\*]|^[S]/ {

    line = trim($0)
	leftchars = substr($0, 1, 2)
    split(line, field, " {1,}")
    next_hop = trim(field[5])

    if ( leftchars ~ /[\*]/ ) { preferred = "1.0" }
    if ( leftchars ~ /^[S]/ ) {
        split(trim(field[2]),val,"/")  
        network=trim(val[1])
        mask=trim(val[2]) 
    }

    # add metrics to table ...
    if ( network != "" && mask != "") { addMetrics() }
    
}


function addMetrics () {

    entry++    
    routes[entry, "network"] = network
    routes[entry, "mask"] = mask
    routes[entry, "next-hop"] = next_hop
    
}

function writeMetrics () {
    writeComplexMetricObjectArray("static-routing-table", null, routes, "true", "Static Routing Table")  # Converted to new syntax by change_ind_scripts.py script
}

END {
    writeMetrics()
}