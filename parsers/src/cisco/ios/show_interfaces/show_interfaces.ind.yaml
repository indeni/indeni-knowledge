name: ios-show-interface
description: IOS show interfaces
type: monitoring
monitoring_interval: 60 minutes
requires:
    vendor: cisco
    os.name: ios
comments:
    network-interface-state:
        why: |
            Capture the interface state. If an interface transitions from up to down an alert would be raised.
        how: |
            This script logs into the Cisco IOS device using SSH and retrieves the output of the "show interface" command. The output includes the interface related information and statistics.
        can-with-snmp: true
        can-with-syslog: true
    network-interface-admin-state:
        why: |
            Capture the interface administrative state.
        how: |
            This script logs into the Cisco IOS device using SSH and retrieves the output of the "show interface" command. The output includes the interface related information and statistics.
        can-with-snmp: true
        can-with-syslog: true
    network-interface-speed:
        why: |
            Capture the interface speed in human readable format such as 1G, 10G, etc.
        how: |
            This script logs into the Cisco IOS device using SSH and retrieves the output of the "show interface" command. The output includes the interface related information and statistics.
        can-with-snmp: true
        can-with-syslog: false
    network-interface-duplex:
        why: |
            Capture the interface duplex in human readable format such as full or half. In modern network environments, it is uncommon to see interfaces configured to half-duplex. It is recommended to investigate as to whether this is an intentional configuration or an oversight. If unintentional, connectivity issues may arise.
        how: |
            This script logs into the Cisco IOS device using SSH and retrieves the output of the "show interface" command. The output includes the interface related information and statistics.
        can-with-snmp: true
        can-with-syslog: true
    network-interface-rx-frame:
        why: |
            Capture the interface Receive Errors (CRC) counter. If this counter increases an alarm will be raised.
        how: |
            This script logs into the Cisco IOS device using SSH and retrieves the output of the "show interface" command. The output includes the interface related information and statistics.
        can-with-snmp: true
        can-with-syslog: false
    network-interface-mac:
        why: |
            Capture the interface MAC address.
        how: |
            This script logs into the Cisco IOS device using SSH and retrieves the output of the "show interface" command. The output includes the interface related information and statistics.
        can-with-snmp: true
        can-with-syslog: false
    network-interface-rx-bits:
        why: |
            Capture the interface Received bits counter. Knowing the amount of bits and packets flowing through an interface can help estimate an interface's performance and utilization.
        how: |
            This script logs into the Cisco IOS device using SSH and retrieves the output of the "show interface" command. The output includes the interface related information and statistics.
        can-with-snmp: true
        can-with-syslog: false
    network-interface-rx-errors:
        why: |
            Capture the interface Received Errors counter. Packet loss may impact traffic performance.
        how: |
            This script logs into the Cisco IOS device using SSH and retrieves the output of the "show interface" command. The output includes the interface related information and statistics.
        can-with-snmp: true
        can-with-syslog: false
    network-interface-rx-packets:
        why: |
            Capture the interface Received Packets counter. Knowing the amount of bits and packets flowing through an interface can help estimate an interface's performance and utilization.
        how: |
            This script logs into the Cisco IOS device using SSH and retrieves the output of the "show interface" command. The output includes the interface related information and statistics.
        can-with-snmp: true
        can-with-syslog: false
    network-interface-ipv4-address:
        why: |
            Capture the interface IPv4 address. Only relevant for layer 3 interfaces (and sub-interfaces), including Vlan interfaces (SVI).
        how: |
            This script logs into the Cisco IOS device using SSH and retrieves the output of the "show interface" command. The output includes the interface related information and statistics.
        can-with-snmp: true
        can-with-syslog: false
    network-interface-ipv4-subnet:
        why: |
            Capture the interface IPv4 subnet mask. Only relevant for layer 3 interfaces, including Vlan interfaces (SVI).
        how: |
            This script logs into the Cisco IOS device using SSH and retrieves the output of the "show interface" command. The output includes the interface #related information and statistics.
        can-with-snmp: true
        can-with-syslog: false
    network-interface-tx-carrier:
        why: |
            Capture the interface carrier state change counter. It would increase every time the interface changes state from up to down.
        how: |
            This script logs into the Cisco IOS device using SSH and retrieves the output of the "show interface" command. The output includes the interface related information and statistics.
        can-with-snmp: true
        can-with-syslog: false
    network-interface-mtu:
        why: |
            Capture the interface MTU (Maximum Transmit Unit).
        how: |
            This script logs into the Cisco IOS device using SSH and retrieves the output of the "show interface" command. The output includes the interface related information and statistics.
        can-with-snmp: true
        can-with-syslog: false
    network-interface-tx-bits:
        why: |
            Capture the interface Transmitted bits counter. Knowing the amount of bits and packets flowing through an interface can help estimate an interface's performance and utilization.
        how: |
            This script logs into the Cisco IOS device using SSH and retrieves the output of the "show interface" command. The output includes the interface related information and statistics.
        can-with-snmp: true
        can-with-syslog: false
    network-interface-tx-errors:
        why: |
            Capture the interface Transmit Errors counter. Transmission errors indicate an issue with duplex/speed matching.
        how: |
            This script logs into the Cisco IOS device using SSH and retrieves the output of the "show interface" command. The output includes the interface related information and statistics.
        can-with-snmp: true
        can-with-syslog: false
    network-interface-tx-packets:
        why: |
            Capture the interface Transmitted Packets counter. Knowing the amount of bits and packets flowing through an interface can help estimate an interface's performance and utilization.
        how: |
            This script logs into the Cisco IOS device using SSH and retrieves the output of the "show interface" command. The output includes the interface related information and statistics.
        can-with-snmp: true
        can-with-syslog: false
    network-interface-rx-overruns:
        why: |
            Capture the interface Receive Overrun errors counter.
        how: |
            This script logs into the Cisco IOS device using SSH and retrieves the output of the "show interface" command. The output includes the interface related information and statistics.
        can-with-snmp: true
        can-with-syslog: false
    network-interface-tx-underruns:
        why: |
            Capture the interface Transmit Underrun errors counter.
        how: |
            This script logs into the Cisco IOS device using SSH and retrieves the output of the "show interface" command. The output includes the interface related information and statistics.
        can-with-snmp: true
        can-with-syslog: false
    network-interface-tx-collisions:
        why: |
            Capture the interface transimit collisions counter. Interface collisions are common when the interfaces are in half duplex mode. High value of collision packets may have impact to the traffic utilization (retransmission for TCP apps) and to the performance of the applications. Check the link for more information about troubleshooting network interface problems: https://www.cisco.com/c/en/us/support/docs/interfaces-modules/port-adapters/12768-eth-collisions.html
        how: |
            This script logs into the Cisco IOS network device by using SSH and retrieves the output of the "show interface" IOS command. The output includes all the interface related information and statistics.
        can-with-snmp: true
        can-with-syslog: false
    network-interface-description:
        why: |
            Interface descriptions are very useful for network administrators especially during event of troubleshooting and configuration change for link migrations. Adding “description” to an interface is useful for administrative purposes since this helps us to remember the port functionality and Infact the circuit details. Check the link for more information: https://www.cisco.com/c/en/us/td/docs/ios/12_4/interface/configuration/guide/icfgenrh.html#wp1102598
        how: |
            This script logs into the Cisco IOS network device by using SSH and retrieves the output of the "show interface" IOS command. The output includes all the interface related information and statistics.
        can-with-snmp: true
        can-with-syslog: false
steps:
   -  run:
          type: SSH
          command: show interfaces
      parse:
          type: AWK
          file: show_interfaces.parser.1.awk
