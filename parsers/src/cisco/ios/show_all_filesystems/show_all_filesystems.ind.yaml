name: ios-dir-all-filesystems
description: IOS dir all-filesystems
type: monitoring
monitoring_interval: 60 minutes
requires:
    vendor: cisco
    os.name: ios
comments:
    disk-used-kbytes:
        why: |
            A Cisco IOS network device maintains its filesystem in an internal flash memory and stores all the files there. This is a memory card inserted into a slot of the device. The capacity of this filesystem depends on the hardware model. When additional flash memory is needed it is common to use an external flash card. This flash card is also used to save the configuration files. It is important to have sufficient available space, otherwise a device could have critical problems e.g. the user might not be able to save a configuration or upload a software image for software upgrade.
        how: |
            This script logs into the Cisco IOS device using SSH and executes the "dir all-filesystems" command to collect information about the used space in kbytes of the file systems. More information can be found to the next configuration guide: https://www.cisco.com/c/en/us/td/docs/switches/lan/catalyst3850/software/release/3se/system_management/configuration_guide/b_sm_3se_3850_cg/b_sm_3se_3850_cg_chapter_010011.html
        can-with-snmp: true
        can-with-syslog: false
    disk-total-kbytes:
        why: |
            A Cisco IOS network device maintains its filesystem in an internal flash memory and stores all the files there. This is a memory card inserted into a slot of the device. The capacity of this filesystem depends on the hardware model. When additional flash memory is needed it is common to use an external flash card. This flash card is also used to save the configuration files. It is important to have sufficient available space, otherwise a device could have critical problems e.g. the user might not be able to save a configuration or upload a software image for software upgrade.
        how: |
            This script logs into the Cisco IOS device using SSH and uses the "dir all-filesystems" command to collect information about the total space in kbytes of the file systems. More information can be found to the next configuration guide: https://www.cisco.com/c/en/us/td/docs/switches/lan/catalyst3850/software/release/3se/system_management/configuration_guide/b_sm_3se_3850_cg/b_sm_3se_3850_cg_chapter_010011.html
        can-with-snmp: true
        can-with-syslog: false
    disk-usage-percentage:
        why: |
            Check the utilization percentage of each of the different file systems on the device. If the free space gets too low an alert will be triggered.
            Filesystems that are over utilized, i.e. reaching their max capacity, can cause device or feature instabilities.
        how: |
            This script logs into the Cisco IOS device using SSH and uses the "dir all-filesystems" to collect information about the device's file systems.
        can-with-snmp: false
        can-with-syslog: false
steps:
-   run:
        type: SSH
        file: show_all_filesystems.remote.1.bash
    parse:
        type: AWK
        file: show_all_filesystems.parser.1.awk
