BEGIN {
   found_sys_name = 0
   found_mgmt_ip = 0
   entry = 0
}

/^--/ {
    sys_name = ""
    mgmt_ip = ""
    next
}

#System Name: not advertised
/^System Name: not advertised/ {
    next
}


#Management Address: not advertised
/^Management Address: not advertised/ {
    next
}

# System Name: <device-name>
/^System Name:/ {
    sys_name = trim($3)
}

# System Description: (contains /[Cc]isco/)
/^System Description:/ {
    getline
    sys_desc = trim($0)
}

# Management Addresses:
#    IP: 10.1.101.16
/^Management Addresses:/ {
    getline
    if ($0 ~ /IP:/) { found_mgmt_ip = 1; mgmt_ip = $2 }
    # if ($0 ~ /Other:/) { found_mgmt_ip = 1; split($0, parts, "Other:"); mgmt_ip = parts[2] }
}

/^Vlan ID:/ {  
    # writeDebug(NR " - " $0)
    
    if (sys_name != "" && mgmt_ip != "" ) {
        writeDebug(" ==> System Name " sys_name)
        writeDebug(" ==> System Description " sys_desc)
        writeDebug(" ==> Management Address " mgmt_ip)
        entry++
        device_list[entry, "name"] = sys_name
        device_list[entry, "ip"] = mgmt_ip
        next
    }
}

END {
    writeComplexMetricObjectArray("known-devices", null, device_list, "false")  # Converted to new syntax by change_ind_scripts.py script
}