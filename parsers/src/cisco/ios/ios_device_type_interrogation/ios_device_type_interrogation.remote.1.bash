# Commands not usually found on Switches
show async ?
show frame-relay ?
show x25 ?
show srp ?
show ip dfp ?

# Available on both product types ...
show version | include (Cisco IOS|uptime|[Pp]rocessor)
# EOF REMOTE::SSH