BEGIN {
    # We assume product is router 
    product = "router"
    # Set error counter to zero (0)
    err=0
}

# Cisco IOS Software, C2951 Software (C2951-UNIVERSALK9-M), Version 15.4(3)M4, RELEASE SOFTWARE (fc1)
/^Cisco IOS [S|s]oftware/ {

    family=trim($4)
    split($0, header, ",")
    split(trim(header[3]), ver, " ")
    version=trim(ver[2])        
    cisco = 1
    ios = 1

}

# Get device model 
/isco.*processor.*memory/ {
    model = trim($2)
}

/Processor board ID/ {
	serial = trim($4)
}

/nrecognized command/ {
    err++
}

END {

    if ( cisco == 1 ) {
    
        # Identifying product type, i.e. switch or router - if errors encountered during 'show' commands
        if ( err > 2 ) { product = "switch" }

        # writeTag("vendor", "cisco")
        writeTag("product", product)
        # writeTag("serial", serial)
        # writeTag("model", model)
        # writeTag("os.name", "ios")
        # writeTag("os.version", version)

    }
}