BEGIN {
    entry = 0
    domain_name = "Not Defined"
}

#ip domain name cisco.com
/^ip domain name/ {
    domain_name = $4
}

# SAMPLE BANNER
# banner login ^C
# Unauthorized Login
# This Device is managed by the ACME corporation
# If you have not been explicitly granted access to this device logoff immediately
# LAST WARNING
# ^C
/^banner login/{

    banner_text = ""

    # Find out character used for delimiter - last field entry
    delimiter = $NF

    # move to next line, where banner actually starts
    getline

    # iterate next lines until 'delimiter' is encountered
    is_first_iteration = 1
    while ( $NF != delimiter ) {

        # in case of first iteration do not append the banner_text.
        if (is_first_iteration == 1) {
            banner_text = $0
            is_first_iteration = 0
        } else {
            banner_text = banner_text "\\n" $0
        }
        getline

    }
}


    

END {
    #publish domain
    domain_tags["display-name"] = "Domain Name"
    writeComplexMetricString("domain", domain_tags, domain_name, "false")  # Converted to new syntax by change_ind_scripts.py script

    #publish the login-banner text
    writeComplexMetricString("login-banner", null, banner_text, "false")  # Converted to new syntax by change_ind_scripts.py script
}