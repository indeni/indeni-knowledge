BEGIN {
    entry=0
    newrec=0
    firstloop=1
}


#SAMPLE
#-------------------------
#Device ID: R3.cisco.com
#Entry address(es): 
#  IP address: 10.10.23.2
#Platform: Cisco 7206VXR,  Capabilities: Router 
#Interface: FastEthernet1/0,  Port ID (outgoing port): FastEthernet1/0
#Holdtime : 159 sec
#
#Version :
#Cisco IOS Software, 7200 Software (C7200-ADVIPSERVICESK9-M), Version 15.2(4)S5, RELEASE SOFTWARE (fc1)
#Technical Support: http://www.cisco.com/techsupport
#Copyright (c) 1986-2014 by Cisco Systems, Inc.
#Compiled Thu 20-Feb-14 06:51 by prod_rel_team
#
#advertisement version: 2
#Duplex: full
#Management address(es): 
#  IP address: 10.10.23.2

/^------/ {
    if (firstloop==0) { 
        addMetrics(); 
        firstloop=1 
        }
    newrec=1
    ip_address=""
    next
}

/^Device/ {
   
    line = trim($0)
    
    split(line, field, ":")
    name = trim(field[2])
    # writeDebug("Device found : " NR " " name)
    next

}


// {
    if (trim($1) ~ /---/ )      { next }
    if (trim($1) == "Device" )  { next }
        
    if (newrec==1) {newrec=0}
    firstloop=0
    
    if ($0 ~ /IP addr/) { 
        if (ip_address == "") { ip_address = $3; next }
        else
        { mgmt_address = $3; next}
        }
    if ($0 ~ /latform/) {   platform = $2 " " $3; 
                            sub(/,/, "", platform); 
                            capabilities = $5; 
                            if (NF > 5) {
                            for (i=6; i <= NF; i++) {
                                capabilities = capabilities " " $i
                            }
                            next
                        }
                            next }
    if ($0 ~ /nterface/) { interface = $2; sub(/,/, "", interface);next }
    if ($0 ~ /oldtime/) { holdtime = $3; next }
    if ($0 ~ /ersion\:/) { next }
    if ($0 ~ /ersion /) { split($0, ver1, "ersion "); split(ver1[2], ver2, ","); version = ver2[1]; next }
    if ($0 ~ /uplex/) { duplex = $2 }
    
}


function debugEntries () {

    writeDebug("Processing entry " entry)

    writeDebug("name         : " name)
    writeDebug("ip_address   : " ip_address)
    writeDebug("platform     : " platform)
    writeDebug("capabilities : " capabilities)
    writeDebug("interface    : " interface)
    writeDebug("holdtime     : " holdtime)
    writeDebug("version      : " version)
    writeDebug("duplex       : " duplex)
    writeDebug("MGMT address : " mgmt_address)
    writeDebug("")
}

function addMetrics () {
    
    entry++
    debugEntries()
    
    cdptags["im.identity-tags"] = "name"
    cdptags["display-name"] = "CDP"
    
    cdpobj[entry, "name"] = name
    cdpobj[entry, "ip_address"] = ip_address
    cdpobj[entry, "platform"] = platform
    cdpobj[entry, "capabilities"] = capabilities
    cdpobj[entry, "interface"] = interface
    cdpobj[entry, "holdtime"] = holdtime
    cdpobj[entry, "version"] = version
    cdpobj[entry, "duplex"] = duplex
    cdpobj[entry, "mgmt_address"] = mgmt_address
}

function writeMetrics () {

    writeComplexMetricObjectArray("known-devices", cdptags, cdpobj, "false")  # Converted to new syntax by change_ind_scripts.py script
}

END {
    addMetrics();
    writeMetrics(); 
}