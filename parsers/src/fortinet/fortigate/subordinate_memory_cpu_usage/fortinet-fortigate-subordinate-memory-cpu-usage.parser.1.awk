
#1.3.6.1.4.1.12356.101.13.2.1.1.1 fgHaStatsIndex An index value that uniquely identifies an unit in the HA Cluster
#1.3.6.1.4.1.12356.101.13.2.1.1.2 fgHaStatsSerial   Serial number of the HA cluster member for this row
#1.3.6.1.4.1.12356.101.13.2.1.1.3 fgHaStatsCpuUsage CPU usage of the specified cluster member (percentage)
#1.3.6.1.4.1.12356.101.13.2.1.1.4 fgHaStatsMemUsage Memory usage of the specified cluster member (percentage)
#1.3.6.1.4.1.12356.101.13.2.1.1.5 fgHaStatsNetUsage Network bandwidth usage of specified cluster member (kbps)
#1.3.6.1.4.1.12356.101.13.2.1.1.6 fgHaStatsSesCount Current session count of specified cluster member
#1.3.6.1.4.1.12356.101.13.2.1.1.7 fgHaStatsPktCount Number of packets processed by the specified cluster member
#1.3.6.1.4.1.12356.101.13.2.1.1.8 fgHaStatsByteCount  Number of bytes processed by the specified cluster member
#1.3.6.1.4.1.12356.101.13.2.1.1.9 fgHaStatsIdsCount Number of IDS/IPS events triggered on the specified cluster member
#1.3.6.1.4.1.12356.101.13.2.1.1.10 fgHaStatsAvCount  Number of anti-virus events triggered on the specified cluster member 
#1.3.6.1.4.1.12356.101.13.2.1.1.11 fgHaStatsHostname Host name of the specified cluster member
#1.3.6.1.4.1.12356.101.13.2.1.1.12 fgHaStatsSyncStatus  Current HA Sync status
#1.3.6.1.4.1.12356.101.13.2.1.1.13 fgHaStatsSyncDatimeSucc Date and time of last successful sync
#1.3.6.1.4.1.12356.101.13.2.1.1.14 fgHaStatsSyncDatimeUnsucc  Date and time of last unsuccessful sync
#1.3.6.1.4.1.12356.101.13.2.1.1.15 fgHaStatsGlobalChecksum Current HA global checksum value
#1.3.6.1.4.1.12356.101.13.2.1.1.16 fgHaStatsMasterSerial  Serial number of master during the last synch attempt (successful of #not)


#CPU Usage
#1.3.6.1.4.1.12356.101.13.2.1.1.3.1 = 20
#1.3.6.1.4.1.12356.101.13.2.1.1.3.2 = 25
#1.3.6.1.4.1.12356.101.13.2.1.1.3.N = 20
#.1 is Primary unit
#.2 is Subordinate unit
#.N is Nth subordiante unit

/^1\.3\.6\.1\.4\.1\.12356\.101\.13\.2\.1\.1\.3/ {
   key = getSnmpOid($0)
   device_index = extractTrailingOid(key, 14)

   if (device_index != 1) {   #We ignore primary device
         cpu_usage_table[device_index] = getSnmpValue($0)
   }
   
}

#Memory Usage
#1.3.6.1.4.1.12356.101.13.2.1.1.4.1 = 90
#1.3.6.1.4.1.12356.101.13.2.1.1.4.2 = 95
#1.3.6.1.4.1.12356.101.13.2.1.1.4.N = 70
#.1 is Primary unit
#.2 is Subordinate unit
#.N is Nth subordiante unit

/^1\.3\.6\.1\.4\.1\.12356\.101\.13\.2\.1\.1\.4/ {
   key = getSnmpOid($0)
   device_index = extractTrailingOid(key, 14)

   if (device_index != 1) {	#We ignore primary device
         memory_usage_table[device_index] = getSnmpValue($0)
   }
}

#Device name appears after the information about memory and cpu, so we store in array
#Device name
/^1\.3\.6\.1\.4\.1\.12356\.101\.13\.2\.1\.1\.11/ {
   key = getSnmpOid($0)
   value = getSnmpValue($0)
   device_index = extractTrailingOid(key, 14)
   device_index_to_name[device_index] = value
}

END {
   for (key in memory_usage_table) {
      tags["memory-usage-device"] = device_index_to_name[key] #We get device name
      subordinate_memory_usage = memory_usage_table[key]      #We get memory value
      writeDoubleMetric("subordinate-fortigate-memory-usage",tags,"gauge",subordinate_memory_usage,"true","Subordinate devices",number,"memory-usage-device")
   }

   for (key in cpu_usage_table) {
      tags["cpu-usage-device"] = device_index_to_name[key]  #We get device name
      subordinate_cpu_usage = cpu_usage_table[key]          #We get cpu value
      writeDoubleMetric("subordinate-fortigate-cpu-usage", tags, "gauge",subordinate_cpu_usage,"true","Subordinate devices",number,"cpu-usage-device")
   }

}