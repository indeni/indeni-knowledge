BEGIN {
    # The number of lines in the output that have content (length > 0)
    # Note that 'NR' variable is not available
    count_line = 0
}

{
    # If there is a line with characters increase the counter
    if(length > 0)
        count_line = count_line + 1
}

END {
   tags["name"] = "Telnet Status"
   tags["im.identity-tags"] = "name"
   writeComplexMetricString("telnet-enabled", tags, is_enabled = count_line > 0 ? "true" : "false", "true", "Management Services")  # Converted to new syntax by change_ind_scripts.py script
}