from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

# "results":[
#       {
#         "ip_version":4,
#         "type":"static",
#         "ip_mask":"0.0.0.0\/0",
#         "distance":10,
#         "metric":0,
#         "gateway":"10.11.93.254",
#         "interface":"port1"
#       },
#       {
#         "ip_version":4,
#         "type":"connect",
#         "ip_mask":"10.11.93.0\/24",
#         "distance":0,
#         "metric":0,
#         "gateway":"0.0.0.0",
#         "interface":"port1"
#       }
#     ],
#     "vdom":"root",
#     "path":"router",
#     "name":"ipv4",
#     "action":"select",
#     "status":"success",
#     "serial":"FGVM04TM19000334",
#     "version":"v6.0.3",
#     "build":200
#   }


class RestMonitorRouter(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:  # we check if not empty
            data = helper_methods.parse_data_as_json(raw_data)
            for entry in data:
                vdom_id = entry['vdom']
                results = entry['results']
                default_static_route = 0
                for route in results:  # we loop for all vdoms
                    if route['type'] == 'static' and route['ip_mask'] == '0.0.0.0/0':
                        default_static_route = 1  # default route found
                tags = {'name': vdom_id}  # We save what vdom we are checking
                self.write_double_metric('no-default-route', tags, 'gauge', default_static_route, False)
        return self.output
