BEGIN{
    # Store mask in
    table_arp_index = 0
}

# Parse all the needed info ('targetip', 'mac' & 'interface') and store them in the table
#10.10.8.145       0          88:1d:fc:60:4b:c6 lan
#212.205.216.193   0          88:1d:fc:60:4b:c6 wan1
/^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}/{

    # Increase index of table
    table_arp_index++

    table_arp[table_arp_index, "targetip"] = $1
    table_arp[table_arp_index, "mac"] = $3
    table_arp[table_arp_index, "interface"] = $4
    table_arp[table_arp_index, "success"] = 1
}

END {
    writeComplexMetricObjectArray("arp-table", null, table_arp, "false")  # Converted to new syntax by change_ind_scripts.py script
}