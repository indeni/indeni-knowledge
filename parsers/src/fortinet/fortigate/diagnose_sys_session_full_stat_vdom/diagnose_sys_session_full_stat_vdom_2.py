from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

states = ['ESTABLISHED', 'SYN_SENT', 'SYN_RECV', 'FIN_WAIT1', 'FIN_WAIT2', 'TIME_WAIT', 'CLOSE', 'CLOSE_WAIT',
            'LAST_ACK', 'LISTEN', 'CLOSING', 'UNKNOWN']

class DiagnoseSysSessionFullStat2(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data = helper_methods.parse_data_as_list(raw_data, 'diagnose_sys_session_full_stat.textfsm')
            if data:
                for vdom_data in data:
                    tags = {}
                    tags['vdom.name'] = dynamic_var['vdom_name']
                    tags['im.identity-tags'] = 'vdom.name'
                    self.write_double_metric('sessions-total-count', tags, 'gauge', int(vdom_data['current_sessions']), True, 'Sessions - Current Total', 'number')
                    self.write_double_metric('sessions-setup-rate', tags, 'gauge', int(vdom_data['sessions_per_sec']), True, 'Sessions - Current Rate (conns/sec)', 'number')
                    self.write_double_metric('clash-sessions', tags, 'counter', int(vdom_data['clash_sessions']), True, 'Sessions - Clash Total', 'number')
                    self.write_double_metric('sessions-removeable', tags, 'gauge', int(vdom_data['removal_sessions']), True, 'Sessions - Removable Total', 'number')
                    self.write_double_metric('memory-tension-drop', tags, 'counter', int(vdom_data['memory_tension_drop']), True, 'Sessions - Removed Due Low Memory', 'number')
                    self.write_double_metric('sessions-dev-down', tags, 'counter', int(vdom_data['removed_sessions_due_down_int']), True, 'Sessions - Removed Due Interface Down', 'number')
                    self.write_double_metric('flush-sessions', tags, 'gauge', int(vdom_data['removed_sessions_due_flush']), True, 'Sessions - Removed Due Flush', 'number')
                    self.write_double_metric('ephemeral-sessions', tags, 'counter', int(vdom_data['sessions_ephemeral_state']), True, 'Sessions - Ephemeral State', 'number')
                    for state in states:
                        tags['tcp_session_state'] = state
                        tags['im.identity-tags'] = 'vdom.name|tcp_session_state'
                        found = False
                        for stat in vdom_data['tcp_sessions']:
                            if len(stat) > 0 and not found:
                                if state == str(stat).split()[2]:
                                    found = True
                                    self.write_double_metric('tcp-sessions-stats', tags, 'gauge', int(str(stat).split()[0]), True, 'Sessions - TCP Stats', 'number')
                        if not found:
                            self.write_double_metric('tcp-sessions-stats', tags, 'gauge', 0, True, 'Sessions - TCP Stats', 'number')
        return self.output
