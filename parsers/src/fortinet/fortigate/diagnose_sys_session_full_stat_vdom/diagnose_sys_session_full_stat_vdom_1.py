from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class DiagnoseSysSessionFullStat1(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        # Step 1 : Data Extraction
        if raw_data:
            data = helper_methods.parse_data_as_list(raw_data, 'diagnose_sys_vd_list.textfsm')
            if data:
                for vdom_entry in data:
                    if 'vsys_' not in vdom_entry['vdom_name']:
                        self.write_dynamic_variable('vdom_name', vdom_entry['vdom_name'])
        return self.output
