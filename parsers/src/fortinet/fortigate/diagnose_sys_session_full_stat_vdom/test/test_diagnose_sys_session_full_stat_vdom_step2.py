import os
import unittest

from fortinet.fortigate.diagnose_sys_session_full_stat_vdom.diagnose_sys_session_full_stat_vdom_2 import DiagnoseSysSessionFullStat2
from parser_service.public.action import *

class TestDiagnoseSysSessionFullStatVdom2(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = DiagnoseSysSessionFullStat2()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_diagnose_sys_session_full_stat(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/diagnose_sys_session_full_stat.input', {'vdom_name': 'root'}, {})
        # Assert
        self.assertEqual(20, len(result))
        self.assertEqual(result[0].name, 'sessions-total-count')
        self.assertEqual(result[0].value, 21)
        self.assertEqual(result[1].name,'sessions-setup-rate')
        self.assertEqual(result[1].value, 0)
        self.assertEqual(result[2].name, 'clash-sessions')
        self.assertEqual(result[2].value, 0)
        self.assertEqual(result[3].name, 'sessions-removeable')
        self.assertEqual(result[3].value, 0)
        self.assertEqual(result[4].name, 'memory-tension-drop')
        self.assertEqual(result[4].value, 0)
        self.assertEqual(result[5].name, 'sessions-dev-down')
        self.assertEqual(result[5].value, 4)
        self.assertEqual(result[6].name, 'flush-sessions')
        self.assertEqual(result[6].value, 3)
        self.assertEqual(result[7].name, 'ephemeral-sessions')
        self.assertEqual(result[7].value, 0)
        self.assertEqual(result[8].name, 'tcp-sessions-stats')
        self.assertEqual(result[8].tags['tcp_session_state'], 'ESTABLISHED')
        self.assertEqual(result[8].value, 7)
        self.assertEqual(result[9].name, 'tcp-sessions-stats')
        self.assertEqual(result[9].tags['tcp_session_state'], 'SYN_SENT')
        self.assertEqual(result[9].value, 7)
        self.assertEqual(result[15].name, 'tcp-sessions-stats')
        self.assertEqual(result[15].tags['tcp_session_state'], 'CLOSE_WAIT')
        self.assertEqual(result[15].value, 1)

if __name__ == '__main__':
    unittest.main()