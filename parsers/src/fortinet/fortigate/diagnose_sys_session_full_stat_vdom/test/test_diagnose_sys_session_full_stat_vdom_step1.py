import os
import unittest

from fortinet.fortigate.diagnose_sys_session_full_stat_vdom.diagnose_sys_session_full_stat_vdom_1 import DiagnoseSysSessionFullStat1
from parser_service.public.action import *

class TestDiagnoseSysSessionFullStatVdom1(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = DiagnoseSysSessionFullStat1()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_1vdom(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/1vdom.input', {}, {})
        # Assert
        self.assertEqual(2, len(result))
        self.assertEqual(result[0].key, 'vdom_name')
        self.assertEqual(result[0].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[0].value, 'root')
        self.assertEqual(result[1].key, 'vdom_name')
        self.assertEqual(result[1].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[1].value, 'test1')

if __name__ == '__main__':
    unittest.main()