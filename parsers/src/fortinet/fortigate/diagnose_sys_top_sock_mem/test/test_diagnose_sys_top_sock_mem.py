import os
import unittest

from fortinet.fortigate.diagnose_sys_top_sock_mem.diagnose_sys_top_sock_mem import DiagnoseSysTopSockMem
from parser_service.public.action import *

class TestDiagnoseSysTopSockMem(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = DiagnoseSysTopSockMem()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_top5_process_sock_memory_FG_7_0_5(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/top5_process_sock_memory_FG_7_0_5.input', {}, {})
        # Assert
        self.assertEqual(5, len(result))
        self.assertEqual(result[2].name, 'top-sockmem')
        self.assertEqual(result[2].tags['id'], '3')
        self.assertEqual(result[2].value['value'], 'lldptx3 (172): 293kB')


    def test_no_process(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/no_process.input', {}, {})
        # Assert
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].name, 'top-sockmem')
        self.assertEqual(result[0].tags['id'], '1')
        self.assertEqual(result[0].value['value'], 'No processes using 1kB or more of socket memory.')

if __name__ == '__main__':
    unittest.main()