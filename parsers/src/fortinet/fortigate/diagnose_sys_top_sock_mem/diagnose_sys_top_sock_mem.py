from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class DiagnoseSysTopSockMem(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        # Step 1 : Data Extraction
        if raw_data:
            position = 1
            for line in raw_data.splitlines():
                if not line.isspace():
                    tags = {}
                    tags['im.identity-tags'] = 'id'
                    tags['id'] = str(position)
                    self.write_complex_metric_string('top-sockmem', tags,line, True, 'Top 5 - Memory Used (by socket)')
                    position = position + 1
                    if position > 5:
                        break
        return self.output
