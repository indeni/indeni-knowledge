# Parse connection status (0 | 1)
#Connection status: Down
/^Connection status: /{

    # Get the last word ('Down' in the example)
    connection_status = tolower($NF)
    is_connected = 0

    # Compare the connection_status set 1 if is 'connected' or 'up'
    if(connection_status == "connected" || connection_status == "up") {
        is_connected = 1
    }

    # Publish in category "FortiManager"
    tags["name"] = "FortiManager Connection Status"
    writeDoubleMetric("trust-connection-state", tags, "gauge", is_connected, "true", "FortiManager", "state", "name")  # Converted to new syntax by change_ind_scripts.py script

}

# Parse registration status (just copy text)
#Registration status: Unknown
/^Registration status:/{

    # Get the last word ('Unknown' in the example)
    registration_status = trim($NF)

    # Publish in "Default/Overview" category
    writeComplexMetricString("fortios-fortimanager-register-status", null, registration_status, "true", "FortiManager Registration Status")  # Converted to new syntax by change_ind_scripts.py script
}