from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class DiagnoseHardSysConserve(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        # Step 1 : Data Extraction
        if raw_data:
            data = helper_methods.parse_data_as_list(raw_data, 'diagnose_hard_sys_conserve.textfsm')
            if data:
                tags = {}
                self.write_double_metric('memory-conserve-status', tags, 'gauge', 1 if data[0]['conserve_mode_status'] == 'off' else 0, True, 'Memory Conserve Mode Status', 'state')
                tags['im.identity-tags'] = 'threshold_type'
                tags['threshold_type'] = 'extreme'
                self.write_complex_metric_string('memory-conserve-extreme', tags, data[0]['conserve_mode_ram_treshold_extreme'], True, 'Memory Conserve Mode - Thresholds')
                tags['threshold_type'] = 'red'
                self.write_complex_metric_string('memory-conserve-red', tags, data[0]['conserve_mode_ram_treshold_red'], True, 'Memory Conserve Mode - Thresholds')
                tags['threshold_type'] = 'green'
                self.write_complex_metric_string('memory-conserve-green', tags, data[0]['conserve_mode_ram_treshold_green'], True, 'Memory Conserve Mode - Thresholds')
        return self.output
