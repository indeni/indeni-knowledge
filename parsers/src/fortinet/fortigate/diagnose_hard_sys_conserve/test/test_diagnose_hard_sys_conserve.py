import os
import unittest

from fortinet.fortigate.diagnose_hard_sys_conserve.diagnose_hard_sys_conserve import DiagnoseHardSysConserve
from parser_service.public.action import *

class TestDiagnoseHardSysConserve(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = DiagnoseHardSysConserve()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_conserve_mode_off_FG_7_2(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/conserve_mode_off_FG_7_2.input', {}, {})
        # Assert
        self.assertEqual(4, len(result))
        self.assertEqual(result[0].name, 'memory-conserve-status')
        self.assertEqual(result[0].value, 1)
        self.assertEqual(result[1].name, 'memory-conserve-extreme')
        self.assertEqual(result[1].value['value'], '1810 MB   97% of total RAM')
        self.assertEqual(result[2].name, 'memory-conserve-red')
        self.assertEqual(result[2].value['value'], '1717 MB   92% of total RAM')
        self.assertEqual(result[3].name, 'memory-conserve-green')
        self.assertEqual(result[3].value['value'], '1586 MB   85% of total RAM')

    def test_conserve_mode_on_FG_7_2(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/conserve_mode_on_FG_7_2.input', {}, {})
        # Assert
        self.assertEqual(4, len(result))
        self.assertEqual(result[0].name, 'memory-conserve-status')
        self.assertEqual(result[0].value, 0)
        self.assertEqual(result[1].name, 'memory-conserve-extreme')
        self.assertEqual(result[1].value['value'], '1810 MB   97% of total RAM')
        self.assertEqual(result[2].name, 'memory-conserve-red')
        self.assertEqual(result[2].value['value'], '1325 MB   71% of total RAM')
        self.assertEqual(result[3].name, 'memory-conserve-green')
        self.assertEqual(result[3].value['value'], '1306 MB   70% of total RAM')

if __name__ == '__main__':
    unittest.main()