#-----------------------------------------------------------------------
# Helper function.
# Convert date in format '2016-11-16' , '15:45:00' to epoch seconds
# Special cases:
#   In case of 'n/a' return 'n/a'
#   In case of invalid format return ''
#-----------------------------------------------------------------------
function getInEpoch(dateString, timeString) {

    # Handle a special case of 'n/a' input
    if (trim(timeString) == "n/a")
        return "n/a"

    # Read date in format '2016-11-16'
    length1 = split(dateString, date_array, "-")

    # Read time in format '15:45:00'
    length2 = split(timeString, time_array, ":")

    # Check size of the arrays (if the length not 3 then return an empty string, failed to parsed input values)
    if (length1 != 3 || length2 != 3)
        return ""

    epoch_time = datetime(date_array[1], date_array[2], date_array[3], time_array[1], time_array[2], time_array[3])
    return epoch_time
}

#-----------------------------------------------------------------------
# Helper function.
# Publish date metric, if the date has a valid positive value.
#-----------------------------------------------------------------------
function publishDateMetricIfExist(metricName, tagName, metricCategory, dateToPublish) {
    # Check first if metric has a value and the value is number
    if (dateToPublish > 0 && dateToPublish != "n/a" && dateToPublish != "") {
        tags["name"] = tagName
        writeDoubleMetric(metricName, tags, "gauge", dateToPublish, "true", metricCategory, "date", "name")  # Converted to new syntax by change_ind_scripts.py script
    }
}


#-----------------------------------------------------------------------
# Helper function.
# Retrieve "updated-date" (third column) from a line like:
#[AV Engine           5.247  2017-05-04 14:26:00  manual    2018-01-01 17:00:00]
# After removing the first column 'stringRemovePart'.
# In the above example the stringRemovePart would be 'AV Engine'
#-----------------------------------------------------------------------
function getLastUpdateEpoch(stringLine, stringRemovePart) {

    #AV Engine           5.247  2017-05-04 14:26:00  manual    2018-01-01 17:00:00
    # Remove the part ('AV Engine') and trim the sentence
    stringLineWithDate = trim( substr(stringLine, length(stringRemovePart)+1) )

    # Replace the 2 spaces or more with a single space
    gsub(/[ ]{2,}/, " ", stringLineWithDate)

    # Split the line in an array using as separator the single ' '
    split(stringLineWithDate, stringLineWithDateArray, /\s/)

    # Use the parts 2 and 3 of the array. part-2 is '2017-05-04' and part-3 is '14:26:00'
    return getInEpoch(stringLineWithDateArray[2], stringLineWithDateArray[3])
}

#AV Engine           5.247  2017-05-04 14:26:00  manual    2018-01-01 17:00:00
/^AV Engine / {

    # Generate in epoch the expiry-time (last-column) the 2016-11-16 & 15:45:00 (Expiration Date of AV Engine)
    date_expire_epoch = getInEpoch($(NF-1), $NF)
    publishDateMetricIfExist("license-expiration", "AntiVirus Engine", "Licenses Expire date", date_expire_epoch)

    # Generate in epoch the update-time (third-column) 2016-11-16 & 15:45:00 (Update Date of AV Engine)
    date_update_epoch = getLastUpdateEpoch($0, "AV Engine")
    publishDateMetricIfExist("package-last-update", "Last Update of the AntiVirus Engine", "FortiGuard Services Last Update", date_update_epoch)

}

#Virus Definitions   52.696  2017-10-31 03:31:17  manual    2018-01-01 17:00:00
/^Virus Definitions / {

    # Generate in epoch the expiry-time (last-column) the 2016-11-16 & 15:45:00 (Expiration Date of Virus Definitions)
    date_expire_epoch = getInEpoch($(NF-1), $NF)
    publishDateMetricIfExist("license-expiration", "AntiVirus Definitions", "Licenses Expire date", date_expire_epoch)

    # Generate in epoch the update-time (third-column) 2016-11-16 & 15:45:00 (Update Date of Virus Definitions)
    date_update_epoch = getLastUpdateEpoch($0, "Virus Definitions")
    publishDateMetricIfExist("package-last-update", "Last Update of the AntiVirus Definitions", "FortiGuard Services Last Update", date_update_epoch)

}

#Extended set        1.000  2018-04-09 18:07:00  manual    2019-02-01 17:00:00 
/^Extended set / {
    date_expire_epoch = getInEpoch($(NF-1), $NF)
    publishDateMetricIfExist("license-expiration", "AntiVirus Extended Definitions", "Licenses Expire date", date_expire_epoch)

    # Generate in epoch the update-time (third-column) 2016-11-16 & 15:45:00 (Update Date of Virus Definitions)
    date_update_epoch = getLastUpdateEpoch($0, "Virus Definitions")
    publishDateMetricIfExist("package-last-update", "Last Update of the AntiVirus Extended Definitions", "FortiGuard Services Last Update", date_update_epoch)

}

#Flow-based Virus Definitions  52.696  2017-10-31 03:31:17  manual    2018-01-01 17:00:00
/^Flow-based Virus Definitions / {

    # Generate in epoch the expiry-time (last-column) the 2016-11-16 & 15:45:00 (Expiration Date of Flow-based Virus Definitions)
    date_expire_epoch = getInEpoch($(NF-1), $NF)
    publishDateMetricIfExist("license-expiration", "Flow-based AntiVirus Definitions", "Licenses Expire date", date_expire_epoch)

}

#Attack Definitions  12.255  2017-10-31 03:31:17  manual    2018-01-01 17:00:00
/^Attack Definitions / {

    # Generate in epoch the expiry-time (last-column) the 2016-11-16 & 15:45:00 (Expiration Date of Attack Definitions)
    date_expire_epoch = getInEpoch($(NF-1), $NF)
    publishDateMetricIfExist("license-expiration", "IPS Attack Definitions", "Licenses Expire date", date_expire_epoch)

    # Generate in epoch the update-time (third-column) 2016-11-16 & 15:45:00 (Update Date of Attack Definitions)
    date_update_epoch = getLastUpdateEpoch($0, "Attack Definitions")
    publishDateMetricIfExist("package-last-update", "Last Update of the IPS Attack Definitions", "FortiGuard Services Last Update", date_update_epoch)

}

#IPS Malicious URL Database  1.001  2015-01-01 01:01:00  manual    n/a
/^IPS Malicious URL Database / {

    # Generate in epoch the expiry-time (last-column) the 2016-11-16 & 15:45:00 (Expiration Date of IPS Malicious URL Database)
    date_expire_epoch = getInEpoch($(NF-1), $NF)
    publishDateMetricIfExist("license-expiration", "Expiration Date of IPS Malicious URL Database", "Licenses Expire date", date_expire_epoch)


}

#Botnet Definitions  1.000  2012-05-28 22:51:00  manual    n/a
/^Botnet Definitions / {

    # Generate in epoch the expiry-time (last-column) the 2016-11-16 & 15:45:00 (Expiration Date of Botnet Definitions)
    date_expire_epoch = getInEpoch($(NF-1), $NF)
    publishDateMetricIfExist("license-expiration", "Botnet Definitions", "Licenses Expire date", date_expire_epoch)

}

#IPS/FlowAV Engine   3.410  2017-03-24 16:25:00  manual    n/a
/^IPS/ && /FlowAV Engine / {

    # Generate in epoch the expiry-time (last-column) the 2016-11-16 & 15:45:00 (Expiration Date of IPS/FlowAV Engine)
    date_expire_epoch = getInEpoch($(NF-1), $NF)
    publishDateMetricIfExist("license-expiration", "IPS/FlowAV Engine", "Licenses Expire date", date_expire_epoch)

    # Generate in epoch the update-time (third-column) 2016-11-16 & 15:45:00 (Update Date of IPS/FlowAV Engine)
    date_update_epoch = getLastUpdateEpoch($0, "IPS/FlowAV Engine")
    publishDateMetricIfExist("package-last-update", "Last Update of the IPS/Flow AntiVirus Engine", "FortiGuard Services Last Update", date_update_epoch)

}

#Application Definitions  6.741  2015-12-01 02:30:00  manual    n/a
/^Application Definitions / {

    # Generate in epoch the expiry-time (last-column) the 2016-11-16 & 15:45:00 (Expiration Date of Application Definitions)
    date_expire_epoch = getInEpoch($(NF-1), $NF)
    publishDateMetricIfExist("license-expiration", "Application Definitions", "Licenses Expire date", date_expire_epoch)

    # Generate in epoch the update-time (third-column) 2016-11-16 & 15:45:00 (Update Date of Application Definitions)
    date_update_epoch = getLastUpdateEpoch($0, "Application Definitions")
    publishDateMetricIfExist("package-last-update", "Last Update of the Application Definitions", "FortiGuard Services Last Update", date_update_epoch)

}

#Industrial Attack Definitions  6.741  2015-12-01 02:30:00  manual    n/a
/^Industrial Attack Definitions / {

    # Generate in epoch the expiry-time (last-column) the 2016-11-16 & 15:45:00 (Expiration Date of Industrial Attack Definitions )
    date_expire_epoch = getInEpoch($(NF-1), $NF)
    publishDateMetricIfExist("license-expiration", "Industrial Attack Definitions", "Licenses Expire date", date_expire_epoch)

}
