from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class GetIpsGlobal(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        # Step 1 : Data Extraction
        if raw_data:
            data = helper_methods.parse_data_as_json(raw_data)
            tags = {}
            if data:
                for vdom_data in data:
                    tags['vdom.name'] = vdom_data['vdom']
                    tags['im.identity-tags'] = 'vdom.name'
                    self.write_double_metric('ips-db-extended-status', tags, 'gauge', 0 if vdom_data['results']['database'] == 'extended' else 1, True, 'IPS - Extended DB status', 'state')
                    self.write_double_metric('ips-fail-open', tags, 'gauge', 0 if vdom_data['results']['fail-open'] == 'extended' else 1, True, 'IPS - Fail open status', 'state')
                    self.write_complex_metric_string('ips-anomaly-mode', tags, str(vdom_data['results']['anomaly-mode']) , True, 'IPS - Anomaly Mode')
                    self.write_complex_metric_string('ips-socket-size', tags, str(vdom_data['results']['socket-size']) , True, 'IPS - Socket size')
        return self.output
