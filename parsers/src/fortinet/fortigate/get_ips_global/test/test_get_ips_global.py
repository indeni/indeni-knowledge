import os
import unittest

from fortinet.fortigate.get_ips_global.get_ips_global import GetIpsGlobal
from parser_service.public.action import *

class TestGetIpsGlobal(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = GetIpsGlobal()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_anomaly_mode_continuous_FortiOS_7_1(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/anomaly_mode_continuous_FortiOS_7_1.input', {}, {})
        # Assert
        self.assertEqual(4, len(result))
        self.assertEqual(result[0].name, 'ips-db-extended-status')
        self.assertEqual(result[0].value, 1)
        self.assertEqual(result[0].tags['vdom.name'], 'root')
        self.assertEqual(result[0].tags['im.dstype.displayType'], 'state')
        self.assertEqual(result[0].tags['display-name'], 'IPS - Extended DB status')
        self.assertEqual(result[1].name, 'ips-fail-open')
        self.assertEqual(result[1].value, 1)
        self.assertEqual(result[1].tags['vdom.name'], 'root')
        self.assertEqual(result[1].tags['im.dstype.displayType'], 'state')
        self.assertEqual(result[1].tags['display-name'], 'IPS - Fail open status')
        self.assertEqual(result[2].name, 'ips-anomaly-mode')
        self.assertEqual(result[2].value['value'], 'continuous')
        self.assertEqual(result[2].tags['vdom.name'], 'root')
        self.assertEqual(result[2].tags['display-name'], 'IPS - Anomaly Mode')
        self.assertEqual(result[3].name, 'ips-socket-size')
        self.assertEqual(result[3].value['value'], '255')
        self.assertEqual(result[3].tags['vdom.name'], 'root')
        self.assertEqual(result[3].tags['display-name'], 'IPS - Socket size')

    def test_db_extended_FortiOS_7_1(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/db_extended_FortiOS_7_1.input', {}, {})
        # Assert
        self.assertEqual(4, len(result))
        self.assertEqual(result[0].name, 'ips-db-extended-status')
        self.assertEqual(result[0].value, 0)
        self.assertEqual(result[0].tags['vdom.name'], 'root')
        self.assertEqual(result[0].tags['im.dstype.displayType'], 'state')
        self.assertEqual(result[0].tags['display-name'], 'IPS - Extended DB status')
        self.assertEqual(result[1].name, 'ips-fail-open')
        self.assertEqual(result[1].value, 1)
        self.assertEqual(result[1].tags['vdom.name'], 'root')
        self.assertEqual(result[1].tags['im.dstype.displayType'], 'state')
        self.assertEqual(result[1].tags['display-name'], 'IPS - Fail open status')
        self.assertEqual(result[2].name, 'ips-anomaly-mode')
        self.assertEqual(result[2].value['value'], 'periodical')
        self.assertEqual(result[2].tags['vdom.name'], 'root')
        self.assertEqual(result[2].tags['display-name'], 'IPS - Anomaly Mode')
        self.assertEqual(result[3].name, 'ips-socket-size')
        self.assertEqual(result[3].value['value'], '255')
        self.assertEqual(result[3].tags['vdom.name'], 'root')
        self.assertEqual(result[3].tags['display-name'], 'IPS - Socket size')

    def test_db_regular_FortiOS_7_1(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/db_regular_FortiOS_7_1.input', {}, {})
        # Assert
        self.assertEqual(4, len(result))
        self.assertEqual(result[0].name, 'ips-db-extended-status')
        self.assertEqual(result[0].value, 1)
        self.assertEqual(result[0].tags['vdom.name'], 'root')
        self.assertEqual(result[0].tags['im.dstype.displayType'], 'state')
        self.assertEqual(result[0].tags['display-name'], 'IPS - Extended DB status')
        self.assertEqual(result[1].name, 'ips-fail-open')
        self.assertEqual(result[1].value, 1)
        self.assertEqual(result[1].tags['vdom.name'], 'root')
        self.assertEqual(result[1].tags['im.dstype.displayType'], 'state')
        self.assertEqual(result[1].tags['display-name'], 'IPS - Fail open status')
        self.assertEqual(result[2].name, 'ips-anomaly-mode')
        self.assertEqual(result[2].value['value'], 'periodical')
        self.assertEqual(result[2].tags['vdom.name'], 'root')
        self.assertEqual(result[2].tags['display-name'], 'IPS - Anomaly Mode')
        self.assertEqual(result[3].name, 'ips-socket-size')
        self.assertEqual(result[3].value['value'], '255')
        self.assertEqual(result[3].tags['vdom.name'], 'root')
        self.assertEqual(result[3].tags['display-name'], 'IPS - Socket size')

if __name__ == '__main__':
    unittest.main()