BEGIN {
    # NOTE: The "fnsysctl" command is not supported on Fortigate v5.4 "FortiGate-VM" models.

    critical_processes["authd"] = "Handles firewall authentication functionality"
    critical_processes["bgpd"] = "Supports firewall configuration for the Border Gateway Protocol (BGP) internet routing protocol"
    critical_processes["cmdbsvr"] = "Configuration Management DataBase SerVeR -- maintains and writes device configuration information"
    critical_processes["dnsproxy"] = "Manages custom Domain Name System rules on the firewall"
    critical_processes["fgfmd"] = "Handles interactions between the firewall and FortiManager"
    critical_processes["fnbamd"] = "The Fortinet non-blocking authentication daemon, supporting authentication functionality"
    critical_processes["foauthd"] = "Supports Fortinet authentication procedures"
    critical_processes["forticron"] = "A new feature added to dump userspace process stacks"
    critical_processes["httpclid"] = "HTTP client for making HTTP requests"
    critical_processes["httpsd"] = "HTTPS server for serving Fortigate's web GUI"
    critical_processes["ipshelper"] = "Part of the Intrusion Prevention System (IPS), the main system that tries to prevent cyber attacks"
    critical_processes["ipsmonitor"] = "Part of the Intrusion Prevention System (IPS), the main system that tries to prevent cyber attacks"
    critical_processes["isisd"] = "Handles the Intermediate System to Intermediate System (IS-IS) routing protocol"
    critical_processes["miglogd"] = "Generates logs for the firewall"
    critical_processes["ntpd"] = "Support for the Network Time Protocol, a protocol that maintains the system date-time of the device"
    critical_processes["ospf6d"] = "Support for the Open Shortest Path First (OSPFv3) routing protocol for IPv6 networks"
    critical_processes["ospfd"] = "Support for the Open Shortest Path First (OSPFv3) routing protocol for IPv4 networks"
    critical_processes["reportd"] = "A Process for handling reporting"
    critical_processes["updated"] = "FortiGuard update processs"
    # Initialize the process_state array -- all critical processes start as "not found"
    for (key in critical_processes) {
        process_state[key] = "0.0"
    }
}

#PID       UID     GID     STATE   CMD
#137       0       0       S       /bin/dhcpcd
#18        0       0       S       [khubd]
/^[0-9]/{
    state = $4
    process_full = $5
    process_name = process_full    # process_name should always have a value

    # parse out the short process name -- look at the test cases to see all the variations.
    # The order of these checks is important
    rbrack_index = index(process_full, "]")
    if (rbrack_index > 0) {
        slash_index = index(process_full, "/")
        if (slash_index > 0) {
            process_name = substr(process_full, 2, (slash_index - 2))
        } else {
            process_name = substr(process_full, 2, (rbrack_index - 2))
        }
    } else if (index(process_full, "/") > 0) {
        slash_arr_len = split(process_full, slash_arr, "/")
        process_name = slash_arr[slash_arr_len]
    }

    # R  running or runnable (on run queue)
    # S  interruptible sleep (waiting for an event to complete)
    # see:  https://idea.popcount.org/2012-12-11-linux-process-states/
    if (process_state[process_name] == "0.0") {  # note unwelcome side effect: if process_name doesn't exist, creates an entry for it in the array, set to ""
        if (state == "S" || state == "R") {
            process_state[process_name] = "1.0"
        }
    }
}
END {
    for (process in process_state) {
        # Executing "if (process_state[process_name] == "0.0")" above creates empty entries in process_state -- this is
        # an unwelcome side-effect; so, here we need to skip those potential empty entries
        if (process_state[process] == "0.0" || process_state[process] == "1.0") {
            process_tags["process-name"] = process
            process_tags["description"] = critical_processes[process]
            writeDoubleMetric("process-state", process_tags, "gauge", process_state[process], "false")  # Converted to new syntax by change_ind_scripts.py script
        }
    }
}