BEGIN{
    # Store mask in
    table_routing_index = 0
}

# Parse all the needed info ('network', 'mask' & 'next-hop') and store them in the table
#S*      0.0.0.0/0 [10/0] via 212.205.216.193, wan1
#S       10.0.0.0/8 [10/0] via 10.10.8.145, lan
#S       1.1.1.1/32 [10/0] is directly connected, port1
/^S/{

    # Ensure that the $5 is ip and not text
    if($5 ~ /[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}/){

        # Increase index of table
        table_routing_index++

        # Reading network and mask. Example is "10.0.0.0/8"
        ip_mask = $2
        split(ip_mask, ip_mask_array, "/")
        table_routing[table_routing_index, "network"] = ip_mask_array[1]
        table_routing[table_routing_index, "mask"] = ip_mask_array[2]

        # Storing next-hop
        next_hop = $5

        # Removing ','
        gsub(",", "", next_hop)
        table_routing[table_routing_index, "next-hop"] = next_hop

    }
}

END {
    writeComplexMetricObjectArray("static-routing-table", null, table_routing, "false")  # Converted to new syntax by change_ind_scripts.py script
}