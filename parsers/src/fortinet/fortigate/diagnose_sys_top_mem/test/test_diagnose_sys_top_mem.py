import os
import unittest

from fortinet.fortigate.diagnose_sys_top_mem.diagnose_sys_top_mem import DiagnoseSysTopMem
from parser_service.public.action import *

class TestDiagnoseSysTopMem(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = DiagnoseSysTopMem()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_top5_process_memory_FG_7_0_5(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/top5_process_memory_FG_7_0_5.input', {}, {})
        # Assert
        self.assertEqual(5, len(result))
        self.assertEqual(result[2].name, 'top-mem')
        self.assertEqual(result[2].tags['id'], '3')
        self.assertEqual(result[2].value['value'], 'cmdbsvr (145): 27618kB')

if __name__ == '__main__':
    unittest.main()