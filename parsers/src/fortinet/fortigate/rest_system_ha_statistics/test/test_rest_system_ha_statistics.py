import os
import unittest

from fortinet.fortigate.rest_system_ha_statistics.rest_system_ha_statistics import RestSystemHaStatistics
from parser_service.public.action import *

class TestRestSystemHaStatistics(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = RestSystemHaStatistics()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_primary_v6_4_8(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/primary_v6_4_8.input', {}, {})
        # Assert
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].key, 'cluster-id')
        self.assertEqual(result[0].value, 'FGVM04TM19000333_FGVM04TM19000334')

    def test_secondary_v6_4_8(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/secondary_v6_4_8.input', {}, {})
        # Assert
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].key, 'cluster-id')
        self.assertEqual(result[0].value, 'FGVM04TM19000333_FGVM04TM19000334')

if __name__ == '__main__':
    unittest.main()