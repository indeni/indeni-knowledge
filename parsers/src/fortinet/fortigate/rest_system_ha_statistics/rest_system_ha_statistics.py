from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class RestSystemHaStatistics(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        # Step 1 : Data Extraction
        if raw_data:
            data = helper_methods.parse_data_as_json(raw_data)
            cluster_id =('_').join(sorted([x['serial_no'] for x in data['results']]))
            self.write_tag('cluster-id', cluster_id)
        return self.output
