from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class RestCmdbFirewallDosPolicy(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        # Step 1 : Data Extraction
        if raw_data:
            data = helper_methods.parse_data_as_json(raw_data)
            tags = {}
            if data:
                for vdom_data in data:
                    tags['vdom.name'] = vdom_data['vdom']
                    tags['im.identity-tags'] = 'vdom.name'
                    self.write_double_metric('dos-ipv4-policy-status', tags, 'gauge', 0 if len(vdom_data['results']) == 0 else 1, True, 'Policy - DoS -Status', 'state')
        return self.output
