import os
import unittest

from fortinet.fortigate.rest_cmdb_firewall_dos_policy.rest_cmdb_firewall_dos_policy import RestCmdbFirewallDosPolicy
from parser_service.public.action import *

class TestRestCmdbFirewallDosPolicy(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = RestCmdbFirewallDosPolicy()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_enabled_on_root_not_enabled_1_vdom(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/enabled_on_root_not_enabled_1_vdom.input', {}, {})
        # Assert
        self.assertEqual(2, len(result))
        self.assertEqual(result[0].name, 'dos-ipv4-policy-status')
        self.assertEqual(result[0].value, 1)
        self.assertEqual(result[0].tags['vdom.name'], 'root')
        self.assertEqual(result[0].tags['im.dstype.displayType'], 'state')
        self.assertEqual(result[0].tags['display-name'], 'Policy - DoS -Status')
        self.assertEqual(result[1].name, 'dos-ipv4-policy-status')
        self.assertEqual(result[1].value, 0)
        self.assertEqual(result[1].tags['vdom.name'], 'test1')
        self.assertEqual(result[1].tags['im.dstype.displayType'], 'state')
        self.assertEqual(result[1].tags['display-name'], 'Policy - DoS -Status')

    def test_enabled_on_root(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/enabled_on_root.input', {}, {})
        # Assert
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].name, 'dos-ipv4-policy-status')
        self.assertEqual(result[0].value, 1)
        self.assertEqual(result[0].tags['vdom.name'], 'root')
        self.assertEqual(result[0].tags['im.dstype.displayType'], 'state')
        self.assertEqual(result[0].tags['display-name'], 'Policy - DoS -Status')

    def test_disabled_on_root(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/disabled_on_root.input', {}, {})
        # Assert
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].name, 'dos-ipv4-policy-status')
        self.assertEqual(result[0].value, 0)
        self.assertEqual(result[0].tags['vdom.name'], 'root')
        self.assertEqual(result[0].tags['im.dstype.displayType'], 'state')
        self.assertEqual(result[0].tags['display-name'], 'Policy - DoS -Status')

if __name__ == '__main__':
    unittest.main()