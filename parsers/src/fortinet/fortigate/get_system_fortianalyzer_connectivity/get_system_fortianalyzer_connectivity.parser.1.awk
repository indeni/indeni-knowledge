#Status: connected
#Status: disable
/^Status: /{

    # Get the last word ('disable' or 'connected' in the example)
    connection_status = tolower($NF)
    is_connected = 0

    # Compare the connection_status set 1 if is 'connected' or 'up'
    if(connection_status == "connected" || connection_status == "up") {
        is_connected = 1
    }
}

# Parse disk usage.
# Note that in case of 'disabled'/(not connected) there is no disk usage line
#Disk Usage: 0%
/^Disk Usage:/{

    # The last word is the percentage. Removing the '%' character
    disk_percentage = $NF
    gsub("%", "", disk_percentage)

    disk_percentage = trim(disk_percentage)
}

END {
    # Publishing metrics in "FortiAnalyzer" category
    tags["name"] = "Connection to Analyzer"
    writeDoubleMetric("fortios-analyzer-is-connected", tags, "gauge", is_connected, "true", "FortiAnalyzer", "state", "name")  # Converted to new syntax by change_ind_scripts.py script

    tags["name"] = "Analyzer Disk usage"
    writeDoubleMetric("fortios-analyzer-disk-usage", tags, "gauge", disk_percentage, "true", "FortiAnalyzer", "percentage", "name")  # Converted to new syntax by change_ind_scripts.py script
}