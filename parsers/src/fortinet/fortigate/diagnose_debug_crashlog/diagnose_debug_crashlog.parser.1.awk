BEGIN {
    count_line = 0
}

#Count lines that are not contain:
#1. 'is brought up.'
#2. 'status=0x100'
#3. 'status=0x0'
#4. 'Crash log interval is 0 seconds''
!/is brought up\./ && !/status=0x100/ && !/status=0x0/ && !/^Crash log interval is /{
    count_line = count_line + 1
}

END {
   tags["name"] = "Number of crtical crashlog entries"
   writeDoubleMetric("crashlog-critical-status", tags, "gauge", count_line, "true", "Crashlog", "number", "name")  # Converted to new syntax by change_ind_scripts.py script
}