function writeCpuUsageMetric(id, cpuIdleAmount, cpuIsAverage) {
    sub(/%/, "", cpuIdleAmount)

    tags_cpu["cpu-id"] = id
    tags_cpu["cpu-is-avg"] = cpuIsAverage
    tags_cpu["resource-metric"] = "true"
    writeDoubleMetric("cpu-usage", tags_cpu, "gauge", 100 - cpuIdleAmount, "true", "CPU Usage", "percentage", "cpu-id")  # Converted to new syntax by change_ind_scripts.py script
}

# v5.4
#Memory states: 66% used
/^Memory states:/ {
    memory_usage = substr($3, 1, 2)

    # the following "RAM" tag value does NOT surface in the UI. It's here just to satisfy the
    # requirements of the rule -- for some reason, we need to have this tag _with_ a value for things
    # to function properly.

    tags_memory["name"] = "RAM"
    tags_memory["resource-metric"] = "true"
    writeDoubleMetric("memory-usage", tags_memory, "gauge", memory_usage, "true", "Memory Usage", "percentage", "")  # Converted to new syntax by change_ind_scripts.py script
}

# v5.6
#Memory: 1019996k total, 354312k used (34%), 665684k free (66%), 1616k buffers
/^Memory:/ {
    percent_memory_usage = substr($6, 2, 2)
    free = substr($7, 1, length($7) - 1)
    total = substr($2, 1, length($2) - 1)
    used = substr($4, 1, length($4) - 1)

    tags_memory["name"] = "Memory: Free"
    writeDoubleMetric("memory-free-kbytes", tags_memory, "gauge", free, "true", "Memory Usage", "kilobytes", "name")  # Converted to new syntax by change_ind_scripts.py script

    tags_memory["name"] = "Memory: Total"
    writeDoubleMetric("memory-total-kbytes", tags_memory, "gauge", total, "true", "Memory Usage", "kilobytes", "name")  # Converted to new syntax by change_ind_scripts.py script

    tags_memory["name"] = "Memory: Used"
    writeDoubleMetric("memory-used-kbytes", tags_memory, "gauge", used, "true", "Memory Usage", "kilobytes", "name")  # Converted to new syntax by change_ind_scripts.py script

    tags_memory["name"] = "Memory Usage"
    tags_memory["resource-metric"] = "true"
    writeDoubleMetric("memory-usage", tags_memory, "gauge", percent_memory_usage, "true", "Memory Usage", "percentage", "name")  # Converted to new syntax by change_ind_scripts.py script
}

# This section handles the "per core" metrics for CPU usage
# v5.4
#CPU0 states: 2% user 4% system 0% nice 94% idle
# v5.6
#CPU1 states: 6% user 8% system 0% nice 86% idle 0% iowait 0% irq 0% softirq
/^CPU[0-9]+ states:/ {
    writeCpuUsageMetric("Per Core - " $1, $9, "false")
}

# "CPU states:" shows the average CPU usage across all CPU cores
# v5.4
#CPU states: 8% user 10% system 0% nice 82% idle
# v5.6
#CPU states: 4% user 6% system 0% nice 90% idle 0% iowait 0% irq 0% softirq
/^CPU states:/ {
    writeCpuUsageMetric("Average - CPU", $9, "true")
}

#Uptime: 3 days,  6 hours,  10 minutes
/^Uptime:/ {
    days = $2
    hours = $4
    minutes = $6
    uptime_in_seconds = days * 86400 + hours * 3600 + minutes * 60
    # Display in Overview - Live Config the uptime (in seconds)
    writeDoubleMetric("uptime-milliseconds", null, "gauge", (uptime_in_seconds*1000), "true", "Device Uptime", "duration", "")  # Converted to new syntax by change_ind_scripts.py script
}