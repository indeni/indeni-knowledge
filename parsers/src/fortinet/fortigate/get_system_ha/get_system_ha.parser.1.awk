#uninterruptible-upgrade: enable
#uninterruptible-upgrade: disable
/uninterruptible-upgrade:/ {
  upgrade = $2
   if ( upgrade != "enable")  #We fire alert if different than enable
    writeDoubleMetric("fortinet-uninterruptible-disabled", null, "gauge", 0, "false")
   else
    writeDoubleMetric("fortinet-uninterruptible-disabled", null, "gauge", 1, "false")

}


#Best Practice: We check if there are at least two interfaces with different priority
#hbdev               : "port2" 50 "port3" 40

/hbdev/ { 
        n = split($0, interfaces)
        interface_priority= -1
        hb_interfaces = 0
        hb_interfaces_metric = 1

        for (i = 3; i <= n;i=i+2) {
                if (interfaces[i+1]!= interface_priority) {
                        interface_priority = interfaces[i+1]
                        hb_interfaces = hb_interfaces + 1
                }

        }
        if (hb_interfaces < 2 && n>=6)      #We have two HB interfaces but different priority
                hb_interfaces_metric = 0

        writeDoubleMetric("fortinet-hb-interfaces-priorities", null, "gauge", hb_interfaces_metric, "false")
}