from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

#     "results":[
#       {
#         ....
#         "srcaddr":[
#           {
#             "q_origin_key":"all",
#             "name":"all"
#           }
#         ],
#         "dstaddr":[
#           {
#             "q_origin_key":"all",
#             "name":"all"
#           }
#         ],
#         .....
#         "service":[
#           {
#             "q_origin_key":"HTTP",
#             "name":"HTTP"
#           }
#         ],
#         "action":"deny",
#         "vlan-filter":""
#       }
#     ],
#     "vdom":"root",
#     ...
#   }


class ExplicitDenyPolicy(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:  # We check not empty
            data = helper_methods.parse_data_as_json(raw_data)
            for entry in data:  # we loop through VDOMs
                vdom_id = entry['vdom']
                results = entry['results']
                if len(results) == 0:  # no policies defined
                    explicit_deny_rule = 0
                else:
                    rules = results[-1]  # We want to check last policy only
                    srcaddr = rules['srcaddr'][0]['name']
                    dstaddr = rules['dstaddr'][0]['name']
                    action = rules['action']
                    service = rules['service'][0]['name']
                    if (srcaddr == 'all' and dstaddr == 'all'
                            and service == 'ALL'
                            and action == 'deny'):
                        explicit_deny_rule = 1  # There is an explicit deny
                    else:
                        explicit_deny_rule = 0
                tags = {'name': vdom_id}
                self.write_double_metric('fortigate-explicit-deny', tags, 'gauge', explicit_deny_rule, False)
        return self.output
