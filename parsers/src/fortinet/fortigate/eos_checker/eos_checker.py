from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser
import yaml
import pathlib
import calendar

class EosChecker(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        vendor = device_tags.get('vendor')
        model = device_tags.get('model')
        os_version = device_tags.get('os.version')
        current_path = pathlib.Path(__file__).parent.absolute()
        full_eos_file_path = '{}/{}.eos.yaml'.format(current_path, vendor)
        try:
            with open(full_eos_file_path, 'r') as eos_file:
                try:
                    data_loaded = yaml.safe_load(eos_file)
                    for model_name in data_loaded['hardware-eos-date'].keys():
                        if model.startswith(str(model_name)):
                            timetuplehw = (int(data_loaded['hardware-eos-date'][model][0]), int(data_loaded['hardware-eos-date'][model][1]), data_loaded['hardware-eos-date'][model][2], 0, 0, 0, 0, 0, 0)
                            secshw = calendar.timegm(timetuplehw)
                            self.write_double_metric('hardware-eos-date', {}, 'gauge', secshw, True, 'Hardware End of Support', 'date', None)

                    for version in data_loaded['software-eos-date'].keys():
                        if os_version.startswith(str(version)):
                            timetuplehw = (int(data_loaded['software-eos-date'][version][0]), int(data_loaded['software-eos-date'][version][1]), data_loaded['software-eos-date'][version][2], 0, 0, 0, 0, 0, 0)
                            secshw = calendar.timegm(timetuplehw)
                            self.write_double_metric('software-eos-date', {}, 'gauge', secshw, True, 'Software End of Support', 'date', None)
                except:
                    pass
        except:
            pass
        return self.output
