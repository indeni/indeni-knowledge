import os
import unittest
from fortinet.fortigate.eos_checker.eos_checker import EosChecker
from parser_service.public.action import *

class TestEosChecker(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = EosChecker()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_fortigate_100E_eos(self):
        tags = {
            'model': "FortiGate-100E",
            'serial': "FG100E4Q17030920",
            'vendor': "fortinet",
            'os.version': "4.1.2"
        }
        result  = self.parser.parse('',{},tags)
        if result:
            pass

if __name__ == '__main__':
    unittest.main()