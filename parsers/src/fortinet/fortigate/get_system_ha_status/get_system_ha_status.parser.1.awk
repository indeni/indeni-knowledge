# ------------------------------------------------------------------
# A utility function that returns the part of the string "string_read_from"
# that is between the parameter strings "string_start" and "string_start".
# If the "string_read_from" does not contain "string_start" it will return ""
# Example:
#   param1 = 'FEX: 112 Description: FEX2k - under N5kUP'
#   param2 = 'FEX: '
#   param3 = ' Description:'
#   result = '112'
# ------------------------------------------------------------------
function getStringBetween(string_read_from, string_start, string_end) {

    index_start = match(string_read_from, string_start)
    if(index_start == 0){
        return "";
    }
    index_start = index_start + length(string_start)

    if (string_end == "") {
        index_end = length(string_read_from) + 1
    } else {
        index_end = match(string_read_from, string_end)
    }

     if (index_start == 0 || index_end == 0 || index_end < index_start) {

            # The "string_start" OR "string_end" is not part of the "string_read_from".
            # Or invalid positions "string_start" is after "string_end"
            return ""

     } else {

        length_of_string = (index_end - index_start)
        return trim(substr(string_read_from, index_start, length_of_string))

    }
}

# ------------------------------------------------------------------
# A utility function that returns the first part of a string separated by '/'
# If there is no '/' it will return the complete string
# Example:
#   param1 = '283083317/474100/229/0'
#   result = 283083317
# ------------------------------------------------------------------
function getFirstPartOfBytes(str) {
    split(str, str_arr, "/")
    return str_arr[1]
}


BEGIN {
    # ----  HA Sync Variables   -----
    # Flag, when enabled (1) means that we process 'sync' lines
    table_sync_is_in = -1

    # if '1' means all lines of 'sync' table are ending with 'in-sync'
    table_sync_status = 1

    # ----- HBDev variables     -----
    # Flag, when enabled (1) means that we process 'hbdev' lines
    table_hbdev_is_in = -1

    # if '1' means all lines of 'hbdev' table are 'up'
    table_hbdev_status = 1

    table_hbdev_total_bytes = 0

    # the number of 'up' is found in hbdev lines
    table_hbdev_up_counter = 0

    # ----- MONDev variables    ------
    # Flag, when enabled (1) means that we process 'mondev' lines
    table_mondev_is_in = -1

    # if '1' means all lines of 'mondev' table are 'up'
    table_mondev_status = 1

    table_mondev_total_bytes = 0
}

#Option 1
#HA Health Status: OK
#Option 2
#HA Health Status:
#     WARNING: FG1HEFTK18000205 has mondev down;
/^HA Health Status/ {

    health_status = ($NF == "OK")
    health_status_warning = ""

    if (health_status == 0) {
        #read next line
        getline tmp
        health_status_warning = trim(tmp)
    }
}

#Mode: HA A-P
#Mode: Standalone
/^Mode:/{
    ha_mode = getStringBetween($0, ":")
    writeComplexMetricString("ha-health-mode", null, ha_mode, "true", "HA Health Mode")  # Converted to new syntax by change_ind_scripts.py script
}

#Debug: 0
/^Debug:/{
    ha_debug = $NF
    writeComplexMetricString("ha-debug-status", null, ha_debug, "true", "HA Debug")  # Converted to new syntax by change_ind_scripts.py script
}

#Group: 0
/^Group:/{
    ha_group = $NF
    writeComplexMetricString("ha-group-id", null, ha_group, "true", "HA Group")  # Converted to new syntax by change_ind_scripts.py script
}

#ses_pickup: enable, ses_pickup_delay=disable
#ses_pickup: disable
/^ses_pickup:/{
    ha_ses_pickup = getStringBetween($0, ":")
    writeComplexMetricString("ha-session-pickup", null, ha_ses_pickup, "true", "HA Session Pickup")  # Converted to new syntax by change_ind_scripts.py script
}

#override: vcluster1 enable, vcluster2 enable
#override: disable
/^override:/{
    ha_override = getStringBetween($0, ":")
    writeComplexMetricString("ha-session-override", null, ha_override, "true", "HA Session Override")  # Converted to new syntax by change_ind_scripts.py script
}

#Cluster Uptime: 17835 days 21:57:57
/^Cluster Uptime:/{
    ha_cluster_uptime = getStringBetween($0, ":")
    writeComplexMetricString("ha-cluster-uptime", null, ha_cluster_uptime, "true", "HA Cluster Uptime")  # Converted to new syntax by change_ind_scripts.py script
}


#Model: FortiGate-VM64
/^Model:/{
  model = $NF
  writeComplexMetricString("model", null, model, "true", "Model")
}

# Set flag we are in the "Configuration Status" lines
#Configuration Status:
/^Configuration Status:/{
    table_sync_is_in = 1;
    next
}

# Set flag we are exited the "Configuration Status" lines
(table_sync_is_in == 1) && !/^\s/ {
    table_sync_is_in = 0
}

# We are processing a line in the "Configuration Status" lines, examples:
#    FG-5KD3914800353(updated 4 seconds ago): in-sync
#    FG-5KD3914800284(updated 4 seconds ago): in-sync
(table_sync_is_in == 1) && /^\s/ {

    # if one line is not ending with 'in-sync', then we set the metric to '0'
    if ($NF != "in-sync") {
        table_sync_status = 0
    }

}

# Set flag we are in the "HBDEV stats" lines
#HBDEV stats:
#    FG-5KD3914800284(updated 4 seconds ago):
#        base1: physical/1000full, up, rx-bytes/packets/dropped/errors=311389146/636565/235/0, tx=310492903/575295/0/0
#        base2: physical/1000full, up, rx-bytes/packets/dropped/errors=283083317/474100/229/0, tx=249587946/410904/0/0
/^HBDEV stats:/{
    table_hbdev_is_in = 1
    next
}

# Set flag we are exited the "HBDEV stats" lines
(table_hbdev_is_in == 1) && !/^\s/ {
    table_hbdev_is_in = 0
}

# We are processing a line in the "HBDEV stats" lines, that are not ending with '):' and start with space
#        base1: physical/1000full, up, rx-bytes/packets/dropped/errors=311389146/636565/235/0, tx=310492903/575295/0/0
#        base2: physical/1000full, up, rx-bytes/packets/dropped/errors=283083317/474100/229/0, tx=249587946/410904/0/0
(table_hbdev_is_in == 1) && /^\s/ && !/\):/ {

    # if one line is not 'up'
    if ($0 !~ " up,") {
        table_hbdev_status = 0
    } else {
        table_hbdev_up_counter++
    }

    rx_bytes = getFirstPartOfBytes(getStringBetween($0, "errors=", ", tx"))
    tx_bytes = getFirstPartOfBytes(getStringBetween($0, "tx="))
    table_hbdev_total_bytes += (rx_bytes + tx_bytes)
}


# Set flag we are in the "MONDEV stats" lines
#MONDEV stats:
#    FG1HEFTK18000206(updated 1 seconds ago):
#        dmz: physical/100auto, up, rx-bytes/packets/dropped/errors=721951689/8193066/0/0, tx=395210869/425448/0/0
#    FG1HEFTK18000205(updated 1 seconds ago):
#        dmz: physical/00, down, rx-bytes/packets/dropped/errors=0/0/0/0, tx=0/0/0/0
/^MONDEV stats:/{
    table_mondev_is_in = 1
    next
}

# Set flag we are exited the "MONDEV stats" lines
(table_mondev_is_in == 1) && !/^\s/ {
    table_mondev_is_in = 0
}

# We are processing a line in the "MONDEV stats" lines, that are not ending with '):' and start with space
#        dmz: physical/100auto, up, rx-bytes/packets/dropped/errors=721951689/8193066/0/0, tx=395210869/425448/0/0
#        dmz: physical/00, down, rx-bytes/packets/dropped/errors=0/0/0/0, tx=0/0/0/0
(table_mondev_is_in == 1) && /^\s/ && !/\):/ {

    # if one line is not 'up'
    if ($0 !~ " up,") {
        table_mondev_status = 0
    }

    rx_bytes = getFirstPartOfBytes(getStringBetween($0, "errors=", ", tx"))
    tx_bytes = getFirstPartOfBytes(getStringBetween($0, "tx="))
    table_mondev_total_bytes += (rx_bytes + tx_bytes)
}


END {

    # Publishing Health Status (add error description if needed)
    tags_health["name"] = "HA Health Status"
    if (health_status == 0) {
        # add the extra warning description when needed
        tags_health["error_desc"] = health_status_warning
    }
    writeDoubleMetric("ha-health-status", tags_health, "gauge", health_status, "true", "HA Status", "state", "name")  # Converted to new syntax by change_ind_scripts.py script

    # Publishing Sync Status
    tags_sync["name"] = "HA Config sync status"
    writeDoubleMetric("ha-config-sync-status", tags_sync, "gauge", table_sync_status, "true", "High Availability Cluster", "state", "name")  # Converted to new syntax by change_ind_scripts.py script

    # Publishing HBDEV Status
    tags_hbdev["name"] = "HA Heartbeat links status"
    writeDoubleMetric("ha-heartbeat-link-status", tags_hbdev, "gauge", table_hbdev_status, "true", "High Availability Cluster", "state", "name")  # Converted to new syntax by change_ind_scripts.py script
    tags_hbdev_bytes["name"] = "HA Heartbeat total bytes"
    writeDoubleMetric("ha-heartbeat-total-bytes", tags_hbdev_bytes, "gauge", table_hbdev_total_bytes, "true", "HA Heartbeat total bytes", "bytes", "name")  # Converted to new syntax by change_ind_scripts.py script

    # Publishing MONDEV Status
    tags_mondev["name"] = "HA Monitor links status"
    writeDoubleMetric("mon-interface-link-status", tags_mondev, "gauge", table_mondev_status, "true", "High Availability Cluster", "state", "name")  # Converted to new syntax by change_ind_scripts.py script
    tags_mondev_bytes["name"] = "HA Monitor total bytes"
    writeDoubleMetric("mon-interface-total-bytes", tags_mondev_bytes, "gauge", table_mondev_total_bytes, "true", "HA Monitor total bytes", "bytes", "name")  # Converted to new syntax by change_ind_scripts.py script

    # Publish Counter Up
    tags_counter["name"] = "HA heatbeat operational int. > 1"
    writeDoubleMetric("ha-heartbeat-int-oper-status-over1", tags_counter, "gauge", table_hbdev_up_counter >=4, "true", "High Availability Cluster", "state", "name")  # Converted to new syntax by change_ind_scripts.py script
}