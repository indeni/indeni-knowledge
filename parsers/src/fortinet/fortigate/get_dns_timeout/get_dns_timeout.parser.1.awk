/^1.3.6.1.4.1.12356.101.10.115.1.7.0/ {
   value = getSnmpValue($0)
   writeComplexMetricString("fortigate-dns-timeout", null, value, "true", "Fortigate DNS Timeout")
}