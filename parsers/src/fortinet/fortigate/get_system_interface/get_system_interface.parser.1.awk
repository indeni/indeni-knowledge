#----------------------------------------------------------------------------------------------------------
#
# Helper function used to extract a specific part of a string [key1: value1 key2: value2.... keyN: valueN] string.
# Example: Retrieve the 'type' value from the string [status: up type: tunnel  sflow-sampler: disable  ...]
#   stringLine:     is the full line from where we will extract the value
#   stringKey:      is the key that we will use for extraction (example: 'type:')
#   indexOfValue:   is the index of the value (there is a special case of the ip value that has two parts)
#
#----------------------------------------------------------------------------------------------------------
function getValueOf(stringLine, stringKey, indexOfValue){

    # Set default value of indexOfValue to '2'
    if (length(indexOfValue) == 0) {
        indexOfValue = 2
    }

    # Find the index of the expected 'key'
    indexStart = index(stringLine, stringKey)

    if (indexStart !=0){
        # if the key exist then substring and take only the needed part
        stringToUse = substr(stringLine, indexStart)
        gsub(/[ ]{2,}/, " ", stringToUse)

        # split string using /\s/, and return the complete array. The second item is the value
        lengthOfArray = split(stringToUse, array_string, /\s/)
        if (indexOfValue <= lengthOfArray) {
            return array_string[indexOfValue]
        }
        return ""
    }
    return ""
}


BEGIN {

    # We store in a table all the physical-interface-metrics. The table-index is 'table_index_physical'
    table_index_physical = 0

    # We store in a table all the non-physical-interface-metrics. The table-index is 'table_index_non_physical'
    table_index_non_physical = 0

    # A flag is set to '1' ONLY if the processing line belongs to the first command (section-a).
    # By default is section-a, meaning that we are processing data of the physical interfaces
    is_in_section_a = 1

    # Index for admin state array
    table_index_interfaces_admin_state = 0

}

# Important: this section must be in the top. Checks if we are in section-b (the second command output)
# If a line starts with 'name', then we are processing section-b, and mark the flag 'is_in_section_a' to false
#name: wan1   mode: static    ip: 212.205.216.194 255.255.255.240   status: up    netbios-forward: disable    type: physical   netflow-sampler: disable    sflow-sampler: disable    scan-botnet-connections: block    src-check: enable    mtu-override: disable    wccp: disable    drop-overlapped-fragment: disable    drop-fragment: disable
/^name/ {

    # we set the flag to false
    is_in_section_a = 0

}

# A. This part is for parsing the first command (get system interface physical)
# Parse the interface name. This is also the "start" of the table-row.
# So we increment the table-index
#        ==[mgmt]
/\s==\[/ && (is_in_section_a == 1) {

    # Increase table index, and store the name of the physical-interface
    table_index_physical++
    table_interface_physical["name", table_index_physical] = substr($1, 4, length($1)-4)
}

# A. This part is for parsing the first command (get system interface physical)
#                mode: static
/\smode: / && (is_in_section_a == 1) {

    # Store the mode of the physical-interface
    table_interface_physical["mode", table_index_physical] = $2

}

# A. This part is for parsing the first command (get system interface physical)
#                ip: 10.10.8.59 255.255.255.0
/\sip: / && (is_in_section_a == 1) {

    # Store the ip and subnet of the physical-interface
    table_interface_physical["ip-address", table_index_physical] = $2
    table_interface_physical["ip-subnet", table_index_physical] = $3

}

# A. This part is for parsing the first command (get system interface physical)
#                status: up
/status: / && (is_in_section_a == 1){


    # Store the status (1 | 0) of the physical-interface

    # Set default value to '0'. Set to 1 only if the value is "up"
    interface_state = 0
    if(tolower(trim($2)) == "up"){
        interface_state = 1
    }
    table_interface_physical["state", table_index_physical] = interface_state

}

# A. This part is for parsing the first command (get system interface physical)
#                speed: n/a
#                speed: 1000Mbps (Duplex: full)
/speed:\s/ && (is_in_section_a == 1){

    table_interface_physical["speed", table_index_physical] = $2

    # Parse "Duplex" only if the word "Duplex" exist
    if (NF == 4 && $3 == "(Duplex:") {

        duplex_type = tolower($4)

        # removing last character ')' from duplex type
        duplex_type = substr(duplex_type, 1, length(duplex_type)-1)
        table_interface_physical["duplex", table_index_physical] = duplex_type

    }
}


# B. This part is for parsing the second command (get system interface)
#Example of none-physical type of interfaces
#name: ssl.root   ip: 0.0.0.0 0.0.0.0   status: up    netbios-forward: disable    type: tunnel   netflow-sampler: disable    sflow-sampler: disable    scan-botnet-connections: disable    src-check: enable    wccp: disable
#name: ssl.dmgmt-vdom   ip: 0.0.0.0 0.0.0.0   status: up    netbios-forward: disable    type: tunnel   netflow-sampler: disable    sflow-sampler: disable    scan-botnet-connections: disable    src-check: enable    explicit-web-proxy: disable    explicit-ftp-proxy: disable    proxy-captive-portal: disable    wccp: disable
/^name:/{

    # Read interface type. Parse only if it is NOT "physical"
    interface_type = getValueOf($0, "type:")

    if (interface_type != "physical" && interface_type != "") {

        table_index_non_physical++
        table_interface_non_physical["name", table_index_non_physical] = $2

        # Read interface_status (up | down) and convert to number
        interface_status = getValueOf($0, "status:")
        interface_status_number = 0
        if (tolower(interface_status) == "up") {
            interface_status_number = 1
        }

        ip_address = getValueOf($0, "ip:", 2)
        ip_subnet = getValueOf($0, "ip:", 3)
        interface_type = getValueOf($0, "type:")

        table_interface_non_physical["type", table_index_non_physical] = interface_type
        table_interface_non_physical["state", table_index_non_physical] = interface_status_number
        table_interface_non_physical["ip-address", table_index_non_physical] = ip_address
        table_interface_non_physical["ip-subnet", table_index_non_physical] = ip_subnet

    }
}



#C. This section is for parsing third command (show system interface)
#config system interface - This is the start of "Show system interface" command output
/^config system interface/ {
        show_system_section = 1
}
#this is the end
/^end/ {
          if (show_system_section == 1) show_system_section == 0
}

#    edit "port1"
/\s*edit/ {
          if (show_system_section == 1) {
                  edit_section = 1
                  port = $2             #we save port information
                  port = substr(port, 2, length(port) - 2) # removed double quotes
                  table_interfaces_admin_state["name",table_index_interfaces_admin_state] = port
                  table_interfaces_admin_state["status",table_index_interfaces_admin_state] = 1 #by default is enabled

          }

}
#        set status down
/\s*set status down/ {
        if (show_system_section == 1 && edit_section == 1 && port != 0) {
                table_interfaces_admin_state["status",table_index_interfaces_admin_state] = 0 # if set status down found, then admin disabled
                }

}

#    next - This closes section for the port
/\s*next/ {
        if (show_system_section == 1) {
                port = 0                # we clear port information
                edit_section = 0
                table_index_interfaces_admin_state++ # we move index
        }
}




END {

    # Publish physical interface metrics
    for (table_i = 1; table_i < table_index_physical + 1; table_i++) {
        tags["name"] = table_interface_physical["name", table_i]

        writeDoubleMetric("network-interface-state", tags, "gauge", table_interface_physical["state", table_i], "true", "Physical Network Interface status", "state", "name")  # Converted to new syntax by change_ind_scripts.py script
        writeComplexMetricString("network-interface-duplex", tags, table_interface_physical["duplex", table_i], "false")  # Converted to new syntax by change_ind_scripts.py script
        writeComplexMetricString("network-interface-mode", tags, table_interface_physical["mode", table_i], "false")  # Converted to new syntax by change_ind_scripts.py script
        writeComplexMetricString("network-interface-type", tags, "physical", "false")  # Converted to new syntax by change_ind_scripts.py script
        writeComplexMetricString("network-interface-ipv4-address", tags, table_interface_physical["ip-address", table_i], "false")  # Converted to new syntax by change_ind_scripts.py script
        writeComplexMetricString("network-interface-ipv4-subnet", tags, table_interface_physical["ip-subnet", table_i], "false")  # Converted to new syntax by change_ind_scripts.py script

        # due to alert requirement, we add different tag for speed
        speed_tags["name"] = table_interface_physical["name", table_i]
        writeComplexMetricString("network-interface-speed", speed_tags, table_interface_physical["speed", table_i], "false")  # Converted to new syntax by change_ind_scripts.py script
    }

    # Publish non-physical interface metrics
    for (table_i = 1; table_i < table_index_non_physical + 1; table_i++) {
        tags["name"] = table_interface_non_physical["name", table_i]

        writeDoubleMetric("network-interface-state", tags, "gauge", table_interface_non_physical["state", table_i], "true", "Non-Physical Network Interfaces status", "state", "name")  # Converted to new syntax by change_ind_scripts.py script
        writeComplexMetricString("network-interface-type", tags, table_interface_non_physical["type", table_i], "false")  # Converted to new syntax by change_ind_scripts.py script
        writeComplexMetricString("network-interface-ipv4-address", tags, table_interface_non_physical["ip-address", table_i], "false")  # Converted to new syntax by change_ind_scripts.py script
        writeComplexMetricString("network-interface-ipv4-subnet", tags, table_interface_non_physical["ip-subnet", table_i], "false")  # Converted to new syntax by change_ind_scripts.py script
    }

    #Publish interface administrative status
    for (table_i = 1; table_i < table_index_interfaces_admin_state + 1; table_i++) {
        tags["name"] = table_interfaces_admin_state["name", table_i]
        writeDoubleMetric("network-interface-admin-state", tags, "gauge", table_interfaces_admin_state["status", table_i], "true", "Network Interface Administrative status", "state", "name")
    }

}
