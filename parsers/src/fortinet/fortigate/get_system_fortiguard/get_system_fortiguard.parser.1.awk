#antispam-license    : Contract
#antispam-license    : Expired
/^antispam-license/{
    antispam_license = $NF
}

#webfilter-license   : Contract
#webfilter-license   : Expired
/^webfilter-license/{
    webfilter_license = $NF
}

#antispam-expiration : Sun Feb 11 2018
#antispam-expiration : N/A
/^antispam-expiration/{
    # if last field is year (4 digits) then parse date using the three last columns
    if($NF ~ /\d{4}/){
        year = $NF
        day = $(NF-1)
        month = parseMonthThreeLetter(tolower($(NF-2)))
        date_antispam_expiration = date(year, month, day)
    }
}

#webfilter-expiration: Sun Feb 11 2018
#webfilter-expiration: N/A
/^webfilter-expiration/{
    # if last field is year (4 digits) then parse date using the three last columns
    if($NF ~ /\d{4}/){
        year = $NF
        day = $(NF-1)
        month = parseMonthThreeLetter(tolower($(NF-2)))
        date_webfilter_expiration = date(year, month, day)
    }
}


END {
    # Publish metrics (only if exist)
    if (length(antispam_license) > 0) {
        tags["name"] = "Antispam License Status"
        tags["im.identity-tags"] = "name"
        writeComplexMetricString("fortios-antispam-lic-status", tags, antispam_license, "true", "License Status")  # Converted to new syntax by change_ind_scripts.py script
    }

    if (length(webfilter_license) > 0) {
        tags["name"] = "Webfilter License Status"
        tags["im.identity-tags"] = "name"
        writeComplexMetricString("fortios-webfilter-lic-status", tags, webfilter_license, "true", "License Status")  # Converted to new syntax by change_ind_scripts.py script
    }

    # Publish date expiration-metric (Expire date for Antispam)
    tags["name"] = "AntiSpam"
    writeDoubleMetric("license-expiration", tags, "gauge", date_antispam_expiration, "true", "Licenses Expire date", "date", "name")  # Converted to new syntax by change_ind_scripts.py script

    # Publish date expiration-metric (Expire date for Web Filter)
    tags["name"] = "WebFilter"
    writeDoubleMetric("license-expiration", tags, "gauge", date_webfilter_expiration, "true", "Licenses Expire date", "date", "name")  # Converted to new syntax by change_ind_scripts.py script
}