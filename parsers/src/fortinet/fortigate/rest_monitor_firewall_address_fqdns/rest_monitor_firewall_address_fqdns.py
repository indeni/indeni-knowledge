from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

#     "results":{
#       "autoupdate.opera.com":{
#         "fqdn":"autoupdate.opera.com",
#         "addrs":[
#           "185.26.182.83",
#           "185.26.182.85"
#         ]
#       },
#       "update.microsoft.com":{
#         "fqdn":"update.microsoft.com",
#         "addrs":[
#           "13.64.25.102",
#           "40.90.247.210"
#         ]
#       },
#       "wildcard_google_1":{
#         "fqdn":"*.google.com",
#         "addrs":[
#         ]
#       }
#     },
#     "vdom":"root",
#       ....
#     "build":200
#   },


class WildcardFQDN(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:  # We check not empty
            data = helper_methods.parse_data_as_json(raw_data)
            for entry in data:  # we loop through VDOMs
                vdom_id = entry['vdom']
                results = entry['results']
                wildcard_fqdn = 1
                if len(results) != 0:  # objects defined
                    for k, v in results.items():
                        if '*' in v['fqdn']:
                            wildcard_fqdn = 0
                        fqdn_details = 'VDOM: ' + vdom_id + ' FQDN: ' + v['fqdn']
                        tags = {'name': fqdn_details}
                        self.write_double_metric('fortios-address-fqdns', tags, 'gauge', wildcard_fqdn, False)
        return self.output
