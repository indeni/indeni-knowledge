# ------------------------------------------------------------------
# A utility function that returns the part of a string after ':'
# Example:
#   param = 'global: 62 21 7a ea ff da c6 fe 0b 29 23 17 9e e9 0e 16'
#   result = '62 21 7a ea ff da c6 fe 0b 29 23 17 9e e9 0e 16'
# ------------------------------------------------------------------
function getStringAfterSemicolon(string_read_from) {
    index_start = match(string_read_from, ":") + 1
    index_end = length(string_read_from) + 1
    if (index_end > index_start)
        return trim(substr(string_read_from, index_start, (index_end - index_start)))
    else
        return "";
 }

# ------------------------------------------------------------
# We store all the data in two tables:
# 'table_device'    : each row holds the 'name' of the device (example 'table_device[1, "name"]' is the name of the first device)
# 'table_zone'      : each row hold 'name_[device-index]', 'value_debugzone_[device-index]', 'value_checksum_[device-index]'
# ------------------------------------------------------------
BEGIN {
    table_index_device = 0
    table_index_zone = 0
    prefix = ""
}

#================== FGT60E4Q16088207 ==================
/^===============/ {
    table_index_device++;
    table_device[table_index_device, "name"] = $2;
    table_index_zone = -1;
}

/^debugzone/{
    table_index_zone = 0;
    checksum_prefix = "debugzone"
}

/^checksum/{
    table_index_zone = 0;
    checksum_prefix = "checksum"
}

#global: 62 21 7a ea ff da c6 fe 0b 29 23 17 9e e9 0e 16
#root: 86 54 df d0 bb 0a 4a 5f ab ad 86 9a 37 e6 c0 30
/:/{
    table_index_zone++
    table_zone[table_index_zone, "name_" table_index_device] = substr($1, 1, length($1)-1);
    table_zone[table_index_zone, "value_" checksum_prefix "_" table_index_device] = getStringAfterSemicolon($0);
}


END {
    # The results for the comparison of "debug" mismatch
    is_zone_debug_invalid = 0
    zone_debug_invalid = ""

    # The results for the comparison of "checksum" mismatch
    is_zone_checksum_invalid = 0
    zone_checksum_invalid = ""

    if (table_index_device > 1) {
        # For each Zone: compare the value of the device[0].zone[i] with the other devices[j>0].zone[i]
        for (zone_index = 1; zone_index <= table_index_zone; zone_index++) {

            # The Default zone name/zone value is from FIRST DEVICE.
            # We will use it as reference to compare it with the other device/zone values
            zone_name = table_zone[zone_index, "name_1"]
            zone_debug_value = table_zone[zone_index, "value_debugzone_1"]
            zone_checksum_value = table_zone[zone_index, "value_checksum_1"]

            for (device_index = 2; device_index <= table_index_device; device_index++) {

                zone_debug_value_next = table_zone[zone_index, "value_debugzone_" device_index];
                zone_checksum_value_next = table_zone[zone_index, "value_checksum_" device_index];

                # Compare Debug
                if (zone_debug_value != zone_debug_value_next) {
                    zone_debug_invalid = "[" table_device[device_index, "name"] ":debug." zone_name "]"(is_zone_debug_invalid ? "/" : "") zone_debug_invalid
                    is_zone_debug_invalid = 1
                }

                # Compare CheckSum
                if (zone_checksum_value != zone_checksum_value_next) {
                    zone_checksum_invalid = "[" table_device[device_index, "name"] ":checksum." zone_name "]"(is_zone_checksum_invalid ? "/" : "") zone_checksum_invalid
                    is_zone_checksum_invalid = 1
                }
             }
        }
    }

    tags_debug["device"] = zone_debug_invalid
    tags_debug["name"] = "HA Debugzone sync status"
    writeDoubleMetric("ha-cluster-debugzone-status", tags_debug, "gauge", !is_zone_debug_invalid, "true", "High Availabilty Cluster Sync", "state", "name")  # Converted to new syntax by change_ind_scripts.py script

    tags_checksum["device"] = zone_checksum_invalid
    tags_checksum["name"] = "HA Checksum sync status"
    writeDoubleMetric("ha-cluster-checksum-status", tags_checksum, "gauge", !is_zone_checksum_invalid, "true", "High Availabilty Cluster Sync", "state", "name")  # Converted to new syntax by change_ind_scripts.py script

 }