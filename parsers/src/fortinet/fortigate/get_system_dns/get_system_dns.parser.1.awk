BEGIN {
    dnsServerCount = 0
}

# Note that I can't seem to "unset" the ipv4 addresses in fortiOS -- 0.0.0.0 is the worst I can figure out to do.
# The ipv6 address can be empty, as "::".
#primary             : 208.91.112.53
#secondary           : 208.91.112.52
#ip6-primary         : 2001:0db8:85a3:0000:0000:8a2e:0370:7334
#ip6-secondary       : ::
/^(primary|secondary|ip6-primary|ip6-secondary)/ {
	dnsServer = $NF
	if (dnsServer != "::") {
        dnsServerCount++
        dnsServers[dnsServerCount, "ipaddress"] = dnsServer
	}
}

END {
    writeComplexMetricObjectArray("dns-servers", null, dnsServers, "true", "DNS Servers")  # Converted to new syntax by change_ind_scripts.py script
}