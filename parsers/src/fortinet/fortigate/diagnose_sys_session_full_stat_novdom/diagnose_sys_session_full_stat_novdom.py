from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

states = ['ESTABLISHED', 'SYN_SENT', 'SYN_RECV', 'FIN_WAIT1', 'FIN_WAIT2', 'TIME_WAIT', 'CLOSE', 'CLOSE_WAIT',
            'LAST_ACK', 'LISTEN', 'CLOSING', 'UNKNOWN']

class DiagnoseSysSessionFullStatNoVdom(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data = helper_methods.parse_data_as_list(raw_data, 'diagnose_sys_session_full_stat.textfsm')
            if data:
                    tags = {}
                    tags['vdom.name'] = 'global'
                    tags['im.identity-tags'] = 'counter'
                    tags['counter'] = 'Current Total'
                    self.write_double_metric('sessions-total-count', tags, 'gauge', int(data[0]['current_sessions']), True, 'Session Counters', 'number')
                    tags['counter'] = 'Current Rate (conns/sec)'
                    self.write_double_metric('sessions-setup-rate', tags, 'gauge', int(data[0]['sessions_per_sec']), True, 'Session Counters', 'number')
                    tags['counter'] = 'Clash Total'
                    self.write_double_metric('clash-sessions', tags, 'counter', int(data[0]['clash_sessions']), True, 'Session Counters', 'number')
                    tags['counter'] = 'Removable Total'
                    self.write_double_metric('sessions-removeable',tags, 'gauge', int(data[0]['removal_sessions']), True, 'Session Counters', 'number')
                    tags['counter'] = 'Removed Due Low Memory'
                    self.write_double_metric('memory-tension-drop',tags, 'counter', int(data[0]['memory_tension_drop']), True, 'Session Counters', 'number')
                    tags['counter'] = 'Removed Due Interface Down'
                    self.write_double_metric('sessions-dev-down', tags, 'counter', int(data[0]['removed_sessions_due_down_int']), True, 'Session Counters', 'number')
                    tags['counter'] = 'Removed Due Flush'
                    self.write_double_metric('flush-sessions', tags, 'gauge', int(data[0]['removed_sessions_due_flush']), True, 'Session Counters', 'number')
                    tags['counter'] = 'Ephemeral State'
                    self.write_double_metric('ephemeral-sessions', tags, 'counter', int(data[0]['sessions_ephemeral_state']), True, 'Session Counters', 'number')
                    tags = {}
                    for state in states:
                        tags['tcp_session_state'] = state
                        tags['im.identity-tags'] = 'tcp_session_state'
                        found = False
                        for stat in data[0]['tcp_sessions']:
                            if len(stat) > 0 and not found:
                                if state == str(stat).split()[2]:
                                    found = True
                                    self.write_double_metric('tcp-sessions-stats', tags, 'gauge', int(str(stat).split()[0]), True, 'Sessions - TCP Stats', 'number')
                        if not found:
                            self.write_double_metric('tcp-sessions-stats', tags, 'gauge', 0, True, 'Sessions - TCP Stats', 'number')

        return self.output
