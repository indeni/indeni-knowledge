# ------------------------------------------------------------------------------
# Helper function.
# Get as parameter a string fraction ex. '200MB/400MB' and return the fraction result.
# For the input '200MB/400MB' the result would be '50'
# ------------------------------------------------------------------------------
function getTheDivisionResultInPercentage(strFraction) {

    # Read the fraction 262MB/31509MB
    split(strFraction, fraction_array, "/")

    # Remove from fraction the "MB" in order to make the division
    # Convert string from "262MB" to "262"
    gsub(/[a-zA-Z]+/, "", fraction_array[1])
    gsub(/[a-zA-Z]+/, "", fraction_array[2])

    # Store the division of the fraction
    fraction_result = fraction_array[1] / fraction_array[2]
    return fraction_result * 100
}


#-----------------------------------------------------------------------
# Helper function.
# Split the input string using ':' as a delimiter and return second part.
# For example for input 'packets:5' the result is '5'
#-----------------------------------------------------------------------
function getSecondPart(stringWithDelim) {
    split(stringWithDelim, stringArray, ":")
    return stringArray[2]
}

# Initialize the variables
BEGIN {

    # The index of table
    index_vdom = 0

    # Initialize variables
    fortigate_hd_usage = 0
    fortigate_hd_log_space = ""

}

# remove values in parehthesis
# Total HD usage: 208MB(199MiB)/31703MB(30235MiB)
/\(.*\)/ {
    gsub(/\([^)]*\)/, "")
}

#Total HD usage: 262MB/31509MB
#Total HD usage: 262MB/31509 MB
/^Total HD usage:/{

    secondPart = getSecondPart($0)

    # Remove spaces (in case of ver.5.4 it could be "262MB/31509 MB")
    gsub(" ","",secondPart)
    fortigate_hd_usage = getTheDivisionResultInPercentage(secondPart)

}

#Total HD logging space: 23631MB
#Total HD logging space: 23631 MB
/^Total HD logging space:/{

    secondPart = getSecondPart($0)

    # Remove spaces (in case of ver.5.4 it could be "262 MB")
    gsub(" ","",secondPart)
    fortigate_hd_log_space = secondPart

}

#HD logging space usage for vdom "root": 18MB/11815MB
#HD logging space usage for vdom "root": 18MB/11815 MB
/^HD logging space usage for vdom/{

    # Increase the array size
    index_vdom++

    # split the line [HD logging space usage for vdom "root": 18MB/11815 MB] using ':' as delimiter
    # so the first part is [HD logging space usage for vdom "root"]
    # and the second part is [18MB/11815 MB]
    split($0, stringArray, ":")
    stringPartOne = stringArray[1]
    stringPartTwo = stringArray[2]

    # Get the name of the vdom ("root:")
    lengthOfArray = split(stringPartOne, arrayName, " ")
    name = arrayName[lengthOfArray]

    # Remove from name ("root:") the characters '"' and ':'
    gsub(/"|:/, "", name)

    # Store the data
    array_vdom[index_vdom, "name"] = name
    array_vdom[index_vdom, "usage"] = getTheDivisionResultInPercentage(stringPartTwo)
}


END {

    # Publishing metric in category "Hard Disk Usage"
    tags["name"] = "Fortinet Hard Disk Usage"
    writeDoubleMetric("fortios-hd-usage", tags, "gauge", fortigate_hd_usage, "true", "Hard Disk Usage", "percentage", "name")  # Converted to new syntax by change_ind_scripts.py script

    # Publishing metrics in "Default/Overview" category
    writeComplexMetricString("fortios-hd-log-space", null, fortigate_hd_log_space, "true", "Fortinet Hard Disk Log Space")  # Converted to new syntax by change_ind_scripts.py script

    for(i = 1; i <= index_vdom; i++){
       tags_to_publish["name"] = array_vdom[i, "name"]
       writeDoubleMetric("fortios-hd-log-usage-vdom", tags_to_publish, "gauge", array_vdom[i, "usage"], "true", "Fortinet Hard Disk Usage VDOM: " array_vdom[i, "name"], "percentage", "")  # Converted to new syntax by change_ind_scripts.py script
    }

}