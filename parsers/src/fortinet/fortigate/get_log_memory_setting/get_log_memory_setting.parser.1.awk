# Parse the status metric (enable or disable)
#status              : disable
#status              : enable
/^status/{

    # get the last part (to lowercase).
    status_value = tolower($NF)

    # Compare the value with 'enable'
    is_status_enable = 0
    if (status_value == "enable") {
        is_status_enable = 1
    }
}

END {
    # Publishing in category 'Logging Local Service'
    # Please note that IF the 'is_status_enable' is not set/initialized then the metric will not be published.
    tags["name"] = "Logging Memory"
    writeDoubleMetric("fortios-memory-logging", tags, "gauge", is_status_enable, "true", "Logging Local Service", "boolean", "name")  # Converted to new syntax by change_ind_scripts.py script
}