# [Tags to parse]   [version],[model]
# Set flag 'is_fortigate_device' to 1 meaning that we have a fortigate
#Version: FortiGate-100D v5.6.0,build1449,170330 (GA)
/Version: Forti/ {
    is_fortigate_device = 1
    model = $2

    split($3, version_build, ",")
    version = version_build[1]

    # vdom related tags
    is_vdom_enabled = "false"
    is_vdom_root = "false"
    vdom_count = 0

    # removing the 'v' from 'v5.6.0'. The final version should be '5.6.0'
    #v5.6.0
    gsub(/v/, "", version)
}


# [Tags to parse]   [serial]
#Serial-Number: FG100D3G15809339
/Serial-Number:/ {
  serial = $2
}


# [Tags to parse]   [os.branch]
#Branch point: 1449]
/Branch point:/ {
  branch_point = $3
}

# [Tags to parse]   [hostname]
#Hostname: FG100D3G15809339
/Hostname:/ {
  hostname = $2
}

# [Tags to parse]   [operation.mode]
#Operation Mode: NAT
/Operation Mode:/ {
  operation_mode = $3
}

# [Tags to parse]   [high-availability]
# If anything apart from 'standalone' , we assume it is in cluster mode
#Current HA mode: standalone
/Current HA mode:/ {
  if ($4 != "standalone") {
    is_ha = "true"
  } else {
    is_ha = "false"
  }
}

# [Tags to parse] [vdom_enabled]
#Virtual domain configuration: disable
#Virtual domain configuration: enable
#Virtual domain configuration: split-task
/^Virtual domain configuration:/{
    if ($NF != "disable") {
        is_vdom_enabled = "true"
    }
}

# [Tags to parse] [vdom_root]
#Current virtual domain: root
/^Current virtual domain:/{
    if ($NF == "root") {
        is_vdom_root = "true"
    }
}

# Virtual domains status: 1 in NAT mode, 0 in TP mode
/^Virtual domains status:/{
    vdom_count = $4 + $8
}

END {

    if (is_fortigate_device == 1) {
        writeTag("vendor", "fortinet")
        writeTag("serial", serial)
        writeTag("model", model)
        writeTag("hostname", hostname)
        writeTag("operation.mode", operation_mode)

        # Fortigate is the firewall product of the fortinet family
        writeTag("product", "firewall")
        writeTag("os.name", "FortiOS")
        writeTag("os.version", version)
        writeTag("os.branch", branch_point)
        writeTag("high-availability", is_ha)
        writeTag("vdom_enabled", is_vdom_enabled)
        writeTag("vdom_root", is_vdom_root)

        writeTag("vsx",is_vdom_enabled)

        if (is_vdom_enabled == "true") {
            writeTag("vs-count", vdom_count)
            writeTag("license-vs-ratio", 10)
        }
    }
}
