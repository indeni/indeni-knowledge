BEGIN{
    # Initialize the table index
    table_hardware_index = 0
}

# Parse all the needed info and store them in the table
# 2 +5V               alarm=0  value=5.048  threshold_status=0
# 4 CPU VCCP          alarm=0  value=0.9609  threshold_status=0
#13 DDR3 VTT          alarm=0  value=0.736  threshold_status=0
#24 FAN_TMP_3         alarm=0  value=42  threshold_status=0
/alarm=.*value=.*threshold_status=/ {

    indexNameStart = index($0, $2)
    indexNameEnd = index($0, "alarm=")
    nameOfElement = trim(substr($0, indexNameStart, indexNameEnd-indexNameStart))

    # Add only 'FAN', 'CPU', 'PS' hardware elements
    if (nameOfElement ~ /(CPU)|(FAN)|(PS)/){
        table_hardware_index++


        # Parsing the text after 'alarm=', in order to read the value (0|1) of alarm
        alarmOfElement = trim(substr($0,indexNameEnd+6,1))
        statusOfElement = 1
        if (alarmOfElement == 1) {
          statusOfElement = 0
        }

        table_hardware[table_hardware_index, "name"] = nameOfElement
        table_hardware[table_hardware_index, "status"] = statusOfElement
    }
}

END {
    # Publishing table data (no Category / Default)
    for(index_of_table = 1; index_of_table <= table_hardware_index ; index_of_table++) {
         tags_to_publish["name"] = table_hardware[index_of_table, "name"]
         writeDoubleMetric("hardware-element-status", tags_to_publish, "gauge", table_hardware[index_of_table, "status"], "true", table_hardware[index_of_table, "name"], "state", "name")  # Converted to new syntax by change_ind_scripts.py script
    }
}