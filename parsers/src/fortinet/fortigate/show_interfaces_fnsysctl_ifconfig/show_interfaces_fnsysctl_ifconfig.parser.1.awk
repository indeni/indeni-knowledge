#-----------------------------------------------------------------------
# Helper function.
# Split the input string using ':' as a delimiter and return second part.
# For example for input 'packets:5' the result is '5'
#-----------------------------------------------------------------------
function getSecondPart(stringWithDelim) {
    split(stringWithDelim, stringArray, ":")
    return stringArray[2]
}

# Initialize the variables
BEGIN {
    # We will store each interface data in an array (array_interfaces)
    # Initialize the index of the array
    index_interface = 0
}

# The begin of the interface package.
# We increase the index_interface and store the data in the array.
#eth0    Link encap:Ethernet  HWaddr 08:5B:0E:EE:CA:B0
/\sLink encap:/ {
    index_interface++

    # 1. Parse first line (interface name)
    #eth1    Link encap:Ethernet  HWaddr 08:5B:0E:EE:CA:B1
    array_interfaces[index_interface, "name"] = $1

    # set default value for "network-interface-interrupts"
    array_interfaces[index_interface, "network-interface-interrupts"] = 0

}

# Parsing the rx line
#        RX packets:165910091 errors:0 dropped:0 overruns:0 frame:0
/\sRX packets:/{
    array_interfaces[index_interface, "network-interface-rx-errors"] = getSecondPart($3)
    array_interfaces[index_interface, "network-interface-rx-dropped"] = getSecondPart($4)
    array_interfaces[index_interface, "network-interface-rx-overruns"] = getSecondPart($5)
}

# Parsing the tx line
#        TX packets:1687661 errors:0 dropped:0 overruns:0 carrier:0
/\sTX packets:/{
    array_interfaces[index_interface, "network-interface-tx-errors"] = getSecondPart($3)
    array_interfaces[index_interface, "network-interface-tx-dropped"] = getSecondPart($4)
    array_interfaces[index_interface, "network-interface-tx-overruns"] = getSecondPart($5)
}

# Parsing the RX packets line
#        RX packets:3866 errors:0 dropped:0 overruns:0 frame:0
/\sRX packets:/{
    array_interfaces[index_interface, "network-interface-rx-packets"] = getSecondPart($2)
}

# Parsing the TX packets line
#        TX packets:0 errors:0 dropped:0 overruns:0 carrier:0
/\sTX packets:/{
    array_interfaces[index_interface, "network-interface-tx-packets"] = getSecondPart($2)
}

# Parsing the collisions line
#        collisions:0 txqueuelen:1000
/\scollisions:/{
    array_interfaces[index_interface, "network-interface-tx-collisions"] = getSecondPart($1)
}

# Parsing the interrupts line
#        Interrupt:19 Memory:df700000-df720000
/\sInterrupt:/{
    array_interfaces[index_interface, "network-interface-interrupts"] = getSecondPart($1)
}

# Parsing the rx/tx in bytes
#        RX bytes:0 (0  Bytes)  TX bytes:0 (0  Bytes)
/\sRX bytes:/{
    array_interfaces[index_interface, "network-interface-rx-bits"] = getSecondPart($2) * 8
    array_interfaces[index_interface, "network-interface-tx-bits"] = getSecondPart($6) * 8
}

END {

    # Publishing metrics based on the parsed data
    for( index_of_table = 1; index_of_table < index_interface + 1; index_of_table++ ){

        tags_name_ui["name"] = array_interfaces[index_of_table, "name"]

        tags_name_ui["name-interface"] = array_interfaces[index_of_table, "name"] " (RX Errors)"
        writeDoubleMetric("network-interface-rx-errors", tags_name_ui, "gauge", array_interfaces[index_of_table, "network-interface-rx-errors"], "true", "Interface Statistics", "number", "name-interface")  # Converted to new syntax by change_ind_scripts.py script

        tags_name_ui["name-interface"] = array_interfaces[index_of_table, "name"] " (RX Dropped)"
        writeDoubleMetric("network-interface-rx-dropped", tags_name_ui, "gauge", array_interfaces[index_of_table, "network-interface-rx-dropped"], "true", "Interface Statistics", "number", "name-interface")  # Converted to new syntax by change_ind_scripts.py script

        tags_name_ui["name-interface"] = array_interfaces[index_of_table, "name"] " (RX Overruns)"
        writeDoubleMetric("network-interface-rx-overruns", tags_name_ui, "gauge", array_interfaces[index_of_table, "network-interface-rx-overruns"], "true", "Interface Statistics", "number", "name-interface")  # Converted to new syntax by change_ind_scripts.py script

        tags_name_ui["name-interface"] = array_interfaces[index_of_table, "name"] " (TX Errors)"
        writeDoubleMetric("network-interface-tx-errors", tags_name_ui, "gauge", array_interfaces[index_of_table, "network-interface-tx-errors"], "true", "Interface Statistics", "number", "name-interface")  # Converted to new syntax by change_ind_scripts.py script

        tags_name_ui["name-interface"] = array_interfaces[index_of_table, "name"] " (TX Dropped)"
        writeDoubleMetric("network-interface-tx-dropped", tags_name_ui, "gauge", array_interfaces[index_of_table, "network-interface-tx-dropped"], "true", "Interface Statistics", "number", "name-interface")  # Converted to new syntax by change_ind_scripts.py script

        tags_name_ui["name-interface"] = array_interfaces[index_of_table, "name"] " (TX Overruns)"
        writeDoubleMetric("network-interface-tx-overruns", tags_name_ui, "gauge", array_interfaces[index_of_table, "network-interface-tx-overruns"], "true", "Interface Statistics", "number", "name-interface")  # Converted to new syntax by change_ind_scripts.py script

        tags_name_ui["name-interface"] = array_interfaces[index_of_table, "name"] " (TX Collisions)"
        writeDoubleMetric("network-interface-tx-collisions", tags_name_ui, "gauge", array_interfaces[index_of_table, "network-interface-tx-collisions"], "true", "Interface Statistics", "number", "name-interface")  # Converted to new syntax by change_ind_scripts.py script

        # In case of not-live-config, the tag-name is not used.
        tags_name_only["name"] = array_interfaces[index_of_table, "name"]

        writeDoubleMetric("network-interface-interrupts", tags_name_only, "gauge", array_interfaces[index_of_table, "network-interface-interrupts"], "false")  # Converted to new syntax by change_ind_scripts.py script

        writeDoubleMetric("network-interface-rx-bits", tags_name_only, "gauge", array_interfaces[index_of_table, "network-interface-rx-bits"], "true", "Network Interfaces - RX Bits", "bits", "name")  # Converted to new syntax by change_ind_scripts.py script

        writeDoubleMetric("network-interface-tx-bits", tags_name_only, "gauge", array_interfaces[index_of_table, "network-interface-tx-bits"], "true", "Network Interfaces - TX Bits", "bits", "name")  # Converted to new syntax by change_ind_scripts.py script

        writeDoubleMetric("network-interface-rx-packets", tags_name_only, "gauge", array_interfaces[index_of_table, "network-interface-rx-packets"], "false")  # Converted to new syntax by change_ind_scripts.py script

        writeDoubleMetric("network-interface-tx-packets", tags_name_only, "gauge", array_interfaces[index_of_table, "network-interface-tx-packets"], "false")  # Converted to new syntax by change_ind_scripts.py script

    }

}