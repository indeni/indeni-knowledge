BEGIN {
	dns_success = 0
	fail = 0
}

# avoid FP on failing to run the ping command
# command parse error before 'ping'
# Command fail. Return code -61
/parse error/ || /Command fail/ {
	fail = 1
}

# Note that pinging a bogus domain name or disconnecting the internet result in the same thing:
# Unable to resolve hostname.
#PING indeni.com (109.199.106.156) 56(84) bytes of data.
#Unable to resolve hostname.
/PING .*indeni.com \([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\)/ {
	dns_success = 1
}

END {
	if (fail == 0) {
		dnstags["name"] = "Currently configured DNS server(s)"
		writeDoubleMetric("dns-server-state", dnstags, "gauge", dns_success, "true", "DNS Servers - State", "state", "name")  # Converted to new syntax by change_ind_scripts.py script
	}
}