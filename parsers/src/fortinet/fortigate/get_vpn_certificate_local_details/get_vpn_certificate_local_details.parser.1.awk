# 1) Fortinet has three "categories" of certs (displayed in the Web UI):  local, remote, and external.
#  It also stores (trusts) many root CA certificates (just like a web browser). This script currently checks
#  all of the local, external, and trusted CA certificates.  We haven't been able to get the "remote" certificates
#  yet -- the data is available from the CLI, but we need "nested config" support from Indeni to get the data.
# 2) AWK is mangling some Unicode of a few certificate names in the live config -- see this Crowd topic:
#  https://community.indeni.com/discussions/topics/46563?page=0.  On my test device, there are only 3 such certs, and
#  they're near the bottom of a long list in the live config.
# 3) Also in the live config, the timezone for the cert expiration date is incorrect. I think this is an Indeni bug.
#  See Crowd topic: https://community.indeni.com/discussions/topics/46573?page=0

#Name:        Fortinet_CA_SSL
/Name:/ {
    certName = $2
    certTags["name"] = certName
}

#Valid to:    2019-05-24 13:15:35  GMT
/Valid to:/ {
    expiresDate = $3
    expiresTime = $4
    split(expiresDate, expiresDateArr, "-")
    split(expiresTime, expiresTimeArr, ":")
    expirationDateTime = datetime(expiresDateArr[1], expiresDateArr[2], expiresDateArr[3], expiresTimeArr[1], expiresTimeArr[2], expiresTimeArr[3])
    writeDoubleMetric("certificate-expiration", certTags, "gauge", expirationDateTime, "true", "Certificates Expiration", "date", "name")  # Converted to new syntax by change_ind_scripts.py script
}