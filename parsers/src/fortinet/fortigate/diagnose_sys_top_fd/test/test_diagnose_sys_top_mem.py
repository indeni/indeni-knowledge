import os
import unittest

from fortinet.fortigate.diagnose_sys_top_fd.diagnose_sys_top_fd import DiagnoseSysTopFd
from parser_service.public.action import *

class TestDiagnoseSysTopFd(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = DiagnoseSysTopFd()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_top5_file_descriptors(self):
        # Act
        result = self.parser.parse_file(self.current_dir + '/top5_file_descriptors.input', {}, {})
        # Assert
        self.assertEqual(5, len(result))
        self.assertEqual(result[0].name, 'top-fd')
        self.assertEqual(result[2].tags['id'], '3')
        self.assertEqual(result[2].value['value'], 'ipsengine (281): 117')

if __name__ == '__main__':
    unittest.main()