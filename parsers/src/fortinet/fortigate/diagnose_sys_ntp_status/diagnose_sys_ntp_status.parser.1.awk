# Initialize the variables
BEGIN {

    # Initialize the values of metrics to publish
    ntp_is_synchronized = 0
    ntp_sync_is_enabled = 0
    ntp_server_mode = ""

    # By default ntp_server_is_configured is 0
    ntp_server_is_configured = 0

    # By default is_all_reachable is 1. (Note that if ntp is not configured we will publish the is_all_reachable to 0)
    ntp_servers_are_all_reachable = 1

    ntp_table_index = 0

}

#synchronized: yes, ntpsync: enabled, server-mode: disabled
/^synchronized:/ {

    # Check if synchronized is 'yes'
    # The ntp_is_synchronized is initialized to 0
    if (trim($2) == "yes,") {
        ntp_is_synchronized = 1
    }


    # Check if ntpsync_status is 'enabled'
    # The ntp_sync_is_enabled is initialized to 0
    ntpsync_status = tolower(trim($4))
    if (ntpsync_status == "enabled,") {
        ntp_sync_is_enabled = 1
    }

    # Store 'server-mode:' value
    ntp_server_mode = $6

}

#ipv4 server(10.10.8.145) 10.10.8.145 -- reachable(0xff) S:0 T:0
/^ipv4 server/ || /^ipv6 server/{

    # Fortinet ntp-server is configured
    ntp_table_index++

    # Set 'ipaddress'
    ntp_table[ntp_table_index, "ipaddress"] = $3

    # Read server name for example "server(10.10.8.145)"
    ntp_server_name = $2

    # Extract from string "server(10.10.8.145)" the text inside the parenthesis
    # Avoid regex and use 'substring' (using the length of 'server(' and last character ')')
    ntp_server_name = substr(ntp_server_name, 8, length(ntp_server_name)-8)

    # Set 'name'
    ntp_table[ntp_table_index, "name"] = ntp_server_name

    # Check if ntp server is reachable
    is_reachable = 1
    if ($0 ~ / unreachable/) {
        ntp_servers_are_all_reachable = 0
        is_reachable = 0
    }

    ntp_table[ntp_table_index, "is_reachable"] = is_reachable

}

END {

    # Publishing metrics
    writeDoubleMetric("ntp-is-synchronized", null, "gauge", ntp_is_synchronized, "true", "NTP Synchronization Status", "state", "")  # Converted to new syntax by change_ind_scripts.py script
    writeDoubleMetric("ntp-sync-is-enabled", null, "gauge", ntp_sync_is_enabled, "true", "NTP Sync Enabled", "state", "")  # Converted to new syntax by change_ind_scripts.py script
    writeComplexMetricString("ntp-server-mode", null, ntp_server_mode, "true", "NTP Server")  # Converted to new syntax by change_ind_scripts.py script
    writeComplexMetricObjectArray("ntp-servers", null, ntp_table, "false")  # Converted to new syntax by change_ind_scripts.py script

    # For each ntp-server publish 'ntp-server-state'
    for(table_index=1; table_index < (ntp_table_index+1); table_index++ ){
        tags["name"] = ntp_table[table_index, "ipaddress"]
        writeDoubleMetric("ntp-server-state", tags, "gauge", ntp_table[table_index, "is_reachable"], "true", "NTP Servers Status", "state", "name")  # Converted to new syntax by change_ind_scripts.py script
    }


}