#-----------------------------------------------------------------------
# Helper function.
# Split the input string using ':' as a delimiter and return second part.
# For example for input 'packets:5' the result is '5'
#-----------------------------------------------------------------------
function getSecondPart(stringWithDelim) {
    split(stringWithDelim, stringArray, ":")
    return stringArray[2]
}

# The first line starting with 'Forti' (FortiGuard or FortiCloud)
#FortiGuard Log & Analysis Service
#FortiCloud
/^Forti/{

    log_service_name = $0
    writeComplexMetricString("fortios-log-service-name", null, log_service_name, "true", "Log Service Name")  # Converted to new syntax by change_ind_scripts.py script
}


#Expire on: 20071231
/^Expire on: /{

    #Read the date in format '20071231'
    date_str = $3

    # Split the date in year,month,day
    year = substr(date_str,1,4)
    month = substr(date_str,5,2)
    day = substr(date_str,7,2)

    # Convert date in epoch-seconds
    date_expire = date(year, month, day)

    # Set from first-line parsing
    tags["name"] = log_service_name
    writeDoubleMetric("fortios-log-service-expire-date", tags, "gauge", date_expire, "true", "Expire date for Forti Log service", "date", "")  # Converted to new syntax by change_ind_scripts.py script
}


#Total disk quota: 1111 MB
/^Total disk quota:/{

    total_disk = getSecondPart($0)
    writeComplexMetricString("fortios-log-service-disk-quota", null, total_disk, "true", "Total Disk Quota")  # Converted to new syntax by change_ind_scripts.py script
}

#Max daily volume: 111 MB
/^Max daily volume:/{

    max_daily = getSecondPart($0)
    writeComplexMetricString("fortios-log-service-max-daily-volume", null, max_daily, "true", "Max Daily Volume")  # Converted to new syntax by change_ind_scripts.py script
}

#Current disk quota usage: 342 MB
/^Current disk quota usage:/{

    disk_quota_usage = getSecondPart($0)
    writeComplexMetricString("fortios-log-disk-quota-usage", null, disk_quota_usage, "true", "Current disk quota usage")  # Converted to new syntax by change_ind_scripts.py script

}