BEGIN{
    # Table index
    table_tunnel_index = 0

    # A flag, to mark the beginning of the table.
    # Set to 1 when we read the table-header
    is_in_table_section = 0
}

# Parse all the needed info ('remote-peer-name', 'peerip' & 'value') and store them in the table
# Sample table lines:
#Remote 10.10.9.1:0 10.0.8.0/255.255.255.0 10.0.9.0/255.255.255.0 up 43333
#test test    1.1.1.1:0        192.168.220.0/255.255.255.0 1.1.1.0/255.255.255.0    down
#ipsec test ATH 3.2.4.5:0        10.10.80.0/255.255.255.0 2.2.2.0/255.255.255.0    down
((NR > 1) && (NF > 3) && (is_in_table_section == 1)) {

    # Increase index of table
    table_tunnel_index++

    # Find the the index of the first ip in the line (this is actually the second column).
    index_of_ip = match($0, /[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}/)


    # Get the peer name. Is the first part between the 1-index and the start of the first-ip of the sentence
    remote_peer_name = trim(substr($0, 1, index_of_ip-1))

    # Store peer-name (first column)
    table_tunnel[table_tunnel_index, "remote-peer-name"] = remote_peer_name

    # Check if the last column is the 'timeout' (number). Otherwise the last-column is the 'status'
    if ($(NF) ~ /\d+/) {

        # Last column is the 'timeout' (number). So there are total 6 columns in line.
        value_status = $(NF-1)

        # Store peer-ip (second column)
        table_tunnel[table_tunnel_index, "peerip"] = $(NF-4)

    } else {

        # Last column is the 'status'. So there are 5 columns in line.
        value_status = $(NF)

        # Store peer-ip (second column)
        table_tunnel[table_tunnel_index, "peerip"] = $(NF-3)

    }

    # Check if is "up"
    is_status_up = 0
    if (value_status == "up") {
        is_status_up = 1
    }
    table_tunnel[table_tunnel_index, "value"] = is_status_up
}

# Set the flag 'is_in_table_section' to 1, meaning that the table is started, so we are reading table-lines
#NAME REMOTE-GW PROXY-ID-SOURCE PROXY-ID-DESTINATION STATUS TIMEOUT
/^NAME\s/{
    is_in_table_section = 1
}

END {
    # Publishing metrics based on the parsed data
    for (index_of_table = 1; index_of_table < table_tunnel_index + 1; index_of_table++) {

        # Construct tags of "vpn-tunnel-state"
        tags["name"] =  "Gateway " table_tunnel[index_of_table, "peerip"]
        tags["peerip"] =  table_tunnel[index_of_table, "peerip"]
        tags["remote-peer-name"] = table_tunnel[index_of_table, "remote-peer-name"]

        # Publish metric
        writeDoubleMetric("vpn-tunnel-state", tags, "gauge", table_tunnel[index_of_table, "value"], "true", "VPN Tunnels - State", "state", "name")  # Converted to new syntax by change_ind_scripts.py script
    }

}