#   Total number of submissions with anomaly      :       0
/Total number of submissions with anomaly/ {
    submissions_with_anomaly = $8
    metric_tag["name"] = "Total number of submissions with anomaly"
    writeDoubleMetric("fireeye-nx-workorders-submissions-with-anomaly", metric_tag, "gauge", submissions_with_anomaly, "true", "Workorders - Statistics", "number", "name")  # Converted to new syntax by change_ind_scripts.py script
}