#   Objects identified as Malicious          :       0
/^\s+Objects identified as Malicious/ {
    malicious_objects = $6
    tags["name"] = "Malicious Objects Found"
    writeDoubleMetric("fireeye-nx-malicious-objects", tags, "counter", malicious_objects, "true", "Object Analysis", "number", "name")  # Converted to new syntax by change_ind_scripts.py script
}