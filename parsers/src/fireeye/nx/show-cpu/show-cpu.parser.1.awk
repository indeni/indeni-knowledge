BEGIN {
    cpu_count = 0
    cpu_total = 0
    cpu_usage_avg = 0
}

#The below section identifies the CPUs
#  CPU 0
/^CPU [0-9]/ {
    cpu_id = $2
    cpu_count++
    next
}

#This section reads the utilization metric per CPU
#  Utilization:                4%
/^\s+Utilization:/ {
    cpu_util = $2
    sub(/%/, "", cpu_util)
    cpu[cpu_id] = cpu_util
    cpu_total = cpu_total + cpu_util
    next
}

END {
    cpu_usage_avg = cpu_total/cpu_count
    tags_cpu["cpu-is-avg"] = "true"
    tags_cpu["resource-metric"] = "true"
    tags_cpu["cpu-id"] = "Average CPU"
    writeDoubleMetric("cpu-usage", tags_cpu, "gauge", cpu_usage_avg, "true", "CPU Usage", "percentage", "cpu-id")

    for (id in cpu) {
        tags_cpu["cpu-is-avg"] = "false"
        tags_cpu["resource-metric"] = "false"
        tags_cpu["cpu-id"] = id
        tags_cpu["cpu-util"] = cpu[id]
        writeDoubleMetric("cpu-usage", tags_cpu, "gauge", cpu[id], "true", "CPU Usage", "percentage", "cpu-id")
    }
}