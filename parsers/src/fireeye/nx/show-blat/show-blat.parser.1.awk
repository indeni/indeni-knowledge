BEGIN {
    #1 is considered enabled
    category = "BLAT - Statistics"
    blat_enabled = 1
}

#If BLAT system is not enabled
#% BLAT subsystem not running.

/BLAT subsystem not running/ {
    blat_enabled = 0
}

function writeMetricWithDisplayName(metric_id, metric_name, metric_value, metric_type) {
    sub(/:/, "", metric_value)
    tags["name"] = metric_name
    writeDoubleMetric(metric_id, tags, metric_type, metric_value, "true", category, "number", "name")  # Converted to new syntax by change_ind_scripts.py script
}


#If BLAT system is enabled below metrics will be collected by Indeni server
#        Packet received                                   :197292
/^\s+Packet received/ {
    packet_received = $3
    writeMetricWithDisplayName("fireeye-nx-blat-packet-received", "Packet Received", packet_received, "gauge")
}

#        Static rules                                      :28319
/^\s+Static rules/ {
    static_rules = $3
    writeMetricWithDisplayName("fireeye-nx-blat-static-rules", "Static Rules", static_rules, "gauge")
}

#        Dynamic rules                                     :0
/^\s+Dynamic rules/ {
    dynamic_rules = $3
    writeMetricWithDisplayName("fireeye-nx-blat-dynamic-rules", "Dynamic Rules", dynamic_rules, "gauge")
}

#        Static bad rules                                  :0
/^\s+Static bad rules/ {
    static_bad_rules = $4
    writeMetricWithDisplayName("fireeye-nx-blat-static-bad-rules", "Static Bad Rules", static_bad_rules, "gague")
}

#        Dynamic bad rules                                 :0
/^\s+Dynamic bad rules/ {
    dynamic_bad_rules = $4
    writeMetricWithDisplayName("fireeye-nx-blat-dynamic-bad-rules", "Dynamic Bad Rules", dynamic_bad_rules, "gauge")
}

#        Events reported                                   :49
/^\s+Events reported/ {
    events_reported = $3
    writeMetricWithDisplayName("fireeye-nx-blat-events-reported", "Events Reported", events_reported, "counter")
}

#        Standby Events reported                           :0
/^\s+Standby Events reported/ {
    standby_events_reported = $4
    writeMetricWithDisplayName("fireeye-nx-blat-standby-events-reported", "Standby Events Reported", standby_events_reported, "gauge")
}

#        Events dropped                                    :0
/^\s+Events dropped/ {
    events_dropped = $3
    writeMetricWithDisplayName("fireeye-nx-blat-events-dropped", "Events Dropped", events_dropped, "gauge")
}

#        Inline dropped                                    :0
/^\s+Inline dropped/ {
    inline_dropped = $3
    writeMetricWithDisplayName("fireeye-nx-blat-inline-dropped", "Inline Dropped", inline_dropped, "gauge")
}

#        Inactive domain captured                          :60
/^\s+Inactive domain captured/ {
    inactive_domain = $4
    writeMetricWithDisplayName("fireeye-nx-blat-inactive-domain", "Inactive Domain Captured", inactive_domain, "gauge")
}

#        Blacklist IPs submitted                           :0
/^\s+Blacklist IPs submitted/ {
    blacklisted_ips = $4
    writeMetricWithDisplayName("fireeye-nx-blat-blacklisted-ips", "Blacklist IPs Submitted", blacklisted_ips, "counter")
}

END {
    tags["name"] = "BLAT Status"
    writeDoubleMetric("fireeye-nx-blat-is-enabled", tags, "gauge", blat_enabled, "true", category, "state", "name")  # Converted to new syntax by change_ind_scripts.py script
}