BEGIN {
    FS = ":"
    is_enabled = ""
    is_status_bad = ""
}

#  Security Content Updates
/\sSecurity Content Updates/ {
    i = 0
    # read next 7 lines one by one to find Enabled and Status
    while (i < 7) {
        getline
        property_name = trim($1)
        #    Enabled              : yes
        if (property_name == "Enabled") {
            property_value = trim($2)
            is_enabled = property_value
        }
        #    Status               : apply-done: Updates installed successfully
        else if (property_name == "Status") {
            property_value = trim($3)
            # check Status against known good status messages
            if (property_value == "Updates installed successfully" || property_value == "No new security updates available" || property_value == "Downloaded updates already installed") {
                is_status_bad = "no"
            }
            else {
                is_status_bad = "yes"
            }
        }
        i++
    }
}

END {
    if (is_enabled == "no" || is_status_bad == "yes") {
        writeDoubleMetric("fireeye-nx-content-update-status", null, "gauge", 0.0, "false")  # Converted to new syntax by change_ind_scripts.py script
    }
    else if (is_enabled == "yes" && is_status_bad == "no") {
        writeDoubleMetric("fireeye-nx-content-update-status", null, "gauge", 1.0, "false")  # Converted to new syntax by change_ind_scripts.py script
    }
}