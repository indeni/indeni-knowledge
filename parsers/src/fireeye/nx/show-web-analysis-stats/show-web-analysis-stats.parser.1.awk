BEGIN {
    missing_packet_flows = 0.0
    asymmetric_flows = 0.0
}

#Missing Packet Flows:   0.0
/^Missing Packet Flows:/ {
    missing_packet_flows = $4
    writeDoubleMetric("fireeye-nx-missing-packet-flows", null, "gauge", missing_packet_flows, "true", "Web Analysis - Missing Packet Flows", "percentage", "")  # Converted to new syntax by change_ind_scripts.py script
}

#Asymmetric Flows:       0.0
/^Asymmetric Flows:/ {
    asymmetric_flows = $3
    writeDoubleMetric("fireeye-nx-asymmetric-flows", null, "gauge", asymmetric_flows, "true", "Web Analysis - Missing Asymmetric Flows", "percentage", "")  # Converted to new syntax by change_ind_scripts.py script
}