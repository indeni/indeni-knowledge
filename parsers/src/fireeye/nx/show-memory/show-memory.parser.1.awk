#Physical 10013 MB   9384 MB    629 MB   9827 MB    185 MB
#Swap     32768 MB    650 MB  32118 MB
/^\w+\s+[0-9]+/ {
    tags["name"] = $1
    total = $2
    used = $4
    free = $6

    total_kbytes = total * 1024
    free_kbytes = free * 1024
    memory_usage = (used / total) * 100

    writeDoubleMetric("memory-free-kbytes", tags, "gauge", free_kbytes, "true", "Memory - Free", "kbytes", "name");
    writeDoubleMetric("memory-total-kbytes", tags, "gauge", total_kbytes, "true", "Memory - Total", "kbytes", "name")
    writeDoubleMetric("memory-usage", tags, "gauge", memory_usage, "true", "Memory - Usage", "percentage", "name")
}