BEGIN {
    # Initializing default values for interrogation tags
    product = ""
    model = ""
    edition = ""
    version = ""
    host_id = ""
    is_nx = 0
}


# Extract 'Product model'
#Product model:     FireEyeNX1500V (virtual system on vmware)

/^Product model:/ {
    #Retrieve model
    if ($3 ~ /FireEye/) {
        product = $3
        model = substr($3, 8)
    }
}

# Extract 'version'
#Version summary:   wmps wMPS (wMPS) 8.2.0.782612 #782612 2018-08-06 22:54:13 x86_64 build@vta425:FireEye/8.2.x-logan
/^Version summary:/ {
    #Retrieve version
    version = $6
    #Additional check to ensure it is NX and not other FireEye Platforms
    if ($4 == "wMPS") {
        is_nx = 1
    }
}

# Extract 'Product edition'
#Product edition:   Classic
/^Product edition:/ {
    #Retrieve edition
    if ($3 ~ /Classic|SmartVision/) {
        edition = $3
    }
    else {
        is_nx = 0
    }
}

# Extract 'Host ID'
#Host ID:           8690de621fe9
/^Host ID:/ {
    #Retrieve Host ID
    host_id = $3
}

END {
    #Only publish tags if FireEyeNX
    if (is_nx == 1) {
        writeTag("vendor", "fireeye")
        writeTag("os.name", "wMPS")

        if (product != "") {
            writeTag("product", product)

            if (model != "") {
                writeTag("model", model)
            }
        }
        if (edition != "") {
            writeTag("edition", edition)
        }
        if (version != "") {
            writeTag("os.version", version)
        }
        if (host_id != "") {
            writeTag("host-id", host_id)
        }
    }
}