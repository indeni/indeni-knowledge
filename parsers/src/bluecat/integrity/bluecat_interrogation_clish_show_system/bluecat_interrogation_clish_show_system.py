from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class BluecatInterrogationClishShowSystem(BaseParser):
    """
    Collect BlueCat tags via SSH:
        Hostname = new.server
        STIG Compliance = Disabled
        Product = Proteus
        Version = 9.4.0-674.GA.bcn
        Boot software version = BlueCat Networks (Proteus 9.4.0-674.GA.bcn)
        Memory = 7978 MB
        HD Size = 343 GB
    """

    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        data = helper_methods.parse_data_as_list(raw_data, 'bluecat_clish_show_system.textfsm')
        if data:
            if '.bcn' in data[0]['version']:
                self.write_tag('vendor', 'bluecat')
                if 'Adonis' in data[0]['product']:
                    self.write_tag('product', 'integrity')
                    self.write_tag('os.name', 'bdds')
                elif 'Proteus' in data[0]['product']:
                    self.write_tag('product', 'integrity')
                    self.write_tag('os.name', 'bam')
                version = data[0]['version'].split('-')[0]
                self.write_tag('os.version', version)

        return self.output
