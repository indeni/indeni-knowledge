import os
import unittest

from bluecat.integrity.bluecat_interrogation_clish_show_system.bluecat_interrogation_clish_show_system import BluecatInterrogationClishShowSystem
from parser_service.public.action import *

class TestBluecatInterrogationClishShowSystem(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = BluecatInterrogationClishShowSystem()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_BAM_VMware_9_4_0(self):
        result = self.parser.parse_file(self.current_dir + '/BAM_VMware_9_4_0.input', {}, {})
        self.assertEqual(4, len(result))
        self.assertEqual(result[0].action_type, 'WriteTag')
        self.assertEqual(result[0].key, 'vendor')
        self.assertEqual(result[0].value, 'bluecat')
        self.assertEqual(result[1].action_type, 'WriteTag')
        self.assertEqual(result[1].key, 'product')
        self.assertEqual(result[1].value, 'integrity')
        self.assertEqual(result[2].action_type, 'WriteTag')
        self.assertEqual(result[2].key, 'os.name')
        self.assertEqual(result[2].value, 'bam')
        self.assertEqual(result[3].action_type, 'WriteTag')
        self.assertEqual(result[3].key, 'os.version')
        self.assertEqual(result[3].value, '9.4.0')


    def test_BDDS_VMware_9_4_0(self):
        result = self.parser.parse_file(self.current_dir + '/BDDS_VMware_9_4_0.input', {}, {})
        self.assertEqual(4, len(result))
        self.assertEqual(result[0].action_type, 'WriteTag')
        self.assertEqual(result[0].key, 'vendor')
        self.assertEqual(result[0].value, 'bluecat')
        self.assertEqual(result[1].action_type, 'WriteTag')
        self.assertEqual(result[1].key, 'product')
        self.assertEqual(result[1].value, 'integrity')
        self.assertEqual(result[2].action_type, 'WriteTag')
        self.assertEqual(result[2].key, 'os.name')
        self.assertEqual(result[2].value, 'bdds')
        self.assertEqual(result[3].action_type, 'WriteTag')
        self.assertEqual(result[3].key, 'os.version')
        self.assertEqual(result[3].value, '9.4.0')


if __name__ == '__main__':
    unittest.main()

    