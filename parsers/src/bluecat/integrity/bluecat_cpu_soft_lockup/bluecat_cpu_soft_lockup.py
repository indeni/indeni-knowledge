from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class BluecatCpuSoftLockup(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            parsed_data = helper_methods.parse_data_as_list(raw_data, 'bluecat_cpu_soft_lockup.textfsm')
            if parsed_data:
                for data in parsed_data:
                    self.write_double_metric('cores-locked', {}, 'gauge', 1 if int(data.get('status')) == 1 else 0, False)
        return self.output