import os
import unittest
from bluecat.integrity.bluecat_cpu_soft_lockup.bluecat_cpu_soft_lockup import BluecatCpuSoftLockup
from parser_service.public.action import *

class TestBluecatCpuSoftLockup(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = BluecatCpuSoftLockup()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))    
    
    def test_cpu_soft_lockup_active(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_cpu_soft_lockup_active.input', {}, {})
        self.assertEqual(2,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'cores-locked')
        self.assertEqual(result[0].value, 1)
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].name, 'cores-locked')
        self.assertEqual(result[1].value, 1)

    def test_cpu_soft_lockup_disabled(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_cpu_soft_lockup_disabled.input', {}, {})
        self.assertEqual(2,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'cores-locked')
        self.assertEqual(result[0].value, 0)
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].name, 'cores-locked')
        self.assertEqual(result[1].value, 0)

if __name__ == '__main__':
    unittest.main()