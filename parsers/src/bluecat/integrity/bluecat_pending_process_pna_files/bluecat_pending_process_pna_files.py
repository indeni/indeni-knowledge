from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class BluecatPendingProcessPnaFiles(BaseParser):
    """
    Parser of:
        SSH: ls /data/notify/incoming | wc -l
    """
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data = helper_methods.parse_data_as_list(raw_data, 'pending_process_pna_files.textfsm')
            if data:
                self.write_double_metric('pna-files-pending', {}, 'gauge', int(data[0]['num_files']), True, 'PNA Files Pending to Process')
        return self.output