import os
import unittest
from bluecat.integrity.bluecat_pending_process_pna_files.bluecat_pending_process_pna_files import BluecatPendingProcessPnaFiles
from parser_service.public.action import *

class TestBluecatPendingProcessPnaFiles(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = BluecatPendingProcessPnaFiles()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))    
    
    def test_pending_files(self):
        result = self.parser.parse_file(self.current_dir + '/pending_files.input', {}, {})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'pna-files-pending')
        self.assertEqual(result[0].value, 345)

    def test_no_pending_files(self):
        result = self.parser.parse_file(self.current_dir + '/no_pending_files.input', {}, {})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'pna-files-pending')
        self.assertEqual(result[0].value, 0)

if __name__ == '__main__':
    unittest.main()