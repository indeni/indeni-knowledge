from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class ConfiguredSSHTimeout(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            try:
                data = helper_methods.parse_data_as_list(raw_data, 'sshd_conf_client_alive.textfsm')
                if int(data[0]['alive_count_max']) > 0:
                    # in miliseconds
                    timeout = int(data[0]['alive_interval']) * \
                        int(data[0]['alive_count_max'])*1000
                else:
                    timeout = 300000  # 300 seconds in milliseconds
                self.write_double_metric('ssh-timeout', {}, 'gauge', timeout, False)
            except:
                pass
        return self.output