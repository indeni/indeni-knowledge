import os
import unittest
from bluecat.integrity.bluecat_ssh_client_timeout.bluecat_ssh_client_timeout import ConfiguredSSHTimeout


class TestConfiguredSSHTimeout(unittest.TestCase):

    def setUp(self):
        self.parser = ConfiguredSSHTimeout()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_configured_ssh_time_out(self):
        result = self.parser.parse_file(self.current_dir + '/configured_ssh_time_out.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'ssh-timeout')
        self.assertEqual(result[0].value, 600000)

    def test_configured_ssh_time_out_no_alive_internal(self):
        result = self.parser.parse_file(self.current_dir + '/configured_ssh_time_out_no_alive_internal.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'ssh-timeout')
        self.assertEqual(result[0].value, 0)

    def test_configured_ssh_time_out_no_alive_count_max(self):
        result = self.parser.parse_file(self.current_dir + '/configured_ssh_time_out_no_alive_count_max.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'ssh-timeout')
        self.assertEqual(result[0].value, 300000)

    def test_configured_configured_ssh_time_out_no_data(self):
        result = self.parser.parse_file(self.current_dir + '/configured_ssh_time_out_no_data.input', {}, {})
        self.assertEqual(0, len(result))

if __name__ == '__main__':
    unittest.main()