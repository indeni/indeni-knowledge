import os
import unittest
from bluecat.integrity.bluecat_interrogation_cloud_id.bluecat_interrogation_cloud_id import BluecatInterrogationCloudId

class TestBluecatInterrogationCloudId(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = BluecatInterrogationCloudId()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))
        
    def test_interrogation_blank_cloud_id(self):
        result = self.parser.parse_file(self.current_dir + '/cloud_id_empty.input', {}, {})
        self.assertIsNone(result)

    def test_interrogation_googlecloud_cloud_id(self):
        result = self.parser.parse_file(self.current_dir + "/cloud_id_gce.input", {}, {})
        self.assertEqual(1,len(result))
        self.assertEqual('WriteTag', result[0].action_type)
        self.assertEqual("model",result[0].key)
        self.assertEqual("Google Cloud",result[0].value)

    def test_interrogation_azure_cloud_id(self):
        result = self.parser.parse_file(self.current_dir + "/cloud_id_azure.input", {}, {})        
        self.assertEqual(1,len(result))
        self.assertEqual('WriteTag', result[0].action_type)
        self.assertEqual("model",result[0].key)
        self.assertEqual("Azure",result[0].value)
        
    def test_interrogation_aws_cloud_id(self):
        result = self.parser.parse_file(self.current_dir + "/cloud_id_aws.input", {}, {})
        self.assertEqual(1,len(result))
        self.assertEqual('WriteTag', result[0].action_type)
        self.assertEqual("model",result[0].key)
        self.assertEqual("AWS",result[0].value)

if __name__ == '__main__':
    unittest.main()