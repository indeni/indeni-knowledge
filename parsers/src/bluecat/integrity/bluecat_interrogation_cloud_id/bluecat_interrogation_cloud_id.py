from parser_service.public.base_parser import BaseParser


class BluecatInterrogationCloudId(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            cloud_id_code = raw_data.strip()
            match cloud_id_code:
                case 'aws':
                    self.write_tag("model", 'AWS')
                case 'azure':
                    self.write_tag("model", 'Azure')
                case 'gce':
                    self.write_tag("model", 'Google Cloud')
            return self.output