from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser
class BluecatProcessesStates(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        CRITICAL_PROCESS = {
            'bam' : {
                'all' : [
                    'postgres: checkpointer',
                    'postgres: checkpointer',
                    'postgres: background writer',
                    'postgres: stats collector',
                    'postmaster -D /data/pgdata',
                    '-XX:StackShadowPages=20 -XX:+UseG1GC -Dsun.rmi.dgc.client.gcInterval=3600000'
                    ],
                'primary' : [
                    'postgres: walwriter',
                    'postgres: autovacuum launcher',
                    'postgres: logical replication launcher'
                    ],
            },
            'bdds' : {
                'all' : [],
                'role-dns': {
                    'primary':[
                        '/usr/local/sbin/named -t /replicated/jail/named'
                        ]
                },
                'role-dhcp':{
                    'all':[
                        [
                            '/usr/local/bluecat/dhcpmond', 
                            '/usr/local/bluecat/dhcpmon '  ## BCIA 9.4
                        ] ## This list will later be changed to just a str value
                    ],
                }
            }
        }
        if raw_data:
            # Parsing for whole ps aux output:
            ps_parse_dict = helper_methods.parse_data_as_list(raw_data, 'bluecat_ps_auxf.textfsm')
            if ps_parse_dict:
                process_present = {}
                process_monitor = CRITICAL_PROCESS[device_tags.get('os.name')]['all']
                if ps_parse_dict[0].get('dns_enabled') == '1':
                    if 'ACTIVE' in ps_parse_dict[0].get('node_state'):
                        process_monitor += CRITICAL_PROCESS['bdds']['role-dns']['primary']

                if ps_parse_dict[0].get('dhcp_enabled') == '1' and 'ACTIVE' in ps_parse_dict[0].get('node_state'):
                    process_monitor += CRITICAL_PROCESS['bdds']['role-dhcp']['all']
                if ps_parse_dict[0].get('bam_role') == '1':
                    process_monitor += CRITICAL_PROCESS['bam']['primary']
                for process in ps_parse_dict:
                    for critical_process in process_monitor:
                        if type(critical_process) is list:
                            for optional_critical_process in critical_process:
                                if optional_critical_process in process['COMMAND']:
                                    process_present[optional_critical_process] = {
                                        'CPU': process.get('CPU'), 'MEM': process.get('MEM')}
                                    process_monitor[process_monitor.index(
                                        critical_process)] = optional_critical_process ##replacing the list with the optional process that is present
                        elif critical_process in process['COMMAND']:
                            process_present[critical_process] = {
                                'CPU': process.get('CPU'), 'MEM': process.get('MEM')}
                for critical_process in process_monitor:
                    tags = {}
                    if 'StackShadowPages' in critical_process:
                        tags['name'] = 'java'
                    else:
                        tags['name'] = critical_process
                    if critical_process in process_present:
                        self.write_double_metric('process-state', tags, 'gauge', 1, True, 'Critical Process(es)', 'state', 'name')
                        self.write_double_metric(
                            'process-cpu', tags, 'gauge', float(process_present[critical_process].get('CPU')), False)
                        self.write_double_metric(
                            'process-memory', tags, 'gauge', float(process_present[critical_process].get('MEM')), False)
                    else:
                        self.write_double_metric('process-state', tags, 'gauge', 0, True, 'Critical Process(es)', 'state', 'name')
        return self.output