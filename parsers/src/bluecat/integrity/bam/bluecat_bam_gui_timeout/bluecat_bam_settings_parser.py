from parser_service.public.base_parser import BaseParser
from parser_service.public.helper_methods import parse_data_as_json


class BAMSettingsParser(BaseParser):
    """
    Checks if the BAM GUI Timeout.
    """
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            bam_response = parse_data_as_json(raw_data)
            data = bam_response.get('data')
            if data is not None:
                global_configuration = next(
                    (configuration for configuration in data if configuration["type"] == "GlobalSettings"), None)
                if global_configuration:
                    self.write_dynamic_variable('global_configuration_id', str(global_configuration['id']))
        return self.output
