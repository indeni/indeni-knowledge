import unittest
from unittest.mock import Mock
from bluecat.integrity.bam.bluecat_bam_gui_timeout.bluecat_bam_settings_parser import BAMSettingsParser


class TestBluecatBamGenerateToken(unittest.TestCase):
    def setUp(self):
        self.parser = BAMSettingsParser()
        self.parser.write_dynamic_variable = Mock()

    def test_parse_with_valid_id(self):
        raw_data = """{
              "count": 9,
              "data": [
                {
                  "type": "SystemSettings",
                  "id": 1,
                  "hostname": "new",
                  "version": "9.5.0-644.GA.bcn",
                  "address": "10.244.134.188",
                  "interfaceRedundancyEnabled": false,
                  "activeSessions": 1,
                  "_links": {
                    "self": {
                      "href": "/api/v2/settings/1"
                    },
                    "collection": {
                      "href": "/api/v2/settings"
                    },
                    "up": {
                      "href": "/api/v2/1"
                    }
                  }
                },
                {
                  "type": "GlobalSettings",
                  "id": 2,
                  "mandatoryCommentsEnabled": false,
                  "rememberLastAddressEnabled": false,
                  "sessionTimeout": "PT10M",
                  "disclaimerEnabled": false,
                  "disclaimerText": null,
                  "customReverseZoneFormatsAllowed": false,
                  "_links": {
                    "self": {
                      "href": "/api/v2/settings/2"
                    },
                    "collection": {
                      "href": "/api/v2/settings"
                    },
                    "up": {
                      "href": "/api/v2/1"
                    }
                  }
                },
                {
                  "type": "AuditDataSettings",
                  "id": 3,
                  "administrativeHistoryRetentionPeriod": null,
                  "sessionAndEventsRetentionPeriod": null,
                  "dhcpTransactionRetentionPeriod": null,
                  "ddnsTransactionRetentionPeriod": null,
                  "exportEnabled": false,
                  "destination": null,
                  "_links": {
                    "self": {
                      "href": "/api/v2/settings/3"
                    },
                    "collection": {
                      "href": "/api/v2/settings"
                    },
                    "up": {
                      "href": "/api/v2/1"
                    }
                  }
                },
                {
                  "type": "SAMLServiceProviderSettings",
                  "id": 4,
                  "addressManagerFqdn": null,
                  "metadataUrl": null,
                  "consumeUrl": null,
                  "logoutUrl": null,
                  "nameIdFormat": null,
                  "signingEnabled": true,
                  "encryptionEnabled": false,
                  "organizationName": null,
                  "organizationUrl": null,
                  "contactName": null,
                  "contactEmail": null,
                  "_links": {
                    "self": {
                      "href": "/api/v2/settings/4"
                    },
                    "collection": {
                      "href": "/api/v2/settings"
                    },
                    "up": {
                      "href": "/api/v2/1"
                    }
                  }
                },
                {
                  "type": "PasswordPolicySettings",
                  "id": 5,
                  "enabled": false,
                  "minLength": null,
                  "maxLength": null,
                  "digitRequired": false,
                  "mixedCaseRequired": false,
                  "specialCharacterRequired": false,
                  "_links": {
                    "self": {
                      "href": "/api/v2/settings/5"
                    },
                    "collection": {
                      "href": "/api/v2/settings"
                    },
                    "up": {
                      "href": "/api/v2/1"
                    }
                  }
                },
                {
                  "type": "LoginPolicySettings",
                  "id": 6,
                  "enabled": false,
                  "failureLimit": 5,
                  "failureLimitPeriod": "PT5M",
                  "delayDuration": "PT30M",
                  "_links": {
                    "self": {
                      "href": "/api/v2/settings/6"
                    },
                    "collection": {
                      "href": "/api/v2/settings"
                    },
                    "up": {
                      "href": "/api/v2/1"
                    }
                  }
                },
                {
                  "type": "LogLevelSettings",
                  "id": 7,
                  "addressManager": "WARN",
                  "restV2": "INFO",
                  "zoneImport": "WARN",
                  "reconciliationService": "WARN",
                  "discoveryEngine": "WARN",
                  "snmpClient": "WARN",
                  "monitoringService": "WARN",
                  "rrdTool": "WARN",
                  "_links": {
                    "self": {
                      "href": "/api/v2/settings/7"
                    },
                    "collection": {
                      "href": "/api/v2/settings"
                    },
                    "up": {
                      "href": "/api/v2/1"
                    }
                  }
                },
                {
                  "type": "SSOEnforcementSettings",
                  "id": 8,
                  "enabled": false,
                  "samlIdentityProviderEnabled": false,
                  "nonSsoAuthenticatorCount": 1,
                  "nonSsoGroupCount": 2,
                  "localAdminUserCount": 4,
                  "localNonAdminUserCount": 0,
                  "_links": {
                    "self": {
                      "href": "/api/v2/settings/8"
                    },
                    "collection": {
                      "href": "/api/v2/settings"
                    },
                    "up": {
                      "href": "/api/v2/1"
                    }
                  }
                },
                {
                  "type": "MonitoringSettings",
                  "id": 9,
                  "addressManagerMonitoring": {
                    "enabled": false,
                    "pollingInterval": "PT5M",
                    "address": null,
                    "snmp": null
                  },
                  "serverMonitoring": {
                    "enabled": false,
                    "pollingInterval": "PT5M",
                    "statusRefreshInterval": "PT20S",
                    "failureDetectionThreshold": 2
                  },
                  "f5ServerMonitoring": {
                    "enabled": false,
                    "pollingInterval": "PT5M"
                  },
                  "_links": {
                    "self": {
                      "href": "/api/v2/settings/9"
                    },
                    "collection": {
                      "href": "/api/v2/settings"
                    },
                    "up": {
                      "href": "/api/v2/1"
                    }
                  }
                }
              ]
            }"""
        output = self.parser.parse(raw_data, {}, {})
        self.parser.write_dynamic_variable.assert_called_once_with('global_configuration_id', '2')
        self.assertEqual(output, self.parser.output)


if __name__ == '__main__':
    unittest.main()

