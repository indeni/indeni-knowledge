import os
import unittest
from bluecat.integrity.bam.bluecat_bam_gui_timeout.bluecat_bam_generate_token import BluecatBamGenerateToken


class TestBluecatBamGenerateToken(unittest.TestCase):
    def setUp(self):
        self.parser = BluecatBamGenerateToken()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_parse_with_token(self):
        result = self.parser.parse_file(self.current_dir + '/generate_api_token_ok.json', {}, {})
        self.assertEqual(2,len(result))
        self.assertEqual(result[0].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[0].key, 'token')
        self.assertEqual(result[0].value, 'token_value')
        self.assertEqual(result[1].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[1].key, 'api_session_id')
        self.assertEqual(result[1].value, '3200')

    def test_generate_api_token_nok_invalid_username_or_password(self):
        result = self.parser.parse_file(self.current_dir + '/generate_api_token_nok_invalid_username_or_password.json', {}, {})
        self.assertEqual(0,len(result))


if __name__ == '__main__':
    unittest.main()

