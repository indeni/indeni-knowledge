import unittest
from unittest.mock import Mock

from bluecat.integrity.bam.bluecat_bam_gui_timeout.bluecat_bam_gui_timeout_parser import BAMTimeoutGUICheck, TIMEOUT_OPTIONS


class TestBAMTimeoutGUICheck(unittest.TestCase):

    def setUp(self):
        self.bam_check = BAMTimeoutGUICheck()
        self.bam_check.write_double_metric = Mock()

    def test_parse_timeout_exists(self):
        raw_data = """
            {
              "type": "GlobalSettings",
              "id": 2,
              "mandatoryCommentsEnabled": false,
              "rememberLastAddressEnabled": false,
              "sessionTimeout": "PT10M",
              "disclaimerEnabled": false,
              "disclaimerText": null,
              "customReverseZoneFormatsAllowed": false,
              "_links": {
                "self": {
                  "href": "/api/v2/settings/2"
                },
                "collection": {
                  "href": "/api/v2/settings"
                },
                "up": {
                  "href": "/api/v2/1"
                }
              }
            }
        """
        dynamic_var = {}
        device_tags = {}
        self.bam_check.parse(raw_data, dynamic_var, device_tags)
        args = self.bam_check.write_double_metric.call_args.args
        self.assertEqual(args[0], 'web-timeout')
        self.assertEqual(args[2], 'gauge')
        self.assertEqual(args[3], TIMEOUT_OPTIONS["PT10M"])
        self.bam_check.write_double_metric.assert_called()


if __name__ == '__main__':
    unittest.main()

