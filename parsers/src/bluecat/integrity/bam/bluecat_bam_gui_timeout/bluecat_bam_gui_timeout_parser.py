from parser_service.public.base_parser import BaseParser
from parser_service.public.helper_methods import parse_data_as_json



TIMEOUT_OPTIONS = { #in miliseconds
    "P1D": 86400000.0,
    "PT30M": 1800000.0,
    "PT20M": 1200000.0,
    "PT10M": 600000.0,
    "PT5M": 300000.0
}


class BAMTimeoutGUICheck(BaseParser):
    """
    Extracts the BAM GUI Session Timeout
    Rule:
    """
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        data = parse_data_as_json(raw_data)
        extracted_timeout = None
        if data:
            session_timeout = data.get('sessionTimeout')
            if session_timeout:
                # Extracting just the numeric value from the session time timeout string
                self.write_double_metric(
                    'web-timeout',
                    {},
                    'gauge',
                    int(TIMEOUT_OPTIONS[session_timeout]),
                    False,
                )
        return self.output
