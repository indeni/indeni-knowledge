from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser
from datetime import datetime
from zoneinfo import ZoneInfo

class BluecatCertificateNotValid(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data = helper_methods.parse_data_as_list(raw_data, 'bluecat_certificate_expiration.textfsm')
            if data:
                time_format = '%b %d %H:%M:%S %Y %Z'
                expiry_time = data[0].get('notAfter_date')
                expiry_object = datetime.strptime(expiry_time, time_format)
                expiry_time_epoch = datetime.timestamp(expiry_object.replace(tzinfo=ZoneInfo('GMT+0')))
                self.write_double_metric('certificate-expiration', {'name': 'bam'}, 'gauge', expiry_time_epoch, True, "Certificate expiration", "date")
        return self.output