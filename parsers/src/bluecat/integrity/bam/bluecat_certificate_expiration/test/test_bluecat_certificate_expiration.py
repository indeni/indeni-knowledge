import os
import unittest
from bluecat.integrity.bam.bluecat_certificate_expiration.bluecat_certificate_expiration import BluecatCertificateNotValid
from parser_service.public.action import *

class TestBluecatCertificateNotValid(unittest.TestCase):
    def setUp(self):
        # Arrange
        self.parser = BluecatCertificateNotValid()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_bluecat_certificate_valid(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_certificate.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'certificate-expiration')
        self.assertEqual(result[0].tags['name'], 'bam')
        self.assertEqual(result[0].tags['live-config'], 'true')
        self.assertEqual(result[0].tags['display-name'], 'Certificate expiration')
        self.assertEqual(result[0].tags['im.dstype.displayType'], 'date')
        self.assertEqual(result[0].value, 1723048899.0)

if __name__ == '__main__':
    unittest.main()