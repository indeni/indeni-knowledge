from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class BluecatBamToBamConnectivity(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            parsed_data = helper_methods.parse_data_as_list(raw_data,
                                                            'bluecat_bam_to_bam_connectivity.textfsm')
            if parsed_data:
                for connectivity_test in parsed_data:
                    tags = {}
                    tags['name'] = f"{connectivity_test['peer']} (TCP {connectivity_test['port']})"
                    self.write_double_metric('bam-to-bam-connection-status', tags, 'gauge',
                                             1 if ('open' or 'succeeded') in connectivity_test['status'] else 0,
                                             True, 'Connectivity - BAM to BAM', 'state', 'name')
        return self.output
