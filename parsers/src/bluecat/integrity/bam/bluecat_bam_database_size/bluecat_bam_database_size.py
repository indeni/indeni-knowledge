from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

units = {
    'GB': 1000000000,
    'MB': 1000000,
    'kB': 1000,
    'bytes': 1,
}

vendors = {
    'bluecat': 'proteusdb',
}

class BluecatBamDatabaseSize(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            size_data = helper_methods.parse_data_as_list(raw_data, 'psql_database_size.textfsm')
            if size_data :
                device_vendor = device_tags.get('vendor')
                for db in size_data:
                    if vendors[device_vendor] == db['database_name']:
                        tags = {}
                        tags['name'] = db['database_name']
                        size_data_mb = float(db['size'])*(units[db['units']])
                        self.write_double_metric('psql-database-size', tags, 'gauge', size_data_mb, True, 'PSQL database size', 'bytes', 'name')
        return self.output