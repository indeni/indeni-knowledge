import os
import unittest
from bluecat.integrity.bam.bluecat_bam_database_size.bluecat_bam_database_size import BluecatBamDatabaseSize
from parser_service.public.action import *

class TestBluecatBamDatabaseSize(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = BluecatBamDatabaseSize()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))    
    
    def test_bam_database_mb(self):
        result = self.parser.parse_file(self.current_dir + '/bam_database_mb.input', {}, {'vendor': 'bluecat'})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'psql-database-size')
        self.assertEqual(result[0].value, 351000000.0)
        self.assertEqual(result[0].tags['name'],'proteusdb')


if __name__ == '__main__':
    unittest.main()