import unittest
from bluecat.integrity.bam.bluecat_bam_database_table_size.bluecat_bam_database_table_size import BluecatBamDatabaseTableSize
from parser_service.public.action import *


class TestBluecatBamDatabaseSize(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = BluecatBamDatabaseTableSize()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_bam_database_table_size(self):
        result = self.parser.parse_file(self.current_dir + '/bam_database_table_size.input', {}, {'vendor': 'bluecat'})
        self.assertEqual(1, len(result))
        # The first table we are writing a specific rule for is audit_queue.  We can add others later.
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'bam-database-audit-queue-table-size')
        self.assertEqual(result[0].value, 16000.0)
        self.assertEqual(result[0].tags['name'], 'audit_queue')


if __name__ == '__main__':
    unittest.main()