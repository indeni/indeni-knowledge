from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

units = {
    'GB': 1000000000,
    'MB': 1000000,
    'kB': 1000,
    'bytes': 1,
}


class BluecatBamDatabaseTableSize(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            size_data = helper_methods.parse_data_as_list(raw_data, 'bam_database_table_size.textfsm')
            if size_data:
                for table in size_data:
                    tags = {'name': table['database_table_name']}
                    size_data_b = float(table['size']) * (units[table['units']])
                    # To add alerts for other tables, write a new metric with a different name
                    # (don't forget to add it to the ind.yaml), and a new rule.
                    if table['database_table_name'] == 'audit_queue':
                        self.write_double_metric('bam-database-audit-queue-table-size', tags, 'gauge', size_data_b, False)
        return self.output
