from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class BluecatBamToBddsConnectivity(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data_parsed = helper_methods.parse_data_as_list(raw_data, 'bluecat_bam_to_bdds_connectivity.textfsm')
            if data_parsed:
                tags = {}
                tags['name'] = dynamic_var['server_name']
                self.write_double_metric('bam-to-bdds-connectivity-status', tags, 'gauge', 1 if ('open' or 'succeeded') in data_parsed[0]['connectivity_result'] else 0,
                                         True, 'Connectivity - BAM to BDDS', 'state', 'name')
        return self.output