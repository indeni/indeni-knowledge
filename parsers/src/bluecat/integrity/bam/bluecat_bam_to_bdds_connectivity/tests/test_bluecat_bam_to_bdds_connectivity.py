import os
import unittest
from bluecat.integrity.bam.bluecat_bam_to_bdds_connectivity.bluecat_bam_generate_token import BluecatBamGenerateToken
from bluecat.integrity.bam.bluecat_bam_to_bdds_connectivity.bluecat_bam_collect_servers_urls import BluecatBamCollectserversUrls
from bluecat.integrity.bam.bluecat_bam_to_bdds_connectivity.bluecat_bam_to_bdds_connectivity import BluecatBamToBddsConnectivity
from parser_service.public.action import *

class TestBluecatBamGenerateToken(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser_step1 = BluecatBamGenerateToken()
        self.parser_step2 = BluecatBamCollectserversUrls()
        self.parser_step3 = BluecatBamToBddsConnectivity()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))    
    
    def test_generate_api_token_ok(self):
        result = self.parser_step1.parse_file(self.current_dir + '/generate_api_token_ok.json', {}, {})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[0].key, 'token')
        self.assertEqual(result[0].value, 'anJpdmVpcm9mZXJuYW5kZXo6bWVHWUFHZlMrUmFNZFFMRmo1a0tpZWxVVS9hRjlhQ09LaDFpU3ZPZg==')

    def test_generate_api_token_nok_invalid_username_or_password(self):
        result = self.parser_step1.parse_file(self.current_dir + '/generate_api_token_nok_invalid_username_or_password.json', {}, {})
        self.assertEqual(0,len(result))

    def test_api_configurations_embed_servers_servers(self):
        result = self.parser_step2.parse_file(self.current_dir + '/api_configurations_embed_servers_servers.json', {'token':'anJpdmVpcm9mZXJuYW5kZXo6bWVHWUFHZlMrUmFNZFFMRmo1a0tpZWxVVS9hRjlhQ09LaDFpU3ZPZg=='}, {})
        self.assertEqual(30,len(result))
        self.assertEqual(result[0].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[0].key, 'server_id')
        self.assertEqual(result[0].value, '100974')
        self.assertEqual(result[1].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[1].key, 'token')
        self.assertEqual(result[1].value, 'anJpdmVpcm9mZXJuYW5kZXo6bWVHWUFHZlMrUmFNZFFMRmo1a0tpZWxVVS9hRjlhQ09LaDFpU3ZPZg==')
        self.assertEqual(result[2].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[2].key, 'server_id')
        self.assertEqual(result[2].value, '100976')
        self.assertEqual(result[4].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[4].key, 'server_id')
        self.assertEqual(result[4].value, '101070')        

    def test_nc_port_open(self):
        result = self.parser_step3.parse_file(self.current_dir + '/nc_port_open.input', {'server_name': 'BDDS03'}, {})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].tags['name'],'BDDS03')
        self.assertEqual(result[0].tags['display-name'],'Connectivity - BAM to BDDS')
        self.assertEqual(result[0].value,1)

    def test_nc_connection_refused(self):
        result = self.parser_step3.parse_file(self.current_dir + '/nc_connection_refused.input', {'server_name': 'BDDS03'}, {})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].tags['name'],'BDDS03')
        self.assertEqual(result[0].tags['display-name'],'Connectivity - BAM to BDDS')
        self.assertEqual(result[0].value,0)

if __name__ == '__main__':
    unittest.main()