from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class BluecatBamCollectserversInterfaceDetails(BaseParser):

    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data_parsed = helper_methods.parse_data_as_json(raw_data)
            if data_parsed:
                self.write_dynamic_variable('server_name', data_parsed['data'][0]['server']['name'])
                self.write_dynamic_variable('server_mgmt_ip', data_parsed['data'][0]['managementAddress'])
                self.write_dynamic_variable('token', dynamic_var['token'])
        return self.output


