from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class BluecatBamGenerateToken(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data = helper_methods.parse_data_as_json(raw_data)
            if data:
                if data.get('basicAuthenticationCredentials') is not None:
                    self.write_dynamic_variable('token', data['basicAuthenticationCredentials'])
        return self.output
