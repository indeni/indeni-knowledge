from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class BluecatBamCollectserversUrls(BaseParser):

    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data_parsed = helper_methods.parse_data_as_json(raw_data)
            if data_parsed:
                for configuration in data_parsed['data']:
                    for server in configuration['_embedded']['servers']:
                        if server['connected'] == True:
                            if server['type'] == 'Server':
                                server_id= server['_links']['interfaces']['href'].split('/')[-2]
                                self.write_dynamic_variable('server_id', server_id)
                                self.write_dynamic_variable('token', dynamic_var['token'])
                            elif server['type'] == 'HAServer':
                                for ha_member in server['_embedded']['servers']:
                                    if ha_member.get('type') == 'Server':
                                        server_id= ha_member['_links']['interfaces']['href'].split('/')[-2]
                                        self.write_dynamic_variable('server_id', server_id)
                                        self.write_dynamic_variable('token', dynamic_var['token'])
        return self.output
