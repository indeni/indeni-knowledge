from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser
from datetime import datetime

class BluecatBackupFailed(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            parsed_data_backup = helper_methods.parse_data_as_list(raw_data, 'bluecat_show_backup.textfsm')
            if parsed_data_backup:
                if (parsed_data_backup[0]['ip_device'] == parsed_data_backup[0]['primary_ip']) or (not parsed_data_backup[0]['primary_ip']):
                    self.write_double_metric('backup-not-enabled', {}, 'gauge', 1 if parsed_data_backup[0]['status'] == 'Disabled' else 0, False)
                    self.write_double_metric('backup-not-configured', {}, 'gauge', 1 if len(parsed_data_backup) == 1 else 0, False)
                    remote_backup_not_configured = 1
                    unsecure_protocol = 0
                    for backup_data in [x for x in parsed_data_backup if len(x['id']) > 0 ]:
                        if backup_data['remote_backup'] == 'yes':
                            remote_backup_not_configured = 0
                        if backup_data['remote_backup'] == 'yes' and backup_data['protocol'] == 'ftp':
                            unsecure_protocol = 1
                    self.write_double_metric('remote-backup-not-configured', {}, 'gauge', remote_backup_not_configured, False)
                    self.write_double_metric('unsecure-protocol', {}, 'gauge', unsecure_protocol, False)

            parsed_data_syslog = helper_methods.parse_data_as_list(raw_data, 'bluecat_backup_syslog.textfsm')
            if parsed_data_syslog and parsed_data_backup[0]['status'] != 'Disabled':
                time_format = '%b %d %H:%M:%S %Y'
                current_time = parsed_data_backup[0]['date_time'].split()
                for i in sorted([0,4] ,reverse=True):
                    del current_time[i]
                ct_object = datetime.strptime(' '.join(current_time), time_format)
                current_time_epoch = datetime.timestamp(ct_object)
                for syslog_data in parsed_data_syslog:
                    syslog_date = syslog_data['date']
                    syslog_date_split = syslog_date.split()
                    if current_time[0] == 'Jan' and syslog_date_split[0] in ['Dec']:
                        if current_time[0] == syslog_date[0]:
                            syslog_date += f" {current_time[3]}"
                        else:
                            syslog_date += f" {str(int(current_time[3])-1)}"
                    else:
                        syslog_date += f" {current_time[3]}"
                    backup_object = datetime.strptime(syslog_date, time_format)
                    backup_time_epoch = datetime.timestamp(backup_object)
                    delta_time = current_time_epoch - backup_time_epoch
                    if 'Backup failed' in syslog_data['message']:
                        self.write_double_metric('local-backup-failed', {}, 'gauge', delta_time, False)
                    elif 'Remote backup failed' in syslog_data['message']:
                        self.write_double_metric('remote-backup-failed', {}, 'gauge', delta_time, False)
                    elif 'A backup is still running' in syslog_data['message']:
                        self.write_double_metric('backup-overlapping', {}, 'gauge', delta_time, False)
        return self.output