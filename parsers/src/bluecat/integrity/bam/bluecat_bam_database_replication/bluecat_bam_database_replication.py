from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class BluecatBamDatabaseReplication(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            replication_state_data = helper_methods.parse_data_as_list(raw_data, 'bluecat_bam_database_replication_state.textfsm')
            replication_state_param = helper_methods.parse_data_as_list(raw_data, 'bluecat_bam_database_replication_param.textfsm')
            if replication_state_data and replication_state_param: #Cluster case
                local_ip_address = replication_state_data[0]['addresses'].split(" ")
                self.write_double_metric('database-is-primary', {}, 'gauge', 1 if int(replication_state_param[0]['replication_role']) == 1 else 0, False)
                self.write_double_metric('database-replication-enabled', {}, 'gauge', 1 if (replication_state_param[0]['cluster_state']).lower() in ['enabled', 'enrolling'] else 0, False)
                for node in replication_state_data:
                    if node['ip_addr'] not in local_ip_address:
                        tags = {}
                        tags ['name'] = node['hostname']
                        self.write_double_metric('database-replication-status', tags, 'gauge', 1 if (node['status'] == 'streaming' or node['role'] == 'primary') else 0,
                                                        True, 'Database Replication Status', 'state', 'name')
                        self.write_double_metric('database-replication-latency', tags, 'gauge', 0 if node['latency'] == 'null'  else int(node['latency']),
                                                        True, 'Database Replication Latency', 'number', 'name')
                        self.write_double_metric('database-replication-latency-threshold-warn', tags, 'gauge', int(replication_state_param[0]['latency_warning']), False)
                        self.write_double_metric('database-replication-latency-threshold-critical', tags, 'gauge', int(replication_state_param[0]['latency_critical']), False)
            
            data = helper_methods.parse_data_as_list(raw_data, 'bluecat_bam_large_accumulated_wal_files.textfsm')
            if data:
                if data[0]['max_wal_size']:
                    if int(data[0]['max_wal_size']) != 0:
                        if 'GB' in data[0]['units_max_wal_size']:
                            max_wal_size_critical = float(data[0]['max_wal_size']) * 1024
                        elif 'MB' in data[0]['units_max_wal_size']:
                            max_wal_size_critical = float(data[0]['max_wal_size'])
                        elif 'kB' in data[0]['units_max_wal_size']:
                            max_wal_size_critical = float(data[0]['max_wal_size']) / 1024
                        max_wal_size_warn = float(max_wal_size_critical/2)
                        wal_size_current = int(data[0]['file_size']) * len(data)
                        self.write_double_metric('wal-files-total-size', {}, 'gauge', wal_size_current, True, "Total size WAL files")
                        self.write_double_metric('wal-file-size-warn', {}, 'gauge', 1 if wal_size_current > max_wal_size_warn and wal_size_current < max_wal_size_critical else 0, False)
                        self.write_double_metric('wal-file-size-critical', {}, 'gauge', 1 if wal_size_current >= max_wal_size_critical else 0, False)
        return self.output