from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class BluecatBamKnownDevices(BaseParser):

    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data_parsed = helper_methods.parse_data_as_json(raw_data)
            if data_parsed:
                known_devices = []
                for configuration in data_parsed['data']:
                    for server in configuration['_embedded']['servers']:
                        if server['type'] == 'Server' :
                            host = {}
                            for interface in server['_embedded']['interfaces']:
                                if interface.get('managementAddress') is not None:
                                    host['name'] = interface['server']['name']
                                    host['ip'] = interface['managementAddress']
                                    known_devices.append(host)
                        elif server['type'] == 'HAServer':
                            for ha_node in server['_embedded']['servers']:
                                for node_interface in ha_node['_embedded']['interfaces']:
                                    if node_interface.get('managementAddress') is not None:
                                        host = {}
                                        host['name'] = node_interface['server']['name']
                                        host['ip'] = node_interface['managementAddress']
                                        known_devices.append(host)
                self.write_complex_metric_object_array('known-devices', {}, known_devices, False)
        return self.output


