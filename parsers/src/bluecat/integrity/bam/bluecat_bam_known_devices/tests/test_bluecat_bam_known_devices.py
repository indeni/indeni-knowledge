import os
import unittest
from bluecat.integrity.bam.bluecat_bam_known_devices.bluecat_bam_generate_token import BluecatBamGenerateToken
from bluecat.integrity.bam.bluecat_bam_known_devices.bluecat_bam_known_devices import BluecatBamKnownDevices

from parser_service.public.action import *

class TestBluecatBamGenerateToken(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser_step1 = BluecatBamGenerateToken()
        self.parser_step2 = BluecatBamKnownDevices()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))    
    
    def test_generate_api_token_ok(self):
        result = self.parser_step1.parse_file(self.current_dir + '/generate_api_token_ok.json', {}, {})
        self.assertEqual(2,len(result))
        self.assertEqual(result[0].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[0].key, 'token')
        self.assertEqual(result[0].value, 'anJpdmVpcm9mZXJuYW5kZXo6bWVHWUFHZlMrUmFNZFFMRmo1a0tpZWxVVS9hRjlhQ09LaDFpU3ZPZg==')
        self.assertEqual(result[1].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[1].key, 'api_session_id')
        self.assertEqual(result[1].value, '49371')

    def test_generate_api_token_nok_invalid_username_or_password(self):
        result = self.parser_step1.parse_file(self.current_dir + '/generate_api_token_nok_invalid_username_or_password.json', {}, {})
        self.assertEqual(0,len(result))

    def test_api_configurations_embed_servers_interfaces(self):
        result = self.parser_step2.parse_file(self.current_dir + '/api_configurations_embed_servers_interfaces.json', {'token':'anJpdmVpcm9mZXJuYW5kZXo6bWVHWUFHZlMrUmFNZFFMRmo1a0tpZWxVVS9hRjlhQ09LaDFpU3ZPZg=='}, {})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].action_type, 'WriteComplexMetric')
        self.assertEqual(result[0].name, 'known-devices')
        self.assertEqual(result[0].tags, {})
        
        self.assertEqual(result[0].value[0]['name'], 'BDDS05')
        self.assertEqual(result[0].value[0]['ip'], '10.255.253.155')
        self.assertEqual(result[0].value[1]['name'], 'BDDS06')
        self.assertEqual(result[0].value[1]['ip'], '10.255.253.156')
        self.assertEqual(result[0].value[2]['name'], 'BDDS03')
        self.assertEqual(result[0].value[2]['ip'], '192.168.0.10')
        self.assertEqual(result[0].value[3]['name'], 'BDDS04')
        self.assertEqual(result[0].value[3]['ip'], '192.168.0.11')
        self.assertEqual(result[0].value[4]['name'], 'bdds01.bluecatlabs.net')
        self.assertEqual(result[0].value[4]['ip'], '10.241.128.3')
        self.assertEqual(result[0].value[5]['name'], 'bdds02.bluecatlabs.net')
        self.assertEqual(result[0].value[5]['ip'], '10.241.128.4')
        self.assertEqual(result[0].value[6]['name'], 'BDDS07')
        self.assertEqual(result[0].value[6]['ip'], '10.49.10.10')
        self.assertEqual(result[0].value[7]['name'], 'BDDS08')
        self.assertEqual(result[0].value[7]['ip'], '172.17.7.10')
        self.assertEqual(result[0].value[8]['name'], 'BDDS09')
        self.assertEqual(result[0].value[8]['ip'], '10.49.10.11')
        self.assertEqual(result[0].value[9]['name'], 'BDDS10')
        self.assertEqual(result[0].value[9]['ip'], '172.17.7.11')
        self.assertEqual(result[0].value[10]['name'], 'BDDS11')
        self.assertEqual(result[0].value[10]['ip'], '10.49.10.12')
        self.assertEqual(result[0].value[11]['name'], 'BDDS12')
        self.assertEqual(result[0].value[11]['ip'], '172.17.7.12')
        self.assertEqual(result[0].value[12]['name'], 'BDDS13')
        self.assertEqual(result[0].value[12]['ip'], '192.168.0.12')
        self.assertEqual(result[0].value[13]['name'], 'BDDS01-HA-NODE1')
        self.assertEqual(result[0].value[13]['ip'], '10.255.253.151')
        self.assertEqual(result[0].value[14]['name'], 'BDDS02-HA-NODE2')
        self.assertEqual(result[0].value[14]['ip'], '10.255.253.152')
        self.assertEqual(result[0].value[15]['name'], 'BDDS14-HA-NODE1')
        self.assertEqual(result[0].value[15]['ip'], '192.168.0.13')
        self.assertEqual(result[0].value[16]['name'], 'BDDS15-HA-NODE2')
        self.assertEqual(result[0].value[16]['ip'], '192.168.0.15')
        self.assertEqual(result[0].value[17]['name'], 'BDDS16')
        self.assertEqual(result[0].value[17]['ip'], '192.168.0.16')
        self.assertEqual(result[0].value[18]['name'], 'BDDS17')
        self.assertEqual(result[0].value[18]['ip'], '192.168.0.17')
        self.assertEqual(result[0].value[19]['name'], 'BDDS18-HA-NODE1')
        self.assertEqual(result[0].value[19]['ip'], '192.168.0.20')
        self.assertEqual(result[0].value[20]['name'], 'BDDS19-HA-NODE2')
        self.assertEqual(result[0].value[20]['ip'], '192.168.0.19')
        self.assertEqual(result[0].value[21]['name'], 'ADC01')
        self.assertEqual(result[0].value[21]['ip'], '10.255.253.153')
        self.assertEqual(result[0].value[22]['name'], 'micetro01')
        self.assertEqual(result[0].value[22]['ip'], '172.17.10.10')
if __name__ == '__main__':
    unittest.main()