from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class BluecatBddsServerDeployment(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data_parsed = helper_methods.parse_data_as_json(raw_data)

            if data_parsed:
                service_deployments = {}
                last_service_deployment = {}
                unsuccessful_deployments = {}
                issues = []

                for deployment in data_parsed['data']:
                    if deployment['service'] == 'DHCPv4':
                        tags = {'name': "DHCPv4 Deployments"}
                    elif deployment['service'] == 'DHCPv6':
                        tags = {'name': "DHCPv6 Deployments"}
                    elif deployment['service'] == 'DNS':
                        tags = {'name': "DNS Deployments"}
                    elif deployment['service'] == 'TFTP':
                        tags = {'name': "TFTP Deployments"}

                    server_id = deployment['_links']['up']['href'].split('/')[-1]
                    unsuccessful_deployments[server_id] = {}
                    last_service_deployment[server_id] = {}

                    try:
                        service_deployments[server_id].append(deployment)
                    except KeyError:
                        service_deployments[server_id] = [deployment]

                for server_id, deployments in service_deployments.items():
                    last_service_deployment[server_id] = deployments[-1]

                for server_id, last_deployment in last_service_deployment.items():
                    if last_deployment['state'] in ('FAILED', 'CANCELLED', 'CANCELLING',
                                                    'COMPLETED_WITH_WARNINGS', 'UNKNOWN'):
                        if "Role is not defined" not in last_deployment['message']:
                            unsuccessful_deployments[server_id] = last_deployment

                if unsuccessful_deployments:
                    for server_id, unsuccessful_deployment in unsuccessful_deployments.items():
                        if unsuccessful_deployment:
                            issues = self.__format_results(issues, unsuccessful_deployment)
                    if issues:
                        self.write_complex_metric_object_array('bdds-deployment-failure', tags, issues, False)
                    else:
                        self.write_complex_metric_object_array('bdds-deployment-failure', tags, [], False)

        return self.output

    def __format_results(self, issues, unsuccessful_deployment):
        server_name = unsuccessful_deployment['message'].split('.')[1].split('(')[0].strip()
        issues.append({f"{server_name}": f"{unsuccessful_deployment['service']} {unsuccessful_deployment['state']}: {unsuccessful_deployment['message']}"})
        return issues
