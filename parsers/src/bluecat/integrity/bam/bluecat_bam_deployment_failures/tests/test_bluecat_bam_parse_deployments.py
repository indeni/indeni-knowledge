import os
import unittest
from bluecat.integrity.bam.bluecat_bam_deployment_failures.bluecat_bam_parse_deployments import BluecatBddsServerDeployment
from bluecat.integrity.bam.bluecat_bam_deployment_failures.bluecat_bam_generate_token import BluecatBamGenerateToken
from parser_service.public.action import *


class TestBluecatBddsServerDeployment(unittest.TestCase):
    def setUp(self):
        self.parser_step1 = BluecatBamGenerateToken()
        self.parser_step2 = BluecatBddsServerDeployment()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_generate_api_token_ok(self):
        result = self.parser_step1.parse_file(self.current_dir + '/generate_api_token_ok.json', {}, {})
        self.assertEqual(2, len(result))
        self.assertEqual(result[0].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[0].key, 'token')
        self.assertEqual(result[0].value, 'anJpdmVpcm9mZXJuYW5kZXo6bWVHWUFHZlMrUmFNZFFMRmo1a0tpZWxVVS9hRjlhQ09LaDFpU3ZPZg==')
        self.assertEqual(result[1].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[1].key, 'api_session_id')
        self.assertEqual(result[1].value, '49371')

    def test_generate_api_token_nok_invalid_username_or_password(self):
        result = self.parser_step1.parse_file(self.current_dir + '/generate_api_token_nok_invalid_username_or_password.json', {}, {})
        self.assertEqual(0, len(result))

    def test_bluecat_dns_deployment_failed(self):
        result = self.parser_step2.parse_file(self.current_dir + '/dns_deployment_failed.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].tags['name'], 'DNS Deployments')
        self.assertEqual(list(result[0].value[0].keys())[0], 'bdds1')
        self.assertEqual(result[0].value[0]['bdds1'], "DNS FAILED: Deployment Finished. bdds1 ( Manual Deployment - Analyzing ) : DNS deployment failed: Exception occurred during server deployment. Started at 22-Mar-2024 14:07:54")

    def test_bluecat_dns_deployment_success(self):
        result = self.parser_step2.parse_file(self.current_dir + '/dns_deployment_success.input', {}, {})
        self.assertEqual(result[0].tags['name'], 'DNS Deployments')
        self.assertEqual(0, len(result[0].value))

    def test_bluecat_dhcp_deployment_failed(self):
        result = self.parser_step2.parse_file(self.current_dir + '/dhcp_deployment_failed.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].tags['name'], 'DHCPv4 Deployments')
        self.assertEqual(2, len(list(result[0].value)))
        self.assertEqual(list(result[0].value[0].keys())[0], 'bdds2')
        self.assertEqual(list(result[0].value[1].keys())[0], 'bdds1')
        self.assertEqual(result[0].value[0]['bdds2'], "DHCPv4 FAILED: Deployment Finished. bdds2 ( Manual Deployment - Full Deployment ) : DHCP not deployed. Started at 22-Mar-2024 14:07:54")
        self.assertEqual(result[0].value[1]['bdds1'], "DHCPv4 FAILED: Deployment Finished. bdds1 ( Manual Deployment - Full Deployment ) : DHCP not deployed. Started at 22-Mar-2024 14:07:54")

    def test_bluecat_dhcp_deployment_success(self):
        result = self.parser_step2.parse_file(self.current_dir + '/dhcp_deployment_success.input', {}, {})
        self.assertEqual(result[0].tags['name'], 'DHCPv4 Deployments')
        self.assertEqual(0, len(result[0].value))

    def test_bluecat_tftp_deployment_failed(self):
        result = self.parser_step2.parse_file(self.current_dir + '/tftp_deployment_failed.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].tags['name'], 'TFTP Deployments')
        self.assertEqual(list(result[0].value[0].keys())[0], 'bdds1')
        self.assertEqual(result[0].value[0]['bdds1'], "TFTP FAILED: Deployment Finished. bdds1 ( Manual Deployment - Full Deployment ) : TFTP not deployed. Started at 26-Mar-2024 17:32:41")

    def test_bluecat_tftp_deployment_success(self):
        result = self.parser_step2.parse_file(self.current_dir + '/tftp_deployment_success.input', {}, {})
        self.assertEqual(result[0].tags['name'], 'TFTP Deployments')
        self.assertEqual(0, len(result[0].value))
