dmesg_output=$(nice -n 15 dmesg --level=err,warn --time-format iso | awk -v ten_minutes_ago="$(date -d '10 minutes ago' +'%Y-%m-%dT%H:%M:%S%z')" '{gsub(/,[0-9]+/, "", $1); timestamp = gensub(/\[|\]/, "", "g", $1); if (timestamp >= ten_minutes_ago) print}') && [ $? -eq 0 ] && matches=$(echo "$dmesg_output" | grep ".*EXT4-fs (\S*): .*error.*") && [ -n "$matches" ] && echo "$matches" || echo "No matches found." || echo "Error: The dmesg command did not run successfully.";


######################################################################################################################################################################################
# # Get the dmesg output with nice priority and filter messages from the last 10 minutes
# dmesg_output=$(
#   nice -n 10 dmesg --level=err,warn --time-format iso | awk '{gsub(/,[0-9]+/, "", $1); print}' | awk -v ten_minutes_ago="$(date -d '5 days ago 4 hours ago 29  minutes ago' +'%Y-%m-%dT%H:%M:%S%z')" '{timestamp = gensub(/\[|\]/, "", "g", $1); if (timestamp >= ten_minutes_ago) print}'
# )

# # Check the return code of the previous command
# if [ $? -eq 0 ]; then
#   # If the return code is 0, proceed to further processing
#   matches=$(
#     echo "$dmesg_output" | grep ".*EXT4-fs (\S*): .*error.*"
#   )
  
#   # Check if there are any matches
#   if [ -n "$matches" ]; then
#     # If matches are found, print them
#     echo "$matches"
#   else
#     # If no matches are found, print a message
#     echo "No matches found."
#   fi
# else
#   # If the dmesg command did not run successfully, print an error message
#   echo "Error: The dmesg command did not run successfully."
# fi

######################################################################################################################################################################################