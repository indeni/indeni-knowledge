import os
import unittest
from bluecat.integrity.bluecat_check_disk_error.bluecat_check_disk_error import CheckDiskError
from parser_service.public.action import *

class TestCheckDiskError(unittest.TestCase):
    def setUp(self):
        # Arrange
        self.parser = CheckDiskError()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_check_disk_error_case_valid_stdout(self):
        result = self.parser.parse_file(self.current_dir + '/disk_error_sample1.input', {}, {})
        print('******************************************')
        print(result)
        print('******************************************')
        self.assertEqual(4, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'check-disk-error')
        self.assertEqual(result[0].tags['name'], 'sdg2')
        self.assertEqual(result[0].tags['live-config'], 'true')
        self.assertEqual(result[0].tags['display-name'], 'Disk Errors')
        self.assertEqual(result[0].tags['im.dstype.displayType'], 'Number')
        self.assertEqual(result[0].value, 4)
    
    def test_check_disk_error_case_no_stdout(self):
        result = self.parser.parse_file(self.current_dir + '/disk_error_sample2.input', {}, {})
        print('#########################################')
        print(result)
        print('******************************************')
        self.assertEqual(0, len(result))

if __name__ == '__main__':
    unittest.main()