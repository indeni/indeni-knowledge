from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class CheckDiskError(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data = helper_methods.parse_data_as_list(raw_data, 'bluecat_check_disk_error.textfsm')
            if data:
                disk_name_metric_value = {}
                for row in data:
                    disk_name = row.get('disk_name')
                    error_count = row.get('error_count')
                    disk_name_metric_value.setdefault(disk_name, []).append(error_count)
                
                for disk_name in disk_name_metric_value:
                    latest_error_count = disk_name_metric_value[disk_name][-1]
                    self.write_double_metric('check-disk-error', {'name': disk_name}, 'gauge', int(latest_error_count), True, 'Disk Errors', 'Number')
        return self.output