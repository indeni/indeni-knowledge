from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser
from datetime import datetime
from zoneinfo import ZoneInfo

class BluecatLicenseInfo(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data = helper_methods.parse_data_as_list(raw_data, 'bluecat_license_info.textfsm')
            if data:
                time_format = '%a %b %d %H:%M:%S %Y'
                tags = {}
                try:
                    tags['name'] = data[0].get('name')
                    expiry_time = data[0].get('expiry_time')
                    expiry_object = datetime.strptime(expiry_time, time_format)
                    expiry_time_epoch = datetime.timestamp(expiry_object.replace(tzinfo=ZoneInfo('GMT+0')))
                    self.write_double_metric('license-expiration', tags, 'gauge', expiry_time_epoch, True, "License expiration", "date")
                except:
                    pass
        return self.output