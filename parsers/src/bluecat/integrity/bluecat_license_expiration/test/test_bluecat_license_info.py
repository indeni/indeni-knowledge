import os
import unittest
from bluecat.integrity.bluecat_license_expiration.bluecat_license_info import BluecatLicenseInfo
from parser_service.public.action import *

class TestBluecatLicenseInfo(unittest.TestCase):

    def setUp(self):
        self.parser = BluecatLicenseInfo()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_license_expiration_nearing_BAM(self):
        result = self.parser.parse_file(self.current_dir + '/license_expiration_nearing_BAM.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].ds_type, 'gauge')
        self.assertEqual(result[0].name, 'license-expiration')
        self.assertEqual(result[0].tags['name'], '20826-90000-F6040-59E53-F2C5B')
        self.assertEqual(result[0].tags['live-config'], 'true')
        self.assertEqual(result[0].tags['display-name'], 'License expiration')
        self.assertEqual(result[0].tags['im.dstype.displayType'], 'date')
        self.assertEqual(result[0].value, 2082690000.0)

    def test_license_expiration_neating_BDDS(self):
        result = self.parser.parse_file(self.current_dir + '/license_expiration_nearing_BDDS.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].ds_type, 'gauge')
        self.assertEqual(result[0].name, 'license-expiration')
        self.assertEqual(result[0].tags['name'], '17357-07600-36DA8-B5E4C-6053A')
        self.assertEqual(result[0].tags['live-config'], 'true')
        self.assertEqual(result[0].tags['display-name'], 'License expiration')
        self.assertEqual(result[0].tags['im.dstype.displayType'], 'date')
        self.assertEqual(result[0].value, 1735707600.0)

    def test_license_expired_BAM(self):
        result = self.parser.parse_file(self.current_dir + '/license_expired_BAM.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].ds_type, 'gauge')
        self.assertEqual(result[0].name, 'license-expiration')
        self.assertEqual(result[0].tags['name'], '20826-90000-F6040-59E53-F2C5B')
        self.assertEqual(result[0].tags['live-config'], 'true')
        self.assertEqual(result[0].tags['display-name'], 'License expiration')
        self.assertEqual(result[0].tags['im.dstype.displayType'], 'date')
        self.assertEqual(result[0].value, 1672462800.0)

    def test_license_expired_BDDS(self):
        result = self.parser.parse_file(self.current_dir + '/license_expired_BDDS.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].ds_type, 'gauge')
        self.assertEqual(result[0].name, 'license-expiration')
        self.assertEqual(result[0].tags['name'], '17357-07600-36DA8-B5E4C-6053A')
        self.assertEqual(result[0].tags['live-config'], 'true')
        self.assertEqual(result[0].tags['display-name'], 'License expiration')
        self.assertEqual(result[0].tags['im.dstype.displayType'], 'date')
        self.assertEqual(result[0].value, 1682830800.0)

if __name__ == '__main__':
    unittest.main()