import os
import unittest

from bluecat.integrity.bluecat_interrogation_serial.bluecat_interrogation_serial import BluecatInterrogationSerial
from parser_service.public.action import *

class TestBluecatInterrogationSerial(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = BluecatInterrogationSerial()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_BAM_VMware_9_4_0(self):
        result = self.parser.parse_file(self.current_dir + '/BAM_VMware_9_4_0.input', {}, {})
        self.assertEqual(2, len(result))
        self.assertEqual(result[0].action_type, 'WriteTag')
        self.assertEqual(result[0].key, 'model')
        self.assertEqual(result[0].value, 'VMware')
        self.assertEqual(result[1].action_type, 'WriteTag')
        self.assertEqual(result[1].key, 'serial')
        self.assertEqual(result[1].value, 'VMware-42 11 83 45 ed 87 01 bf-ee b9 6f fc 2e d9 b1 70')

    def test_BDDS_VMware_9_4_0(self):
        result = self.parser.parse_file(self.current_dir + '/BDDS_VMware_9_4_0.input', {}, {})
        self.assertEqual(2, len(result))
        self.assertEqual(result[0].action_type, 'WriteTag')
        self.assertEqual(result[0].key, 'model')
        self.assertEqual(result[0].value, 'VMware')
        self.assertEqual(result[1].action_type, 'WriteTag')
        self.assertEqual(result[1].key, 'serial')
        self.assertEqual(result[1].value, 'VMware-42 11 45 fb a0 f7 b5 d1-f3 4a 51 36 33 1e 0d c3')

if __name__ == '__main__':
    unittest.main()

    