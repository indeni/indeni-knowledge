from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class BluecatInterrogationSerial(BaseParser):
    """
    Collect BlueCat tags via SNMP:
        bcnSysIdPlatform    1.3.6.1.4.1.13315.3.2.2.1.5.0 = STRING: "VMware"
        bcnSysIdSerial      1.3.6.1.4.1.13315.3.2.2.1.3.0 = STRING: "VMware-42 11 83 45 ed 87 01 bf-ee b9 6f fc 2e d9 b1 70"
    """
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        lines = raw_data.splitlines()
        platform = ''
        serial = ''
        for line in lines:
            if line.startswith('1.3.6.1.4.1.13315.3.2.2.1.5.0 '):  # bcnSysIdPlatform
                platform = line.split("= ")[1]
            elif line.startswith('1.3.6.1.4.1.13315.3.2.2.1.3.0 '):  # bcnSysIdSerial
                serial = line.split("= ")[1]
        self.write_tag('model', platform)
        self.write_tag('serial', serial)

        return self.output
