import os
import unittest
from bluecat.integrity.bluecat_ntp_service_down.bluecat_ntp_service_down import BluecatNtpServiceDown
from parser_service.public.action import *

class TestBluecatNtpServiceDown(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = BluecatNtpServiceDown()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))    
    
    def test_ntp_server_up(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_ntp_service_state.input', {}, {})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'ntp-service-status')
        self.assertEqual(result[0].value, 1)

if __name__ == '__main__':
    unittest.main()