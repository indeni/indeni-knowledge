from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class BluecatNtpServiceDown(BaseParser):
    """
    Parser of:
        MIB: BCN-NTP-MIB
        node: bcnNtpSerOperState
        OID: 1.3.6.1.4.1.13315.3.1.4.2.1.1.0
    """
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            lines = raw_data.splitlines()
            for line in lines:
                if line.startswith('1.3.6.1.4.1.13315.3.1.4.2.1.1.0 '):  # bcnNtpSerOperState
                    ntp_status = int(line.split("= ")[1])
                    self.write_double_metric('ntp-service-status', {}, 'gauge', 1 if ntp_status == 1 else 0, False)
        return self.output