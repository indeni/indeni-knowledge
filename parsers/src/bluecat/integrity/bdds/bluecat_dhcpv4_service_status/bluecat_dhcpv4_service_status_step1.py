from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class BluecatBDDSNodeStatus(BaseParser):
    """
    Parser of:
        ACTIVE NODE
            node get-notify state=NODE_HA_ACTIVE
            node get-notify dhcp-enable=1
            node get-cmplt retcode=ok
        PASSIVE NODE
            node get-notify state=NODE_HA_PASSIVE
            node get-notify dhcp-enable=1
            node get-cmplt retcode=ok
        STANDALONE
            node get-notify state=NODE_SA_ACTIVE
            node get-notify dhcp-enable=1
            node get-cmplt retcode=ok
    """
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data = helper_methods.parse_data_as_list(raw_data, 'bluecat_psclient_node_get.textfsm')
            if data:
                self.write_dynamic_variable('dhcp_enabled', data[0]['dhcp_enable'])
                self.write_dynamic_variable('node_state', data[0]['node_state'])
        return self.output