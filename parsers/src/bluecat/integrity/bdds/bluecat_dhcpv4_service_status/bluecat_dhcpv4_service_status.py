from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class BluecatDhcpv4ServiceStatus(BaseParser):
    """
    Parser of:
        MIB: BCN-DHCPV4-MIB
        node: bcnDhcpv4SerOperState
        OID: 1.3.6.1.4.1.13315.3.1.1.2.1.1.0
    """
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if (dynamic_var.get('dhcp_enabled') == '0' or dynamic_var.get('node_state') == 'NODE_HA_PASSIVE'): # case dhcp-not enabled  or passive member, no alert
            dhcp_status = 1
        else:
            if raw_data:
                lines = raw_data.splitlines()
                for line in lines:
                    if line.startswith('1.3.6.1.4.1.13315.3.1.1.2.1.1.0 '):  # bcnDhcpv4SerOperState
                        dhcp_status = int(line.split("= ")[1])
        if dhcp_status:
                    self.write_double_metric('dhcpv4-service-state', {}, 'gauge', 0 if dhcp_status in [2,4,5] else 1, True, 'DHCPv4 Service Status', 'state')
        return self.output