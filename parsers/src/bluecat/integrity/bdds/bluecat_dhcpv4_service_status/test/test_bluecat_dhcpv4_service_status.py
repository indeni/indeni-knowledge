import os
import unittest
from bluecat.integrity.bdds.bluecat_dhcpv4_service_status.bluecat_dhcpv4_service_status import BluecatDhcpv4ServiceStatus
from parser_service.public.action import *

class TestBluecatDhcpv4ServiceStatus(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = BluecatDhcpv4ServiceStatus()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))    
    
    def test_dhcp_service_up(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_dhcpv4_service_up.input', {}, {})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'dhcpv4-service-state')
        self.assertEqual(result[0].value, 1)

    def test_dhcp_service_down(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_dhcpv4_service_down.input', {}, {})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'dhcpv4-service-state')
        self.assertEqual(result[0].value, 0)

if __name__ == '__main__':
    unittest.main()