import os
import unittest
from bluecat.integrity.bdds.bluecat_bdds_dns_connectivity_check.bluecat_bdds_dns_connectivity_check import BluecatBddsDnsConnectivityCheck
from parser_service.public.action import *

class TestBluecatBddsDnsConnectivityCheck(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = BluecatBddsDnsConnectivityCheck()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))    

    '''
        Test that we can selectively parse a primaryRoleTest when there are no errors report by the secondaryRoleTest
        Test data:
        {
          "Primary":[
            {
              "name":"primaryRoleTest",
              "errMsg":[
                {
                  "server":"192.168.0.12",
                  "reason":"no-response",
                  "port":"53",
                  "protocol":"udp"
                }
              ]
            }
          ],
          "Secondary":[
            {
              "name":"secondaryRoleTest"
            }
          ]
        }
    '''
    def test_bdds_dns_connectivity_check_test_one(self):
        result = self.parser.parse_file(self.current_dir + '/report-1.ndjson', {}, {})
        self.assertEqual(2,len(result))
        self.assertEqual(result[0].action_type, 'WriteComplexMetric')
        self.assertEqual(1,len(result[0].value))
        self.assertEqual("192.168.0.12 (53/udp) - no-response",result[0].value[0].get("issue"))
        self.assertEqual(result[1].action_type, 'WriteComplexMetric')
        self.assertEqual(0,len(result[1].value))

    '''
        Test that we can parse all the items from the primaryRoleTest and from the secondaryRoleTest
        Test data:
        {
          "Primary":[
            {
              "name":"primaryRoleTest",
              "errMsg":[
                {
                  "server":"192.168.0.10",
                  "reason":"no-response",
                  "port":"53",
                  "protocol":"udp"
                },
                {
                  "server":"192.168.0.12",
                  "reason":"host-unreachable",
                  "port":"53",
                  "protocol":"udp"
                }
              ]
            }
          ],
          "Secondary":[
            {
              "name":"secondaryRoleTest",
              "errMsg":[
                {
                  "server":"192.168.0.10",
                  "reason":"no-response",
                  "port":"53",
                  "protocol":"tcp"
                }
              ]
            }
          ]
        }
    '''
    def test_bdds_dns_connectivity_check_test_two(self):
        result = self.parser.parse_file(self.current_dir + '/report-2.ndjson', {}, {})
        self.assertEqual(2,len(result))
        self.assertEqual(result[0].action_type, 'WriteComplexMetric')
        self.assertEqual(2,len(result[0].value))
        self.assertEqual("192.168.0.10 (53/udp) - no-response",result[0].value[0].get("issue"))
        self.assertEqual("192.168.0.12 (53/udp) - host-unreachable",result[0].value[1].get("issue"))
        self.assertEqual(result[1].action_type, 'WriteComplexMetric')
        self.assertEqual(1,len(result[1].value))
        self.assertEqual(2,len(result[0].value))
        self.assertEqual("192.168.0.10 (53/tcp) - no-response",result[1].value[0].get("issue"))

    '''
        Test that we don't produce item's issues if there are no errors reported in either test
        but that we always produce the metrics (this will clear the alert when going from an item's with issues
        situation to no issues in either item
        Test data:
        {
          "Primary":[
            {
              "name":"primaryRoleTest"
            }
          ],
          "Secondary":[
            {
              "name":"secondaryRoleTest"
            }
          ]
        }
    '''
    def test_bdds_dns_connectivity_check_test_three(self):
        result = self.parser.parse_file(self.current_dir + '/report-3.ndjson', {}, {})
        self.assertEqual(2,len(result))
        self.assertEqual(result[0].action_type, 'WriteComplexMetric')
        self.assertEqual(result[1].action_type, 'WriteComplexMetric')
        self.assertEqual(0,len(result[0].value))
        self.assertEqual(0,len(result[1].value))

    '''
        Test that in case of the indeni-agent returns nothing that we don't produce any metrics
        Test data: blank
    '''
    def test_bdds_dns_connectivity_check_test_four(self):
        result = self.parser.parse_file(self.current_dir + '/no-report.ndjson', {}, {})
        self.assertEqual(0,len(result))

    '''
        Test that in case of the indeni-agent returns garbage i.e. some error message or an output
        that isn't a json, that we don't produce any metrics
        Test data: no file or directory found: location#12
    '''
    def test_bdds_dns_connectivity_check_test_five(self):
        result = self.parser.parse_file(self.current_dir + '/no-report.ndjson', {}, {})
        self.assertEqual(0,len(result))

    '''
        The indeni-agent is a new project an to support BlueCat specific devices deeply. It may produce multiple
        output (some outputs are expensive to recalculate but are calculated as part of other operations). We are 
        keeping our choices open on how we parse the report. For this specific test it looks for a line with the keys
        "Primary" and "Secondary" case sensitive.
        
        Test that it can find and parse the relevant json line in a multi-line ndjson report
    '''
    def test_bdds_dns_connectivity_check_test_six(self):
        result = self.parser.parse_file(self.current_dir + '/report-6.ndjson', {}, {})
        self.assertEqual(2,len(result))
        self.assertEqual(result[0].action_type, 'WriteComplexMetric')
        self.assertEqual(1,len(result[0].value))
        self.assertEqual("192.168.0.12 (53/udp) - no-response",result[0].value[0].get("issue"))
        self.assertEqual(result[1].action_type, 'WriteComplexMetric')
        self.assertEqual(0,len(result[1].value))

    '''
        Test that we can parse all the items from the primaryRoleTest, secondaryRoleTest, forwardRoleTest and stubRoleTest
        Test data:
        {
          "Primary":[
            {
              "name":"primaryRoleTest",
              "errMsg":[
                {
                  "server":"192.168.0.10",
                  "reason":"no-response",
                  "port":"53",
                  "protocol":"udp"
                },
                {
                  "server":"192.168.0.12",
                  "reason":"host-unreachable",
                  "port":"53",
                  "protocol":"udp"
                }
              ]
            }
          ],
          "Secondary":[
            {
              "name":"secondaryRoleTest",
              "errMsg":[
                {
                  "server":"192.168.0.10",
                  "reason":"no-response",
                  "port":"53",
                  "protocol":"tcp"
                }
              ]
            }
          ],
          "Forward":[
            {
              "name":"forwardRoleTest",
              "errMsg":[
                {
                  "server":"192.168.0.16",
                  "reason":"no-response",
                  "port":"53",
                  "protocol":"tcp"
                }
              ]
            }
          ],
          "Forwarding": [
            {
              "name": "forwardingTest",
              "errMsg": [
                {
                  "server": "10.244.29.102",
                  "reason": "no-response",
                  "port": "53",
                  "protocol": "udp"
                }
              ]
            }
          ],
          "Stub":[
            {
              "name":"stubRoleTest",
              "errMsg":[
                {
                  "server":"192.168.0.13",
                  "reason":"no-response",
                  "port":"53",
                  "protocol":"udp"
                }
              ]
            }
          ]
        }
    '''
    def test_bdds_dns_connectivity_check_test_7(self):
        result = self.parser.parse_file(self.current_dir + '/report-7.ndjson', {}, {})
        self.assertEqual(5,len(result))

        self.assertEqual(result[0].action_type, 'WriteComplexMetric')
        self.assertEqual(2,len(result[0].value))
        self.assertEqual("192.168.0.10 (53/udp) - no-response",result[0].value[0].get("issue"))
        self.assertEqual("192.168.0.12 (53/udp) - host-unreachable",result[0].value[1].get("issue"))
        self.assertEqual("Primary to followers",result[0].tags.get("name"))

        self.assertEqual(result[1].action_type, 'WriteComplexMetric')
        self.assertEqual(1,len(result[1].value))
        self.assertEqual("192.168.0.10 (53/tcp) - no-response",result[1].value[0].get("issue"))
        self.assertEqual("Secondary to primaries",result[1].tags.get("name"))

        self.assertEqual(result[2].action_type, 'WriteComplexMetric')
        self.assertEqual(1,len(result[2].value))
        self.assertEqual("192.168.0.16 (53/tcp) - no-response",result[2].value[0].get("issue"))
        self.assertEqual("Zones forwarders connectivity",result[2].tags.get("name"))

        self.assertEqual(result[3].action_type, 'WriteComplexMetric')
        self.assertEqual(1,len(result[3].value))
        self.assertEqual("10.244.29.102 (53/udp) - no-response",result[3].value[0].get("issue"))
        self.assertEqual("Forwarding servers connectivity",result[3].tags.get("name"))

        self.assertEqual(result[4].action_type, 'WriteComplexMetric')
        self.assertEqual(1,len(result[4].value))
        self.assertEqual("192.168.0.13 (53/udp) - no-response",result[4].value[0].get("issue"))
        self.assertEqual("Stub to upstream",result[4].tags.get("name"))

if __name__ == '__main__':
    unittest.main()