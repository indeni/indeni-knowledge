from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class BluecatBddsDnsConnectivityCheck(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            lines = raw_data.splitlines()
            for line in lines:
                # not adding Forward and Stub here to maintain backward compatibility
                if all(k in line for k in ("Primary", "Secondary")):
                    data_parsed = helper_methods.parse_data_as_json(line)
                    if data_parsed:
                        for test in data_parsed['Primary']:
                            if test.get("name") == "primaryRoleTest":
                                # format test results
                                tags, issues = self.__format_results(test,'Primary to followers')
                                self.write_complex_metric_object_array('bcn-bdds-dns-connectivity', tags, issues, False)
                        for test in data_parsed['Secondary']:
                            if test.get("name") == "secondaryRoleTest":
                                # format test results
                                tags, issues = self.__format_results(test,'Secondary to primaries')
                                self.write_complex_metric_object_array('bcn-bdds-dns-connectivity', tags, issues, False)
                        if 'Forward' in data_parsed:
                            for test in data_parsed['Forward']:
                                if test.get("name") == "forwardRoleTest":
                                    # format test results
                                    tags, issues = self.__format_results(test,'Zones forwarders connectivity')
                                    self.write_complex_metric_object_array('bcn-bdds-dns-connectivity', tags, issues, False)
                        if 'Forwarding' in data_parsed:
                            for test in data_parsed['Forwarding']:
                                if test.get("name") == "forwardingTest":
                                    # format test results
                                    tags, issues = self.__format_results(test,'Forwarding servers connectivity')
                                    self.write_complex_metric_object_array('bcn-bdds-dns-connectivity', tags, issues, False)
                        if 'Stub' in data_parsed:
                            for test in data_parsed['Stub']:
                                if test.get("name") == "stubRoleTest":
                                    # format test results
                                    tags, issues = self.__format_results(test,'Stub to upstream')
                                    self.write_complex_metric_object_array('bcn-bdds-dns-connectivity', tags, issues, False)

        return self.output

    def __format_results(self, test_result, item_name):
        tags = {}
        issues = []
        tags['name'] = item_name
        if 'errMsg' in test_result:
            for err in test_result['errMsg'][:10]:
                issues.append({'issue': f"{err['server']} ({err['port']}/{err['protocol']}) - {err['reason']}"})
        return tags, issues
