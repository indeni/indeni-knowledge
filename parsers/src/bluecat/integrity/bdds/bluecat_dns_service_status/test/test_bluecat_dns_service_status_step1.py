import os
import unittest
from bluecat.integrity.bdds.bluecat_dns_service_status.bluecat_dns_service_status_step1 import BluecatBDDSNodeStatus
from parser_service.public.action import *

class TestBluecatDnsServiceStatus(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = BluecatBDDSNodeStatus()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))    
    
    def test_bluecat_dns_service_status_step1_active_node_get(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_dns_service_status_step1_active_node_get.input', {}, {})
        self.assertEqual(2,len(result))
        self.assertEqual(result[0].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[0].key, 'dns_enabled')
        self.assertEqual(result[0].value, '1')
        self.assertEqual(result[1].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[1].key, 'node_state')
        self.assertEqual(result[1].value, 'NODE_HA_ACTIVE')

    def test_bluecat_dns_service_status_step1_passive_node_get(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_dns_service_status_step1_passive_node_get.input', {}, {})
        self.assertEqual(2,len(result))
        self.assertEqual(result[0].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[0].key, 'dns_enabled')
        self.assertEqual(result[0].value, '1')
        self.assertEqual(result[1].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[1].key, 'node_state')
        self.assertEqual(result[1].value, 'NODE_HA_PASSIVE')

    def test_bluecat_dns_service_status_step1_standalone_node_get(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_dns_service_status_step1_standalone_node_get.input', {}, {})
        self.assertEqual(2,len(result))
        self.assertEqual(result[0].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[0].key, 'dns_enabled')
        self.assertEqual(result[0].value, '1')
        self.assertEqual(result[1].action_type, 'WriteDynamicVariable')
        self.assertEqual(result[1].key, 'node_state')
        self.assertEqual(result[1].value, 'NODE_SA_PASSIVE')

if __name__ == '__main__':
    unittest.main()