import os
import unittest
from bluecat.integrity.bdds.bluecat_dns_service_status.bluecat_dns_service_status_step2 import BluecatDnsServiceStatus
from parser_service.public.action import *

class TestBluecatDnsServiceStatus(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = BluecatDnsServiceStatus()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))    
    
    def test_bluecat_dns_service_status_step2_active_member_up(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_dns_service_status_step2_up.input', {'dns_enabled':'1', 'node_state':'NODE_HA_ACTIVE' }, {})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'dns-service-state')
        self.assertEqual(result[0].value, 1)

    def test_bluecat_dns_service_status_step2_active_member_down(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_dns_service_status_step2_down.input',{'dns_enabled':'1', 'node_state':'NODE_HA_ACTIVE' }, {})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'dns-service-state')
        self.assertEqual(result[0].value, 0)

    def test_bluecat_dns_service_status_step2_passive_member_down(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_dns_service_status_step2_down.input', {'dns_enabled':'1', 'node_state':'NODE_HA_PASSIVE' }, {})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'dns-service-state')
        self.assertEqual(result[0].value, 1)

    def test_bluecat_dns_service_status_step2_standalone_member_up(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_dns_service_status_step2_up.input', {'dns_enabled':'1', 'node_state':'NODE_SA_ACTIVE' }, {})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'dns-service-state')
        self.assertEqual(result[0].value, 1)

    def test_bluecat_dns_service_status_step2_standalone_member_down(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_dns_service_status_step2_down.input', {'dns_enabled':'1', 'node_state':'NODE_SA_ACTIVE' }, {})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'dns-service-state')
        self.assertEqual(result[0].value, 0)

    def test_bluecat_dns_service_status_step2_active_member_disabled(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_dns_service_status_step2_down.input', {'dns_enabled':'0', 'node_state':'NODE_HA_ACTIVE' }, {})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'dns-service-state')
        self.assertEqual(result[0].value, 1)
        
    def test_bluecat_dns_service_status_step2_passive_member_disabled(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_dns_service_status_step2_down.input', {'dns_enabled':'0', 'node_state':'NODE_HA_PASSIVE' }, {})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'dns-service-state')
        self.assertEqual(result[0].value, 1)

    def test_bluecat_dns_service_status_step2_standalone_disabled(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_dns_service_status_step2_down.input', {'dns_enabled':'0', 'node_state':'NODE_SA_ACTIVE' }, {})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'dns-service-state')
        self.assertEqual(result[0].value, 1)

if __name__ == '__main__':
    unittest.main()