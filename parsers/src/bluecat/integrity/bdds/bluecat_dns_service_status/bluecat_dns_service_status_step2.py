from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class BluecatDnsServiceStatus(BaseParser):
    """
    Parser of:
        MIB: BCN-DNS-MIB
        node: bcnDnsSerOperState
        OID: 1.3.6.1.4.1.13315.3.1.2.2.1.1.0
    """
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if (dynamic_var.get('dns_enabled') == '0' or dynamic_var.get('node_state') == 'NODE_HA_PASSIVE'): # case dns-not enabled  or passive member, no alert
            dns_status = 1
        else:
            if raw_data:
                lines = raw_data.splitlines()
                for line in lines:
                    if line.startswith('1.3.6.1.4.1.13315.3.1.2.2.1.1.0 '):  # bcnDnsSerOperState
                        dns_status = int(line.split("= ")[1])
        if dns_status: #to consider the case SNMP no return info 
            self.write_double_metric('dns-service-state', {}, 'gauge', 0 if dns_status in [2,4,5] else 1, True, 'DNS Service Status', 'state')
        
        return self.output
