import os
import unittest
from bluecat.integrity.bdds.bluecat_dns_queries_failure.bluecat_dns_queries_failure import BluecatDnsQueriesFailure
from parser_service.public.action import *

class TestBluecatDnsQueriesFailure(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = BluecatDnsQueriesFailure()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))    
    
    def test_dns_queries_failure_low(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_dns_queries_failure_low.input', {}, {})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'dns-queries-failure')
        self.assertEqual(result[0].value, 1)

    def test_dns_queries_failure_high(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_dns_queries_failure_high.input', {}, {})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'dns-queries-failure')
        self.assertEqual(result[0].value, 2000)
        
if __name__ == '__main__':
    unittest.main()