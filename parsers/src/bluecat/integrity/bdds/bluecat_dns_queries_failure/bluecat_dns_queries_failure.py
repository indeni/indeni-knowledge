from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class BluecatDnsQueriesFailure(BaseParser):
    """
    Parser of:
        MIB: BCN-DNS-MIB
        node: bcnDnsStatSrvQryFailure
        OID: 1.3.6.1.4.1.13315.3.1.2.2.2.1.6
    """
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            lines = raw_data.splitlines()
            for line in lines:
                if line.startswith('1.3.6.1.4.1.13315.3.1.2.2.2.1.6.0 '):  # bcnDnsStatSrvQryFailure
                    dns_queries_failure = int(line.split("= ")[1])
                    self.write_double_metric('dns-queries-failure', {}, 'gauge', dns_queries_failure, True, 'DNS Queries Failure')
        return self.output