from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class DNS_TCP_Connection_Usage(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data = helper_methods.parse_data_as_list(raw_data, 'bluecat_dns_tcp_connection_usage.textfsm')
            if data:
                for row in data:
                    connection_percentage =round(eval(row.get('tcp_connection'))*100,2)
                    self.write_double_metric('dns-tcp-connection-usage', {}, 'gauge', connection_percentage, True, 'Current TCP connection percentage')
        return self.output