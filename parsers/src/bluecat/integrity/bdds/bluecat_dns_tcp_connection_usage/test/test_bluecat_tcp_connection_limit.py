import os
import unittest
from bluecat.integrity.bdds.bluecat_dns_tcp_connection_usage.bluecat_dns_tcp_connection_usage import DNS_TCP_Connection_Usage
from parser_service.public.action import *

class TestDNS_TCP_Connection_Usaget(unittest.TestCase):
    def setUp(self):
        # Arrange
        self.parser = DNS_TCP_Connection_Usage()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_tcp_connection_limit_valid(self):
        result = self.parser.parse_file(self.current_dir + '/tcp_connection_sample1.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'dns-tcp-connection-usage')
        self.assertEqual(result[0].value, 0.00)
    
    def test_tcp_connection_limit_empty(self):
        result = self.parser.parse_file(self.current_dir + '/tcp_connection_sample2.input', {}, {})
        self.assertEqual(0, len(result))
    

    def test_tcp_connection_limit_valid_nonzero(self):
        result = self.parser.parse_file(self.current_dir + '/tcp_connection_sample3.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'dns-tcp-connection-usage')
        self.assertEqual(result[0].value, 26.67)

if __name__ == '__main__':
    unittest.main()