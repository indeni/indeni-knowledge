from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class BluecatBDDSSyslogServer(BaseParser):
    
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            log_paths = helper_methods.parse_data_as_list(
                raw_data, 'syslog_conf_log_paths.textfsm')
            destinations = helper_methods.parse_data_as_list(
                raw_data, 'syslog_conf_destinations.textfsm')
            filters = helper_methods.parse_data_as_list(
                raw_data, 'syslog_conf_filters.textfsm')
            servers_list = []
            for log_path in log_paths:
                syslog_server_data = {}
                for destination in destinations:
                    if log_path['destination'] == destination['destination']:
                        syslog_server_data['host'] = destination['dst_ip']
                for filter_name in filters:
                    if log_path['filter'] == filter_name['filter']:
                        syslog_server_data['severity'] = filter_name['severity_low']
                if syslog_server_data.get('host') and syslog_server_data.get('severity'):
                    servers_list.append(syslog_server_data)
            self.write_complex_metric_object_array('syslog-servers', {}, servers_list, True, "Syslog servers")
        return self.output