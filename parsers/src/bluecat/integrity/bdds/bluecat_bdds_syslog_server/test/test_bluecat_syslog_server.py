import os
import unittest
from bluecat.integrity.bdds.bluecat_bdds_syslog_server.bluecat_bdds_syslog_server import BluecatBDDSSyslogServer
from parser_service.public.action import *


class TestBluecatBDDSSyslogServer(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = BluecatBDDSSyslogServer()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_single_syslog_server_configured(self):
        result = self.parser.parse_file(self.current_dir + '/single_syslog_server_configured.input', {}, {})
        self.assertEqual(len(result), 1)
        self.assertEqual(result[0].action_type, 'WriteComplexMetric')
        self.assertEqual(result[0].name, 'syslog-servers')
        self.assertEqual(result[0].value[0]['host'], '10.10.10.10')
        self.assertEqual(result[0].value[0]['severity'],'info')

    def test_single_syslog_server_configured_BDDS_v9_4(self):
        result = self.parser.parse_file(self.current_dir + '/single_syslog_server_configured_BDDS_v9.4.input', {}, {})
        self.assertEqual(len(result), 1)
        self.assertEqual(result[0].action_type, 'WriteComplexMetric')
        self.assertEqual(result[0].name, 'syslog-servers')
        self.assertEqual(result[0].value[0]['host'], '10.40.6.150')
        self.assertEqual(result[0].value[0]['severity'],'info')

    def test_multiple_syslog_server_configured(self):
        result = self.parser.parse_file(self.current_dir + '/multiple_syslog_server_configured.input', {}, {})
        self.assertEqual(len(result), 1)
        self.assertEqual(result[0].action_type, 'WriteComplexMetric')
        self.assertEqual(result[0].name, 'syslog-servers')
        self.assertEqual(result[0].value[0]['host'], '10.10.10.10')
        self.assertEqual(result[0].value[0]['severity'],'info')
        self.assertEqual(result[0].value[1]['host'], '10.10.10.11')
        self.assertEqual(result[0].value[1]['severity'],'info')
        self.assertEqual(result[0].value[2]['host'], '10.10.10.12')
        self.assertEqual(result[0].value[2]['severity'],'info')
        self.assertEqual(result[0].value[3]['host'], '10.10.10.13')
        self.assertEqual(result[0].value[3]['severity'],'info')
        
    def test_syslog_server_not_configured(self):
        result = self.parser.parse_file(self.current_dir + '/syslog_server_not_configured.input', {}, {})
        self.assertEqual(len(result), 0)
        
 
if __name__ == '__main__':
    unittest.main()