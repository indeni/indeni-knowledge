current_dir=$(pwd)
mkdir -p "$current_dir/indeni-agent"
tarfile=$(find "$current_dir/indeni-agent" -maxdepth 1 -regextype egrep -regex "$current_dir/indeni-agent.*\.tar.gz"|sort -r|head -n 1)
tar -xvf "$tarfile" -C "$current_dir/indeni-agent"