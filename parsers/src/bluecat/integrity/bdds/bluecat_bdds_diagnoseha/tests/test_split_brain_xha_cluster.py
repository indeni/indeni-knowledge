import os
import unittest
from bluecat.integrity.bdds.bluecat_bdds_diagnoseha.bluecat_bdds_diagnoseha import BluecatBDDSDiagnoseHA
from parser_service.public.action import *

class TestBluecatBDDSDiagnoseHA(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = BluecatBDDSDiagnoseHA()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))    
    
    def test_diagnoseha_no_split_brain(self):
        result = self.parser.parse_file(self.current_dir + '/diagnoseha_no_split_brain.input', {}, {})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'xha-cluster-split-brain')
        self.assertEqual(result[0].value, 0)

    def test_diagnoseha_split_brain(self):
        result = self.parser.parse_file(self.current_dir + '/diagnoseha_split_brain.input', {}, {})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'xha-cluster-split-brain')
        self.assertEqual(result[0].value, 1)       
        
        
    def test_diagnoseha_no_ha(self):
        result = self.parser.parse_file(self.current_dir + '/diagnoseha_no_ha.input', {}, {})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'xha-cluster-split-brain')
        self.assertEqual(result[0].value, 0)       

    def test_diagnose_ha_empty(self):
        result = self.parser.parse_file(self.current_dir + '/diagnose_ha_empty.input', {}, {})
        self.assertEqual(0,len(result))

if __name__ == '__main__':
    unittest.main()