from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class BluecatBDDSDiagnoseHA(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data_parsed = helper_methods.parse_data_as_list(raw_data, 'bluecat_diagnoseha.textfsm')
            if (data_parsed[0]['xha_status'] == "active" and 
            (data_parsed[0]['dns_on_active'] == "running" or data_parsed[0]['dhcp_on_active'] == "running") and
            data_parsed[0]['drbd_connect_state'] != "Connected" and
            data_parsed[0]['local_role'] == "Primary" and 
            data_parsed[0]['peer_role'] != "Secondary" and 
            data_parsed[0]['peer_disk_state'] != "UpToDate"):
                self.write_double_metric('xha-cluster-split-brain', {}, 'gauge', 1, False)
            else:
                self.write_double_metric('xha-cluster-split-brain', {}, 'gauge', 0, False)
        return self.output
