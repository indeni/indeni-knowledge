import os
import unittest
from bluecat.integrity.bdds.bluecat_cluster_member_state.bluecat_cluster_member_state import BluecatClusterMemberState
from parser_service.public.action import *

class TestBluecatClusterMemberState(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = BluecatClusterMemberState()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))    
    
    def test_cluster_member_state_standalone(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_cluster_member_state_standalone.input', {}, {})
        self.assertEqual(0,len(result))

    def test_cluster_member_state_pasive(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_cluster_member_state_passive.input', {}, {})
        self.assertEqual(2,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'cluster-member-active')
        self.assertEqual(result[0].value, 0)
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].name, 'cluster-state')
        self.assertEqual(result[1].value, 1)

    def test_cluster_member_state_active(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_cluster_member_state_active.input', {}, {})
        self.assertEqual(2,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'cluster-member-active')
        self.assertEqual(result[0].value, 1)
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].name, 'cluster-state')
        self.assertEqual(result[1].value, 1)

    def test_bluecat_cluster_sync_not_configured(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_cluster_sync_not_configured.input', {}, {})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'cluster-config-synced')
        self.assertEqual(result[0].value, 0)

    def test_bluecat_cluster_sync_replicating(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_cluster_sync_replicating.input', {}, {})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'cluster-config-synced')
        self.assertEqual(result[0].value, 0)

    def test_bluecat_cluster_sync_synced(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_cluster_sync_synced.input', {}, {})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'cluster-config-synced')
        self.assertEqual(result[0].value, 1)

    def test_cluster_member_state_active_and_synced(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_cluster_member_state_active_and_synced.input', {}, {})
        self.assertEqual(3,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'cluster-member-active')
        self.assertEqual(result[0].value, 1)
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].name, 'cluster-state')
        self.assertEqual(result[1].value, 1)
        self.assertEqual(result[2].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[2].name, 'cluster-config-synced')
        self.assertEqual(result[2].value, 1)

    def test_cluster_member_state_passive_and_synced(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_cluster_member_state_passive_and_synced.input', {}, {})
        self.assertEqual(3,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'cluster-member-active')
        self.assertEqual(result[0].value, 0)
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].name, 'cluster-state')
        self.assertEqual(result[1].value, 1)
        self.assertEqual(result[2].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[2].name, 'cluster-config-synced')
        self.assertEqual(result[2].value, 1)

if __name__ == '__main__':
    unittest.main()