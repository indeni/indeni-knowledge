from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class BluecatClusterMemberState(BaseParser):
    """
    Parser of:
        MIB: BCN-HA-MIB
        node: bcnHaSerOperState
        OID: 1.3.6.1.4.1.13315.3.1.5.2.1.1.0
    Description:    
        Operational state of the Service. The possible states are:
        standalone(1) The node is not configured to provide clustering
                      services.
        active(2)     The system is operational and the node is providing
                      clustering services.
        passive(3)    The system is operational and the node is active as
                      a passive standby for the cluster.
        stopped(4)    The service is stopped either intentionally (i.e.:
                      the service is not supposed to run on this node) or
                      unintentionally (a problem has occurred).
                      This state might apply to both standalong and
                      clustered nodes.
        stopping(5)   The service is in the process of stopping. Stopping
                      a service might be necessary after a configuration
                      change.
        becomingActive (6)  The node is becoming active, either as a result
                      of a switchover or by initial start.
        becomingPassive (7) The node is failing over. Another node is taking
                      charge of the services.
        fault(8)      An error has been detected and the state is undefined.

    Parser of:
        MIB: BCN-HA-MIB
        node:  bcnHaSerReplicationState
        OID:  .1.3.6.1.4.1.13315.3.1.5.2.1.2.0
    Description:    
        notConfigured(1),
        replicating(2),
        synchronized(3)
}
    """
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            lines = raw_data.splitlines()
            for line in lines:
                if line.startswith('1.3.6.1.4.1.13315.3.1.5.2.1.1.0 '):  # bcnHaSerOperState
                    cluster_member_state = int(line.split("= ")[1])
                    if cluster_member_state != 1:
                        self.write_double_metric('cluster-member-active', {'name' : 'xHA'}, 'gauge', 1 if cluster_member_state == 2 else 0, False)
                        self.write_double_metric('cluster-state', {}, 'gauge', 1 if cluster_member_state in [2, 3, 6, 7] else 0, True, 'Cluster State', 'state')
                if line.startswith('1.3.6.1.4.1.13315.3.1.5.2.1.2.0 '):  # bcnHaSerReplicationState
                    cluster_member_sync = int(line.split("= ")[1])
                    self.write_double_metric('cluster-config-synced', {}, 'gauge', 1 if cluster_member_sync == 3 else 0, True, 'HA Config Sync', 'state')
        return self.output