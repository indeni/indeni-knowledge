import os
import unittest
from bluecat.integrity.bdds.bluecat_xha_backbone.bluecat_xha_backbone import BluecatXhaBackbone
from parser_service.public.action import *

class TestBluecatXhaBackbone(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = BluecatXhaBackbone()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))    
    
    def test_xha_backbone_active(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_xha_backbone_active.input', {}, {})
        self.assertEqual(4,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'xha-backbone-status')
        self.assertEqual(result[0].value, 1)
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].name, 'xha-backbone-overlapping')
        self.assertEqual(result[1].tags['name'], 'lo(127.0.0.1/8)')
        self.assertEqual(result[1].value, 0)
        self.assertEqual(result[2].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[2].name, 'xha-backbone-overlapping')
        self.assertEqual(result[2].tags['name'], 'eth0(192.168.0.13/24)')
        self.assertEqual(result[2].value, 0)
        self.assertEqual(result[3].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[3].name, 'xha-backbone-overlapping')
        self.assertEqual(result[3].tags['name'], 'eth0(192.168.0.14/24)')
        self.assertEqual(result[3].value, 0)
        
    def test_xha_backbone_down(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_xha_backbone_down.input', {}, {})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'xha-backbone-status')
        self.assertEqual(result[0].value, 0)

    def test_xha_backbone_single_overlapping(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_xha_backbone_single_overlapping.input', {}, {})
        self.assertEqual(4,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'xha-backbone-status')
        self.assertEqual(result[0].value, 1)
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].name, 'xha-backbone-overlapping')
        self.assertEqual(result[1].tags['name'], 'lo(127.0.0.1/8)')
        self.assertEqual(result[1].value, 0)
        self.assertEqual(result[2].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[2].name, 'xha-backbone-overlapping')
        self.assertEqual(result[2].tags['name'], 'eth0(192.168.0.13/24)')
        self.assertEqual(result[2].value, 1)
        self.assertEqual(result[3].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[3].name, 'xha-backbone-overlapping')
        self.assertEqual(result[3].tags['name'], 'eth0(192.168.0.14/24)')
        self.assertEqual(result[3].value, 1)

    def test_xha_backbone_multiple_overlapping(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_xha_backbone_multiple_overlapping.input', {}, {})
        self.assertEqual(5,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'xha-backbone-status')
        self.assertEqual(result[0].value, 1)
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].name, 'xha-backbone-overlapping')
        self.assertEqual(result[1].tags['name'], 'lo(127.0.0.1/8)')
        self.assertEqual(result[1].value, 0)
        self.assertEqual(result[2].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[2].name, 'xha-backbone-overlapping')
        self.assertEqual(result[2].tags['name'], 'eth0(192.168.0.13/24)')
        self.assertEqual(result[2].value, 1)
        self.assertEqual(result[3].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[3].name, 'xha-backbone-overlapping')
        self.assertEqual(result[3].tags['name'], 'eth0(192.168.0.14/24)')
        self.assertEqual(result[3].value, 1)
        self.assertEqual(result[4].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[4].name, 'xha-backbone-overlapping')
        self.assertEqual(result[4].tags['name'], 'eth2(192.168.0.15/24)')
        self.assertEqual(result[4].value, 1)

if __name__ == '__main__':
    unittest.main()