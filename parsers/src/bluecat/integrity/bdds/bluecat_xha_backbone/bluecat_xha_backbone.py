from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser
from ipaddress import ip_network

class BluecatXhaBackbone(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            parsed_data = helper_methods.parse_data_as_list(raw_data, 'bluecat_xha_backbone.textfsm')
            if parsed_data[0].get('status'):
                self.write_double_metric('xha-backbone-status', {}, 'gauge', 0 if parsed_data[0].get('status') == 'Disable' else 1, False)
                if parsed_data[0].get('status') != 'Disable':
                    backbone_int = [{x['interface']:x['ip']} for x in parsed_data if x['interface'] == 'eth1']
                    other_int = [{x['interface']:x['ip']} for x in parsed_data if x['interface'] != 'eth1']
                    for interface in other_int:
                        tags = {}
                        tags['name'] = f"{list(interface.keys())[0]}({interface[list(interface.keys())[0]]})"
                        if ip_network(backbone_int[0].get('eth1'), strict = False).network_address == ip_network(interface.get(list(interface.keys())[0]), strict = False).network_address:
                            self.write_double_metric('xha-backbone-overlapping', tags, 'gauge', 1, False)
                        else:
                            self.write_double_metric('xha-backbone-overlapping', tags, 'gauge', 0, False)
        return self.output