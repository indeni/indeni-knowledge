from parser_service.public.base_parser import BaseParser
from parser_service.public import helper_methods

dns_stat_types =  {
    'opcode':'Operation Code',
    'rcode':'Return Code',
    'qtype':'Query Type',
    'nsstat':'Nameserver',
    'zonestat':'Zone Maintenance',
    'resstat':'Reset',
    'sockstat':'Socket',
}

critical_stats = [
    'UpdateRej',
    'UpdateReqFwd',
    'UpdateRespFwd',
    'UpdateDone',
    'UpdateFail',
    'UpdateBadPrereq',
    'Requestv4',
    'ReqEdns0',
    'RPZRewrites',
    'ReqBadEDNSVer',
    'ReqTCP',
    'AuthQryRej',
    'RecQryRej',
    'Response',
    'RespTruncated',
    'TruncatedResp',
    'RespEDNS0',
    'QrySuccess',
    'QryAuthAns',
    'QryNoauthAns',
    'QryReferral',
    'QryNxrrset',
    'QrySERVFAIL',
    'QryFORMERR',
    'QryNXDOMAIN',
    'QryRecursion',
    'QryDuplicate',
    'QryDropped',
    'RateDropped',
    'NotifyOutv4',
    'NotifyOutv6',
    'NotifyInv4',
    'NotifyInv6',
    'AXFRReqv4',
    'AXFRReqv6',
    'IXFRReqv4',
    'IXFRReqv6',
    'XfrSuccess',
    'XfrFail'
]

class BluecatDnsStats(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            if any(x in raw_data for x in ['Connection refused', 'xHA Secondary no need to test']):
                pass
            else:
                data = helper_methods.parse_data_as_xml(raw_data.strip('\r'))
                if 'counters' in data['statistics']['server'].keys() and len(data['statistics']['server']['counters']) > 0:
                    for stat_type in data['statistics']['server']['counters']:
                        if stat_type.get('@type') in dns_stat_types.keys():
                            type_name = stat_type['@type']
                            if 'counter' in stat_type.keys() and len(stat_type['counter']) > 0:
                                if isinstance(stat_type['counter'], list):
                                    for stat_name in stat_type['counter']:
                                        if stat_name.get('@name') in critical_stats:
                                            tags  = {}
                                            tags['type'] = type_name
                                            tags['name'] = stat_name['@name']
                                            tags['im.identity-tags'] = 'name'
                                            self.write_double_metric(f"dns-stats-{tags['type']}", tags, 'counter', int(stat_name['#text']), True, f"DNS Stats - {dns_stat_types[tags['type']]}")
                                elif isinstance(stat_type['counter'], dict):
                                    stat_name = stat_type['counter']
                                    tags  = {}
                                    tags['type'] = type_name
                                    tags['name'] = stat_name['@name']
                                    tags['im.identity-tags'] = 'name'
                                    self.write_double_metric(f"dns-stats-{tags['type']}", tags, 'counter', int(stat_name['#text']), True, f"DNS Stats - {dns_stat_types[tags['type']]}")

        return self.output