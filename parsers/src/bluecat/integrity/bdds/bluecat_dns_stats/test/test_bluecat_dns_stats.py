import os
import unittest
from bluecat.integrity.bdds.bluecat_dns_stats.bluecat_dns_stats import BluecatDnsStats
from parser_service.public.action import *

class TestBluecatDnsStats(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = BluecatDnsStats()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_collect_metrics(self):
        data = self.parser.parse_file(self.current_dir + '/collect_metrics.xml', {}, {})
        self.assertEqual(38, len(data))


if __name__ == '__main__':
    unittest.main()