import unittest
from unittest.mock import patch, Mock, MagicMock
import os
from parser_service.public.action import *
from bluecat.integrity.bdds.bluecat_bdds_zone_transfer_failure.bluecat_bdds_zone_transfer_failure import BluecatBDDSZoneTransferFailure

class TestBluecatBDDSZoneTransferFailure(unittest.TestCase):
    @patch('logging.config.fileConfig')
    @patch.object(BluecatBDDSZoneTransferFailure, 'load_global_config')
    @patch.object(BluecatBDDSZoneTransferFailure, 'read_cooldown_threshold')
    def setUp(self, mock_read_cooldown, mock_load_global_config, mock_logging_config):
        # Prevent logging configuration error
        mock_logging_config.return_value = None
        
        mock_load_global_config.return_value = {
            'parserLogConfig': 'fake_log_config',
            'indeniKnowledgeStableBasePath': '/fake/path/',
            'indeniServiceConfigLocation': 'fake_service_config',
            'indeniServiceOverwriteConfigLocation': 'fake_service_overwrite_config'
        }
        mock_read_cooldown.return_value = 20
        
        self.parser = BluecatBDDSZoneTransferFailure()
        self.device_tags = {'ip-address': '192.168.1.1'}

    @patch('parser_service.public.helper_methods.parse_data_as_list')
    @patch.object(BluecatBDDSZoneTransferFailure, 'load_prev_failures')
    @patch.object(BluecatBDDSZoneTransferFailure, 'persist_failures_to_state_file')
    def test_zone_transfer_failure(self, mock_persist, mock_load_prev, mock_parse_data):
        # Arrange
        mock_load_prev.return_value = {}
        mock_parse_data.return_value = [
            {'VIEW': 'default', 'ZONE': 'aquamarine.beagle.co.jp', 'REASON': 'host unreachable'},
            {'VIEW': 'default', 'ZONE': 'example.com', 'REASON': 'master 10.244.29.78#53 is unreachable (cached)'},
            {'VIEW': 'default', 'ZONE': 'example2.com', 'REASON': 'REFUSED'},
            {'VIEW': 'default', 'ZONE': 'low.rpz.bluecatnetworks.com', 'REASON': 'tsig indicates error'}
        ]

        # Act
        raw_data = "sample raw data"
        result = self.parser.parse(raw_data, {}, self.device_tags)

        # Assert
        self.assertEqual(4, len(result))
        self.assertEqual(result[0].action_type, 'WriteComplexMetric')
        self.assertEqual(result[0].name, 'bdds-log-zone-transfer-fail')
        self.assertEqual(result[0].tags['name'], 'default | aquamarine.beagle.co.jp')
        self.assertEqual(result[0].value[0].get('Issue'), 'host unreachable')

        mock_load_prev.assert_called_once()
        mock_persist.assert_called_once()

    @patch('parser_service.public.helper_methods.parse_data_as_list')
    @patch.object(BluecatBDDSZoneTransferFailure, 'load_prev_failures')
    @patch.object(BluecatBDDSZoneTransferFailure, 'persist_failures_to_state_file')
    def test_zone_transfer_empty(self, mock_persist, mock_load_prev, mock_parse_data):
        # Arrange
        mock_load_prev.return_value = {}
        mock_parse_data.return_value = []

        # Act
        result = self.parser.parse("", {}, self.device_tags)

        # Assert
        self.assertEqual(0, len(result))
        mock_load_prev.assert_called_once()
        mock_persist.assert_called_once()

    def tearDown(self):
        # Clean up any state files
        state_folder = '/fake/path/ind/parsers/src/bluecat/integrity/bdds/bluecat_bdds_zone_transfer_failure/prev_state/'
        state_file = f"{state_folder}{self.device_tags['ip-address']}_bdds-log-zone-transfer-fail"
        if os.path.exists(state_file):
            os.remove(state_file)

if __name__ == '__main__':
    unittest.main()