from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser
from parser_service.public.state_parser import StateParser
import re
import logging
import logging.config

class BluecatBDDSZoneTransferFailure(BaseParser, StateParser):
    def __init__(self):
        super().__init__()
        global_config_path = '/etc/bcia-config.json'
        global_config = self.load_global_config(global_config_path)

        logging.config.fileConfig(global_config['parserLogConfig'])
        self.state_file_path = f"{global_config['indeniKnowledgeStableBasePath']}ind/parsers/src/bluecat/integrity/bdds/bluecat_bdds_zone_transfer_failure/prev_state/"
        self.cooldown_threshold = self.read_cooldown_threshold(global_config['indeniServiceConfigLocation'], global_config['indeniServiceOverwriteConfigLocation'])
        self.maintain_state_for_metrics = ['bdds-log-zone-transfer-fail']
        self.additional_params_for_metric = {'bdds-log-zone-transfer-fail': {'is_live_config': False}}
    
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):

        # Load previous failures for the current metric from the state file
        prev_failures = self.load_prev_failures(self.state_file_path, device_tags['ip-address']+'_bdds-log-zone-transfer-fail')

        logging.debug('#######################################')
        logging.debug('#### IP ADDRESS: ' + str(device_tags['ip-address']) + ' ####')
        logging.debug('#######################################')

        if not raw_data or not helper_methods.parse_data_as_list(raw_data.strip(), 'bluecat_bdds_zone_transfer_failure.textfsm'):
            logging.debug('****** NO raw_data ******\n\t prev_failures = \n\t' + str(prev_failures))
            unresolved_failures, _ = self.resolve_prev_failures(
                'bdds-log-zone-transfer-fail', 'string', 
                self.write_complex_metric_object_array,
                self.additional_params_for_metric['bdds-log-zone-transfer-fail'],
                self.cooldown_threshold, prev_failures)
            logging.debug('\t unresolved_failures = \n\t' + str(unresolved_failures))
            self.persist_failures_to_state_file(self.state_file_path, device_tags['ip-address'], unresolved_failures)

        if raw_data:
            logging.debug('****** raw_data ******\n\t' + str(raw_data))
            data = helper_methods.parse_data_as_list(raw_data.strip(), 'bluecat_bdds_zone_transfer_failure.textfsm')

            if data:
                logging.debug('****** TextFSM parsed data ******\n\t' + str(data))
                current_failures = dict() # Initialize a dictionary to track current failures and avoid duplicates
                current_failures_issues = dict()  # Initialize a dictionary to track issues for failures
                for zone_info in data:
                    failed_zone = f"{zone_info['VIEW']} | {zone_info['ZONE']}"
                    issues = {'Issue': (re.sub(r'\s*\(.*?\)', '', zone_info['REASON'], 1)).replace('master', 'Primary') if 'master' in zone_info['REASON'] else zone_info['REASON']}
                    if failed_zone not in current_failures:
                        current_failures[failed_zone] = 0 # Flag as a failure
                        current_failures_issues[failed_zone] = [issues]  # Store the issues
                
                self.__write_complex_metric_object_array('bdds-log-zone-transfer-fail', 
                                                         device_tags, prev_failures, 
                                                         current_failures, current_failures_issues)
        return self.output

    def __write_complex_metric_object_array(self, metric, device_tags, prev_failures, current_failures, current_failures_issues):
        logging.debug(f'****** Failures from CURRENT RUN with Issues for METRIC: {metric} ******\n\t' + str(current_failures_issues))

        unresolved_failures, trimmed_current_failures = self.resolve_prev_failures(
            metric, 'string', self.write_complex_metric_object_array,
            self.additional_params_for_metric[metric],
            self.cooldown_threshold, prev_failures, current_failures)

        # Write metrics for unresolved failures
        logging.debug('****** Unresolved failures from Previous Run ******\n\t' + str(unresolved_failures))
        self.write_metric_from_failure_issues_dict(metric, 'string', self.write_complex_metric_object_array, self.additional_params_for_metric[metric], unresolved_failures, current_failures_issues)

        # write metrics for trimmed current failures
        logging.debug('****** Trimmed failures from Current Run ******\n\t' + str(trimmed_current_failures))
        self.write_metric_from_failure_issues_dict(metric, 'string', self.write_complex_metric_object_array, self.additional_params_for_metric[metric], trimmed_current_failures, current_failures_issues)

        # Write the zone failures to the state file
        self.persist_failures_to_state_file(self.state_file_path, device_tags['ip-address']+'_'+metric, unresolved_failures | trimmed_current_failures)