from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class BluecatManualOverrideState(BaseParser):
    
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            parsed_data = helper_methods.parse_data_as_list(raw_data, 'bluecat_manual_override_state.textfsm')
            if parsed_data:
                override = []
                name_list = parsed_data[0]['name'].split(',')
                if len(name_list) > 0:
                    for service in name_list:
                        if len(service) >0:
                            item = {}
                            item['service'] = service
                            override.append(item)
                self.write_complex_metric_object_array('manual-override', {}, override, True, "Manual-override services")
        return self.output