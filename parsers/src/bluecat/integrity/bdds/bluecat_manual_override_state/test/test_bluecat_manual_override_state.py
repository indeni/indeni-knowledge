import os
import unittest
from bluecat.integrity.bdds.bluecat_manual_override_state.bluecat_manual_override_state import BluecatManualOverrideState
from parser_service.public.action import *

class TestBluecatManualOverrideState(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = BluecatManualOverrideState()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))    
    
    def test_single_manual_override(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_single_manual_override.input', {}, {})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].action_type, 'WriteComplexMetric')
        self.assertEqual(result[0].name, 'manual-override')
        self.assertEqual(result[0].value, [{'service': 'ntp'}])

    def test_multiple_manual_override(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_multiple_manual_override.input', {}, {})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].action_type, 'WriteComplexMetric')
        self.assertEqual(result[0].name, 'manual-override')
        self.assertEqual(result[0].value, [{'service': 'ntp'}, {'service': 'syslog'}, {'service': 'anycast'}])

    def test_manual_override_empty(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_manual_override_empty.input', {}, {})
        self.assertEqual("result" not in locals(), False)


if __name__ == '__main__':
    unittest.main()