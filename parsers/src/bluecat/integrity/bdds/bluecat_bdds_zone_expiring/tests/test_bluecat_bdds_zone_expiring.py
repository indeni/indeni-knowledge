import unittest
from bluecat.integrity.bdds.bluecat_bdds_zone_expiring.bluecat_bdds_zone_expiring import BluecatBddsDnsZoneExpiry
from parser_service.public.action import *

class TestBluecatBddsDnsZoneExpiring(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = BluecatBddsDnsZoneExpiry()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    '''
        Test that we don't produce metrics when the command returned 
        Test data:
        []
    '''
    def test_bdds_zone_expiring_when_empty(self):
        result = []
        self.assertEqual(0,len(result))

    '''
        Test that we pick up near expiry dns zones in the output
        Test data:
        [
          {
            "zoneName":"yoel.com",
            "zoneType":"secondary",
            "view":"default",
            "mdate":"2024-02-08T09:47:10.961807-05:00",
            "expire":10,
            "expireIn":10000
          }
        ]
    '''
    def test_bdds_zone_expiring_one(self):
        result = self.parser.parse_file(self.current_dir + '/inspect-zone-expiry.output', {}, {})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].action_type, 'WriteComplexMetric')
        self.assertEqual(1,len(result[0].value))
        self.assertEqual("Zone: yoel.com, View: default, Updated At: 2024-02-08T09:47:10.961807-05:00, Expires in: 10000 seconds",result[0].value[0].get("issue"))

    '''
        Test that we pick up near expiry dns zones in the output
        Test data:
        [
          {
            "zoneName":"yoel.com",
            "zoneType":"secondary",
            "view":"default",
            "mdate":"2024-02-08T09:47:10.961807-05:00",
            "expire":10,
            "expireIn":10000
          },
          {
            "zoneName":"yoel.ca",
            "zoneType":"stub",
            "view":"default",
            "mdate":"2024-02-08T09:47:10.961807-05:00"
          }
        ]
    '''
    def test_bdds_zone_expiring_two(self):
        result = self.parser.parse_file(self.current_dir + '/inspect-zone-expiry-two.output', {}, {})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].action_type, 'WriteComplexMetric')
        self.assertEqual(1,len(result[0].value))
        self.assertEqual("Zone: yoel.com, View: default, Updated At: 2024-02-08T09:47:10.961807-05:00, Expires in: 10000 seconds",result[0].value[0].get("issue"))
