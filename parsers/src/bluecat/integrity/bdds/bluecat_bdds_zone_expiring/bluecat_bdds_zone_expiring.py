from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class BluecatBddsDnsZoneExpiry(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data_parsed = helper_methods.parse_data_as_json(raw_data)
            if data_parsed:
                near_exp_zone = dict[str, []]()
                for z in data_parsed:
                    # at present, supported zoneTypes are: secondary and stub zones
                    if 'expire' not in z:
                        continue
                    near_exp_zone.setdefault(z["zoneType"], []).append(z)
                for zone_type in near_exp_zone:
                    tags, issues = self.__format_results(near_exp_zone[zone_type], f"{zone_type.capitalize()} Zones")
                    self.write_complex_metric_object_array('bcn-bdds-zone-expiring', tags, issues, False)

        return self.output

    def __format_results(self, zones, item_name):
        tags = {}
        issues = []
        tags['name'] = item_name
        for z in zones[:10]:
            issues.append({'issue': f"Zone: {z['zoneName']}, View: {z['view']}, Updated At: {z['mdate']}, Expires in: {z['expireIn']} seconds"})
        return tags, issues
