from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class BluecatXhaServiceState(BaseParser):
    """
    Parser of:
        MIB: BCN-HA-MIB
        node: bcnHaSerOperState
        OID: 1.3.6.1.4.1.13315.3.1.5.2.1.
    
    Current running state of xHA service:
        1—Running
        2—Not Running
        3—Starting
        4—Stopping
        5—Fault
    """
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            lines = raw_data.splitlines()
            for line in lines:
                if line.startswith('1.3.6.1.4.1.13315.3.1.5.2.1.1.0'):  # bcnHaSerOperState
                    xha_state = int(line.split("= ")[1])
                    self.write_double_metric('xha-service-state', {}, 'gauge', 1 if xha_state in [1,2,3] else 0, True, 'HA Service Status', 'state')
        return self.output