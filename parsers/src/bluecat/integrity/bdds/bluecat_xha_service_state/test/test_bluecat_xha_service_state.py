import os
import unittest
from bluecat.integrity.bdds.bluecat_xha_service_state.bluecat_xha_service_state import BluecatXhaServiceState
from parser_service.public.action import *

class TestBluecatXhaServiceState(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = BluecatXhaServiceState()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))    
    
    def test_xha_service_standalone(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_xha_service_standalone.input', {}, {})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'xha-service-state')
        self.assertEqual(result[0].value, 1)

    def test_xha_service_active(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_xha_service_active.input', {}, {})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'xha-service-state')
        self.assertEqual(result[0].value, 1)

    def test_xha_service_passive(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_xha_service_passive.input', {}, {})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'xha-service-state')
        self.assertEqual(result[0].value, 1)

    def test_xha_service_down(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_xha_service_down.input', {}, {})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'xha-service-state')
        self.assertEqual(result[0].value, 0)

if __name__ == '__main__':
    unittest.main()