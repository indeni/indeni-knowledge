from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class BluecatConcurrentConnectionLimitNearing(BaseParser):
    
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            parsed_data = helper_methods.parse_data_as_list(raw_data, 'bluecat_concurrent_connection_limit_nearing.textfsm')
            if parsed_data:
                total_connections = int(parsed_data[0].get('total'))
                self.write_double_metric('concurrent-connections', {}, 'gauge', total_connections, True, "Concurrent connections", "number")
        return self.output