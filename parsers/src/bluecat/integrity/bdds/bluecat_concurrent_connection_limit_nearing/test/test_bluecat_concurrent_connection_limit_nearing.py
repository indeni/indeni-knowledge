import os
import unittest
from bluecat.integrity.bdds.bluecat_concurrent_connection_limit_nearing.bluecat_concurrent_connection_limit_nearing import BluecatConcurrentConnectionLimitNearing
from parser_service.public.action import *

class TestBluecatConcurrentConcurrentConnectionLimitNearing(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = BluecatConcurrentConnectionLimitNearing()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))    
    
    def test_bluecat_concurrent_connection_limit_low(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_concurrent_connection_limit_low.input', {}, {})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'concurrent-connections')
        self.assertEqual(result[0].tags['live-config'], 'true')
        self.assertEqual(result[0].tags['display-name'], 'Concurrent connections')
        self.assertEqual(result[0].tags['im.dstype.displayType'], 'number')
        self.assertEqual(result[0].value, 156)

    def test_bluecat_concurrent_connection_limit_high(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_concurrent_connection_limit_high.input', {}, {})
        self.assertEqual(1,len(result))    
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'concurrent-connections')
        self.assertEqual(result[0].tags['live-config'], 'true')
        self.assertEqual(result[0].tags['display-name'], 'Concurrent connections')
        self.assertEqual(result[0].tags['im.dstype.displayType'], 'number')
        self.assertEqual(result[0].value, 1200)

if __name__ == '__main__':
    unittest.main()