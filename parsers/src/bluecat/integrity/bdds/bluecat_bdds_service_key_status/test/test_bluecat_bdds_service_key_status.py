import os
import unittest
from bluecat.integrity.bdds.bluecat_bdds_service_key_status.bluecat_bdds_service_key_status import BDDSServiceKeyStatus
from parser_service.public.action import *


class TestBDDSServiceKeyStatus(unittest.TestCase):
    def setUp(self):
        # Arrange
        self.parser = BDDSServiceKeyStatus()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_bluecat_bdds_service_key_status_missing(self):
        result = self.parser.parse_file(
            self.current_dir + '/bdds_service_key_status_sample1.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'bdds-service-key-status')
        self.assertEqual(result[0].tags['name'], 'unknown')
        self.assertEqual(result[0].tags['display-name'],
                         'Service Keys Invalid/Missing')
        self.assertEqual(result[0].tags['im.dstype.displayType'], 'Number')
        self.assertEqual(result[0].value, 0)

    def test_bluecat_bdds_service_key_status_invalid(self):
        result = self.parser.parse_file(
            self.current_dir + '/bdds_service_key_status_sample2.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'bdds-service-key-status')
        self.assertEqual(result[0].tags['name'], 'unknown')
        self.assertEqual(result[0].tags['display-name'],
                         'Service Keys Invalid/Missing')
        self.assertEqual(result[0].tags['im.dstype.displayType'], 'Number')
        self.assertEqual(result[0].value, 0)

    def test_bluecat_bdds_service_key_status_valid(self):
        result = self.parser.parse_file(
            self.current_dir + '/bdds_service_key_status_sample3.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'bdds-service-key-status')
        self.assertEqual(result[0].tags['name'], 'success')
        self.assertEqual(result[0].tags['display-name'],
                         'Service Keys Valid')
        self.assertEqual(result[0].tags['im.dstype.displayType'], 'Number')
        self.assertEqual(result[0].value, 1)

    def test_bluecat_bdds_service_key_status_command_error(self):
        result = self.parser.parse_file(
            self.current_dir + '/bdds_service_key_status_sample4.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'bdds-service-key-status')
        self.assertEqual(result[0].tags['name'], 'command-error')
        self.assertEqual(result[0].tags['display-name'],
                         'Command Error')
        self.assertEqual(result[0].tags['im.dstype.displayType'], 'Number')
        self.assertEqual(result[0].value, 0)


if __name__ == '__main__':
    unittest.main()