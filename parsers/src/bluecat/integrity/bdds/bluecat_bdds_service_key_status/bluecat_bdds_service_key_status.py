from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class BDDSServiceKeyStatus(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data and str(raw_data).strip().split('\n')[-1]=='0':
            data = helper_methods.parse_data_as_list(
                raw_data, 'bluecat_bdds_service_key_status.textfsm')
            if data:
                for row in data:
                    error_type = row.get('error_type').strip().lower()
                    server_type = row.get('server_type').strip().lower()
                
                    if server_type != 'unknown' and error_type == '':
                        # When service keys is valid
                        self.write_double_metric('bdds-service-key-status', {
                                                    'name': 'success' }, 'gauge', 1, False, 'Service Keys Valid', 'Number')
                    elif server_type == 'unknown' and error_type != '':
                        # When service keys is invalid or missing
                        self.write_double_metric('bdds-service-key-status', {
                            'name': 'unknown'}, 'gauge', 0, False, 'Service Keys Invalid/Missing', 'Number')
        else:
            ## When servtype command failed to run
            self.write_double_metric(
                'bdds-service-key-status', {'name': 'command-error'}, 'gauge', 0, False, 'Command Error', 'Number')
        return self.output