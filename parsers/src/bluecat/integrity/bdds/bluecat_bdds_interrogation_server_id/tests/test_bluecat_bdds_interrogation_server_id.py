import unittest
from bluecat.integrity.bdds.bluecat_bdds_interrogation_server_id.bluecat_bdds_interrogation_server_id import BluecatBddsInterrogationServerId

class TestBluecatBddsInterrogationServerId(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = BluecatBddsInterrogationServerId()

    def test_bdds_interrogation_blank_server_id(self):
        result = self.parser.parse("", {}, {})
        self.assertEqual(0, len(result))

    def test_bdds_interrogation_server_id(self):
        result = self.parser.parse("\r420eda18-f0a0-458d-1ee0-8fbd6935fd12", {}, {})
        self.assertEqual(1,len(result))
        self.assertEqual('WriteTag', result[0].action_type)
        self.assertEqual("server-id",result[0].key)
        self.assertEqual("420eda18-f0a0-458d-1ee0-8fbd6935fd12",result[0].value)


if __name__ == '__main__':
    unittest.main()