from parser_service.public.base_parser import BaseParser


class BluecatBddsInterrogationServerId(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            server_id = raw_data.strip()
            if len(server_id) > 0:
                self.write_tag("server-id", str(server_id))

        return self.output