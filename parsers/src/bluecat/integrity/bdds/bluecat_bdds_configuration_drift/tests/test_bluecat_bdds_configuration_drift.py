import os
import unittest
from bluecat.integrity.bdds.bluecat_bdds_configuration_drift.bluecat_bdds_configuration_drift import BluecatBddsConfigurationDrift
from parser_service.public.action import *

class TestBluecatBddsConfigurationDrift(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = BluecatBddsConfigurationDrift()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))    

    def test_bdds_failover_dns_nok_dhcp_nok(self):
        result = self.parser.parse_file(self.current_dir + '/bdds_failover_dns_nok_dhcp_nok.input', {}, {})
        self.assertEqual(2,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'config-drift-status')
        self.assertEqual(result[0].tags['name'],'DNS')
        self.assertEqual(result[0].value, 0)
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].name, 'config-drift-status')
        self.assertEqual(result[1].tags['name'],'DHCP')
        self.assertEqual(result[1].value, 0)

    def test_bdds_failover_dns_nok_dhcp_ok(self):
        result = self.parser.parse_file(self.current_dir + '/bdds_failover_dns_nok_dhcp_ok.input', {}, {})
        self.assertEqual(2,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'config-drift-status')
        self.assertEqual(result[0].tags['name'],'DNS')
        self.assertEqual(result[0].value, 0)
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].name, 'config-drift-status')
        self.assertEqual(result[1].tags['name'],'DHCP')
        self.assertEqual(result[1].value, 1)

    def test_bdds_failover_dns_ok_dhcp_nok(self):
        result = self.parser.parse_file(self.current_dir + '/bdds_failover_dns_ok_dhcp_nok.input', {}, {})
        self.assertEqual(2,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'config-drift-status')
        self.assertEqual(result[0].tags['name'],'DNS')
        self.assertEqual(result[0].value, 1)
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].name, 'config-drift-status')
        self.assertEqual(result[1].tags['name'],'DHCP')
        self.assertEqual(result[1].value, 0)

    def test_bdds_failover_dns_ok_dhcp_ok(self):
        result = self.parser.parse_file(self.current_dir + '/bdds_failover_dns_ok_dhcp_ok.input', {}, {})
        self.assertEqual(2,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'config-drift-status')
        self.assertEqual(result[0].tags['name'],'DNS')
        self.assertEqual(result[0].value, 1)
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].name, 'config-drift-status')
        self.assertEqual(result[1].tags['name'],'DHCP')
        self.assertEqual(result[1].value, 1)
    
    def test_bdds_xha_active_dns_nok(self):
        result = self.parser.parse_file(self.current_dir + '/bdds_xha_active_dns_nok.input', {}, {})
        self.assertEqual(2,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'config-drift-status')
        self.assertEqual(result[0].tags['name'],'DNS')
        self.assertEqual(result[0].value, 0)
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].name, 'config-drift-status')
        self.assertEqual(result[1].tags['name'],'DHCP')
        self.assertEqual(result[1].value, 1)

    def test_bdds_xha_active_dns_ok(self):
        result = self.parser.parse_file(self.current_dir + '/bdds_xha_active_dns_ok.input', {}, {})
        self.assertEqual(2,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'config-drift-status')
        self.assertEqual(result[0].tags['name'],'DNS')
        self.assertEqual(result[0].value, 1)
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].name, 'config-drift-status')
        self.assertEqual(result[1].tags['name'],'DHCP')
        self.assertEqual(result[1].value, 1)

    def test_bdds_xha_passive(self):
        result = self.parser.parse_file(self.current_dir + '/bdds_xha_passive.input', {}, {})
        self.assertEqual(2,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'config-drift-status')
        self.assertEqual(result[0].tags['name'],'DNS')
        self.assertEqual(result[0].value, 1)
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].name, 'config-drift-status')
        self.assertEqual(result[1].tags['name'],'DHCP')
        self.assertEqual(result[1].value, 1)


if __name__ == '__main__':
    unittest.main()