from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class BluecatBddsConfigurationDrift(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data_parsed = helper_methods.parse_data_as_list(raw_data, 'bluecat_bdds_configuration_drift.textfsm')
            if 'ACTIVE' in data_parsed[0]['node_state']:
                    if data_parsed[0]['dns_enable'] == '1':
                        self.write_double_metric('config-drift-status', {'name':'DNS'}, 'gauge', 1 if data_parsed[0]['dns_diff'] == 'identical' else 0, False)
                    else:
                        self.write_double_metric('config-drift-status', {'name':'DNS'}, 'gauge', 1, False)
                    if data_parsed[0]['dhcp_enable'] == '1':
                        self.write_double_metric('config-drift-status', {'name':'DHCP'}, 'gauge', 1 if data_parsed[0]['dhcp_diff'] == 'identical' else 0, False)
                    else:
                        self.write_double_metric('config-drift-status', {'name':'DHCP'}, 'gauge', 1, False)
            else: #if (data_parsed[0]['xha_mode'] == '0' and data_parsed[0]['node_state'] == 'NODE_HA_PASSIVE'):
                self.write_double_metric('config-drift-status', {'name':'DNS'}, 'gauge', 1, False)
                self.write_double_metric('config-drift-status', {'name':'DHCP'}, 'gauge', 1, False)
        return self.output