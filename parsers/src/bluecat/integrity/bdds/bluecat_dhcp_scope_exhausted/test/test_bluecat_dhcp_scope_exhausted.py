import os
import unittest
from bluecat.integrity.bdds.bluecat_dhcp_scope_exhausted.bluecat_dhcp_scope_exhausted import BluecatDHCPScopeExhausted
from parser_service.public.action import *


class TestBluecatDHCPScopeExhausted(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = BluecatDHCPScopeExhausted()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))    
    
    def test_dhcp_scope_exhausted_empty(self):
        result = self.parser.parse_file(
            self.current_dir + '/dhcp_scope_exhausted_empty.input', {}, {})
        self.assertEqual(0,len(result))

    def test_dhcp_scope_exhausted_single_scope_no_exhausted(self):
        result = self.parser.parse_file(
            self.current_dir + '/dhcp_scope_exhausted_single_scope_no_exhausted.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'bdds-dhcp-scope-exhausted')
        self.assertEqual(result[0].value, 33.333333333333336)
    
    def test_dhcp_scope_exhausted_single_scope_single_exhausted(self):
        result = self.parser.parse_file(
            self.current_dir + '/dhcp_scope_exhausted_single_scope_single_exhausted.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'bdds-dhcp-scope-exhausted')
        self.assertEqual(result[0].value, 100)
    
    def test_dhcp_scope_exhausted_double_scope_no_exhausted(self):
        result = self.parser.parse_file(
            self.current_dir + '/dhcp_scope_exhausted_double_scope_no_exhausted.input', {}, {})
        self.assertEqual(2, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'bdds-dhcp-scope-exhausted')
        self.assertEqual(result[0].value, 33.333333333333336)
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].name, 'bdds-dhcp-scope-exhausted')
        self.assertEqual(result[1].value, 0)
    
    def test_dhcp_scope_exhausted_double_scope_single_exhausted(self):
        result = self.parser.parse_file(
            self.current_dir + '/dhcp_scope_exhausted_double_scope_single_exhausted.input', {}, {})
        self.assertEqual(2,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'bdds-dhcp-scope-exhausted')
        self.assertEqual(result[0].value, 100)
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].name, 'bdds-dhcp-scope-exhausted')
        self.assertEqual(result[1].value, 0)

    def test_dhcp_scope_exhausted_double_scope_double_exhausted(self):
        result = self.parser.parse_file(
            self.current_dir + '/dhcp_scope_exhausted_double_scope_double_exhausted.input', {}, {})
        self.assertEqual(2,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'bdds-dhcp-scope-exhausted')
        self.assertEqual(result[0].value, 100)
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].name, 'bdds-dhcp-scope-exhausted')
        self.assertEqual(result[1].value, 88)

if __name__ == '__main__':
    unittest.main()