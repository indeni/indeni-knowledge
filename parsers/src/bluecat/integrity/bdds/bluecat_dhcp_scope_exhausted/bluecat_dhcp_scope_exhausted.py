from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser
import sys

class BluecatDHCPScopeExhausted(BaseParser):
    """
    Parser of:
        OID: .1.3.6.1.4.1.13315.3.1.1.2.2.3.1.4
    Description:    
        .bcnDhcpv4PoolSize = The size of the pool
        
    Parser of:
        OID:  .1.3.6.1.4.1.13315.3.1.1.2.2.3.1.5
    Description:    
        .bcnDhcpv4PoolFreeAddresses = The number of IP addresses available in the pool
}
    """
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            lines = raw_data.splitlines()
            scopes_size = {}
            scopes_free_slots = {}

            try:
                if len(lines) % 2 == 0:
                    # Only parse if every Scope in the range has one line with its Size and one line with its Free Slots
                    for line in lines:
                        if line.startswith('1.3.6.1.4.1.13315.3.1.1.2.2.3.1.4.'):
                            # Add each scope with pool size to dict
                            scope = line.split(' = ')[0].replace(
                                '1.3.6.1.4.1.13315.3.1.1.2.2.3.1.4.', '')
                            pool_size = int(line.split(' = ')[1])
                            if pool_size != 0:
                                # If Pool size is ZERO skip adding that Scope
                                scopes_size[scope] = pool_size
                        
                        if line.startswith('1.3.6.1.4.1.13315.3.1.1.2.2.3.1.5.'):
                            # Add each scope with available slots to dict
                            scope = line.split(' = ')[0].replace(
                                '1.3.6.1.4.1.13315.3.1.1.2.2.3.1.5.', '')
                            free_slots = int(line.split(' = ')[1])
                            if scope in scopes_size.keys():
                                # Only add if Scope already exists
                                scopes_free_slots[scope] = free_slots
            except Exception as e:
                print('Error parsing output')
                sys.exit(e.args[0])

            for scope in scopes_size.keys():
                if (scopes_size[scope] - 1)/scopes_size[scope] < 0.85 and scopes_free_slots[scope] == 0:
                    # Upto a scope size of 7 there will be 1 free slot when 85% is filled. Hence we choose this arbitrary number 7.
                    # ******************************************** #
                    # * ONLY CRITICAL ALERT in SMALL DHCP Scopes * #
                    # ******************************************** #
                    # Raise CRITICAL alert when: Scope size < 7 and NO Free slots in Pool
                    self.write_double_metric('bdds-dhcp-scope-exhausted', {
                        'name': f'{scope} [{scopes_size[scope] - scopes_free_slots[scope]}/{scopes_size[scope]} Used]'}, 'gauge', 100, False, f'{scope} Scope Exhausted', 'Number')
                elif scopes_size[scope] >= 7 and scopes_free_slots[scope] == 0:
                    # Raise CRITICAL alert when: Scope size >= 7 and NO Free slots in Pool
                    self.write_double_metric('bdds-dhcp-scope-exhausted', {
                        'name': f'{scope} [{scopes_size[scope] - scopes_free_slots[scope]}/{scopes_size[scope]} Used]'}, 'gauge', 100, False, f'{scope} Scope Exhausted', 'Number')
                elif scopes_size[scope] >= 7 and scopes_free_slots[scope]/scopes_size[scope] < 0.15:
                    # Raise WARN alert: Scope size >= 7 and Free slots in Pool < 15%
                    self.write_double_metric('bdds-dhcp-scope-exhausted', {
                        'name': f'{scope} [{scopes_size[scope] - scopes_free_slots[scope]}/{scopes_size[scope]} Used]'}, 'gauge', (scopes_size[scope] - scopes_free_slots[scope])*100/scopes_size[scope], False, f'{scope} Scope Nearing Exhaustion', 'Number')
                else:
                    # When there is enough free slots
                    self.write_double_metric('bdds-dhcp-scope-exhausted', {
                        'name': f'{scope} [{scopes_size[scope] - scopes_free_slots[scope]}/{scopes_size[scope]} Used]'}, 'gauge', (scopes_size[scope] - scopes_free_slots[scope])*100/scopes_size[scope], False, f'{scope} Scope Has Available IPs', 'Number')
        return self.output