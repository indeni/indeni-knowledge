src_ip=$(egrep -e 'primary;|secondary;' /etc/dhcpd.conf -A1| awk '/address/ {print $2}'| awk 'BEGIN{FS=";"} {print $1}')
dst_ip=$(grep 'peer address' /etc/dhcpd.conf | awk '{print $3}'| awk 'BEGIN{FS=";"} {print $1}')
dst_port=$(grep 'peer port' /etc/dhcpd.conf | awk '{print $3}'| awk 'BEGIN{FS=";"} {print $1}')
nc -zvn -s $src_ip $dst_ip $dst_port