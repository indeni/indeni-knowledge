from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class BluecatDhcpFailoverConnectionFailure(BaseParser):
    
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            parsed_data = helper_methods.parse_data_as_list(raw_data, 'bluecat_dhcp_failover_connection_failure.textfsm')
            if parsed_data:
                self.write_double_metric('failover-connection-status', {'TCP': parsed_data[0]['port']}, 'gauge', 1 if (parsed_data[0]['status']).lower() == 'open' else 0, False)
        return self.output