import os
import unittest
from bluecat.integrity.bdds.bluecat_dhcp_failover_connection_failure.bluecat_dhcp_failover_connection_failure import BluecatDhcpFailoverConnectionFailure
from parser_service.public.action import *

class TestBluecatDhcpFailoverConnectionFailure(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = BluecatDhcpFailoverConnectionFailure()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))    
    
    def test_dhcp_failover_connection_primary(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_failover_connection_primary.input', {}, {})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'failover-connection-status')
        self.assertEqual(result[0].tags['TCP'], '647')
        self.assertEqual(result[0].value, 0)

    def test_dhcp_failover_connection_secondary(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_failover_connection_secondary.input', {}, {})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'failover-connection-status')
        self.assertEqual(result[0].tags['TCP'], '847')
        self.assertEqual(result[0].value, 0)

    def test_dhcp_failover_connection_ok(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_failover_connection_ok.input', {}, {})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'failover-connection-status')
        self.assertEqual(result[0].tags['TCP'], '647')
        self.assertEqual(result[0].value, 1)

if __name__ == '__main__':
    unittest.main()