import os
import unittest
from bluecat.integrity.bdds.bluecat_failover_state.bluecat_failover_state import BluecatFailoverState
from parser_service.public.action import *

class TestBluecatFailoverState(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = BluecatFailoverState()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))    
        
    def test_failover_member_startup(self):
        result = self.parser.parse_file(self.current_dir + '/failover_member_startup.input', {}, {})
        self.assertEqual(3,len(result))
        self.assertEqual(result[0].action_type, 'WriteComplexMetric')
        self.assertEqual(result[0].name, 'bluecat-ha-dhcp-failover-state')
        self.assertEqual(result[0].tags['name'], 'Failover state')
        self.assertEqual(result[0].value['value'], 'startup')
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].name, 'bluecat-dhcp-failover-cluster-state')
        self.assertEqual(result[1].value, 1)
        self.assertEqual(result[2].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[2].name, 'bluecat-dhcp-failover-cluster-member-active')
        self.assertEqual(result[2].tags['name'], 'Failover')
        self.assertEqual(result[2].value, 1)

    def test_failover_member_normal(self):
        result = self.parser.parse_file(self.current_dir + '/failover_member_normal.input', {}, {})
        self.assertEqual(3,len(result))
        self.assertEqual(result[0].action_type, 'WriteComplexMetric')
        self.assertEqual(result[0].name, 'bluecat-ha-dhcp-failover-state')
        self.assertEqual(result[0].tags['name'], 'Failover state')
        self.assertEqual(result[0].value['value'], 'normal')
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].name, 'bluecat-dhcp-failover-cluster-state')
        self.assertEqual(result[1].value, 1)
        self.assertEqual(result[2].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[2].name, 'bluecat-dhcp-failover-cluster-member-active')
        self.assertEqual(result[2].tags['name'], 'Failover')
        self.assertEqual(result[2].value, 1)

    def test_failover_member_communications_interrupted(self):
        result = self.parser.parse_file(self.current_dir + '/failover_member_communications_interrupted.input', {}, {})
        self.assertEqual(3,len(result))
        self.assertEqual(result[0].action_type, 'WriteComplexMetric')
        self.assertEqual(result[0].name, 'bluecat-ha-dhcp-failover-state')
        self.assertEqual(result[0].tags['name'], 'Failover state')
        self.assertEqual(result[0].value['value'], 'communicationsInterrupted')
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].name, 'bluecat-dhcp-failover-cluster-state')
        self.assertEqual(result[1].value, 0)
        self.assertEqual(result[2].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[2].name, 'bluecat-dhcp-failover-cluster-member-active')
        self.assertEqual(result[2].tags['name'], 'Failover')
        self.assertEqual(result[2].value, 1)

    def test_failover_member_partner_down(self):
        result = self.parser.parse_file(self.current_dir + '/failover_member_partner_down.input', {}, {})
        self.assertEqual(3,len(result))
        self.assertEqual(result[0].action_type, 'WriteComplexMetric')
        self.assertEqual(result[0].name, 'bluecat-ha-dhcp-failover-state')
        self.assertEqual(result[0].tags['name'], 'Failover state')
        self.assertEqual(result[0].value['value'], 'partnerDown')
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].name, 'bluecat-dhcp-failover-cluster-state')
        self.assertEqual(result[1].value, 0)
        self.assertEqual(result[2].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[2].name, 'bluecat-dhcp-failover-cluster-member-active')
        self.assertEqual(result[2].tags['name'], 'Failover')
        self.assertEqual(result[2].value, 1)

    def test_failover_member_potential_conflict(self):
        result = self.parser.parse_file(self.current_dir + '/failover_member_potential_conflict.input', {}, {})
        self.assertEqual(3,len(result))
        self.assertEqual(result[0].action_type, 'WriteComplexMetric')
        self.assertEqual(result[0].name, 'bluecat-ha-dhcp-failover-state')
        self.assertEqual(result[0].tags['name'], 'Failover state')
        self.assertEqual(result[0].value['value'], 'potentialConflict')
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].name, 'bluecat-dhcp-failover-cluster-state')
        self.assertEqual(result[1].value, 0)
        self.assertEqual(result[2].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[2].name, 'bluecat-dhcp-failover-cluster-member-active')
        self.assertEqual(result[2].tags['name'], 'Failover')
        self.assertEqual(result[2].value, 0)     

    def test_failover_member_recover(self):
        result = self.parser.parse_file(self.current_dir + '/failover_member_recover.input', {}, {})
        self.assertEqual(3,len(result))
        self.assertEqual(result[0].action_type, 'WriteComplexMetric')
        self.assertEqual(result[0].name, 'bluecat-ha-dhcp-failover-state')
        self.assertEqual(result[0].tags['name'], 'Failover state')
        self.assertEqual(result[0].value['value'], 'recover')
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].name, 'bluecat-dhcp-failover-cluster-state')
        self.assertEqual(result[1].value, 0)
        self.assertEqual(result[2].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[2].name, 'bluecat-dhcp-failover-cluster-member-active')
        self.assertEqual(result[2].tags['name'], 'Failover')
        self.assertEqual(result[2].value, 0)    

    def test_failover_member_paused(self):
        result = self.parser.parse_file(self.current_dir + '/failover_member_paused.input', {}, {})
        self.assertEqual(3,len(result))
        self.assertEqual(result[0].action_type, 'WriteComplexMetric')
        self.assertEqual(result[0].name, 'bluecat-ha-dhcp-failover-state')
        self.assertEqual(result[0].tags['name'], 'Failover state')
        self.assertEqual(result[0].value['value'], 'paused')
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].name, 'bluecat-dhcp-failover-cluster-state')
        self.assertEqual(result[1].value, 0)
        self.assertEqual(result[2].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[2].name, 'bluecat-dhcp-failover-cluster-member-active')
        self.assertEqual(result[2].tags['name'], 'Failover')
        self.assertEqual(result[2].value, 0) 

    def test_failover_member_shutdown(self):
        result = self.parser.parse_file(self.current_dir + '/failover_member_shutdown.input', {}, {})
        self.assertEqual(3,len(result))
        self.assertEqual(result[0].action_type, 'WriteComplexMetric')
        self.assertEqual(result[0].name, 'bluecat-ha-dhcp-failover-state')
        self.assertEqual(result[0].tags['name'], 'Failover state')
        self.assertEqual(result[0].value['value'], 'shutdown')
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].name, 'bluecat-dhcp-failover-cluster-state')
        self.assertEqual(result[1].value, 0)
        self.assertEqual(result[2].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[2].name, 'bluecat-dhcp-failover-cluster-member-active')
        self.assertEqual(result[2].tags['name'], 'Failover')
        self.assertEqual(result[2].value, 0)

    def test_failover_member_recover_done(self):
        result = self.parser.parse_file(self.current_dir + '/failover_member_recover_done.input', {}, {})
        self.assertEqual(3,len(result))
        self.assertEqual(result[0].action_type, 'WriteComplexMetric')
        self.assertEqual(result[0].name, 'bluecat-ha-dhcp-failover-state')
        self.assertEqual(result[0].tags['name'], 'Failover state')
        self.assertEqual(result[0].value['value'], 'recoverDone')
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].name, 'bluecat-dhcp-failover-cluster-state')
        self.assertEqual(result[1].value, 0)
        self.assertEqual(result[2].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[2].name, 'bluecat-dhcp-failover-cluster-member-active')
        self.assertEqual(result[2].tags['name'], 'Failover')
        self.assertEqual(result[2].value, 0)

    def test_failover_member_recover_wait(self):
        result = self.parser.parse_file(self.current_dir + '/failover_member_recover_wait.input', {}, {})
        self.assertEqual(3,len(result))
        self.assertEqual(result[0].action_type, 'WriteComplexMetric')
        self.assertEqual(result[0].name, 'bluecat-ha-dhcp-failover-state')
        self.assertEqual(result[0].tags['name'], 'Failover state')
        self.assertEqual(result[0].value['value'], 'recoverWait')
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].name, 'bluecat-dhcp-failover-cluster-state')
        self.assertEqual(result[1].value, 0)
        self.assertEqual(result[2].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[2].name, 'bluecat-dhcp-failover-cluster-member-active')
        self.assertEqual(result[2].tags['name'], 'Failover')
        self.assertEqual(result[2].value, 0)

    def test_failover_member_missing_file(self):
        result = self.parser.parse_file(self.current_dir + '/failover_member_missing_file.input', {}, {})
        self.assertEqual(0,len(result))

if __name__ == '__main__':
    unittest.main()