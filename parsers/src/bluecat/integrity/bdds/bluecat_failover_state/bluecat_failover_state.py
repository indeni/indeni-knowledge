from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class BluecatFailoverState(BaseParser):

    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        mapping = {
                '1' : 'startup',
                '2' : 'normal',
                '3' : 'communicationsInterrupted',
                '4' : 'partnerDown',
                '5' : 'potentialConflict',
                '6' : 'recover',
                '7' : 'paused',
                '8' : 'shutdown',
                '9' : 'recoverDone',
                '254' : 'recoverWait'
            }
        if raw_data:
            try:
                self.write_complex_metric_string('bluecat-ha-dhcp-failover-state', {'name': 'Failover state'}, mapping[raw_data.strip()], False)
                self.write_double_metric('bluecat-dhcp-failover-cluster-state', {}, 'gauge', 1 if raw_data.strip() in ['1','2'] else 0, True, 'Cluster State', 'state')
                self.write_double_metric('bluecat-dhcp-failover-cluster-member-active', {'name' : 'Failover'}, 'gauge', 1 if raw_data.strip() in ['1','2','3','4'] else 0,False)
            except:
                pass
        return self.output