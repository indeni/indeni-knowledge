import os
import unittest
from bluecat.integrity.bdds.bluecat_command_server_service_state.bluecat_command_server_service_state import BluecatCommandServerServiceState
from parser_service.public.action import *

class TestBluecatCommandServerServiceState(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = BluecatCommandServerServiceState()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))    
    
    def test_command_server_service_up(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_command_server_service_up.input', {}, {})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'command-server-state')
        self.assertEqual(result[0].value, 1)

    def test_command_server_service_down(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_command_server_service_down.input', {}, {})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'command-server-state')
        self.assertEqual(result[0].value, 0)

if __name__ == '__main__':
    unittest.main()