from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class BluecatCommandServerServiceState(BaseParser):
    """
    Parser of:
        MIB: BCN-COMMANDSERVER-MIB
        node: bcnCommandServerSerOperState
        OID: 1.3.6.1.4.1.13315.3.1.7.2.1.1.0
    """
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            lines = raw_data.splitlines()
            for line in lines:
                if line.startswith('1.3.6.1.4.1.13315.3.1.7.2.1.1.0 '):  # bcnCommandServerSerOperState
                    command_server_status = int(line.split("= ")[1])
                    self.write_double_metric('command-server-state', {}, 'gauge', 0 if command_server_status in [2, 4, 5] else 1, False)
        return self.output