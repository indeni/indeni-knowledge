from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class BluecatKernelConnectionTrackingNearingLimit(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            parsed_data = helper_methods.parse_data_as_list(raw_data, 'bluecat_kernel_connection_tracking_nearing_limit.textfsm')
            if parsed_data:
                result_max = int(parsed_data[0].get('conntrack_max'))
                result_count = int(parsed_data[0].get('conntrack_count'))
                result_percentage = (result_count/result_max)*100
                self.write_double_metric('kernel-connection-table-percentage', {}, 'gauge', result_percentage, True, "Kernel connection usage", "percentage")
        return self.output