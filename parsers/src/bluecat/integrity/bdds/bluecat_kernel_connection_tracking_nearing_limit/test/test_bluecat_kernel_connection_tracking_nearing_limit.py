import os
import unittest
from bluecat.integrity.bdds.bluecat_kernel_connection_tracking_nearing_limit.bluecat_kernel_connection_tracking_nearing_limit import BluecatKernelConnectionTrackingNearingLimit
from parser_service.public.action import *

class TestBluecatKernelConnectionTracking(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = BluecatKernelConnectionTrackingNearingLimit()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))    
    
    def test_kernel_connection_tracking_conntrack_count_empty(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_kernel_connection_tracking_conntrack_count_empty.input', {}, {})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'kernel-connection-table-percentage')
        self.assertEqual(result[0].tags['live-config'], 'true')
        self.assertEqual(result[0].tags['display-name'], 'Kernel connection usage')
        self.assertEqual(result[0].tags['im.dstype.displayType'], 'percentage')
        self.assertEqual(result[0].value, 0.0)

    def test_kernel_connection_tracking_conntrack_count_not_empty(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_kernel_connection_tracking_conntrack_count_not_empty.input', {}, {})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'kernel-connection-table-percentage')
        self.assertEqual(result[0].tags['live-config'], 'true')
        self.assertEqual(result[0].tags['display-name'], 'Kernel connection usage')
        self.assertEqual(result[0].tags['im.dstype.displayType'], 'percentage')
        self.assertEqual(result[0].value, 6.0)

    def test_kernel_connection_tracking_conntrack_ok(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_kernel_connection_tracking_conntrack_ok.input', {}, {})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'kernel-connection-table-percentage')
        self.assertEqual(result[0].tags['live-config'], 'true')
        self.assertEqual(result[0].tags['display-name'], 'Kernel connection usage')
        self.assertEqual(result[0].tags['im.dstype.displayType'], 'percentage')
        self.assertEqual(result[0].value, 0.0)

if __name__ == '__main__':
    unittest.main()