from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class BluecatNamedMonState(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        namedmon_process = '/usr/bin/python3 /usr/local/bluecat/namedmon.py'
        namedmon_present = False
        tags = {'name': namedmon_process}
        if raw_data:
            ps_parse_dict = helper_methods.parse_data_as_list(raw_data, 'bluecat_bdds_namedmon_state.textfsm')
            if ps_parse_dict:
                if ps_parse_dict[0].get('dns_enabled') == '1':
                    for process in ps_parse_dict:
                        if namedmon_process in process['COMMAND']:
                            namedmon_present = True
                            namedmon_cpu = process.get('CPU')
                            namedmon_mem = process.get('MEM')
                            break
                    
                    if namedmon_present:
                        self.write_double_metric('namedmon-state', tags, 'gauge', 1, True, 'Critical Process(es)',
                                                 'state', 'name')
                        self.write_double_metric(
                            'process-cpu', tags, 'gauge', float(namedmon_cpu), False)
                        self.write_double_metric(
                            'process-memory', tags, 'gauge', float(namedmon_mem), False)
                    else:
                        self.write_double_metric('namedmon-state', tags, 'gauge', 0, True, 'Critical Process(es)',
                                                 'state', 'name')

        return self.output
