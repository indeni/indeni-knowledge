import os
import unittest
from bluecat.integrity.bdds.bluecat_bdds_namedmon_state.bluecat_bdds_namedmon_state import BluecatNamedMonState
from parser_service.public.action import WriteDoubleMetric


class TestBluecatNamedMonState(unittest.TestCase):
    def setUp(self):
        self.parser = BluecatNamedMonState()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_ps_aux_namedmon_present(self):
        result = self.parser.parse_file(self.current_dir + '/ps_aux_BDDS_namedmon_good.input', {}, {'os.name': 'bdds','role-dns' : "true"})
        # Assert
        self.assertEqual(3, len(result))
        self.assertTrue(isinstance(result[0], WriteDoubleMetric))
        self.assertEqual('namedmon-state', result[0].name)
        self.assertEqual('/usr/bin/python3 /usr/local/bluecat/namedmon.py', result[0].tags['name'])
        self.assertEqual(1, result[0].value)

        self.assertTrue(isinstance(result[1], WriteDoubleMetric))
        self.assertEqual('process-cpu', result[1].name)
        self.assertEqual('/usr/bin/python3 /usr/local/bluecat/namedmon.py', result[1].tags['name'])
        self.assertEqual(1.2, result[1].value)

        self.assertTrue(isinstance(result[2], WriteDoubleMetric))
        self.assertEqual('process-memory', result[2].name)
        self.assertEqual('/usr/bin/python3 /usr/local/bluecat/namedmon.py', result[2].tags['name'])
        self.assertEqual(0.2, result[2].value)

    def test_ps_aux_namedmon_missing(self):
        result = self.parser.parse_file(self.current_dir + '/ps_aux_BDDS_namedmon_bad.input', {}, {'os.name': 'bdds','role-dns' : "true"})
        # Assert
        self.assertEqual(1, len(result))
        self.assertTrue(isinstance(result[0], WriteDoubleMetric))
        self.assertEqual('namedmon-state', result[0].name)
        self.assertEqual('/usr/bin/python3 /usr/local/bluecat/namedmon.py', result[0].tags['name'])
        self.assertEqual(0, result[0].value)
    def test_dns_disabled(self):
        result = self.parser.parse_file(self.current_dir + '/ps_aux_BDDS_dns_disabled.input', {}, {'os.name': 'bdds','role-dns' : "true"})
        # Assert
        self.assertEqual(0, len(result))
