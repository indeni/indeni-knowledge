import os
import unittest
from bluecat.integrity.bdds.bluecat_bdds_to_bam_connectivity.bluecat_bdds_to_bam_connectivity import BluecatBddsToBamConnectivity
from parser_service.public.action import *

class TestBluecatBddsToBamConnectivity(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = BluecatBddsToBamConnectivity()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))    
    
    def test_2_bam_connectivity_ok(self):
        result = self.parser.parse_file(self.current_dir + '/2_bam_connectivity_ok.input', {}, {})
        self.assertEqual(4,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].tags['name'], '10.255.253.141 (TCP 10045)')
        self.assertEqual(result[0].tags['live-config'], 'true')
        self.assertEqual(result[0].tags['display-name'], 'Connectivity - BDDS to BAM')
        self.assertEqual(result[0].value, 1)
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].tags['name'], '10.255.253.141 (TCP 10046)')
        self.assertEqual(result[1].tags['live-config'], 'true')
        self.assertEqual(result[1].tags['display-name'], 'Connectivity - BDDS to BAM')
        self.assertEqual(result[1].value, 1)
        self.assertEqual(result[2].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[2].tags['name'], '10.255.253.142 (TCP 10045)')
        self.assertEqual(result[2].tags['live-config'], 'true')
        self.assertEqual(result[2].tags['display-name'], 'Connectivity - BDDS to BAM')
        self.assertEqual(result[2].value, 1)
        self.assertEqual(result[3].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[3].tags['name'], '10.255.253.142 (TCP 10046)')
        self.assertEqual(result[3].tags['live-config'], 'true')
        self.assertEqual(result[3].tags['display-name'], 'Connectivity - BDDS to BAM')
        self.assertEqual(result[3].value, 1)

    def test_2_bam_connectivity_1_ok_1_nok(self):
        result = self.parser.parse_file(self.current_dir + '/2_bam_connectivity_1_ok_1_nok.input', {}, {})
        self.assertEqual(4,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].tags['name'], '10.255.253.141 (TCP 10045)')
        self.assertEqual(result[0].tags['live-config'], 'true')
        self.assertEqual(result[0].tags['display-name'], 'Connectivity - BDDS to BAM')
        self.assertEqual(result[0].value, 0)
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].tags['name'], '10.255.253.141 (TCP 10046)')
        self.assertEqual(result[1].tags['live-config'], 'true')
        self.assertEqual(result[1].tags['display-name'], 'Connectivity - BDDS to BAM')
        self.assertEqual(result[1].value, 1)
        self.assertEqual(result[2].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[2].tags['name'], '10.255.253.142 (TCP 10045)')
        self.assertEqual(result[2].tags['live-config'], 'true')
        self.assertEqual(result[2].tags['display-name'], 'Connectivity - BDDS to BAM')
        self.assertEqual(result[2].value, 1)
        self.assertEqual(result[3].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[3].tags['name'], '10.255.253.142 (TCP 10046)')
        self.assertEqual(result[3].tags['live-config'], 'true')
        self.assertEqual(result[3].tags['display-name'], 'Connectivity - BDDS to BAM')
        self.assertEqual(result[3].value, 1)

if __name__ == '__main__':
    unittest.main()