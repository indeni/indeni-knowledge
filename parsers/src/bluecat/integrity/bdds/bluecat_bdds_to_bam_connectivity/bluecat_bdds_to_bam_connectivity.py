from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class BluecatBddsToBamConnectivity(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data_parsed = helper_methods.parse_data_as_list(raw_data, 'bluecat_bdds_to_bam_connectivity.textfsm')
            if data_parsed:
                for connectivity_test in data_parsed:
                    tags = {}
                    tags['name'] = f"{connectivity_test['dst_ip']} (TCP {connectivity_test['port']})"
                    self.write_double_metric('bdds-to-bam-connectivity-status', tags, 'gauge', 1 if ('open' or 'succeeded') in connectivity_test['connectivity_result'] else 0, 
                                             True, 'Connectivity - BDDS to BAM', 'state', 'name')
        return self.output