from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser


class BluecatBddsDigBindVersion(BaseParser):
    
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            self.write_double_metric('bluecat-bdds-dns-server-resolution-state', {}, 'gauge', 0 if 'no servers could be reached' in raw_data  else 1, False)
        return self.output