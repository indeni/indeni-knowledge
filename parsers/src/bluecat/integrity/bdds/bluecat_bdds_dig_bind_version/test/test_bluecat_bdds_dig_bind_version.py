import os
import unittest
from bluecat.integrity.bdds.bluecat_bdds_dig_bind_version.bluecat_bdds_dig_bind_version import BluecatBddsDigBindVersion
from parser_service.public.action import *

class TestBluecatBddsDigBindVersion(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = BluecatBddsDigBindVersion()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))    
    
    def test_bind_no_response(self):
        result = self.parser.parse_file(self.current_dir + '/bind_no_response.input', {}, {})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'bluecat-bdds-dns-server-resolution-state')
        self.assertEqual(result[0].value, 0)

    def test_bind_response(self):
        result = self.parser.parse_file(self.current_dir + '/bind_response.input', {}, {})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'bluecat-bdds-dns-server-resolution-state')
        self.assertEqual(result[0].value, 1)

    def test_no_test_xha_secondary(self):
        result = self.parser.parse_file(self.current_dir + '/no_test_xha_secondary.input', {}, {})
        self.assertEqual(1,len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'bluecat-bdds-dns-server-resolution-state')
        self.assertEqual(result[0].value, 1)

if __name__ == '__main__':
    unittest.main()