from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser
import re


class BluecatPowerSuppliesStatus(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            # allowed_statuses are if a customer doesn't want a certain status to raise an alert (e.g. standby)
            allowed_statuses = ['Present']

            for line in raw_data.splitlines():
                ps_regex = re.search(r"^PS(?P<index>\d+) Status\s{2,}(?P<status>.*)\s{2,}", line)
                try:
                    power_supply_index = ps_regex.group('index').strip()
                    power_supply_status = re.sub(r"\s{2,}.*", "", ps_regex.group('status')).strip()
                except AttributeError:
                    continue

                tags = {"name": f"Power Supply {power_supply_index}"}
                issues = [{"Status": f"{power_supply_status}"}]
                if power_supply_status in allowed_statuses:
                    self.write_complex_metric_object_array('bluecat-power-supplies-status', tags, [], False)
                else:
                    self.write_complex_metric_object_array('bluecat-power-supplies-status', tags, issues, False)

        return self.output
