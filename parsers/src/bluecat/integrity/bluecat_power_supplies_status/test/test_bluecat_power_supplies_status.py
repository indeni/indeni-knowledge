import unittest
from bluecat.integrity.bluecat_power_supplies_status.bluecat_power_supplies_status import BluecatPowerSuppliesStatus
from parser_service.public.action import *


class TestBluecatPowerSuppliesStatus(unittest.TestCase):
    def setUp(self):
        # Arrange
        self.parser = BluecatPowerSuppliesStatus()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_power_supplies_status_fail(self):
        result = self.parser.parse_file(self.current_dir + '/power_supply_status_1_fail.input', {}, {})
        self.assertEqual(2, len(result))
        self.assertEqual(result[0].action_type, 'WriteComplexMetric')
        self.assertEqual(result[0].name, 'bluecat-power-supplies-status')
        self.assertEqual(result[0].tags['name'], "Power Supply 1")
        self.assertEqual(0, len(result[0].value))
        self.assertEqual(result[1].action_type, 'WriteComplexMetric')
        self.assertEqual(result[1].name, 'bluecat-power-supplies-status')
        self.assertEqual(result[1].tags['name'], "Power Supply 2")
        self.assertEqual(result[1].value[0]['Status'], 'Failure')

    def test_power_supplies_status_fail2(self):
        result = self.parser.parse_file(self.current_dir + '/power_supply_status_2_fail.input', {}, {})
        self.assertEqual(2, len(result))
        self.assertEqual(result[0].action_type, 'WriteComplexMetric')
        self.assertEqual(result[0].name, 'bluecat-power-supplies-status')
        self.assertEqual(result[0].tags['name'], "Power Supply 1")
        self.assertEqual(result[0].value[0]['Status'], 'Failure')
        self.assertEqual(result[1].tags['name'], "Power Supply 2")
        self.assertEqual(result[1].value[0]['Status'], 'Predictive Failure')

    def test_power_supplies_status_good(self):
        result = self.parser.parse_file(self.current_dir + '/power_supply_status_good.input', {}, {})
        self.assertEqual(2, len(result))
        self.assertEqual(result[0].action_type, 'WriteComplexMetric')
        self.assertEqual(result[0].name, 'bluecat-power-supplies-status')
        self.assertEqual(result[0].tags['name'], "Power Supply 1")
        self.assertEqual(0, len(result[0].value))
        self.assertEqual(result[1].action_type, 'WriteComplexMetric')
        self.assertEqual(result[1].name, 'bluecat-power-supplies-status')
        self.assertEqual(result[1].tags['name'], "Power Supply 2")
        self.assertEqual(0, len(result[1].value))

    def test_power_supplies_status_no_racadm(self):
        result = self.parser.parse_file(self.current_dir + '/power_supply_status_no_racadm.input', {}, {})
        self.assertEqual(0, len(result))

    def test_power_supplies_status_vm(self):
        result = self.parser.parse_file(self.current_dir + '/power_supply_status_vm.input', {}, {})
        self.assertEqual(0, len(result))


if __name__ == '__main__':
    unittest.main()
