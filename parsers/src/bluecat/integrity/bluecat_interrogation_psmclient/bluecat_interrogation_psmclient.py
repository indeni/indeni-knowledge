from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class BluecatInterrogationPsmclient(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            parsed_data = helper_methods.parse_data_as_list(raw_data, 'bluecat_interrogation_psmclient.textfsm')
            if parsed_data:
                self.write_tag('role-dns', "true" if int(parsed_data[0]['dns']) == 1 else "false" )
                self.write_tag('role-dhcp', "true" if int(parsed_data[0]['dhcp']) == 1 else "false" )
                if parsed_data[0].get('xha') is not None and parsed_data[0].get('xha') == '1':
                    self.write_tag('high-availability', "true")
                    self.write_tag('xha', "true")
                    if len(parsed_data[0]['vip_address']) > 0:
                        self.write_tag('cluster-id', parsed_data[0]['vip_address'] )
                if parsed_data[0].get('failover') is not None and parsed_data[0].get('failover') == '1':
                    self.write_tag('high-availability', "true")
                    self.write_tag('failover', "true")
        return self.output