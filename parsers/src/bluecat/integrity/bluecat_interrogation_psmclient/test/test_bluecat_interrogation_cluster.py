import os
import unittest
from bluecat.integrity.bluecat_interrogation_psmclient.bluecat_interrogation_psmclient import BluecatInterrogationPsmclient
from parser_service.public.action import *

class TestBluecatInterrogationPsmclient(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = BluecatInterrogationPsmclient()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))    
    
    def test_cluster_member_state_standalone(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_cluster_member_state_standalone.input', {}, {})
        self.assertEqual(2,len(result))
        self.assertEqual(result[0].action_type, 'WriteTag')
        self.assertEqual(result[0].key, 'role-dns')
        self.assertEqual(result[0].value, 'true')
        self.assertEqual(result[1].action_type, 'WriteTag')
        self.assertEqual(result[1].key, 'role-dhcp')
        self.assertEqual(result[1].value, 'false')


    def test_cluster_member_state_active(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_cluster_member_state_active.input', {}, {})
        self.assertEqual(4,len(result))
        self.assertEqual(result[0].action_type, 'WriteTag')
        self.assertEqual(result[0].key, 'role-dns')
        self.assertEqual(result[0].value, 'true')
        self.assertEqual(result[1].action_type, 'WriteTag')
        self.assertEqual(result[1].key, 'role-dhcp')
        self.assertEqual(result[1].value, 'false')
        self.assertEqual(result[2].action_type, 'WriteTag')
        self.assertEqual(result[2].key, 'high-availability')
        self.assertEqual(result[2].value, 'true')
        self.assertEqual(result[3].action_type, 'WriteTag')
        self.assertEqual(result[3].key, 'xha')
        self.assertEqual(result[3].value, 'true')
        
    def test_cluster_member_state_active_dhcp(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_cluster_member_state_active_dhcp.input', {}, {})
        self.assertEqual(4,len(result))
        self.assertEqual(result[0].action_type, 'WriteTag')
        self.assertEqual(result[0].key, 'role-dns')
        self.assertEqual(result[0].value, 'true')
        self.assertEqual(result[1].action_type, 'WriteTag')
        self.assertEqual(result[1].key, 'role-dhcp')
        self.assertEqual(result[1].value, 'true')
        self.assertEqual(result[2].action_type, 'WriteTag')
        self.assertEqual(result[2].key, 'high-availability')
        self.assertEqual(result[2].value, 'true')
        self.assertEqual(result[3].action_type, 'WriteTag')
        self.assertEqual(result[3].key, 'xha')
        self.assertEqual(result[3].value, 'true')

    def test_cluster_member_state_active_dhcp_failover(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_cluster_member_state_active_dhcp_failover.input', {}, {})
        self.assertEqual(4,len(result))
        self.assertEqual(result[0].action_type, 'WriteTag')
        self.assertEqual(result[0].key, 'role-dns')
        self.assertEqual(result[0].value, 'true')
        self.assertEqual(result[1].action_type, 'WriteTag')
        self.assertEqual(result[1].key, 'role-dhcp')
        self.assertEqual(result[1].value, 'true')
        self.assertEqual(result[2].action_type, 'WriteTag')
        self.assertEqual(result[2].key, 'high-availability')
        self.assertEqual(result[2].value, 'true')
        self.assertEqual(result[3].action_type, 'WriteTag')
        self.assertEqual(result[3].key, 'failover')
        self.assertEqual(result[3].value, 'true')

if __name__ == '__main__':
    unittest.main()