from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

states_ok = ['active','reloading', 'activating']
states_nok = ['inactive', 'maintenance','deactivating','failed','error']
load_ok = ['loaded']
load_nok = ['error', 'not-found', 'bad-setting', 'masked']
is_enabled_ok = ['enabled', 'enabled-runtime', 'linked', 'linked-runtime']
is_enabled_nok = ['masked', 'masked-runtime', 'static', 'indirect', 'disabled', 'bad']
is_enabled_not_defined = ['alias', 'generated', 'transient']

units = {
    'G': 1000000000,
    'M': 1000000,
    'K': 1000,
}

services = {
    'bam' : ['postgresServer', 'proteusServer', 'lcd', 'psmd', 'snmp', 'syslog-ng', 'ssh', 'ntp', 'cron', 'auditd','perfstats'],
    'bdds': ['lcd', 'psmd', 'snmp', 'syslog-ng', 'ssh', 'auditd', 'perfstats', 'cs'],
    'role-dhcp' : ['dhcpd', 'dhcpddns', 'dhcpmon2', 'dhcpsnmpd', 'fomon']
}

class BluecatServicesState(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            data = helper_methods.parse_data_as_list(raw_data, 'bluecat_services_state.textfsm')
            if data:
                services_monitor = services[device_tags.get('os.name')]
                if device_tags.get('role-dhcp') == "true":
                    services_monitor += services['role-dhcp']
                for service in data:
                    if service['service_name'] in services_monitor:
                        tags = {}
                        tags['im.identity-tags'] = 'name|type'
                        tags['name'] = service['service_name']

                        if service.get('service_description'):
                            tags['type'] = 'description'
                            self.write_complex_metric_string('systemctl-service-description', tags, service['service_description'], True, 'Critical Services - Details')

                        if service.get('unit_file_source'):
                            tags['type'] = 'source file'
                            self.write_complex_metric_string('systemctl-service-source-file', tags, service['unit_file_source'], True, 'Critical Services - Details')

                        if service.get('unit_memory_usage_size'):
                            tags['type'] = 'memory usage size'
                            self.write_double_metric('systemctl-service-memory-usage-size', tags, 'gauge', float(service['unit_memory_usage_size'])*(units[service['unit_memory_usage_units']]), True, 'Critical Services - Memory Usage', 'bytes')

                        if service.get('unit_activation_state'):
                            tags['type'] = 'state status'
                            self.write_double_metric('systemctl-service-state', tags, 'gauge', 1 if service['unit_activation_state'].lower() in states_ok else 0, True, 'Critical Services - Status', 'state', 'name|type')

                        if service.get('unit_activation_detail'):
                            tags['type'] = 'state status detail'
                            self.write_complex_metric_string('systemctl-service-state-detail', tags, service['unit_activation_detail'], True, 'Critical Services - Details')

                        if service.get('unit_memory_load'):
                            tags['type'] = 'load in memory'
                            self.write_double_metric('systemctl-service-memory-load', tags, 'gauge', 1 if service['unit_memory_load'].lower() in load_ok else 0, True, 'Critical Services - Status', 'state', 'name|type')

                        if service.get('unit_start_at_boot'):
                            tags['type'] = 'start at boot'
                            self.write_double_metric('systemctl-service-boot-start', tags, 'gauge', 1 if service['unit_start_at_boot'].lower() in is_enabled_ok else 0, True, 'Critical Services - Status', 'state', 'name|type')

                        if service.get('unit_activation_date'):
                            tags['type'] = 'state status since'
                            self.write_complex_metric_string('systemctl-service-status-since', tags, service['unit_activation_date'], True, 'Critical Services - Details')

            return self.output