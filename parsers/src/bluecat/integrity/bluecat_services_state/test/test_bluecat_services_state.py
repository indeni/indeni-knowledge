import os
import unittest
from bluecat.integrity.bluecat_services_state.bluecat_services_state import BluecatServicesState
from parser_service.public.action import *

class TestBluecatServicesState(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = BluecatServicesState()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))    
    
    def test_BAM_all_services(self):
        result = self.parser.parse_file(self.current_dir + '/BAM_all_services.input', {}, {'os.name': 'bam'})
        self.assertEqual(88,len(result))


    def test_BDDS_all_services(self):
        result = self.parser.parse_file(self.current_dir + '/BDDS_all_services.input', {}, {'os.name': 'bdds','role-dhcp' : "true"})
        self.assertEqual(69,len(result))


if __name__ == '__main__':
    unittest.main()