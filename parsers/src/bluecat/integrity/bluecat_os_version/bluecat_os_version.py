from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class BluecatOsVersion(BaseParser):
    """
    Collect BlueCat OS version via SNMP:
        version             1.3.6.1.4.1.13315.3.1.4.2.2.12.0 = STRING: "Proteus - 9.4.0-674.GA.bcn"
                            1.3.6.1.4.1.13315.3.1.4.2.2.12.0 = STRING: "Adonis - 9.4.0-674.GA.bcn"
    """
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        lines = raw_data.splitlines()
        version = ''
        for line in lines:
            if line.startswith('Version = '):
                version = line.split("= ")[1].split('-')[0]
                self.write_complex_metric_string('os-version',{},version, False)
        return self.output
