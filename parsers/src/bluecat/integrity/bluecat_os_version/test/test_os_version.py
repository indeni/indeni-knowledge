import os
import unittest

from bluecat.integrity.bluecat_os_version.bluecat_os_version import BluecatOsVersion
from parser_service.public.action import *

class TestBluecatOsVersion(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = BluecatOsVersion()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_BAM_VMware_9_4_0(self):
        result = self.parser.parse_file(self.current_dir + '/BAM_VMware_9_4_0.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].action_type, 'WriteComplexMetric')
        self.assertEqual(result[0].name, 'os-version')
        self.assertEqual(result[0].value, {'value': '9.4.0'})


    def test_BDDS_VMware_9_4_0(self):
        result = self.parser.parse_file(self.current_dir + '/BDDS_VMware_9_4_0.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].action_type, 'WriteComplexMetric')
        self.assertEqual(result[0].name, 'os-version')
        self.assertEqual(result[0].value, {'value': '9.4.0'})



if __name__ == '__main__':
    unittest.main()

    