from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class BluecatEdgeSPInterrogation(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            try:
                parsed_data = helper_methods.parse_data_as_json(raw_data)
                if parsed_data:
                    if parsed_data.get('services') is not None:
                        services = [x['serviceName'] for x in parsed_data['services']]
                        if 'DNS nameserver' or 'dns-resolver-service' in services:
                            self.write_tag('vendor', 'bluecat')
                            self.write_tag('product', 'edge')
                            self.write_tag('os.name', 'edge-sp')
                            self.write_tag('os.version', parsed_data.get('version'))
                            self.write_tag('model', parsed_data.get('platform'))
            except:
                pass

        return self.output