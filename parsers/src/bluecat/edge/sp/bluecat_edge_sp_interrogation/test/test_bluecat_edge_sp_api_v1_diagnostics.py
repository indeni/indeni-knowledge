import os
import unittest
from bluecat.edge.sp.bluecat_edge_sp_interrogation.bluecat_edge_sp_interrogation import BluecatEdgeSPInterrogation
from parser_service.public.action import *

class TestBluecatEdgeSPInterrogation(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = BluecatEdgeSPInterrogation()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))    
    
    def test_bluecat_edge_sp_api_v1_diagnostics_example1(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_edge_sp_api_v1_diagnostics_example1.json', {}, {})
        self.assertEqual(5,len(result))
        self.assertEqual(result[0].action_type, 'WriteTag')
        self.assertEqual(result[0].key, 'vendor')
        self.assertEqual(result[0].value, 'bluecat')
        self.assertEqual(result[1].action_type, 'WriteTag')
        self.assertEqual(result[1].key, 'product')
        self.assertEqual(result[1].value, 'edge')
        self.assertEqual(result[2].action_type, 'WriteTag')
        self.assertEqual(result[2].key, 'os.name')
        self.assertEqual(result[2].value, 'edge-sp')
        self.assertEqual(result[3].action_type, 'WriteTag')
        self.assertEqual(result[3].key, 'os.version')
        self.assertEqual(result[3].value, '4.5.0')
        self.assertEqual(result[4].action_type, 'WriteTag')
        self.assertEqual(result[4].key, 'model')
        self.assertEqual(result[4].value, 'vmware')     
           
    def test_bluecat_edge_sp_api_v1_diagnostics_example2(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_edge_sp_api_v1_diagnostics_example2.json', {}, {})
        self.assertEqual(5,len(result))
        self.assertEqual(result[0].action_type, 'WriteTag')
        self.assertEqual(result[0].key, 'vendor')
        self.assertEqual(result[0].value, 'bluecat')
        self.assertEqual(result[1].action_type, 'WriteTag')
        self.assertEqual(result[1].key, 'product')
        self.assertEqual(result[1].value, 'edge')
        self.assertEqual(result[2].action_type, 'WriteTag')
        self.assertEqual(result[2].key, 'os.name')
        self.assertEqual(result[2].value, 'edge-sp')
        self.assertEqual(result[3].action_type, 'WriteTag')
        self.assertEqual(result[3].key, 'os.version')
        self.assertEqual(result[3].value, '4.7.0')
        self.assertEqual(result[4].action_type, 'WriteTag')
        self.assertEqual(result[4].key, 'model')
        self.assertEqual(result[4].value, 'vmware')        
        
    def test_bluecat_edge_sp_api_v1_diagnostics_example3(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_edge_sp_api_v1_diagnostics_example3.json', {}, {})
        self.assertEqual(5,len(result))
        self.assertEqual(result[0].action_type, 'WriteTag')
        self.assertEqual(result[0].key, 'vendor')
        self.assertEqual(result[0].value, 'bluecat')
        self.assertEqual(result[1].action_type, 'WriteTag')
        self.assertEqual(result[1].key, 'product')
        self.assertEqual(result[1].value, 'edge')
        self.assertEqual(result[2].action_type, 'WriteTag')
        self.assertEqual(result[2].key, 'os.name')
        self.assertEqual(result[2].value, 'edge-sp')
        self.assertEqual(result[3].action_type, 'WriteTag')
        self.assertEqual(result[3].key, 'os.version')
        self.assertEqual(result[3].value, '4.7.0')
        self.assertEqual(result[4].action_type, 'WriteTag')
        self.assertEqual(result[4].key, 'model')
        self.assertEqual(result[4].value, 'vmware')        

if __name__ == '__main__':
    unittest.main()