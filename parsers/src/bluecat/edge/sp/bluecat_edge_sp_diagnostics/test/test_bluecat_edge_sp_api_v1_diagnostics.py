import os
import unittest
from bluecat.edge.sp.bluecat_edge_sp_diagnostics.bluecat_edge_sp_diagnostics import BluecatEdgeSPDiagnostics
from parser_service.public.action import *

class TestBluecatEdgeSPDiagnostics(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = BluecatEdgeSPDiagnostics()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))
        self.device_tags = {'ip-address': '10.244.29.101'}
    
    def test_bluecat_edge_sp_api_v1_diagnostics_forwarder_ex1(self):
        # Testing forwarder latencies
        result = self.parser.parse_file(self.current_dir + '/bluecat_edge_sp_api_v1_diagnostics_example1-a.json', {}, self.device_tags)
        self.assertEqual(27, len(result))
        self.assertEqual(result[13].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[13].name, 'namespace-forwarder-latency')
        self.assertEqual(result[13].tags['name'], 'Namespace: bcia-namespace Forwarder: 1.1.1.1:53')
        self.assertEqual(result[13].value, 0)
        self.assertEqual(result[14].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[14].name, 'namespace-forwarder-latency')
        self.assertEqual(result[14].tags['name'], 'Namespace: bcia-namespace Forwarder: 8.8.4.4:53')
        self.assertEqual(result[14].value, 60)
        self.assertEqual(result[15].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[15].name, 'namespace-forwarder-latency')
        self.assertEqual(result[15].tags['name'], 'Namespace: Default Forwarder: 8.8.8.8:53')
        self.assertEqual(result[15].value, 120)

    def test_bluecat_edge_sp_api_v1_diagnostics_forwarder_ex2(self):
        # Testing forwarder latencies with data in prev_state
        result = self.parser.parse_file(self.current_dir + '/bluecat_edge_sp_api_v1_diagnostics_example1-b.json', {}, self.device_tags)
        self.assertEqual(31, len(result))
        self.assertEqual(result[13].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[13].name, 'namespace-forwarder-latency')
        self.assertEqual(result[13].tags['name'], 'Namespace: bcia-namespace Forwarder: 1.1.1.1:53')
        self.assertEqual(result[13].value, 0)
        self.assertEqual(result[14].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[14].name, 'namespace-forwarder-latency')
        self.assertEqual(result[14].tags['name'], 'Namespace: bcia-namespace Forwarder: 8.8.4.4:53')
        self.assertEqual(result[14].value, 0)
        self.assertEqual(result[15].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[15].name, 'namespace-forwarder-latency')
        self.assertEqual(result[15].tags['name'], 'Namespace: Default Forwarder: 8.8.8.8:53')
        self.assertEqual(result[15].value, 120)
        self.assertEqual(result[16].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[16].name, 'namespace-forwarder-latency')
        self.assertEqual(result[16].tags['name'], 'Namespace: bcia-namespace_2 Forwarder: 1.1.1.1:53')
        self.assertEqual(result[16].value, 0)
        self.assertEqual(result[17].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[17].name, 'namespace-forwarder-latency')
        self.assertEqual(result[17].tags['name'], 'Namespace: bcia-namespace_2 Forwarder: 8.8.4.4:53')
        self.assertEqual(result[17].value, 60)
        self.test_bluecat_edge_sp_api_v1_diagnostics_forwarder_ex6()

    def test_bluecat_edge_sp_api_v1_diagnostics_forwarder_ex3(self):
        # Testing forwarder drop rates
        result = self.parser.parse_file(self.current_dir + '/bluecat_edge_sp_api_v1_diagnostics_example2.json', {}, self.device_tags)
        self.assertEqual(23, len(result))
        self.assertEqual(result[14].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[14].name, 'namespace-forwarder-drop-rate')
        self.assertEqual(result[14].tags['name'], 'Namespace: Default Forwarder: 8.8.8.8:53')
        self.assertEqual(result[14].value, 2)
        self.test_bluecat_edge_sp_api_v1_diagnostics_forwarder_ex6()

    def test_bluecat_edge_sp_api_v1_diagnostics_forwarder_ex4(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_edge_sp_api_v1_diagnostics_example3.json', {}, self.device_tags)
        self.assertEqual(25, len(result))
        self.test_bluecat_edge_sp_api_v1_diagnostics_forwarder_ex6()

    def test_bluecat_edge_sp_api_v1_diagnostics_forwarder_ex5(self):
        # Testing Forwarder Reachability
        result = self.parser.parse_file(self.current_dir + '/bluecat_edge_sp_api_v1_diagnostics_example4.json', {}, self.device_tags)
        self.assertEqual(28, len(result))
        self.assertEqual(result[17].action_type, 'WriteComplexMetric')
        self.assertEqual(result[17].name, 'namespace-forwarder-reachability')
        self.assertEqual(result[17].tags['name'], 'Namespace: Ari')
        self.assertEqual(result[17].value[0]['Forwarder'], '10.244.160.25:9953')
        self.test_bluecat_edge_sp_api_v1_diagnostics_forwarder_ex6()

    def test_bluecat_edge_sp_api_v1_diagnostics_forwarder_ex6(self):
        ip = self.device_tags['ip-address']
        parent_dir = os.path.dirname(self.current_dir)
        state_folder = parent_dir + '/prev_state/'
        for metric in ['_namespace-forwarder-drop-rate', '_namespace-forwarder-latency', '_namespace-forwarder-reachability']:
            if os.path.exists(state_folder + ip + metric):
                os.remove(state_folder + ip + metric)


if __name__ == '__main__':
    unittest.main()