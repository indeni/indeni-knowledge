from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser
from parser_service.public.state_parser import StateParser
import time
from calendar import timegm
import logging
import logging.config


class BluecatEdgeSPDiagnostics(BaseParser, StateParser):
    def __init__(self):
        super().__init__()
        global_config_path = '/etc/bcia-config.json'
        global_config = self.load_global_config(global_config_path)

        logging.config.fileConfig(global_config['parserLogConfig'])
        self.state_file_path = f"{global_config['indeniKnowledgeStableBasePath']}ind/parsers/src/bluecat/edge/sp/bluecat_edge_sp_diagnostics/prev_state/"
        self.cooldown_threshold = self.read_cooldown_threshold(global_config['indeniServiceConfigLocation'], global_config['indeniServiceOverwriteConfigLocation'])

        self.maintain_state_for_metrics = ['namespace-forwarder-reachability', 'namespace-forwarder-latency', 'namespace-forwarder-drop-rate']
        self.additional_params_for_metric = {
                                    'namespace-forwarder-reachability': {'is_live_config': False},
                                    'namespace-forwarder-latency': {'ds_type': 'gauge', 'is_live_config': True, 'display_name': 'DNS gateway service - Forwarders Latency (ms)',
                                                'display_type': 'number', 'identity_tags': 'name'},
                                    'namespace-forwarder-drop-rate': {'ds_type': 'gauge', 'is_live_config': True, 'display_name': 'DNS gateway service - Forwarders Drop Rate',
                                                'display_type': 'number', 'identity_tags': 'name'}
                                }

    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        logging.info('#######################################')
        logging.info('#### IP ADDRESS: ' + str(device_tags['ip-address']) + ' ####')
        logging.info('#######################################')


        if raw_data:
            parsed_data = helper_methods.parse_data_as_json(raw_data)
            if parsed_data:
                if services:=parsed_data.get('services'):
                    # DNS Resolver services status
                    for service in services:
                        try:
                            if service['diagnostics']['additionalDetails'].get('currentLocalTime'):
                                # LocalTime
                                utc_time = time.strptime(service['diagnostics']['additionalDetails']['currentLocalTime'], '%Y-%m-%d %H:%M:%S')
                                self.write_double_metric('current-datetime', {}, 'gauge', timegm(utc_time), True, 'Current Date/Time', 'date', '')
                            if service.get('serviceName') in ['dns-resolver-service']:
                                self.write_double_metric('dns-resolver-status',
                                                            {},
                                                            'gauge',
                                                            1 if service['status'] == 'HEALTHY' else 0,
                                                            True,
                                                            'DNS Resolver Service Status',
                                                            'state',
                                                            'name')
                            for service_l2 in service['diagnostics']['services']:
                                if service_l2.get('id') in ['sp-controller-service', 'parclo-logging', 'dns-gateway-service', 'status-service']:
                                    self.write_double_metric('dns-resolver-service-status',
                                                            {'name' : service_l2['id']},
                                                            'gauge',
                                                            1 if service_l2['status'] == 'GOOD' else 0,
                                                            True,
                                                            'Services Status',
                                                            'state',
                                                            'name')

                                match service_l2.get('id'):
                                    case 'sp-controller-service':
                                        for dns_resolve_response in service_l2['additionalDetails']['dns-resolving-responses'].keys():
                                            self.write_double_metric('dns-resolving-responses', {'name' : dns_resolve_response}, 'gauge',
                                                                    1 if service_l2['additionalDetails']['dns-resolving-responses'][dns_resolve_response] == 'NOERROR' else 0, True, 'Nameservers resolving',  'state', 'name')

                                        self.write_double_metric('customer-instance-connection',
                                                                {},
                                                                'gauge',
                                                                1 if [resource['status'] for resource in service_l2['resources'] if resource['id'] == 'customerInstanceConnection'][0]  == 'GOOD' else 0,
                                                                True,
                                                                'Connectivity to cloud management',
                                                                'state',
                                                                '')

                                    case 'parclo-logging':
                                        for resource in service_l2['resources']:
                                            match resource.get('id'):
                                                case 'unlogged-queries':
                                                    self.write_double_metric('incoming-dns-events-dropped-count',
                                                                            {},
                                                                            'gauge',
                                                                            [info_metric['value'] for info_metric in resource['info'] if info_metric['name'] == 'incoming-dns-events-dropped-count'][0],
                                                                            True,
                                                                            'Parclo Logging - Incoming DNS events dropped',
                                                                            'number',
                                                                            '')
                                                case 'kinesis-stream':
                                                    kinesis_put_success = [info_metric['value'] for info_metric in resource['info'] if info_metric['name'] == 'kinesis-put_success'][0]
                                                    kinesis_put_fail = [info_metric['value'] for info_metric in resource['info'] if info_metric['name'] == 'kinesis-put_fail'][0]
                                                    self.write_double_metric('kinesis-put-success',
                                                                            {},
                                                                            'gauge',
                                                                            kinesis_put_success,
                                                                            True,
                                                                            'Parclo Logging - Kinesis put success',
                                                                            'number',
                                                                            '')
                                                    self.write_double_metric('kinesis-fail-success',
                                                                            {},
                                                                            'gauge',
                                                                            kinesis_put_fail,
                                                                            True,
                                                                            'Parclo Logging - Kinesis put fail',
                                                                            'number',
                                                                            '')
                                                    self.write_double_metric('kinesis-success-ratio',
                                                                            {},
                                                                            'gauge',
                                                                            100 if (kinesis_put_success == 0 and kinesis_put_fail == 0) else (kinesis_put_success/(kinesis_put_success+kinesis_put_fail))*100,
                                                                            True,
                                                                            'Parclo Logging - Kinesis put (%)',
                                                                            'percentage',
                                                                            '')
                                                case 'logging-endpoint':
                                                    logging_endpoint_put_success_count = [info_metric['value'] for info_metric in resource['info'] if info_metric['name'] == 'logging-endpoint-put-success-count'][0]
                                                    logging_endpoint_put_failed_count = [info_metric['value'] for info_metric in resource['info'] if info_metric['name'] == 'logging-endpoint-put-failed-count'][0]
                                                    self.write_double_metric('logging-endpoint-put-success-count',
                                                                            {},
                                                                            'gauge',
                                                                            logging_endpoint_put_success_count,
                                                                            True,
                                                                            'Parclo Logging - Logging endpoint put success',
                                                                            'number',
                                                                            '')
                                                    self.write_double_metric('logging-endpoint-put-failed-count',
                                                                            {},
                                                                            'gauge',
                                                                            logging_endpoint_put_failed_count,
                                                                            True,
                                                                            'Parclo Logging - Logging endpoint put fail',
                                                                            'number',
                                                                            '')
                                                    self.write_double_metric('logging-endpoint-success-ratio',
                                                                            {},
                                                                            'gauge',
                                                                            100 if (logging_endpoint_put_success_count == 0 and logging_endpoint_put_failed_count ==0 )else (logging_endpoint_put_success_count/(logging_endpoint_put_success_count+logging_endpoint_put_failed_count))*100,
                                                                            True,
                                                                            'Parclo Logging - Logging endpoint put (%)',
                                                                            'percentage',
                                                                            '')

                                    case 'dns-gateway-service':
                                        # Last policy event, 2 options, most probably delta best option to implement the rule. Visualization maybe not
                                        self.write_double_metric('policy-event-last-modified-delta',
                                                                {},
                                                                'gauge',
                                                                service_l2['additionalDetails']['policyDiagnostics']['policyEventTimestamp']-(time.mktime(utc_time)*1000),
                                                                True,
                                                                'DNS gateway service - Last policy event delta',
                                                                'duration',
                                                                '')
                                        self.write_double_metric('policy-event-last-modified-date',
                                                                {},
                                                                'gauge',
                                                                timegm(time.gmtime(service_l2['additionalDetails']['policyDiagnostics']['policyEventTimestamp']/1000)),
                                                                True,
                                                                'DNS gateway service - Last policy event',
                                                                'date',
                                                                '')

                                        #Collecting namespaces+forwarders
                                        # Iterate through metrics that we want to maintain the state for
                                        for metric in self.maintain_state_for_metrics:
                                            logging.info('#######################################')
                                            logging.info('#### Metric: ' + str(metric) + ' ####')
                                            logging.info('#######################################')
                                            current_failures = dict() # Initialize a dictionary to track current failures and avoid duplicates
                                            current_failures_issues = dict()  # Initialize a dictionary to track issues for failures

                                            # Load previous failures for the current metric from the state file
                                            prev_failures = self.load_prev_failures(self.state_file_path, device_tags['ip-address'] + '_' + metric)

                                            for resource in service_l2['resources']:
                                                match resource.get('type'):
                                                    case 'forwarder':
                                                        namespace_id = resource['id'].split('namespace-')[1].split('-forwarder')[0]
                                                        namespace_name = [namespace['name'] for namespace in service_l2['additionalDetails']['settingsDiagnostics']['namespaceDetails'] if namespace['id'] == namespace_id][0]

                                                        if metric == 'namespace-forwarder-reachability':
                                                            tags = f"Namespace: {namespace_name}"
                                                            issues = self.__format_results(namespace_id, [], service_l2['resources'])
                                                            if tags not in current_failures and len(issues) > 0: # If we have issues for this namespace and it is not already in current failures
                                                                current_failures[tags] = 0 # Flag as a failure
                                                                current_failures_issues[tags] = issues # Store the issues

                                                        elif metric == 'namespace-forwarder-latency':
                                                            tags = f"Namespace: {namespace_name} Forwarder: {[info_metric['value'] for info_metric in resource['info'] if info_metric['name'] == 'address'][0]}"
                                                            issues = [info_metric['value'] for info_metric in resource['info'] if info_metric['name'] == 'latency'][0]
                                                            if tags not in current_failures: # If this tag hasn't been recorded as a failure
                                                                current_failures[tags] = 'issue' # Flag as a failure
                                                                current_failures_issues[tags] = issues # Store the issues

                                                        elif metric == 'namespace-forwarder-drop-rate':
                                                            tags = f"Namespace: {namespace_name} Forwarder: {[info_metric['value'] for info_metric in resource['info'] if info_metric['name'] == 'address'][0]}"
                                                            issues = [info_metric['value'] for info_metric in resource['info'] if info_metric['name'] == 'dropRate'][0]
                                                            if tags not in current_failures: # If this tag hasn't been recorded as a failure
                                                                current_failures[tags] = 'issue' # Flag as a failure
                                                                current_failures_issues[tags] = issues # Store the issues


                                            # logging.warning(f'****** Failures from CURRENT RUN with Issues for METRIC: {metric} ******\n\t' + str(current_failures_issues))

                                            if metric == 'namespace-forwarder-reachability':
                                                self.__write_complex_metric_object_array('namespace-forwarder-reachability', device_tags, prev_failures, current_failures, current_failures_issues)

                                            elif metric == 'namespace-forwarder-latency':
                                                self.__write_double_metric('namespace-forwarder-latency', device_tags, prev_failures, current_failures, current_failures_issues)

                                            elif metric == 'namespace-forwarder-drop-rate':
                                                self.__write_double_metric('namespace-forwarder-drop-rate', device_tags, prev_failures, current_failures, current_failures_issues)

                        except:
                            pass
                if parsed_data.get('networkConnectivity'):
                    self.write_complex_metric_object_array('fleet-mgmt-endpoint-error-message',
                                                           {},
                                                           {'error_message' : parsed_data['networkConnectivity']['fleetMgmtEndpointErrorMessage']} if parsed_data['networkConnectivity']['fleetMgmtEndpointErrorMessage'] != '' else {},
                                                           False)
                    self.write_double_metric('fleet-mgmt-endpoint-status',
                                                {},
                                                'gauge',
                                                1 if parsed_data['networkConnectivity']['fleetMgmtEndpointStatus']==200 else 0,
                                                True,
                                                'Network Connectivity - Fleet management endpoint status',
                                                'state',
                                                '')
                    self.write_double_metric('local-ip-status',
                                                {},
                                                'gauge',
                                                1 if parsed_data['networkConnectivity']['localIpStatus']=='OK' else 0,
                                                True,
                                                'Network Connectivity - Local IP status',
                                                'state',
                                                '')
                    self.write_double_metric('local-network-status',
                                                {},
                                                'gauge',
                                                1 if parsed_data['networkConnectivity']['localNetworkStatus']=='OK' else 0,
                                                True,
                                                'Network Connectivity - Local Network status',
                                                'state',
                                                '')
                if parsed_data.get('ntpStatus'):
                    self.write_double_metric('ntp-server-state',
                                                {},
                                                'gauge',
                                                1 if parsed_data['ntpStatus']['synchronized']  else 0,
                                                True,
                                                'NTP status',
                                                'state',
                                                '')
                if parsed_data.get('registrationStatus'):
                    self.write_double_metric('sp-registration-status',
                                                {},
                                                'gauge',
                                                1 if parsed_data['registrationStatus']['state'] == 'REGISTERED'  else 0,
                                                True,
                                                'SP registration status',
                                                'state',
                                                '')
                if parsed_data.get('connectionStatus'):
                    self.write_double_metric('sp-connection-status',
                                                {},
                                                'gauge',
                                                1 if parsed_data['connectionStatus'] == 'CONNECTED'  else 0,
                                                True,
                                                'SP connection status',
                                                'state',
                                                '')
        return self.output

    def __format_results(self, namespace_id, issues, l2_resources):
        for resource in l2_resources:
            match resource.get('type'):
                case 'forwarder':
                    if namespace_id in resource['id']:
                        fwd_address = ''
                        fwd_state = ''
                        for info_item in resource['info']:
                            if info_item['name'] == 'address':
                                fwd_address = info_item['value']
                            if info_item['name'] == 'state':
                                fwd_state = info_item['value']
                        if fwd_state == 'down':
                            issues.append({"Forwarder": f"{fwd_address}"})
        return issues

    def __write_complex_metric_object_array(self, metric, device_tags, prev_failures, current_failures, current_failures_issues):
        logging.info(f'****** Failures from CURRENT RUN with Issues for METRIC: {metric} ******\n\t' + str(current_failures_issues))

        unresolved_failures, trimmed_current_failures = self.resolve_prev_failures(
            metric, 'string', self.write_complex_metric_object_array,
            self.additional_params_for_metric[metric],
            self.cooldown_threshold, prev_failures, current_failures)

        # Write metrics for unresolved failures
        logging.info('****** Unresolved failures from Previous Run ******\n\t' + str(unresolved_failures))
        self.write_metric_from_failure_issues_dict(metric, 'string', self.write_complex_metric_object_array, self.additional_params_for_metric[metric], unresolved_failures, current_failures_issues)

        # write metrics for trimmed current failures
        logging.info('****** Trimmed failures from Current Run ******\n\t' + str(trimmed_current_failures))
        self.write_metric_from_failure_issues_dict(metric, 'string', self.write_complex_metric_object_array, self.additional_params_for_metric[metric], trimmed_current_failures, current_failures_issues)

        # Write the zone failures to the state file
        self.persist_failures_to_state_file(self.state_file_path, device_tags['ip-address']+'_'+metric, unresolved_failures | trimmed_current_failures)

    def __write_double_metric(self, metric, device_tags, prev_failures, current_failures, current_failures_issues):
        logging.info(f'****** Failures from CURRENT RUN with Issues for METRIC: {metric} ******\n\t' + str(current_failures_issues))

        unresolved_failures, trimmed_current_failures = self.resolve_prev_failures(
            metric, 'numeric', self.write_double_metric,
            self.additional_params_for_metric[metric],
            self.cooldown_threshold, prev_failures, current_failures)

        # Write metrics for unresolved failures
        logging.info('****** Unresolved failures from Previous Run ******\n\t' + str(unresolved_failures))
        self.write_metric_from_failure_issues_dict(metric, 'numeric', self.write_double_metric, self.additional_params_for_metric[metric], unresolved_failures, current_failures_issues)

        # write metrics for trimmed current failures
        logging.info('****** Trimmed failures from Current Run ******\n\t' + str(trimmed_current_failures))
        self.write_metric_from_failure_issues_dict(metric, 'numeric', self.write_double_metric, self.additional_params_for_metric[metric], trimmed_current_failures, current_failures_issues)

        # Write the zone failures to the state file
        self.persist_failures_to_state_file(self.state_file_path, device_tags['ip-address']+'_'+metric, unresolved_failures | trimmed_current_failures)