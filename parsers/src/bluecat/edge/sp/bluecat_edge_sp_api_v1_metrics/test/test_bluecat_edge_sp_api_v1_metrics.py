import os
import unittest
from bluecat.edge.sp.bluecat_edge_sp_api_v1_metrics.bluecat_edge_sp_api_v1_metrics import BluecatEdgeSpApiV1Metrics
from parser_service.public.action import *

class TestBluecatEdgeSpApiV1Metrics(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = BluecatEdgeSpApiV1Metrics()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))    
    
    def test_bluecat_edge_sp_api_v1_metrics(self):
        result = self.parser.parse_file(self.current_dir + '/bluecat_edge_sp_api_v1_metrics_example2.input', {}, {})
        self.assertEqual(23,len(result))
        #self.assertEqual(result[0].action_type, 'WriteTag')
        #self.assertEqual(result[0].key, 'role-dns')
        #self.assertEqual(result[0].value, 'true')
        #self.assertEqual(result[1].action_type, 'WriteTag')
        #self.assertEqual(result[1].key, 'role-dhcp')
        #self.assertEqual(result[1].value, 'false')

if __name__ == '__main__':
    unittest.main()