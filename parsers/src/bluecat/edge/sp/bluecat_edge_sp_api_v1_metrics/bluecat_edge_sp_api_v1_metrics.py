from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser
from datetime import datetime
class BluecatEdgeSpApiV1Metrics(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            parsed_data = helper_methods.parse_data_as_prometheus(raw_data)
            if parsed_data:
                
                # uptime-milliseconds
                system_uptime_metrics = [metric for metric in parsed_data if metric['name'] == 'system_uptime']
                self.write_double_metric('uptime-milliseconds', {}, 'gauge', int(system_uptime_metrics[0]['points'][0]['value'])*1000, True, 
                                         'System Uptime', 'duration', None)
                
                # disk-used-kbytes, disk-total-kbytes, disk-usage-percentage
                disk_used_metrics = [metric for metric in parsed_data if metric['name'] == 'disk_used']
                for point in disk_used_metrics[0]['points']:
                    tags = {
                        'file-system' : point['tags']['path']
                    }
                    self.write_double_metric('disk-used-kbytes', tags, 'gauge', int(point['value']) / 1024, True,
                                             'File Systems Used', 'kbytes', 'file-system')
                disk_total_metrics = [metric for metric in parsed_data if metric['name'] == 'disk_total']
                for point in disk_total_metrics[0]['points']:
                    tags = {
                        'file-system' : point['tags']['path']
                    }
                    self.write_double_metric('disk-total-kbytes', tags, 'gauge', int(point['value']) / 1024, True,
                                             'File Systems Total', 'kbytes', 'file-system')
                disk_used_percent_metrics = [metric for metric in parsed_data if metric['name'] == 'disk_used_percent']
                for point in disk_used_percent_metrics[0]['points']:
                    tags = {
                        'file-system' : point['tags']['path']
                    }
                    self.write_double_metric('disk-usage-percentage', tags, 'gauge', int(point['value']), True, 
                                             'File Systems Usage ', 'percentage', 'file-system')
                
                # cpu-usage
                disk_used_metrics = [metric for metric in parsed_data if metric['name'] == 'cpu_usage_idle']
                for point in disk_used_metrics[0]['points']:
                    if point['tags']['cpu'] == 'cpu-total':
                        tags = {
                            'cpu-is-avg' : 'true',
                            'cpu-id' : 'all-average',
                            'resource-metric' : 'true'
                            } 
                    self.write_double_metric('cpu-usage', tags, 'gauge', 100 - int(point['value']), True,
                                             'CPU', 'percentage', 'cpu-id')
                
                # memory-free-kbytes, memory-total-kbytes, memory-usage 
                # RAM
                tags = {'name' : 'RAM'} 
                ram_memory_free_bytes = [metric['points'][0]['value'] for metric in parsed_data if metric['name'] == 'mem_free'][0]
                ram_memory_total_bytes = [metric['points'][0]['value'] for metric in parsed_data if metric['name'] == 'mem_total'][0]
                ram_memory_used_percentage = [metric['points'][0]['value'] for metric in parsed_data if metric['name'] == 'mem_used_percent'][0]
                self.write_double_metric('memory-free-kbytes', tags , 'gauge', int(ram_memory_free_bytes) / 1024, True, 
                                         'Memory - Free', 'kilobytes', 'name')
                self.write_double_metric('memory-total-kbytes', tags, 'gauge', int(ram_memory_total_bytes) / 1024, True, 
                                         'Memory - Total', 'kilobytes', 'name')
                self.write_double_metric('memory-usage', tags, 'gauge', ram_memory_used_percentage, True, 
                                         'Memory Usage', 'percentage', 'name')
                # SWAP
                tags = {'name' : 'swap'} 
                swap_memory_free_bytes = [metric['points'][0]['value'] for metric in parsed_data if metric['name'] == 'mem_swap_free'][0]
                swap_memory_total_bytes = [metric['points'][0]['value'] for metric in parsed_data if metric['name'] == 'mem_swap_total'][0]
                self.write_double_metric('memory-free-kbytes', tags , 'gauge', int(swap_memory_free_bytes) / 1024, True, 
                                         'Memory - Free', 'kilobytes', 'name')
                self.write_double_metric('memory-total-kbytes', tags, 'gauge', int(swap_memory_total_bytes) / 1024, True, 
                                         'Memory - Total', 'kilobytes', 'name')
                tags['resource-metric'] = 'true'
                self.write_double_metric('memory-usage', tags, 'gauge', (1 - (swap_memory_free_bytes/swap_memory_total_bytes))*100, True, 
                                         'Memory Usage', 'percentage', 'name')
                
                # Load Average
                self.write_double_metric('load-average-one-minute-live-config', {} , 'gauge', 
                                         [metric['points'][0]['value'] for metric in parsed_data if metric['name'] == 'system_load1'][0], 
                                         True, 'Load Average (1 Minute)', 'number')
                self.write_double_metric('load-average-five-minute-live-config', {} , 'gauge', 
                                         [metric['points'][0]['value'] for metric in parsed_data if metric['name'] == 'system_load5'][0], 
                                         True, 'Load Average (5 Minute)', 'number')
                self.write_double_metric('load-average-fifteen-minute-live-config', {} , 'gauge', 
                                         [metric['points'][0]['value'] for metric in parsed_data if metric['name'] == 'system_load15'][0], 
                                         True, 'Load Average (15 Minute)', 'number')

        return self.output