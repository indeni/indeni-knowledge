from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class CasPortStatus(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            parsed_data = helper_methods.parse_data_as_json(raw_data)
            if parsed_data:
                tags = {}
                for network_interfaces in parsed_data['machine_status']['network_interfaces']:
                    if 'nic' in network_interfaces['name']:
                        tags['name'] = network_interfaces['name']
                        tags_mac_address = {'name': network_interfaces['name'] , 'im.identity-tags': 'name', 'im.dstype.displaytype': 'string'}
                        self.write_double_metric('network-interface-state', tags, 'gauge', 1 if network_interfaces['status'] == 'UP' else 0, True, 'Network Interfaces - state', 'state', 'name')
                        self.write_double_metric('network-interface-rx-packets', tags, 'counter', int(network_interfaces['rx_packets']), True, 'Network Interfaces - RX Packets', 'pkts', 'name')
                        self.write_double_metric('network-interface-tx-packets', tags, 'counter', int(network_interfaces['tx_packets']), True, 'Network Interfaces - TX Packets', 'pkts', 'name')
                        for network_interfaces_index2 in parsed_data['machine_configuration']['network_interfaces']:
                            if network_interfaces['name'] == network_interfaces_index2['name']:
                                self.write_double_metric('network-interface-mtu', tags, 'gauge', int(network_interfaces_index2['mtu']), True, 'Network Interfaces - MTU', 'number', 'name')
                                self.write_complex_metric_string('network-interface-mac', tags_mac_address, network_interfaces_index2['mac_address'], True, 'Network Interfaces - MAC Address')
        return self.output