import os
import unittest
from symantec.cas.cas_port_status.cas_port_status import CasPortStatus
from parser_service.public.action import *

class TestCasPortStatus(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = CasPortStatus()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_cas_port_status(self):
        result = self.parser.parse_file(self.current_dir + '/cas_port_status.input', {}, {})
        self.assertEqual(10, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'network-interface-state')
        self.assertEqual(result[0].tags['name'], 'nic0_0')
        self.assertEqual(result[0].value, 1)
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].name,'network-interface-rx-packets')
        self.assertEqual(result[1].tags['name'], 'nic0_0')
        self.assertEqual(result[1].value, 74916016)
        self.assertEqual(result[2].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[2].name,'network-interface-tx-packets')
        self.assertEqual(result[2].tags['name'], 'nic0_0')
        self.assertEqual(result[2].value, 67504860)
        self.assertEqual(result[3].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[3].name,'network-interface-mtu')
        self.assertEqual(result[3].tags['name'], 'nic0_0')
        self.assertEqual(result[3].value, 1500)
        self.assertEqual(result[4].action_type, 'WriteComplexMetric')
        self.assertEqual(result[4].name,'network-interface-mac')
        self.assertEqual(result[4].tags['name'], 'nic0_0')
        self.assertEqual(result[4].value['value'], '00:50:56:AC:08:1A')
        self.assertEqual(result[5].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[5].name, 'network-interface-state')
        self.assertEqual(result[5].tags['name'], 'nic1_0')
        self.assertEqual(result[5].value, 1)
        self.assertEqual(result[6].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[6].name,'network-interface-rx-packets')
        self.assertEqual(result[6].tags['name'], 'nic1_0')
        self.assertEqual(result[6].value, 515738)
        self.assertEqual(result[7].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[7].name,'network-interface-tx-packets')
        self.assertEqual(result[7].tags['name'], 'nic1_0')
        self.assertEqual(result[7].value, 0)
        self.assertEqual(result[8].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[8].name,'network-interface-mtu')
        self.assertEqual(result[8].tags['name'], 'nic1_0')
        self.assertEqual(result[8].value, 1500)
        self.assertEqual(result[9].action_type, 'WriteComplexMetric')
        self.assertEqual(result[9].name,'network-interface-mac')
        self.assertEqual(result[9].tags['name'], 'nic1_0')
        self.assertEqual(result[9].value['value'], '00:50:56:AC:BB:0B')
                
if __name__ == '__main__':
    unittest.main()