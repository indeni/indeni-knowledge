from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class CasHardwareStateFailure(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            parsed_data = helper_methods.parse_data_as_list(raw_data, 'cas_health_monitoring_current.textfsm')
            if parsed_data:
                tags = {}
                tags['name'] = parsed_data[0]['name']
                self.write_double_metric('cpu-usage', tags, 'gauge', float(parsed_data[0]['percentage']), True, "CPU Usage", "percentage", "name")
                for data in parsed_data:
                    if data['memory'] != "":
                        memory_data = data['memory'].split("/")
                        mem_used_k = int(memory_data[0])*1000
                        mem_total_k = int(memory_data[1])*1000
                        tags["name"] = "Free Memory"
                        self.write_double_metric('memory-free-kbytes', tags, 'gauge', mem_total_k - mem_used_k, True, "Memory Usage", "kilobytes", "name")
                        tags["name"] = "Total Memory"
                        self.write_double_metric('memory-total-kbytes', tags, 'gauge', mem_total_k, True, "Memory Usage", "kilobytes", "name")
                        tags["name"] = "Used Memory"
                        self.write_double_metric('memory-used-kbytes', tags, 'gauge', mem_used_k, True, "Memory Usage", "kilobytes", "name")
                        tags["name"] = "Percent Memory Usage"
                        self.write_double_metric('memory-usage', tags, 'gauge', int(data['percentage']), True, "Memory Usage", "percentage", "name")
                    tags['name'] = data['name']
                    self.write_double_metric('hardware-element-status', tags, 'gauge', 1 if data['status'] == 'OK' else 0, False)

            raid_data = helper_methods.parse_data_as_list(raw_data, 'cas_show_raid_array.textfsm')
            if raid_data:
                tags = {}
                tags['name'] = raid_data[0]['name']
                disk_used = float(raid_data[0]['disk_used'])*1000*1000
                disk_total = float(raid_data[0]['disk_total'])*1000*1000
                self.write_complex_metric_string('raid-level', tags, raid_data[0]['raid_level'], True, "Raid Level")
                self.write_double_metric('disk-used-kbytes', tags, 'gauge', disk_used, True, "Disk Usage", "kilobytes", "name")
                self.write_double_metric('disk-total-kbytes', tags, 'gauge', disk_total, True, "Disk Total", "kilobytes", "name")
                self.write_double_metric('hardware-element-status', tags, 'gauge', 1 if raid_data[0]['raid_status'] in ('active','clean') else 0, False)
                self.write_double_metric('disk-usage-percentage', tags, 'gauge', int(raid_data[0]['raid_percentage']), True, "RAID progress", "percentage", "name")
            
            slot_data = helper_methods.parse_data_as_list(raw_data, 'cas_show_raid_member.textfsm')
            if slot_data:
                tags = {}
                for parsed_slot in slot_data:
                    tags['name'] = parsed_slot['slot']
                    self.write_double_metric('hardware-element-status', tags, 'gauge', 1 if parsed_slot['slot_status'] == 'active sync' else 0, False)
        return self.output