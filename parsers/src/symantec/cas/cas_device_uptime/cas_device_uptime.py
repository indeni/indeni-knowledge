from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class CasDeviceUptime(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            parsed_data = helper_methods.parse_data_as_list(raw_data, 'cas_device_uptime.textfsm')
            if parsed_data:
                self.write_double_metric('uptime-milliseconds', {}, 'gauge', int(parsed_data[0]['uptime'])*10, True, 'Device uptime', 'duration')
        return self.output