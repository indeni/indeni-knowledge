import os
import unittest
from symantec.cas.cas_device_uptime.cas_device_uptime import CasDeviceUptime
from parser_service.public.action import *

class TestCasDeviceUptime(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = CasDeviceUptime()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_cas_device_uptime(self):
        result = self.parser.parse_file(self.current_dir + '/cas_device_uptime.input', {}, {})
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'uptime-milliseconds')
        self.assertEqual(result[0].tags['live-config'], 'true')
        self.assertEqual(result[0].tags['display-name'], 'Device uptime')
        self.assertEqual(result[0].tags['im.dstype.displayType'], 'duration')
        self.assertEqual(result[0].value, 34841690)

if __name__ == '__main__':
    unittest.main()