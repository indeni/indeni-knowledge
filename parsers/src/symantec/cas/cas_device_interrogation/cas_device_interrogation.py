from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class CasDeviceInterrogation(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            parsed_data = helper_methods.parse_data_as_list(raw_data, 'cas_device_interrogation.textfsm')
            if parsed_data:
                self.write_tag("model", parsed_data[0]['model'])
                self.write_tag("serial", parsed_data[0]['serial'])
        return self.output