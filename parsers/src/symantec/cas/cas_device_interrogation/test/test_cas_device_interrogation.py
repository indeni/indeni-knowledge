import os
import unittest
from symantec.cas.cas_device_interrogation.cas_device_interrogation import CasDeviceInterrogation
from parser_service.public.action import *

class TestCasDeviceInterrogation(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = CasDeviceInterrogation()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_cas_device_interrogation(self):
        result = self.parser.parse_file(self.current_dir + '/cas_device_interrogation.input', {}, {})
        self.assertEqual(2, len(result))
        self.assertEqual(result[0].action_type, 'WriteTag')
        self.assertEqual(result[0].key, 'model')
        self.assertEqual(result[0].value, 'CAS-V100')
        self.assertEqual(result[1].action_type, 'WriteTag')
        self.assertEqual(result[1].key, 'serial')
        self.assertEqual(result[1].value, '2587491')

if __name__ == '__main__':
    unittest.main()