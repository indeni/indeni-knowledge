BEGIN {
    is_update_available = 1.0
}

#No updates are available
/^No updates are available/ {
    is_update_available = 0.0
}

END {
    writeDoubleMetric("cas-update-available", null, "gauge", is_update_available, "false")  # Converted to new syntax by change_ind_scripts.py script
}