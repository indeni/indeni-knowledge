import os
import unittest
from symantec.cas.cas_port_utilization.cas_port_utilization import CasPortUtilization
from parser_service.public.action import *

class TestCasPortUtilization(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = CasPortUtilization()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_cas_port_utilization(self):
        result = self.parser.parse_file(self.current_dir + '/cas_port_utilization.input', {}, {})
        self.assertEqual(2, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'network-interface-percentage')
        self.assertEqual(result[0].tags['name'], 'interface 1')
        self.assertEqual(result[0].value, 0.0)
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].name, 'network-interface-percentage')
        self.assertEqual(result[1].tags['name'], 'interface 2')
        self.assertEqual(result[1].value, 16.0)
        
if __name__ == '__main__':
    unittest.main()