from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

class CasPortUtilization(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            lines = raw_data.splitlines()
            tags = {}
            for line in lines:
                interface_percentage = float(line.split("= ")[1])
                if line.startswith('1.3.6.1.4.1.3417.2.4.1.1.1.4.3'):  # Interface 1 utilization percentage
                    tags['name'] = 'interface 1'
                elif line.startswith('1.3.6.1.4.1.3417.2.4.1.1.1.4.4'):  # Interface 2 utilization percentage
                    tags['name'] = 'interface 2'
                self.write_double_metric('network-interface-percentage', tags, 'gauge', interface_percentage, False)
        return self.output