#CAS release 2.3.1.2 (217803)
#Notes: The command "show version" is vague and used in other interrogation. As such, this interrogation script must grep for unique values of the output. 
#Suggestion is to use "CAS release" as the grepped value
/CAS release/{
    writeTag("vendor", "symantec")
    writeTag("os.name", "cas")
    writeTag("os.version", $3)
}