import os
import unittest
from symantec.cas.cas_icap_queue_count.cas_icap_queue_count import CasIcapQueueCount
from parser_service.public.action import *

class TestCasIcapQueueCount(unittest.TestCase):

    def setUp(self):
        # Arrange
        self.parser = CasIcapQueueCount()
        self.current_dir = os.path.dirname(os.path.realpath(__file__))

    def test_cas_icap_queue_count(self):
        result = self.parser.parse_file(self.current_dir + '/cas_icap_queue_count.input', {}, {'model': 'CAS-V100'})
        self.assertEqual(2, len(result))
        self.assertEqual(result[0].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[0].name, 'cas-icap-queue-count')
        self.assertEqual(result[0].value, 0)
        self.assertEqual(result[1].action_type, 'WriteDoubleMetric')
        self.assertEqual(result[1].name, 'cas-icap-queue-limit')
        self.assertEqual(result[1].value, 100)

if __name__ == '__main__':
    unittest.main()