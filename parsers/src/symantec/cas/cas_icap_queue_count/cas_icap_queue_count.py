from parser_service.public import helper_methods
from parser_service.public.base_parser import BaseParser

model = {'CAS-S200-A1' : 100,
            'CAS-S400-A1' : 100,
            'CAS-S400-A2' : 200,
            'CAS-S400-A3' : 300,
            'CAS-S400-A4' : 400,
            'CAS-S500-A1' : 500,
            'CAS-V100' : 100,
            'CAS-VA-C4' : 100,
            'CAS-VA-C8' : 200,
            'CAS-VA-C16' : 300,
            'CAS-VA-C32' : 400,
            'CAS-VA-C64' : 500
            }

class CasIcapQueueCount(BaseParser):
    def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
        if raw_data:
            parsed_data = helper_methods.parse_data_as_list(raw_data, 'cas_icap_queue_count.textfsm')
            if parsed_data:
                cas_model = device_tags.get('model')
                cas_limit = model.get(cas_model)
                if cas_limit is not None:
                    self.write_double_metric('cas-icap-queue-count', {}, 'gauge', int(parsed_data[0]['icap_queue_count']), False)
                    self.write_double_metric('cas-icap-queue-limit', {}, 'gauge', cas_limit, False)
        return self.output