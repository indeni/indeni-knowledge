/^Expiration Date/ {
    expiration_date = ""
    if ($NF ~ "-") {
	     split($NF, date_array, "-")
	     expiration_date = date(date_array[1], date_array[2], date_array[3])
    }
    next
}

#Component Name      : Content Analysis System 2
/^Component Name/ {
    if (expiration_date != "") {
       split($0, name_array, ": ")
       tags["name"] = name_array[2]
       writeDoubleMetric("license-expiration", tags, "gauge", expiration_date, "false")  # Converted to new syntax by change_ind_scripts.py script
    }
    next
}