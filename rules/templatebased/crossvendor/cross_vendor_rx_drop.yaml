rule_type: template-based
template_name: NearingCapacityWithItems
rule_name: cross_vendor_rx_drop
rule_categories:
  - HealthChecks
rule_friendly_name: "RX packets dropped"
rule_description: "BCIA tracks the number of dropped received packets (RX) and alerts if the ratio is too high."
usage_metric_name: network-interface-rx-dropped
limit_metric_name: network-interface-rx-packets
meta_condition: "Tag(vendor) != \"checkpoint\" and Tag(vendor) != \"rhel\""
applicable_metric_tag: name
threshold: 0.5
minimum_value_to_alert: 1000.0  # We don't want to alert if the number of packets is really low
alert_description: |
  Some network RX ports drop rate is higher than 0.5% from total received packets, per second.
  Below information is in the format of (packets dropped / packets received), per second.
alert_item_description_format: "(%.0f / %.0f)"
base_remediation_text: "Packet drops usually occur when the rate of packets received is higher than device ability to handle."
alert_items_header: "Affected Ports"
vendor_to_remediation_text:
  VENDOR_JUNIPER: |
    1. Run the “show interface extensive” command to review the interface statistics.
    2. Check for packet drops and input/output traffic rate.
    3. Run the “show class-of-service interface x/x/x detail"  to determine any QoS policy applied to interface which may cause packet drops.
    4. If the interface is saturated, the number of packets dropped by the is indicated by the input queue of the I/O Manager ASIC. This number increments once for every packet that is dropped by the ASIC's RED mechanism.
    5. Review the following article on Juniper tech support site: https://www.juniper.net/documentation/en_US/junos/topics/reference/command-summary/show-interfaces-security.html#jd0e1772<a linkText>Ethernet Switching and Layer 2 Transparent Mode Feature Guide for Security Devices</a>
    6. If the problem persists, contact the Juniper Networks Technical Assistance Center (JTAC)
  VENDOR_FORTINET: |
    1. Run "diag hardware deviceinfo nic <interface>" command to display a list of hardware related error names and values. Review  the next link for more details: http://help.fortinet.com/fos50hlp/54/Content/FortiOS/fortigate-toubleshooting-54/troubleshooting_tools.htm
    2. Run command "fnsysctl cat /proc/net/dev" to get a summary of the interface statistics.
    3. Check for speed and duplex mismatch in the interface settings on both sides of a cable, and check for a damaged cable. Review the next link for more info: http://kb.fortinet.com/kb/documentLink.do?externalID=10653
not_supported:
  checkpoint:
    - "*"
  rhel:
    - "*"
  bluecat:
    - "edge-sp"