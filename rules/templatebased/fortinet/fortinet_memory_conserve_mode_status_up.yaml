rule_type: template-based
template_name: StateDown
rule_name: fortinet_memory_conserve_mode_status_up
rule_categories:
  - HealthChecks
rule_friendly_name: "FortiGate is in Conserve mode"
rule_description: "BCIA will alert if Memory conserve mode is enabled."
metric_name: memory-conserve-status
alert_if_down: false
alert_description: "Memory conserve mode activates protection measures in order to recover memory space"
base_remediation_text: |
  1. Login via ssh to the Fortinet firewall and run the FortiOS command “diagnose hardware sysinfo conserve”. The command output states the conserve mode status and lists the configured conserve mode thresholds. This command should be executed on the global vdom if VDOM is activated.
  2. Run the FortiOS command “get system performance status” to get CPU and memory states, average network usage, average sessions, and session setup rate, the virus caught, IPS attacks blocked, and uptime.
  3. Run the FortiOS command “diagnose sys print-conserve-info” to print all conserve related info.
  4.1 You can use the following commands to increase the three conserve mode memory thresholds:
    config system global
    set memory-use-threshold-extreme <memory-use>
    set memory-use-threshold-red <memory-use>
    set memory-use-threshold-green <memory-use>
    end
  4.2 You can use the following command to configure how antivirus processing acts when conserve mode is reached:
  config system global
  set av-failopen {pass | off | one-shot}

  Finally, the following actions will help to save memory resource:
  - Reduce the number of firewall sessions as described in the related Knowledge Base article "Technical Note : FortiGate CPU resource optimization configuration steps".
  - Reduce the maximum file size for antivirus scanning.
  - Turn off all non mandatory features such as Logging, archiving, data leak prevention, IPS.
  - Remove 'content summary' (especially if no FortiAnalyzers are configured).
  - Reduce memory caching in some features (Explicit proxy, FortiGuard Antispam/Webfiltering ...)
  - Enable automation stitching as described here: https://BCIA.com/blog/fortinet-fortigate-automation/

  5. Contact Fortinet Technical support at https://support.fortinet.com/ for further assistance.
