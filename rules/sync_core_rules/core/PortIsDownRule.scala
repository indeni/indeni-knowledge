package com.indeni.server.rules.library.core
import com.indeni.ruleengine.expressions.conditions.{And, Equals}
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.server.common.data.conditions.True
import com.indeni.server.rules._
import com.indeni.server.rules.library.{ConditionalRemediationSteps, PerDeviceRule, RuleHelper}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity


case class PortIsDownRule() extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata.builder("cross_vendor_network_port_down", "Network port(s) down",
    "Indeni will trigger an issue if one or more network ports is down.", AlertSeverity.CRITICAL, categories = Set(RuleCategory.HealthChecks), deviceCategory = DeviceCategory.AllDevices).build()

  override def expressionTree(context: RuleContext): StatusTreeExpression = {

    val actualValue = TimeSeriesExpression[Double]("network-interface-state").last
    val adminValue = TimeSeriesExpression[Double]("network-interface-admin-state").last

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
      And(StatusTreeExpression(
        // The additional tags we care about (we'll be including this in alert data)
        SelectTagsExpression(context.tsDao, Set("name"), withTagsCondition("network-interface-state")),
        And(
          StatusTreeExpression(
            SelectTimeSeriesExpression[Double](context.tsDao, Set("network-interface-state"), denseOnly = false),
            Equals(ConstantExpression[Option[Double]](Some(0)), actualValue)
          ).withoutInfo().asCondition(),
          StatusTreeExpression(
            SelectTimeSeriesExpression[Double](context.tsDao, Set("network-interface-admin-state"), denseOnly = false),
            adminValue.isNot(0.0)
          ).withoutInfo().asCondition().orElse(Some(true))
        )
      ).withSecondaryInfo(
        scopableStringFormatExpression("${scope(\"name\")}"),
        EMPTY_STRING,
        title = "Ports Affected"
      ).asCondition(), generateDevicePassiveAndPassiveLinkStateCondition(context.tsDao))
    ).withRootInfo(
      getHeadline(),
      ConstantExpression("One or more ports are down."),
      ConditionalRemediationSteps("Review the cause for the ports being down.",
        RemediationStepCondition.VENDOR_JUNIPER ->
          """|
            |1. On the device command line interface run "show interfaces extensive" command to check the status of the interface.
            |2. Execute "show configuration interface" command to check interface configuration.
            |3. Check the encapsulation type and physical media on the port.
            |4. Check the port specification and the fiber cable.
            |5. Review the following article on Juniper TechLibrary for more information, Operational Commands: show interfaces (SRX Series): https://www.juniper.net/documentation/en_US/junos/topics/reference/command-summary/show-interfaces-security.html#jd0e1772""".stripMargin,
        RemediationStepCondition.VENDOR_FORTINET ->
          """|
            |1. Monitor hardware network operations (e.g. speed, duplex settings) by using the "diag hardware deviceinfo nic <interface>" FortiOS command.
            |2. Run the command "diag hardware deviceinfo nic <interface>" command to display a list of hardware related names and values. Review the next link for more details: http://help.fortinet.com/fos50hlp/54/Content/FortiOS/fortigate-toubleshooting-54/troubleshooting_tools.htm
            |3. Run the hidden FortiOS command "fnsysctl cat /proc/net/dev" to get a summary of  the interface statistics.
            |4. Check for a mismatch in the speed and duplex interface settings on two sides of a cable, or for a damaged cable / SFP. Try to manually configure both sides to the same speed/duplex mode when you can. For more information, review "Symptoms of Ethernet speed/duplex mismatches" at http://kb.fortinet.com/kb/documentLink.do?externalID=10653
            |5. Review the log history for interfaces status changes.
            |6. Review the interface configuration. For more information, use the following interface configuration guide: http://help.fortinet.com/fos50hlp/52data/Content/FortiOS/fortigate-system-administration-52/Interfaces/interfaces.htm""".stripMargin,
        RemediationStepCondition.VENDOR_PANOS ->
          """|
            |Troubleshoot link down issue: https://knowledgebase.paloaltonetworks.com/KCSArticleDetail?id=kA10g000000PNcBCAW""".stripMargin
      )
    )
  }
}
