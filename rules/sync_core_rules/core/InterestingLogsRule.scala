package com.indeni.server.rules.library.core
import com.indeni.ruleengine._
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data._
import com.indeni.server.common.data.conditions.True
import com.indeni.server.rules._
import com.indeni.server.rules.library.{ConditionalRemediationSteps, PerDeviceRule, RuleHelper}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

/**
  * This rule allows ind scripts to pull in their own interesting logs, separate from Dendron.
  * This is usually done for log infromation that cannot be sent via syslog, or is best analyzed
  * over SSH by using grep/awk or something similar.
  */
case class InterestingLogsRule() extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata.builder("cross_vendor_interesting_logs", "Interesting logs found",
    "For each supported device, indeni will look for logs that are deemed \"interesting\" and trigger an issue when these are found.", AlertSeverity.ERROR,
    categories= Set(RuleCategory.HealthChecks), deviceCategory = DeviceCategory.AllDevices).build()

  override def expressionTree(context: RuleContext): StatusTreeExpression = {
    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
      StatusTreeExpression(
        SelectSnapshotsExpression(context.snapshotsDao, Set("interesting-logs")).multi(),
        StatusTreeExpression(
          IterateSnapshotDimensionExpression("interesting-logs"),
          com.indeni.ruleengine.expressions.conditions.True
        ).withSecondaryInfo(
            scopableStringFormatExpression("${scope(\"interesting-logs:line\")}"),
            scopableStringFormatExpression("${scope(\"interesting-logs:info\")}"),
            "Interesting Logs",
          Set[InvisibleScopeKey](InvisibleScopeKey("line",Some("interesting-logs")), InvisibleScopeKey("info",Some("interesting-logs")))
        ).asCondition()
      ).withoutInfo().asCondition()
      // Details of the alert itself
    ).withRootInfo(
        getHeadline(),
        ConstantExpression("Interesting logs have been found on this device. Review the list below for which logs were found and what action should be taken."),
        ConditionalRemediationSteps("For each line, extended information is included with specific remediation steps.")
    )

  }
}
