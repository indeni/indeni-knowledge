package com.indeni.server.rules.library.core
import com.indeni.ruleengine.expressions.conditions.{And, Equals, GreaterThanOrEqual}
import com.indeni.ruleengine.expressions.core.{ConstantExpression, StatusTreeExpression}
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.ruleengine.expressions.scope.ScopeValueExpression
import com.indeni.server.common.data.conditions.True
import com.indeni.server.rules.library.{ConditionalRemediationSteps, PerDeviceRule, RuleHelper}
import com.indeni.server.rules.{DeviceCategory, DeviceKey, RuleCategory, RuleContext, RuleMetadata}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class SmartEventLogHandlingRule() extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata.builder("check_point_smartevent_log_handling_issue", "SmartEvent log handling too slow",
    "If SmartEvent can't handle logs fast enough a backlog may occur, or the storage fills up. indeni will track the log handling by SmartEvent and alert if it's too slow.", AlertSeverity.ERROR, categories= Set(RuleCategory.VendorBestPractices), deviceCategory = DeviceCategory.CheckPointDevices).build()

  override def expressionTree(context: RuleContext): StatusTreeExpression = {
    val actualValue = TimeSeriesExpression[Double]("folder-file-count").last
    val folderPath = ScopeValueExpression("path").visible().asString().noneable

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      StatusTreeExpression(
            // The time-series we check the test condition against:
            SelectTagsExpression(context.tsDao, Set("path"), withTagsCondition("folder-file-count")),

            // The condition which, if true, we have an issue. Checked against the time-series we've collected
        StatusTreeExpression(
                // The time-series we check the test condition against:
                SelectTimeSeriesExpression[Double](context.tsDao, Set("folder-file-count"), denseOnly = false),

                // The condition which, if true, we have an issue. Checked against the time-series we've collected
                And(
                  GreaterThanOrEqual(
                    actualValue,
                    ConstantExpression(Some(100.0))),
                  Equals(folderPath, ConstantExpression(Some("$RTDIR/distrib")))
                )

          ).withRootInfo(
                getHeadline(),
                scopableStringFormatExpression("A look at $RTDIR/distrib shows there are currently %.0f logs being handled and it appears to remain at this level constantly. This may indicate a performance issue.", actualValue),
                ConditionalRemediationSteps("Contact your technical support provider, mention SK92766.")
        ).asCondition()
      ).withoutInfo().asCondition()
    ).withoutInfo()
  }
}
