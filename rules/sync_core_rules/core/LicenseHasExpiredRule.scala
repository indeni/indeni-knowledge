package com.indeni.server.rules.library.core
import com.indeni.apidata.time.TimeSpan
import com.indeni.apidata.time.TimeSpan.TimePeriod
import com.indeni.ruleengine.expressions.conditions.{And, GreaterThan, LesserThan}
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data._
import com.indeni.ruleengine.expressions.math.PlusExpression
import com.indeni.ruleengine.expressions.utility.NowExpression
import com.indeni.server.common.data.conditions.True
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules._
import com.indeni.server.rules.library.{ConditionalRemediationSteps, PerDeviceRule, RuleHelper}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity


case class LicenseHasExpiredRule() extends PerDeviceRule with RuleHelper {

  private val highThresholdParameterName = "Effective_Duration_Threshold"
  private val highThresholdParameter = new ParameterDefinition(
    highThresholdParameterName,
    "",
    "Effective Duration Threshold",
    "How many days the license expiration issue should be effective",
    UIType.TIMESPAN,
    TimeSpan.fromDays(30)
  )

  override val metadata: RuleMetadata = RuleMetadata
    .builder(
      "cross_vendor_license_has_expired",
      "License expired",
      "indeni will trigger an issue when a license has expired. Licenses that have expired more that a set number of days will be ignored.  The threshold for the number of days after licence expiration can be adjusted by the user.",
      AlertSeverity.ERROR,
      categories = Set(RuleCategory.OngoingMaintenance),
      deviceCategory = DeviceCategory.AllDevices
    )
    .configParameter(highThresholdParameter)
    .build()

  override def expressionTree(context: RuleContext): StatusTreeExpression = {
    val actualValue = TimeSeriesExpression[Double]("license-expiration").last.toTimeSpan(TimePeriod.SECOND)

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),
      // What constitutes an issue
      StatusTreeExpression(
        // The additional tags we care about (we'll be including this in alert data)
        SelectTagsExpression(context.tsDao, Set("name"), withTagsCondition("license-expiration")),
        StatusTreeExpression(
          // The time-series we check the test condition against:
          SelectTimeSeriesExpression[Double](context.tsDao, Set("license-expiration"), denseOnly = false),
          // The condition which, if true, we have an issue. Checked against the time-series we've collected
          And(
            LesserThan(
              actualValue,
              NowExpression()),
            GreaterThan(
              PlusExpression[TimeSpan](actualValue, getParameterTimeSpanForTimeSeries(highThresholdParameter)),
              NowExpression())
          )

          // The Alert Item to add for this specific item
        ).withSecondaryInfo(
            scopableStringFormatExpression("${scope(\"name\")}"),
            scopableStringFormatExpression("Expired on %s", timeSpanToDateExpression(actualValue)),
            title = "Affected Licenses"
          )
          .asCondition()
      ).withoutInfo().asCondition()
    ).withRootInfo(
      getHeadline(),
      ConstantExpression("One or more licenses have expired. See the list below."),
      ConditionalRemediationSteps(
        "Renew any licenses that need to be renewed.",
        RemediationStepCondition.VENDOR_CP -> "Make sure you have purchased the required licenses and have updated them in your management server: https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk33089",
        RemediationStepCondition.VENDOR_PANOS -> "Review this page on licensing:  https://docs.paloaltonetworks.com/pan-os/9-1/pan-os-admin/subscriptions/activate-subscription-licenses",
        RemediationStepCondition.VENDOR_FORTINET ->
          """
            |1. Login via ssh to the Fortinet firewall and execute the FortiOS “get system fortiguard-service status” and “diag autoupdate versions” commands to list current update package versions and license expiry status.
            |2. Login via https to the Fortinet firewall and go to the menu System > Dashboard > Status to locate the License Information widget. All subscribed services should have a green checkmark, indicating that connections are successful. A gray X indicates that the FortiGate unit cannot connect to the FortiGuard network, or that the FortiGate unit is not registered. A red X indicates that the FortiGate unit was able to connect but that a subscription has expired or has not been activated.
            |3. Login via https to the Fortinet firewall to view the FortiGuard connection status by going to System > Config > FortiGuard menu.
            |4. Purchase additional licenses if are needed.
            |5. Consider enabling the issue email setting to the Fortinet firewall in order to receive a issue email prior to FortiGuard license expiration (notification date range: 1 - 100 days). The current issue email status can be provided with the next command: “get alertemail setting”. More details can be found in the next link: https://docs.fortinet.com/uploaded/files/2798/fortigate-cli-ref-54.pdf
            |6. For more information about licensing review  the next  online article “Setting up FortiGuard services” : http://cookbook.fortinet.com/setting-fortiguard-services-54/
            |7. Contact Fortinet Technical support at https://support.fortinet.com/ for further assistance.""".stripMargin,
        RemediationStepCondition.VENDOR_BLUECAT ->
         """|1. Use the “lcd show” command to display the complete list of licensed features and capacities of a particular Bluecat appliance.
            |2. In case of lost activation-key, please send the serial number your Bluecat support contact. A new key will be provided by the Bluecat licensing team.
            |3. If you see the correct activation-key under the "lcd show" but it doesn’t take effect then try to reboot the device. If this doesn't, please then reach your Bluecat support contact.""".stripMargin
      )
    )
  }
}
