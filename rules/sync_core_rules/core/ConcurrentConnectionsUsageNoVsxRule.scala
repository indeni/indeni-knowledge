package com.indeni.server.rules.library.core
import com.indeni.ruleengine.expressions.OptionalExpression
import com.indeni.ruleengine.expressions.conditions.GreaterThanOrEqual
import com.indeni.ruleengine.expressions.core.{ConstantExpression, StatusTreeExpression}
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.ruleengine.expressions.math.{DivExpression, TimesExpression}
import com.indeni.server.common.ParameterValue
import com.indeni.server.common.data.conditions.Equals
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules.library.{ConditionalRemediationSteps, PerDeviceRule, RuleHelper}
import com.indeni.server.rules.{DeviceCategory, DeviceKey, RemediationStepCondition, RuleCategory, RuleContext, RuleMetadata}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class ConcurrentConnectionsUsageNoVsxRule() extends PerDeviceRule with RuleHelper {

  private val highThresholdParameterName = "High_Threshold_of_Connection_usage"
  private val highThresholdParameter = new ParameterDefinition(highThresholdParameterName,
    "",
    "High Threshold of Concurrent Connection Usage",
    "What is the threshold for the concurrent connection usage for which once it is crossed an issue will be triggered.",
    UIType.DOUBLE,
    new ParameterValue((80.0).asInstanceOf[Object])
  )

  override val metadata: RuleMetadata = RuleMetadata.builder("concurrent_connection_limit_novsx", "Concurrent connection limit nearing",
    "indeni will trigger an issue the number of connections for a device is too high.", AlertSeverity.ERROR,
    categories = Set(RuleCategory.HealthChecks), deviceCategory = DeviceCategory.AllDevices).configParameter(highThresholdParameter).build()

  override def expressionTree(context: RuleContext): StatusTreeExpression = {
    val actualValue = TimeSeriesExpression[Double]("concurrent-connections").last
    val threshold: OptionalExpression[Double] = getParameterDouble(highThresholdParameter)
    val limit = TimeSeriesExpression[Double]("concurrent-connections-limit").last

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), !Equals("vsx", "true")),

      StatusTreeExpression(
        // The time-series we check the test condition against:
        SelectTimeSeriesExpression[Double](context.tsDao, Set("concurrent-connections", "concurrent-connections-limit"), denseOnly = false),

        // The condition which, if true, we have an issue. Checked against the time-series we've collected
        GreaterThanOrEqual(
          actualValue,
          TimesExpression(limit, DivExpression(threshold, ConstantExpression(Some(100.0)))))

      ).withRootInfo(
        getHeadline(),
        scopableStringFormatExpression("This device has a high number of concurrent connections: %.0f (vs limit of %.0f) which is above the threshold of %.0f%%.", actualValue, limit, threshold),
        ConditionalRemediationSteps("Review why this may be happening and consider upgrading the device or redirecting traffic.",
          RemediationStepCondition.VENDOR_CP -> "Consider enabling aggressive aging if it is not yet enabled: <a target=\"blank\" href=\"https://sc1.checkpoint.com/documents/R76/CP_R76_IPS_AdminGuide/12857.htm#o12861\">Aggressive Aging Configurations</a>",
          RemediationStepCondition.VENDOR_PANOS -> "Compare the products and the maximum sessions allowed: <a target=\"_blank\" href=\"https://www.paloaltonetworks.com/products/product-selection\">Compare Firewalls</a>",
          RemediationStepCondition.VENDOR_BLUECOAT ->
            """The number of current connected clients has reached the device limit.
              |1. Login to the device's web interface and click on "Statistics" -> "Advanced" -> "HTTP" -> "Show HTTP Statistics".
              |2. Check  the currently established client connections value and compare it to the maximum acceptable concurrent client connections.
              |3. Limit the number of users or upgrade your license or appliance after consulting with Symantec support.""".stripMargin,
          RemediationStepCondition.VENDOR_JUNIPER ->
            """|1. Each device has a limit for concurrent sessions or connections based on the hardware capacity. Exceeding this limit will cause traffic drops.
               |2. Run the "show security flow session summary" to review the current number of sessions.
               |3. Consider enabling aggressive aging if it is not yet enabled.
               |4. Review the following articles on Juniper TechLibrary for more information: <a target="_blank" href="https://www.juniper.net/documentation/en_US/junos/topics/concept/security-session-capacity-device-expanding.html">Expanding Session Capacity by Device</a>
               |<a target="_blank" href="https://www.juniper.net/documentation/en_US/junos/topics/example/session-termination-for-srx-series-controlling-cli.html">Example: Controlling Session Termination for SRX Series Services Gateways</a>
               |5. If the problem persists, contact the Juniper Networks Technical Assistance Center (JTAC).""".stripMargin,
          RemediationStepCondition.OS_CISCO_ASA ->
          """1. Run the “show resource usage resource conns” command to view the current number of TCP or UDP connections, the peak and the allowed limit. The value under the deny column shows the sessions that were denied because they exceeded the resource limit.
            |2. Run the “show resource usage resource rate conns” command to view the number of TCP or UDP connections per second. For more options for this command refer to the command reference guide: https://www.cisco.com/c/en/us/td/docs/security/asa/asa-command-reference/S/cmdref3/s11.html#pgfId-1527546
            |3. Execute the “show con count” command to get more connection information including the most used and in use connections.
            |4. Consider to increase the maximum limit of the connection to this context in case that Multi Mode is enabled to the firewall.
            |5. Identify any host that could cause hung connections to the firewall and clear.
            |6. If the high number of connections is expected then consider to upgrade the firewall.""".stripMargin
        )
      ).asCondition()
    ).withoutInfo()
  }
}
