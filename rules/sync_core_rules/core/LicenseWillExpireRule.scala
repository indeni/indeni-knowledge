package com.indeni.server.rules.library.core
import com.indeni.apidata.time.TimeSpan
import com.indeni.apidata.time.TimeSpan.TimePeriod
import com.indeni.ruleengine.expressions.conditions.{And, GreaterThan, LesserThan}
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data._
import com.indeni.ruleengine.expressions.math.PlusExpression
import com.indeni.ruleengine.expressions.utility.NowExpression
import com.indeni.server.common.data.conditions.True
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules._
import com.indeni.server.rules.library.{ConditionalRemediationSteps, PerDeviceRule, RuleHelper}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity


case class LicenseWillExpireRule() extends PerDeviceRule with RuleHelper {

  private val highThresholdParameterName = "Ahead_Alerting_Threshold"
  private val highThresholdParameter = new ParameterDefinition(highThresholdParameterName,
                                                               "",
                                                               "Expiration Threshold",
                                                               "How long before expiration should Indeni notify.",
                                                               UIType.TIMESPAN,
                                                               TimeSpan.fromDays(56))

  override val metadata: RuleMetadata = RuleMetadata
    .builder("cross_vendor_license_expiration",
             "License expiration nearing",
             "indeni will trigger an issue when a license is about to expire.",
             AlertSeverity.WARN,
              categories = Set(RuleCategory.OngoingMaintenance),
              deviceCategory = DeviceCategory.AllDevices)
    .configParameter(highThresholdParameter)
    .build()

  override def expressionTree(context: RuleContext): StatusTreeExpression = {
    val actualValue = TimeSeriesExpression[Double]("license-expiration").last.toTimeSpan(TimePeriod.SECOND)

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),
      // What constitutes an issue
      StatusTreeExpression(
        // The additional tags we care about (we'll be including this in alert data)
        SelectTagsExpression(context.tsDao, Set("name"), withTagsCondition("license-expiration")),
        StatusTreeExpression(
          // The time-series we check the test condition against:
          SelectTimeSeriesExpression[Double](context.tsDao, Set("license-expiration"), denseOnly = false),
          // The condition which, if true, we have an issue. Checked against the time-series we've collected
          And(
            GreaterThan(actualValue, NowExpression()),
            LesserThan(actualValue,
                       PlusExpression(NowExpression(), getParameterTimeSpanForTimeSeries(highThresholdParameter)))
          )

          // The Alert Item to add for this specific item
        ).withSecondaryInfo(
            scopableStringFormatExpression("${scope(\"name\")}"),
            scopableStringFormatExpression("Will expire on %s", timeSpanToDateExpression(actualValue)),
            title = "Affected Licenses"
          )
          .asCondition()
      ).withoutInfo().asCondition()
    ).withRootInfo(
      getHeadline(),
      ConstantExpression("One or more licenses are about to expire. See the list below."),
      ConditionalRemediationSteps(
        "Renew any licenses that need to be renewed.",
        RemediationStepCondition.VENDOR_CP -> "Make sure you have purchased the required licenses and have updated them in your management server: https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk33089",
        RemediationStepCondition.VENDOR_PANOS -> "Review this page on licensing: https://www.paloaltonetworks.com/documentation/80/pan-os/pan-os/getting-started/activate-licenses-and-subscriptions",
        RemediationStepCondition.VENDOR_CISCO ->
          """1. Run the “show license usage” NX-OS command to display information about the current license usage and the expire date.
            |2. Run the “show license” NX-OS command to view the installed licenses.
            |3. Run the “show license usage XXX” NX-OS command e.g.” sh license usage ENHANCED_LAYER2_PKG” to display information about the activated features which utilize this license.
            |4. Consider activate the grace-period for the license.
            |5. Order new license from the CISCO.
            |6. For more information please review: <a target="_blank" href="https://www.cisco.com/c/m/en_us/techdoc/dc/reference/cli/nxos/commands/fund/show-license-usage.html">Cisco Guide</a> """.stripMargin,
        RemediationStepCondition.VENDOR_FORTINET ->
          """
            |1. Login via ssh to the Fortinet firewall and execute the FortiOS “get system fortiguard-service status” and “diag autoupdate versions” commands to list current update package versions and license expiry status.
            |2. Login via https to the Fortinet firewall and go to the menu System > Dashboard > Status to locate the License Information widget. All subscribed services should have a green checkmark, indicating that connections are successful. A gray X indicates that the FortiGate unit cannot connect to the FortiGuard network, or that the FortiGate unit is not registered. A red X indicates that the FortiGate unit was able to connect but that a subscription has expired or has not been activated.
            |3. Login via https to the Fortinet firewall to view the FortiGuard connection status by going to System > Config > FortiGuard menu.
            |4. Purchase additional licenses if are needed.
            |5. Consider enabling the issue email setting to the Fortinet firewall in order to receive a issue email prior to FortiGuard license expiration (notification date range: 1 - 100 days). The current issue email status can be provided with the next command: “get alertemail setting”. More details can be found in the next link: https://docs.fortinet.com/uploaded/files/2798/fortigate-cli-ref-54.pdf
            |6. For more information about licensing review  the next  online article “Setting up FortiGuard services” : http://cookbook.fortinet.com/setting-fortiguard-services-54/
            |7. Contact Fortinet Technical support at https://support.fortinet.com/ for further assistance.""".stripMargin,
        RemediationStepCondition.VENDOR_JUNIPER ->
          """|1. Run the “show system license usage” command to view installed licenses.
             |2. Check the current license usage and the expiry date for each license.
             |3. Review the following article on Juniper tech support site: <a target="_blank" href="https://www.juniper.net/documentation/en_US/junos/topics/task/verification/junos-licenses-verifying.html">Verifying Junos OS License Installation (CLI)</a>""".stripMargin,
        RemediationStepCondition.OS_CISCO_ASA ->
          """|1. Use the “show version” and/or the “show activation-key” command to display the complete list of licensed features and capacities of a particular Cisco ASA device along with the activation information
             |2. In case of lost activation-key then it is needed to send the serial number and/or PAK number to licensing@cisco.com. A new key will be provided by Cisco licensing team.
             |3. If you see the correct activation-key under the "show activation-key" or “show version” output  but it doesn’t take effect then try to reboot the device. If this doesn't help then reach the licensing@cisco.com.
             |4. Refer to the next ASA license guide for more information: http://www.ciscopress.com/articles/article.asp?p=2209314""".stripMargin,
        RemediationStepCondition.VENDOR_BLUECAT ->
          """|1. Use the “lcd show” command to display the complete list of licensed features and capacities of a particular Bluecat appliance.
             |2. In case of lost activation-key, please send the serial number your Bluecat support contact. A new key will be provided by the Bluecat licensing team.
             |3. If you see the correct activation-key under the "lcd show" but it doesn’t take effect then try to reboot the device. If this doesn't, please then reach your Bluecat support contact.""".stripMargin
      )
    )
  }
}
