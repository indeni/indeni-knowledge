package com.indeni.server.rules.library.core

import com.indeni.apidata.time.TimeSpan
import com.indeni.ruleengine.Scope.{Scope, ImplicitScopeValueHelper}
import com.indeni.ruleengine.expressions.{Expression, conditions}
import com.indeni.ruleengine.expressions.conditions.{And, Equals, Not, Or}
import com.indeni.ruleengine.expressions.core._
import com.indeni.ruleengine.expressions.data._
import com.indeni.ruleengine.expressions.scope.ScopableExpression
import com.indeni.ruleengine.expressions.utility.IsEmptyExpression.IsEmptyExpressionHelper
import com.indeni.ruleengine.expressions.utility.SeqDiffWithoutOrderExpression
import com.indeni.server.common.AlertNotificationSettings
import com.indeni.server.common.AlertNotificationSettings.NONE
import com.indeni.server.common.data.conditions.True
import com.indeni.server.rules._
import com.indeni.server.rules.library.{ConditionalRemediationSteps, PerDeviceRule, RuleHelper}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity


case class CrossVendorStaticRoutingTableChangeRule() extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata.builder("cross_vendor_static_routing_table_change", "Static routing table has changed",
    "A change of the static routing table can be caused intentionally, or unintentionally. If a static route table is changed, Indeni will trigger an issue", AlertSeverity.WARN,
    categories = Set(RuleCategory.OrganizationStandards), deviceCategory = DeviceCategory.AllDevices)
    .defaultAction(AlertNotificationSettings(NONE))
    .disableGlobalRuleSet(true)
    .build()

  override def expressionTree(context: RuleContext): StatusTreeExpression = {
    val currentValue = SnapshotExpression("static-routing-table").asMulti().mostRecent().value()
    val previousValue = SnapshotExpression("static-routing-table").asMulti().middle()
    val diff = SeqDiffWithoutOrderExpression(
      NoneableMultiSnapshotExtractVectorExpression(previousValue.optionValue(), false, "network", "mask", "destination", "next-hop"),
      MultiSnapshotExtractVectorExpression(currentValue, false, "network", "mask", "destination", "next-hop"),
    )

    val diffLineHeadline = new ScopableExpression[String] {
      override protected def evalWithScope(time: Long, scope: Scope): String = scope.getVisible("routeConfig").get.asInstanceOf[Seq[String]].mkString(", ")
      override def args: Set[Expression[_]] = Set()
    }

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
      StatusTreeExpression(
        // The time-series we check the test condition against:
        SelectSnapshotsExpression(context.snapshotsDao, Set("static-routing-table"), historyLength = ConstantExpression(TimeSpan.fromMinutes(60))).multi(),

        // The condition which, if true, we have an issue. Checked against the time-series we've collected
        Or(diff.missings.nonEmpty, diff.redundants.nonEmpty),
        multiInformers = Set(
          MultiIssueInformer(
            diffLineHeadline,
            EMPTY_STRING,
            "Added routes"
          ).iterateOver(
            diff.missings,
            "routeConfig",
            conditions.True
          ),
          MultiIssueInformer(
            diffLineHeadline,
            EMPTY_STRING,
            "Removed routes"
          ).iterateOver(
            diff.redundants,
            "routeConfig",
            conditions.True
          )
        )
      ).withoutInfo().asCondition()

      // Details of the alert itself
    ).withRootInfo(
        getHeadline(),
        ConstantExpression("Static routing table changed"),
        ConditionalRemediationSteps("Review the added/remove routes. You can click on the circle with the hyphen inside next to the route to acknowledge this specific change."
        )
    )
  }
}
