package com.indeni.server.rules.library.core
import com.indeni.apidata.time.TimeSpan
import com.indeni.apidata.time.TimeSpan.TimePeriod
import com.indeni.ruleengine.expressions.conditions.{And, LesserThan, Or, Equals => RuleEquals}
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression, _}
import com.indeni.ruleengine.expressions.math.MinusExpression
import com.indeni.server.common.data.conditions.True
import com.indeni.server.rules._
import com.indeni.server.rules.library.{ConditionalRemediationSteps, PerDeviceRule, RuleHelper}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity


case class PanwAppPackageNotUpdatedRule() extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata
    .builder(
      "panw_app_lag_check",
      "Application package not up to date",
      "indeni will trigger an issue if the application package has not been updated according to schedule.",
      AlertSeverity.WARN,
      categories= Set(RuleCategory.VendorBestPractices),
      deviceCategory = DeviceCategory.PaloAltoNetworksFirewalls
    )
    .build()

  override def expressionTree(context: RuleContext): StatusTreeExpression = {
    val currentTime = TimeSeriesExpression[Double]("current-datetime").last.toTimeSpan(TimePeriod.SECOND)
    val acceptableLag = TimeSeriesExpression[Double]("app-update-acceptable-lag").last.toTimeSpan(TimePeriod.MILLISECOND)
    val installedPackageDate =
      TimeSeriesExpression[Double]("panw-installed-app-release-date").last.toTimeSpan(TimePeriod.SECOND)
    val externalServiceError = TimeSeriesExpression[Double]("external-service-error").last
    val contentUpdateState = SnapshotExpression("content-update-state").asSingle().mostRecent().value().noneable

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),
      // What constitutes an issue
      Or(
        StatusTreeExpression(
          // The time-series we check the test condition against:
          SelectTimeSeriesExpression[Double](context.tsDao, Set("external-service-error"), denseOnly = false),
          // The condition which, if true, we have an issue. Checked against the time-series we've collected
          externalServiceError.is(1)
          // The Alert Item to add for this specific item
        ).withSecondaryInfo(
          ConstantExpression("Error retrieving version number in cloud"),
          EMPTY_STRING,
          "Issues"
        )
          .asCondition(),
        StatusTreeExpression(
          // The time-series we check the test condition against:
          SelectTimeSeriesExpression[Double](context.tsDao,
            Set("panw-installed-app-release-date",
              "current-datetime",
              "app-update-acceptable-lag"),
            denseOnly = false),
          // The condition which, if true, we have an issue. Checked against the time-series we've collected
          And(
            LesserThan(acceptableLag, MinusExpression[TimeSpan](currentTime, installedPackageDate)),
            StatusTreeExpression(
              SelectSnapshotsExpression(context.snapshotsDao, Set("content-update-state")).single(),
              RuleEquals(RuleHelper.createComplexStringConstantExpression("false"), contentUpdateState)
            ).withoutInfo().asCondition()
          )
          // The Alert Item to add for this specific item
        ).withSecondaryInfo(
          ConstantExpression("Application package not up to date"),
          scopableStringFormatExpression(
            "The current app update package's release date is %s. This indicates the update schedule is not operating correctly.",
            timeSpanToDateExpression(installedPackageDate)
          ),
          "Issues"
        )
          .asCondition()
      )
    ).withRootInfo(
      getHeadline(),
      scopableStringFormatExpression("Application package not up to date"),
      ConditionalRemediationSteps(
        """|Run the command 'request content upgrade check’.
           |
           |If you do not receive an error you may request an upgrade manually by running ‘request content upgrade install version latest’.
           |
           |Review possible causes for the update package not being checked successfully.
           |1. Try to ping from the firewall "ping host updates.paloaltonetworks.com". If fails DNS resolution check configured DNS servers.
           |2. Check the source interface for Palo Alto Networks Services and routes for that interface to the Internet.
           |
           |Review possible root causes for the update package not being updated successfully. Some things that can occur:
           |1. Error communicating with the cloud.
           |2. Failed to install after download.
           |3. Corrupt content database that needs repair.
           |4. Less mp-log ms.log on the firewall CLI to look for error messages
           |
           |Check the Indeni Crowd https://community.indeni.com and https://live.paloaltonetworks.com for possible reasons of dynamic update failures (content or anti-virus).
        """.stripMargin)
    )
  }
}
