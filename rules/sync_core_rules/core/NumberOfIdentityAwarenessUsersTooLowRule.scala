package com.indeni.server.rules.library.core
import com.indeni.apidata.time.TimeSpan.TimePeriod
import com.indeni.ruleengine.expressions.OptionalExpression
import com.indeni.ruleengine.expressions.casting.date.{DayRangeParseExpression, StartOfTheDayExpression}
import com.indeni.ruleengine.expressions.conditions.{And, GreaterThanOrEqual, LesserThanOrEqual}
import com.indeni.ruleengine.expressions.core.{EMPTY_STRING, StatusTreeExpression}
import com.indeni.ruleengine.expressions.data._
import com.indeni.ruleengine.expressions.math.PlusExpression
import com.indeni.server.common.data.conditions.True
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules.config.expressions.DynamicParameterExpression
import com.indeni.server.rules.library.{ConditionalRemediationSteps, PerDeviceRule, RuleHelper}
import com.indeni.server.rules.{DeviceKey, RuleContext, RuleMetadata, _}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class NumberOfIdentityAwarenessUsersTooLowRule() extends PerDeviceRule with RuleHelper {

  private[library] val LowThresholdParameterName = "identity_awareness_users_minimum"
  private val lowThresholdParameter = new ParameterDefinition(
    LowThresholdParameterName,
    "",
    "Minimum Amount of users Threshold",
    "How many minimum users are logged in",
    UIType.INTEGER,
    0
  )

  private val dayRangesParameterName = "day_ranges_whitelist"
  private val dayRangesParameter = new ParameterDefinition(
    dayRangesParameterName,
    "",
    "Day Ranges (Whitelist)",
    "Enter the list of dayRanges that should be checked, each one on its own line according to the following format:\n" +
      "\"DAY,HH:MM,HH:MM\".\n" +
      "Indeni will alert if the numbers of users is equal or lower than the threshold only during those times.",
    UIType.MULTILINE_TEXT,
    ""
  )

  override val metadata: RuleMetadata = RuleMetadata
    .builder(
      "number_of_awareness_users_too_low",
      "Low Identity Awareness User Count",
      "Indeni can identify if the current Identity Awareness User Count is below acceptable levels during active hours.",
      AlertSeverity.ERROR,
      categories= Set(RuleCategory.VendorBestPractices),
      deviceCategory = DeviceCategory.CheckPointDevices
    )
    .configParameters(lowThresholdParameter, dayRangesParameter)
    .build()

  override def expressionTree(context: RuleContext): StatusTreeExpression = {
    val actualValue = TimeSeriesExpression[Double]("identity-awareness-users-actual").last
    val date = TimeSeriesExpression[Double]("current-datetime").last.toTimeSpan(TimePeriod.MILLISECOND)
    val threshold: OptionalExpression[Double] = getParameterDouble(lowThresholdParameter)
    val dayRanges = DynamicParameterExpression.withConstantDefault(dayRangesParameterName, Seq[String]())
    val timezone =
      SingleSnapshotExtractExpression(SnapshotExpression("timezone").asSingle().mostRecent().value(), "value")

    StatusTreeExpression(
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),
      StatusTreeExpression(
        SelectTimeSeriesExpression[Double](context.tsDao,
                                           Set("identity-awareness-users-actual", "current-datetime"),
                                           denseOnly = false),
        And(
          StatusTreeExpression(
            SelectSnapshotsExpression(context.snapshotsDao, Set("timezone")).single(),
            And(
              GreaterThanOrEqual(
                date,
                PlusExpression(
                  StartOfTheDayExpression(date, timezone),
                  DayRangeParseExpression(date, timezone, dayRanges, upper = false),
                )
              ),
              GreaterThanOrEqual(
                PlusExpression(
                  DayRangeParseExpression(date, timezone, dayRanges, upper = true),
                  StartOfTheDayExpression(date, timezone)
                ),
                date
              )
            )
          ).withSecondaryInfo(
            scopableStringFormatExpression("Actual value: %.0f, Threshold: %.0f", actualValue, threshold),
            EMPTY_STRING,
            "User Count",
          ).asCondition(),
          LesserThanOrEqual(actualValue, threshold)
        )
      ).withRootInfo(
          getHeadline(),
            scopableStringFormatExpression("The current Identity Awareness User Count is below acceptable levels during active hours. Generally, this indicates that the gateway's ability to communicate with the Active Directory is down and should be investigated."),
          ConditionalRemediationSteps(
            "Please review the following article to determine the root cause behind the low count: http://downloads.checkpoint.com/dc/download.htm?ID=12625. Otherwise, contact your Check Point technical support provider. If the threshold is too sensitive, please request for the Indeni threshold to be modified as necessary.")
        )
        .asCondition()
    ).withoutInfo()
  }

}
