package com.indeni.server.rules.library

import com.indeni.apidata.metrics.GlobalTags
import com.indeni.apidata.time.TimeSpan
import com.indeni.ruleengine.Scope.{Scope, ImplicitScopeValueHelper}
import com.indeni.ruleengine.expressions.conditions.{CompareCondition, GreaterThanOrEqual, LesserThan, _}
import com.indeni.ruleengine.expressions.core.{ConstantExpression, StatusTreeExpression}
import com.indeni.ruleengine.expressions.data.{SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.ruleengine.expressions.scope.ScopableExpression
import com.indeni.ruleengine.expressions.utility.SwitchCaseExpression
import com.indeni.ruleengine.expressions.{EvaluationException, Expression, OptionalExpression}
import com.indeni.server.common.data.Snapshot.{MultiDimension, SingleDimension}
import com.indeni.server.common.data.conditions.{Equals, Or, TagsStoreCondition}
import com.indeni.server.common.data.{TaggedTimeSeries, TimeSeriesDao}
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.rules
import com.indeni.server.rules._
import com.indeni.server.rules.config.expressions.DynamicParameterExpression
import com.indeni.server.rules.library.RuleHelper._
import com.indeni.server.rules.library.yamlrules._
import com.typesafe.scalalogging.Logger
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

import scala.collection.GenTraversable

trait RuleHelper {
  self: Rule =>

  protected def getParameter[A <% Ordered[A]](parameter: ParameterDefinition): OptionalExpression[A] = {
    parameter.getUiType() match {
      case ParameterDefinition.UIType.DOUBLE  => getParameterDouble(parameter).asInstanceOf[OptionalExpression[A]]
      case ParameterDefinition.UIType.INTEGER => getParameterInt(parameter).asInstanceOf[OptionalExpression[A]]
      case ParameterDefinition.UIType.TIMESPAN => getParameterTimeSpanForTimeSeries(parameter).asInstanceOf[OptionalExpression[A]]
      case _ => ConstantExpression[A](null.asInstanceOf[A]).noneable
    }
  }

  protected def getParameterDouble(parameter: ParameterDefinition): OptionalExpression[Double] =
    RuleHelper.getParameterDouble(parameter.getName, parameter.getDefaultValue.asDouble.doubleValue)

  protected def getParameterInt(parameter: ParameterDefinition): OptionalExpression[Int] = {
    DynamicParameterExpression
      .withConstantDefault(parameter.getName, parameter.getDefaultValue.asInteger().intValue())
      .noneable
  }

  /**
    * Timespan is retrieved as seconds, because time-series for timespan/duration are Doubles representing seconds.
    *
    * @param parameter
    * @return
    */
  protected def getParameterTimeSpanForTimeSeries(parameter: ParameterDefinition): OptionalExpression[TimeSpan] = {
    DynamicParameterExpression
      .withConstantDefault(parameter.getName, parameter.getDefaultValue.asTimeSpan)
      .noneable
  }

  /**
    * When timestamp is used for rule configuration, we need an actual TimeSpan object and not the Double value returned by
    * getParameterTimeSpanForTimeSeries.
    *
    * @param parameter
    * @return
    */
  protected def getParameterTimeSpanForRule(parameter: ParameterDefinition): Expression[TimeSpan] = {
    DynamicParameterExpression
      .withConstantDefault(parameter.getName, parameter.getDefaultValue.asTimeSpan())
  }

  def scopableStringFormatExpression(format: String, exprs: Expression[_]*): ScopableExpression[String] = RuleHelper.scopableStringFormatExpression(format, exprs: _*)

  val dateFormat = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")

  protected def timeSpanToDateExpression(dateMetric: Expression[Option[TimeSpan]]): Expression[Option[String]] =
    new Expression[Option[String]] {

      override def eval(time: Long): Option[String] = {
        dateMetric.eval(time) match {
          case Some(x: TimeSpan) => Some(dateFormat.print(new DateTime(x.toMilliseconds)))
          case _                 => None
        }
      }

      /**
        * @return The argument expressions.
        */
      override def args: Set[Expression[_]] = Set(dateMetric)
    }

  def getHeadline(): Expression[String] = ConstantExpression(metadata.friendlyName.replaceFirst(".*?\\: ", ""))

  protected def withTagsCondition(firstKey: String, keys: String*): TagsStoreCondition = {
    val conditions: Seq[TagsStoreCondition] = (keys :+ firstKey).map(Equals(GlobalTags.MetricNameTag, _))

    if (conditions.length == 1) conditions.head
    else Or(conditions, nonRedundantHint = true)
  }

  protected def generateCompareCondition[A <% Ordered[A], B](direction: ThresholdDirection, first: Expression[Option[A]], second: Expression[Option[B]])(
    implicit ev: A =:= B): CompareCondition[A, B] = {
    if (direction == rules.ThresholdDirection.ABOVE) GreaterThanOrEqual(first, second)
    else LesserThan(first, second)
  }

  protected def generateDevicePassiveAndPassiveLinkStateCondition(ts: TimeSeriesDao)= {
    Not(And(StatusTreeExpression(
      SelectTimeSeriesExpression[Double](ts, Set("device-is-passive"), denseOnly = false),
      TimeSeriesExpression("device-is-passive").last.is(1.0)
    ).withoutInfo().asCondition().orElse(Some(false)),
      StatusTreeExpression(
        SelectTimeSeriesExpression[Double](ts, Set("passive-link-state"), denseOnly = false),
        TimeSeriesExpression("passive-link-state").last.is(0)
      ).withoutInfo().asCondition().orElse(Some(false))))
  }

  protected def generateNetworkInterfaceAdminStateCondition(ts: TimeSeriesDao, expected: Double): OptionalExpression[Boolean] = {

    networkInterfaceAdminState(ts, expected, positive = false)(logger)
  }
}

case class ScopableStringFormatExpression(format: String, exprs: Seq[Expression[_]]) extends ScopableExpression[String] {

  protected def evalWithScope(time: Long, scope: Scope): String = {

    def getScopeField(scope: Scope, field: String): String = {
      if (field.contains(":")) {
        val NameSpaceAndMetrics = field.split("\\:")
        val metrics = NameSpaceAndMetrics.tail.head
        val nameSpace = if (NameSpaceAndMetrics.head == "") None else Some(NameSpaceAndMetrics.head)
        if (metrics.contains(">")) {
          val MetricsName = metrics.split("\\>")
          if (nameSpace.getOrElse("") == "TS") {
            val innerMetric = MetricsName.tail.head
            scope
              .getInvisible(MetricsName.head, nameSpace)
              .get
              .asInstanceOf[TaggedTimeSeries[Double]]
              .tags(innerMetric)
              .toString
          } else {
            scope.getInvisible(metrics, nameSpace).getOrElse("").toString
          }
        } else {
          scope.getInvisible(metrics, nameSpace).getOrElse("").toString
        }
      } else {
        scope.getVisible(field).get.toString
      }
    }

    // Evaluate all the expressions into normal objects
    val evaluatedArgs = exprs.map { expression =>
      expression.eval(time) match {
        case Some(value) => value
        case value       => value
      }
    }

    // First handle the "%" of the format structure (because they may be in the scope data)
    val formattedString = format.format(evaluatedArgs: _*)

    // Do like s"$scope("blahblah")" handling. Trying to get this done via code was getting insane,
    // so we're just handling it ourselves (but don't support other parameters at this time).
    scopeVarPattern.replaceAllIn(formattedString, m => getScopeField(scope, m.group("scopeField")))
  }

  override def args: Set[Expression[_]] = exprs.toSet
}

object RuleHelper {
  val scopeVarPattern = "\\$\\{scope\\(\"([^\"]+)\"\\)\\}".r("scopeField")
  def scopableStringFormatExpression(format: String, exprs: Expression[_]*): ScopableExpression[String] =
    ScopableStringFormatExpression(format, exprs)

  def getParameterDouble(parameterName: String, parameterDefaultValue: Double): OptionalExpression[Double] = {
    DynamicParameterExpression
      .withConstantDefault(parameterName, parameterDefaultValue)
      .noneable
  }
  def getParameterString(parameterName: String, parameterDefaultValue: String): OptionalExpression[String] = {
    DynamicParameterExpression
      .withConstantDefault(parameterName, parameterDefaultValue)
      .noneable
  }

  def createComplexStringConstantExpression(value: String): Expression[Option[SingleDimension]] =
    ConstantExpression(Some(Map("value" -> value)))

  def createEmptyComplexArrayConstantExpression(): Expression[Option[MultiDimension]] =
    ConstantExpression(Some(Seq()))

  def networkInterfaceAdminState(ts: TimeSeriesDao, expected: Double, positive: Boolean = true)(logger: Logger): OptionalExpression[Boolean] = {

    val selectState = SelectTimeSeriesExpression[Double](ts, Set("network-interface-admin-state"), denseOnly = false)
    val lastStateValue = TimeSeriesExpression[Double]("network-interface-admin-state").last
    val stateValueCondition = if (positive) lastStateValue.is(expected) else lastStateValue.isNot(expected)

    StatusTreeExpression(selectState, stateValueCondition)(logger).withoutInfo().asCondition().orElse(Some(true))
  }

  // TODO: Consider making SimpleMetricConditionType and FullMetricConditionType extend some generic trait to allow uniting the next two methods
  private def recursivelyIterateOnSimpleMetricCondition[T](condition: ConditionNode[SimpleMetricConditionType.type], f: BasicSimpleMetricConditionNode => Set[T]): Set[T] = condition match {
    case NotNode(innerCond) => recursivelyIterateOnSimpleMetricCondition(innerCond, f)
    case AndNode(innerConds) => innerConds.flatMap(recursivelyIterateOnSimpleMetricCondition(_, f)).toSet
    case OrNode(innerConds) => innerConds.flatMap(recursivelyIterateOnSimpleMetricCondition(_, f)).toSet
    case TrueSimpleMetricConditionNode => Set.empty
    case FalseSimpleMetricConditionNode => Set.empty
    case basicCondition: BasicSimpleMetricConditionNode => f(basicCondition)
  }

  private def recursivelyIterateOnFullMetricCondition[T](condition: ConditionNode[FullMetricConditionType.type], f: BasicFullMetricConditionNode => Set[T]): Set[T] = condition match {
    case NotNode(innerCond) => recursivelyIterateOnFullMetricCondition(innerCond, f)
    case AndNode(innerConds) => innerConds.flatMap(recursivelyIterateOnFullMetricCondition(_, f)).toSet
    case OrNode(innerConds) => innerConds.flatMap(recursivelyIterateOnFullMetricCondition(_, f)).toSet
    case TrueFullMetricConditionNode => Set.empty
    case FalseFullMetricConditionNode => Set.empty
    case basicCondition: BasicFullMetricConditionNode => f(basicCondition)
  }

  def getAllMetricsFromSimpleMetricsCondition(condition: ConditionNode[SimpleMetricConditionType.type], metricType: MetricType): Set[String] =
    recursivelyIterateOnSimpleMetricCondition(condition, {
      case BasicSimpleMetricConditionNode(metricNode, _) =>
        if (metricNode.metricType == metricType) Set(metricNode.metricName) else Set.empty
    })

  def getAllAlertItemIdsFromFullMetricsCondition(condition: ConditionNode[FullMetricConditionType.type]): Set[String] = {
    recursivelyIterateOnFullMetricCondition(condition, {
      case BasicFullMetricConditionNode(tagsAggregation, _) => tagsAggregation.alertItemsId.toSet
    })
  }

  def getAllConfigParametersFromFullMetricsCondition(condition: ConditionNode[FullMetricConditionType.type]): Set[(String, Option[ConfigurationParameterType])] = {
    recursivelyIterateOnFullMetricCondition(condition, {
      case BasicFullMetricConditionNode(_, simpleMetricsCondition) => getAllConfigParametersFromSimpleMetricsCondition(simpleMetricsCondition)
    })
  }

  private def getConfigParameterFromConstantNode(constantNode: ConstantNode): Option[(String, Option[ConfigurationParameterType])] = constantNode match {
    case StringConfigurationParameterNode(parameterName) => Some((parameterName, Some(StringParameterType)))
    case DoubleConfigurationParameterNode(parameterName) => Some((parameterName, Some(DoubleParameterType)))
    case UntypedConfigurationParameterNode(parameterName) => Some((parameterName, None))
    case _ => None
  }

  def getAllMetricTagsFromSimpleMetricsCondition(condition: ConditionNode[SimpleMetricConditionType.type]): Set[String] = {
    recursivelyIterateOnSimpleMetricCondition(condition, {
      // TODO: Add logic here when we support some usage of metric tags in the condition
      case BasicSimpleMetricConditionNode(_, _) => Set.empty
    })
  }

  def getAllConfigParametersFromSimpleMetricsCondition(condition: ConditionNode[SimpleMetricConditionType.type]): Set[(String, Option[ConfigurationParameterType])] =
    recursivelyIterateOnSimpleMetricCondition(condition, {
      // TODO: Is there a way to do this without enumerating all types of predicate nodes? Perhaps somehow with reflection?
      // This is important because if we change string literals to string constants that allow configuration parameters,
      // this code will continue working and not find the configuration parameters
      case BasicSimpleMetricConditionNode(_, metricPredicateNode) =>
        metricPredicateNode match {
          case ComparisonPredicateNode(_, constantNode) => getConfigParameterFromConstantNode(constantNode).toSet
          case ContainsStringPredicateNode(value)       => getConfigParameterFromConstantNode(value).toSet
          case ContainsAtKeyPredicateNode(value, _)     => getConfigParameterFromConstantNode(value).toSet
          case EndsWithPredicateNode(value)             => getConfigParameterFromConstantNode(value).toSet
          case EqualsAsSetPredicateNode(_)              => Set.empty
          case InStringsMetricPredicateNode(values) => values.flatMap(getConfigParameterFromConstantNode).toSet
          case IsEmptyPredicateNode           => Set.empty
          case RegexMatchPredicateNode(_)     => Set.empty
          case StartsWithPredicateNode(value) => getConfigParameterFromConstantNode(value).toSet
        }
    })

  def hasMetrics(condition: ConditionNode[SimpleMetricConditionType.type]): Boolean =
    condition match {
      case NotNode(innerCond)                             => hasMetrics(innerCond)
      case AndNode(innerConds)                            => innerConds.exists(hasMetrics)
      case OrNode(innerConds)                             => innerConds.exists(hasMetrics)
      case TrueSimpleMetricConditionNode                  => false
      case FalseSimpleMetricConditionNode                 => false
      case _: BasicSimpleMetricConditionNode => true
    }

  def getAllReferencesFromAlertItems(alertItemsDescriptor: AlertItemsDescriptor, sectionName: String): Seq[String] = {
    Seq(alertItemsDescriptor.descriptionTemplate, alertItemsDescriptor.headlineTemplate).flatMap(template =>
      TemplateStringHelper.getKeysUsed(template).getOrElse(sectionName, Set.empty)
    )
  }
}

case class SnapshotDiffExpression(thisDev: Expression[Option[Any]], otherDev: Expression[Option[Any]], alertItem: ScopableExpression[String])
    extends ScopableExpression[String] {

  override def evalWithScope(time: Long, scope: Scope): String = {
    val thisValue = thisDev.eval(time)
    val otherValue = otherDev.eval(time)
    // TODO What happens if at least one value is `None`? (a runtime exception will be thrown)
    (thisValue, otherValue) match {
      case (Some(thisArray: MultiDimension), Some(otherArray: MultiDimension)) =>
        val thisNotInOther = thisArray.filterNot(otherArray.toSet)
        val otherNotInThis = otherArray.filterNot(thisArray.toSet)
        val second_header = if (thisNotInOther.nonEmpty) alertItem.eval(time) else ""
        (if (thisNotInOther.nonEmpty)
          s"does not have:\n" + diffListToString(thisNotInOther) + "\n\n"
        else "") +
          (if (otherNotInThis.nonEmpty)
            s"\n$second_header has and this device does not:\n" + diffListToString(otherNotInThis)
          else "")
      case (Some(thisSingle: SingleDimension), Some(otherSingle: SingleDimension)) =>
        val thisVal = thisSingle.getOrElse("value", "")
        val otherVal = otherSingle.getOrElse("value", "")
        s"is set to: $otherVal while this device is set to $thisVal."
    }
  }

  /**
    * @return The argument expressions.
    */
  override def args: Set[Expression[_]] = Set(thisDev, otherDev)

  def diffListToString(diffList: MultiDimension): String = {
    if (diffList.length == 1 && diffList.head.contains("value")) diffList.head.getOrElse("value", "")
    else diffList.map(_.toList.sortBy(_._1).map(x => s"${x._1}: ${x._2}").mkString(", ")).mkString("\n")
  }
}

case class ConditionalRemediationSteps(vendorOsSwitch: SwitchCaseExpression[_, String], vendorSwitch: SwitchCaseExpression[_, String], baseText: String)
    extends Expression[String] {

  override def eval(time: Long): String =
    vendorOsSwitch.eval(time) match {
      case Some(text) => baseText + " " + text
      case None       =>  vendorSwitch.eval(time) match {
        case Some(text) => baseText + " " + text
        case None       => baseText
      }
    }


  override def args: Set[Expression[_]] = Set(vendorSwitch, vendorOsSwitch)
}

object ConditionalRemediationSteps {
  case class ConditionalRemediationStepKey(vendor: String , os:Option[String] = None)

  def apply(baseText: String, vendorToText: (RemediationStepCondition, String)*): ConditionalRemediationSteps = {
    val vendorWithOsToTextMap = vendorToText.map {
      case (vendor, remediationInfo) => ConditionalRemediationStepKey(vendor.getVendor, Option(vendor.getOsName)) -> remediationInfo
    }.toMap
    apply(baseText, vendorWithOsToTextMap)
  }

  def apply(baseText: String, vendorWithOsToText: Map[ConditionalRemediationStepKey, String]): ConditionalRemediationSteps = {
     def evalVendor(time: Long, scope: Scope, includeOs: Boolean): ConditionalRemediationStepKey = {
      scope
        .getInvisible(Vendor)
        .map {
          case vendor: String =>
            if (includeOs) {
              val os = scope.getInvisible(OsKey).map { case o: String => o }
              ConditionalRemediationStepKey(vendor, os)
            } else {
              ConditionalRemediationStepKey(vendor)
            }
          case other => throw new EvaluationException(s"Vendor is not a string, but: $other")
        }
        .getOrElse(ConditionalRemediationStepKey(RemediationStepCondition.VENDOR_OTHER.getVendor))
    }

    val vendorWithOsExpression = new ScopableExpression[ConditionalRemediationStepKey] {

      override protected def evalWithScope(time: Long, scope: Scope): ConditionalRemediationStepKey = evalVendor(time, scope, true)

      override def args: Set[Expression[_]] = Set()
    }

    val vendorExpression = new ScopableExpression[ConditionalRemediationStepKey] {

      override protected def evalWithScope(time: Long, scope: Scope): ConditionalRemediationStepKey = evalVendor(time, scope, false)

      override def args: Set[Expression[_]] = Set()
    }

    val switchVendorOsCase = SwitchCaseExpression(vendorWithOsExpression, vendorWithOsToText)
    val switchVendorCase = SwitchCaseExpression(vendorExpression, vendorWithOsToText)
    ConditionalRemediationSteps(switchVendorOsCase, switchVendorCase, baseText)
  }
}

case class CollectionPrinter[A](collection: Expression[GenTraversable[A]], sep: String) extends Expression[String] {

  /**
    * @param time The time of the evaluation (in milliseconds).
    * @return The result of the evaluation.
    * @throws EvaluationException if an error occurs during the evaluation.
    */
  override def eval(time: Long): String = collection.eval(time).map(_.toString.trim).mkString(sep)

  /**
    * @return The argument expressions.
    */
  override def args: Set[Expression[_]] = Set(collection)
}

case class Collection2DPrinter[A](collection: Expression[GenTraversable[GenTraversable[A]]],
                                  sepLine: String,
                                  sepInLine: String)
    extends Expression[String] {

  /**
    * @param time The time of the evaluation (in milliseconds).
    * @return The result of the evaluation.
    * @throws EvaluationException if an error occurs during the evaluation.
    */
  override def eval(time: Long): String =
    collection
      .eval(time)
      .map(
        line => line.mkString(sepInLine)
      )
      .mkString(sepLine)

  /**
    * @return The argument expressions.
    */
  override def args: Set[Expression[_]] = Set(collection)
}
