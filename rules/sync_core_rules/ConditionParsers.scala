package com.indeni.server.rules.library

import java.util.regex.PatternSyntaxException

import scala.language.postfixOps
import scala.util.Try
import scala.util.parsing.combinator.RegexParsers

sealed trait Token
case class IDENTIFIER(s: String) extends Token
case class STRING_LITERAL(str: String) extends Token
case class DOUBLE_LITERAL(double: Double) extends Token
case class INT_LITERAL(double: Double) extends Token
case object TAG extends Token
case object LEFT_PAREN extends Token
case object RIGHT_PAREN extends Token
case object LEFT_BRACKET extends Token
case object RIGHT_BRACKET extends Token
case object LEFT_BRACE extends Token
case object RIGHT_BRACE extends Token
case object COMMA extends Token
case object DOLLAR extends Token
case object COLON extends Token
case object FAT_ARROW extends Token
case object EQUALS extends Token
case object NOT_EQUALS extends Token
case object LESS_THAN extends Token
case object LESS_THAN_OR_EQUALS_TO extends Token
case object GREATER_THAN extends Token
case object GREATER_THAN_OR_EQUALS_TO extends Token
case object OR extends Token
case object AND extends Token
case object NOT extends Token
case object IS_EMPTY extends Token
case object IN extends Token
case object STR_METRIC extends Token
case object OBJ_ARRAY_METRIC extends Token
case object DOUBLE_METRIC extends Token
case object CONTAINS extends Token
case object MATCHES_REGEX extends Token
case object STARTS_WITH extends Token
case object ENDS_WITH extends Token
case object CONFIG extends Token
case object EQUAL_AS_SETS extends Token
case object AT_KEY extends Token
case object ANY extends Token
case object ALL extends Token
case object AT_LEAST_K extends Token
case object PERCENT extends Token

// TODO: Consider separating into lexing and parsing. Remember to keep source position if you do.

sealed trait MetricType
case object DoubleMetricType extends MetricType
case object StringMetricType extends MetricType
case object ObjArrayMetricType extends MetricType

sealed trait ASTNode
sealed trait ConditionType

/*
 Some notes on the AST classes.
 All classes used in AST should derive from ASTNode
 There could be some confusion between the terms Simple/Atomic/Basic, so let's explain them.
 There are three condition types:
   TagConditionType for device tag conditions
       e.g. Tag(vendor) == v1
   SimpleMetricConditionType for metrics conditions without aggregations, used e.g. in the template-based yaml rules
       e.g. DoubleMetric(disk-usage) > 1000
   FullMetricConditionType for full metrics conditions used in core yaml rules
       e.g. any[disk_id](DoubleMetric(disk-usage) > 1000
 For each condition type T, a Condition[T] is either an AtomicConditionNode[T] or a recursive And/Or/Not combination of
 those.
 An AtomicConditionNode[T] is either a TrueTConditionNode, a FalseTConditionNode, or a BasicTConditionNode
 A BasicTConditionNode is a concrete case class composed of the needed AST components to describe the basic building block
 of that condition type.
 */

case object TagConditionType extends ConditionType
case object SimpleMetricConditionType extends ConditionType
case object FullMetricConditionType extends ConditionType
sealed trait ConditionNode[T <: ConditionType] extends ASTNode

case class NotNode[T <: ConditionType](innerNode: ConditionNode[T]) extends ConditionNode[T]
case class AndNode[T <: ConditionType](innerNodes: Seq[ConditionNode[T]]) extends ConditionNode[T]
case class OrNode[T <: ConditionType](innerNodes: Seq[ConditionNode[T]]) extends ConditionNode[T]
sealed trait AtomicConditionNode[T <: ConditionType] extends ConditionNode[T]

case class TagNode(tagName: String) extends ASTNode
case object TrueTagConditionNode extends AtomicConditionNode[TagConditionType.type]
case object FalseTagConditionNode extends AtomicConditionNode[TagConditionType.type]
case class BasicTagConditionNode(tagNode: TagNode, tagPredicateNode: TagPredicateNode) extends AtomicConditionNode[TagConditionType.type]
trait TagPredicateNode extends ASTNode

case class EqualsStringTagPredicateNode(value: String) extends TagPredicateNode
case class NotEqualsStringTagPredicateNode(value: String) extends TagPredicateNode
// TODO: Consider converting True/False conditions into generic classes

case class MetricNode(metricName: String, metricType: MetricType) extends ASTNode
case object TrueSimpleMetricConditionNode extends AtomicConditionNode[SimpleMetricConditionType.type]
case object FalseSimpleMetricConditionNode extends AtomicConditionNode[SimpleMetricConditionType.type]
case object TrueFullMetricConditionNode extends AtomicConditionNode[FullMetricConditionType.type]
case object FalseFullMetricConditionNode extends AtomicConditionNode[FullMetricConditionType.type]
case class BasicSimpleMetricConditionNode(metricNode: MetricNode, metricPredicateNode: MetricPredicateNode) extends AtomicConditionNode[SimpleMetricConditionType.type]
trait MetricPredicateNode extends ASTNode
case class ComparisonPredicateNode(comparisonOperator: ComparisonOperatorNode, literal: ConstantNode) extends MetricPredicateNode
case class InStringsMetricPredicateNode(values: Seq[StringConstant]) extends MetricPredicateNode
case object IsEmptyPredicateNode extends MetricPredicateNode
// TODO: add support for other literals, not just string here
case class ContainsAtKeyPredicateNode(value: StringConstant, key: String) extends MetricPredicateNode
case class RegexMatchPredicateNode(regex: String) extends MetricPredicateNode
case class ContainsStringPredicateNode(value: StringConstant) extends MetricPredicateNode
case class StartsWithPredicateNode(value: StringConstant) extends MetricPredicateNode
case class EndsWithPredicateNode(value: StringConstant) extends MetricPredicateNode

// TODO: Do we want to allow a literal objArray here too?
case class EqualsAsSetPredicateNode(metric: MetricNode) extends MetricPredicateNode

case class ConditionParserConfig(maxRemainingTextSizeForErrorMsg: Int = 50)

sealed trait ComparisonOperatorNode
case object EqualsNode extends ComparisonOperatorNode
case object NotEqualsNode extends ComparisonOperatorNode
case object LessThanNode extends ComparisonOperatorNode
case object LessThanOrEqualToNode extends ComparisonOperatorNode
case object GreaterThanNode extends ComparisonOperatorNode
case object GreaterThanOrEqualToNode extends ComparisonOperatorNode

sealed trait ConstantNode extends ASTNode
sealed trait StringConstant extends ConstantNode
sealed trait DoubleConstant extends ConstantNode
trait ConfigParameterNode extends ConstantNode {
  def parameterName: String
}
case class StringLiteralNode(string: String) extends StringConstant
case class StringConfigurationParameterNode(override val parameterName: String) extends ConfigParameterNode with StringConstant
case class NumberLiteralNode(number: Double) extends DoubleConstant
case class DoubleConfigurationParameterNode(parameterName: String) extends ConfigParameterNode with DoubleConstant

/**
  * Untyped is used only in the interim stage between parsing and validation. Therefore it's private
  */
private case class UntypedConfigurationParameterNode(parameterName: String) extends ConfigParameterNode

case class BasicFullMetricConditionNode(tagsAggregation: TagsAggregation, simpleMetricCondition: ConditionNode[SimpleMetricConditionType.type]) extends AtomicConditionNode[FullMetricConditionType.type]
case class TagsAggregation(aggregationSpecifier: AggregationSpecifier, tagNames: Seq[String], alertItemsId: Option[String]) extends ASTNode
sealed trait AggregationSpecifier extends ASTNode
case object AnyAggregation extends AggregationSpecifier
case object AllAggregation extends AggregationSpecifier
case class AtLeastKAggregeation(k: Int) extends AggregationSpecifier
case class AtLeastKPercentAggregation(kPercent: Double) extends AggregationSpecifier


/*
 * Some helpful documentation of the scala parser combinator syntax we use:
 *
 * Simply and generally speaking, a parser for a part of the the language (Token/ASTNode) is composed of two parts:
 * The left-hand side is a description of what input we expect to parse which can be either a regex, a literal string, or a composition
 * of other parsers. The-right hand side controls what will be returned by this parser upon a successful parse. The
 * sides are separated either by ^^ or by ^^^.
 *
 * Note that in the current implementation there isn't a real separation of lexing and parsing, i.e. the basic building
 * block of the parser is still the single character, and not a whole token. This probably should change in the future to support
 * clearer error messages.
 * ^^: After successful parsing of the left-hand side, run the right-hand side function on the result of the parsing and its return value is the value of the parse
 * ^^^: After successful parsing of the left-hand side, just return the right-hand side as the value of the parse
 * ^?: After successful parsing of the left-hand side, if the given partial function (the first part on the right side) is applicable,
 *     evaluate it and that's the result of the parsing. If it isn't applicable, evaluate the second function (the second part on the right side)
 *     to get an error message.
 * ~: Concatenation. This works both for the parsing side and in the function in a match-case
 * ~!: Concatenation, with no backtrack. This helps the parser be certain that the decision it made is correct with relation to parsing this phrase.
 *     If whatever is on the left side of ~! has been parsed successfully, then the parser will not revert this decision even if the right-hand side
 *     fails to parse.
 * ~> / <~ : Concatenation, when we don't need parts of the input in the value-computing function. E.g.
 *           a ~> b ^^ function : upon successful parse of a followed by b, the function will only get b as a parameter
 *           a <~ b ^^ function : upon successful parse of a followed by b, the function will only get a as a pamaraeter
 *           a ~> b <~ c ^^ function: upon successful parse of a followed by b followed by c, the function will only get b as a parameter
 * ~>! : Combination of ~> and ~!, i.e. it denotes concatenation, when we both
 *       - Don't need the part of the input on the left-hand side of this operator,
 *       - Don't need backtracking after successful parsing of the left-hand side
 * *: Like in regex, denotes repetition 0 or more times. The value that is then passed to the right-hand side function is a List
 * |: Multiple options for parsing this part of the language
 *
 * Examples of TagsStoreConditions:
 * Tag(t1) == v1
 * ( (Tag(t1) == v1) or (Tag(t1) == v2) ) and not Tag(t2) == v3
 *
 * Examples of ConditionExpressions:
 * StrMetric(m1) == "v1" and not (ObjArrayMetric(m2) contains "v2" atKey "k1")
 */

case class ValidationException(msg: String) extends Exception(msg)

trait BaseConditionParser extends RegexParsers {
  protected def config: ConditionParserConfig

  protected def tagKeyword: Parser[TAG.type] = "Tag" ^^^ TAG
  protected def andKeyword: Parser[AND.type] = "and" ^^^ AND
  protected def orKeyword: Parser[OR.type] = "or" ^^^ OR
  protected def notKeyword: Parser[NOT.type] = "not" ^^^ NOT
  protected def isEmptyKeyword: Parser[IS_EMPTY.type] = "isEmpty" ^^^ IS_EMPTY
  protected def inKeyword: Parser[IN.type] = "in" ^^^ IN
  protected def anyKeyword: Parser[ANY.type] = "any" ^^^ ANY
  protected def allKeyword: Parser[ALL.type] = "all" ^^^ ALL
  protected def atLeastKKeyword: Parser[AT_LEAST_K.type] = "at-least-k" ^^^ AT_LEAST_K
  protected def percent: Parser[PERCENT.type] = "%" ^^^ PERCENT
  protected def strMetricKeyword: Parser[STR_METRIC.type] = "StrMetric" ^^^ STR_METRIC
  protected def objArrayMetricKeyword: Parser[OBJ_ARRAY_METRIC.type] = "ObjArrayMetric" ^^^ OBJ_ARRAY_METRIC
  protected def doubleMetricKeyword: Parser[DOUBLE_METRIC.type] = "DoubleMetric" ^^^ DOUBLE_METRIC
  protected def containsKeyword: Parser[CONTAINS.type] = "contains" ^^^ CONTAINS
  protected def matchesRegexKeyword: Parser[MATCHES_REGEX.type] = "matchesRegex" ^^^ MATCHES_REGEX
  protected def startsWithKeyword: Parser[STARTS_WITH.type] = "startsWith" ^^^ STARTS_WITH
  protected def endsWithKeyword: Parser[ENDS_WITH.type] = "endsWith" ^^^ ENDS_WITH
  protected def configKeyword: Parser[CONFIG.type] = "config" ^^^ CONFIG
  protected def equalsAsSetKeyword: Parser[EQUAL_AS_SETS.type] = "equalsAsSet" ^^^ EQUAL_AS_SETS
  protected def atKeyKeyword: Parser[AT_KEY.type] = "atKey" ^^^ AT_KEY
  protected def leftParen: Parser[LEFT_PAREN.type] = "(" ^^^ LEFT_PAREN
  protected def rightParen: Parser[RIGHT_PAREN.type] = ")" ^^^ RIGHT_PAREN
  protected def leftBracket: Parser[LEFT_BRACKET.type] = "[" ^^^ LEFT_BRACKET
  protected def rightBracket: Parser[RIGHT_BRACKET.type] = "]" ^^^ RIGHT_BRACKET
  protected def leftBrace: Parser[LEFT_BRACE.type] = "{" ^^^ LEFT_BRACE
  protected def rightBrace: Parser[RIGHT_BRACE.type] = "}" ^^^ RIGHT_BRACE
  protected def comma: Parser[COMMA.type] = "," ^^^ COMMA
  protected def dollar: Parser[DOLLAR.type] = "$" ^^^ DOLLAR
  protected def colon: Parser[COLON.type] = ":" ^^^ COLON
  protected def fatArrow: Parser[FAT_ARROW.type] = "=>" ^^^ FAT_ARROW
  protected def equalsSign: Parser[EQUALS.type] = "==" ^^^ EQUALS
  protected def notEqualsSign: Parser[NOT_EQUALS.type] = ("!="|"<>") ^^^ NOT_EQUALS
  protected def lessThanSign: Parser[LESS_THAN.type] = "<" ^^^ LESS_THAN
  protected def lessThanOrEqualToSign: Parser[LESS_THAN_OR_EQUALS_TO.type] = "<=" ^^^ LESS_THAN_OR_EQUALS_TO
  protected def greaterThanSign: Parser[GREATER_THAN.type] = ">" ^^^ GREATER_THAN
  protected def greaterThanOrEqualToSign: Parser[GREATER_THAN_OR_EQUALS_TO.type] = ">=" ^^^ GREATER_THAN_OR_EQUALS_TO
  protected def identifier: Parser[IDENTIFIER] =
    "[a-zA-Z_][a-zA-Z_0-9-]*".r ^^ { str => IDENTIFIER(str) } withFailureMessage "Expected valid identifier"
  // TODO: Support escaping quotes inside string literal
  protected def stringLiteral: Parser[STRING_LITERAL] =
    "\"" ~> "[^\"]*".r <~ "\"" ^^ { STRING_LITERAL }
  protected def doubleLiteral: Parser[DOUBLE_LITERAL] = """-?\d+(\.\d*)?""".r ^^ { doubleAsString => DOUBLE_LITERAL(doubleAsString.toDouble) } withFailureMessage "Expected number"
  protected def intLiteral: Parser[INT_LITERAL] = """-?\d+""".r ^^ { intAsString => INT_LITERAL(intAsString.toInt)} withFailureMessage "Expected integer"
  protected def textSnippet(pos: Int, remainingText: String): String = {
    val prefix = if (pos == 0) "\"" else "\"..."
    val suffix = if (prefix.length + remainingText.length <= config.maxRemainingTextSizeForErrorMsg - 1) "\"" else "...\""
    prefix + remainingText.substring(0, Math.min(remainingText.length,
      config.maxRemainingTextSizeForErrorMsg - prefix.length - suffix.length)) + suffix
  }

  protected def stringLiteralNode: Parser[StringLiteralNode] =
    stringLiteral ^^ { case STRING_LITERAL(str) => StringLiteralNode(str) } withFailureMessage "Expected string literal surrounded by quotes"

  case class ConditionDescriptor[T <: ConditionType](atomicCondition: Parser[ConditionNode[T]], descriptionOfExpected: String)

  private def singleCondition[T <: ConditionType](condDescriptor: ConditionDescriptor[T]): Parser[ConditionNode[T]] =
    condDescriptor.atomicCondition |
      leftParen ~> condition(condDescriptor) <~ rightParen
  def condition[T <: ConditionType](condDescriptor: ConditionDescriptor[T]): Parser[ConditionNode[T]] =
    possibleAndCondition(condDescriptor) ~! ((orKeyword ~> possibleAndCondition(condDescriptor))*) ^^ { case and ~ moreAnds => if (moreAnds.isEmpty) and else OrNode(and +: moreAnds) } withFailureMessage s"Expected ${condDescriptor.descriptionOfExpected}"
  private def possibleAndCondition[T <: ConditionType](condDescriptor: ConditionDescriptor[T]): Parser[ConditionNode[T]] =
    possibleNotCondition(condDescriptor) ~! ((andKeyword ~> possibleNotCondition(condDescriptor))*) ^^ { case or ~ moreOrs => if (moreOrs.isEmpty) or else AndNode(or +: moreOrs) }
  private def possibleNotCondition[T <: ConditionType](condDescriptor: ConditionDescriptor[T]): Parser[ConditionNode[T]] =
    singleCondition(condDescriptor) |
      notKeyword ~> singleCondition(condDescriptor) ^^ { NotNode(_) }

  /**
    * Validates the parsed condition and also performs additional processing.
    * At the moment, the only processing done is "deciding" for Untyped configuration parameter
    * references which type they should be, but in the future we could add other functionalites
    * to this method.
    */
  @throws[ValidationException]
  protected def validateAndProcess[T <: ConditionType](condNode: ConditionNode[T], atomicValidationFunction: AtomicConditionNode[T] => AtomicConditionNode[T]): ConditionNode[T] = {
    condNode match {
      case OrNode(innerNodes)  => OrNode(innerNodes.map(validateAndProcess(_, atomicValidationFunction)))
      case AndNode(innerNodes) => AndNode(innerNodes.map(validateAndProcess(_, atomicValidationFunction)))
      case NotNode(innerNode)  => NotNode(validateAndProcess(innerNode, atomicValidationFunction))
      case x : AtomicConditionNode[T] => atomicValidationFunction(x)
    }
  }
}

sealed trait ConditionParser[T <: ConditionType] extends BaseConditionParser {
  type CondNode = ConditionNode[T]
  protected val conditionDescriptor: ConditionDescriptor[T]

  @throws[ValidationException]
  protected def validateAndProcessAtomicCondition(atomicConditionNode: AtomicConditionNode[T]): AtomicConditionNode[T]

  def parseString(str: String): Either[String, CondNode] = {
    parseAll(condition(conditionDescriptor), str) match {
      case Success(result, _) => {
        try {
          Right(validateAndProcess(result, validateAndProcessAtomicCondition))
        } catch {
          case e : ValidationException => Left(e.msg)
        }
      }
      case NoSuccess(msg, next) => {
        val pos = next.offset
        val rest = next.rest.source.toString.substring(pos)
        Left(s"Error parsing at position $pos (${ textSnippet(pos, rest)}): $msg")
      }
    }
  }
}

class TagsConditionParser(protected val config: ConditionParserConfig) extends ConditionParser[TagConditionType.type] {
  private def basicCondition: Parser[ConditionNode[TagConditionType.type]] =
    tag ~! predicate ^^ { case tag ~ predicate => BasicTagConditionNode(TagNode(tag.tagName), predicate) }
  private def tag: Parser[TagNode] = tagKeyword ~> leftParen ~> identifier <~ rightParen ^^ (identifier => TagNode(identifier.s)) withFailureMessage "Expected Tag"
  private def predicate: Parser[TagPredicateNode] =
    equalsSign ~! stringLiteralNode ^^ { case _ ~ literal => EqualsStringTagPredicateNode(literal.string).asInstanceOf[TagPredicateNode] } |
    notEqualsSign ~! stringLiteralNode ^^ { case _ ~ literal => NotEqualsStringTagPredicateNode(literal.string) }

  @throws[ValidationException]
  override def validateAndProcessAtomicCondition(atomicConditionNode: AtomicConditionNode[TagConditionType.type]): AtomicConditionNode[TagConditionType.type] = {
    atomicConditionNode match {
      case basicTagConditionNode @ BasicTagConditionNode(tagNode, tagPredicateNode) => {
        basicTagConditionNode
      }
      case FalseTagConditionNode => FalseTagConditionNode
      case TrueTagConditionNode => TrueTagConditionNode
    }
  }
  private val conditionDescription: String = "Device tags condition, e.g. Tag(vendor) == \"checkpoint\""
  override protected val conditionDescriptor = ConditionDescriptor(basicCondition, conditionDescription)
}

trait BaseMetricsConditionParser extends BaseConditionParser {
  protected def metric: Parser[MetricNode] =
    metricType ~! (leftParen ~> identifier <~ rightParen) ^^ { case metricType ~ metricName => MetricNode(metricName.s, metricType) }
  protected def metricType: Parser[MetricType] =
      strMetricKeyword ^^^ StringMetricType |
      doubleMetricKeyword ^^^ DoubleMetricType |
      objArrayMetricKeyword ^^^ ObjArrayMetricType

  protected def predicate: Parser[MetricPredicateNode] =
      comparisonOperator ~! constant ^^ {case compOp ~ comparedToValue => ComparisonPredicateNode(compOp, comparedToValue).asInstanceOf[MetricPredicateNode]} |
      inKeyword ~! leftBracket ~> stringConstant ~ ((comma ~> stringConstant)*) <~ rightBracket ^^ {case constant ~ moreConstants => InStringsMetricPredicateNode(constant +: moreConstants) } |
      isEmptyKeyword ^^^ IsEmptyPredicateNode |
      (containsKeyword ~> stringConstant) ~! ((atKeyKeyword ~> stringLiteralNode)?) ^^ {
        case value ~ Some(key) => ContainsAtKeyPredicateNode(value, key.string)
        case value ~ None => ContainsStringPredicateNode(value) } |
      startsWithKeyword ~> stringConstant ^^ ( constant => StartsWithPredicateNode(constant) ) |
      endsWithKeyword ~> stringConstant ^^ ( constant => EndsWithPredicateNode(constant) ) |
      matchesRegexKeyword ~> stringLiteralNode ^^ ( literal => RegexMatchPredicateNode(literal.string) ) |
      equalsAsSetKeyword ~> metric ^^ { metricNode => EqualsAsSetPredicateNode(metricNode) }

  protected def constant: Parser[ConstantNode]
  // Next two are defined differently in SimpleMetricsConditionParser/FullMetricsConditionParser.
  // Note that the simple conditions that are part of a full condition will use the definitions in FullMetricsConditionParser
  protected def stringConstant: Parser[StringConstant]
  protected def doubleConstant: Parser[DoubleConstant]
  protected def stringConfigParameter: Parser[StringConfigurationParameterNode] =
    configParameter ^^ { configParameterName => StringConfigurationParameterNode(configParameterName.s)}
  protected def doubleConfigParameter: Parser[DoubleConfigurationParameterNode] =
    configParameter ^^ { configParameterName => DoubleConfigurationParameterNode(configParameterName.s)}
  protected def untypedConfigParameter: Parser[ConfigParameterNode] =
    configParameter ^^ { configParameterName => UntypedConfigurationParameterNode(configParameterName.s) }
  protected def configParameter: Parser[IDENTIFIER] =
    (dollar ~ leftBrace) ~>! configKeyword ~> colon ~> identifier <~ rightBrace

  // TODO: Rename these if tokenization and parsing are in separate classes
  protected def numberLiteralNode: Parser[NumberLiteralNode] =
    doubleLiteral ^^ {case DOUBLE_LITERAL(double) => NumberLiteralNode(double)}
  protected def comparisonOperator: Parser[ComparisonOperatorNode] =
      equalsSign ^^^ EqualsNode |
      notEqualsSign ^^^ NotEqualsNode |
      lessThanSign ^^^ LessThanNode |
      lessThanOrEqualToSign ^^^ LessThanOrEqualToNode |
      greaterThanSign ^^^ GreaterThanNode |
      greaterThanOrEqualToSign ^^^ GreaterThanOrEqualToNode

  protected def validateRegexMatch(metricType: MetricType, regex: String): Unit = {
    if (metricType != StringMetricType)
      throw ValidationException(s"'matchesRegex' conditions are only allowed for ObjArrayMetrics; got: $metricType")
    Try(regex.r) match {
      case scala.util.Failure(e) => {
        val errMsg = e match {
          case exception: PatternSyntaxException => exception.getDescription
          case _                                 => e.getMessage
        }
        throw ValidationException(s"Invalid regular expression '$regex'. Error was: $errMsg")
      }
      case scala.util.Success(_) =>
    }
  }

  protected def validateAndProcessComparisonPredicate(comparisonPredicateNode: ComparisonPredicateNode, metricType: MetricType): ComparisonPredicateNode = comparisonPredicateNode match {
    case ComparisonPredicateNode(comparisonOperator, constantNode) =>
      (constantNode, metricType) match {
        case (_: StringConstant, StringMetricType) =>
          if (!Seq(EqualsNode, NotEqualsNode).contains(comparisonOperator))
            throw ValidationException(s"Only equals and notEquals are supported for string metric comparisons")
          else
            comparisonPredicateNode
        case (UntypedConfigurationParameterNode(parameterName), StringMetricType) =>
          ComparisonPredicateNode(comparisonOperator, StringConfigurationParameterNode(parameterName))
        case (_: DoubleConstant, DoubleMetricType)                    => comparisonPredicateNode
        case (UntypedConfigurationParameterNode(parameterName), DoubleMetricType) =>
          ComparisonPredicateNode(comparisonOperator, DoubleConfigurationParameterNode(parameterName))
        case (_, ObjArrayMetricType) =>
          throw ValidationException(s"Direct comparison of ObjArrayMetrics is not currently supported")
        case _ =>
          throw ValidationException(
            s"Incompatible types in comparison condition. Metric type is $metricType and constant is ${constantNode.getClass.getName}")
      }
  }


  // Defined here because needed both by the SimpleMetricsConditionParser parser and the FullMetricsConditionParser
  private def basicSimpleCondition: Parser[ConditionNode[SimpleMetricConditionType.type]] =
    metric ~! predicate ^^ {case metric ~ predicate => BasicSimpleMetricConditionNode(metric, predicate)}
  private val basicConditionDescription: String = "Metrics condition, e.g. StrMetric(cpu-monitor-enabled) == \"true\""
  protected val basicConditionDescriptor = ConditionDescriptor(basicSimpleCondition, basicConditionDescription)
  def validateAndProcessAtomicSimpleCondition(atomicSimpleCondition: AtomicConditionNode[SimpleMetricConditionType.type]): AtomicConditionNode[SimpleMetricConditionType.type] =
    atomicSimpleCondition match {
      case BasicSimpleMetricConditionNode(metricNode, metricPredicateNode) => {
        val metricType = metricNode.metricType
        val newMetricPredicateNode: MetricPredicateNode = metricPredicateNode match {
          case comparisonPredicateNode: ComparisonPredicateNode =>
            validateAndProcessComparisonPredicate(comparisonPredicateNode, metricType)
          case _ : InStringsMetricPredicateNode =>
            if (metricType != StringMetricType)
              throw ValidationException(s"'in' conditions are only allowed for StrMetrics; got: $metricType")
            else
              metricPredicateNode
          case IsEmptyPredicateNode =>
            if (metricType != ObjArrayMetricType)
              throw ValidationException(s"'isEmpty' conditions are only allowed for ObjArrayMetrics; got: $metricType")
            else
              metricPredicateNode
          case _ : ContainsAtKeyPredicateNode =>
            if (metricType != ObjArrayMetricType)
              throw ValidationException(s"'containsAtKey' conditions are only allowed for ObjArrayMetrics; got: $metricType")
            metricPredicateNode
          case RegexMatchPredicateNode(regex) => {
            validateRegexMatch(metricType, regex)
            metricPredicateNode
          }
          case ContainsStringPredicateNode(str) =>
            if (metricType != StringMetricType)
              throw ValidationException(s"'contains' conditions are only allowed for StringMetrics; got: $metricType")
            else
              metricPredicateNode
          case StartsWithPredicateNode(str) =>
            if (metricType != StringMetricType)
              throw ValidationException(s"'startsWith' conditions are only allowed for StringMetrics; got: $metricType")
            else
              metricPredicateNode
          case EndsWithPredicateNode(str) =>
            if (metricType != StringMetricType)
              throw ValidationException(s"'endsWith' conditions are only allowed for StringMetrics; got: $metricType")
            else
              metricPredicateNode
          case EqualsAsSetPredicateNode(otherMetric) =>
            if (metricType != ObjArrayMetricType || otherMetric.metricType != ObjArrayMetricType)
              throw ValidationException(s"'equalsAsSet' conditions are only allowed for comparing ObjArrayMetrics against other ObjArrayMetrics; got: ($metricType, ${otherMetric.metricType})")
            else
              metricPredicateNode
        }
        // Add more validations here if/when necessary
        BasicSimpleMetricConditionNode(metricNode, newMetricPredicateNode)
      }
      case FalseSimpleMetricConditionNode => FalseSimpleMetricConditionNode
      case TrueSimpleMetricConditionNode => TrueSimpleMetricConditionNode
    }
}


/**
  * Used for the complex_condition field in template-based YAML rules
  */
class SimpleMetricsConditionParser(val config: ConditionParserConfig) extends ConditionParser[SimpleMetricConditionType.type] with BaseMetricsConditionParser {
  override protected def stringConstant: Parser[StringConstant] =
    stringLiteralNode
  override protected def doubleConstant: Parser[DoubleConstant] =
    numberLiteralNode
  override protected def constant: Parser[ConstantNode] =
    (stringLiteralNode | numberLiteralNode) withFailureMessage "Expected either a string literal surrounded by quotes or a number literal"

  @throws[ValidationException]
  override def validateAndProcessAtomicCondition(atomicConditionNode: AtomicConditionNode[SimpleMetricConditionType.type]): AtomicConditionNode[SimpleMetricConditionType.type] =
    validateAndProcessAtomicSimpleCondition(atomicConditionNode)
  override protected val conditionDescriptor: ConditionDescriptor[SimpleMetricConditionType.type] =
    basicConditionDescriptor
}


/**
  * Used for the metrics_condition field in template-based YAML rules
  */
class FullMetricsConditionParser(val config: ConditionParserConfig) extends ConditionParser[FullMetricConditionType.type] with BaseMetricsConditionParser {
  override protected def stringConstant: Parser[StringConstant] =
    stringLiteralNode | stringConfigParameter
  override protected def doubleConstant: Parser[DoubleConstant] =
    numberLiteralNode | doubleConfigParameter
  override protected def constant: Parser[ConstantNode] =
    (stringLiteralNode | numberLiteralNode | untypedConfigParameter) withFailureMessage "Expected either a string literal surrounded by quotes, a number literal, or a ${config:parameterName}-reference to a configuration parameters"
  private def basicCondition: Parser[ConditionNode[FullMetricConditionType.type]] =
    tagsAggregation ~! (leftParen ~> condition(basicConditionDescriptor) <~ rightParen) ^^ {
      case tagsAggregation ~ simpleCondition => BasicFullMetricConditionNode(tagsAggregation, simpleCondition) }

  private def aggregationSpecifier: Parser[AggregationSpecifier] =
    anyKeyword ^^^ AnyAggregation |
    allKeyword ^^^ AllAggregation |
    atLeastKKeyword ~>! atLeastKSpecifier

  private def atLeastKSpecifier: Parser[AggregationSpecifier] =
    leftBrace ~>! doubleLiteral <~ rightBrace ^? (
      { case DOUBLE_LITERAL(double) if double.isValidInt => AtLeastKAggregeation(double.toInt).asInstanceOf[AggregationSpecifier] },
      double_literal => s"Expected integer, got: ${double_literal.double}")  |
      (percent ~>! atLeastKPercentSpecifier withFailureMessage "Expected either at-least-k%{number} or at-least-k{integer}")

  private def atLeastKPercentSpecifier: Parser[AggregationSpecifier] =
    leftBrace ~> doubleLiteral <~ rightBrace ^^ {
      case DOUBLE_LITERAL(double) => AtLeastKPercentAggregation(double)
    }

  private def tagsList: Parser[Seq[String]] = (identifier?) ~ ((comma ~> identifier)*) ^^ {
    case identifierOption ~ moreIdentifiers => (identifierOption.toSeq ++ moreIdentifiers).map(_.s)}

  protected def tagsAggregation: Parser[TagsAggregation] =
    (aggregationSpecifier ~! (leftBracket ~> tagsList)) ~! ((fatArrow ~>! identifier)?) <~! rightBracket ^^ {
      case aggregationSpec ~ tagsList ~ identifierOption => TagsAggregation(aggregationSpec, tagsList, identifierOption.map(_.s)) }

  override protected def validateAndProcessAtomicCondition(atomicConditionNode: AtomicConditionNode[FullMetricConditionType.type]): AtomicConditionNode[FullMetricConditionType.type] =
    atomicConditionNode match {
      // TODO: Add more validations as needed
      case BasicFullMetricConditionNode(tagsAggregation, simpleMetricCondition) => {
        val newSimpleCondition = validateAndProcess(simpleMetricCondition, validateAndProcessAtomicSimpleCondition)

        if (tagsAggregation.tagNames.isEmpty) {
          if (tagsAggregation.alertItemsId.isDefined)
            throw ValidationException("Cannot have an alert items id without aggregation tags in the brackets")
          if (tagsAggregation.aggregationSpecifier != AnyAggregation)
            throw ValidationException("Only the 'any' aggregation is allowed with an empty tags list")
        }
        if (!RuleHelper.hasMetrics(simpleMetricCondition) && (tagsAggregation.aggregationSpecifier != AnyAggregation || tagsAggregation.tagNames.nonEmpty))
          throw ValidationException("If a condition does not refer to any metrics, it can only be in an empty any[] clause without tags or alert items")

        tagsAggregation.aggregationSpecifier match {
          case AnyAggregation =>
          case AllAggregation =>
          case AtLeastKAggregeation(k) => {
            if (k <= 0) {
              throw ValidationException(s"The value k in at-least-k should be positive. Got: $k")
            }
          }
          case AtLeastKPercentAggregation(kPercent) => {
            if (kPercent <= 0 || kPercent >= 100) {
              throw ValidationException(s"The percentage value k in at-least-k% should be strictly greater than 0 and strictly less than 100. Got: $kPercent")
            }
          }
        }
        BasicFullMetricConditionNode(tagsAggregation, newSimpleCondition)
      }
      case FalseFullMetricConditionNode => FalseFullMetricConditionNode
      case TrueFullMetricConditionNode => TrueFullMetricConditionNode
    }

  private val conditionDescription: String = "Aggregated metrics condition, e.g. any[cpu-id => id1](DoubleMetric(cpu-usage) > 70.0)"
  override protected val conditionDescriptor = ConditionDescriptor(basicCondition, conditionDescription)
}
