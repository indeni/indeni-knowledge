package com.indeni.server.rules.library.checkpoint

import com.indeni.ruleengine.expressions.conditions.GreaterThan
import com.indeni.ruleengine.expressions.core._
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.server.common.data.conditions.Equals
import com.indeni.server.rules._
import com.indeni.server.rules.library.{ConditionalRemediationSteps, PerDeviceRule, RuleHelper}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class ClusterXLTooManySyncNicsNoVsxRule() extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata.builder("clusterxl_too_many_sync_nics_novsx", "Firewall more than one sync interface defined",
    "ClusterXL works best with just one sync interface. To achieve redundancy, interface bonding is recommended. indeni will alert when more than one sync interface is used.", AlertSeverity.ERROR, categories = Set(RuleCategory.VendorBestPractices),deviceCategory = DeviceCategory.CheckPointClusterXLNonVSX).build()

  override def expressionTree(context: RuleContext): StatusTreeExpression = {
    val inUseValue = TimeSeriesExpression[Double]("cphaprob-required-secured-interfaces").last

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), !Equals("vsx", "true") & !Equals("cloudguard-gateway", "azure")),

      // What constitutes an issue
        StatusTreeExpression(
          // The time-series we check the test condition against:
          SelectTimeSeriesExpression[Double](context.tsDao, Set("cphaprob-required-secured-interfaces"), denseOnly = false),

          // The condition which, if true, we have an issue. Checked against the time-series we've collected
          GreaterThan(
            inUseValue,
            ConstantExpression(Some(1.0)))
        ).withoutInfo().asCondition()

      // Details of the alert itself
    ).withRootInfo(
        getHeadline(),
        ConstantExpression("SK92804 and Check Point's best practices recommend you only have one sync interface. To achieve redundancy, a bond interface should be used."),
        ConditionalRemediationSteps("Read Valeri Loukine's post on the subject: http://checkpoint-master-architect.blogspot.com/2014/05/notes-about-sync-redundancy.html.")
    )
  }
}
