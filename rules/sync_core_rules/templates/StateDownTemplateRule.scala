package com.indeni.server.rules.library.templates

import com.indeni.apidata.time.TimeSpan
import com.indeni.ruleengine.Scope.{Scope, ImplicitScopeValueHelper}
import com.indeni.ruleengine.expressions.conditions.{And, _}
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data._
import com.indeni.ruleengine.expressions.regex.RegexExpression
import com.indeni.ruleengine.expressions.scope.{ScopableExpression, ScopeValueExpression}
import com.indeni.ruleengine.expressions.{Expression, conditions}
import com.indeni.ruleengine.utility.LastNNonEmptyValues
import com.indeni.server.common.ParameterValue
import com.indeni.server.common.data.conditions.{TagsStoreCondition, True}
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules._
import com.indeni.server.rules.config.expressions.DynamicParameterExpression
import com.indeni.server.rules.library.yamlrules.YamlRuleProcessorUtils._
import com.indeni.server.rules.library._
import com.indeni.server.rules.library.templates.StateDownTemplateRule._
import com.indeni.server.rules.library.yamlrules.{YamlRuleParseException, YamlRuleProcessorUtils}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import net.jcazevedo.moultingyaml.YamlObject

import scala.util.matching.Regex

class StateDownTemplateRule(ruleName: String,
                            ruleFriendlyName: String,
                            ruleDescription: String,
                            severity: AlertSeverity = DefaultSeverity,
                            metricName: String,
                            applicableMetricTag: String = DefaultApplicableMetricTag,
                            alertItemsHeader: String = DefaultAlertItemsHeader,
                            descriptionMetricTag: String = DefaultDescriptionMetricTag,
                            descriptionStringFormat: String = DefaultDescriptionStringFormat,
                            alertDescription: String,
                            baseRemediationText: String,
                            metaCondition: TagsStoreCondition = DefaultMetaCondition,
                            alertIfDown: Boolean = DefaultAlertIfDown,
                            itemSpecificDescription: Seq[(Regex, String)] = DefaultItemSpecificDescription,
                            itemsToIgnore: Set[Regex] = DefaultItemsToIgnore,
                            stateDescriptionComplexMetricName: String = DefaultStateDescriptionComplexMetricName,
                            historyLength: Int = DefaultHistoryLength,
                            generateStateDownCondition: (Int, TimeSeriesExpression[Double], ConstantExpression[Double]) => Condition = DefaultGenerateStateDownCondition,
                            ruleCategories:Set[RuleCategory] = DefaultRuleCategories,
                            deviceCategory: DeviceCategory = DefaultDeviceCategory)
                           (vendorToRemediationText: (RemediationStepCondition, String)*)
  extends PerDeviceRule with RuleHelper {

  private val minimumNumberOfIssueItemsParameterName = "min_num_of_issue_items"
  private val minimumNumberOfIssueItemsParameter = new ParameterDefinition(
    minimumNumberOfIssueItemsParameterName,
    "",
    "Minimum Number of Issue Items (0=All)",
    "The minimum number of issue items before an issue is triggered.",
    UIType.INTEGER,
    new ParameterValue(1.asInstanceOf[Object])
  )

  private val configParameters =
    if (applicableMetricTag != null)
      Seq(minimumNumberOfIssueItemsParameter)
    else
      Seq()

  override val metadata: RuleMetadata = RuleMetadata.builder(ruleName, ruleFriendlyName, ruleDescription, severity, ruleCategories,deviceCategory = deviceCategory)
    .configParameters(configParameters: _*).build()

  override def expressionTree(context: RuleContext): StatusTreeExpression = {

    val complexStateDescription = stateDescriptionComplexMetricName match {
      case snapshotName: String => new Expression[String] {

        private val selectSnapshot = ForExpression(
          SelectSnapshotsExpression(context.snapshotsDao, Set(snapshotName)).single(),
          SnapshotExpression(snapshotName).asSingle().mostRecent().value())

        override def eval(time: Long): String = {

          selectSnapshot.eval(time).values.headOption.flatMap(_.get("value")).getOrElse("")
        }

        override def args: Set[Expression[_]] = Set(selectSnapshot)
      }
      case _ => ConstantExpression("")
  }


    val currentValue = TimeSeriesExpression[Double](metricName).last
    val tsToTestAgainst = TimeSeriesExpression[Double](metricName)
    val stateToLookFor = ConstantExpression(if (alertIfDown) 0.0 else 1.0)

    val condition = generateStateDownCondition(historyLength, tsToTestAgainst, stateToLookFor)

    // If we need more than one repetition, we need more than the default history
    val history = if (historyLength == 1) SelectTimeSeriesExpression.DEFAULT_HISTORY_LENGTH else ConstantExpression(TimeSpan.fromHours(24))

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), metaCondition),

      // What constitutes an issue
      And(
      if (applicableMetricTag != null)
        StatusTreeExpression(

          // The additional tags we care about (we'll be including this in alert data)
          SelectTagsExpression(context.tsDao, if (descriptionMetricTag == null) Set(applicableMetricTag) else Set(applicableMetricTag, descriptionMetricTag), withTagsCondition(metricName)),

          StatusTreeExpression(
            // The time-series we check the test condition against:
            SelectTimeSeriesExpression[Double](context.tsDao, Set(metricName), historyLength = history, denseOnly = false),

            // The condition which, if true, we have an issue. Checked against the time-series we've collected
            And(
              condition,
              Or(
                itemsToIgnore.map(r => ResultsFound(RegexExpression(ScopeValueExpression(applicableMetricTag).visible().asString(), r))).toSeq
              ).not)

            // The Alert Item to add for this specific item
          ).withSecondaryInfo(
            scopableStringFormatExpression("${scope(\"" + applicableMetricTag + "\")}" + (if (descriptionMetricTag != null) " (${scope(\"" + descriptionMetricTag + "\")})" else "")),
            new ScopableExpression[String] {
              val innerDescription = scopableStringFormatExpression(descriptionStringFormat, currentValue)

              override protected def evalWithScope(time: Long, scope: Scope): String = {
                val metricTagValue = scope.getVisible(applicableMetricTag).get.toString
                val descriptionTagValue = if (descriptionMetricTag != null) scope.getVisible(descriptionMetricTag).get.toString else ""

                innerDescription.eval(time) + itemSpecificDescription.collectFirst {
                  case item if (item._1.findFirstMatchIn(metricTagValue).nonEmpty || item._1.findFirstMatchIn(descriptionTagValue).nonEmpty) => item._2
                }.get + complexStateDescription.eval(time)
              }

              override def args: Set[Expression[_]] = Set(innerDescription, complexStateDescription)
            },
            title = alertItemsHeader
          ).asCondition()
        ).withoutInfo().asCondition(minimumIssueCount =
          DynamicParameterExpression.withConstantDefault(
            minimumNumberOfIssueItemsParameter.getName,
            minimumNumberOfIssueItemsParameter.getDefaultValue.asInteger().intValue()))
      else {
        val treeFactory = StatusTreeExpression(
          // The time-series we check the test condition against:
          SelectTimeSeriesExpression[Double](context.tsDao, Set(metricName), historyLength = history, denseOnly = false),

          // The condition which, if true, we have an issue. Checked against the time-series we've collected
          condition
        )
        val tree = if (stateDescriptionComplexMetricName != null) {
          treeFactory.withSecondaryInfo(
            complexStateDescription,
            EMPTY_STRING,
            title = "Status Description"
          )
        } else {
          treeFactory.withoutInfo()
        }
        tree.asCondition()
      }, deviceCondition(context))
      // Details of the alert itself
    ).withRootInfo(
      getHeadline(),
      ConstantExpression(alertDescription),
      ConditionalRemediationSteps(baseRemediationText, vendorToRemediationText: _*)
    )
  }

  protected def deviceCondition(context: RuleContext): Condition = DefaultDeviceCondition
}

object StateDownTemplateRule {
  private val DefaultSeverity = AlertSeverity.ERROR
  private val DefaultApplicableMetricTag: String = null
  private val DefaultAlertItemsHeader: String = null
  private val DefaultDescriptionMetricTag: String = null
  private val DefaultDescriptionStringFormat = ""
  private val DefaultMetaCondition: TagsStoreCondition = True
  private val DefaultAlertIfDown = true
  private val DefaultItemSpecificDescription = Seq(".*".r -> "")
  private val DefaultItemsToIgnore = Set("^$".r)
  private val DefaultStateDescriptionComplexMetricName: String = null
  private val DefaultHistoryLength = 1
  private val DefaultGenerateStateDownCondition: (Int, TimeSeriesExpression[Double], ConstantExpression[Double]) => Condition =
    (historyLength: Int, tsToTestAgainst: TimeSeriesExpression[Double], stateToLookFor: ConstantExpression[Double]) =>
      EndsWithRepetition(tsToTestAgainst, stateToLookFor, historyLength)
  private val DefaultRuleCategories: Set[RuleCategory] = Set()
  private val DefaultDeviceCategory = DeviceCategory.AllDevices
  private val DefaultDeviceCondition: Condition = conditions.True

  @throws[YamlRuleParseException]
  def fromYaml(yaml: YamlObject, utils: YamlRuleProcessorUtils): StateDownTemplateRule = {
    import utils._
    val AllFields = Set(RuleNameField, RuleFriendlyNameField, RuleDescriptionField, SeverityField, MetricNameField,
      ApplicableMetricTagField, AlertItemsHeaderField, DescriptionMetricTagField, DescriptionStringFormatField,
      AlertDescriptionField, BaseRemediationTextField, MetaConditionField, AlertIfDownField,
      ItemSpecificDescriptionField, ItemsToIgnoreField, StateDescriptionComplexMetricNameField, HistoryLengthField,
      ShouldUseDevicePassiveAndPassiveLinkStateConditionField, ShouldUseHistoryContainsStateConditionField,
      RuleCategoriesField, DeviceCategoryField, VendorToRemediationTextField
    )
    validateNoExtraFields(yaml, AllFields)

    val severity = readOptional(yaml, SeverityField, alertSeverityParser).getOrElse(DefaultSeverity)
    val applicableMetricTag = readOptional(yaml, ApplicableMetricTagField, stringParser).getOrElse(DefaultApplicableMetricTag)
    val alertItemsHeader = readOptional(yaml, AlertItemsHeaderField, stringParser).getOrElse(DefaultAlertItemsHeader)
    val descriptionMetricTag = readOptional(yaml, DescriptionMetricTagField, stringParser).getOrElse(DefaultDescriptionMetricTag)
    val descriptionStringFormat = readOptional(yaml, DescriptionStringFormatField, stringParser).getOrElse(DefaultDescriptionStringFormat)
    val metaCondition = readOptional(yaml, MetaConditionField, metaConditionParser).getOrElse(DefaultMetaCondition)
    val alertIfDown = readOptional(yaml, AlertIfDownField, booleanParser).getOrElse(DefaultAlertIfDown)
    val itemSpecificDescription = readOptionalNonemptyList(yaml, ItemSpecificDescriptionField, regexStringPairTypeParser).getOrElse(DefaultItemSpecificDescription)
    val itemsToIgnore = readOptionalSet(yaml, ItemsToIgnoreField, regexTypeParser).getOrElse(DefaultItemsToIgnore)
    val stateDescriptionComplexMetricName = readOptional(yaml, StateDescriptionComplexMetricNameField, stringParser).getOrElse(DefaultStateDescriptionComplexMetricName)
    val historyLength = readOptional(yaml, HistoryLengthField, intParser).getOrElse(DefaultHistoryLength)
    val shouldUseHistoryContainsStateCondition = readOptional(yaml, ShouldUseHistoryContainsStateConditionField, booleanParser).getOrElse(false)
    val generateStateDownCondition: (Int, TimeSeriesExpression[Double], ConstantExpression[Double]) => Condition =
      if (shouldUseHistoryContainsStateCondition)
        (historyLength, tsToTestAgainst, stateToLookFor) => Contains(LastNNonEmptyValues(tsToTestAgainst, historyLength), stateToLookFor)
      else
        DefaultGenerateStateDownCondition
    val ruleCategories = readOptionalSet(yaml, RuleCategoriesField, ruleCategoryParser).getOrElse(DefaultRuleCategories)
    val deviceCategory = readOptional(yaml, DeviceCategoryField, deviceCategoryParser).getOrElse(DefaultDeviceCategory)
    val shouldUseDevicePassiveAndPassiveLinkStateCondition = readOptional(yaml, ShouldUseDevicePassiveAndPassiveLinkStateConditionField, booleanParser).getOrElse(false)

    new StateDownTemplateRule(
      ruleName = readMandatory(yaml, RuleNameField, stringParser),
      ruleFriendlyName = readMandatory(yaml, RuleFriendlyNameField, stringParser),
      ruleDescription = readMandatory(yaml, RuleDescriptionField, stringParser),
      severity = severity,
      metricName = readMandatory(yaml, MetricNameField, stringParser),
      applicableMetricTag = applicableMetricTag,
      alertItemsHeader = alertItemsHeader,
      descriptionMetricTag = descriptionMetricTag,
      descriptionStringFormat = descriptionStringFormat,
      alertDescription = readMandatory(yaml, AlertDescriptionField, stringParser),
      baseRemediationText = readMandatory(yaml, BaseRemediationTextField, stringParser),
      metaCondition = metaCondition,
      alertIfDown = alertIfDown,
      itemSpecificDescription = itemSpecificDescription,
      itemsToIgnore = itemsToIgnore,
      stateDescriptionComplexMetricName = stateDescriptionComplexMetricName,
      historyLength = historyLength,
      generateStateDownCondition = generateStateDownCondition,
      ruleCategories = ruleCategories,
      deviceCategory = deviceCategory
    )(readOptionalVendorToRemediationText(yaml, VendorToRemediationTextField): _*) {
      override protected def deviceCondition(context: RuleContext): Condition =
        if (shouldUseDevicePassiveAndPassiveLinkStateCondition) {
          generateDevicePassiveAndPassiveLinkStateCondition(context.tsDao)
        } else {
          DefaultDeviceCondition
        }
    }
  }
}
