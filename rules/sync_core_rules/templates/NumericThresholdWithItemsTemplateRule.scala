package com.indeni.server.rules.library.templates

import com.indeni.ruleengine.Scope.{Scope, ImplicitScopeValueHelper}
import com.indeni.ruleengine.expressions.Expression
import com.indeni.ruleengine.expressions.conditions.{And, ConditionHelper, Or, ResultsFound}
import com.indeni.ruleengine.expressions.core.{ConstantExpression, StatusTreeExpression}
import com.indeni.ruleengine.expressions.data.{SelectExpression, SelectTagsExpression}
import com.indeni.ruleengine.expressions.regex.RegexExpression
import com.indeni.ruleengine.expressions.scope.{ScopableExpression, ScopeValueExpression}
import com.indeni.server.common.data.TagsStoreDao
import com.indeni.server.common.data.conditions.{TagsStoreCondition, True}
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules.library.{ConditionalRemediationSteps, PerDeviceRule, RuleHelper}
import com.indeni.server.rules.{DeviceCategory, DeviceKey, RemediationStepCondition, RuleCategory, RuleContext, RuleMetadata, ThresholdDirection}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

import scala.util.matching.Regex

class NumericThresholdWithItemsTemplateRule[A: Numeric](ruleName: String,
                                                        ruleFriendlyName: String,
                                                        ruleDescription: String,
                                                        severity: AlertSeverity = AlertSeverity.ERROR,
                                                        value: Expression[Option[A]],
                                                        tagsDao: RuleContext => TagsStoreDao,
                                                        selectExpression: RuleContext => SelectExpression[Scope],
                                                        metricName: String,
                                                        threshold: A,
                                                        metaCondition: TagsStoreCondition = True,
                                                        applicableMetricTag: String,
                                                        alertItemDescriptionFormat: String,
                                                        alertItemDescriptionValueConverter: Expression[Option[A]] => Expression[Option[Double]],
                                                        alertDescription: String,
                                                        baseRemediationText: String,
                                                        thresholdDirection: ThresholdDirection = ThresholdDirection.ABOVE,
                                                        alertItemsHeader: String,
                                                        itemsToIgnore: Set[Regex] = Set("^$".r),
                                                        itemSpecificDescription: Seq[(Regex, String)] = Seq(".*".r -> ""),
                                                        ruleCategories:Set[RuleCategory] = Set(),
                                                        deviceCategory: DeviceCategory = DeviceCategory.AllDevices)(vendorToRemediationText: (RemediationStepCondition, String)*)
  extends PerDeviceRule
    with RuleHelper {

  private[library] val thresholdParameterName = "threshold"
  private val thresholdParameter = new ParameterDefinition(
    thresholdParameterName,
    "",
    "Alerting Threshold",
    "indeni will trigger an issue if the value is " + (if (thresholdDirection == ThresholdDirection.ABOVE) "above" else "below") + " this value.",
    UIType.fromObjectClass(threshold.getClass),
    threshold
  )

  override val metadata: RuleMetadata = RuleMetadata
    .builder(ruleName, ruleFriendlyName, ruleDescription, AlertSeverity.ERROR,ruleCategories, deviceCategory = deviceCategory)
    .configParameter(thresholdParameter)
    .build()

  override def expressionTree(context: RuleContext): StatusTreeExpression = {
    val thresholdValue = getParameter(thresholdParameter)

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), metaCondition),
      StatusTreeExpression(
        // The additional tags we care about (we'll be including this in alert data)
        SelectTagsExpression(tagsDao(context), Set(applicableMetricTag), withTagsCondition(metricName)),
        StatusTreeExpression(
          // The time-series we check the test condition against:
          selectExpression(context),
          // The condition which, if true, we have an issue. Checked against the time-series we've collected
          And(
            generateCompareCondition(thresholdDirection, value, thresholdValue),
            Or(
              itemsToIgnore
                .map(r =>
                  ResultsFound(RegexExpression(ScopeValueExpression(applicableMetricTag).visible().asString(), r)))
                .toSeq
            ).not
          )

          // The Alert Item to add for this specific item
        ).withSecondaryInfo(
          scopableStringFormatExpression("${scope(\"" + applicableMetricTag + "\")}"),
          new ScopableExpression[String] {
            val innerDescription = scopableStringFormatExpression(alertItemDescriptionFormat, alertItemDescriptionValueConverter(value))

            override protected def evalWithScope(time: Long, scope: Scope): String = {
              val metricTagValue = scope.getVisible(applicableMetricTag).get.toString
              innerDescription.eval(time) + itemSpecificDescription.collectFirst {
                case item if (!item._1.findFirstMatchIn(metricTagValue).isEmpty) => item._2
              }.get
            }

            override def args: Set[Expression[_]] = Set(innerDescription)
          },
          title = alertItemsHeader
        )
          .asCondition()
      ).withoutInfo().asCondition()
    ).withRootInfo(
      getHeadline(),
      ConstantExpression(alertDescription),
      ConditionalRemediationSteps(baseRemediationText, vendorToRemediationText: _*)
    )
  }
}
