package com.indeni.server.rules.library.templates

import com.indeni.ruleengine.Scope.{Scope, ImplicitScopeValueHelper}
import com.indeni.ruleengine.expressions.Expression
import com.indeni.ruleengine.expressions.core.{ConstantExpression, EMPTY_STRING, MultiIssueInformer, StatusTreeExpression}
import com.indeni.ruleengine.expressions.data._
import com.indeni.ruleengine.expressions.scope.ScopableExpression
import com.indeni.ruleengine.expressions.utility.IsEmptyExpression.IsEmptyExpressionHelper
import com.indeni.ruleengine.expressions.utility.{SeqDiffMissingsExpression, SeqDiffRedundantsExpression, SeqDiffWithoutOrderExpression}
import com.indeni.server.common.AlertNotificationSettings
import com.indeni.server.common.AlertNotificationSettings._
import com.indeni.server.common.data.conditions.{TagsStoreCondition, True}
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules._
import com.indeni.server.rules.config.expressions.DynamicParameterExpression
import com.indeni.server.rules.library._
import com.indeni.server.rules.library.templates.MultiSnapshotComplianceCheckTemplateRule.{DefaultDeviceCategory, DefaultMetaCondition, DefaultRuleCategories, DefaultSeverity}
import com.indeni.server.rules.library.yamlrules.YamlRuleProcessorUtils._
import com.indeni.server.rules.library.yamlrules.{YamlRuleParseException, YamlRuleProcessorUtils}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import net.jcazevedo.moultingyaml.YamlObject

class MultiSnapshotComplianceCheckTemplateRule(ruleName: String, ruleFriendlyName: String, ruleDescription: String,
                                               severity: AlertSeverity = DefaultSeverity,
                                               metricName: String, itemKey: String, alertDescription: String,
                                               baseRemediationText: String,
                                               metaCondition: TagsStoreCondition = DefaultMetaCondition,
                                               requiredItemsParameterName: String, requiredItemsParameterDescription: String,
                                               ruleCategories:Set[RuleCategory] = DefaultRuleCategories,
                                               deviceCategory: DeviceCategory = DefaultDeviceCategory)(vendorToRemediationText: (RemediationStepCondition, String)*) extends PerDeviceRule with RuleHelper {

  private val requiredItems = new ParameterDefinition(
    "required_items",
    "",
    requiredItemsParameterName,
    requiredItemsParameterDescription,
    UIType.MULTILINE_TEXT,
    "")

  override val metadata: RuleMetadata = RuleMetadata.builder(ruleName, ruleFriendlyName, ruleDescription, severity,ruleCategories,deviceCategory = deviceCategory)
    .defaultAction(AlertNotificationSettings(NONE))
    .configParameter(requiredItems)
    .disableGlobalRuleSet(true)
    .build()

  override def expressionTree(context: RuleContext): StatusTreeExpression = {
    val itemTag = "item_tag"
    val itemsExpected = DynamicParameterExpression.withConstantDefault(requiredItems.getName, Seq[String]()).withLazy
    val diff = SeqDiffWithoutOrderExpression[String](
      MultiSnapshotExtractScalarExpression(SnapshotExpression(metricName).asMulti().mostRecent().value(), itemKey).noneable,
      itemsExpected
    ).withLazy

    val headline = new ScopableExpression[String] {
      override protected def evalWithScope(time: Long, scope: Scope): String = scope.getVisible(itemTag).get.toString

      override def args: Set[Expression[_]] = Set()
    }

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), metaCondition),

      // What constitutes an issue
      StatusTreeExpression(
        // The snapshot we check the test condition against:
        SelectSnapshotsExpression(context.snapshotsDao, Set(metricName)).multi(),

        // The condition which, if true, we have an issue. Checked against the snapshots we've collected
        diff.nonEmpty,
        multiInformers =
          Set(
            MultiIssueInformer(
              headline,
              EMPTY_STRING,
              "Missing Configurations"
            ).iterateOver(SeqDiffMissingsExpression(diff),
              itemTag,
              com.indeni.ruleengine.expressions.conditions.True
            ),
            MultiIssueInformer(
              headline,
              EMPTY_STRING,
              "Incorrect Configurations"
            ).iterateOver(
              SeqDiffRedundantsExpression(diff),
              itemTag,
              com.indeni.ruleengine.expressions.conditions.True
            )
          )
        // Details of the alert itself
      ).withoutInfo().asCondition()
    ).withRootInfo(
      getHeadline(),
      ConstantExpression(alertDescription),
      ConditionalRemediationSteps(baseRemediationText, vendorToRemediationText:_*)
    )
  }
}

object MultiSnapshotComplianceCheckTemplateRule {
  val DefaultSeverity = AlertSeverity.ERROR
  val DefaultMetaCondition: TagsStoreCondition = True
  val DefaultRuleCategories: Set[RuleCategory] = Set()
  val DefaultDeviceCategory = DeviceCategory.AllDevices
  @throws[YamlRuleParseException]
  def fromYaml(yaml: YamlObject, utils: YamlRuleProcessorUtils): MultiSnapshotComplianceCheckTemplateRule = {
    import utils._
    val AllFields = Set(RuleNameField, RuleFriendlyNameField, RuleDescriptionField, SeverityField, MetricNameField,
      ItemKeyField, AlertDescriptionField, BaseRemediationTextField, MetaConditionField,
      RequiredItemsParameterNameField, RequiredItemsParameterDescriptionField, RuleCategoriesField,
      DeviceCategoryField, VendorToRemediationTextField)
    validateNoExtraFields(yaml, AllFields)
    val severity = readOptional(yaml, SeverityField, alertSeverityParser).getOrElse(DefaultSeverity)
    val metaCondition = readOptional(yaml, MetaConditionField, metaConditionParser).getOrElse(DefaultMetaCondition)
    val ruleCategories = readOptionalSet(yaml, RuleCategoriesField, ruleCategoryParser).getOrElse(DefaultRuleCategories)
    val deviceCategory = readOptional(yaml, DeviceCategoryField, deviceCategoryParser).getOrElse(DefaultDeviceCategory)

    new MultiSnapshotComplianceCheckTemplateRule(
      ruleName = readMandatory(yaml, RuleNameField, stringParser),
      ruleFriendlyName = readMandatory(yaml, RuleFriendlyNameField, stringParser),
      ruleDescription = readMandatory(yaml, RuleDescriptionField, stringParser),
      severity = severity,
      metricName = readMandatory(yaml, MetricNameField, stringParser),
      itemKey = readMandatory(yaml, ItemKeyField, stringParser),
      alertDescription = readMandatory(yaml, AlertDescriptionField, stringParser),
      baseRemediationText = readMandatory(yaml, BaseRemediationTextField, stringParser),
      metaCondition = metaCondition,
      requiredItemsParameterName = readMandatory(yaml, RequiredItemsParameterNameField, stringParser),
      requiredItemsParameterDescription = readMandatory(yaml, RequiredItemsParameterDescriptionField, stringParser),
      ruleCategories = ruleCategories,
      deviceCategory = deviceCategory
    )(readOptionalVendorToRemediationText(yaml, VendorToRemediationTextField): _*)
  }
}
