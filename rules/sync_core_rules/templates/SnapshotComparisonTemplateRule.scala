package com.indeni.server.rules.library.templates

import com.indeni.ruleengine.expressions.cluster.SelectClusterComparisonExpression
import com.indeni.ruleengine.expressions.conditions.{And, Condition, Equals, GreaterThanOrEqual}
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SingleSnapshotExtractExpression}
import com.indeni.ruleengine.expressions.math.MinExpression
import com.indeni.ruleengine.expressions.{Expression, conditions}
import com.indeni.server.common.data.Snapshot.SingleDimension
import com.indeni.server.common.data.conditions.{TagsStoreCondition, True}
import com.indeni.server.metrics.DeviceStoreActor
import com.indeni.server.rules.library._
import com.indeni.server.rules.library.templates.SnapshotComparisonTemplateRule._
import com.indeni.server.rules.library.yamlrules.YamlRuleProcessorUtils._
import com.indeni.server.rules.library.yamlrules.{YamlRuleParseException, YamlRuleProcessorUtils}
import com.indeni.server.rules.{RuleContext, RuleMetadata, _}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import net.jcazevedo.moultingyaml.YamlObject

import scala.concurrent.duration._

/**
  * Note: An this rule will result with an issue only if the following condition is yields with a result equals to True
  * assuming the snapshot samples are being reported relatively on the same time
  *
  * {{{
  *  this(most-resent.value) != cluster(most-resent.value) &&
  *  min((eval-time - this(most-recent.first-seen)), (eval-time - cluster(most-recent.first-seen))) > th
  *
  * case 1: -------------------------------------------> T
  * this    : o, o, o, o, o, o, o, o, x, x ,x, x, x, x
  * cluster : o, o, o, o, x, x, x, x, x ,x, x, x, x, x
  *
  * case 2:
  * this    : o, o, o, o, x, x, x, x, x ,x, x, x, x, x
  * cluster : o, o, o, o, o, o, o, o, x, x ,x, x, x, x
  *
  * case 3:
  * this    : o
  * cluster : x
  *
  * case 4:
  * this    : None
  * cluster : o
  *
  * case 5: (rare) not addressed in with this rule
  * this    : o, o ,o, o, o, o, z, z, z ,z ,z, z, z, z
  * cluster : o, o ,o, o, x, y, y, z, z ,z ,z, z, z, z
  * }}}
  */
class SnapshotComparisonTemplateRule(
    ruleName: String,
    ruleFriendlyName: String,
    severity: AlertSeverity = DefaultSeverity,
    ruleDescription: String,
    metricName: String,
    isArray: Boolean,
    applicableMetricTag: String = DefaultApplicableMetricTag,
    alertDescription: String,
    alertItemsHeader: String = DefaultAlertItemsHeader,
    descriptionMetricTag: String = DefaultDescriptionMetricTag,
    descriptionStringFormat: String = DefaultDescriptionStringFormat,
    baseRemediationText: String,
    metaCondition: TagsStoreCondition = DefaultMetaCondition,
    samplesTimeDifferenceThreshold: Option[FiniteDuration] = DefaultSamplesTimeDifferenceThreshold,
    includeSnapshotDiff: Boolean = DefaultIncludeSnapshotDiff,
    ruleCategories: Set[RuleCategory] = DefaultRuleCategories,
    deviceCategory: DeviceCategory = DefaultDeviceCategory,
    alertIfMembersValuesAreTheSame: Boolean = DefaultAlertIfMembersValuesAreTheSame,
    alertValue: String = DefaultAlertValue)(vendorToRemediationText: (RemediationStepCondition, String)*)
    extends PerDeviceRule
    with RuleHelper {

  override val metadata: RuleMetadata =
    RuleMetadata.builder(ruleName, ruleFriendlyName, ruleDescription, severity, ruleCategories, deviceCategory).build()

  override def expressionTree(context: RuleContext): StatusTreeExpression = {

    val alertIfClustersValuesAreTheSameCondition =
      if (alertIfMembersValuesAreTheSame) conditions.True else conditions.False
    val diffThresholdExpression =
      ConstantExpression[Option[Long]](samplesTimeDifferenceThreshold.map(_.toMillis).orElse(Some(0)))

    val thisSnapshot = SelectClusterComparisonExpression.thisSnapshotExpression().mostRecent()
    val clusterSnapshot = SelectClusterComparisonExpression.clusterSnapshotExpression().mostRecent()

    val thisSnapshotValue = thisSnapshot.value().noneable.withLazy
    val clusterSnapshotValue = clusterSnapshot.value().noneable.withLazy

    val thisSnapshotFirstSeen = thisSnapshot.firstSeen().noneable.withLazy
    val clusterSnapshotFirstSeen = clusterSnapshot.firstSeen().noneable.withLazy

    val alertValueCondition =
      if (isArray || alertValue == null)
        conditions.True
      else
        Equals(SingleSnapshotExtractExpression(thisSnapshot.value().asInstanceOf[Expression[SingleDimension]], "value"),
          ConstantExpression(alertValue).noneable)


    val timeDifferenceExpression = MinExpression(new Expression[Seq[Option[Long]]] {

      override def eval(time: Long): Seq[Option[Long]] = {
        Seq(thisSnapshotFirstSeen, clusterSnapshotFirstSeen).map(_.eval(time).map(time - _))
      }

      override def args: Set[Expression[_]] = Set(thisSnapshotFirstSeen, clusterSnapshotFirstSeen)
    })

    val selectExpression = SelectClusterComparisonExpression(
      context.metaDao,
      context.devicesDao,
      DeviceStoreActor.TAG_DEVICE_ID,
      DeviceStoreActor.TAG_DEVICE_NAME,
      DeviceStoreActor.TAG_DEVICE_IP,
      "cluster-id",
      context.snapshotsDao,
      metricName
    )

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), metaCondition),
      And(
        // What constitutes an issue
        if (applicableMetricTag != null) {
          val alertItem = scopableStringFormatExpression(
            "${scope(\"" + applicableMetricTag + "\")}" + (if (descriptionMetricTag != null)
                                                             " (${scope(\"" + descriptionMetricTag + "\")})"
                                                           else "") + " on %s (%s)",
            SelectClusterComparisonExpression.clusterDeviceNameExpression(),
            SelectClusterComparisonExpression.clusterDeviceIpExpression()
          )
          StatusTreeExpression(
            // The additional tags we care about (we'll be including this in alert data)
            SelectTagsExpression(
              context.snapshotsDao,
              if (descriptionMetricTag == null) Set(applicableMetricTag)
              else Set(applicableMetricTag, descriptionMetricTag),
              withTagsCondition(metricName)
            ),
            StatusTreeExpression(
              if (isArray) selectExpression.multi() else selectExpression.single(),
              // The condition which, if true, we have an issue. Checked against the time-series we've collected
              And(
                alertValueCondition,
                Equals(Equals(thisSnapshotValue, clusterSnapshotValue), alertIfClustersValuesAreTheSameCondition),
                GreaterThanOrEqual(timeDifferenceExpression, diffThresholdExpression)
              )

              // The Alert Item to add for this specific item
            ).withSecondaryInfo(
                alertItem,
                if (includeSnapshotDiff) SnapshotDiffExpression(thisSnapshotValue, clusterSnapshotValue, alertItem)
                else EMPTY_STRING,
                title = alertItemsHeader
              )
              .asCondition()
          ).withoutInfo().asCondition()
        } else {
          val alertItem =
            scopableStringFormatExpression("%s (%s)",
                                           SelectClusterComparisonExpression.clusterDeviceNameExpression(),
                                           SelectClusterComparisonExpression.clusterDeviceIpExpression())
          StatusTreeExpression(
            // The time-series we check the test condition against:
            if (isArray) selectExpression.multi() else selectExpression.single(),
            // The condition which, if true, we have an issue. Checked against the time-series we've collected
            And(
              alertValueCondition,
              Equals(Equals(thisSnapshotValue, clusterSnapshotValue), alertIfClustersValuesAreTheSameCondition),
              GreaterThanOrEqual(timeDifferenceExpression, diffThresholdExpression)
            )
          ).withSecondaryInfo(
              alertItem,
              if (includeSnapshotDiff) SnapshotDiffExpression(thisSnapshotValue, clusterSnapshotValue, alertItem)
              else EMPTY_STRING,
              title = alertItemsHeader
            )
            .asCondition()
        },
        deviceCondition(context)
      )
      // Details of the alert itself
    ).withRootInfo(
      getHeadline(),
      ConstantExpression(alertDescription),
      ConditionalRemediationSteps(baseRemediationText, vendorToRemediationText: _*)
    )
  }

  protected def deviceCondition(context: RuleContext): Condition = DefaultDeviceCondition
}

object SnapshotComparisonTemplateRule {

  private val DefaultSeverity = AlertSeverity.ERROR
  private val DefaultApplicableMetricTag: String = null
  private val DefaultAlertItemsHeader = "Mismatching Cluster Members"
  private val DefaultDescriptionMetricTag: String = null
  private val DefaultDescriptionStringFormat = ""
  private val DefaultMetaCondition: TagsStoreCondition = True
  private val DefaultSamplesTimeDifferenceThreshold: Option[FiniteDuration] = Some(5 minutes)
  private val DefaultIncludeSnapshotDiff = true
  private val DefaultRuleCategories: Set[RuleCategory] = Set()
  private val DefaultDeviceCategory = DeviceCategory.AllDevices
  private val DefaultDeviceCondition: Condition = conditions.True
  private val DefaultAlertIfMembersValuesAreTheSame: Boolean = false
  private val DefaultAlertValue: String = null

  @throws[YamlRuleParseException]
  def fromYaml(yaml: YamlObject, utils: YamlRuleProcessorUtils): SnapshotComparisonTemplateRule = {
    import utils._
    val AllFields = Set(
      RuleNameField,
      RuleFriendlyNameField,
      SeverityField,
      RuleDescriptionField,
      MetricNameField,
      IsArrayField,
      ApplicableMetricTagField,
      AlertDescriptionField,
      AlertItemsHeaderField,
      DescriptionMetricTagField,
      DescriptionStringFormatField,
      BaseRemediationTextField,
      MetaConditionField,
      SamplesTimeDifferenceThresholdField,
      IncludeSnapshotDiffField,
      ShouldUseDevicePassiveAndPassiveLinkStateConditionField,
      RuleCategoriesField,
      DeviceCategoryField,
      VendorToRemediationTextField,
      AlertIfMembersValuesAreTheSameField,
      AlertValueField
    )
    validateNoExtraFields(yaml, AllFields)

    val severity = readOptional(yaml, SeverityField, alertSeverityParser).getOrElse(DefaultSeverity)
    val applicableMetricTag =
      readOptional(yaml, ApplicableMetricTagField, stringParser).getOrElse(DefaultApplicableMetricTag)
    val alertItemsHeader = readOptional(yaml, AlertItemsHeaderField, stringParser).getOrElse(DefaultAlertItemsHeader)
    val descriptionMetricTag =
      readOptional(yaml, DescriptionMetricTagField, stringParser).getOrElse(DefaultDescriptionMetricTag)
    val descriptionStringFormat =
      readOptional(yaml, DescriptionStringFormatField, stringParser).getOrElse(DefaultDescriptionStringFormat)
    val metaCondition = readOptional(yaml, MetaConditionField, metaConditionParser).getOrElse(DefaultMetaCondition)
    val samplesTimeDifferenceThreshold = readOptional(yaml, SamplesTimeDifferenceThresholdField, timeIntervalParser)
      .orElse(DefaultSamplesTimeDifferenceThreshold)
    val includeSnapshotDiff =
      readOptional(yaml, IncludeSnapshotDiffField, booleanParser).getOrElse(DefaultIncludeSnapshotDiff)
    val ruleCategories = readOptionalSet(yaml, RuleCategoriesField, ruleCategoryParser).getOrElse(DefaultRuleCategories)
    val deviceCategory = readOptional(yaml, DeviceCategoryField, deviceCategoryParser).getOrElse(DefaultDeviceCategory)
    val shouldUseDevicePassiveAndPassiveLinkStateCondition =
      readOptional(yaml, ShouldUseDevicePassiveAndPassiveLinkStateConditionField, booleanParser).getOrElse(false)
    val alertIfMembersValuesAreTheSame =
      readOptional(yaml, AlertIfMembersValuesAreTheSameField, booleanParser).getOrElse(
        DefaultAlertIfMembersValuesAreTheSame)
    val alertValue =
      readOptional(yaml, AlertValueField, stringParser).getOrElse(DefaultAlertValue)

    new SnapshotComparisonTemplateRule(
      ruleName = readMandatory(yaml, RuleNameField, stringParser),
      ruleFriendlyName = readMandatory(yaml, RuleFriendlyNameField, stringParser),
      severity = severity,
      ruleDescription = readMandatory(yaml, RuleDescriptionField, stringParser),
      metricName = readMandatory(yaml, MetricNameField, stringParser),
      isArray = readMandatory(yaml, IsArrayField, booleanParser),
      applicableMetricTag = applicableMetricTag,
      alertDescription = readMandatory(yaml, AlertDescriptionField, stringParser),
      alertItemsHeader = alertItemsHeader,
      descriptionMetricTag = descriptionMetricTag,
      descriptionStringFormat = descriptionStringFormat,
      baseRemediationText = readMandatory(yaml, BaseRemediationTextField, stringParser),
      metaCondition = metaCondition,
      samplesTimeDifferenceThreshold = samplesTimeDifferenceThreshold,
      includeSnapshotDiff = includeSnapshotDiff,
      ruleCategories = ruleCategories,
      deviceCategory = deviceCategory,
      alertIfMembersValuesAreTheSame = alertIfMembersValuesAreTheSame,
      alertValue = alertValue
    )(readOptionalVendorToRemediationText(yaml, VendorToRemediationTextField): _*) {
      override protected def deviceCondition(context: RuleContext): Condition =
        if (shouldUseDevicePassiveAndPassiveLinkStateCondition) {
          generateDevicePassiveAndPassiveLinkStateCondition(context.tsDao)
        } else {
          DefaultDeviceCondition
        }
    }
  }
}
