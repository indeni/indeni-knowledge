package com.indeni.server.rules.library.templates

import com.indeni.ruleengine.Scope.{Scope, ImplicitScopeValueHelper}
import com.indeni.ruleengine.expressions.conditions.{And, Condition}
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data._
import com.indeni.ruleengine.expressions.scope.ScopableExpression
import com.indeni.ruleengine.expressions.{Expression, conditions}
import com.indeni.server.common.data.conditions.{TagsStoreCondition, True}
import com.indeni.server.rules._
import com.indeni.server.rules.library.RuleHelper._
import com.indeni.server.rules.library._
import com.indeni.server.rules.library.templates.SingleSnapshotValueCheckTemplateRule._
import com.indeni.server.rules.library.yamlrules.YamlRuleProcessorUtils._
import com.indeni.server.rules.library.yamlrules.{YamlRuleParseException, YamlRuleProcessorUtils}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import com.typesafe.scalalogging.Logger
import net.jcazevedo.moultingyaml.YamlObject

import scala.util.matching.Regex

class SingleSnapshotValueCheckTemplateRule(ruleName: String,
                                           ruleFriendlyName: String,
                                           ruleDescription: String,
                                           severity: AlertSeverity = DefaultSeverity,
                                           metricName: String,
                                           applicableMetricTag: String = DefaultApplicableMetricTag,
                                           alertAlternativeMetricTag: Option[String] = DefaultAlternativeMetricTag,
                                           alertDescription: String,
                                           alertItemsHeader: String = DefaultAlertItemsHeader,
                                           baseRemediationText: String,
                                           complexCondition: Condition,
                                           secondaryCondition: RuleContext => Logger => Condition = DefaultSecondaryCondition,
                                           metaCondition: TagsStoreCondition = DefaultMetaCondition,
                                           itemSpecificDescription: Seq[(Regex, String)] = DefaultItemSpecificDescription,
                                           alertItemHeadlineExpersion: Expression[String] = DefaultAlertItemHeadlineExpression,
                                           headlineFormat: String = DefaultHeadlineFormat,
                                           ruleCategories: Set[RuleCategory] = DefaultRuleCategories,
                                           deviceCategory: DeviceCategory = DefaultDeviceCategory)
                                          (vendorToRemediationText: (RemediationStepCondition, String)*)
  extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata =
    RuleMetadata.builder(ruleName, ruleFriendlyName, ruleDescription, severity, ruleCategories, deviceCategory).build()

  override def expressionTree(context: RuleContext): StatusTreeExpression = {

    val alertInfoExpression = new ScopableExpression[String] {

      override protected def evalWithScope(time: Long, scope: Scope): String = {

        def fetchTagValue: Option[String] = {

          val tagValue = alertAlternativeMetricTag match {
            case Some(alertTag) => scope.getInvisible(alertTag)
            case None           => scope.getVisible(applicableMetricTag)
          }

          tagValue.map(_.toString)
        }

        def constructItem(tagValue: String): Option[String] = itemSpecificDescription.collectFirst {
          case item if item._1.findFirstMatchIn(tagValue).isDefined => item._2
        }

        fetchTagValue.flatMap(constructItem).get
      }

      override def args: Set[Expression[_]] = Set()
    }

    def itemInfoExpression: ScopableExpression[String] = {
      val itemTag = alertAlternativeMetricTag.map(":" + _).getOrElse(applicableMetricTag)
      scopableStringFormatExpression("${scope(\"" + itemTag + "\")}")
    }

    StatusTreeExpression(

      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), metaCondition),

      // What constitutes an issue
      if (null != applicableMetricTag) {

        StatusTreeExpression(

          // The additional tags we care about (we'll be including this in alert data)
          SelectTagsExpression(context.snapshotsDao,
                               Set(applicableMetricTag),
                               withTagsCondition(metricName),
                               alertAlternativeMetricTag.toSet),

          StatusTreeExpression(

            // The time-series we check the test condition against:
            SelectSnapshotsExpression(context.snapshotsDao, Set(metricName)).single(),

            // The condition which, if true, we have an issue. Checked against the time-series we've collected
            And(complexCondition, secondaryCondition(context)(logger))

          ).withSecondaryInfo(
              scopableStringFormatExpression(headlineFormat, itemInfoExpression, alertItemHeadlineExpersion),
              alertInfoExpression,
              title = alertItemsHeader
            )
            .asCondition()
        ).withoutInfo().asCondition()
      } else {
        StatusTreeExpression(

          // The time-series we check the test condition against:
          SelectSnapshotsExpression(context.snapshotsDao, Set(metricName)).single(),

          // The condition which, if true, we have an issue. Checked against the time-series we've collected
          And(complexCondition, secondaryCondition(context)(logger))

        ).withoutInfo().asCondition()
      }
      // Details of the alert itself
    ).withRootInfo(
      getHeadline(),
      ConstantExpression(alertDescription),
      ConditionalRemediationSteps(baseRemediationText, vendorToRemediationText: _*)
    )
  }
}

object SingleSnapshotValueCheckTemplateRule {
  private val DefaultSeverity = AlertSeverity.ERROR
  private val DefaultApplicableMetricTag: String = null
  private val DefaultAlertItemsHeader: String = null
  private val DefaultMetaCondition: TagsStoreCondition = True
  private val DefaultItemSpecificDescription = Seq(".*".r -> "")
  private val DefaultAlertItemHeadlineExpression: Expression[String] = EMPTY_STRING
  private val DefaultHeadlineFormat = "%s"
  private val DefaultRuleCategories: Set[RuleCategory] = Set()
  private val DefaultDeviceCategory = DeviceCategory.AllDevices
  private val DefaultAlternativeMetricTag: Option[String] = None
  private val DefaultSecondaryCondition: RuleContext => Logger => Condition = _ => _ => conditions.True

  @throws[YamlRuleParseException]
  def fromYaml(yaml: YamlObject, utils: YamlRuleProcessorUtils): SingleSnapshotValueCheckTemplateRule = {
    import utils._
    val AllFields = Set(RuleNameField, RuleFriendlyNameField, RuleDescriptionField, SeverityField, MetricNameField,
      ApplicableMetricTagField, AlertDescriptionField, AlertItemsHeaderField,
      BaseRemediationTextField, ComplexConditionField, MetaConditionField, ItemSpecificDescriptionField,
      AlertItemHeadlineStrMetricField, HeadlineFormatField, RuleCategoriesField, DeviceCategoryField,
      VendorToRemediationTextField, AlertInfoMetricTagField, AddNetworkInterfaceAdminStateSecondaryConditionField
    )
    validateNoExtraFields(yaml, AllFields)

    val severity = readOptional(yaml, SeverityField, alertSeverityParser).getOrElse(DefaultSeverity)
    val applicableMetricTag = readOptional(yaml, ApplicableMetricTagField, stringParser).getOrElse(DefaultApplicableMetricTag)
    val alertItemsHeader = readOptional(yaml, AlertItemsHeaderField, stringParser).getOrElse(DefaultAlertItemsHeader)
    val metaCondition = readOptional(yaml, MetaConditionField, metaConditionParser).getOrElse(DefaultMetaCondition)
    val itemSpecificDescription = readOptionalList(yaml, ItemSpecificDescriptionField, regexStringPairTypeParser).getOrElse(DefaultItemSpecificDescription)
    val alertItemHeadlineStrMetric = readOptional(yaml, AlertItemHeadlineStrMetricField, stringParser)
    val alertItemHeadlineExpression = alertItemHeadlineStrMetric.map(strMetricName => new Expression[String] {
      private val metricExpression = SnapshotExpression(strMetricName).asSingle().mostRecent()
      override def eval(time: Long): String = metricExpression.eval(time).value.getOrElse("value", "")
      override def args: Set[Expression[_]] = Set(metricExpression)
    }).getOrElse(DefaultAlertItemHeadlineExpression)
    val headlineFormat = readOptional(yaml, HeadlineFormatField, stringParser).getOrElse(DefaultHeadlineFormat)
    val ruleCategories = readOptionalSet(yaml, RuleCategoriesField, ruleCategoryParser).getOrElse(DefaultRuleCategories)
    val deviceCategory = readOptional(yaml, DeviceCategoryField, deviceCategoryParser).getOrElse(DefaultDeviceCategory)
    val alertInfoTag = readOptional(yaml, AlertInfoMetricTagField, stringParser).orElse(DefaultAlternativeMetricTag)
    val addNetworkInterfaceAdminStateSecondaryCondition = readOptional(yaml, AddNetworkInterfaceAdminStateSecondaryConditionField, booleanParser).getOrElse(false)
    val secondaryCondition: RuleContext => Logger => Condition =
      if (addNetworkInterfaceAdminStateSecondaryCondition)
        c => networkInterfaceAdminState(c.tsDao, expected = 1.0)
      else
        DefaultSecondaryCondition

    new SingleSnapshotValueCheckTemplateRule(
      ruleName = readMandatory(yaml, RuleNameField, stringParser),
      ruleFriendlyName = readMandatory(yaml, RuleFriendlyNameField, stringParser),
      ruleDescription = readMandatory(yaml, RuleDescriptionField, stringParser),
      severity = severity,
      metricName = readMandatory(yaml, MetricNameField, stringParser),
      applicableMetricTag = applicableMetricTag,
      alertAlternativeMetricTag = alertInfoTag,
      alertDescription = readMandatory(yaml, AlertDescriptionField, stringParser),
      alertItemsHeader = alertItemsHeader,
      baseRemediationText = readMandatory(yaml, BaseRemediationTextField, stringParser),
      complexCondition = readMandatory(yaml, ComplexConditionField, complexConditionParser),
      secondaryCondition = secondaryCondition,
      metaCondition = metaCondition,
      itemSpecificDescription = itemSpecificDescription,
      alertItemHeadlineExpersion = alertItemHeadlineExpression,
      headlineFormat = headlineFormat,
      ruleCategories = ruleCategories,
      deviceCategory = deviceCategory
    )(readOptionalVendorToRemediationText(yaml, VendorToRemediationTextField): _*)
  }
}
