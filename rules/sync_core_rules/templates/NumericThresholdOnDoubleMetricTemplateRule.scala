package com.indeni.server.rules.library.templates

import com.indeni.ruleengine.expressions.core._
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.ruleengine.expressions.math.DivExpression
import com.indeni.server.common.data.conditions.{TagsStoreCondition, True}
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules._
import com.indeni.server.rules.library._
import com.indeni.server.rules.library.templates.NumericThresholdOnDoubleMetricTemplateRule._
import com.indeni.server.rules.library.yamlrules.YamlRuleProcessorUtils._
import com.indeni.server.rules.library.yamlrules.{YamlRuleParseException, YamlRuleProcessorUtils}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import net.jcazevedo.moultingyaml.YamlObject

class NumericThresholdOnDoubleMetricTemplateRule(ruleName: String,
                                                 ruleFriendlyName: String,
                                                 ruleDescription: String,
                                                 severity: AlertSeverity = DefaultSeverity,
                                                 metricName: String,
                                                 threshold: Double,
                                                 metaCondition: TagsStoreCondition = DefaultMetaCondition,
                                                 alertDescriptionFormat: String,
                                                 unitConverter: ConstantExpression[Option[Double]] = DefaultUnitConverter,
                                                 baseRemediationText: String,
                                                 thresholdDirection: ThresholdDirection = DefaultThresholdDirection,
                                                 ruleCategories:Set[RuleCategory] = DefaultRuleCategories,
                                                 deviceCategory: DeviceCategory = DefaultDeviceCategory
                                                )
                                                (vendorToRemediationText: (RemediationStepCondition, String)*) extends PerDeviceRule with RuleHelper {

  private[library] val thresholdParameterName = "threshold"
  private val thresholdParameter =
    new ParameterDefinition(thresholdParameterName,
      "",
      "Alerting Threshold",
      "Indeni will alert if the value is " + (if (thresholdDirection == ThresholdDirection.ABOVE) "above" else "below") + " this value.",
      UIType.DOUBLE,
      threshold)

  override val metadata: RuleMetadata = RuleMetadata
    .builder(ruleName, ruleFriendlyName, ruleDescription, severity, ruleCategories, deviceCategory)
    .configParameter(thresholdParameter)
    .build()

  override def expressionTree(context: RuleContext): StatusTreeExpression = {

    // This is the value used to evaluate this rule
    val value = TimeSeriesExpression[Double](metricName).last

    // This is the threshold value for the rule condition
    val thresholdValue = getParameter(thresholdParameter)

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), metaCondition),

      StatusTreeExpression(
        // The time-series we check the test condition against:
        SelectTimeSeriesExpression[Double](context.tsDao, Set(metricName), denseOnly = false),

        // The condition which, if true, we have an issue. Checked against the time-series we've collected
        generateCompareCondition(
          thresholdDirection,
          value,
          thresholdValue)

        // The Alert Item to add for this specific item
      ).withRootInfo(
        getHeadline(),
        scopableStringFormatExpression(alertDescriptionFormat, DivExpression(value, unitConverter)),
        ConditionalRemediationSteps(baseRemediationText, vendorToRemediationText: _*)
      ).asCondition()
    ).withoutInfo()
  }
}

object NumericThresholdOnDoubleMetricTemplateRule {
  private val DefaultSeverity = AlertSeverity.ERROR
  private val DefaultMetaCondition: TagsStoreCondition = True
  private val DefaultUnitConverter = ConstantExpression[Option[Double]](Some(1.0))
  private val DefaultThresholdDirection = ThresholdDirection.ABOVE
  private val DefaultRuleCategories: Set[RuleCategory] = Set()
  private val DefaultDeviceCategory = DeviceCategory.AllDevices

  @throws[YamlRuleParseException]
  def fromYaml(yaml: YamlObject, utils: YamlRuleProcessorUtils): NumericThresholdOnDoubleMetricTemplateRule = {
    import utils._
    val AllFields = Set(RuleNameField, RuleFriendlyNameField, RuleDescriptionField, SeverityField,
      MetricNameField, ThresholdField, MetaConditionField, AlertDescriptionFormatField, UnitConverterDoubleValueField,
      BaseRemediationTextField, ThresholdDirectionField, RuleCategoriesField, DeviceCategoryField,
      VendorToRemediationTextField
    )
    validateNoExtraFields(yaml, AllFields)

    val severity = readOptional(yaml, SeverityField, alertSeverityParser).getOrElse(DefaultSeverity)
    val metaCondition = readOptional(yaml, MetaConditionField, metaConditionParser).getOrElse(DefaultMetaCondition)
    val unitConverter = readOptional(yaml, UnitConverterDoubleValueField, doubleParser).map(d => ConstantExpression[Option[Double]](Some(d))).getOrElse(DefaultUnitConverter)
    val thresholdDirection = readOptional(yaml, ThresholdDirectionField, thresholdDirectionParser).getOrElse(DefaultThresholdDirection)
    val ruleCategories = readOptionalSet(yaml, RuleCategoriesField, ruleCategoryParser).getOrElse(DefaultRuleCategories)
    val deviceCategory = readOptional(yaml, DeviceCategoryField, deviceCategoryParser).getOrElse(DefaultDeviceCategory)

    new NumericThresholdOnDoubleMetricTemplateRule(
      ruleName = readMandatory(yaml, RuleNameField, stringParser),
      ruleFriendlyName = readMandatory(yaml, RuleFriendlyNameField, stringParser),
      ruleDescription = readMandatory(yaml, RuleDescriptionField, stringParser),
      severity = severity,
      metricName = readMandatory(yaml, MetricNameField, stringParser),
      threshold = readMandatory(yaml, ThresholdField, doubleParser),
      metaCondition = metaCondition,
      alertDescriptionFormat = readMandatory(yaml, AlertDescriptionFormatField, stringParser),
      unitConverter = unitConverter,
      baseRemediationText = readMandatory(yaml, BaseRemediationTextField, stringParser),
      thresholdDirection = thresholdDirection,
      ruleCategories = ruleCategories,
      deviceCategory = deviceCategory
    )(readOptionalVendorToRemediationText(yaml, VendorToRemediationTextField): _*)
  }
}
