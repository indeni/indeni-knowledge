package com.indeni.server.rules.library.templates

import com.indeni.ruleengine.expressions.conditions.Change
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.server.common.data.conditions.{TagsStoreCondition, True}
import com.indeni.server.rules._
import com.indeni.server.rules.library.templates.ClusterMemberNoLongerActiveTemplateRule.{DefaultFriendlyName, DefaultMetaCondition}
import com.indeni.server.rules.library.yamlrules.YamlRuleProcessorUtils._
import com.indeni.server.rules.library.yamlrules.{YamlRuleParseException, YamlRuleProcessorUtils}
import com.indeni.server.rules.library.{ConditionalRemediationSteps, PerDeviceRule, RuleHelper}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import net.jcazevedo.moultingyaml.YamlObject

class ClusterMemberNoLongerActiveTemplateRule(ruleName: String,
                                              ruleDescription: String,
                                              severity: AlertSeverity = ClusterMemberNoLongerActiveTemplateRule.DefaultSeverity,
                                              metricName: String,
                                              applicableMetricTag: String,
                                              alertRemediationSteps: String,
                                              friendlyName: String = DefaultFriendlyName,
                                              metaCondition: TagsStoreCondition = DefaultMetaCondition)
                                             (vendorToRemediationText: (RemediationStepCondition, String)*)
  extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata
    .builder(ruleName,
      friendlyName,
      ruleDescription,
      severity,
      categories= Set(RuleCategory.HighAvailability),
      deviceCategory = DeviceCategory.ClusteredDevices)
    .build()

  override def expressionTree(context: RuleContext): StatusTreeExpression = {
    val stateTs = TimeSeriesExpression[Double](metricName)

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), metaCondition),

      StatusTreeExpression(

        // The additional tags we care about (we'll be including this in alert data)
        SelectTagsExpression(context.tsDao, Set(applicableMetricTag), withTagsCondition(metricName)),

        StatusTreeExpression(
          // The time-series we check the test condition against:
          SelectTimeSeriesExpression[Double](context.tsDao, Set(metricName), denseOnly = false),

          // The condition which, if true, we have an issue. Checked against the time-series we've collected
          Change(
            stateTs,
            ConstantExpression(Some(1.0)),
            ConstantExpression(Some(0.0)))

          // The Alert Item to add for this specific item
        ).withSecondaryInfo(
          scopableStringFormatExpression("${scope(\"" + applicableMetricTag + "\")}"),
          EMPTY_STRING,
          title = "Clustering Mechanisms Affected"
        ).asCondition()
      ).withoutInfo().asCondition()
    ).withRootInfo(
      getHeadline(),
      ConstantExpression("This cluster member used to be Active. It no longer is. Failover may have occurred"),
      ConditionalRemediationSteps(alertRemediationSteps, vendorToRemediationText:_*)
    )
  }
}


object ClusterMemberNoLongerActiveTemplateRule {
  private val DefaultMetaCondition: TagsStoreCondition = True
  private val DefaultFriendlyName: String ="Cluster member no longer Active"
  private val DefaultSeverity = AlertSeverity.ERROR
  private val DefaultRuleDescription = "indeni will trigger an issue if the cluster member used to be Active and is now Standby or down."
  private val DefaultAlertRemediationSteps = "Review the possible cause and troubleshoot."

  @throws[YamlRuleParseException]
  def fromYaml(yaml: YamlObject, utils: YamlRuleProcessorUtils): ClusterMemberNoLongerActiveTemplateRule = {
    import utils._
    val AllFields = Set(RuleNameField, MetricNameField, MetaConditionField, ApplicableMetricTagField,
      SeverityField, RuleFriendlyNameField, BaseRemediationTextField, AlertDescriptionField,
      AlertRemediationStepsField, VendorToRemediationTextField)
    validateNoExtraFields(yaml, AllFields)
    val severity = readOptional(yaml, SeverityField, alertSeverityParser).getOrElse(DefaultSeverity)

    val metaCondition = readOptional(yaml, MetaConditionField, metaConditionParser).getOrElse(DefaultMetaCondition)
    new ClusterMemberNoLongerActiveTemplateRule(
      ruleName = readMandatory(yaml, RuleNameField, stringParser),
      ruleDescription = readOptional(yaml, RuleDescriptionField, stringParser).getOrElse(DefaultRuleDescription),
      severity = severity,
      metricName = readMandatory(yaml, MetricNameField, stringParser),
      alertRemediationSteps = readOptional(yaml, AlertRemediationStepsField, stringParser).getOrElse(DefaultAlertRemediationSteps),
      friendlyName = readOptional(yaml, RuleFriendlyNameField, stringParser).getOrElse(DefaultFriendlyName),
      applicableMetricTag = readMandatory(yaml, ApplicableMetricTagField, stringParser),
      metaCondition = metaCondition
    )(readOptionalVendorToRemediationText(yaml, VendorToRemediationTextField): _*)
  }
}
