package com.indeni.server.rules.library.templates

import com.indeni.ruleengine.expressions.conditions.GreaterThanOrEqual
import com.indeni.ruleengine.expressions.core._
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.ruleengine.expressions.math.{DivExpression, TimesExpression}
import com.indeni.server.common.data.conditions.{Equals, True}
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules._
import com.indeni.server.rules.library.templates.DataplanePoolUsageTemplateRule.DefaultRuleCategories
import com.indeni.server.rules.library.yamlrules.YamlRuleProcessorUtils._
import com.indeni.server.rules.library.yamlrules.{YamlRuleParseException, YamlRuleProcessorUtils}
import com.indeni.server.rules.library.{ConditionalRemediationSteps, PerDeviceRule, RuleHelper}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import net.jcazevedo.moultingyaml.YamlObject
class DataplanePoolUsageTemplateRule(poolName: String,
                                     usageThreshold: Double,
                                     ruleCategories:Set[RuleCategory] = DefaultRuleCategories,
                                    ) extends PerDeviceRule with RuleHelper {

  private[library] val highThresholdParameterName = "High_Threshold_of_Pool_usage"

  private val highThresholdParameter = new ParameterDefinition(highThresholdParameterName,
    "",
    "High Threshold of Pool Usage",
    "What is the threshold for the license usage for which once it is crossed an alert will be issued.",
    UIType.DOUBLE,
    usageThreshold)

  override val metadata: RuleMetadata = RuleMetadata.builder("panw_pool_usage_" + poolName.replaceAll("[^A-Za-z0-9]", ""), poolName + " dataplane pool utilization high",
    "The dataplane of a Palo Alto Networks firewall has several pools, each with a different role. indeni will alert when a pool is near exhaustion.", AlertSeverity.ERROR, ruleCategories, deviceCategory = DeviceCategory.PaloAltoNetworksFirewalls).configParameter(highThresholdParameter).build()

  override def expressionTree(context: RuleContext): StatusTreeExpression = {
    val inUseValue = TimeSeriesExpression[Double]("dataplane-pool-used").last
    val limitValue = TimeSeriesExpression[Double]("dataplane-pool-limit").last

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
        StatusTreeExpression(
          // The time-series we check the test condition against:
          SelectTimeSeriesExpression[Double](context.tsDao, Set("dataplane-pool-used", "dataplane-pool-limit"), condition = Equals("name", poolName), denseOnly = false),

          // The condition which, if true, we have an issue. Checked against the time-series we've collected
          GreaterThanOrEqual(
            inUseValue,
            TimesExpression[Double](limitValue, DivExpression[Double](getParameterDouble(highThresholdParameter), ConstantExpression[Option[Double]](Some(100.0)))))

        ).withRootInfo(
            getHeadline(),
            scopableStringFormatExpression("There are %.0f elements used from this pool where the limit is %.0f. This may impact firewall performance.", inUseValue, limitValue),
            ConditionalRemediationSteps("Please refer to Palo Alto Networks KB article: https://knowledgebase.paloaltonetworks.com/KCSArticleDetail?id=kA10g000000ClRTCA0")
        ).asCondition()
    ).withoutInfo()
  }
}

object DataplanePoolUsageTemplateRule {
  private val DefaultRuleCategories: Set[RuleCategory] = Set(RuleCategory.HealthChecks)

  @throws[YamlRuleParseException]
  def fromYaml(yaml: YamlObject, utils: YamlRuleProcessorUtils): DataplanePoolUsageTemplateRule = {
    import utils._
    val AllFields = Set(PoolNameField, UsageThresholdField, RuleCategoriesField)
    val ruleCategories = readOptionalSet(yaml, RuleCategoriesField, ruleCategoryParser).getOrElse(DefaultRuleCategories)
    validateNoExtraFields(yaml, AllFields)
    new DataplanePoolUsageTemplateRule(
      poolName = readMandatory(yaml, PoolNameField, stringParser),
      usageThreshold = readMandatory(yaml, UsageThresholdField, doubleParser),
      ruleCategories = ruleCategories
    )
  }
}
