package com.indeni.server.rules.library.templates
import com.indeni.ruleengine.expressions.conditions
import com.indeni.ruleengine.expressions.conditions.{And, _}
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data._
import com.indeni.server.common.data.conditions.{False, TagsStoreCondition, True}
import com.indeni.server.rules._
import com.indeni.server.rules.library._
import com.indeni.server.rules.library.templates.MultiConditionTimeSeriesTemplateRule._
import com.indeni.server.rules.library.yamlrules.YamlRuleProcessorUtils._
import com.indeni.server.rules.library.yamlrules.{YamlRuleParseException, YamlRuleProcessorUtils}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import net.jcazevedo.moultingyaml.YamlObject

class MultiConditionTimeSeriesTemplateRule(ruleName: String,
                                           ruleFriendlyName: String,
                                           ruleDescription: String,
                                           severity: AlertSeverity = DefaultSeverity,
                                           metricsNames: List[String],
                                           applicableMetricTag: String = DefaultApplicableMetricTag,
                                           alertItemsHeader: String = DefaultAlertItemsHeader,
                                           descriptionMetricTag: String = DefaultDescriptionMetricTag,
                                           alertDescription: String,
                                           baseRemediationText: String,
                                           metaCondition: TagsStoreCondition = DefaultMetaCondition,
                                           ruleCategories:Set[RuleCategory] = DefaultRuleCategories,
                                           deviceCategory: DeviceCategory = DefaultDeviceCategory,
                                           complexCondition: Condition,
                                           issueItemsMetricName: String = DefaultIssueItemsMetricsNames,
                                           issueItemsComplexCondition: Condition = DefaultIssueItemsComplexCondition)
                                          (vendorToRemediationText: (RemediationStepCondition, String)*)  extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata.builder(ruleName, ruleFriendlyName, ruleDescription, severity, ruleCategories,deviceCategory = deviceCategory).build()
  override def expressionTree(context: RuleContext): StatusTreeExpression = {

    val issueCondition = StatusTreeExpression(
      // The time-series we check the test condition against:
      SelectTimeSeriesExpression[Double](context.tsDao, metricsNames.toSet, denseOnly = false),

      // The condition which, if true, we have an issue. Checked against the time-series we've collected
      complexCondition
    ).withoutInfo().asCondition()

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), metaCondition),

      // What constitutes an issue
      if (applicableMetricTag != null)
        And(
          issueCondition,
          StatusTreeExpression(
            // The additional tags we care about (we'll be including this in alert data)
            SelectTagsExpression(context.tsDao, if (descriptionMetricTag == null) Set(applicableMetricTag) else Set(applicableMetricTag, descriptionMetricTag), withTagsCondition(issueItemsMetricName)),
            StatusTreeExpression(
              // The time-series we check the test condition against:
              SelectTimeSeriesExpression[Double](context.tsDao, Set(issueItemsMetricName), denseOnly = false),

              // The condition which, if true, we have an issue. Checked against the time-series we've collected
              issueItemsComplexCondition
          ).withSecondaryInfo(
              scopableStringFormatExpression("${scope(\"" + applicableMetricTag + "\")}" + (if (descriptionMetricTag != null) " (${scope(\"" + descriptionMetricTag + "\")})" else "")),
              EMPTY_STRING,
              title = alertItemsHeader
            ).asCondition()
        ).withoutInfo().asCondition())
      else {
        issueCondition
      }
      // Details of the alert itself
    ).withRootInfo(
      getHeadline(),
      ConstantExpression(alertDescription),
      ConditionalRemediationSteps(baseRemediationText, vendorToRemediationText: _*)
    )
  }
}

object MultiConditionTimeSeriesTemplateRule {
  private val DefaultSeverity = AlertSeverity.ERROR
  private val DefaultAlertItemsHeader: String = null
  private val DefaultApplicableMetricTag: String = null
  private val DefaultDescriptionMetricTag: String = null
  private val DefaultMetaCondition: TagsStoreCondition = True
  private val DefaultRuleCategories: Set[RuleCategory] = Set()
  private val DefaultIssueItemsMetricsNames: String = null
  private val DefaultIssueItemsComplexCondition: Condition = null
  private val DefaultDeviceCategory = DeviceCategory.AllDevices

  @throws[YamlRuleParseException]
  def fromYaml(yaml: YamlObject, utils: YamlRuleProcessorUtils): MultiConditionTimeSeriesTemplateRule = {
    import utils._
    val AllFields = Set(RuleNameField, RuleFriendlyNameField, RuleDescriptionField, SeverityField, MetricsNamesField,
      ApplicableMetricTagField, AlertItemsHeaderField, DescriptionMetricTagField,  AlertDescriptionField, BaseRemediationTextField,
      MetaConditionField, RuleCategoriesField, DeviceCategoryField, VendorToRemediationTextField, ComplexConditionField, IssueItemsMetricName, IssueItemsComplexConditionField
    )
    validateNoExtraFields(yaml, AllFields)

    new MultiConditionTimeSeriesTemplateRule(
      ruleName = readMandatory(yaml, RuleNameField, stringParser),
      ruleFriendlyName = readMandatory(yaml, RuleFriendlyNameField, stringParser),
      ruleDescription = readMandatory(yaml, RuleDescriptionField, stringParser),
      severity = readOptional(yaml, SeverityField, alertSeverityParser).getOrElse(DefaultSeverity),
      metricsNames = readMandatoryList(yaml, MetricsNamesField, stringParser),
      applicableMetricTag = readOptional(yaml, ApplicableMetricTagField, stringParser).getOrElse(DefaultApplicableMetricTag),
      alertItemsHeader = readOptional(yaml, AlertItemsHeaderField, stringParser).getOrElse(DefaultAlertItemsHeader),
      descriptionMetricTag = readOptional(yaml, DescriptionMetricTagField, stringParser).getOrElse(DefaultDescriptionMetricTag),
      alertDescription = readMandatory(yaml, AlertDescriptionField, stringParser),
      baseRemediationText = readMandatory(yaml, BaseRemediationTextField, stringParser),
      metaCondition = readOptional(yaml, MetaConditionField, metaConditionParser).getOrElse(DefaultMetaCondition),
      ruleCategories = readOptionalSet(yaml, RuleCategoriesField, ruleCategoryParser).getOrElse(DefaultRuleCategories),
      deviceCategory = readOptional(yaml, DeviceCategoryField, deviceCategoryParser).getOrElse(DefaultDeviceCategory),
      complexCondition = readMandatory(yaml, ComplexConditionField, complexConditionParser),
      issueItemsMetricName = readOptional(yaml, IssueItemsMetricName, stringParser).getOrElse(DefaultIssueItemsMetricsNames),
      issueItemsComplexCondition = readOptional(yaml, IssueItemsComplexConditionField, complexConditionParser).getOrElse(
        DefaultIssueItemsComplexCondition)
    )(readOptionalVendorToRemediationText(yaml, VendorToRemediationTextField): _*)
  }
}
