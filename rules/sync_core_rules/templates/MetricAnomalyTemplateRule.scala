package com.indeni.server.rules.library.templates

import com.indeni.ruleengine.expressions.conditions
import com.indeni.ruleengine.expressions.core.{ConstantExpression, StatusTreeExpression}
import com.indeni.ruleengine.expressions.data._
import com.indeni.server.common.data.conditions.{Equals, TagsStoreCondition, True}
import com.indeni.server.rules.library._
import com.indeni.server.rules.library.yamlrules.YamlRuleProcessorUtils._
import com.indeni.server.rules.library.yamlrules.{YamlRuleParseException, YamlRuleProcessorUtils}
import com.indeni.server.rules._
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import net.jcazevedo.moultingyaml.YamlObject

case class MetricAnomalyTemplateRule(ruleName: String,
                                     friendlyName: String,
                                     ruleDescriptionFormat: String,
                                     measurement: String,
                                     applicableMetricTagValue: String,
                                     applicableMetricTag: String,
                                     metricType: String,
                                     anomalyTitle: String,
                                     alertDescriptionFormat: String,
                                     baseRemediationText: String,
                                     metaCondition: Option[TagsStoreCondition],
                                     ruleCategories: Set[RuleCategory],
                                     deviceCategory: DeviceCategory,
                                     alertSeverity: AlertSeverity,
                                     analysisPeriod: String = "1 week")
    extends PerDeviceRule
    with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata
    .builder(
      ruleName,
      friendlyName,
      String.format(ruleDescriptionFormat, analysisPeriod),
      alertSeverity,
      categories = ruleCategories,
      deviceCategory = deviceCategory
    )
    .build()

  val mostRecentAnomaly = TimeSeriesExpression[Double](measurement).head

  override def expressionTree(context: RuleContext): StatusTreeExpression = {

    // Build the StatusTreeExpression using the statuses
    StatusTreeExpression(

      SelectTagsExpression(context.metaDao, Set(DeviceKey), metaCondition.getOrElse(True)),

      StatusTreeExpression(

        // The time-series we check the test condition against:
        SelectTimeSeriesExpression[Double](context.sqlMetricsDao, Set(measurement),
          condition = Equals(applicableMetricTag, applicableMetricTagValue), denseOnly = metricType == MetricAnomalyTemplateRule.MetricTypeCounter),

        // Issue conditions which when it is true we have an issue
        // we have an issue if there are any anomalies
        conditions.ResultsFound(TimeSeriesExpression[Double](measurement))

        // The Alert Item to add for this specific item
      ).withSecondaryInfo(
        getHeadline(),
        scopableStringFormatExpression(alertDescriptionFormat, mostRecentAnomaly),
        title = anomalyTitle
      ).asCondition()
    ).withRootInfo(
      getHeadline(),
      ConstantExpression(""),
      ConditionalRemediationSteps(baseRemediationText)
    )
  }
}

object MetricAnomalyTemplateRule {

  private val DefaultRuleCategories: Set[RuleCategory] = Set(RuleCategory.HealthChecks)
  private val ApplicableMetricTagValueField = "applicable_metric_tag_value"
  private val AnomalyTitleField = "anomaly_title"
  private val RuleDescriptionFormatField = "rule_description_format"
  private val MetricTypeCounter = "counter"
  private val MetricTypeGauge = "gauge"

  @throws[YamlRuleParseException]
  def fromYaml(yaml: YamlObject, utils: YamlRuleProcessorUtils): MetricAnomalyTemplateRule = {
    import utils._
    val AllFields = Set(
      RuleNameField,
      RuleFriendlyNameField,
      MetricNameField,
      MetricTypeField,
      RuleDescriptionFormatField,
      ApplicableMetricTagField,
      MetaConditionField,
      ApplicableMetricTagValueField,
      BaseRemediationTextField,
      RuleCategoriesField,
      SeverityField,
      AnomalyTitleField,
      AlertDescriptionFormatField,
      DeviceCategoryField)

    validateNoExtraFields(yaml, AllFields)

    MetricAnomalyTemplateRule(
      ruleName = readMandatory(yaml, RuleNameField, stringParser),
      friendlyName = readMandatory(yaml, RuleFriendlyNameField, stringParser),
      ruleDescriptionFormat = readMandatory(yaml, RuleDescriptionFormatField, stringParser),
      metaCondition = readOptional(yaml, MetaConditionField, metaConditionParser),
      measurement = readMandatory(yaml, MetricNameField, stringParser),
      metricType = readMandatory(yaml, MetricTypeField, stringParser),
      applicableMetricTag = readMandatory(yaml, ApplicableMetricTagField, stringParser),
      applicableMetricTagValue = readMandatory(yaml, ApplicableMetricTagValueField, stringParser),
      baseRemediationText = readMandatory(yaml, BaseRemediationTextField, stringParser),
      ruleCategories = readOptionalSet(yaml, RuleCategoriesField, ruleCategoryParser).getOrElse(DefaultRuleCategories),
      deviceCategory = readMandatory(yaml, DeviceCategoryField, deviceCategoryParser),
      alertSeverity = readMandatory(yaml, SeverityField, alertSeverityParser),
      anomalyTitle = readMandatory(yaml, AnomalyTitleField, stringParser),
      alertDescriptionFormat = readMandatory(yaml, AlertDescriptionFormatField, stringParser)
    )
  }
}
