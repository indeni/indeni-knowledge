package com.indeni.server.rules.library.templates

import com.indeni.ruleengine.Scope.{Scope, ImplicitScopeValueHelper}
import com.indeni.ruleengine.expressions.Expression
import com.indeni.ruleengine.expressions.conditions.{And, ConditionHelper, GreaterThanOrEqual, Or, ResultsFound}
import com.indeni.ruleengine.expressions.core._
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.ruleengine.expressions.math.{DivExpression, MaxExpression, MinExpression, TimesExpression}
import com.indeni.ruleengine.expressions.regex.RegexExpression
import com.indeni.ruleengine.expressions.scope.{ScopableExpression, ScopeValueExpression}
import com.indeni.server.common.data.conditions.{TagsStoreCondition, True}
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules.library._
import com.indeni.server.rules.library.templates.NearingCapacityWithItemsTemplateRule._
import com.indeni.server.rules.library.yamlrules.YamlRuleProcessorUtils._
import com.indeni.server.rules.library.yamlrules.{YamlRuleParseException, YamlRuleProcessorUtils}
import com.indeni.server.rules.{RuleContext, ThresholdDirection, _}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import net.jcazevedo.moultingyaml.YamlObject

import scala.util.matching.Regex

/**
  * Similar to NearingCapacityTemplateRule, except it supports having specific items in the alert.
  */
class NearingCapacityWithItemsTemplateRule(ruleName: String,
                                           ruleFriendlyName: String,
                                           ruleDescription: String,
                                           severity: AlertSeverity = DefaultSeverity,
                                           historyLength: Option[Int] = None,
                                           usageMetricName: String,
                                           limitMetricName: String = DefaultLimitMetricName,
                                           threshold: Double,
                                           metaCondition: TagsStoreCondition = DefaultMetaCondition,
                                           applicableMetricTag: String,
                                           alertItemDescriptionFormat: String,
                                           alertDescription: String,
                                           baseRemediationText: String,
                                           thresholdDirection: ThresholdDirection = DefaultThresholdDirection,
                                           minimumValueToAlert: Double = DefaultMinimumValueToAlert,
                                           alertItemsHeader: String,
                                           itemSpecificDescription: Seq[(Regex, String)] = DefaultItemSpecificDescription,
                                           itemsToIgnore: Set[Regex] = DefaultItemsToIgnore,
                                           ruleCategories:Set[RuleCategory] = DefaultRuleCategories,
                                           deviceCategory : DeviceCategory = DefaultDeviceCategory)
                                          (vendorToRemediationText: (RemediationStepCondition, String)*)
  extends PerDeviceRule with RuleHelper {

  private[library] val highThresholdParameterName = "High_Threshold_of_Store_use"
  private val highThresholdParameter = new ParameterDefinition(highThresholdParameterName,
    "",
    "High Threshold of Usage",
    "indeni will evaluate the current utilization vs the limit and trigger an issue if the percentage of usage crosses this number.",
    UIType.DOUBLE,
    threshold)

  override val metadata: RuleMetadata = RuleMetadata.builder(ruleName, ruleFriendlyName, ruleDescription, severity, ruleCategories,deviceCategory).configParameter(highThresholdParameter).build()

  override def expressionTree(context: RuleContext): StatusTreeExpression = {

    val inUseValue = historyLength match {
      case None => TimeSeriesExpression[Double](usageMetricName).last
      case Some(_) if thresholdDirection == ThresholdDirection.ABOVE => MinExpression(TimeSeriesExpression[Double](usageMetricName), historyLength)
      case Some(_) if thresholdDirection == ThresholdDirection.BELOW => MaxExpression(TimeSeriesExpression[Double](usageMetricName), historyLength)
    }

    val limitValue = if (null != limitMetricName) TimeSeriesExpression[Double](limitMetricName).last else ConstantExpression(Some(100.0))

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), metaCondition),

      StatusTreeExpression(
        // The additional tags we care about (we'll be including this in alert data)
        SelectTagsExpression(context.tsDao, Set(applicableMetricTag), withTagsCondition(usageMetricName, limitMetricName)),

        StatusTreeExpression(
          // The time-series we check the test condition against:
          SelectTimeSeriesExpression[Double](context.tsDao, if (null != limitMetricName) Set(usageMetricName, limitMetricName) else Set(usageMetricName), denseOnly = false),

          // The condition which, if true, we have an issue. Checked against the time-series we've collected
          And(
            generateCompareCondition(
              thresholdDirection,
              inUseValue,
              TimesExpression[Double](limitValue, DivExpression[Double](getParameterDouble(highThresholdParameter), ConstantExpression[Option[Double]](Some(100.0))))),
            GreaterThanOrEqual(inUseValue, ConstantExpression(Some(minimumValueToAlert))),
            Or(
              itemsToIgnore.map(r => ResultsFound(RegexExpression(ScopeValueExpression(applicableMetricTag).visible().asString(), r))).toSeq
            ).not)

          // The Alert Item to add for this specific item
        ).withSecondaryInfo(
          scopableStringFormatExpression("${scope(\"" + applicableMetricTag + "\")}"),
          new ScopableExpression[String] {
            val innerDescription = scopableStringFormatExpression(alertItemDescriptionFormat, inUseValue, limitValue)

            override protected def evalWithScope(time: Long, scope: Scope): String = {
              val metricTagValue = scope.getVisible(applicableMetricTag).get.toString
              innerDescription.eval(time) + itemSpecificDescription.collectFirst {
                case item if (!item._1.findFirstMatchIn(metricTagValue).isEmpty) => item._2
              }.get
            }

            override def args: Set[Expression[_]] = Set(innerDescription)
          },
          title = alertItemsHeader
        ).asCondition()
      ).withoutInfo().asCondition()
    ).withRootInfo(
      getHeadline(),
      ConstantExpression(alertDescription),
      ConditionalRemediationSteps(baseRemediationText, vendorToRemediationText: _*)
    )
  }
}

object NearingCapacityWithItemsTemplateRule {
  private val DefaultSeverity = AlertSeverity.ERROR
  private val DefaultLimitMetricName: String = null
  private val DefaultMetaCondition: TagsStoreCondition = True
  private val DefaultThresholdDirection = ThresholdDirection.ABOVE
  private val DefaultMinimumValueToAlert: Double = 0
  private val DefaultItemSpecificDescription = Seq(".*".r -> "")
  private val DefaultItemsToIgnore = Set("^$".r)
  private val DefaultRuleCategories: Set[RuleCategory] = Set()
  private val DefaultDeviceCategory = DeviceCategory.AllDevices

  @throws[YamlRuleParseException]
  def fromYaml(yaml: YamlObject, utils: YamlRuleProcessorUtils): NearingCapacityWithItemsTemplateRule = {
    import utils._
    val AllFields = Set(RuleNameField, RuleFriendlyNameField, RuleDescriptionField, SeverityField,
      UsageMetricNameField, LimitMetricNameField, ThresholdField, MetaConditionField,
      ApplicableMetricTagField, AlertItemDescriptionFormatField, AlertDescriptionField, BaseRemediationTextField,
      ThresholdDirectionField, MinimumValueToAlertField, AlertItemsHeaderField, ItemSpecificDescriptionField,
      ItemsToIgnoreField, RuleCategoriesField, DeviceCategoryField, VendorToRemediationTextField, HistoryLengthField
    )
    validateNoExtraFields(yaml, AllFields)

    val severity = readOptional(yaml, SeverityField, alertSeverityParser).getOrElse(DefaultSeverity)
    val historyLength = readOptional(yaml, HistoryLengthField, intParser)
    val limitMetricName = readOptional(yaml, LimitMetricNameField, stringParser).getOrElse(DefaultLimitMetricName)
    val metaCondition = readOptional(yaml, MetaConditionField, metaConditionParser).getOrElse(DefaultMetaCondition)
    val thresholdDirection = readOptional(yaml, ThresholdDirectionField, thresholdDirectionParser).getOrElse(DefaultThresholdDirection)
    val minimumValueToAlert = readOptional(yaml, MinimumValueToAlertField, doubleParser).getOrElse(DefaultMinimumValueToAlert)
    val itemSpecificDescription = readOptionalList(yaml, ItemSpecificDescriptionField, regexStringPairTypeParser).getOrElse(DefaultItemSpecificDescription)
    val itemsToIgnore = readOptionalSet(yaml, ItemsToIgnoreField, regexTypeParser).getOrElse(DefaultItemsToIgnore)
    val ruleCategories = readOptionalSet(yaml, RuleCategoriesField, ruleCategoryParser).getOrElse(DefaultRuleCategories)
    val deviceCategory = readOptional(yaml, DeviceCategoryField, deviceCategoryParser).getOrElse(DefaultDeviceCategory)

    new NearingCapacityWithItemsTemplateRule(
      ruleName = readMandatory(yaml, RuleNameField, stringParser),
      ruleFriendlyName = readMandatory(yaml, RuleFriendlyNameField, stringParser),
      ruleDescription = readMandatory(yaml, RuleDescriptionField, stringParser),
      severity = severity,
      historyLength = historyLength,
      usageMetricName = readMandatory(yaml, UsageMetricNameField, stringParser),
      limitMetricName = limitMetricName,
      threshold = readMandatory(yaml, ThresholdField, doubleParser),
      metaCondition = metaCondition,
      applicableMetricTag = readMandatory(yaml, ApplicableMetricTagField, stringParser),
      alertItemDescriptionFormat = readMandatory(yaml, AlertItemDescriptionFormatField, stringParser),
      alertDescription = readMandatory(yaml, AlertDescriptionField, stringParser),
      baseRemediationText = readMandatory(yaml, BaseRemediationTextField, stringParser),
      thresholdDirection = thresholdDirection,
      minimumValueToAlert = minimumValueToAlert,
      alertItemsHeader = readMandatory(yaml, AlertItemsHeaderField, stringParser),
      itemSpecificDescription = itemSpecificDescription,
      itemsToIgnore = itemsToIgnore,
      ruleCategories = ruleCategories,
      deviceCategory = deviceCategory
    )(readOptionalVendorToRemediationText(yaml, VendorToRemediationTextField): _*)
  }
}
