package com.indeni.server.rules.library.templates
import com.indeni.server.common.data.conditions.TagsStoreCondition
import com.indeni.server.rules.library.templates.EnabledInterfaceSnapshotComparisonTemplateRule._
import com.indeni.server.rules.library.yamlrules.{YamlRuleParseException, YamlRuleProcessorUtils}
import com.indeni.server.rules.library.yamlrules.YamlRuleProcessorUtils._
import com.indeni.server.rules.{DeviceCategory, RemediationStepCondition, RuleCategory, RuleContext}
import net.jcazevedo.moultingyaml.YamlObject

class EnabledInterfaceSnapshotComparisonTemplateRule(ruleName: String,
                                                     ruleFriendlyName: String,
                                                     ruleDescription: String,
                                                     metricName: String,
                                                     isArray: Boolean,
                                                     applicableMetricTag: String = DefaultApplicableMetricTag,
                                                     alertDescription: String,
                                                     descriptionMetricTag: String = DefaultDescriptionMetricTag,
                                                     descriptionStringFormat: String = DefaultDescriptionStringFormat,
                                                     baseRemediationText: String,
                                                     metaCondition: TagsStoreCondition,
                                                     vendorToRemediationText: Seq[(RemediationStepCondition, String)] = Seq.empty,
                                                     ruleCategories : Set[RuleCategory] = DefaultRuleCategories,
                                                     deviceCategory: DeviceCategory = DefaultDeviceCategory)
    extends SnapshotComparisonTemplateRule(
      ruleName = ruleName,
      ruleFriendlyName = ruleFriendlyName,
      ruleDescription = ruleDescription,
      metricName = metricName,
      isArray = isArray,
      applicableMetricTag = applicableMetricTag,
      alertDescription = alertDescription,
      descriptionMetricTag = descriptionMetricTag,
      baseRemediationText = baseRemediationText,

      metaCondition = metaCondition,
      ruleCategories = ruleCategories,
      deviceCategory = deviceCategory
    )(vendorToRemediationText: _*) {
  override def deviceCondition(context: RuleContext) = generateNetworkInterfaceAdminStateCondition(context.tsDao, 0.0)
}

object EnabledInterfaceSnapshotComparisonTemplateRule {
  private val DefaultRuleCategories: Set[RuleCategory] = Set()
  private val DefaultDeviceCategory = DeviceCategory.AllDevices
  private val DefaultApplicableMetricTag: String = null
  private val DefaultDescriptionMetricTag = null
  private val DefaultDescriptionStringFormat = ""


  @throws[YamlRuleParseException]
  def fromYaml(yaml: YamlObject, utils: YamlRuleProcessorUtils): EnabledInterfaceSnapshotComparisonTemplateRule = {
    import utils._
    val AllFields = Set(
      RuleNameField, RuleFriendlyNameField, RuleDescriptionField, MetricNameField, IsArrayField, ApplicableMetricTagField,
      AlertDescriptionField, DescriptionMetricTagField, DescriptionStringFormatField, BaseRemediationTextField,
      MetaConditionField, RuleCategoriesField, DeviceCategoryField, VendorToRemediationTextField)

    validateNoExtraFields(yaml, AllFields)
    val applicableMetricTag = readOptional(yaml, ApplicableMetricTagField, stringParser).getOrElse(DefaultApplicableMetricTag)
    val descriptionMetricTag = readOptional(yaml, DescriptionMetricTagField, stringParser).getOrElse(DefaultDescriptionMetricTag)
    val descriptionStringFormat = readOptional(yaml, DescriptionStringFormatField, stringParser).getOrElse(DefaultDescriptionStringFormat)
    val ruleCategories = readOptionalSet(yaml, RuleCategoriesField, ruleCategoryParser).getOrElse(DefaultRuleCategories)
    val deviceCategory = readOptional(yaml, DeviceCategoryField, deviceCategoryParser).getOrElse(DefaultDeviceCategory)

    new EnabledInterfaceSnapshotComparisonTemplateRule(
      ruleName = readMandatory(yaml, RuleNameField, stringParser),
      ruleFriendlyName = readMandatory(yaml, RuleFriendlyNameField, stringParser),
      ruleDescription = readMandatory(yaml, RuleDescriptionField, stringParser),
      metricName = readMandatory(yaml, MetricNameField, stringParser),
      isArray = readMandatory(yaml, IsArrayField, booleanParser),
      applicableMetricTag = applicableMetricTag,
      alertDescription = readMandatory(yaml, AlertDescriptionField, stringParser),
      descriptionMetricTag = descriptionMetricTag,
      descriptionStringFormat = descriptionStringFormat,
      baseRemediationText = readMandatory(yaml, BaseRemediationTextField, stringParser),
      vendorToRemediationText = readOptionalVendorToRemediationText(yaml, VendorToRemediationTextField),
      metaCondition = readMandatory(yaml, MetaConditionField, metaConditionParser),
      ruleCategories = ruleCategories,
      deviceCategory = deviceCategory
    )
  }
}
