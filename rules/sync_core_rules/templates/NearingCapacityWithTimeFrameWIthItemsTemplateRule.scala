package com.indeni.server.rules.library.templates

import com.indeni.apidata.time.TimeSpan
import com.indeni.ruleengine.expressions.conditions.{And, GreaterThanOrEqual}
import com.indeni.ruleengine.expressions.core._
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.ruleengine.expressions.math.{DivExpression, MinExpression, TimesExpression}
import com.indeni.server.common.data.conditions.{TagsStoreCondition, True}
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules.library._
import com.indeni.server.rules.library.templates.NearingCapacityWithTimeFrameWithItemsTemplateRule._
import com.indeni.server.rules.library.yamlrules.YamlRuleProcessorUtils._
import com.indeni.server.rules.library.yamlrules.{YamlRuleParseException, YamlRuleProcessorUtils}
import com.indeni.server.rules.{RuleContext, ThresholdDirection, _}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import net.jcazevedo.moultingyaml.YamlObject

class NearingCapacityWithTimeFrameWithItemsTemplateRule(
    ruleName: String,
    applicableMetricTag: String,
    ruleFriendlyName: String,
    ruleDescription: String,
    severity: AlertSeverity = DefaultSeverity,
    usageMetricName: String,
    limitMetricName: String = DefaultLimitMetricName,
    threshold: Double,
    minimumValueToAlert: Double = DefaultMinimumValueToAlert,
    metaCondition: TagsStoreCondition = DefaultMetaCondition,
    alertDescription: String,
    baseRemediationText: String,
    alertItemDescriptionFormat: String,
    alertItemsHeader: String,
    thresholdDirection: ThresholdDirection = DefaultThresholdDirection,
    ruleCategories: Set[RuleCategory] = DefaultRuleCategories,
    deviceCategory: DeviceCategory = DefaultDeviceCategory,
    timeThreshold: Long)(vendorToRemediationText: (RemediationStepCondition, String)*)
    extends PerDeviceRule
    with RuleHelper {

  private[library] val highThresholdParameterName = "High_Threshold_of_Store_use"
  private val highThresholdParameter = new ParameterDefinition(
    highThresholdParameterName,
    "",
    "High Threshold of Usage",
    "indeni will evaluate the current utilization vs the limit and triggers an issue if the percentage of usage crosses this number.",
    UIType.DOUBLE,
    threshold
  )

  private val timeThresholdParameterName = "time_frame_in_minutes"
  private val timeThresholdParameter = new ParameterDefinition(
    timeThresholdParameterName,
    "",
    "Time Threshold",
    s"The value of $usageMetricName needs to remain above the value set in " +
      "\"" + highThresholdParameterName + "\"" +
      " for this amount of time before a issue is triggered.",
    UIType.TIMESPAN,
    TimeSpan.fromMinutes(timeThreshold)
  )

  override val metadata: RuleMetadata = RuleMetadata
    .builder(ruleName, ruleFriendlyName, ruleDescription, severity, ruleCategories, deviceCategory)
    .configParameters(highThresholdParameter, timeThresholdParameter)
    .build()

  override def expressionTree(context: RuleContext): StatusTreeExpression = {

    val inUseValue = MinExpression(TimeSeriesExpression[Double](usageMetricName))
    val thresholdValue = getParameterDouble(highThresholdParameter)
    val limitValue =
      if (null != limitMetricName) TimeSeriesExpression[Double](limitMetricName).last
      else ConstantExpression(Some(100.0))

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), metaCondition),
      StatusTreeExpression(
        // The additional tags we care about (we'll be including this in alert data)
        SelectTagsExpression(context.tsDao,
                             Set(applicableMetricTag),
                             withTagsCondition(usageMetricName, limitMetricName)),
        StatusTreeExpression(
          // The time-series we check the test condition against:
          SelectTimeSeriesExpression[Double](
            context.tsDao,
            if (null != limitMetricName) Set(usageMetricName, limitMetricName) else Set(usageMetricName),
            historyLength = getParameterTimeSpanForRule(timeThresholdParameter),
            denseOnly = true
          ),
          // The condition which, if true, we have an issue. Checked against the time-series we've collected
          And(
            generateCompareCondition(
              thresholdDirection,
              inUseValue,
              TimesExpression[Double](limitValue,
                                      DivExpression[Double](getParameterDouble(highThresholdParameter),
                                                            ConstantExpression[Option[Double]](Some(100.0))))
            ),
            GreaterThanOrEqual(inUseValue, ConstantExpression(Some(minimumValueToAlert)))
          )

          // The Alert Item to add for this specific item
        ).withSecondaryInfo(
            scopableStringFormatExpression("${scope(\"" + applicableMetricTag + "\")}"),
            scopableStringFormatExpression(alertItemDescriptionFormat, inUseValue, limitValue),
            title = alertItemsHeader
          )
          .asCondition()
      ).withoutInfo().asCondition()
    ).withRootInfo(
      getHeadline(),
      ConstantExpression(alertDescription),
      ConditionalRemediationSteps(baseRemediationText, vendorToRemediationText: _*)
    )
  }
}

object NearingCapacityWithTimeFrameWithItemsTemplateRule {
  private val DefaultSeverity = AlertSeverity.ERROR
  private val DefaultLimitMetricName: String = null
  private val DefaultMinimumValueToAlert: Double = 0
  private val DefaultMetaCondition: TagsStoreCondition = True
  private val DefaultThresholdDirection = ThresholdDirection.ABOVE
  private val DefaultRuleCategories: Set[RuleCategory] = Set()
  private val DefaultDeviceCategory = DeviceCategory.AllDevices

  @throws[YamlRuleParseException]
  def fromYaml(yaml: YamlObject, utils: YamlRuleProcessorUtils): NearingCapacityWithTimeFrameWithItemsTemplateRule = {
    import utils._
    val AllFields = Set(
      RuleNameField,
      RuleFriendlyNameField,
      RuleDescriptionField,
      SeverityField,
      UsageMetricNameField,
      LimitMetricNameField,
      ThresholdField,
      TimeThresholdField,
      MinimumValueToAlertField,
      MetaConditionField,
      AlertDescriptionField,
      BaseRemediationTextField,
      ThresholdDirectionField,
      RuleCategoriesField,
      DeviceCategoryField,
      VendorToRemediationTextField,
      ApplicableMetricTagField,
      AlertItemDescriptionFormatField,
      AlertItemsHeaderField
    )
    validateNoExtraFields(yaml, AllFields)

    val severity = readOptional(yaml, SeverityField, alertSeverityParser).getOrElse(DefaultSeverity)
    val limitMetricName = readOptional(yaml, LimitMetricNameField, stringParser).getOrElse(DefaultLimitMetricName)
    val minimumValueToAlert =
      readOptional(yaml, MinimumValueToAlertField, doubleParser).getOrElse(DefaultMinimumValueToAlert)
    val metaCondition = readOptional(yaml, MetaConditionField, metaConditionParser).getOrElse(DefaultMetaCondition)
    val thresholdDirection =
      readOptional(yaml, ThresholdDirectionField, thresholdDirectionParser).getOrElse(DefaultThresholdDirection)
    val ruleCategories = readOptionalSet(yaml, RuleCategoriesField, ruleCategoryParser).getOrElse(DefaultRuleCategories)
    val deviceCategory = readOptional(yaml, DeviceCategoryField, deviceCategoryParser).getOrElse(DefaultDeviceCategory)
    new NearingCapacityWithTimeFrameWithItemsTemplateRule(
      applicableMetricTag = readMandatory(yaml, ApplicableMetricTagField, stringParser),
      ruleName = readMandatory(yaml, RuleNameField, stringParser),
      ruleFriendlyName = readMandatory(yaml, RuleFriendlyNameField, stringParser),
      ruleDescription = readMandatory(yaml, RuleDescriptionField, stringParser),
      severity = severity,
      usageMetricName = readMandatory(yaml, UsageMetricNameField, stringParser),
      limitMetricName = limitMetricName,
      threshold = readMandatory(yaml, ThresholdField, doubleParser),
      timeThreshold = readMandatory(yaml, TimeThresholdField, intParser),
      minimumValueToAlert = minimumValueToAlert,
      metaCondition = metaCondition,
      alertDescription = readMandatory(yaml, AlertDescriptionField, stringParser),
      alertItemsHeader = readMandatory(yaml, AlertItemsHeaderField, stringParser),
      baseRemediationText = readMandatory(yaml, BaseRemediationTextField, stringParser),
      thresholdDirection = thresholdDirection,
      alertItemDescriptionFormat = readMandatory(yaml, AlertItemDescriptionFormatField, stringParser),
      ruleCategories = ruleCategories,
      deviceCategory = deviceCategory
    )(readOptionalVendorToRemediationText(yaml, VendorToRemediationTextField): _*)
  }
}
