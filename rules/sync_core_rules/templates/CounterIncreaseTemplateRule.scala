package com.indeni.server.rules.library.templates

import com.indeni.ruleengine.Scope.{Scope, ImplicitScopeValueHelper}
import com.indeni.ruleengine.expressions.Expression
import com.indeni.ruleengine.expressions.conditions._
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data._
import com.indeni.ruleengine.expressions.scope.ScopableExpression
import com.indeni.server.common.data.conditions
import com.indeni.server.rules._
import com.indeni.server.rules.library.yamlrules.YamlRuleProcessorUtils._
import com.indeni.server.rules.library._
import com.indeni.server.rules.library.yamlrules.{YamlRuleParseException, YamlRuleProcessorUtils}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import net.jcazevedo.moultingyaml.YamlObject

import scala.util.matching.Regex

class CounterIncreaseTemplateRule(ruleName: String,
                                  ruleFriendlyName: String,
                                  ruleDescription: String,
                                  severity: AlertSeverity = CounterIncreaseTemplateRule.DefaultSeverity,
                                  metricName: String,
                                  applicableMetricTag: String,
                                  alertDescription: String,
                                  alertRemediationSteps: String,
                                  alertItemsHeader: String,
                                  itemSpecificDescription: Seq[(Regex, String)] = CounterIncreaseTemplateRule.DefaultItemSpecificDescription,
                                  ruleCategories:Set[RuleCategory] = CounterIncreaseTemplateRule.DefaultRuleCategories,
                                  deviceCategory: DeviceCategory = CounterIncreaseTemplateRule.DefaultDeviceCategory)(vendorToRemediationText: (RemediationStepCondition, String)*) extends PerDeviceRule with RuleHelper {
  override val metadata: RuleMetadata = RuleMetadata.builder(ruleName, ruleFriendlyName, ruleDescription, severity, ruleCategories,deviceCategory).build()

  override def expressionTree(context: RuleContext): StatusTreeExpression = {
    val actualValue = TimeSeriesExpression[Double](metricName).last

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), conditions.True),

      // What constitutes an issue
      StatusTreeExpression(

        // The additional tags we care about (we'll be including this in alert data)
        SelectTagsExpression(context.tsDao, Set(applicableMetricTag), withTagsCondition(metricName)),

        StatusTreeExpression(
          // The time-series we check the test condition against:
          SelectTimeSeriesExpression[Double](context.tsDao, Set(metricName), denseOnly = false),

          // The condition which, if true, we have an issue. Checked against the time-series we've collected
          GreaterThan(
            actualValue,
            ConstantExpression(Some(0.0)))

          // The Alert Item to add for this specific item
          ).withSecondaryInfo(
            scopableStringFormatExpression("${scope(\"" + applicableMetricTag + "\")}"),
            new ScopableExpression[String] {
              override protected def evalWithScope(time: Long, scope: Scope): String = {
                val metricTagValue = scope.getVisible(applicableMetricTag).get.toString
                itemSpecificDescription.collectFirst {
                  case item if (!item._1.findFirstMatchIn(metricTagValue).isEmpty) => item._2
                }.get
              }

              override def args: Set[Expression[_]] = Set()
            },
            title = alertItemsHeader
        ).asCondition()
      ).withoutInfo()
        .asCondition()
    ).withRootInfo(
      getHeadline(),
      ConstantExpression(alertDescription),
      ConditionalRemediationSteps(alertRemediationSteps, vendorToRemediationText:_*)
    )
  }
}

object CounterIncreaseTemplateRule {
  private val DefaultSeverity = AlertSeverity.ERROR
  private val DefaultItemSpecificDescription = Seq(".*".r -> "")
  private val DefaultRuleCategories: Set[RuleCategory] = Set()
  private val DefaultDeviceCategory = DeviceCategory.AllDevices

  @throws[YamlRuleParseException]
  def fromYaml(yaml: YamlObject, utils: YamlRuleProcessorUtils): CounterIncreaseTemplateRule = {
    import utils._
    val AllFields = Set(
      RuleNameField, RuleFriendlyNameField, RuleDescriptionField, MetricNameField, ApplicableMetricTagField,
      AlertDescriptionField, AlertRemediationStepsField, AlertItemsHeaderField, SeverityField,
      ItemSpecificDescriptionField, RuleCategoriesField, DeviceCategoryField, VendorToRemediationTextField)
    validateNoExtraFields(yaml, AllFields)
    val severity = readOptional(yaml, SeverityField, alertSeverityParser).getOrElse(DefaultSeverity)
    val itemSpecificDescription = readOptionalNonemptyList(yaml, ItemSpecificDescriptionField, regexStringPairTypeParser).getOrElse(DefaultItemSpecificDescription)
    val ruleCategories = readOptionalSet(yaml, RuleCategoriesField, ruleCategoryParser).getOrElse(DefaultRuleCategories)
    val deviceCategory = readOptional(yaml, DeviceCategoryField, deviceCategoryParser).getOrElse(DefaultDeviceCategory)
    new CounterIncreaseTemplateRule(
      ruleName = readMandatory(yaml, RuleNameField, stringParser),
      ruleFriendlyName = readMandatory(yaml, RuleFriendlyNameField, stringParser),
      ruleDescription = readMandatory(yaml, RuleDescriptionField, stringParser),
      metricName = readMandatory(yaml, MetricNameField, stringParser),
      applicableMetricTag = readMandatory(yaml, ApplicableMetricTagField, stringParser),
      alertDescription = readMandatory(yaml, AlertDescriptionField, stringParser),
      alertRemediationSteps = readMandatory(yaml, AlertRemediationStepsField, stringParser),
      alertItemsHeader = readMandatory(yaml, AlertItemsHeaderField, stringParser),
      severity = severity,
      itemSpecificDescription = itemSpecificDescription,
      ruleCategories = ruleCategories,
      deviceCategory = deviceCategory
    )(readOptionalVendorToRemediationText(yaml, VendorToRemediationTextField): _*)
  }
}
