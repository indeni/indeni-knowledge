package com.indeni.server.rules.library.templates

import com.indeni.ruleengine.expressions.conditions.{And, Equals}
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.server.common.data.conditions.{TagsStoreCondition, True}
import com.indeni.server.rules._
import com.indeni.server.rules.library.templates.VpnTunnelIsDownTemplateRule.{
  DefaultMetaCondition, DefaultRuleCategories
}
import com.indeni.server.rules.library.yamlrules.YamlRuleProcessorUtils._
import com.indeni.server.rules.library.yamlrules.{YamlRuleParseException, YamlRuleProcessorUtils}
import com.indeni.server.rules.library.{ConditionalRemediationSteps, PerDeviceRule, RuleHelper}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import net.jcazevedo.moultingyaml.YamlObject

class VpnTunnelIsDownTemplateRule(ruleName: String,
                                  ruleFriendlyName: String,
                                  alertTags: Set[String],
                                  optionalTags: Set[String],
                                  secondaryInfoHeadline: String,
                                  baseRemediationText: String,
                                  metaCondition: TagsStoreCondition = DefaultMetaCondition,
                                  categories: Set[RuleCategory] = DefaultRuleCategories,
                                  deviceCategory: DeviceCategory,
                                  metricName: String)(vendorToRemediationText: (RemediationStepCondition, String)*)
    extends PerDeviceRule
    with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata
    .builder(
      ruleName,
      ruleFriendlyName,
      "indeni will trigger an issue if one or more dynamic VPN tunnels is down.",
      AlertSeverity.INFO,
      categories = categories,
      deviceCategory = deviceCategory
    )
    .build()

  override def expressionTree(context: RuleContext): StatusTreeExpression = {
    val actualValue = TimeSeriesExpression[Double](metricName).last

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), metaCondition),
      // What constitutes an issue
      And(
        StatusTreeExpression(
          // The additional tags we care about (we'll be including this in alert data)
          SelectTagsExpression(context.tsDao, alertTags, True, optionalTags),
          // The condition which, if true, we have an issue. Checked against the metrics we've collected
          StatusTreeExpression(
            SelectTimeSeriesExpression[Double](context.tsDao, Set(metricName), denseOnly = false),
            Equals(actualValue, ConstantExpression(Some(0.0)))
          ).withSecondaryInfo(
              scopableStringFormatExpression(secondaryInfoHeadline),
              scopableStringFormatExpression("This tunnel is down"),
              title = "Dynamic VPN Tunnels Affected"
            )
            .asCondition()
        ).withoutInfo().asCondition(),
        generateDevicePassiveAndPassiveLinkStateCondition(context.tsDao)
      )
    ).withRootInfo(
      getHeadline(),
      ConstantExpression("One or more dynamic VPN tunnels are down."),
      ConditionalRemediationSteps(baseRemediationText, vendorToRemediationText: _*)
    )
  }
}

object VpnTunnelIsDownTemplateRule {
  private val DefaultMetaCondition: TagsStoreCondition = True
  private val DefaultRuleCategories: Set[RuleCategory] = Set()

  @throws[YamlRuleParseException]
  def fromYaml(yaml: YamlObject, utils: YamlRuleProcessorUtils): VpnTunnelIsDownTemplateRule = {
    import utils._
    val AllFields = Set(
      RuleNameField,
      RuleFriendlyNameField,
      AlertTagsField,
      OptionalTagsField,
      SecondaryInfoHeadlineField,
      MetaConditionField,
      RuleCategoriesField,
      DeviceCategoryField,
      MetricNameField,
      VendorToRemediationTextField,
      BaseRemediationTextField
    )
    validateNoExtraFields(yaml, AllFields)
    val metaCondition = readOptional(yaml, MetaConditionField, metaConditionParser).getOrElse(DefaultMetaCondition)
    val ruleCategories = readOptionalSet(yaml, RuleCategoriesField, ruleCategoryParser).getOrElse(DefaultRuleCategories)
    new VpnTunnelIsDownTemplateRule(
      ruleName = readMandatory(yaml, RuleNameField, stringParser),
      ruleFriendlyName = readMandatory(yaml, RuleFriendlyNameField, stringParser),
      alertTags = readMandatorySet(yaml, AlertTagsField, stringParser),
      optionalTags = readMandatorySet(yaml, OptionalTagsField, stringParser),
      secondaryInfoHeadline = readMandatory(yaml, SecondaryInfoHeadlineField, stringParser),
      metaCondition = metaCondition,
      baseRemediationText = readMandatory(yaml, BaseRemediationTextField, stringParser),
      categories = ruleCategories,
      deviceCategory = readMandatory(yaml, DeviceCategoryField, deviceCategoryParser),
      metricName = readMandatory(yaml, MetricNameField, stringParser)
    )(readOptionalVendorToRemediationText(yaml, VendorToRemediationTextField): _*)
  }
}
