package com.indeni.server.rules.library.templates

import com.indeni.ruleengine.Scope.{Scope, ImplicitScopeValueHelper}
import com.indeni.ruleengine.expressions.conditions.Condition
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data._
import com.indeni.ruleengine.expressions.scope.ScopableExpression
import com.indeni.ruleengine.expressions.utility.SeqDiffWithoutOrderExpression
import com.indeni.ruleengine.expressions.{Expression, NoArgsExpression}
import com.indeni.server.common.data.conditions.True
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules._
import com.indeni.server.rules.config.expressions.DynamicParameterExpression
import com.indeni.server.rules.library.templates.MultiSnapshotAllValuesExistInSnapshotTemplateRule.{DefaultAlertItemsHeader, DefaultDisableGlobalRuleSet, DefaultRuleCategories, DefaultSeverity}
import com.indeni.server.rules.library.yamlrules.YamlRuleProcessorUtils._
import com.indeni.server.rules.library.yamlrules.{YamlRuleParseException, YamlRuleProcessorUtils}
import com.indeni.server.rules.library.{ConditionalRemediationSteps, PerDeviceRule, RuleHelper}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import net.jcazevedo.moultingyaml.YamlObject

class MultiSnapshotAllValuesExistInSnapshotTemplateRule(ruleName: String,
                                                        ruleFriendlyName: String,
                                                        ruleDescription: String,
                                                        severity: AlertSeverity = DefaultSeverity,
                                                        metricName: String,
                                                        alertDescription: String,
                                                        alertItemsHeader: String = DefaultAlertItemsHeader,
                                                        baseRemediationText: String,
                                                        snapshotItemKey: String,
                                                        snapshotItemList: Seq[String],
                                                        disableGlobalRuleSet: Boolean = DefaultDisableGlobalRuleSet,
                                                        ruleCategories: Set[RuleCategory] = DefaultRuleCategories)(vendorToRemediationText: (RemediationStepCondition, String)*)
  extends PerDeviceRule
    with RuleHelper {

  private val linesParameter = new ParameterDefinition(
    "items_to_look_for",
    "",
    "Lines Needed (multi-line)",
    "List the items that need to show in the output.",
    UIType.MULTILINE_TEXT,
    snapshotItemList.mkString("\n")
  )

  override val metadata: RuleMetadata = RuleMetadata
    .builder(ruleName, ruleFriendlyName, ruleDescription, severity, ruleCategories)
    .configParameter(linesParameter)
    .disableGlobalRuleSet(disableGlobalRuleSet)
    .build()

  override def expressionTree(context: RuleContext): StatusTreeExpression = {

    val expectedValues = DynamicParameterExpression
      .withConstantDefault[Seq[String]](linesParameter.getName, linesParameter.getDefaultValue.asSeq())
      .withLazy

    val actualValues = SnapshotExpression(metricName).asMulti().mostRecent().value()

    val diffExpression = new SeqDiffWithoutOrderExpression[String](
      MultiSnapshotExtractScalarExpression(actualValues, snapshotItemKey).noneable,
      expectedValues)

    val missingValues = new ScopableExpression[Set[Scope]] {

      override protected def evalWithScope(time: Long, scope: Scope): Set[Scope] = {
        val missing = diffExpression.eval(time).missing
        if (missing.isEmpty) Set(scope) else missing.map(scope.withVisible(snapshotItemKey, _))
      }

      override def args: Set[Expression[_]] = Set(diffExpression)
    }

    val hasMissingValue: Condition =
      new ScopableExpression[Option[Boolean]] with NoArgsExpression[Option[Boolean]] with Condition {

        override protected def evalWithScope(time: Long, scope: Scope): Option[Boolean] = {
          Some(scope.getVisible(snapshotItemKey).fold(false)(_ => true))
        }
      }

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),
      // What constitutes an issue
      StatusTreeExpression(
        // The snapshot we check the test condition against:
        SelectSnapshotsExpression(context.snapshotsDao, Set(metricName)).multi(),
        StatusTreeExpression(
          missingValues,
          // The condition which, if true, we have an issue. Checked against the snapshots we've collected
          hasMissingValue
        ).withSecondaryInfo(
            scopableStringFormatExpression(snapshotItemKey),
            scopableStringFormatExpression("Missing ${scope(\"" + snapshotItemKey + "\")}"),
            title = "Missing lines"
          )
          .asCondition()
      ).withoutInfo().asCondition()
    ).withRootInfo(
      getHeadline(),
      ConstantExpression(
        "Some line items that should be exist are missing:\n" +
          "Indeni has found that some line items are missing. These are listed below"),
      ConditionalRemediationSteps("Modify the device's configuration as required.")
    )
  }
}

object MultiSnapshotAllValuesExistInSnapshotTemplateRule {
  val DefaultSeverity = AlertSeverity.ERROR
  val DefaultAlertItemsHeader: String = null
  val DefaultDisableGlobalRuleSet = true
  val DefaultRuleCategories: Set[RuleCategory] = Set()

  @throws[YamlRuleParseException]
  def fromYaml(yaml: YamlObject, utils: YamlRuleProcessorUtils): MultiSnapshotAllValuesExistInSnapshotTemplateRule = {
    import utils._
    val AllFields = Set(
      RuleNameField, RuleFriendlyNameField, RuleDescriptionField, SeverityField, MetricNameField, AlertDescriptionField,
      AlertItemsHeaderField, BaseRemediationTextField, SnapshotItemKeyField, SnapshotItemListField, DisableGlobalRuleSetField,
      RuleCategoriesField, VendorToRemediationTextField)
    validateNoExtraFields(yaml, AllFields)
    val severity = readOptional(yaml, SeverityField, alertSeverityParser).getOrElse(DefaultSeverity)
    val alertItemsHeader = readOptional(yaml, AlertItemsHeaderField, stringParser).getOrElse(DefaultAlertItemsHeader)
    val disableGlobalRuleSet = readOptional(yaml, DisableGlobalRuleSetField, booleanParser).getOrElse(DefaultDisableGlobalRuleSet)
    val ruleCategories = readOptionalSet(yaml, RuleCategoriesField, ruleCategoryParser).getOrElse(DefaultRuleCategories)

    new MultiSnapshotAllValuesExistInSnapshotTemplateRule(
      ruleName = readMandatory(yaml, RuleNameField, stringParser),
      ruleFriendlyName = readMandatory(yaml, RuleFriendlyNameField, stringParser),
      ruleDescription = readMandatory(yaml, RuleDescriptionField, stringParser),
      severity = severity,
      metricName = readMandatory(yaml, MetricNameField, stringParser),
      alertDescription = readMandatory(yaml, AlertDescriptionField, stringParser),
      alertItemsHeader = alertItemsHeader,
      baseRemediationText = readMandatory(yaml, BaseRemediationTextField, stringParser),
      snapshotItemKey = readMandatory(yaml, SnapshotItemKeyField, stringParser),
      snapshotItemList = readMandatoryList(yaml, SnapshotItemListField, utils.stringParser),
      disableGlobalRuleSet = disableGlobalRuleSet,
      ruleCategories = ruleCategories
    )(readOptionalVendorToRemediationText(yaml, VendorToRemediationTextField): _*)
  }
}
