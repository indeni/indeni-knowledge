package com.indeni.server.rules.library.templates

import com.indeni.apidata.time.TimeSpan
import com.indeni.apidata.time.TimeSpan.TimePeriod
import com.indeni.ruleengine.expressions.data.{SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.server.common.data.conditions.{TagsStoreCondition, True}
import com.indeni.server.rules._
import com.indeni.server.rules.library.templates.TimeThresholdOnDoubleMetricWithItemsTemplateRule._
import com.indeni.server.rules.library.yamlrules.YamlRuleProcessorUtils._
import com.indeni.server.rules.library.yamlrules.{YamlRuleParseException, YamlRuleProcessorUtils}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import net.jcazevedo.moultingyaml.YamlObject

import scala.util.matching.Regex

class TimeThresholdOnDoubleMetricWithItemsTemplateRule(ruleName: String,
                                                       ruleFriendlyName: String,
                                                       ruleDescription: String,
                                                       severity: AlertSeverity = DefaultSeverity,
                                                       metricName: String,
                                                       threshold: TimeSpan,
                                                       metricUnits: TimePeriod,
                                                       metaCondition: TagsStoreCondition = DefaultMetaCondition,
                                                       applicableMetricTag: String,
                                                       alertItemDescriptionFormat: String,
                                                       alertItemDescriptionUnits: TimePeriod,
                                                       alertDescription: String,
                                                       baseRemediationText: String,
                                                       thresholdDirection: ThresholdDirection = DefaultThresholdDirection,
                                                       alertItemsHeader: String,
                                                       itemsToIgnore: Set[Regex] = DefaultItemsToIgnore,
                                                       itemSpecificDescription: Seq[(Regex, String)] = DefaultItemSpecificDescription,
                                                       ruleCategories:Set[RuleCategory] = DefaultRuleCategories,
                                                       deviceCategory: DeviceCategory = DefaultDeviceCategory
                                                      )(vendorToRemediationText: (RemediationStepCondition, String)*)
  extends NumericThresholdWithItemsTemplateRule[TimeSpan](
    ruleName,
    ruleFriendlyName,
    ruleDescription,
    severity,
    TimeSeriesExpression[Double](metricName).last.toTimeSpan(metricUnits),
    _.tsDao,
    context => SelectTimeSeriesExpression[Double](context.tsDao, Set(metricName), denseOnly = false),
    metricName,
    threshold,
    metaCondition,
    applicableMetricTag,
    alertItemDescriptionFormat,
    value => value.toDouble(alertItemDescriptionUnits),
    alertDescription,
    baseRemediationText,
    thresholdDirection,
    alertItemsHeader,
    itemsToIgnore,
    itemSpecificDescription,
    ruleCategories,
    deviceCategory
  )(vendorToRemediationText: _*) {}

object TimeThresholdOnDoubleMetricWithItemsTemplateRule {
  private val DefaultSeverity = AlertSeverity.ERROR
  private val DefaultMetaCondition: TagsStoreCondition = True
  private val DefaultThresholdDirection = ThresholdDirection.ABOVE
  private val DefaultItemsToIgnore = Set("^$".r)
  private val DefaultItemSpecificDescription = Seq(".*".r -> "")
  private val DefaultRuleCategories: Set[RuleCategory] = Set()
  private val DefaultDeviceCategory = DeviceCategory.AllDevices

  @throws[YamlRuleParseException]
  def fromYaml(yaml: YamlObject, utils: YamlRuleProcessorUtils): TimeThresholdOnDoubleMetricWithItemsTemplateRule = {
    import utils._
    val AllFields = Set(RuleNameField, RuleFriendlyNameField, RuleDescriptionField, SeverityField, MetricNameField,
      ThresholdField, MetricUnitsField, MetaConditionField, ApplicableMetricTagField, AlertItemDescriptionFormatField,
      AlertItemDescriptionUnitsField, AlertDescriptionField, BaseRemediationTextField, ThresholdDirectionField,
      AlertItemsHeaderField, ItemsToIgnoreField, ItemSpecificDescriptionField, RuleCategoriesField,
      DeviceCategoryField, VendorToRemediationTextField
    )
    validateNoExtraFields(yaml, AllFields)

    val severity = readOptional(yaml, SeverityField, alertSeverityParser).getOrElse(DefaultSeverity)
    val metaCondition = readOptional(yaml, MetaConditionField, metaConditionParser).getOrElse(DefaultMetaCondition)
    val thresholdDirection = readOptional(yaml, ThresholdDirectionField, thresholdDirectionParser).getOrElse(DefaultThresholdDirection)
    val itemsToIgnore = readOptionalSet(yaml, ItemsToIgnoreField, regexTypeParser).getOrElse(DefaultItemsToIgnore)
    val itemSpecificDescription = readOptionalList(yaml, ItemSpecificDescriptionField, regexStringPairTypeParser).getOrElse(DefaultItemSpecificDescription)
    val ruleCategories = readOptionalSet(yaml, RuleCategoriesField, ruleCategoryParser).getOrElse(DefaultRuleCategories)
    val deviceCategory = readOptional(yaml, DeviceCategoryField, deviceCategoryParser).getOrElse(DefaultDeviceCategory)

    new TimeThresholdOnDoubleMetricWithItemsTemplateRule(
      ruleName = readMandatory(yaml, RuleNameField, stringParser),
      ruleFriendlyName = readMandatory(yaml, RuleFriendlyNameField, stringParser),
      ruleDescription = readMandatory(yaml, RuleDescriptionField, stringParser),
      severity = severity,
      metricName = readMandatory(yaml, MetricNameField, stringParser),
      threshold = convertFiniteDurationToTimeSpan(readMandatory(yaml, ThresholdField, timeIntervalParser)),
      metricUnits = readMandatory(yaml, MetricUnitsField, timePeriodParser),
      metaCondition = metaCondition,
      applicableMetricTag = readMandatory(yaml, ApplicableMetricTagField, stringParser),
      alertItemDescriptionFormat = readMandatory(yaml, AlertItemDescriptionFormatField, stringParser),
      alertItemDescriptionUnits = readMandatory(yaml, AlertItemDescriptionUnitsField, timePeriodParser),
      alertDescription = readMandatory(yaml, AlertDescriptionField, stringParser),
      baseRemediationText = readMandatory(yaml, BaseRemediationTextField, stringParser),
      thresholdDirection = thresholdDirection,
      alertItemsHeader = readMandatory(yaml, AlertItemsHeaderField, stringParser),
      itemsToIgnore = itemsToIgnore,
      itemSpecificDescription = itemSpecificDescription,
      ruleCategories = ruleCategories,
      deviceCategory = deviceCategory
    )(readOptionalVendorToRemediationText(yaml, VendorToRemediationTextField): _*)
  }
}
