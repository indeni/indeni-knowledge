package com.indeni.server.rules.library.templates

import com.indeni.ruleengine.expressions.data._
import com.indeni.server.common.data.conditions.{TagsStoreCondition, True}
import com.indeni.server.rules.library.templates.NumericThresholdOnDoubleMetricTemplateRule.{DefaultDeviceCategory => _, DefaultMetaCondition => _, DefaultRuleCategories => _, DefaultSeverity => _, DefaultThresholdDirection => _}
import com.indeni.server.rules.library.templates.NumericThresholdOnDoubleMetricWithItemsTemplateRule._
import com.indeni.server.rules.library.yamlrules.YamlRuleProcessorUtils._
import com.indeni.server.rules.library.yamlrules.{YamlRuleParseException, YamlRuleProcessorUtils}
import com.indeni.server.rules.{DeviceCategory, RemediationStepCondition, RuleCategory, ThresholdDirection}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import net.jcazevedo.moultingyaml.YamlObject

import scala.util.matching.Regex

class NumericThresholdOnDoubleMetricWithItemsTemplateRule(ruleName: String,
                                                          ruleFriendlyName: String,
                                                          ruleDescription: String,
                                                          severity: AlertSeverity = DefaultSeverity,
                                                          metricName: String,
                                                          threshold: Double,
                                                          metaCondition: TagsStoreCondition = DefaultMetaCondition,
                                                          applicableMetricTag: String,
                                                          alertItemDescriptionFormat: String,
                                                          alertDescription: String,
                                                          baseRemediationText: String,
                                                          thresholdDirection: ThresholdDirection = DefaultThresholdDirection,
                                                          alertItemsHeader: String,
                                                          itemsToIgnore: Set[Regex] = DefaultItemsToIgnore,
                                                          itemSpecificDescription: Seq[(Regex, String)] = DefaultItemSpecificDescription,
                                                          ruleCategories:Set[RuleCategory] = DefaultRuleCategories,
                                                          deviceCategory: DeviceCategory = DefaultDeviceCategory)(vendorToRemediationText: (RemediationStepCondition, String)*)
  extends NumericThresholdWithItemsTemplateRule[Double](
    ruleName,
    ruleFriendlyName,
    ruleDescription,
    severity,
    TimeSeriesExpression[Double](metricName).last,
    _.tsDao,
    context => SelectTimeSeriesExpression[Double](context.tsDao, Set(metricName), denseOnly = false),
    metricName,
    threshold,
    metaCondition,
    applicableMetricTag,
    alertItemDescriptionFormat,
    identity => identity,
    alertDescription,
    baseRemediationText,
    thresholdDirection,
    alertItemsHeader,
    itemsToIgnore,
    itemSpecificDescription,
    ruleCategories,
    deviceCategory
  )(vendorToRemediationText: _*) {}

object NumericThresholdOnDoubleMetricWithItemsTemplateRule {
  private val DefaultSeverity = AlertSeverity.ERROR
  private val DefaultMetaCondition: TagsStoreCondition = True
  private val DefaultThresholdDirection = ThresholdDirection.ABOVE
  private val DefaultItemsToIgnore = Set("^$".r)
  private val DefaultItemSpecificDescription = Seq(".*".r -> "")
  private val DefaultRuleCategories: Set[RuleCategory] = Set()
  private val DefaultDeviceCategory = DeviceCategory.AllDevices

  @throws[YamlRuleParseException]
  def fromYaml(yaml: YamlObject, utils: YamlRuleProcessorUtils): NumericThresholdOnDoubleMetricWithItemsTemplateRule = {
    import utils._
    val AllFields = Set(RuleNameField, RuleFriendlyNameField, RuleDescriptionField, SeverityField,
      MetricNameField, ThresholdField, MetaConditionField, ApplicableMetricTagField, AlertItemDescriptionFormatField,
      AlertDescriptionField, BaseRemediationTextField, ThresholdDirectionField, AlertItemsHeaderField,
      ItemsToIgnoreField, ItemSpecificDescriptionField, RuleCategoriesField, DeviceCategoryField,
      VendorToRemediationTextField
    )
    validateNoExtraFields(yaml, AllFields)

    val severity = readOptional(yaml, SeverityField, alertSeverityParser).getOrElse(DefaultSeverity)
    val metaCondition = readOptional(yaml, MetaConditionField, metaConditionParser).getOrElse(DefaultMetaCondition)
    val thresholdDirection = readOptional(yaml, ThresholdDirectionField, thresholdDirectionParser).getOrElse(DefaultThresholdDirection)
    val itemsToIgnore = readOptionalSet(yaml, ItemsToIgnoreField, regexTypeParser).getOrElse(DefaultItemsToIgnore)
    val itemSpecificDescription = readOptionalNonemptyList(yaml, ItemSpecificDescriptionField, regexStringPairTypeParser).getOrElse(DefaultItemSpecificDescription)
    val ruleCategories = readOptionalSet(yaml, RuleCategoriesField, ruleCategoryParser).getOrElse(DefaultRuleCategories)
    val deviceCategory = readOptional(yaml, DeviceCategoryField, deviceCategoryParser).getOrElse(DefaultDeviceCategory)

    new NumericThresholdOnDoubleMetricWithItemsTemplateRule(
      ruleName = readMandatory(yaml, RuleNameField, stringParser),
      ruleFriendlyName = readMandatory(yaml, RuleFriendlyNameField, stringParser),
      ruleDescription = readMandatory(yaml, RuleDescriptionField, stringParser),
      severity = severity,
      metricName = readMandatory(yaml, MetricNameField, stringParser),
      threshold = readMandatory(yaml, ThresholdField, doubleParser),
      metaCondition = metaCondition,
      applicableMetricTag = readMandatory(yaml, ApplicableMetricTagField, stringParser),
      alertItemDescriptionFormat = readMandatory(yaml, AlertItemDescriptionFormatField, stringParser),
      alertDescription = readMandatory(yaml, AlertDescriptionField, stringParser),
      baseRemediationText = readMandatory(yaml, BaseRemediationTextField, stringParser),
      thresholdDirection = thresholdDirection,
      alertItemsHeader = readMandatory(yaml, AlertItemsHeaderField, stringParser),
      itemsToIgnore = itemsToIgnore,
      itemSpecificDescription = itemSpecificDescription,
      ruleCategories = ruleCategories,
      deviceCategory = deviceCategory
    )(readOptionalVendorToRemediationText(yaml, VendorToRemediationTextField): _*)
  }
}
