package com.indeni.server.rules.library.templates

import com.indeni.apidata.time.TimeSpan
import com.indeni.apidata.time.TimeSpan.TimePeriod
import com.indeni.ruleengine.expressions.conditions.LesserThan
import com.indeni.ruleengine.expressions.core._
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.ruleengine.expressions.math.PlusExpression
import com.indeni.ruleengine.expressions.utility.NowExpression
import com.indeni.server.common.data.conditions.True
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules.library._
import com.indeni.server.rules.library.templates.ValueNearExpirationDateTemplateRule._
import com.indeni.server.rules.library.yamlrules.YamlRuleProcessorUtils._
import com.indeni.server.rules.library.yamlrules.{YamlRuleParseException, YamlRuleProcessorUtils}
import com.indeni.server.rules.{RuleContext, _}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import net.jcazevedo.moultingyaml.YamlObject

class ValueNearExpirationDateTemplateRule(ruleName: String,
                                      ruleFriendlyName: String,
                                      ruleDescription: String,
                                      severity: AlertSeverity = DefaultSeverity,
                                      metricName: String,
                                      threshold: Int,
                                      alertDescription: String,
                                      baseRemediationText: String,
                                      ruleCategories: Set[RuleCategory] = DefaultRuleCategories,
                                      deviceCategory: DeviceCategory = DefaultDeviceCategory)(
    vendorToRemediationText: (RemediationStepCondition, String)*)
    extends PerDeviceRule
    with RuleHelper {

  private val highThresholdParameterName = "Ahead_Alerting_Threshold"
  private val highThresholdParameter = new ParameterDefinition(highThresholdParameterName,
    "",
    "Expiration Threshold",
    "How long before end of support should Indeni alert.",
    UIType.TIMESPAN,
    TimeSpan.fromDays(threshold))

  override val metadata: RuleMetadata = RuleMetadata
    .builder(
      ruleName,
      ruleFriendlyName,
      ruleDescription,
      severity,
      categories = ruleCategories,
      deviceCategory = deviceCategory
    )
    .configParameter(highThresholdParameter)
    .build()

  override def expressionTree(context: RuleContext): StatusTreeExpression = {
    val actualValue = TimeSeriesExpression[Double](metricName).last.toTimeSpan(TimePeriod.SECOND)

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),
      // What constitutes an issue
      StatusTreeExpression(
        // The time-series we check the test condition against:
        SelectTimeSeriesExpression[Double](context.tsDao, Set(metricName), denseOnly = false),
        // The condition which, if true, we have an issue. Checked against the time-series we've collected
        LesserThan(actualValue,
          PlusExpression[TimeSpan](NowExpression(), getParameterTimeSpanForTimeSeries(highThresholdParameter)))

        // The Alert Item to add for this specific item
      ).withRootInfo(
        getHeadline(),
        scopableStringFormatExpression(alertDescription,
          timeSpanToDateExpression(actualValue)),
        ConditionalRemediationSteps(baseRemediationText, vendorToRemediationText: _*)
      )
        .asCondition()
    ).withoutInfo()
  }
}

object ValueNearExpirationDateTemplateRule {
  private val DefaultSeverity = AlertSeverity.ERROR
  private val DefaultRuleCategories: Set[RuleCategory] = Set(RuleCategory.OngoingMaintenance)
  private val DefaultDeviceCategory = DeviceCategory.AllDevices

  @throws[YamlRuleParseException]
  def fromYaml(yaml: YamlObject, utils: YamlRuleProcessorUtils): ValueNearExpirationDateTemplateRule = {
    import utils._
    val AllFields = Set(
      RuleNameField,
      RuleFriendlyNameField,
      RuleDescriptionField,
      SeverityField,
      MetricNameField,
      ThresholdField,
      AlertDescriptionField,
      BaseRemediationTextField,
      RuleCategoriesField,
      DeviceCategoryField,
      VendorToRemediationTextField
    )
    validateNoExtraFields(yaml, AllFields)

    val severity = readOptional(yaml, SeverityField, alertSeverityParser).getOrElse(DefaultSeverity)
    val ruleCategories =
      readOptionalSet(yaml, RuleCategoriesField, ruleCategoryParser).getOrElse(DefaultRuleCategories)
    val deviceCategory =
      readOptional(yaml, DeviceCategoryField, deviceCategoryParser).getOrElse(DefaultDeviceCategory)

    new ValueNearExpirationDateTemplateRule(
      ruleName = readMandatory(yaml, RuleNameField, stringParser),
      ruleFriendlyName = readMandatory(yaml, RuleFriendlyNameField, stringParser),
      ruleDescription = readMandatory(yaml, RuleDescriptionField, stringParser),
      severity = severity,
      metricName = readMandatory(yaml, MetricNameField, stringParser),
      threshold = readMandatory(yaml, ThresholdField, intParser),
      alertDescription = readMandatory(yaml, AlertDescriptionField, stringParser),
      baseRemediationText = readMandatory(yaml, BaseRemediationTextField, stringParser),
      ruleCategories = readOptionalSet(yaml, RuleCategoriesField, ruleCategoryParser).getOrElse(DefaultRuleCategories),
      deviceCategory = readOptional(yaml, DeviceCategoryField, deviceCategoryParser).getOrElse(DefaultDeviceCategory)
    )(readOptionalVendorToRemediationText(yaml, VendorToRemediationTextField): _*)
  }
}
