package com.indeni.server.rules.library.templates

import com.indeni.ruleengine.Scope.{Scope, ImplicitScopeValueHelper}
import com.indeni.ruleengine.expressions.Expression
import com.indeni.ruleengine.expressions.conditions.Condition
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data._
import com.indeni.ruleengine.expressions.scope.ScopableExpression
import com.indeni.server.common.data.conditions.{TagsStoreCondition, True}
import com.indeni.server.rules._
import com.indeni.server.rules.library._
import com.indeni.server.rules.library.templates.MultiSnapshotValueCheckTemplateRule._
import com.indeni.server.rules.library.yamlrules.YamlRuleProcessorUtils._
import com.indeni.server.rules.library.yamlrules.{YamlRuleParseException, YamlRuleProcessorUtils}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import net.jcazevedo.moultingyaml.YamlObject

import scala.util.matching.Regex

class MultiSnapshotValueCheckTemplateRule(ruleName: String,
                                          ruleFriendlyName: String,
                                          ruleDescription: String,
                                          severity: AlertSeverity = DefaultSeverity,
                                          metricName: String,
                                          applicableMetricTag: String = DefaultApplicableMetricTag,
                                          alertDescription: String,
                                          alertItemsHeader: String = DefaultAlertItemsHeader,
                                          baseRemediationText: String,
                                          complexCondition: Condition,
                                          metaCondition: TagsStoreCondition = DefaultMetaCondition,
                                          itemSpecificDescription: Seq[(Regex, String)] = MultiSnapshotValueCheckTemplateRule.DefaultItemSpecificDescription,
                                          ruleCategories:Set[RuleCategory] = DefaultRuleCategories,
                                          deviceCategory: DeviceCategory = DefaultDeviceCategory)(vendorToRemediationText: (RemediationStepCondition, String)*) extends PerDeviceRule with RuleHelper {
  override val metadata: RuleMetadata =
    RuleMetadata.builder(ruleName, ruleFriendlyName, ruleDescription, severity, ruleCategories).build()

  override def expressionTree(context: RuleContext): StatusTreeExpression = {
    val value = SnapshotExpression(metricName).asMulti().mostRecent()

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), metaCondition),
      // What constitutes an issue
      if (null != applicableMetricTag)
        StatusTreeExpression(
          // The additional tags we care about (we'll be including this in alert data)
          SelectTagsExpression(context.snapshotsDao, Set(applicableMetricTag), withTagsCondition(metricName)),
          StatusTreeExpression(
            // The time-series we check the test condition against:
            SelectSnapshotsExpression(context.snapshotsDao, Set(metricName)).multi(),
            // The condition which, if true, we have an issue. Checked against the time-series we've collected
            complexCondition
          ).withSecondaryInfo(
              scopableStringFormatExpression("${scope(\"" + applicableMetricTag + "\")}"),
              new ScopableExpression[String] {
                override protected def evalWithScope(time: Long, scope: Scope): String = {
                  val metricTagValue = scope.getVisible(applicableMetricTag).get.toString
                  itemSpecificDescription.collectFirst {
                    case item if (!item._1.findFirstMatchIn(metricTagValue).isEmpty) => item._2
                  }.get + value
                    .eval(time)
                    .value
                    .map(_.toList.sortBy(_._1).map(x => s"${x._1}: ${x._2}").mkString(", "))
                    .mkString("\n")
                }

                override def args: Set[Expression[_]] = Set(value)
              },
              title = alertItemsHeader
            )
            .asCondition()
        ).withoutInfo().asCondition()
      else
        StatusTreeExpression(
          // The time-series we check the test condition against:
          SelectSnapshotsExpression(context.snapshotsDao, Set(metricName)).multi(),
          // The condition which, if true, we have an issue. Checked against the time-series we've collected
          complexCondition
        ).withoutInfo().asCondition()

      // Details of the alert itself
    ).withRootInfo(
      getHeadline(),
      ConstantExpression(alertDescription),
      ConditionalRemediationSteps(baseRemediationText, vendorToRemediationText: _*)
    )
  }
}

object MultiSnapshotValueCheckTemplateRule {
  val DefaultSeverity = AlertSeverity.ERROR
  val DefaultApplicableMetricTag: String = null
  val DefaultAlertItemsHeader: String = null
  val DefaultMetaCondition: TagsStoreCondition = True
  val DefaultItemSpecificDescription: Seq[(Regex, String)] = Seq(".*".r -> "")
  val DefaultRuleCategories: Set[RuleCategory] = Set()
  val DefaultDeviceCategory = DeviceCategory.AllDevices

  @throws[YamlRuleParseException]
  def fromYaml(yaml: YamlObject, utils: YamlRuleProcessorUtils): MultiSnapshotValueCheckTemplateRule = {
    import utils._
    val AllFields = Set(RuleNameField, RuleFriendlyNameField, RuleDescriptionField, SeverityField, MetricNameField,
      ApplicableMetricTagField, AlertDescriptionField, AlertItemsHeaderField, BaseRemediationTextField,
      ComplexConditionField, MetaConditionField, ItemSpecificDescriptionField, RuleCategoriesField,
      DeviceCategoryField, VendorToRemediationTextField)
    validateNoExtraFields(yaml, AllFields)
    val severity = readOptional(yaml, SeverityField, alertSeverityParser).getOrElse(DefaultSeverity)
    val applicableMetricTag = readOptional(yaml, ApplicableMetricTagField, stringParser).getOrElse(DefaultApplicableMetricTag)
    val alertItemsHeader = readOptional(yaml, AlertItemsHeaderField, stringParser).getOrElse(DefaultAlertItemsHeader)
    val metaCondition = readOptional(yaml, MetaConditionField, metaConditionParser).getOrElse(DefaultMetaCondition)
    val itemSpecificDescription = readOptionalNonemptyList(yaml, ItemSpecificDescriptionField, regexStringPairTypeParser).getOrElse(DefaultItemSpecificDescription)
    val ruleCategories = readOptionalSet(yaml, RuleCategoriesField, ruleCategoryParser).getOrElse(DefaultRuleCategories)
    val deviceCategory = readOptional(yaml, DeviceCategoryField, deviceCategoryParser).getOrElse(DefaultDeviceCategory)
    new MultiSnapshotValueCheckTemplateRule(
      ruleName = readMandatory(yaml, RuleNameField, stringParser),
      ruleFriendlyName = readMandatory(yaml, RuleFriendlyNameField, stringParser),
      ruleDescription = readMandatory(yaml, RuleDescriptionField, stringParser),
      severity = severity,
      metricName = readMandatory(yaml, MetricNameField, stringParser),
      applicableMetricTag = applicableMetricTag,
      alertDescription = readMandatory(yaml, AlertDescriptionField, stringParser),
      alertItemsHeader = alertItemsHeader,
      baseRemediationText = readMandatory(yaml, BaseRemediationTextField, stringParser),
      complexCondition = readMandatory(yaml, ComplexConditionField, complexConditionParser),
      metaCondition = metaCondition,
      itemSpecificDescription = itemSpecificDescription,
      ruleCategories = ruleCategories,
      deviceCategory = deviceCategory
    )(readOptionalVendorToRemediationText(yaml, VendorToRemediationTextField): _*)
  }
}
