package com.indeni.server.rules.library.templates
import com.indeni.ruleengine.expressions.casting.ToStringExpression
import com.indeni.ruleengine.expressions.conditions.Equals
import com.indeni.ruleengine.expressions.core.StatusTreeExpression
import com.indeni.ruleengine.expressions.data._
import com.indeni.server.common.AlertNotificationSettings
import com.indeni.server.common.AlertNotificationSettings._
import com.indeni.server.common.data.conditions.{TagsStoreCondition, True}
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules._
import com.indeni.server.rules.config.expressions.DynamicParameterExpression
import com.indeni.server.rules.library._
import com.indeni.server.rules.library.templates.SingleSnapshotComplianceCheckTemplateRule.{DefaultDeviceCategory, DefaultMetaCondition, DefaultRuleCategories, DefaultSeverity}
import com.indeni.server.rules.library.yamlrules.YamlRuleProcessorUtils._
import com.indeni.server.rules.library.yamlrules.{YamlRuleParseException, YamlRuleProcessorUtils}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import net.jcazevedo.moultingyaml.YamlObject


class SingleSnapshotComplianceCheckTemplateRule[T: scala.reflect.runtime.universe.TypeTag](ruleName: String,
                                                                                           ruleFriendlyName: String,
                                                                                           ruleDescription: String,
                                                                                           severity: AlertSeverity = DefaultSeverity,
                                                                                           metricName: String,
                                                                                           baseRemediationText: String,
                                                                                           metaCondition: TagsStoreCondition = DefaultMetaCondition,
                                                                                           expectedValue: T,
                                                                                           parameterName: String,
                                                                                           parameterDescription: String,
                                                                                           ruleCategories:Set[RuleCategory] = DefaultRuleCategories,
                                                                                           deviceCategory: DeviceCategory = DefaultDeviceCategory)
                                                                                          (vendorToRemediationText: (RemediationStepCondition, String)*)
  extends PerDeviceRule
    with RuleHelper {

  private val requiredValue = new ParameterDefinition("required_value",
                                                      "",
                                                      parameterName,
                                                      parameterDescription,
                                                      UIType.fromObjectClass(expectedValue.getClass),
    expectedValue)

  override val metadata: RuleMetadata = RuleMetadata
    .builder(ruleName, ruleFriendlyName, ruleDescription, severity,deviceCategory = deviceCategory)
    .defaultAction(AlertNotificationSettings(NONE))
    .configParameter(requiredValue)
    .disableGlobalRuleSet(true)
    .build()

  override def expressionTree(context: RuleContext): StatusTreeExpression = {
    val expected = ToStringExpression(
      DynamicParameterExpression.withConstantDefault[T](requiredValue.getName, expectedValue)).noneable
    val actual =
      SingleSnapshotExtractExpression(SnapshotExpression(metricName).asSingle().mostRecent().value(), "value")

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),
      // What constitutes an issue
      StatusTreeExpression(
        // The snapshot we check the test condition against:
        SelectSnapshotsExpression(context.snapshotsDao, Set(metricName)).single(),
        // The condition which, if true, we have an issue. Checked against the snapshots we've collected
        Equals(actual, expected).not
      ).withRootInfo(
          // Details of the alert itself
          getHeadline(),
          scopableStringFormatExpression("The configuration is not as expected:\nExpected: %s, Actual: %s.",
                                         expected,
                                         actual),
          ConditionalRemediationSteps(baseRemediationText, vendorToRemediationText: _*)
        )
        .asCondition()
    ).withoutInfo()
  }
}

object SingleSnapshotComplianceCheckTemplateRule {
  private val DefaultSeverity = AlertSeverity.ERROR
  private val DefaultMetaCondition: TagsStoreCondition = True
  private val DefaultRuleCategories: Set[RuleCategory] = Set()
  private val DefaultDeviceCategory = DeviceCategory.AllDevices

  @throws[YamlRuleParseException]
  def fromYaml(yaml: YamlObject, utils: YamlRuleProcessorUtils) = {
    import utils._
    val AllFields = Set(RuleNameField, RuleFriendlyNameField, RuleDescriptionField, SeverityField, MetricNameField,
      BaseRemediationTextField, MetaConditionField, ExpectedValueField, ParameterNameField,
      ParameterDescriptionField, RuleCategoriesField, DeviceCategoryField, VendorToRemediationTextField
    )
    validateNoExtraFields(yaml, AllFields)

    val severity = readOptional(yaml, SeverityField, alertSeverityParser).getOrElse(DefaultSeverity)
    val metaCondition = readOptional(yaml, MetaConditionField, metaConditionParser).getOrElse(DefaultMetaCondition)
    val ruleCategories = readOptionalSet(yaml, RuleCategoriesField, ruleCategoryParser).getOrElse(DefaultRuleCategories)
    val deviceCategory = readOptional(yaml, DeviceCategoryField, deviceCategoryParser).getOrElse(DefaultDeviceCategory)
    val vendorToRemediationText = readOptionalVendorToRemediationText(yaml, VendorToRemediationTextField)
    val ruleName = readMandatory(yaml, RuleNameField, stringParser)
    val ruleFriendlyName = readMandatory(yaml, RuleFriendlyNameField, stringParser)
    val ruleDescription = readMandatory(yaml, RuleDescriptionField, stringParser)
    val metricName = readMandatory(yaml, MetricNameField, stringParser)
    val baseRemediationText = readMandatory(yaml, BaseRemediationTextField, stringParser)
    val parameterName = readMandatory(yaml, ParameterNameField, stringParser)
    val parameterDescription = readMandatory(yaml, ParameterDescriptionField, stringParser)
    val expectedValue = readMandatory(yaml, ExpectedValueField, primitiveValueParser)

    def createInstance[T: scala.reflect.runtime.universe.TypeTag](expectedValue: T) = {
      new SingleSnapshotComplianceCheckTemplateRule(
        ruleName = ruleName,
        ruleFriendlyName = ruleFriendlyName,
        ruleDescription = ruleDescription,
        severity = severity,
        metricName = metricName,
        baseRemediationText = baseRemediationText ,
        metaCondition = metaCondition,
        expectedValue = expectedValue,
        parameterName = parameterName,
        parameterDescription = parameterDescription,
        ruleCategories = ruleCategories,
        deviceCategory = deviceCategory
      )(vendorToRemediationText: _*)


    }
   expectedValue match {
      case str: String => createInstance[String](str)
      case bool: Boolean => createInstance[Boolean](bool)
      case double: Double => createInstance[Double](double)
    }
  }

}
