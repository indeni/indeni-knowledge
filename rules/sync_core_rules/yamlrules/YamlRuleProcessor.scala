package com.indeni.server.rules.library.yamlrules

import com.indeni.server.rules.Rule
import com.indeni.server.rules.library._
import com.indeni.server.rules.library.templates._
import net.jcazevedo.moultingyaml.{YamlObject, _}
import org.yaml.snakeyaml.parser.ParserException

import scala.io.Source

class YamlRuleProcessor(val config: YamlRuleProcessorConfig) {
  val RULE_TYPE_FIELD = YamlString("rule_type")
  val TEMPLATE_NAME_FIELD = YamlString("template_name")
  val tagsStoreConditionBuilder = new TagsStoreConditionBuilder(config.tagsStoreConditionBuilderConfig)
  // TODO: Consider handling of common fields like rule name, rule friendly name, rule description and alert severity
  // in a common context in this class. But note that there is currently one template that doesn't have those fields,
  // namely DataplanePoolUsageTemplateRule .
  @throws[YamlRuleParseException]
  def createRuleFromYamlString(rawYaml: String): PerDeviceRule = {
    val utils = new YamlRuleProcessorUtils(config)
    val yamlValue = try {
      rawYaml.parseYaml
    } catch {
      case e: ParserException => throw YamlRuleParseException(s"Error in YAML format:  ${e.getMessage}")
    }
    yamlValue match {
      case yamlObj @ YamlObject(fields) => {
        fields.get(RULE_TYPE_FIELD) match {
          case Some(YamlString("core-rule")) => {
            val ruleDescriptor = YamlCoreRuleDescriptor.fromYaml(yamlObj, utils)
            val yamlCoreRule = new YamlCoreRule(ruleDescriptor, config.conditionExpressionBuilderConfig, tagsStoreConditionBuilder)
            yamlCoreRule.validate()
            yamlCoreRule
          }
          case Some(YamlString("template-based")) => {
            fields.get(TEMPLATE_NAME_FIELD) match {
              case Some(YamlString(templateName)) =>
                templateName match {
                  case "ClusterMemberNoLongerActive" => ClusterMemberNoLongerActiveTemplateRule.fromYaml(yamlObj, utils)
                  case "CounterIncrease"    => CounterIncreaseTemplateRule.fromYaml(yamlObj, utils)
                  case "NumericValueNotChanged"    => NumericValueNotChangedTemplateRule.fromYaml(yamlObj, utils)
                  case "DataplanePoolUsage" => DataplanePoolUsageTemplateRule.fromYaml(yamlObj, utils)
                  case "EnabledInterfaceSnapshotComparison" =>
                    EnabledInterfaceSnapshotComparisonTemplateRule.fromYaml(yamlObj, utils)
                  case "MultiSnapshotAllValuesExistInSnapshot" =>
                    MultiSnapshotAllValuesExistInSnapshotTemplateRule.fromYaml(yamlObj, utils)
                  case "MultiSnapshotComplianceCheck" => MultiSnapshotComplianceCheckTemplateRule.fromYaml(yamlObj, utils)
                  case "MultiSnapshotValueCheck"      => MultiSnapshotValueCheckTemplateRule.fromYaml(yamlObj, utils)
                  case "NearingCapacity"              => NearingCapacityTemplateRule.fromYaml(yamlObj, utils)
                  case "NearingCapacityWithTimeFrame"     => NearingCapacityWithTimeFrameTemplateRule.fromYaml(yamlObj, utils)
                  case "NearingCapacityWithTimeFrameWithItems"     => NearingCapacityWithTimeFrameWithItemsTemplateRule.fromYaml(yamlObj, utils)
                  case "NearingCapacityWithItems"     => NearingCapacityWithItemsTemplateRule.fromYaml(yamlObj, utils)
                  case "NumericThresholdOnComplexMetricWithItems" =>
                    NumericThresholdOnComplexMetricWithItemsTemplateRule.fromYaml(yamlObj, utils)
                  case "NumericThresholdOnDoubleMetric" => NumericThresholdOnDoubleMetricTemplateRule.fromYaml(yamlObj, utils)
                  case "NumericThresholdOnDoubleMetricWithItems" =>
                    NumericThresholdOnDoubleMetricWithItemsTemplateRule.fromYaml(yamlObj, utils)
                  case "SecurityVulnerabilityChecks"   => SecurityVulnerabilityChecksTemplateRule.fromYaml(yamlObj, utils)
                  case "SingleSnapshotComplianceCheck" => SingleSnapshotComplianceCheckTemplateRule.fromYaml(yamlObj, utils)
                  case "SingleSnapshotValueCheck"      => SingleSnapshotValueCheckTemplateRule.fromYaml(yamlObj, utils)
                  case "SnapshotComparison"            => SnapshotComparisonTemplateRule.fromYaml(yamlObj, utils)
                  case "StateDown"                     => StateDownTemplateRule.fromYaml(yamlObj, utils)
                  case "TimeIntervalThresholdOnDoubleMetric" =>
                    TimeIntervalThresholdOnDoubleMetricTemplateRule.fromYaml(yamlObj, utils)
                  case "TimeThresholdOnDoubleMetricWithItems" =>
                    TimeThresholdOnDoubleMetricWithItemsTemplateRule.fromYaml(yamlObj, utils)
                  case "VpnTunnelIsDown" => VpnTunnelIsDownTemplateRule.fromYaml(yamlObj, utils)
                  case "ValueNearExpirationDate" => ValueNearExpirationDateTemplateRule.fromYaml(yamlObj, utils)
                  case "MultiConditionTimeSeries" => MultiConditionTimeSeriesTemplateRule.fromYaml(yamlObj, utils)
                  case "MetricAnomalyDetection" => MetricAnomalyTemplateRule.fromYaml(yamlObj, utils)
                  case otherTemplateName => throw YamlRuleParseException(s"Unrecognized template name: $otherTemplateName")
                }
              case Some(other) =>
                throw YamlRuleParseException(s"Expected a string as value of $TEMPLATE_NAME_FIELD. Instead got: $other")
              case None => throw YamlRuleParseException(s"Mandatory field missing: $TEMPLATE_NAME_FIELD")
            }
          }
          case Some(other) => throw YamlRuleParseException(s"Expected a string as value of $RULE_TYPE_FIELD. Instead got: $other")
          case None => throw YamlRuleParseException(s"Mandatory field missing: $RULE_TYPE_FIELD")
        }
      }
      case _ => throw YamlRuleParseException("Expected a yaml object at root position")
    }
  }

  @throws[YamlRuleParseException]
  def createRuleFromYamlFile(yamlFilePath: String): Rule = {
    val source = Source.fromFile(yamlFilePath, "UTF-8")
    try {
      createRuleFromYamlString(source.mkString)
    } finally {
      source.close()
    }
  }
}
