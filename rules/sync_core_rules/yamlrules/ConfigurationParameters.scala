package com.indeni.server.rules.library.yamlrules

sealed trait ConfigurationParameterType
case object StringParameterType extends ConfigurationParameterType
case object DoubleParameterType extends ConfigurationParameterType

sealed trait ConfigurationParameterDescriptor {
  def parameterType: ConfigurationParameterType
}

case class StringConfigurationParameterDescriptor(defaultValue: String) extends ConfigurationParameterDescriptor {
  override def parameterType: ConfigurationParameterType = StringParameterType
}
case class DoubleConfigurationParameterDescriptor(defaultValue: Double) extends ConfigurationParameterDescriptor {
  override def parameterType: ConfigurationParameterType = DoubleParameterType
}

