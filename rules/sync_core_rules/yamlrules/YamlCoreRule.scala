package com.indeni.server.rules.library.yamlrules

import com.indeni.ruleengine.expressions.core.{ConstantExpression, StatusTreeExpression}
import com.indeni.ruleengine.expressions.data.SelectTagsExpression
import com.indeni.server.common.data.conditions.True
import com.indeni.server.rules
import com.indeni.server.rules.library.ConditionalRemediationSteps.ConditionalRemediationStepKey
import com.indeni.server.rules.library._
import com.indeni.server.rules.{RuleContext, RuleMetadata}

class YamlCoreRule(ruleDescriptor: YamlCoreRuleDescriptor, conditionExpressionBuilderConfig: ConditionExpressionBuilderConfig,
                   tagsStoreConditionBuilder: TagsStoreConditionBuilder) extends PerDeviceRule with RuleHelper {
  private val metricsCondition = ruleDescriptor.metricsCondition
  private val requiresConditionExpr = ruleDescriptor.requiresCondition.map(tagsStoreConditionBuilder.buildFromAST).getOrElse(True)
  private val conditionBuilder = new ConditionFromFullMetricConditionBuilder(conditionExpressionBuilderConfig)
  private val alertItemsSection = ruleDescriptor.idToAlertItemsDescriptor
  private val configParameters = ruleDescriptor.configParameters

  /**
    * Validates the relationship between the metrics condition (*) and the config parameters / alert items ids
    *
    * (*) maybe in the future also tags condition a.k.a. requiresConditionExpr
    */
  @throws[YamlRuleParseException]
  def validate(): Unit = {
    validateAlertItemIds()
    validateConfigParameters()
  }

  private def validateConfigParameters(): Unit = {
    val configParametersUsed: Set[(String, Option[ConfigurationParameterType])] = RuleHelper
      .getAllConfigParametersFromFullMetricsCondition(metricsCondition) ++ alertItemsSection.values
      .flatMap(alertItems => RuleHelper.getAllReferencesFromAlertItems(alertItems, TemplateStringHelper.ConfigSection))
      .map((_, None))
    val configParametersErrors = configParametersUsed.flatMap {
      case (parameterName, optionOfType) => {
        val configParameterDescriptor = configParameters.get(parameterName)
        if (configParameterDescriptor.isEmpty)
          Some(s"Unknown config parameter: $parameterName")
        else
          optionOfType match {
            case Some(parameterType) =>
              if (parameterType != configParameterDescriptor.get.parameterType)
                Some(s"Use of config parameter $parameterName is not compatible with its type")
              else
                None
            case None => None
          }
      }
    }
    if (configParametersErrors.nonEmpty)
      throw YamlRuleParseException(
        s"Configuration parameter errors found in rule: ${configParametersErrors.mkString("; ")}")
  }

  /**
    * Alert item ids can appear in the metrics condition and in the Alert Items section. We check that the same ids appear
    * in both places
    */
  private def validateAlertItemIds(): Unit = {
    val idsInCondition = RuleHelper.getAllAlertItemIdsFromFullMetricsCondition(metricsCondition)
    val idsInAlertItemsSection = alertItemsSection.keySet
    val idsOnlyInCondition = idsInCondition.diff(idsInAlertItemsSection)
    if (idsOnlyInCondition.nonEmpty)
      throw YamlRuleParseException(
        s"Alert item ids found in condition but not in alert_items section: ${idsOnlyInCondition.mkString(",")}")
    val idsOnlyInAlertItemsSection = idsInAlertItemsSection.diff(idsInCondition)
    if (idsOnlyInAlertItemsSection.nonEmpty)
      throw YamlRuleParseException(
        s"Alert item ids found in alert items section but not in condition: ${idsOnlyInAlertItemsSection.mkString(",")}")
  }

  override def expressionTree(context: RuleContext): StatusTreeExpression = {
    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(rules.DeviceKey), requiresConditionExpr),
      conditionBuilder.buildFromAST(metricsCondition, configParameters, alertItemsSection, context)
    ).withRootInfo(
      ruleDescriptor.alertHeadline.map(ConstantExpression(_)).getOrElse(getHeadline()),
      ConstantExpression(ruleDescriptor.alertDescription),
      ConditionalRemediationSteps(
        ruleDescriptor.baseRemediationText.getOrElse(""),
        ruleDescriptor.vendorToRemediationText
          .map(_.map ({ case (k, v) => ConditionalRemediationStepKey(k.getVendor, Option(k.getOsName)) -> v }))
          .getOrElse(Map.empty)
      )
    )
  }

  override def metadata: RuleMetadata = {
    import ruleDescriptor._
    RuleMetadata.builder(ruleName, ruleFriendlyName, ruleDescription, alertSeverity, ruleCategories, deviceCategory)
      .build()
  }
}
