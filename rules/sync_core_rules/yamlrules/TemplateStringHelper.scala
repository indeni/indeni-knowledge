package com.indeni.server.rules.library.yamlrules
import java.util.regex.Matcher

import com.indeni.ruleengine.expressions.scope.ScopableExpression
import com.indeni.server.rules.library.RuleHelper

case class InvalidTemplateString(msg: String) extends Exception(msg)
case class InvalidSubstitution(msg: String) extends Exception(msg)

object TemplateStringHelper {
  val ConfigSection = "config"
  val MetricTagSection = "metricTag"
  val DeviceTagSection = "deviceTag"
  // TODO: Consider using an enum/case objects for sections instead of strings in inputs/outputs of public methods of this class
  private val sectionNames = Seq(ConfigSection, MetricTagSection, DeviceTagSection)
  private val singleSubstitution = """\$\{(""" + sectionNames.map(sectionName => s"(?:$sectionName)").mkString("|") + """):([a-zA-Z0-9._-]+)}"""
  private val singleSubstitutionRegex = singleSubstitution.r
  private val entireStringPredicate = ("""^[^$%]*(?:(?:""" + singleSubstitution + """)[^$%]*)*$""").r.pattern.asPredicate

  @throws[InvalidTemplateString]
  def validate(s: String): Unit = {
    if (!entireStringPredicate.test(s)) {
      throw InvalidTemplateString("Invalid use of ${} substitutions. Syntax is ${section:identifier} where section is one of: " + sectionNames.mkString(","))
    }
  }

  @throws[InvalidSubstitution]
  def instantiateToString(s: String, sectionsToKeysToValues: Map[String, Map[String, String]]): String = {
    require(sectionsToKeysToValues.keySet.subsetOf(sectionNames.toSet))
    validate(s)
    singleSubstitutionRegex.replaceAllIn(s, m => m match {
      case singleSubstitutionRegex(sectionName, key) => Matcher.quoteReplacement(
        sectionsToKeysToValues
          .getOrElse(sectionName, throw InvalidSubstitution(s"Invalid section $sectionName"))
          .getOrElse(key, throw InvalidSubstitution(s"Invalid key $key in section $sectionName"))
      )
    })
  }

  def getKeysUsed(s: String): Map[String, Set[String]] = {
    validate(s)
    singleSubstitutionRegex.findAllIn(s).map {
      case singleSubstitutionRegex(sectionName, key) => {
        if (!sectionNames.contains(sectionName))
          throw InvalidSubstitution("Invalid section $sectionName")
        sectionName -> key
      }
    }.toSeq.groupBy(_._1).mapValues(_.map(_._2).toSet)
  }

  /**
    * Currently only supports metric tags and configuration parameters.
    * TODO: Add support for device tags if needed
    * TODO: Add support for metric values if needed
    */
  @throws[InvalidSubstitution]
  def instantiateToExpression(s: String, configParameters: Map[String, ConfigurationParameterDescriptor]): ScopableExpression[String] = {
    validate(s)
    val configParameterExpressions = singleSubstitutionRegex.findAllIn(s).collect {
      case singleSubstitutionRegex(ConfigSection, key) =>
        configParameters.get(key) match {
          case Some(StringConfigurationParameterDescriptor(defaultValue)) => RuleHelper.getParameterString(key, defaultValue)
          case Some(DoubleConfigurationParameterDescriptor(defaultValue)) => RuleHelper.getParameterDouble(key, defaultValue)
          case None => throw InvalidSubstitution(s"Unknown configuration parameter: $key")
        }
    }
    val formatString = singleSubstitutionRegex.replaceAllIn(s, m => m match {
      case singleSubstitutionRegex(sectionName, key) => Matcher.quoteReplacement({
        sectionName match {
          case ConfigSection => configParameters.getOrElse(key, throw InvalidSubstitution(s"Unknown config parameter: $key")).parameterType match {
            case StringParameterType => "%s"
            case DoubleParameterType => "%.2f"
          }
          case DeviceTagSection => throw new NotImplementedError("Device tags not implemented yet")
          case MetricTagSection => "${scope(\"" + key + "\")}"
          case _ => throw InvalidSubstitution(s"Invalid section encountered: $sectionName")
        }
      })
    })
    RuleHelper.scopableStringFormatExpression(formatString, configParameterExpressions.toSeq: _*)
  }
}
