package com.indeni.server.rules.library.yamlrules
import com.indeni.server.rules.library.{ConditionNode, FullMetricConditionType, TagConditionType}
import com.indeni.server.rules.{DeviceCategory, RemediationStepCondition, RuleCategory}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import net.jcazevedo.moultingyaml.YamlObject

case class YamlCoreRuleDescriptor(
    ruleName: String, author: String, ruleFriendlyName: String, ruleDescription: String, configParameters: Map[String, ConfigurationParameterDescriptor],
    requiresCondition: Option[ConditionNode[TagConditionType.type]], metricsCondition: ConditionNode[FullMetricConditionType.type],
    alertSeverity: AlertSeverity, alertHeadline: Option[String], alertDescription: String, idToAlertItemsDescriptor: Map[String, AlertItemsDescriptor],
    ruleCategories: Set[RuleCategory], deviceCategory: DeviceCategory, baseRemediationText: Option[String], vendorToRemediationText: Option[Map[RemediationStepCondition, String]]) {
}

object YamlCoreRuleDescriptor {
  def fromYaml(yaml: YamlObject, utils: YamlRuleProcessorUtils): YamlCoreRuleDescriptor = {
    import YamlRuleProcessorUtils._
    import utils._
    val ruleName = utils.readMandatory(yaml, RuleNameField, stringParser)
    val author = utils.readMandatory(yaml, AuthorField, stringParser)
    val ruleFriendlyName = utils.readMandatory(yaml, RuleFriendlyNameField, stringParser)
    val ruleDescription = utils.readMandatory(yaml, RuleDescriptionField, stringParser)
    val configParameters = utils.readOptionalMap(yaml, ConfigurationParametersField, stringParser, configParametersParser).getOrElse(Map.empty)
    val requiresCondition = utils.readOptional(yaml, RequiresField, tagsConditionParser)
    val metricsCondition = utils.readMandatory(yaml, MetricsConditionField, fullMetricsConditionParser)
    val alertSeverity = utils.readMandatory(yaml, SeverityField, alertSeverityParser)
    val alertHeadline = utils.readOptional(yaml, AlertHeadlineField, stringParser)
    val alertDescription = utils.readMandatory(yaml, AlertDescriptionField, stringParser)
    val idToAlertItemsDescriptor = utils.readOptionalMap(yaml, AlertItemsField, stringParser, alertItemsDescriptorParser).getOrElse(Map.empty)
    val ruleCategories = utils.readMandatorySet(yaml, RuleCategoriesField, ruleCategoryParser)
    val deviceCategory = utils.readMandatory(yaml, DeviceCategoryField, deviceCategoryParser)
    val baseRemediationText = utils.readOptional(yaml, BaseRemediationTextField, stringParser)
    val vendorToRemediationText = utils.readOptionalVendorToRemediationTextRaw(yaml, VendorToRemediationTextField)
    YamlCoreRuleDescriptor(ruleName, author, ruleFriendlyName, ruleDescription, configParameters, requiresCondition, metricsCondition, alertSeverity,
      alertHeadline, alertDescription, idToAlertItemsDescriptor, ruleCategories, deviceCategory, baseRemediationText, vendorToRemediationText)
  }
}
