package com.indeni.server.rules.library.yamlrules
import java.util.regex.PatternSyntaxException

import com.indeni.ruleengine.expressions.conditions.Condition
import com.indeni.server.common.data.conditions.TagsStoreCondition
import com.indeni.server.rules.library._
import net.jcazevedo.moultingyaml.{YamlArray, YamlBoolean, YamlNumber, YamlObject, YamlString, YamlValue}

import scala.concurrent.duration.{Duration, FiniteDuration}
import scala.util.matching.Regex

trait Parser[T] {
  @throws[YamlRuleParseException]
  def parse(yaml: YamlValue): T
}

class PairParser[A, B](firstParser: Parser[A], secondParser: Parser[B], config: YamlRuleProcessorConfig) extends Parser[(A, B)] {
  @throws[YamlRuleParseException]
  override def parse(yaml: YamlValue): (A, B) = yaml match {
    case YamlArray(elements) =>
      if (elements.size != 2)
        throw YamlRuleParseException(s"Expected pair (array of size 2); got array of size ${elements.size}")
      else
        (firstParser.parse(elements(0)), secondParser.parse(elements(1)))
    case other => throw YamlRuleParseException(s"Expected pair (array of size 2); got: ${other.prettyPrint}")
  }
}

class StringParser(config: YamlRuleProcessorConfig) extends Parser[String] {
  @throws[YamlRuleParseException]
  override def parse(yaml: YamlValue): String = yaml match {
    case YamlString(str) => str
    case other => throw YamlRuleParseException(s"Expected string; got: ${other.prettyPrint}")
  }
}

class BooleanParser(config: YamlRuleProcessorConfig) extends Parser[Boolean] {
  @throws[YamlRuleParseException]
  override def parse(yaml: YamlValue): Boolean = yaml match {
    case YamlBoolean(b) => b
    case other => throw YamlRuleParseException(s"Expected boolean; got: ${other.prettyPrint}")
  }
}

class DoubleParser(config: YamlRuleProcessorConfig) extends Parser[Double] {
  @throws[YamlRuleParseException]
  override def parse(yaml: YamlValue): Double = yaml match {
    // TODO: Is this good for all usecases or do we need a parser that keeps the value as a BigDecimal ?
    case YamlNumber(value) => value.toDouble
    case other => throw YamlRuleParseException(s"Expected number; got: ${other.prettyPrint}")
  }
}

class IntParser(config: YamlRuleProcessorConfig) extends Parser[Int] {
  val doubleParser = new DoubleParser(config)

  @throws[YamlRuleParseException]
  override def parse(yaml: YamlValue): Int = {
    val double = doubleParser.parse(yaml)
    if (double.isValidInt) double.toInt else throw YamlRuleParseException(s"Expected integer; got: $double")
  }
}

class IdentityParser(config: YamlRuleProcessorConfig) extends Parser[YamlValue] {
  override def parse(yamlValue: YamlValue): YamlValue = yamlValue
}

/**
  * Can read either string, boolean, or double. Returns a java.lang.Object that can be either a
  * String, a java Boolean or a java Double
  */
class PrimitiveValueParser(config: YamlRuleProcessorConfig) extends Parser[Any] {
  @throws[YamlRuleParseException]
  override def parse(yaml: YamlValue): Any = yaml match {
    case YamlString(str) => str
    case YamlBoolean(b) => java.lang.Boolean.valueOf(b)
    case YamlNumber(value) => java.lang.Double.valueOf(value.toDouble)
    case other => throw YamlRuleParseException(s"Expected primitive value (string, boolean, or number); got: ${other.prettyPrint}")
  }
}

class RegexParser(config: YamlRuleProcessorConfig) extends Parser[Regex] {
  val stringParser = new StringParser(config)

  @throws[YamlRuleParseException]
  override def parse(yaml: YamlValue): Regex = {
    val str = stringParser.parse(yaml)
    try {
      str.r
    } catch {
      case e: PatternSyntaxException => throw YamlRuleParseException(s"Invalid regex $str: ${e.getMessage}")
    }
  }
}

class AlertItemsDescriptorParser(config: YamlRuleProcessorConfig) extends Parser[AlertItemsDescriptor] {
  private val TitleField = "title"
  private val HeadlineField = "headline"
  private val DescriptionField = "description"
  private val stringParser = new StringParser(config)
  private val mapParser = new MapParser(config, stringParser, stringParser)

  @throws[YamlRuleParseException]
  override def parse(yaml: YamlValue): AlertItemsDescriptor = {
    val map = mapParser.parse(yaml)
    val exceptionMsg = s"Expected exactly 3 string fields: $TitleField, $HeadlineField, $DescriptionField"
    if (map.size != 3) {
      throw YamlRuleParseException(exceptionMsg)
    }
    val title :: headline :: description :: Nil = try {
      Seq(TitleField, HeadlineField, DescriptionField).map(map)
    } catch {
      case _: NoSuchElementException => throw YamlRuleParseException(exceptionMsg)
    }
    if (title.contains("${"))
      throw YamlRuleParseException("No ${} substitutions are allowed in title field")
    Seq((HeadlineField, headline), (DescriptionField, description)).foreach {
      case (fieldName, fieldValue) => try {
        TemplateStringHelper.validate(fieldValue)
      } catch {
        case e: InvalidTemplateString => throw YamlRuleParseException(s"Error in $fieldName: ${e.msg}")
      }
    }
    AlertItemsDescriptor(title, headline, description)
  }
}

class ConfigParametersDescriptorParser(config: YamlRuleProcessorConfig) extends Parser[ConfigurationParameterDescriptor] {
  private val TypeField = "type"
  private val DefaultValueField = "default_value"
  private val StringType = "string"
  private val DoubleType = "double"
  private val stringParser = new StringParser(config)
  private val doubleParser = new DoubleParser(config)
  private val typeParser: EnumParser[ConfigurationParameterType] = new EnumParser(config, Set(StringType, DoubleType), {
    case StringType => StringParameterType
    case DoubleType => DoubleParameterType
  })
  private val identityParser = new IdentityParser(config)
  private val mapParser = new MapParser(config, stringParser, identityParser)

  @throws[YamlRuleParseException]
  override def parse(yaml: YamlValue): ConfigurationParameterDescriptor = {
    val map = mapParser.parse(yaml)
    val exceptionMsg = s"Expected exactly 2 fields: $TypeField, $DefaultValueField"
    if (map.size != 2) {
      throw YamlRuleParseException(exceptionMsg)
    }
    val parameterType :: defaultValue :: Nil = try {
      Seq(TypeField, DefaultValueField).map(map)
    } catch {
      case _: NoSuchElementException => throw YamlRuleParseException(exceptionMsg)
    }
    typeParser.parse(parameterType) match {
      case StringParameterType => StringConfigurationParameterDescriptor(stringParser.parse(defaultValue))
      case DoubleParameterType => DoubleConfigurationParameterDescriptor(doubleParser.parse(defaultValue))
    }
  }
}


class EnumParser[T](config: YamlRuleProcessorConfig, valuesSet: Set[String], constructor: String => T) extends Parser[T] {
  private val stringParser = new StringParser(config)
  @throws[YamlRuleParseException]
  override def parse(yaml: YamlValue): T = {
    val str = stringParser.parse(yaml)
    if (valuesSet.contains(str)) {
      constructor(str)
    } else
      throw YamlRuleParseException(s"Invalid enum value $str. Expected one of $valuesSet")
  }
}

class ListParser[T](config: YamlRuleProcessorConfig, elementParser: Parser[T]) extends Parser[List[T]] {
  @throws[YamlRuleParseException]
  override def parse(yaml: YamlValue): List[T] = yaml match {
    case YamlArray(elements) => elements.map(elementParser.parse).toList
    case other => throw YamlRuleParseException(s"Expected array; got: ${other.prettyPrint}")
  }
}

class SetParser[T](config: YamlRuleProcessorConfig, elementParser: Parser[T]) extends Parser[Set[T]] {
  val listParser = new ListParser(config, elementParser)
  @throws[YamlRuleParseException]
  override def parse(yaml: YamlValue): Set[T] = {
    val list = listParser.parse(yaml)
    val duplicates = list.groupBy(identity).collect{ case (value, occurrences) if occurrences.size > 1 => value }
    if (duplicates.nonEmpty)
      throw YamlRuleParseException(s"Duplicates detected in set: $duplicates")
    list.toSet
  }
}

class MapParser[K, V](config: YamlRuleProcessorConfig, keyParser: Parser[K], valueParser: Parser[V]) extends Parser[Map[K, V]] {
  @throws[YamlRuleParseException]
  override def parse(yaml: YamlValue): Map[K, V] = yaml match {
    case YamlObject(fields) => fields.map{
      case (key, value) => (keyParser.parse(key), valueParser.parse(value))
    }
    case other => throw YamlRuleParseException(s"Expected object; got: ${other.prettyPrint}")
  }
}

class YamlTagsConditionParser(config: YamlRuleProcessorConfig) extends Parser[ConditionNode[TagConditionType.type]] {
  private val stringParser = new StringParser(config)
  private val parser = new TagsConditionParser(config.conditionParserConfig)

  @throws[YamlRuleParseException]
  override def parse(yaml: YamlValue): ConditionNode[TagConditionType.type] = {
    val str = stringParser.parse(yaml)
    parser.parseString(str) match {
      case Right(ast) => ast
      case Left(msg) => throw YamlRuleParseException(msg)
    }
  }
}

class MetaConditionParser(config: YamlRuleProcessorConfig) extends Parser[TagsStoreCondition] {
  private val yamlTagsConditionParser = new YamlTagsConditionParser(config)
  private val builder = new TagsStoreConditionBuilder(config.tagsStoreConditionBuilderConfig)

  @throws[YamlRuleParseException]
  override def parse(yaml: YamlValue): TagsStoreCondition = {
    val ast = yamlTagsConditionParser.parse(yaml)
    builder.buildFromAST(ast)
  }
}

class YamlSimpleMetricsConditionParser(config: YamlRuleProcessorConfig) extends Parser[ConditionNode[SimpleMetricConditionType.type]] {
  private val stringParser = new StringParser(config)
  private val parser = new SimpleMetricsConditionParser(config.conditionParserConfig)

  @throws[YamlRuleParseException]
  override def parse(yaml: YamlValue): ConditionNode[SimpleMetricConditionType.type] = {
    val str = stringParser.parse(yaml)
    parser.parseString(str) match {
      case Right(ast) => ast
      case Left(msg) => throw YamlRuleParseException(msg)
    }
  }
}

class YamlFullMetricsConditionParser(config: YamlRuleProcessorConfig) extends Parser[ConditionNode[FullMetricConditionType.type]] {
  private val stringParser = new StringParser(config)
  private val parser = new FullMetricsConditionParser(config.conditionParserConfig)

  @throws[YamlRuleParseException]
  override def parse(yaml: YamlValue): ConditionNode[FullMetricConditionType.type] = {
    val str = stringParser.parse(yaml)
    parser.parseString(str) match {
      case Right(ast) => ast
      case Left(msg) => throw YamlRuleParseException(msg)
    }
  }
}

class ComplexConditionParser(config: YamlRuleProcessorConfig) extends Parser[Condition] {
  private val yamlMetricsConditionParser = new YamlSimpleMetricsConditionParser(config)
  private val builder = new ConditionFromSimpleMetricConditionBuilder(config.conditionExpressionBuilderConfig, None)

  @throws[YamlRuleParseException]
  override def parse(yaml: YamlValue): Condition = {
    val ast = yamlMetricsConditionParser.parse(yaml)
    builder.buildFromAST(ast)
  }
}

/**
  * Parses string such as "5 seconds"
  */
class TimeIntervalParser(config: YamlRuleProcessorConfig) extends Parser[FiniteDuration] {
  val stringParser = new StringParser(config)
  @throws[YamlRuleParseException]
  override def parse(yaml: YamlValue): FiniteDuration = {
    val str = stringParser.parse(yaml)
    try {
      Duration(str) match {
        case x : FiniteDuration => x
        case _ => throw YamlRuleParseException(s"Invalid time interval string: $str")
      }
    } catch {
      case _: NumberFormatException => throw YamlRuleParseException(s"Invalid time interval string: $str")
    }
  }
}

