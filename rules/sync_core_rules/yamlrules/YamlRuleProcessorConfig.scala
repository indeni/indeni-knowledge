package com.indeni.server.rules.library.yamlrules
import com.indeni.server.rules.library.{ConditionExpressionBuilderConfig, ConditionParserConfig, TagsStoreConditionBuilderConfig}

case class YamlRuleProcessorConfig(conditionParserConfig: ConditionParserConfig,
                                   tagsStoreConditionBuilderConfig: TagsStoreConditionBuilderConfig,
                                   conditionExpressionBuilderConfig: ConditionExpressionBuilderConfig)
