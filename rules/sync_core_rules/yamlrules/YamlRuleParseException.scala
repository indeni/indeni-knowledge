package com.indeni.server.rules.library.yamlrules

case class YamlRuleParseException(msg: String) extends Exception(msg)
