package com.indeni.server.rules.library.yamlrules
import com.indeni.apidata.time.TimeSpan
import com.indeni.apidata.time.TimeSpan.TimePeriod
import com.indeni.server.rules.{DeviceCategory, RemediationStepCondition, RuleCategory, ThresholdDirection}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import net.jcazevedo.moultingyaml.{YamlObject, YamlString}

import scala.concurrent.duration.FiniteDuration


class YamlRuleProcessorUtils(val config: YamlRuleProcessorConfig) {
  val alertItemsDescriptorParser = new AlertItemsDescriptorParser(config)
  val configParametersParser = new ConfigParametersDescriptorParser(config)
  val booleanParser = new BooleanParser(config)
  val complexConditionParser = new ComplexConditionParser(config)
  val doubleParser = new DoubleParser(config)
  val fullMetricsConditionParser = new YamlFullMetricsConditionParser(config)
  val intParser = new IntParser(config)
  val metaConditionParser = new MetaConditionParser(config)
  val primitiveValueParser = new PrimitiveValueParser(config)
  val regexTypeParser = new RegexParser(config)
  val simpleMetricsConditionParser = new YamlSimpleMetricsConditionParser(config)
  val stringParser = new StringParser(config)
  val tagsConditionParser = new YamlTagsConditionParser(config)
  val timeIntervalParser = new TimeIntervalParser(config)

  val stringPairTypeParser = new PairParser(stringParser, stringParser, config)
  val regexStringPairTypeParser = new PairParser(regexTypeParser, stringParser, config)

  val alertSeverityParser = new EnumParser[AlertSeverity](config, AlertSeverity.values().toSet.map((x: AlertSeverity) => x.toString), AlertSeverity.valueOf)
  val deviceCategoryParser = new EnumParser[DeviceCategory](config, DeviceCategory.values().toSet.map((x: DeviceCategory) => x.toString), DeviceCategory.valueOf)
  val thresholdDirectionParser = new EnumParser[ThresholdDirection](config, ThresholdDirection.values().toSet.map((x: ThresholdDirection) => x.toString), ThresholdDirection.valueOf)
  val timePeriodParser = new EnumParser[TimePeriod](config, TimePeriod.values().toSet.map((x: TimePeriod) => x.toString), TimePeriod.valueOf)
  val remediationStepConditionParser = new EnumParser[RemediationStepCondition](config, RemediationStepCondition.values().toSet.map((x: RemediationStepCondition) => x.toString), RemediationStepCondition.valueOf)
  val ruleCategoryParser = new EnumParser[RuleCategory](config, RuleCategory.values().toSet.map((x: RuleCategory) => x.toString), RuleCategory.valueOf)

  @throws[YamlRuleParseException]
  def readOptional[T](yaml: YamlObject, fieldName: String, parser: Parser[T]): Option[T] = {
    yaml.fields.get(YamlString(fieldName)).map(yamlValue =>
      try {
        parser.parse(yamlValue)
      } catch {
        case e: YamlRuleParseException => throw YamlRuleParseException(s"Error parsing field $fieldName: ${e.msg}")
      }
    )
  }

  @throws[YamlRuleParseException]
  def readMandatory[T](yaml: YamlObject, fieldName: String, parser: Parser[T]): T = {
    readOptional(yaml, fieldName, parser).getOrElse {
      throw YamlRuleParseException(s"Missing field $fieldName")
    }
  }

  @throws[YamlRuleParseException]
  def readMandatoryList[T](yaml: YamlObject, fieldName: String, elementParser: Parser[T]): List[T] =
    readMandatory(yaml, fieldName, new ListParser(config, elementParser))

  @throws[YamlRuleParseException]
  def readOptionalList[T](yaml: YamlObject, fieldName: String, elementParser: Parser[T]): Option[List[T]] =
    readOptional(yaml, fieldName, new ListParser(config, elementParser))

  @throws[YamlRuleParseException]
  def readOptionalNonemptyList[T](yaml: YamlObject, fieldName: String, elementParser: Parser[T]): Option[List[T]] = {
    val l = readOptionalList(yaml, fieldName, elementParser)
    l.foreach(l => if (l.isEmpty) throw YamlRuleParseException(s"Expected nonempty array for field $fieldName"))
    l
  }

  @throws[YamlRuleParseException]
  def readOptionalSet[T](yaml: YamlObject, fieldName: String, elementParser: Parser[T]): Option[Set[T]] =
    readOptional(yaml, fieldName, new SetParser(config, elementParser))

  @throws[YamlRuleParseException]
  def readMandatorySet[T](yaml: YamlObject, fieldName: String, elementParser: Parser[T]): Set[T] =
    readMandatory(yaml, fieldName, new SetParser(config, elementParser))

  @throws[YamlRuleParseException]
  def readOptionalMap[K, V](yaml: YamlObject, fieldName: String, keyParser: Parser[K], valueParser: Parser[V]): Option[Map[K, V]] =
    readOptional(yaml, fieldName, new MapParser(config, keyParser, valueParser))

  @throws[YamlRuleParseException]
  def readOptionalVendorToRemediationTextRaw(yaml: YamlObject, fieldName: String): Option[Map[RemediationStepCondition, String]] =
    readOptionalMap(yaml, fieldName, remediationStepConditionParser, stringParser)

  @throws[YamlRuleParseException]
  def readOptionalVendorToRemediationText(yaml: YamlObject, fieldName: String): List[(RemediationStepCondition, String)] =
    readOptionalVendorToRemediationTextRaw(yaml, fieldName)
      .getOrElse(Map.empty)
      .toList

  @throws[YamlRuleParseException]
  def validateNoExtraFields(yaml: YamlObject, fields: Set[String]): Unit = {
    val allFields = fields.union(YamlRuleProcessorUtils.GENERIC_FIELDS)
    val extraFields = yaml.fields.keySet.collect {
      case YamlString(value) if !allFields.contains(value) => value
      case nonStringValue if !nonStringValue.isInstanceOf[YamlString] => nonStringValue.prettyPrint.trim
    }
    if (extraFields.nonEmpty)
      throw YamlRuleParseException(s"Unexpected fields: ${extraFields.mkString(", ")}")
  }
}
object YamlRuleProcessorUtils {
  val GENERIC_FIELDS: Set[String] = Set("template_name", "rule_type", "supported", "not_supported")

  def convertFiniteDurationToTimeSpan(finiteDuration: FiniteDuration): TimeSpan = {
    TimeSpan.fromMilliseconds(finiteDuration.toMillis)
  }

  val AddNetworkInterfaceAdminStateSecondaryConditionField = "add_network_interface_admin_state_secondary_condition"
  val AlertDescriptionField = "alert_description"
  val AlertDescriptionFormatField = "alert_description_format"
  val AlertDescriptionValueUnitsField = "alert_description_value_units"
  val AlertHeadlineField = "alert_headline"
  val AlertIfDownField = "alert_if_down"
  val AlertValueField = "alert_value"
  val AlertIfMembersValuesAreTheSameField = "alert_if_members_values_are_the_same"
  val AlertInfoMetricTagField = "alert_info_metric_tag"
  val AlertItemDescriptionFormatField = "alert_item_description_format"
  val AlertItemDescriptionUnitsField = "alert_item_description_units"
  val AlertItemHeadlineStrMetricField = "alert_item_headline_str_metric"
  val AlertItemsField = "alert_items"
  val AlertItemsHeaderField = "alert_items_header"
  val AlertRemediationStepsField = "alert_remediation_steps"
  val AlertTagsField = "alert_tags"
  val ApplicableMetricTagField = "applicable_metric_tag"
  val AuthorField = "author"
  val BaseRemediationTextField = "base_remediation_text"
  val ComplexConditionField = "complex_condition"
  val ConfigurationParametersField = "configuration_parameters"
  val DescriptionMetricTagField = "description_metric_tag"
  val DescriptionStringFormatField = "description_string_format"
  val DeviceCategoryField = "device_category"
  val DisableGlobalRuleSetField = "disable_global_rule_set"
  val ExpectedValueField = "expected_value"
  val HeadlineFormatField = "headline_format"
  val HistoryLengthField = "history_length"
  val IncludeSnapshotDiffField = "include_snapshot_diff"
  val IsArrayField = "is_array"
  val ItemKeyField = "item_key"
  val ItemSpecificDescriptionField = "item_specific_description"
  val ItemsToIgnoreField = "items_to_ignore"
  val LimitMetricNameField = "limit_metric_name"
  val MetaConditionField = "meta_condition"
  val MetricNameField = "metric_name"
  val MetricTypeField = "metric_type"
  val MetricUnitsField = "metric_units"
  val MetricsConditionField = "metrics_condition"
  val MinimumValueToAlertField = "minimum_value_to_alert"
  val OptionalTagsField = "optional_tags"
  val OsNameField = "os_name"
  val ParameterDescriptionField = "parameter_description"
  val ParameterNameField = "parameter_name"
  val PoolNameField = "pool_name"
  val RequiredItemsParameterDescriptionField = "required_items_parameter_description"
  val RequiredItemsParameterNameField = "required_items_parameter_name"
  val RequiresField = "requires"
  val RuleCategoriesField = "rule_categories"
  val RuleDescriptionField = "rule_description"
  val RuleFriendlyNameField = "rule_friendly_name"
  val RuleNameField = "rule_name"
  val SamplesTimeDifferenceThresholdField = "samples_time_difference_threshold"
  val SecondaryInfoHeadlineField = "secondary_info_headline"
  val SeverityField = "severity"
  val ShouldUseDevicePassiveAndPassiveLinkStateConditionField = "should_use_device_passive_and_passive_link_state_condition"
  val ShouldUseHistoryContainsStateConditionField = "should_use_history_contains_state_down_condition"
  val SnapshotItemKeyField = "snapshot_item_key"
  val SnapshotItemListField = "snapshot_item_list"
  val StateDescriptionComplexMetricNameField = "state_description_complex_metric_name"
  val ThresholdDirectionField = "threshold_direction"
  val ThresholdField = "threshold"
  val TimeThresholdField = "time_frame_in_minutes"
  val UnitConverterDoubleValueField = "unit_converter_double_value"
  val UsageMetricNameField = "usage_metric_name"
  val UsageThresholdField = "usage_threshold"
  val VendorSeverityRatingField = "vendor_severity_rating"
  val VendorToRemediationTextField = "vendor_to_remediation_text"
  val VersionRangesField = "version_ranges"
  val IssueItemsMetricName = "issue_items_metric_name"
  val IssueItemsComplexConditionField = "issue_items_complex_condition"
  val MetricsNamesField = "metrics_names"
  val ProductField = "product"
}
