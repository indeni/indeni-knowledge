package com.indeni.server.rules.library.yamlrules

case class AlertItemsDescriptor(title: String, headlineTemplate: String, descriptionTemplate: String)

