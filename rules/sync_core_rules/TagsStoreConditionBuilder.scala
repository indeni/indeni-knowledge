package com.indeni.server.rules.library

import com.indeni.server.common.data.conditions._

case class TagsStoreConditionBuilderConfig()

class TagsStoreConditionBuilder(tagsStoreConditionBuilderConfig: TagsStoreConditionBuilderConfig) {
  def buildFromAST(tagConditionNode: ConditionNode[TagConditionType.type]): TagsStoreCondition = tagConditionNode match {
    case BasicTagConditionNode(tagNode, tagPredicateNode) => {
      tagPredicateNode match {
        case EqualsStringTagPredicateNode(stringLiteral) => Equals(tagNode.tagName, stringLiteral)
        case NotEqualsStringTagPredicateNode(stringLiteral) => Not(Equals(tagNode.tagName, stringLiteral))
      }
    }
    case TrueTagConditionNode => True
    case FalseTagConditionNode => False
    case NotNode(innerNode) => Not(buildFromAST(innerNode))
    case AndNode(innerNodes) => And(innerNodes.map(buildFromAST))
    case OrNode(innerNodes) => Or(innerNodes.map(buildFromAST))
  }
}
