package com.indeni.server.rules.library

import com.indeni.ruleengine.Scope.Scope
import com.indeni.ruleengine.expressions.Expression
import com.indeni.ruleengine.expressions.conditions.{And, Condition, Contains, Equals, GreaterThan, GreaterThanOrEqual, LesserThan, LesserThanOrEqual, Not, Or, StatusTreeCondition, False => FalseMetricsCondition, True => TrueMetricsCondition}
import com.indeni.ruleengine.expressions.core.StatusTreeExpression.StatusTreeExpressionFactory
import com.indeni.ruleengine.expressions.core.{ConstantExpression, StatusTreeExpression}
import com.indeni.ruleengine.expressions.data._
import com.indeni.ruleengine.expressions.utility.SwitchCaseExpression
import com.indeni.server.common.data.TagsStoreDao
import com.indeni.server.common.data.conditions.{True => TrueTagsCondition}
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.yamlrules.{AlertItemsDescriptor, ConfigurationParameterDescriptor, DoubleConfigurationParameterDescriptor, TemplateStringHelper}
import com.typesafe.scalalogging.Logger

case class ConditionExpressionBuilderConfig()

class BuilderException(msg: String) extends Exception(msg)
case class NotSupportedException(msg: String) extends BuilderException(msg)  // Used when we do not support building a Expression-type condition for a specific condition type

/**
  * Pass None for the map map when using it for simple conditions as part of the template-based yamls
  */
class ConditionFromSimpleMetricConditionBuilder(conditionExpressionBuilderConfig: ConditionExpressionBuilderConfig, configParameters: Option[Map[String, ConfigurationParameterDescriptor]]) {

  @throws[BuilderException]
  def buildFromAST(metricConditionNode: ConditionNode[SimpleMetricConditionType.type]): Condition = metricConditionNode match {
    case BasicSimpleMetricConditionNode(metricNode, tagPredicateNode) => {
      lazy val snapshotMetricExpr = SnapshotExpression(metricNode.metricName)
      lazy val noneableSingleMetricExpr = snapshotMetricExpr.asSingle().mostRecent().value().noneable
      lazy val multiMetricExpr = snapshotMetricExpr.asMulti().mostRecent().value()
      tagPredicateNode match {
        case ComparisonPredicateNode(comparisonOperator, literal) => buildFromComparisonPredicateNode(metricNode, literal, comparisonOperator)
        case InStringsMetricPredicateNode(strValues) => Or(strValues.map {
          case StringLiteralNode(s) => Equals(RuleHelper.createComplexStringConstantExpression(s), noneableSingleMetricExpr)
          case _ : StringConfigurationParameterNode => throw NotSupportedException("String configuration parameters not supported")
        })
        case IsEmptyPredicateNode                    => Equals(RuleHelper.createEmptyComplexArrayConstantExpression(), multiMetricExpr.noneable)
        case ContainsAtKeyPredicateNode(value, key)  => value match {
          case StringLiteralNode(s) =>
            Contains(MultiSnapshotExtractScalarExpression(multiMetricExpr, key), ConstantExpression(s))
          case _: StringConfigurationParameterNode =>
            throw NotSupportedException("String configuration parameters not supported")
        }
        case RegexMatchPredicateNode(_)              => throw NotSupportedException("matchesRegex not supported")
        case ContainsStringPredicateNode(_)          => throw NotSupportedException("contains for strings not supported")
        case StartsWithPredicateNode(_)              => throw NotSupportedException("startsWith not supported")
        case EndsWithPredicateNode(_)                => throw NotSupportedException("endsWith not supported")
        case EqualsAsSetPredicateNode(_)             => throw NotSupportedException("equalAsSets not supported")
      }
    }
    case TrueSimpleMetricConditionNode => TrueMetricsCondition
    case FalseSimpleMetricConditionNode => FalseMetricsCondition
    case NotNode(innerNode)  => Not(buildFromAST(innerNode))
    case AndNode(innerNodes) => And(innerNodes.map(buildFromAST))
    case OrNode(innerNodes)  => Or(innerNodes.map(buildFromAST))
  }

  private def buildFromComparisonPredicateNode(metricNode: MetricNode, literal: ConstantNode, comparisonOperator: ComparisonOperatorNode) = {
    (metricNode.metricType, literal) match {
      case (StringMetricType, stringConstant : StringConstant) => {
        stringConstant match {
          case StringLiteralNode(strValue) => {
            val noneableSingleMetricExpr = SnapshotExpression(metricNode.metricName).asSingle().mostRecent().value().noneable
            val equalsCondition =
              Equals(RuleHelper.createComplexStringConstantExpression(strValue), noneableSingleMetricExpr)
            comparisonOperator match {
              case EqualsNode    => equalsCondition
              case NotEqualsNode => Not(equalsCondition)
              case _             => throw new Exception("Should have failed validation")
            }
          }
          case _: StringConfigurationParameterNode =>
            throw NotSupportedException("String configuration parameters not supported")
        }
      }
      case (DoubleMetricType, numberConstant: DoubleConstant) => {
        val numberValueExpr = numberConstant match {
          case NumberLiteralNode(numberValue) => ConstantExpression[Option[Double]](Some(numberValue))
          case DoubleConfigurationParameterNode(parameterName) => configParameters match {
            case Some(map) => map.get(parameterName) match {
              case Some(configParameterDescriptor: DoubleConfigurationParameterDescriptor) => RuleHelper.getParameterDouble(parameterName, configParameterDescriptor.defaultValue)
              case _ => throw new Exception("Should have failed valiation")
            }
            case None => throw new Exception("Simple condition with configuration parameter references. Should have failed validation")
          }
        }
        lazy val timeSeriesMetricExpr = TimeSeriesExpression[Double](metricNode.metricName).last
        lazy val equalsCondition = Equals(timeSeriesMetricExpr, numberValueExpr)
        comparisonOperator match {
          case EqualsNode               => equalsCondition
          case NotEqualsNode            => Not(equalsCondition)
          case LessThanNode             => LesserThan(timeSeriesMetricExpr, numberValueExpr)
          case LessThanOrEqualToNode    => LesserThanOrEqual(timeSeriesMetricExpr, numberValueExpr)
          case GreaterThanNode          => GreaterThan(timeSeriesMetricExpr, numberValueExpr)
          case GreaterThanOrEqualToNode => GreaterThanOrEqual(timeSeriesMetricExpr, numberValueExpr)
        }
      }
      case _ => throw new Exception("Should have failed validation")
    }
  }
}

class ConditionFromFullMetricConditionBuilder(conditionExpressionBuilderConfig: ConditionExpressionBuilderConfig)(implicit logger: Logger) {
  private def createSimpleConditionBuilder(configParameters: Map[String, ConfigurationParameterDescriptor]) = new ConditionFromSimpleMetricConditionBuilder(conditionExpressionBuilderConfig, Some(configParameters))

  @throws[BuilderException]
  def buildFromAST(metricConditionNode: ConditionNode[FullMetricConditionType.type], configParameters: Map[String, ConfigurationParameterDescriptor], idToAlertItemsDescriptor: Map[String, AlertItemsDescriptor], context: RuleContext): Condition = metricConditionNode match {
    case BasicFullMetricConditionNode(tagsAggregation, simpleMetricCondition) => {
      val alertItemsDescriptor: Option[AlertItemsDescriptor] = tagsAggregation.alertItemsId.map(alertItemsId =>
        idToAlertItemsDescriptor.getOrElse(alertItemsId,
          throw new Exception("Should have failed validation"))
      )
      buildTreeForSingleAggregation(tagsAggregation, simpleMetricCondition, alertItemsDescriptor, configParameters, context)
    }
    case TrueFullMetricConditionNode  => TrueMetricsCondition
    case FalseFullMetricConditionNode => FalseMetricsCondition
    case NotNode(innerNode)           => Not(buildFromAST(innerNode, configParameters, idToAlertItemsDescriptor, context))
    case AndNode(innerNodes)          => And(innerNodes.map(buildFromAST(_, configParameters, idToAlertItemsDescriptor, context)))
    case OrNode(innerNodes)           => Or(innerNodes.map(buildFromAST(_, configParameters, idToAlertItemsDescriptor, context)))
  }

  private[library] class MutableBoolean(var value: Boolean)

  private def buildTreeLevel(
      previousLevel: Condition, alertItemsDescriptor: Option[AlertItemsDescriptor],
      shouldAddSecondaryInfoAndMinimumIssueCount: MutableBoolean, minimumIssueCount: Int,
      tags: Set[String], relevantDao: TagsStoreDao, collectionToBuildScopeFrom: Set[String],
      scopeCreatingExpression: Set[String] => Expression[Set[Scope]], configParameters: Map[String, ConfigurationParameterDescriptor]): Condition = {
    if (collectionToBuildScopeFrom.isEmpty) {
      previousLevel
    } else {
      val statusTreeExpressionFactory = StatusTreeExpression(
        scopeCreatingExpression(collectionToBuildScopeFrom),
        previousLevel
      )
      val condition = createConditionFromExpressionFactory(
        statusTreeExpressionFactory, alertItemsDescriptor, shouldAddSecondaryInfoAndMinimumIssueCount,
        minimumIssueCount, configParameters
      )
      maybeAddTagsToCondition(condition, tags, relevantDao)
    }
  }

  private def maybeAddTagsToCondition(condition: Condition, tags: Set[String], relevantDao: TagsStoreDao): Condition = {
    val conditionWithTagsIfNeeded =
      if (tags.nonEmpty)
        StatusTreeExpression(
          SelectTagsExpression(relevantDao, tags, TrueTagsCondition),
          condition
        ).withoutInfo().asCondition()
      else
        condition
    conditionWithTagsIfNeeded
  }

  /**
    * Creates a condition from a StatusTreeExpressionFactory, adding secondary info and minimum issue count, if necessary
    */
  private def createConditionFromExpressionFactory(
      statusTreeExpressionFactory: StatusTreeExpressionFactory, alertItemsDescriptor: Option[AlertItemsDescriptor],
      shouldAddSecondaryInfoAndMinimumIssueCount: ConditionFromFullMetricConditionBuilder.this.MutableBoolean,
      minimumIssueCount: Int, configParameters: Map[String, ConfigurationParameterDescriptor]): StatusTreeCondition = {
    val result =
      if (shouldAddSecondaryInfoAndMinimumIssueCount.value)
        alertItemsDescriptor
          .map(
            alertItems =>
              statusTreeExpressionFactory.withSecondaryInfo(
                headline = TemplateStringHelper.instantiateToExpression(alertItems.headlineTemplate, configParameters),
                description =
                  TemplateStringHelper.instantiateToExpression(alertItems.descriptionTemplate, configParameters),
                title = alertItems.title
            ))
          .getOrElse(statusTreeExpressionFactory.withoutInfo())
          .asCondition(minimumIssueCount)
      else
        statusTreeExpressionFactory.withoutInfo().asCondition()
    shouldAddSecondaryInfoAndMinimumIssueCount.value = false
    result
  }

  private def buildTreeForSingleAggregation(tagsAggregation: TagsAggregation, simpleMetricCondition: ConditionNode[SimpleMetricConditionType.type], alertItemsDescriptor: Option[AlertItemsDescriptor], configParameters: Map[String, ConfigurationParameterDescriptor], context: RuleContext): Condition = {
    val tags = tagsAggregation.tagNames.toSet ++ RuleHelper.getAllMetricTagsFromSimpleMetricsCondition(simpleMetricCondition) ++ alertItemsDescriptor.map(alertItems => RuleHelper.getAllReferencesFromAlertItems(alertItems, TemplateStringHelper.MetricTagSection)).getOrElse(Seq.empty).toSet
    val doubleMetrics = RuleHelper.getAllMetricsFromSimpleMetricsCondition(simpleMetricCondition, DoubleMetricType)
    val stringMetrics = RuleHelper.getAllMetricsFromSimpleMetricsCondition(simpleMetricCondition, StringMetricType)
    val objArrayMetrics = RuleHelper.getAllMetricsFromSimpleMetricsCondition(simpleMetricCondition, ObjArrayMetricType)
    val minimumIssueCount = tagsAggregation.aggregationSpecifier match {
      case AnyAggregation          => 1
      case AllAggregation          => throw NotSupportedException("'all' aggregation not supported in current rule engine")
      case AtLeastKAggregeation(k) => k
      case _: AtLeastKPercentAggregation =>
        throw NotSupportedException("'at-least-k%' aggregation not supported in current rule engine")
    }

    val simpleMetricConditionExpression: Condition = createSimpleConditionBuilder(configParameters).buildFromAST(simpleMetricCondition)
    var shouldAddSecondaryInfoAndMinimumIssueCount = new MutableBoolean(true)
    val conditionWithObjArrayMetrics = buildTreeLevel(simpleMetricConditionExpression, alertItemsDescriptor, shouldAddSecondaryInfoAndMinimumIssueCount, minimumIssueCount, tags, context.snapshotsDao, objArrayMetrics, SelectSnapshotsExpression(context.snapshotsDao, _).multi(), configParameters)
    // TODO: Note that in the rare case of an atomic full metric condition referencing both an objArray metric and a string metric,
    //       this will have the same SelectTagsExpression twice in the tree, in two different levels. However, this was
    //       tested and yielded correct results.
    val conditionWithStringMetrics = buildTreeLevel(conditionWithObjArrayMetrics, alertItemsDescriptor, shouldAddSecondaryInfoAndMinimumIssueCount, minimumIssueCount, tags, context.snapshotsDao, stringMetrics, SelectSnapshotsExpression(context.snapshotsDao, _).single(), configParameters)
    val conditionWithDoubleMetrics = buildTreeLevel(conditionWithStringMetrics, alertItemsDescriptor, shouldAddSecondaryInfoAndMinimumIssueCount, minimumIssueCount, tags, context.tsDao, doubleMetrics, SelectTimeSeriesExpression(context.tsDao, _, denseOnly = false), configParameters)
    conditionWithDoubleMetrics
  }
}

