package com.indeni.server.rules.library.crossvendor

import com.indeni.apidata.time.TimeSpan
import com.indeni.apidata.time.TimeSpan.TimePeriod
import com.indeni.ruleengine.expressions.conditions.{And, GreaterThan, LesserThan}
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, _}
import com.indeni.ruleengine.expressions.math.PlusExpression
import com.indeni.ruleengine.expressions.utility.NowExpression
import com.indeni.server.common.data.conditions.True
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules._
import com.indeni.server.rules.library.{ConditionalRemediationSteps, PerDeviceRule, RuleHelper}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class CrossVendorCertificateHasExpiredRule() extends PerDeviceRule with RuleHelper {

  private val highThresholdParameterName = "Effective_Duration_Threshold"
  private val highThresholdParameter = new ParameterDefinition(highThresholdParameterName,
                                                               "",
                                                               "Effective_Duration_Threshold",
                                                               "How long before expiration should Indeni alert.",
                                                               UIType.TIMESPAN,
                                                               TimeSpan.fromDays(30))

  override val metadata: RuleMetadata = RuleMetadata
    .builder(
      "cross_vendor_certificate_has_expired",
      "Certificate(s) has expired",
      "Indeni will alert when a certificate has expired. Certificates that have expired more that a set number of days will be ignored. " +
        "The threshold for the number of days after certificate expiration can be adjusted by the user.",
      AlertSeverity.ERROR,
      categories = Set(RuleCategory.OngoingMaintenance),
      deviceCategory = DeviceCategory.AllDevices
    )
    .configParameter(highThresholdParameter)
    .build()

  override def expressionTree(context: RuleContext): StatusTreeExpression = {
    val actualValue = TimeSeriesExpression[Double]("certificate-expiration").last.toTimeSpan(TimePeriod.SECOND)

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),
      // What constitutes an issue
      StatusTreeExpression(
        // The additional tags we care about (we'll be including this in alert data)
        SelectTagsExpression(context.tsDao, Set("name"), withTagsCondition("certificate-expiration")),
        StatusTreeExpression(
          // The time-series we check the test condition against:
          SelectTimeSeriesExpression[Double](context.tsDao, Set("certificate-expiration"), denseOnly = false),
          // The condition which, if true, we have an issue. Checked against the time-series we've collected
          And(
            LesserThan(
              actualValue,
              NowExpression()
            ),
            GreaterThan(
              PlusExpression[TimeSpan](actualValue, getParameterTimeSpanForTimeSeries(highThresholdParameter)),
              NowExpression()
            )
          )
          // The Alert Item to add for this specific item
        ).withSecondaryInfo(
            scopableStringFormatExpression("${scope(\"name\")}"),
            scopableStringFormatExpression("Expired on %s", timeSpanToDateExpression(actualValue)),
            title = "Affected Certificates"
          )
          .asCondition()
      ).withoutInfo().asCondition()
    ).withRootInfo(
      getHeadline(),
      ConstantExpression("One or more certificates has expired. See the list below."),
      ConditionalRemediationSteps("Renew any certificates that need to be renewed.",
        RemediationStepCondition.VENDOR_CP ->
          """Please review: https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk104400"
            |and the articles to which it links at the bottom.""".stripMargin,
        RemediationStepCondition.VENDOR_PANOS ->
          """Please review this article on Palo Alto Networks Support Site: https://docs.paloaltonetworks.com/pan-os/9-1/pan-os-admin/certificate-management/revoke-and-renew-certificates""".stripMargin,
        RemediationStepCondition.VENDOR_FORTINET ->
          """
            |1. Login via ssh to the Fortinet firewall and run the FortiOS command “get vpn certificate <X> detail”  to review the period for which the certificate is valid.
            |2. Login via ssh to the Fortinet firewall and run the FortiOS command “get vpn certificate setting” to review the settings.
            |3. Login via https to the Fortinet firewall and go to the menu System > Certificates tab to review the list of the certificates. Double click each certificate to get detailed information.
            |4. For more information review the Fortinet Certification Configuration Guide: http://help.fortinet.com/fos50hlp/54/Content/FortiOS/fortigate-authentication-54/Certificates.htm
            |5. If the problem persists, contact Fortinet Technical support at https://support.fortinet.com/ for further assistance.""".stripMargin,
        RemediationStepCondition.OS_CISCO_ASA ->
          """
            |1. View the Installed Certificates via the CLI with the next ASA command ‘show crypto ca certificate’ and review the validity date
            |2. Please refer to the official Cisco "how to" guide for more information about certification Installation & Renewalt https://www.cisco.com/c/en/us/support/docs/security-vpn/public-key-infrastructure-pki/200339-Configure-ASA-SSL-Digital-Certificate-I.html
            |3. Please refer to the official Cisco troubleshooting guide for AnyConnect and Certification problem: https://www.cisco.com/c/en/us/support/docs/security/asa-5500-x-series-firewalls/212972-anyconnect-vpn-client-troubleshooting-gu.html#anc50
            |4. Run the debug commands in the case of an SSL Certificate Installation failure : debug crypto ca 255, debug crypto ca messages 255, debug crypto ca transactions 255""".stripMargin,
        RemediationStepCondition.VENDOR_BLUECAT ->
          """
            |Please follow these steps to upload your new certificates (you can also refer to our knowledge base following this link.
            |1. Login to your BlueCat Address Manager as an administrative User2. Click on the tab “Administration”3. Under “User Management” click on “Secure Access”4. Under “Server Certificate Settings”, select “Custom” -> “Load Custom Certificate”5. Under “Upload Certificate” upload your renewed (signed) certificate. If you don’t want to use the same private key, please unselect “Use previously configured private key”6. Click on Update and your renewed certificate is applied.
            |2. Restart proteus services, then load the BAM GUI
            |
            |If you are experiencing issues with renewing your certificates, please open a ticket with Bluecat Care (https://care.bluecatnetworks.com/s/)
            |""".stripMargin
      )
    )
  }
}
