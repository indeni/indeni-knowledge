package com.indeni.server.rules.library.crossvendor

import com.indeni.ruleengine.expressions.conditions.{And, GreaterThan, LesserThan}
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, _}
import com.indeni.ruleengine.expressions.math.PlusExpression
import com.indeni.ruleengine.expressions.utility.NowExpression
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules._
import com.indeni.server.rules.library.{ConditionalRemediationSteps, PerDeviceRule, RuleHelper}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import com.indeni.server.common.data.conditions.True
import com.indeni.apidata.time.TimeSpan
import com.indeni.apidata.time.TimeSpan.TimePeriod

case class CrossVendorCertificateWillExpireRule() extends PerDeviceRule with RuleHelper {

  private val highThresholdParameterName = "Ahead_Alerting_Threshold"
  private val highThresholdParameter = new ParameterDefinition(highThresholdParameterName,
                                                               "",
                                                               "Expiration Threshold",
                                                               "How long before expiration should Indeni alert.",
                                                               UIType.TIMESPAN,
                                                               TimeSpan.fromDays(56))

  override val metadata: RuleMetadata = RuleMetadata
    .builder(
      "cross_vendor_certificate_will_expire",
      "Certificate(s) expiration nearing",
      "Indeni will alert when a certificate is about to expire. " +
        "The threshold for the number of days before certificate expiration can be adjusted by the user.",
      AlertSeverity.WARN,
      categories = Set(RuleCategory.OngoingMaintenance),
      deviceCategory = DeviceCategory.AllDevices
    )
    .configParameter(highThresholdParameter)
    .build()

  override def expressionTree(context: RuleContext): StatusTreeExpression = {
    val actualValue = TimeSeriesExpression[Double]("certificate-expiration").last.toTimeSpan(TimePeriod.SECOND)

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),
      // What constitutes an issue
      StatusTreeExpression(
        // The additional tags we care about (we'll be including this in alert data)
        SelectTagsExpression(context.tsDao, Set("name"), withTagsCondition("certificate-expiration")),
        StatusTreeExpression(
          // The time-series we check the test condition against:
          SelectTimeSeriesExpression[Double](context.tsDao, Set("certificate-expiration"), denseOnly = false),
          // The condition which, if true, we have an issue. Checked against the time-series we've collected
          And(
            GreaterThan(
              actualValue,
              NowExpression()
            ),
            LesserThan(actualValue,
                       PlusExpression[TimeSpan](NowExpression(),
                                                getParameterTimeSpanForTimeSeries(highThresholdParameter)))
          )
          // The Alert Item to add for this specific item
        ).withSecondaryInfo(
            scopableStringFormatExpression("${scope(\"name\")}"),
            scopableStringFormatExpression("Will expire on %s", timeSpanToDateExpression(actualValue)),
            title = "Affected Certificates"
          )
          .asCondition()
      ).withoutInfo().asCondition()
    ).withRootInfo(
      getHeadline(),
      ConstantExpression("One or more certificates are about to expire. See the list below."),
      ConditionalRemediationSteps(
        "Renew any certificates that need to be renewed.",
        RemediationStepCondition.VENDOR_CP ->
          """Please review: https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk104400
            |and the articles to which it links at the bottom.""".stripMargin,
        RemediationStepCondition.VENDOR_PANOS ->
          """Please review this article on Palo Alto Networks Support Site: https://docs.paloaltonetworks.com/pan-os/9-1/pan-os-admin/certificate-management/revoke-and-renew-certificates""".stripMargin,
        RemediationStepCondition.VENDOR_FORTINET ->
          """
            |1. Login via ssh to the Fortinet firewall and run the FortiOS command “get vpn certificate <X> detail”  to review the period for which the certificate is valid.
            |2. Login via ssh to the Fortinet firewall and run the FortiOS command “get vpn certificate setting” to review the settings.
            |3. Login via https to the Fortinet firewall and go to the menu System > Certificates tab to review the list of the certificates. Double click each certificate to get detailed information.
            |4. For more information review the Fortinet Certification Configuration Guide: http://help.fortinet.com/fos50hlp/54/Content/FortiOS/fortigate-authentication-54/Certificates.htm
            |5. If the problem persists, contact Fortinet Technical support at https://support.fortinet.com/ for further assistance.""".stripMargin,
        RemediationStepCondition.OS_CISCO_ASA ->
          """
            |1. View the Installed Certificates via the CLI with the next ASA command ‘show crypto ca certificate’ and review the validity date
            |2. Please refer to the official Cisco "how to" guide for more information about certification Installation & Renewalt https://www.cisco.com/c/en/us/support/docs/security-vpn/public-key-infrastructure-pki/200339-Configure-ASA-SSL-Digital-Certificate-I.html
            |3. Please refer to the official Cisco troubleshooting guide for AnyConnect and Certification problem: https://www.cisco.com/c/en/us/support/docs/security/asa-5500-x-series-firewalls/212972-anyconnect-vpn-client-troubleshooting-gu.html#anc50
            |4. Run the debug commands in the case of an SSL Certificate Installation failure : debug crypto ca 255, debug crypto ca messages 255, debug crypto ca transactions 255""".stripMargin,
        RemediationStepCondition.VENDOR_BLUECAT ->
          """
            |Quickly re-apply your custom certificates if enabling/disabling HTTP or HTTPS or if you need to re-authenticate replaced servers on your network.
            |
            |Note:
            |1. You must already have a certificate loaded in Address Manager in order to re-apply certificates.
            |2. Use the Re-applying certificates function to quickly modify the state of HTTP and HTTPS.
            | 1. To re-apply a self-signed or custom certificate:
            |   1. Select the Administration tab. Tabs remember the page you last worked on, so select the tab again to ensure you're on the Administration page.
            |   2. Under User Management, click Secure Access.
            |   3. Under General, complete the following:
            |      i. Select Server—by default, this is the IP address of a standalone Address Manager server. If running Address Manager in replication, use the drop-down menu to select the IP address of Primary or Standby Address Manager servers.
            |      ii. HTTP—from the drop-down menu, select either Enable, Disable, orRedirect to HTTPS.
            |
            |        Note: Redirect to HTTPS
            |        Selecting Redirect to HTTPS will redirect users to HTTPS if they attempt to access Address Manager using HTTP. You must have HTTPS enabled to use Redirect to HTTPS.
            |          1. If the Address Manager domain name is configured to resolve to an IPv6 address, enabling Redirect to HTTPS will redirect the domain name in the URL to an IPv6 address, resulting in an unknown certificate warning in your browser. For more information, refer to knowledge base article 5978 on BlueCat Customer Care.
            |      iii. HTTPS—from the drop-down menu, select Enable. Important: Disabling HTTPS You can't disable HTTPS if HTTP is configured to redirect to HTTPS.
            |	  4. Under Server Certificate Settings, select Custom.
            |	  5. Select Re-apply.
            |	  6. Click Update. The Confirm Web Access Configuration opens.
            |	  7. Under Confirm Configuration, verify your changes. Listed changes will include the IP address of the Address Manager server, HTTPS or HTTPS status (enable/disable), and certificate type.
            |	  8. Click Yes. The Address Manager server will be temporarily unavailable as the changes are committed and the server restarts.
            |	2. Result:
            |	  1. Log in to Address Manager once the configuration is compete. Note: After modifying HTTP or HTTPS, your browser might warn you about an unknown or invalid certificate. This warning will cease once you accept the certificate and log in to Address Manager.
            |	  2. From the certificate warning, proceed to the site. Depending on your browser, this might entail clicking a button or creating an exception.""".stripMargin
      )
    )
  }
}
