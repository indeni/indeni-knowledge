package com.indeni.server.rules.library.crossvendor

import java.time.format.DateTimeFormatter
import java.time.{Instant, ZoneOffset, ZonedDateTime}

import com.indeni.ruleengine.InvisibleScopeKey
import com.indeni.ruleengine.Scope.{Scope, ImplicitScopeValueHelper}
import com.indeni.ruleengine.expressions.Expression
import com.indeni.ruleengine.expressions.conditions.And
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data._
import com.indeni.ruleengine.expressions.scope.ScopableExpression
import com.indeni.ruleengine.expressions.utility.IsEmptyExpression.IsEmptyExpressionHelper
import com.indeni.server.common.data.conditions.True
import com.indeni.server.rules._
import com.indeni.server.rules.library.{ConditionalRemediationSteps, PerDeviceRule, RuleHelper}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

import scala.language.reflectiveCalls

case class CrossVendorCoreDumpCreatedRule() extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata.builder("cross_vendor_core_dump_created", "Core dump files found",
    "A core dump is created when a process crashes. Indeni will alert when a core dump file is created.", AlertSeverity.ERROR, categories = Set(RuleCategory.HealthChecks), deviceCategory = DeviceCategory.AllDevices)
    .build()

  override def expressionTree(context: RuleContext): StatusTreeExpression = {
    val currentValue = SnapshotExpression("core-dumps").asMulti().mostRecent().value()
    val previousValue = SnapshotExpression("core-dumps").asMulti().middle().optionValue()
    val headline = new ScopableExpression[String] {
      override protected def evalWithScope(time: Long, scope: Scope): String = {
        val created = scope.getInvisible("created",Some("core-dumps")).get.asInstanceOf[String].toLong
        val createdInUTC =  ZonedDateTime.ofInstant(Instant.ofEpochSecond(created), ZoneOffset.UTC)
        DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss").format(createdInUTC) + " UTC"
      }
      override def args: Set[Expression[_]] = Set()
    }

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
      StatusTreeExpression(
        // The time-series we check the test condition against:
        SelectSnapshotsExpression(context.snapshotsDao, Set("core-dumps")).multi(),
        And(
          currentValue.nonEmpty,
          previousValue.isNot(currentValue)
        ),         multiInformers =
          Set( MultiIssueInformer(
            headline,
            scopableStringFormatExpression("${scope(\"core-dumps:path\")}"),
            "Core dump files",
            Set[InvisibleScopeKey](InvisibleScopeKey("created",Some("core-dumps")), InvisibleScopeKey("path",Some("core-dumps")))
          ).iterateOver(
            IterateSnapshotDimensionExpression("core-dumps"),
            com.indeni.ruleengine.expressions.conditions.True
          ))
      ).withoutInfo().asCondition()

      // Details of the alert itself
    ).withRootInfo(
      getHeadline(),
      ConstantExpression("A core dump file has been created. This happens when a process crashes, and the core dump can contain information on why this happened. This usually means that there is an issue that should be investigated."),
      ConditionalRemediationSteps("The list of core dumps is retrieved by logging into the shell over SSH and retrieving the details of files found in /var/log/dump/usermode/, /var/tmp/*core* or /var/crash. Investigate the core dump files. If the issue is not clear, open up a case with vendor support and send them the file.",
        RemediationStepCondition.VENDOR_F5 ->
          """|Login to your device with SSH and run "ls /var/core". Investigate the core dump files.
             |If the cause of the issue is not clear, open up a case with F5 and follow the instructions on this page in order to provide them with the information needed:
             |<a target="_blank" href="https://support.f5.com/csp/article/K10062">Article: K10062 on AskF5</a>.""".stripMargin,
        RemediationStepCondition.VENDOR_JUNIPER ->
          """|1. Run "show system core-dumps" command to locate system core files. The core dump files usually are located in "/var/log/dump/usermode/", "/var/tmp/*core*" or "/var/crash" directories.
             |2. Check for core-dumps that were created at to the time of failover.
             |3. If there is a core-dump created at the time of failover, consider uploading the file to the Juniper Networks Technical Assistance Center (JTAC) FTP server for further analysis and opening a case with the JTAC.
             |4. Review the following articles on Juniper TechLibrary for more information: <a target="_blank" href="https://kb.juniper.net/InfoCenter/index?page=content&id=KB18867">Syslog message: Core dumped</a>
             |<a target="_blank" href="https://kb.juniper.net/InfoCenter/index?page=content&id=KB26963">How to get a core-dump off the router and to the Juniper FTP server</a>""".stripMargin)
    )
  }
}
