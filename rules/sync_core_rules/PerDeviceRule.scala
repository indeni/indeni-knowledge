package com.indeni.server.rules.library
import com.indeni.ruleengine.expressions.core.StatusTreeExpression
import com.indeni.server.rules.{Rule, RuleContext}

/**
  * A rule that its top/root expression is a for-expression over devices.
  */
trait PerDeviceRule extends Rule {

  /* --- Methods --- */

  /* --- Public Methods --- */

  override def expressionTree(context: RuleContext): StatusTreeExpression


}
