package com.indeni.server.rules.library.system

import com.indeni.server.common.AlertNotificationSettings
import com.indeni.server.common.AlertNotificationSettings._
import com.indeni.server.rules.RuleMetadata
import com.indeni.server.rules.library.system.ServerDnsConfigurationRule._
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class ServerDnsConfigurationRule() extends SystemRule {

  override def metadata: RuleMetadata =
    RuleMetadata
      .builder(
        ID,
        "DNS server(s) are not configured",
        "indeni server DNS server(s) are not configured.",
        AlertSeverity.ERROR
      )
      .defaultAction(AlertNotificationSettings(ALL))
      .build()
}

object ServerDnsConfigurationRule {
  val ID = "indeni_server_dns_server_configuration"
}
