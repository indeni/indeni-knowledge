package com.indeni.server.rules.library.system
import com.indeni.server.common.AlertNotificationSettings
import com.indeni.server.rules.RuleMetadata
import com.indeni.server.rules.library.system.LicenseExpiredRule.ID
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class LicenseExpiredRule() extends SystemRule {

  override def metadata: RuleMetadata =
    RuleMetadata
      .builder(
        ID,
        "Indeni license expired",
        "The indeni license has expired",
        AlertSeverity.CRITICAL
      )
      .defaultAction(AlertNotificationSettings(AlertNotificationSettings.ALL))
      .build()
}

object LicenseExpiredRule {
  val ID = "indeni_license_expired"
}

