package com.indeni.server.rules.library.system

import com.indeni.server.common.AlertNotificationSettings
import com.indeni.server.common.AlertNotificationSettings._
import com.indeni.server.rules.RuleMetadata
import com.indeni.server.rules.library.system.ServerAvailableDiskRule._
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class ServerAvailableDiskRule() extends SystemRule {

  override def metadata: RuleMetadata =
    RuleMetadata
      .builder(
        ID,
        "Indeni Server high disk usage",
        "Some disks or file systems are under high usage. Backup and other Indeni features will be disabled",
        AlertSeverity.CRITICAL
      )
      .defaultAction(AlertNotificationSettings(ALL))
      .build()
}

object ServerAvailableDiskRule {
  val ID = "indeni_server_available_disk_gb"
}
