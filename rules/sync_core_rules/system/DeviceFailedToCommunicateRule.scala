package com.indeni.server.rules.library.system
import com.indeni.server.common.AlertNotificationSettings
import com.indeni.server.common.AlertNotificationSettings._
import com.indeni.server.rules.RuleMetadata
import com.indeni.server.rules.library.system.DeviceFailedToCommunicateRule._
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class DeviceFailedToCommunicateRule() extends SystemRule {

  override def metadata: RuleMetadata =
    RuleMetadata
      .builder(
        ID,
        "Failed to Communicate",
        "Indeni was not able to successfully communicate with the device.",
        AlertSeverity.CRITICAL
      )
      .defaultAction(AlertNotificationSettings(SYSLOG | EMAIL))
      .build()

}

object DeviceFailedToCommunicateRule {
  val ID = "DeviceFailedToCommunicate"
}
