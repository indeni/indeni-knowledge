package com.indeni.server.rules.library.system

import com.indeni.server.common.AlertNotificationSettings
import com.indeni.server.common.AlertNotificationSettings._
import com.indeni.server.rules.RuleMetadata
import com.indeni.server.rules.library.system.ServerNtpConfigurationRule._
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class ServerNtpConfigurationRule() extends SystemRule {

  override def metadata: RuleMetadata =
    RuleMetadata
      .builder(
        ID,
        "NTP server(s) are not configured",
        "indeni server NTP server(s) are not configured.",
        AlertSeverity.ERROR
      )
      .defaultAction(AlertNotificationSettings(ALL))
      .build()
}

object ServerNtpConfigurationRule {
  val ID = "indeni_server_ntp_server_configuration"
}
