package com.indeni.server.rules.library.system

import com.indeni.server.common.AlertNotificationSettings
import com.indeni.server.common.AlertNotificationSettings._
import com.indeni.server.rules.RuleMetadata
import com.indeni.server.rules.library.system.ServerMemoryUsageRule._
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class ServerMemoryUsageRule() extends SystemRule {

  override def metadata: RuleMetadata =
    RuleMetadata
      .builder(
        ID,
        "Indeni Server high memory usage",
        "Some memory elements are nearing their maximum capacity.",
        AlertSeverity.CRITICAL
      )
      .defaultAction(AlertNotificationSettings(ALL))
      .build()
}

object ServerMemoryUsageRule {
  val ID = "indeni_server_memory_usage"
}
