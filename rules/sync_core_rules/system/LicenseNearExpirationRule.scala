package com.indeni.server.rules.library.system
import com.indeni.server.common.AlertNotificationSettings
import com.indeni.server.rules.RuleMetadata
import com.indeni.server.rules.library.system.LicenseNearExpirationRule.ID
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class LicenseNearExpirationRule() extends SystemRule {

  override def metadata: RuleMetadata =
    RuleMetadata
      .builder(
        ID,
        "Indeni License near expiration",
        "The indeni license expiration is near",
        AlertSeverity.WARN
      )
      .defaultAction(AlertNotificationSettings(AlertNotificationSettings.ALL))
      .build()
}

object LicenseNearExpirationRule {
  val ID = "indeni_license_near_expiration"
}
