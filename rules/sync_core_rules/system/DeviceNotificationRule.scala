package com.indeni.server.rules.library.system

import com.indeni.server.common.AlertNotificationSettings
import com.indeni.server.common.AlertNotificationSettings._
import com.indeni.server.rules.RuleMetadata
import com.indeni.server.rules.library.system.DeviceNotificationRule._
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class DeviceNotificationRule() extends SystemRule {

  override def metadata: RuleMetadata =
    RuleMetadata
      .builder(
        ID,
        "Missing Privileged Credentials",
        "Missing privileged-command-level credentials, some commands will not be executed.",
        AlertSeverity.INFO
      )
    .defaultAction(AlertNotificationSettings(SYSLOG | EMAIL))
      .build()

}

object DeviceNotificationRule {
  val ID = "DeviceNotificationAlert"
}
