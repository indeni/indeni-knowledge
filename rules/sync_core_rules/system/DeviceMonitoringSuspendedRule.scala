package com.indeni.server.rules.library.system

import com.indeni.server.common.AlertNotificationSettings
import com.indeni.server.common.AlertNotificationSettings._
import com.indeni.server.rules.RuleMetadata
import com.indeni.server.rules.library.system.DeviceMonitoringSuspendedRule._
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity


case class DeviceMonitoringSuspendedRule() extends SystemRule {

  override def metadata: RuleMetadata =
    RuleMetadata
      .builder(
        ID,
        "Device Temporarily Suspended",
        "A high resource utilization has been measured on the device. Data collection is reduced to CPU and memory only until resource utilization returns to normal levels.",
        AlertSeverity.ERROR
      )
    .defaultAction(AlertNotificationSettings(SYSLOG | EMAIL))
      .build()

}

object DeviceMonitoringSuspendedRule {
  val ID = "DeviceMonitoringSuspended"
}
