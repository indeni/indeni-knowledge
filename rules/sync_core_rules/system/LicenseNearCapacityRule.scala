package com.indeni.server.rules.library.system

import com.indeni.server.common.AlertNotificationSettings
import com.indeni.server.rules.RuleMetadata
import com.indeni.server.rules.library.system.LicenseNearCapacityRule._
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class LicenseNearCapacityRule() extends SystemRule {

  override def metadata: RuleMetadata =
    RuleMetadata
      .builder(
        ID,
        "Indeni License near capacity limit",
        "The Indeni license is near capacity limit. " +
          "The license overdraft feature allows an additional 10% of extra licenses beyond the purchase quantity. " +
          "This gives you time to determine if you need additional licenses and purchasing them without disrupting the service. " +
          "Once the 10% is exceeded, the system will stop collecting data until the license utilization decreases.",
        AlertSeverity.WARN
      )
      .defaultAction(AlertNotificationSettings(AlertNotificationSettings.ALL))
      .build()
}

object LicenseNearCapacityRule {
  val ID = "indeni_license_count_near_limit"
}




