package com.indeni.server.rules.library.system

import com.indeni.server.common.AlertNotificationSettings
import com.indeni.server.rules.RuleMetadata
import com.indeni.server.rules.library.system.DeviceNotRespondingRule._
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class DeviceNotRespondingRule() extends SystemRule {

  override def metadata: RuleMetadata =
    RuleMetadata
      .builder(
        ID,
        "Device not responding",
        "No data has been retrieved from the device. This may be due to a communication issue. For assistance, please contact Indeni technical support.",
        AlertSeverity.CRITICAL
      )
      .defaultAction(AlertNotificationSettings(AlertNotificationSettings.ALL))
      .build()
}

object DeviceNotRespondingRule {
  val ID = "device_heartbeat_monitor"
}
