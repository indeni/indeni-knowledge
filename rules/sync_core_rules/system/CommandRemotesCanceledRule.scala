package com.indeni.server.rules.library.system

import com.indeni.server.common.AlertNotificationSettings
import com.indeni.server.common.AlertNotificationSettings.ALL
import com.indeni.server.rules.RuleMetadata
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import com.indeni.utils.common.akka.EventAggregator.Topics

final case class CommandRemotesCanceledRule() extends SystemRule {

  override def metadata: RuleMetadata =
    RuleMetadata
      .builder(
        "command-remote-canceled",
        "Indeni command execution canceled",
        "Indeni commands can yield an extensive dynamic execution tree depends on device's configuration, Indeni canceled a single execution to avoid high pressure on xdevice.",
        AlertSeverity.CRITICAL
      )
      .defaultAction(AlertNotificationSettings(ALL))
      .build()
}
