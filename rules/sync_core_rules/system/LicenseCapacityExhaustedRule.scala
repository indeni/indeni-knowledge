package com.indeni.server.rules.library.system

import com.indeni.server.common.AlertNotificationSettings
import com.indeni.server.rules.RuleMetadata
import com.indeni.server.rules.library.system.LicenseCapacityExhaustedRule._
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class LicenseCapacityExhaustedRule() extends SystemRule {

  override def metadata: RuleMetadata =
    RuleMetadata
      .builder(
        ID,
        "Indeni License capacity exhausted",
        "The indeni license capacity has been reached. " +
          "The license overdraft features allows an additional 10% of extra license beyond the purchase quantity. " +
          "This gives you time to purchase additional licenses without disrupting the service. " +
          "If the overage is more than the 10% overdraft allowance, the system will shut down and alert notifications will stop immediately. " +
          "" +
          "Note: The license overdraft feature is offered as a convenience, not a license entitlement. " +
          "Any overdraft licenses should be purchased within 30 days of use.",
        AlertSeverity.CRITICAL
      )
      .defaultAction(AlertNotificationSettings(AlertNotificationSettings.ALL))
      .build()
}

object LicenseCapacityExhaustedRule {
  val ID = "indeni_license_count_reached"
}


