package com.indeni.server.rules.library.system

import com.indeni.server.common.AlertNotificationSettings
import com.indeni.server.common.AlertNotificationSettings._
import com.indeni.server.rules.RuleMetadata
import com.indeni.server.rules.library.system.ServerDiskUsageRule._
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class ServerDiskUsageRule() extends SystemRule {

  override def metadata: RuleMetadata =
    RuleMetadata
      .builder(
        ID,
        "Indeni Server high disk usage (Percentage)",
        "Some disks or file systems are under high usage. Backup and other Indeni features will be disabled",
        AlertSeverity.CRITICAL
      )
      .defaultAction(AlertNotificationSettings(ALL))
      .build()
}

object ServerDiskUsageRule {
  val ID = "indeni_server_disk_usage_percentage"
}
