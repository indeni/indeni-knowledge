package com.indeni.server.rules.library.system

import com.indeni.server.common.AlertNotificationSettings
import com.indeni.server.common.AlertNotificationSettings._
import com.indeni.server.rules.RuleMetadata
import com.indeni.server.rules.library.system.ServerCpuUsageRule._
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class ServerCpuUsageRule() extends SystemRule {

  override def metadata: RuleMetadata =
    RuleMetadata
      .builder(
        ID,
        "Indeni Server high CPU usage of specific cores",
        "CPU cores are under high usage.",
        AlertSeverity.ERROR
      )
      .defaultAction(AlertNotificationSettings(ALL))
      .build()
}

object ServerCpuUsageRule {
  val ID = "indeni_server_cpu_usage"
}


