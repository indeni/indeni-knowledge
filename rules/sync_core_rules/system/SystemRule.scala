package com.indeni.server.rules.library.system
import com.indeni.ruleengine.StatusTree
import com.indeni.ruleengine.expressions.Expression
import com.indeni.server.rules.{Rule, RuleContext}

abstract class SystemRule extends Rule{
/**
    *
    * @return The expression tree, which its evaluation will determine the issues in the system.
    */
  override def expressionTree(context: RuleContext): Expression[StatusTree] = null

  override def isSystemRule: Boolean = true

}
