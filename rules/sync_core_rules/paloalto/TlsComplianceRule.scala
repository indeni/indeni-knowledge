package com.indeni.server.rules.library.paloalto

import com.indeni.ruleengine.expressions.conditions.{Equals => CompareEquals}
import com.indeni.ruleengine.expressions.core._
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.server.rules._
import com.indeni.server.common.data.conditions.{Equals => TagStoreEquals}
import com.indeni.server.common.data.conditions.True
import com.indeni.server.rules.{RuleContext, RuleMetadata}
import com.indeni.server.rules.library.{ConditionalRemediationSteps, PerDeviceRule, RuleHelper}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class TlsComplianceRule() extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata.builder(
    "pan_tls_compliance_rule",
    "TLS 1.3 not supported",
    "Indeni will alert if TLS 1.3 is not supported by the device",
    AlertSeverity.WARN,
    categories= Set(RuleCategory.SecurityRisks),
    deviceCategory = DeviceCategory.PaloAltoNetworks)
    .build()

  override def expressionTree(context: RuleContext): StatusTreeExpression = {
    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

          StatusTreeExpression(
            // The time-series we check the test condition against:
            SelectTimeSeriesExpression[Double](context.tsDao, Set("ssl-cipher-not-supported"),
              condition=TagStoreEquals("cipher", "TLS 1.3"),
              denseOnly=false),

            // The condition which, if true, we have an issue. Checked against the time-series we've collected
            CompareEquals(TimeSeriesExpression[Double]("ssl-cipher-not-supported").last, ConstantExpression(Some(0.0)))
          ).withoutInfo().asCondition().orElse(Some(false))
    ).withRootInfo(
        getHeadline(),
        ConstantExpression("The version of PAN-OS you are running is too old to support \"cipher\" when performing SSL decryption."),
        ConditionalRemediationSteps(
          """Please update your PAN-OS to support "cipher".
            | Please see this link for more information: https://live.paloaltonetworks.com/t5/General-Topics/TLS-1-3-is-Coming-How-to-deal-with-it/m-p/242065#M69293""".stripMargin
    )
  )
  }
}
