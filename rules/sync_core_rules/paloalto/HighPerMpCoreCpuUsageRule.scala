package com.indeni.server.rules.library.paloalto

import com.indeni.apidata.time.TimeSpan
import com.indeni.ruleengine.expressions.conditions.{And, ConditionHelper, GreaterThanOrEqual}
import com.indeni.ruleengine.expressions.core._
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.ruleengine.expressions.math.MinExpression
import com.indeni.ruleengine.expressions.scope.ScopeValueExpression
import com.indeni.server.common.ParameterValue
import com.indeni.server.common.data.conditions.True
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules._
import com.indeni.server.rules.config.expressions.DynamicParameterExpression
import com.indeni.server.rules.library.{ConditionalRemediationSteps, PerDeviceRule, RuleHelper}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity


case class HighPerMpCoreCpuUsageRule() extends PerDeviceRule with RuleHelper {

  private val highThresholdParameterName = "high_threshold_of_cpu_usage"
  private val highThresholdParameter = new ParameterDefinition(highThresholdParameterName,
    "",
    "High Threshold of CPU Usage",
    "What is the threshold for the CPU usage for which once it is crossed an issue will be triggered.",
    UIType.DOUBLE,
    new ParameterValue((80.0).asInstanceOf[Object])
  )

  private val minimumNumberOfCoresParameterName = "higher_than_threshold_cores"
  private val minimumNumberOfCoresParameter = new ParameterDefinition(minimumNumberOfCoresParameterName,
    "",
    "Number of Cores",
    "The number of CPU cores with usage above the value set in " +
      "\"" + highThresholdParameterName + "\"" +
      " before a issue is triggered.",
    UIType.INTEGER,
    new ParameterValue((1).asInstanceOf[Object])
  )

  private val timeThresholdParameterName = "time_threshold"
  private val timeThresholdParameter = new ParameterDefinition(timeThresholdParameterName,
    "",
    "Time Threshold",
    "The CPU cores need to remain above the value set in " +
      "\"" + highThresholdParameterName + "\"" +
      " for this amount of time before a issue is triggered.",
    UIType.TIMESPAN,
    TimeSpan.fromMinutes(10))


  override def metadata: RuleMetadata = RuleMetadata.builder("high_per_mp_core_cpu_use_by_device", "High CPU usage per MP core(s)",
    "High CPU usage is a symptom of a system which is unable to handle " +
      "the required load or a symptom of a specific issue with the system " +
      "and the applications and services running on it. Indeni will monitor the MP (Management Plane) CPU usage " +
      "of each MP core separately and alert if any of the cores' CPU usage crosses the threshold.",
    AlertSeverity.ERROR,
    Set(RuleCategory.HealthChecks),
    deviceCategory = DeviceCategory.PaloAltoNetworksDevices).
    configParameters(highThresholdParameter, minimumNumberOfCoresParameter, timeThresholdParameter).
    build()

  override def expressionTree(context: RuleContext): StatusTreeExpression = {
    val inUseValue = MinExpression(TimeSeriesExpression[Double]("cpu-usage"))

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
      And(
          StatusTreeExpression(

          // The additional tags we care about (we'll be including this in alert data)
          SelectTagsExpression(context.tsDao, Set("cpu-id", "cpu-is-avg", "dp"), True),

          StatusTreeExpression(
            // The time-series we check the test condition against:
            SelectTimeSeriesExpression[Double](context.tsDao, Set("cpu-usage"), historyLength = getParameterTimeSpanForRule(timeThresholdParameter), denseOnly = true),

            // The condition which, if true, we have an issue. Checked against the time-series we've collected
            And(
              ScopeValueExpression("cpu-is-avg").visible().isIn(Set("true")).not,
              ScopeValueExpression("dp").visible().isIn(Set("true")).not,
              GreaterThanOrEqual(
                inUseValue,
                getParameterDouble(highThresholdParameter)))

            // The Alert Item to add for this specific item
          ).withSecondaryInfo(
            scopableStringFormatExpression("${scope(\"cpu-id\")}"),
            scopableStringFormatExpression("Current CPU utilization is: %.0f%%", inUseValue),
            title = "Cores with High CPU Usage"
          ).asCondition()).withoutInfo().asCondition(minimumIssueCount = DynamicParameterExpression.withConstantDefault(minimumNumberOfCoresParameter.getName, minimumNumberOfCoresParameter.getDefaultValue.asInteger().intValue())),
          ScopeValueExpression("vendor").invisible().isIn((Set("paloaltonetworks"))))
    ).withRootInfo(
      getHeadline(),
      ConstantExpression("Some CPU cores are under high usage."),
      ConditionalRemediationSteps("Determine the cause for the high CPU usage of the listed cores.",
        RemediationStepCondition.VENDOR_PANOS ->
          """For MP (management plane) CPU, look at https://knowledgebase.paloaltonetworks.com/KCSArticleDetail?id=kA14u000000oNDrCAM""".stripMargin,
      )
    )
  }
}
