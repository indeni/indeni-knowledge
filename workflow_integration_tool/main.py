import xmlrunner

from integrator.flow_builder import FlowBuilder
from integrator.integrator import MockWorkflowTester, get_all_conclusions_in_workflow, get_all_blocks_in_workflow
from indeni_workflow.block.block_type import BlockType
from typing import List, Set


def __workflow_name_to_path(test_path: str, workflow_name: str) -> str:
    import os
    return os.path.join(os.path.dirname(test_path), workflow_name)


def __is_test_file(file_name: str) -> bool:
    return file_name.startswith('workflow_test') and file_name.endswith('.yaml')


def __parse_args() -> List[str]:
    import glob
    from os.path import isdir, join
    import sys
    if len(sys.argv) != 2:
        print(f'Usage: {sys.argv[0]} <Workflow_Test_Path OR Directory_with_many_Workflow_Test_files>')
        exit(1)
    given_path = sys.argv[1]
    if isdir(given_path):
        # All integration test files must begin with "workflow_test" and end with ".yaml"
        glob_str = join(given_path, '**', '*workflow_test.yaml')
        return glob.glob(glob_str, recursive=True)
    else:
        return [given_path]


def __disable_logging():
    import logging
    logging.disable(logging.INFO)


def __ensure_all_conclusion_reached(workflow_path: str, reached_conclusions: Set[str]):
    all_conclusions = get_all_conclusions_in_workflow(workflow_path)
    if 'variant_conclusion' in all_conclusions:
        all_conclusions.remove('variant_conclusion')
    difference = all_conclusions.difference(reached_conclusions)
    difference_str = '\n\t'.join(list(sorted(difference)))
    if difference:
        print(f'{"=" * 70}\n{len(difference)} conclusion(s) not tested:\n\t{difference_str}')
        __print_failure_and_exit()
    else:
        print('All conclusions tested')


def __ensure_all_blocks_reached(workflow_path: str, reached_blocks: Set[str]):
    all_blocks = {b.block_id for b in get_all_blocks_in_workflow(workflow_path) if b.block_type != BlockType.CONCLUSION}
    difference = all_blocks.difference(reached_blocks)
    difference_str = '\n\t'.join(list(sorted(difference)))
    if difference:
        print(f'{"=" * 70}\n{len(difference)} block(s) not traversed:\n\t{difference_str}')
        __print_failure_and_exit()
    else:
        print('All blocks traversed')


def __print_failure_and_exit():
    print('---------------------------')
    print('INTEGRATION TESTING FAILURE')
    print('---------------------------')
    exit(1)


def __run_single_file_test_successful(file_path: str) -> bool:
    flow_builder = FlowBuilder(file_path)
    tags, workflow_name = flow_builder.tags, flow_builder.workflow_name
    workflow_path = __workflow_name_to_path(file_path, workflow_name)
    suite, visited_blocks = MockWorkflowTester.suite_builder(workflow_path, flow_builder.flows, tags)
    result = xmlrunner.XMLTestRunner(output='test_reports', verbosity=0).run(suite)
    __ensure_all_conclusion_reached(workflow_path, flow_builder.conclusions)
    __ensure_all_blocks_reached(workflow_path, visited_blocks)
    return result.wasSuccessful()


def main():
    __disable_logging()
    test_paths = __parse_args()
    for test_path in test_paths:
        if not __run_single_file_test_successful(test_path):
            exit(1)


if __name__ == '__main__':
    main()
