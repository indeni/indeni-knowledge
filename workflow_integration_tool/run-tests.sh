#!/bin/bash -xeu

__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
__script="${__dir}/$(basename "${BASH_SOURCE[0]}")"
__start_dir=`pwd`

cd ${__dir}/..

deactivate || true

virtualenv tests_env
source tests_env/bin/activate

pip3 install -r ${__dir}/integrator_requirements.txt --extra-index-url https://indeni.jfrog.io/indeni/api/pypi/indeni-pypi-develop/simple --extra-index-url https://indeni.jfrog.io/indeni/api/pypi/indeni-pypi-rc/simple --extra-index-url https://indeni.jfrog.io/indeni/api/pypi/indeni-pypi-stable/simple
echo we are here: ${PWD}
python3 ${__dir}/main.py automation
mv test_reports ../
deactivate || true

cd ${__start_dir}