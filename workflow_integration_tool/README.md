# Workflow Integration Tester (WIT) #

This guide is intended to instruct users of the WIT program on how to use the tool.

## Installation ##

It is recommended (though not necessary) that you use a virtual environment for installing WIT. If you are not familiar with virtual environments, search Google for “python virtualenv tutorial” or similar.  WIT does not need to be installed per se, but its dependencies do. To do so, run:

### Install virtual environment and pip packages ###
Located in indeni-knowledge repository root path
```
bash workflow_integration_tool/run-tests.sh
```

### Enabling virutal environment ###

```
source tests_env/bin/activate
```
### Install additional packages ###
```
pip3 install "indeni_workflow==0.0.0.*" --extra-index-url https://indeni.jfrog.io/indeni/api/pypi/indeni-pypi-develop/simple
pip3 install "indeni_parser==0.0.0.*" --extra-index-url https://indeni.jfrog.io/indeni/api/pypi/indeni-pypi-develop/simple
```

## Usage ##

From the root of the indeni-knowledge repository, run
```
python3 workflow_integration_tool/main.py automation/workflows/<path to workflow.yaml’s folder>
```
This will automatically find the test file whose name must end with “workflow_test.yaml” and run all flows described therein. 

### Understanding Output ###
The output should be relatively self-explanatory with some exceptions. A common error (which is being worked on) is the following:

```
ERROR:root:no mock found for command: <My specific command>
```

This can be caused by a variety of issues. 

There is a typo in the command specified at the top of its Data File

The output of the command is empty (which is currently not allowed, but this will be fixed)

The flow unexpectedly reached a device_task block which caused it to look for a Data File unexpectedly.

Other than this, the output is mostly the same as what is printed by python’s unittest library, with the addition of some information on which blocks/conclusions are not reached. 

Notice, if not all blocks/conclusions are reached by some flow, the integration testing will fail.

### Tips & Tricks ###

If you’re using an IDE like PyCharm, you can set debug points inside WIT itself to see the actual Python objects that represent the flows, the execution, the data files, etc.

### Feedback ###

Contact Jon by slack or at jonathan.s@indeni.com with any feedback or for assistance. This is a new tool which is still being improved, so don’t be shy to ask questions. If you’re stuck on a surprising output or if the tool is not working, ask.


## License ##

You agree that all contributions are and will be given voluntarily. By contributing code to this repository you hereby irrevocably and unconditionally license the Contribution (as defined below) under the Apache license version 2 https://www.apache.org/licenses/LICENSE-2.0 . You represent that you are legally entitled to grant the above license. If your employer(s) has rights to intellectual property that you create that includes your Contributions, you represent that you have received permission to make Contributions on behalf of that employer or that your employer has waived such rights for your Contributions. You further represent that each of your Contributions is your original creation. You agree to notify this repository's managers of any facts or circumstances of which you become aware that would make these representations inaccurate in any respect. "Contribution" means any original work of authorship, including any modifications or additions to an existing work, that is submitted by You to this repository

## Who do I talk to? ##

product@indeni.com
