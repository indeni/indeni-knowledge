from typing import Dict


class CommandOutputVariantStore:
    def __init__(self, pair_name: str, command: str, output: str, variant: Dict[str, str]):
        self.pair_name = pair_name
        self.command = command
        self.output = output
        self.variant = variant

    def __str__(self):
        return f'Pair: {self.pair_name}\nCommand: {self.command}\nOutput: {self.output}'
