from typing import List, Union
from integrator.command_output_variant_store import CommandOutputVariantStore
from integrator.command_output_name_store import CommandOutputNameStore


class Flow:
    def __init__(self,
                 flow_name: str,
                 pairs: List[Union[CommandOutputNameStore, CommandOutputVariantStore]],
                 conclusion: str,
                 issue_items=None):
        self._flow_name = flow_name
        self._pairs = pairs
        self._conclusion = conclusion
        self._issue_items = issue_items if issue_items is not None else set()

    def get_pairs(self):
        return self._pairs.copy()

    def get_conclusion(self):
        return self._conclusion

    def __str__(self):
        return f'Flow: {self._flow_name}: {self._pairs} -> {self._conclusion}'

    @property
    def pairs(self):
        return self._pairs

    @property
    def name(self):
        return self._flow_name

    @property
    def issue_items(self):
        return self._issue_items.copy()
