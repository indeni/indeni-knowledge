import glob
import os
import unittest
from typing import Dict, List, Set, cast

from integrator.flow_builder import Flow
from indeni_workflow.workflow_response import WorkflowSuccessResponse, WorkflowFailedResponse
from indeni_workflow.workflow_response import WorkflowIssueItemsConclusionResponse
from indeni_workflow.workflow_test_tool import DeviceData
from indeni_workflow.block.block_type import BlockType
from indeni_workflow.workflow import Workflow
from indeni_workflow.remote.device_credentials import MockCredentials, MockInput


MAX_FRIENDLY_NAME_LENGTH: int = 60

MAX_ACTION_BLOCK_NAME_LENGTH: int = 88
MAX_CONCLUSION_BLOCK_NAME_LENGTH: int = 60
MAX_CONDITION_BLOCK_NAME_LENGTH: int = 56
MAX_FOR_EACH_BLOCK_NAME_LENGTH: int = 74


class MockWorkflowTester(unittest.TestCase):
    def __init__(self, workflow_path: str, flow: Flow, tags: Dict[str, str], *args, **kwargs):
        self.flow = flow
        self.tags = tags
        self.path = workflow_path
        self._parent_dir = os.path.dirname(self.path)
        mock_data = MockCredentials([MockInput(x.command, x.output) for x in flow.get_pairs()])
        device_data = DeviceData('0.0.0.0', tags, [mock_data])
        self.workflow = Workflow(workflow_path)
        self.results = self.workflow.run(device_data, flow.issue_items)
        self.all_blocks = get_all_blocks_in_workflow(workflow_path='', workflow=self.workflow)
        super().__init__(*args, **kwargs)

    def test_flow_did_not_crash(self):
        valid_types = [WorkflowSuccessResponse, WorkflowIssueItemsConclusionResponse]
        if type(self.results) not in valid_types:
            temp_results = cast(WorkflowFailedResponse, self.results)
            self.fail(f"""Failed to run flow {self.flow.name}, did not reach a conclusion
            Error: {temp_results.exception}""")
        else:
            print(f'Flow {self.flow.name} ran without error\n\t{self.path}')

    def test_flow_reached_correct_conclusion(self):
        if type(self.results) == WorkflowSuccessResponse:
            self.assertEqual(self.flow.get_conclusion(), self.results.block_records[-1].block_id)
        elif type(self.results) == WorkflowIssueItemsConclusionResponse:
            temp_results = self.results
            temp_results = cast(WorkflowIssueItemsConclusionResponse, temp_results)
            for conclusion_response in temp_results.conclusion_response_per_issue_item.values():
                self.assertEqual(self.flow.get_conclusion(), conclusion_response.block_records[-1].block_id)
        else:
            self.fail(f'Workflow {self.flow.name} did not succeed, cannot check conclusion block')
        print(f'Flow {self.flow.name} reached correct conclusion.\n\tConclusion: {self.flow.get_conclusion()}\n'
              f'\tVariant: {self.tags}')

    def test_triage_fields_not_null(self):
        invalid_conclusion_values = ['', 'n/a', 'none']
        invalid_remediation_values = ['n/a', 'none']
        blocks = [b for b in self.all_blocks if b.block_type == BlockType.CONCLUSION]
        for block in blocks:
            self.assertNotIn(block.triage_conclusion.lower(), invalid_conclusion_values,
                             f'Block {block.block_id} has invalid conclusion "{block.triage_conclusion}"')
            self.assertNotIn(block.triage_remediation_steps.lower(), invalid_remediation_values,
                             f'Block {block.block_id} has invalid remediation steps "{block.triage_remediation_steps}"')

    def test_friendly_name_not_too_long(self):
        friendly_name = self.workflow.friendly_name or ''
        self.assertTrue(len(friendly_name) <= MAX_FRIENDLY_NAME_LENGTH,
                        f'Workflow friendly_name field must be no longer than {MAX_FRIENDLY_NAME_LENGTH} characters')

    def test_action_block_names_not_too_long(self):
        action_blocks = [b for b in self.all_blocks if b.block_type == BlockType.DEVICE_TASK]
        for block in action_blocks:
            if block.name:
                self.assertTrue(len(block.name) <= MAX_ACTION_BLOCK_NAME_LENGTH,
                                f'Block name {block.name} too long. Max {MAX_ACTION_BLOCK_NAME_LENGTH} characters')

    def test_conclusion_block_names_not_too_long(self):
        conclusion_blocks = [b for b in self.all_blocks if b.block_type == BlockType.CONCLUSION]
        for block in conclusion_blocks:
            self.assertTrue(len(block.name) <= MAX_CONCLUSION_BLOCK_NAME_LENGTH,
                            f'Block name {block.name} too long. Max {MAX_CONCLUSION_BLOCK_NAME_LENGTH} characters')

    def test_condition_block_names_not_too_long(self):
        condition_blocks = [b for b in self.all_blocks if b.block_type == BlockType.IF]
        for block in condition_blocks:
            self.assertTrue(len(block.name) <= MAX_CONDITION_BLOCK_NAME_LENGTH,
                            f'Block name {block.name} too long. Max {MAX_CONDITION_BLOCK_NAME_LENGTH} characters')

    def test_foreach_block_names_not_too_long(self):
        foreach_blocks = [b for b in self.all_blocks if b.block_type == BlockType.FOREACH]
        for block in foreach_blocks:
            self.assertTrue(len(block.name) <= MAX_FOR_EACH_BLOCK_NAME_LENGTH,
                            f'Block name {block.name} too long. Max {MAX_FOR_EACH_BLOCK_NAME_LENGTH} characters')

    def test_friendly_names_are_sentences(self):
        for block in self.all_blocks:
            if hasattr(block, 'name'):
                name: str = block.name
                if name:
                    self.assertTrue(name[0].isupper(), f'"name" field must begin with upper-case letter: {block.name}')

    def test_unit_testing_exists_and_full_coverage(self):
        mock_data_files = glob.glob(os.path.join(self._parent_dir, '*mock_data.py'))
        if len(mock_data_files) == 0:
            self.fail('No candidate Mock Data file found. '
                      'Mock Data Files should end with "mock_data.py" and appear beside workflow')
        test_files = glob.glob(os.path.join(self._parent_dir, '*parser_test.py'))
        if len(test_files) == 0:
            self.fail(f'No Test file found. Test Files should end with "parser_test.py" and appear beside workflow')
        else:
            test_file = test_files[0]
            os.system(f'coverage run -m unittest {test_file}')
            os.system('coverage html')

    @staticmethod
    def suite_builder(workflow_path: str, flows: List[Flow], tags: Dict[str, str]):
        """
        Build and return a suite of tests. Each test in MockWorkflowTester is run against each Flow
        :param workflow_path: Path to the ATE
        :param flows: List of all "flows" through the ATE to test
        :param tags: Tags passed to MockCredentials to specify the device
        :return: Suite of tests
        """
        suite = unittest.TestSuite()
        test_loader = unittest.TestLoader()
        test_names = test_loader.getTestCaseNames(MockWorkflowTester)
        visited_blocks = set()

        def recursive_block_finder(_block):
            if type(_block) == BlockType.FOREACH:
                for _block2 in _block.blocks.values():
                    recursive_block_finder(_block2)
            visited_blocks.add(_block.block_id)

        for flow in flows:
            for test_name in test_names:
                mock_workflow_test = MockWorkflowTester(workflow_path, flow, tags, test_name)
                if type(mock_workflow_test.results) == WorkflowSuccessResponse:
                    for block in mock_workflow_test.results.block_records:
                        # visited_blocks.add(block.block_id)
                        recursive_block_finder(block)
                # TODO: this block is probably not quite right, test with actual issue items.
                elif type(mock_workflow_test.results) == WorkflowIssueItemsConclusionResponse:
                    temp_results = mock_workflow_test.results
                    # This cast doesn't actually change the object, it just guides the type-checker
                    temp_results = cast(WorkflowIssueItemsConclusionResponse, temp_results)
                    for response in temp_results.conclusion_response_per_issue_item.values():
                        for block in response.block_records:
                            # visited_blocks.add(block.block_id)
                            recursive_block_finder(block)
                suite.addTest(mock_workflow_test)
        return suite, visited_blocks


# noinspection PyBroadException
def get_all_blocks_in_workflow(workflow_path: str, workflow=None) -> Set:
    """
    Find and return the IDs of all conclusion blocks in the workflow/ATE
    :param workflow:
    :param workflow_path: path to the ATE
    :return: Set of all conclusion block IDs
    """
    try:
        workflow = workflow or Workflow(workflow_path)
    except Exception as e:
        print('Error checking for conclusions')
        raise e
    all_blocks = set()

    def recursive_block_adder(_block):
        if _block.block_type == BlockType.FOREACH:
            for inner_block in _block.blocks.values():
                recursive_block_adder(inner_block)
        all_blocks.add(_block)

    for block in workflow.blocks.values():
        recursive_block_adder(block)
    return all_blocks


def get_all_conclusions_in_workflow(work_flow_path: str) -> Set[str]:
    return {b.block_id for b in get_all_blocks_in_workflow(work_flow_path) if b.block_type == BlockType.CONCLUSION}
