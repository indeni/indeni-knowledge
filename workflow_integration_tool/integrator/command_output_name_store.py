from collections import namedtuple

CommandOutputNameStore = namedtuple('CommandOutputNameStore', ['pair_name', 'command', 'output_name'])
