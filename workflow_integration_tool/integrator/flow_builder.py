from integrator.command_output_variant_store import CommandOutputVariantStore
from integrator.command_output_name_store import CommandOutputNameStore
from integrator.yaml_parsers import WorkflowTestParser, WorkflowCommandParser
from integrator.flow import Flow
from typing import Dict, List, Set


OUTPUTS_FOLDER_NAME = 'outputs'


class FlowBuilder:
    def __init__(self, path: str):
        self._parser = WorkflowTestParser(path)
        self.tags: Dict[str, str] = {}
        self.__generate_tags()
        # Flows before any processing has been done
        self._raw_flows: List[Flow] = []
        # Flows after the output name has been replaced by the actual output
        self._processed_flows: List[Flow] = []
        self.__generate_raw_flows()
        self.__generate_processed_flows()
        self._workflow_name: str = ''
        self.__generate_workflow_name()

    def __generate_tags(self):
        self.tags = self._parser.valid_yaml['tags']

    def __generate_raw_flows(self):
        all_flows_yaml = self._parser.valid_yaml['flows']
        for key, val in all_flows_yaml.items():
            listed_pairs = val['arrange']
            pairs = []
            for listed_pair in listed_pairs:
                pairs.append(CommandOutputNameStore(key, listed_pair[0], listed_pair[1]))
            issue_items = set(val.get('issue_items', []))
            self._raw_flows.append(Flow(key, pairs, val['assert']['conclusion'], issue_items))

    def __generate_processed_flows(self):
        self._processed_flows = [self.__raw_flow_to_processed_flow(flow) for flow in self._raw_flows]

    def __raw_flow_to_processed_flow(self, raw_flow: Flow) -> Flow:
        new_pairs = []
        for pair in raw_flow.pairs:
            pair_name = pair.pair_name
            command = f'{OUTPUTS_FOLDER_NAME}/{self.tags["vendor"]}/{pair.command}'
            command_parser = WorkflowCommandParser(command, self.tags)
            output_name = pair.output_name
            try:
                command_pair = command_parser.search_pair_by_output_name(output_name)
                if self.__ensure_tags_match_variant(command_pair.variant):
                    new_pairs.append(CommandOutputVariantStore(pair_name, command_parser.command, command_pair.output,
                                                               command_pair.variant))
            except ValueError as e:
                print(e)

        return Flow(raw_flow.name, new_pairs, raw_flow.get_conclusion(), raw_flow.issue_items)

    def __ensure_tags_match_variant(self, variant_tags: Dict[str, str]) -> bool:
        for tag_key, tag_val in self.tags.items():
            if tag_key not in variant_tags:
                # In this case, the variant says "we work for any tag_val"
                continue
            elif tag_val not in self.__make_list(variant_tags[tag_key]):
                # Basically, if it's specified as a tag_val AND specified in the variant, it should match/be in list
                # so if it doesn't match/isn't in the list, return False.
                return False
        return True

    def __generate_workflow_name(self):
        self._workflow_name = self._parser.valid_yaml['workflow']

    @staticmethod
    def __make_list(obj):
        return obj if type(obj) == list else [obj]

    @property
    def workflow_name(self):
        return self._workflow_name

    @property
    def flows(self) -> List[Flow]:
        return self._processed_flows.copy()

    @property
    def conclusions(self) -> Set[str]:
        return {x.get_conclusion() for x in self.flows}
