import yaml
from typing import List, Dict
from integrator.command_output_variant_store import CommandOutputVariantStore


class WorkflowTestParser:
    def __init__(self, path: str):
        self._input_yaml_path: str = path
        self._raw_yaml_content = yaml.safe_load(open(path, 'r'))
        self.__validate_yaml_structure()

    def __validate_yaml_structure(self):
        y_dict = self._raw_yaml_content.copy()
        assert type(y_dict.get('workflow', None)) == str
        assert type(y_dict.get("tags", None)) == dict
        assert type(y_dict.get('flows', None)) == dict
        for flow in y_dict['flows'].values():
            assert type(flow.get('arrange', None)) == list
            assert type(flow.get('assert', None)) == dict
            if 'issue_items' in flow:
                assert type(flow['issue_items']) == list
            assert type(flow['assert'].get('conclusion', None)) == str

    @property
    def valid_yaml(self):
        return self._raw_yaml_content.copy()


class WorkflowCommandParser:
    def __init__(self, command_path: str, tags: Dict):
        """
        Object for extracting all appropriate pairs (specified by tags) from a command file and turning them into
        CommandOutputVariantStore objects.
        :param command_path: Path to command file (currently those in outputs/VENDOR/TYPE/...)
        :param tags: Dictionary of tags that commands should match against
        """
        self._command_path = command_path
        self.command = None
        self._tags = tags
        self._raw_yaml_content = yaml.safe_load(open(self._command_path, 'r'))
        self.__validate_structure()
        self._command_output_pairs: List[CommandOutputVariantStore] = []
        self.generate_command_output_pairs()

    def __validate_structure(self):
        y_dict = self._raw_yaml_content
        assert 'command' in y_dict and type(y_dict['command']) == str, "commands should be strings"
        assert 'variants' in y_dict and type(y_dict['variants']) == dict
        for variant_name, variant in y_dict['variants'].items():
            assert type(variant) == dict
            assert 'outputs' in variant and type(variant['outputs']) == dict
            for output_name, output in variant['outputs'].items():
                assert type(output) == str, "outputs should be strings"

    def search_pair_by_output_name(self, pair_name: str) -> CommandOutputVariantStore:
        # Not the most efficient search (actually, asymptotically it is) but better for testing
        results = [x for x in self._command_output_pairs if x.pair_name == pair_name]
        if len(results) == 0:
            raise ValueError(f'No pair called {pair_name} in "{self._command_path}" found matching tags {self._tags}')
        return results[0]

    def get_command_output_pairs(self) -> List[CommandOutputVariantStore]:
        return self._command_output_pairs.copy()

    def generate_command_output_pairs(self) -> None:
        """
        Takes the first command in self.raw_yaml_content that matches self.tags and turns it into a CommandOutputPair.
        :return: None
        """
        y_dict = self._raw_yaml_content.copy()
        self.command = y_dict['command']
        variant_dict = y_dict['variants']
        for variant_name, variant_obj in variant_dict.items():
            if not self.__does_variant_dict_match_tags(variant_obj, self.tags):
                continue
            output_dict = variant_obj['outputs']
            for output_name, output in output_dict.items():
                pair = CommandOutputVariantStore(output_name, self.command, output, variant_name)
                self._command_output_pairs.append(pair)

    @staticmethod
    def __does_variant_dict_match_tags(variant_dict: Dict, tags_dict: Dict) -> bool:
        def make_into_list(obj):
            return obj if type(obj) == list else [obj]
        # If a tag isn't specified in the YAML, it means all values are acceptable. Otherwise tag's val should match
        for tag, val in tags_dict.items():
            if tag in variant_dict and val not in make_into_list(variant_dict[tag]):
                return False
        return True

    @property
    def tags(self):
        return self._tags
