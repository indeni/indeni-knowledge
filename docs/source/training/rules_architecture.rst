.. _rules_architecture:

Rules Architecture
==================


Core-Rules
----------

* Created by Indeni developers.
* Embedded into the indeni source code.
* Cannot be changed on the client environment.

Stable-Template-Base-Rules
--------------------------

* Created by IKEs
* Inherits from templates
* Must be with extension \*.scala
* The rules are packaged through Jenkins into template-based-rules.jar.
* The file is located on ``/usr/share/indeni-knowledge/stable/rules/templatebased/``
* The Jar file should not be changed on the client environment

Overwrite-Template-Base-Rules
-----------------------------

* Created by IKEs
* Inherits from templates
* Can be with extension \*.scala or \*.rule but should be \*.scala for consistency
* The files should be added to ``/usr/share/indeni-knowledge/overwrite/rules/templatebased/``
* In case of collision with core-rules, the core-rules will be used.
* In case of collision with  stable-template-base-rules, overwrite-template-base-rules  will be used


*Important:*
There is a log file  /usr/share/indeni-server/logs/rules/general.log
general.log shows the loading/failing of each rule.
The IKEs should use the file for debugging.