.. _knowledge_training:

Knowledge Training
==================
Indeni is always looking for the best and brightest talent. You can view the openings on our career page here. Once you have been selected as an Indeni Knowledge Expert (IKE), you will go through the following training courses.

Prerequisites for training
--------------------------
Official certification in the technology you apply for and at least five years experience in working with it.
Strong English skills are required as you will be doing some documentation (but no need to be Shakespeare).