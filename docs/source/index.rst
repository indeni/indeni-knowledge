.. Indeni Knowledge documentation master file, created by
   sphinx-quickstart on Thu Jan 24 17:42:48 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Indeni Knowledge's documentation!
============================================


About Indeni knowledge
----------------------
Indeni crowdsources the issues, commands (rules) and data points (metrics) that monitor and inspect devices. The combination of data and context is what we call “Indeni Knowledge”.

The collection and analysis of script outputs located in the knowledge database are coded by experts at the device manufacturer, industry experts, and Indeni customers. It is through these custom scripts and the collaboration across environments that Indeni is able to learn ‘normal’ and ‘abnormal’ behavior.


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   training/knowledge_training
   training/rules_architecture
   devtools/devtools




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
