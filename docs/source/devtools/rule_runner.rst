.. _rule_runner:

Rule Runner
===========

A command line tool for executing rules


If you have Indeni server install you will have ``rule-runner`` cli tools build into your Indeni server.
To run rule-runner on you PC you can get the latest rule-runner from `here <https://indeni-public.s3.amazonaws.com/packages/tools/devtools/rule-runner-0.0.0.develop.zip>`_


usage
-----

.. code-block:: bash

    Usage: rule-runner [compile|run|display] [options] <args>...

In order to run a rule with the rule-runner tool, one must provide a file which describes the input of the rule.
The file is written in YAML format, and looks as so:


.. code-block:: YAML

    devices:
        device-a: # device ID
            tags: # device's tags
                x: y
                n: 1
                n2: 2.0
                b: true
            metrics: # device's metrics
                -
                    type: ts # time-series / double metric
                    name: cpu-usage1 # im.type
                    tags: # metrics' tags (optional)
                        cpu-id: 1
                    data: [1, 1, null, 2.0] # the time-series points. First is the oldest while last is the latest.
                -
                    type: ts
                    name: cpu-usage2
                    step: 10 # overrides the default time-series step (1 minute) in milliseconds
                    data: [1, 2.0, 3.3]
                -
                    type: ts
                    name: cpu-usage3
                    step: 10
                    shift: -50 # "shifts" the entire time-series from the time of the rule execution in milliseconds. This must be a negative number (default is 0) and multiple of the step.
                    tags:
                        cpu-id: 3
                    data: [] # it's possible that the metric is empty
                -
                    type: snapshot # snapshot / complex metric
                    name: health1
                    tags:
                        disk: /path
                    data:
                        most-recent:
                            single: # "single" dimension (not array)
                                x: 1
                        middle: # optional
                            timestamp: -1 # optional, much like the time-series shift (default 0)
                            first-seen: -2 # same
                            single: # can be empty
                -
                    type: snapshot
                    name: health2
                    data:
                        most-recent:
                            multi: # "multi" dimension (array)
                                -
                                    ip: 1
                                    mask: 1
                                -
                                    ip: 2
                                    mask: 2
                        middle: # optional
                            multi:
                                -
                                    ip: 1
                                    mask: 1
                                - # can be empty
                        oldest: # optional
                            multi: [] # can be empty
        device-b: # can have more devices
    # this is optional
    config-parameters:
        a: val
        b: 1
        c: 2.0
        d: false


The input in this file is completely fictitious and mostly serves as a purpose to demonstrate what is a valid input.

The file consists of two major sections; devices, which detail all of the devices and their metrics; and config-parameters, an optional section allowing override the default values of the rule's configuration parameters, as if they were configured by a user.


Examples
--------

The following command compile ``/rules/templatebased/paloaltonetworks/panw_url_filtering_ensure_block_categories.scala`` rule and test the rule against ``/test/panw/panos/panos-url-filtering-bad-categories/panos-url-filtering-bad-categories-no-issue.yaml``. Run the `rule-runner` from the root folder  of your knowledge repository

.. code-block:: bash

   <path-to-your-command-runner>/rule-runner compile ./rules/templatebased/paloaltonetworks/panw_url_filtering_ensure_block_categories.scala --input ./test/panw/panos/panos-url-filtering-bad-categories/panos-url-filtering-bad-categories-no-issue.yaml



