.. _command_runner:

Command Runner
==============

The command-runner is a command line tool for executing IND scripts. You can use CommandRunner to test .ind scripts (both interrogation and monitoring). Here are the most common "test modes":

* **parse-only** – Test your script against some input data (e.g., some command output you've copied from a device) without actually connecting to a live device.

* **full-command** – Test your script against a live, running device.

* **test create** – Create a unit test for an .ind script.

* **test run** – Run all of the unit tests for an .ind script.

If you have Indeni server install you will have ``command-runner`` cli tools build into your Indeni server. To run command-runner on you PC you can get the latest command-runner from `here <https://indeni-public.s3.amazonaws.com/packages/tools/devtools/command-runner-latest.zip>`_



usage
-----

.. code-block:: bash

    Usage: command-runner [parse-only|compile-only|full-command|test] [options] <args>...

Run ``command-runner --help`` for more details.
