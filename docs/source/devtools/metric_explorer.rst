.. _metric_explorer:

Metric Explorer
===============

Introduction
------------
The Metric Explorer gives you the ability to to look into the “in memory” metrics inside Indeni’s server in a visualization way such as graphs for the double metrics and as a JSON for the complex metrics

How to use it
-------------
The Metric Explorer is hidden behind a “feature flag” and it is not available to our customers. You must access the specific route to see the Metric Explorer page.
In your browser go to: ``https://<YOUR_IP>/#!/dev/metric-explorer``

You should see this screen:

.. image:: ../images/metric_explorer/metric_explorer.*

On the left side, you have two select boxes, the first one is for the device/s and the second one is for the metrics. *Please make sure you choose a device/s first* otherwise you will not get any metrics.

Devices selection
-----------------
.. image:: ../images/metric_explorer/device_selection.*

Metrics selection
-----------------
.. image:: ../images/metric_explorer/metric_selection.*


Explore the data
-----------------

Double metric graphs:

.. image:: ../images/metric_explorer/double_metric_graph.*

Complex metric view:

.. image:: ../images/metric_explorer/complex_metric.*

METADATA:

The metadata component includes all the “tags” that the metric has. On this example, we are looking at “CPU usage” of the F5 device. We can see that we have two “tags” properties. You swap between them with the “NEXT” button.

.. image:: ../images/metric_explorer/metadata.*











