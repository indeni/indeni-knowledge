from parser_service.public import helper_methods

# Copy below content into the parser file and change the parameters.
# Make sure to remove these lines before you create the test files and before issuing PR
FILE_PATH_RAW_DATA = "<filename>"
f = open(FILE_PATH_RAW_DATA, "r")
raw_data = f.read()
f.close()
helper_methods.print_list("<class_name>".parse(raw_data, {}, {}))


# Example from ../parsers/src/checkpoint/asg/maestro_mho_device_count/maestro_mho_device_count.py
# class MaestroSecurityGroupDeviceCountParser(BaseParser):
#
#     def parse(self, raw_data: str, dynamic_var: dict, device_tags: dict):
#
#         # Step 1 : Data Extraction
#         data = helper_methods.parse_data_as_json(raw_data)
#
#         # Step 2 : Data Processing
#         member_count = 0
#         for sg in data['security_groups']:
#             chassis_count = 0
#             for chassis in data['security_groups'][sg]['chassis']:
#                     chassis_count += 1
#
#             if chassis_count > member_count:
#                 member_count = chassis_count
#         # Step 3 : Data Reporting
#         self.write_tag("device-count", str(member_count))
#
#         return self.output
#
#
# FILE_PATH_RAW_DATA = "/Users/shaynaveh/repos/indeni-knowledge/parsers/test/checkpoint/asg/maestro_mho_device_count/4-gw-2-sg/input_0_0"
# f = open(FILE_PATH_RAW_DATA, "r")
# raw_data = f.read()
# f.close()
# helper_methods.print_list(MaestroSecurityGroupDeviceCountParser().parse(raw_data, {}, {}))