import argparse
import urllib.request
import urllib.parse
import json


def parse(data: list, prefix_name: str):
    try:
        ref = data['source']['defect'].pop()
        versions_list = data['affects']['vendor']['vendor_data'].pop()['product']['product_data'].pop()['version']['version_data']
        versions_tuple = []
        for line in versions_list:
            if '=' in line.values():
                versions_tuple_temp = "  -\n"'    - "' + line['version_name'] + '"\n' + '    - "' + str(line['version_value']).replace('*', '9999') + '"'
                versions_tuple.append(versions_tuple_temp)
            elif '!>=' in line.values():
                versions_tuple_temp = "  -\n"'    - "' + line['version_name'] + '"\n' + '    - "' + line['version_value'] + '"'
                versions_tuple.append(versions_tuple_temp)
            elif '<=' in line.values():
                versions_tuple_temp = "  -\n"'    - "' + line['version_name'] + '"\n' + '    - "' + line['version_value'] + '"'
                versions_tuple.append(versions_tuple_temp)
        friendly_name = str(data['CVE_data_meta']['TITLE'])
        alert_description_long = str(data['description']['description_data'][0]['value']).replace("\n", ' ').replace("  ", "")
        if "This issue affects" in alert_description_long:
            alert_description, value = alert_description_long.split("This issue affects", 1)
        else:
            alert_description = alert_description_long
        vendor_severity_rating = str(data['impact']['cvss']['baseSeverity']).title()
        if vendor_severity_rating == 'Critical':
            severity = 'CRITICAL'
        else:
            severity = 'WARN'
        if not data['work_around']:
             base_remediation_text = 'None, consider upgrading your version'
        else:
             base_remediation_text = str(data['work_around'].pop()['value']).replace("\n\n", '\n  ')
        parsed_data = [ref, friendly_name, alert_description, vendor_severity_rating, base_remediation_text, versions_tuple, severity]
        return parsed_data
    except:
        print("An exception error occurred, please make sure the json web-site is in the correct format, and the code can parse it correctly")


def create_rule(parsed_data: list):
    path = "/Users/Imanuelm/temp/"  # This line should be modified manually by the user.
    prefix_name_lower = prefix_name.lower().replace('-', '_')
    if 'CVE' in prefix_name:
        try:
            with open(path + "panos_vulnerability_" + prefix_name_lower + "_rule.yaml","w+") as file:
                    file.write("rule_type: template-based\n"
                               "template_name: SecurityVulnerabilityChecks\n"
                               "rule_name: panos_vulnerability_" + prefix_name_lower + "_rule\n"
                               "rule_categories:\n"
                               "  - SecurityRisks\n"
                               "rule_friendly_name: " + '"' + prefix_name + ' ' + parsed_data[1] + '"\n' +
                               "alert_description: " + '"' + parsed_data[2] + " (Ref #" + parsed_data[0] + ") (" + prefix_name + ')"' "\n"
                               "version_ranges:\n")
                    for versions_print in parsed_data[5]:
                        file.write(versions_print + "\n")
                    file.write("severity: " + parsed_data[6] + "\n"
                               "os_name: panos\n"
                               "vendor_severity_rating: " + parsed_data[3] + "\n"
                               "device_category: PaloAltoNetworks\n"
                               "base_remediation_text: |\n  " + parsed_data[4] + "\n")
                    print("\nFile " + path + "panos_vulnerability_" + prefix_name_lower + "_rule.yaml" " has been created successfully!\nPlease make sure to review the file, and version ranges before you push the code")
        except:
            print("An exception error occurred, please make sure the json web-site is in the correct format, and the code can parse it correctly")
    else:
        prefix_name_lower_pansa = prefix_name_lower.replace("pan_sa", "pansa")
        try:
            with open(path + "panos_vulnerability_" + prefix_name_lower_pansa + "_rule.yaml", "w+") as file:
                file.write("rule_type: template-based\n"
                           "template_name: SecurityVulnerabilityChecks\n"
                           "rule_name: panos_vulnerability_" + prefix_name_lower_pansa + "_rule\n"
                           "rule_categories:\n"
                           "  - SecurityRisks\n"
                           "rule_friendly_name: " + '"' + prefix_name + ' ' + parsed_data[1] + '"\n' +
                           "alert_description: " + '"' + parsed_data[2] + " (Ref #" + parsed_data[0] + ") (" + prefix_name + ')"' "\n"
                           "version_ranges:\n")
                for versions_print in parsed_data[5]:
                    file.write(versions_print + "\n")
                file.write("severity: " + parsed_data[6] + "\n"
                           "os_name: panos\n"
                           "vendor_severity_rating: " + parsed_data[3] + "\n"
                           "device_category: PaloAltoNetworks\n"
                           "base_remediation_text: |\n  " + parsed_data[4] + "\n")
                print("\nFile " + path + "panos_vulnerability_" + prefix_name_lower_pansa + "_rule.yaml" " has been created successfully!\nPlease make sure to review the file, and version ranges before you push the code")
        except:
            print("An exception error occurred, please make sure the json web-site is in the correct format, and the code can parse it correctly")


example_text = """Usage:
    python pan_cve_create.py --cve "<URL>"
Example:
    python3 pan_cve_create.py --cve https://security.paloaltonetworks.com/json/PAN-SA-2020-0006"""

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Create new Panos rule from CVE link.',
                                     epilog=example_text,
                                     formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('--cve',
                        help="Convert URL data into a new Indeni rule for Pan CVE",
                        action='store',
                        dest='url',
                        default=None,
                        required=True)
    url = parser.parse_args().url
    response = urllib.request.urlopen(url)
    data = json.loads(response.read())
    if data is None:
        print("data is empty")
        exit(1)

    prefix_name = urllib.parse.urlsplit(url).path.translate({ord(i): None for i in "json/"})
    if prefix_name is None:
        print("prefix_name is empty")
        exit(1)

    parsed_data = parse(data, prefix_name)
    if parsed_data is None:
        print("parsed_data is empty")
        exit(1)

    create_rule(parsed_data)

# TODO:
# Once the rule file is created, manual changes needed for the "version ranges" section:
# 1. Need to go over the version ranges, and substract 1 from all the LAST ones.
# Example:
# From:
#   -
#     - "8.1"
#     - "8.1.13"
# Change to:
# -
#     - "8.1"
#     - "8.1.12"
#
# 2. In case both versions would mean the same after the subtract, remove it (make sure to review carefully the PAN web page, before doing that):
# Example:
# The following seems to apply this version is not affected, so we can remove it:
# -
#     - "9.0"
#     - "9.0.1"
