#  \_\_init__.py File Finder

\_\_init__.py File Finder is a Python script which search in all folders in the tree structure of a given path (Python root path ) and returns a list of folders where the init file must be added. It is also implemented the automation of fixing all that folders where the init file is needed and it is not present.

## Configuration

In order to configure the paths where the script is going to search it is needed to modify the list __project_python_root_folders__ defined in the very beginning of the script including all the folders to be considered.

The folder path can be defined as absolute path or relative path from script location.

By default the script only considers Indeni-Knowledge Python root paths:

```bash
.
├── automation
├── parsers
│   ├── src
```

## Usage
Execute located in same folder as script is located:

```bash
python3 init_file_finder.py
```

Navigate through the menu to pick one of the options listed :
1. List all folders where \_\_init__.py file is pending to add
2. Add \_\_init__.py file in all folders listed in option 1

Also a prompt otput is returned with a summary:
```bash
    ##########################
       __init__.py Finder
    ##########################
> [0] Exit
  [1] List pending __init__.py
  [2] Fix pending __init__.py
```



## Version info

|Version|Date|Name|Email|
|-------|----|----|-----|
|0.1|21/10/2020|Jorge Riveiro|jorge.r@indeni.com|