import glob
import os
import time
from simple_term_menu import TerminalMenu   # pip install simple-term-menu

project_python_root_folders = [
    '../../automation',
    '../../parsers/src',
]

def create_list_folders () -> list:
    for root_folder in project_python_root_folders:
        rexp = root_folder + '/**/*.py'
        files = glob.glob(rexp,   recursive = True)
        folders = []
        for file in files:
            folders.append(os.path.dirname(file)) if os.path.dirname(file) not in folders else folders
        folders_to_add_file = []
        for folder in folders:
            init_path = folder + '/__init__.py'
            if not glob.glob(init_path)  :
                folders_to_add_file.append (folder)
            while folder != root_folder:
                folder = folder.rsplit ("/", 1)
                init_path = folder[0] + '/__init__.py'
                if not glob.glob(init_path)  :
                    folders_to_add_file.append (folder[0])  if folder[0] not in folders_to_add_file else folders_to_add_file
                folder = folder[0]
    return (folders_to_add_file)

def main():
    init_finder_menu = [
        "[0] Exit",
        "[1] List pending __init__.py",
        "[2] Fix pending __init__.py",
        ]
    menu_title = """
    ##########################
        __init__.py Finder
    ##########################
    """

    terminal_menu = TerminalMenu(init_finder_menu,
                                                            title=menu_title,
                                                            accept_keys=("enter", "alt-d", "ctrl-i"))
    main_sel = terminal_menu.show()
    if main_sel == 1:
        for folder in create_list_folders():
            print(folder)
    elif main_sel == 2:
        for folder_to_fix in create_list_folders():
            with open('{}/__init__.py'.format(folder_to_fix), 'w') as initfile:
                initfile.close



if __name__ == "__main__":
    main()
