#!/usr/bin/python

import os
import shutil
import sys
from ruamel.yaml import YAML
from ruamel.yaml.comments import CommentedMap

yaml = YAML()
yaml.indent(mapping=4, sequence=6, offset=3)


def find_files(last_data_dir, ending):
    files_list = []
    for root, dirs, files in os.walk(last_data_dir):
        for f in files:
            if f.endswith(ending):
                files_list.append(os.path.join(root, f))
    return files_list


def get_header(line):
    if line.startswith("#! META"):
        return "Metadata"
    if line.startswith("#! COMMENTS"):
        return "Comments"
    if line.startswith("#! REMOTE"):
        return "Remote"
    if line.startswith("#! PARSER"):
        return "Parser"


def parse_sections(dsl):
    lines = dsl.split("\n")
    sections = []
    current_type = None
    current_header = None
    current_text = []
    for line in lines:
        header = get_header(line.strip())
        if header is None:
            current_text.append(line)
        else:
            if current_type is not None:
                sections.append((current_type, current_header, '\n'.join(current_text)))
            current_text = []
            current_type = header
            current_header = line
    sections.append((current_type, current_header, '\n'.join(current_text)))
    return sections


def get_remote_body(remote_type, remote_text):
    if remote_type == "SSH":
        return remote_text
    if remote_type == "HTTP":
        return remote_text.split("\n")[0].split("url:")[1]
    if remote_type == "COPY":
        return remote_text.split("-")[1]


def get_parser_ext(remote_type):
    if remote_type == "AWK":
        return ".awk"
    if remote_type == "XML":
        return ".xml.yaml"
    if remote_type == "JSON":
        return ".json.yaml"


def create_raw_step(remote, parser):
    step = CommentedMap()
    step["run"] = remote
    if parser is not None:
        step["parse"] = parser
    return step


def get_steps(step_sections, script_name, script_folder):
    def get_remote_step(remote_step, step_number):
        remote_type = remote_step[1].split("::")[1]
        body = get_remote_body(remote_type, remote_step[2]).strip()
        if remote_type == "SSH" and ("awk" in body or "\n" in body):
            remote_file_name = script_name + ".remote." + str(step_number) + ".bash"
            full_path = script_folder + "/" + remote_file_name
            with open(full_path, "w") as ind_file:
                ind_file.write(body)
            return CommentedMap([("type", remote_type), ("file", remote_file_name)])
        else:
            return CommentedMap([("type", remote_type), ("command", body)])

    def try_get_parser_step(parser_step, step_number):
        if parser_step is not None:
            parser_type = parser_step[1].split("::")[1]
            parser_ext = get_parser_ext(parser_type)
            body = parser_step[2]
            parser_file_name = script_name + ".parser." + str(step_number) + parser_ext
            full_path = script_folder + "/" + parser_file_name
            with open(full_path, "w") as ind_file:
                ind_file.write(body)
            return CommentedMap([("type", parser_type), ("file", parser_file_name)])

    def internal_get_steps(sections, result):
        remote_steps = [section for section in sections if section[0] == "Remote"]
        if len(remote_steps) == 0:
            return result
        else:
            remote_step = remote_steps[0]
            remote_index = sections.index(remote_step)
            is_remote_last = remote_index + 1 == len(sections)
            if not is_remote_last and sections[remote_index + 1][0] == "Parser":
                parser_step = sections[remote_index + 1]
            else:
                parser_step = None
            step_number = len(result) + 1
            step = create_raw_step(get_remote_step(remote_step, step_number),
                                   try_get_parser_step(parser_step, step_number))
            result.append(step)
            return internal_get_steps(sections[remote_index + 1:], result)

    return internal_get_steps(step_sections, [])

if len(sys.argv) == 1:
    print('Missing input folder argument')
    sys.exit()
print('Converting ind scripts in folder ', sys.argv[1])
files = find_files(sys.argv[1], ".ind")
success = 0
fail = 0
for file in files:
    try:
        script_name = os.path.splitext(os.path.basename(file))[0]
        script_folder = os.path.splitext(file)[0]
        if os.path.exists(script_folder):
            shutil.rmtree(script_folder)
        os.mkdir(script_folder)
        with open(file, "r") as f:
            parser_text = f.read()
        sections = parse_sections(parser_text)
        metadata_section = [section for section in sections if section[0] == "Metadata"]
        metadata_yaml = yaml.load(metadata_section[0][2])
        comments_section = [section for section in sections if section[0] == "Comments"]
        if len(comments_section) == 1:
            comments_yaml = yaml.load(comments_section[0][2])
            metadata_yaml["comments"] = comments_yaml
        step_sections = [section for section in sections if section[0] == "Parser" or section[0] == "Remote"]
        steps = get_steps(step_sections, script_name, script_folder)
        metadata_yaml["steps"] = steps
        parser_file_name = script_name + ".ind.yaml"
        full_path = script_folder + "/" + parser_file_name
        with open(full_path, 'w') as outfile:
            yaml.dump(metadata_yaml, outfile)
        success = success + 1
        os.remove(file)
    except Exception as e:
        print(("issue with file " + file))
        print(e)
        fail = fail + 1
print(success,'/',success+fail, " files converted")
