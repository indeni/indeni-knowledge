#!/usr/bin/env python3
# PYTHON_ARGCOMPLETE_OK

import argparse
import glob
import ipaddress
import os
import pprint
import re
import shutil
import sys
import time
from collections import namedtuple
from typing import cast, Dict, List


Credentials = namedtuple('Credentials', ['dbname', 'user', 'password'])

DB_USERNAME = 'indeni'
DB_PASSWORD = 'indeni'
DB_LOCATION = 'indeni'
DB_CREDENTIALS = Credentials(DB_USERNAME, DB_PASSWORD, DB_LOCATION)


TERMINAL_WIDTH = 120

KNOWLEDGE_VERSION_PROPERTIES_PATH = '/usr/share/indeni-knowledge/stable/ind/meta/knowledge-version.properties'


def restart_indeni_services():
    import pexpect
    imanage = pexpect.spawn('python /usr/share/indeni-services/triton/deploy/roles/itools/files/imanage.py')
    time.sleep(1)
    imanage.sendline('3')
    imanage.interact()


def delete_all_alerts(credentials, device_id):
    user_input = input('Are you sure you wish to DELETE ALL ALERTS [y|N]')
    if user_input != 'y':
        print('No alerts deleted')
        sys.exit(0)
    identifier = make_device_id(credentials, device_id)
    query = f"delete from alert where device_id='{identifier}';"
    open_conn_and_execute_command(query, credentials, commit=True)
    restart_indeni_services()


def delete_alert(credentials, alert_id):
    user_input = input('Are you sure you wish to DELETE ALERT [y|N]')
    if user_input != 'y':
        print('No alert deleted')
        sys.exit(0)
    query = f"delete from alert where id='{alert_id}'"
    open_conn_and_execute_command(query, credentials, commit=True)
    inp = input('Restart Indeni Services? [y|N]')
    if inp == 'y':
        restart_indeni_services_foreground()


def is_ip_address(string: str) -> bool:
    try:
        _ = ipaddress.IPv4Address(string)
        return True
    except ipaddress.AddressValueError:
        return False


def make_device_id(credentials, string: str) -> str:
    if not is_ip_address(string):
        return string
    query = f"select id from device where ip_address='{string}';"
    query_result = open_conn_and_execute_command(query, credentials, fetch_all=True)
    if len(query_result) > 0:
        return query_result[0][0]
    else:
        print(f'IP address {string} not found in Database')
        sys.exit()


def make_device_ip(credentials, string: str) -> str:
    if is_ip_address(string):
        return string
    query = f"select ip_address from device where id='{string}'"
    query_result = open_conn_and_execute_command(query, credentials, fetch_one=True)
    # open_conn_and_execute_command() returns a tuple, so must access position 0
    return query_result[0]


def show_device_info(credentials, device_id):
    from tabulate import tabulate
    device_id = make_device_id(credentials, device_id)
    headers = ['id', 'ip_address', 'name', 'monitoring_enabled', 'last_interrogation', 'last_seen', 'tags']
    headers_str = ', '.join(headers)
    query = f"select {headers_str} from device where id='{device_id}'"
    record = open_conn_and_execute_command(query, credentials, fetch_one=True)
    print(f'Device info for "{device_id}"')
    record_as_strings = [str(x) for x in record]
    record_as_strings[-1] = record_as_strings[-1].replace(', ', ',\n')
    print(tabulate([record_as_strings], headers=headers))


def show_all_devices(credentials):
    from tabulate import tabulate
    headers = ['id', 'ip_address', 'name']
    headers_str = ', '.join(headers)
    query = f'select {headers_str} from device;'
    records = open_conn_and_execute_command(query, credentials, fetch_all=True)
    print(f'Device info for all devices')
    print(tabulate(records, headers=headers))


def overwrite_file(file_path: str):
    def print_candidates(_candidates):
        for _idx, _can in enumerate(_candidates):
            print(f'{_idx})\t\t{_can}')

    file_name = os.path.basename(file_path)
    candidates = glob.glob(f'/usr/share/indeni-knowledge/stable/**/{file_name}', recursive=True)
    if len(candidates) == 0:
        print(f'No files found matching the name: {file_name}')
        return
    elif len(candidates) == 1:
        overwrite_path = candidates[0]
    else:
        # must select a file.
        while True:
            print_candidates(candidates)
            res = input('Select from one of the above')
            try:
                res = int(res)
                if 0 <= res < len(candidates):
                    break
                else:
                    print(f'Select a valid number (0-{len(candidates) - 1}) or CTRL+c to exit')
            except ValueError:
                print('Input must be a number')
            except KeyboardInterrupt:
                sys.exit(0)
        overwrite_path = candidates[res]
    # can now setup directory structure of file to copy and copy it.
    target_folder = os.path.dirname(overwrite_path).replace('stable', 'overwrite')
    if not os.path.exists(target_folder):
        os.makedirs(target_folder, exist_ok=True)
    shutil.copy(file_path, target_folder)
    print(f'Copied "{file_path}" to "{target_folder}"')


def open_conn_and_execute_command(command: str, credentials: Credentials,
                                  fetch_one=False, fetch_all=False, commit=False):
    import psycopg2
    connection = None
    cursor = None
    records = []
    try:
        connection = psycopg2.connect(database=credentials.dbname,
                                      user=credentials.user,
                                      password=credentials.password)
        cursor = connection.cursor()
        cursor.execute(command)
        if fetch_all:
            records = cursor.fetchall()
        elif fetch_one:
            records = cursor.fetchone()

        if commit:
            connection.commit()
    except psycopg2.Error as e:
        print('Error connecting to Postgres Database', e)
    finally:
        if connection:
            cursor.close()
            connection.close()
        return records


def fetch_all_device_id():
    query = 'select id from device;'
    records = [x[0] for x in open_conn_and_execute_command(query, DB_CREDENTIALS, fetch_all=True)]
    return records


def fetch_all_device_id_ip():
    query = 'select id, ip_address from device;'
    return open_conn_and_execute_command(query, DB_CREDENTIALS, fetch_all=True)


def create_device_to_metric_dict_for_all_devices(restart=False, device_ips=None) -> Dict[str, List[str]]:
    import tqdm
    if not restart:
        if not device_ips:
            device_ips = [os.path.splitext(os.path.basename(x))[0] for x in fetch_all_current_log_file_paths()]
        device_ip_id = {ip: _id for (_id, ip) in fetch_all_device_id_ip()}
        device_ids = [val for (key, val) in device_ip_id.items() if key in device_ips]
        device_to_metric_dict: Dict[str, List[str]] = {}
        # tqdm.tqdm is our progress bar
        for device_ip, device_id in tqdm.tqdm(sorted(list(device_ip_id.items()))):
            if device_id in device_ids:
                device_to_metric_dict[device_ip] = sorted(list(fetch_ind_files_from_device_no_restart(device_id)))
        return device_to_metric_dict
    else:
        return fetch_ind_files_from_devices_with_restart()


def fetch_all_ind_files_by_ip_and_log_path(_name: str, _file_path: str) -> set:
    while True:
        try:
            _log_file = open(_file_path, 'r')
            break
        except IOError:
            time.sleep(2)
    # at this point, the file has been opened
    _num_ind_files_to_wait_for = -1
    _ind_files_found = set()
    try:
        while _num_ind_files_to_wait_for == -1 or len(_ind_files_found) < _num_ind_files_to_wait_for:
            _line = _log_file.readline()
            if _line:
                # If the _num_ind_files_to_wait_for hasn't been set, set it
                if 'Start running command' in _line and _num_ind_files_to_wait_for == -1:
                    _num_ind_files_to_wait_for = int(_line.strip().split()[-1])
                    time.sleep(1)
                elif 'com.indeni.collector.actors.SchedulerActor: Command' in _line and ' scheduled' in _line:
                    _between_quotes = re.findall(r"'([^']*)'", _line)[0]
                    _ind_files_found.add(_between_quotes)
                elif 'Disconnected. Re-interrogating in' in _line:
                    time.sleep(1)
            else:
                time.sleep(1)
    except Exception as e:
        print(f'Device {_name}: {e}')
        raise e
    return _ind_files_found


def fetch_ind_files_from_device_no_restart(device_id: str) -> set:
    device_logs_path = '/usr/share/indeni-collector/logs/devices/'
    query = f"select ip_address from device where id='{device_id}';"
    ip_address = open_conn_and_execute_command(query, DB_CREDENTIALS, fetch_one=True)[0]
    path = os.path.join(device_logs_path, ip_address + '.log')
    return fetch_all_ind_files_by_ip_and_log_path(ip_address, path)


def fetch_ind_files_from_devices_with_restart() -> dict:
    device_logs_path = '/usr/share/indeni-collector/logs/devices/'
    device_backup_path = '/home/indeni/log_backup/'
    os.makedirs(device_backup_path, exist_ok=True)
    device_ip_paths = fetch_all_current_log_file_paths()
    device_ips = [os.path.splitext(os.path.basename(x))[0] for x in device_ip_paths]
    log_file_paths = []
    for device_ip in device_ips:
        print('Processing device', device_ip)
        log_file_path = os.path.join(device_logs_path, device_ip + '.log')
        log_file_backup_path = os.path.join(device_backup_path, device_ip + '.log')
        if not os.path.isfile(log_file_path):
            print(log_file_path, 'not found')
            continue
        log_file_paths.append(log_file_path)
        shutil.move(log_file_path, log_file_backup_path)
    restart_indeni_services_foreground()
    print('Beginning device inspection (this may take some time)')
    device_ip_to_ind_set_dict = {}
    for number, path in enumerate(log_file_paths, start=1):
        name = os.path.splitext(os.path.basename(path))[0]
        device_ip_to_ind_set_dict[name] = fetch_all_ind_files_by_ip_and_log_path(name, path)
        print(f'Progress: {number} / {len(log_file_paths)}')

    return device_ip_to_ind_set_dict


def write_device_to_metric_dict_to_file(target_file_path: str, device_list_path):
    import json
    with open(device_list_path, 'r') as f:
        ip_list = [x.strip() for x in f.readlines() if is_ip_address(x.strip())]
    with open(target_file_path, 'w') as f:
        json.dump(create_device_to_metric_dict_for_all_devices(device_ips=ip_list), f, indent=4, sort_keys=True)


def diff_two_json_files(old_json_path, new_json_path):
    def fetch_device_details_string(_device_id):
        _query = f"select name from device where ip_address='{_device_id}'"
        return ' '.join(open_conn_and_execute_command(_query, DB_CREDENTIALS, fetch_one=True))
    import json
    old_device_to_ind_dict = json.load(open(old_json_path))
    new_device_to_ind_dict = json.load(open(new_json_path))

    old_device_ids = set(old_device_to_ind_dict.keys())
    new_device_ids = set(new_device_to_ind_dict.keys())

    # if there are old device IDs that are now missing
    if old_device_ids.difference(new_device_ids):
        print('INFO: Some devices present before upgrade are no longer present.')
        print('INFO: We will compare only devices that appear both before and after upgrade.')
        print('INFO: Missing devices:')
        for device_id in old_device_ids.difference(new_device_ids):
            print(f'\t{device_id}')
    overlapping_ids = old_device_ids.intersection(new_device_ids)
    devices_with_diffs = set()
    for overlapping_id in overlapping_ids:
        old_set = set(old_device_to_ind_dict[overlapping_id])
        new_set = set(new_device_to_ind_dict[overlapping_id])
        if old_set == new_set:
            continue
        else:
            devices_with_diffs.add(overlapping_id)
    if devices_with_diffs:
        print('Metric diff:\n')
        print('  Changes:')
    else:
        print('No changes detected')
    for device_with_diff_id in devices_with_diffs:
        print(f'    Device: {device_with_diff_id}', fetch_device_details_string(device_with_diff_id))
        old_set = set(old_device_to_ind_dict[device_with_diff_id])
        new_set = set(new_device_to_ind_dict[device_with_diff_id])
        for removed_ind in old_set.difference(new_set):
            print(f'      - {removed_ind}')
        for added_ind in new_set.difference(old_set):
            print(f'      + {added_ind}')


def compare_current_device_to_metric_dict_with_old_json(old_json_path: str):
    def fetch_device_details_string(_device_id):
        _query = f"select name from device where ip_address='{_device_id}'"
        return ' '.join(open_conn_and_execute_command(_query, DB_CREDENTIALS, fetch_one=True))
    import json
    if not os.path.isabs(old_json_path):
        print('ERROR: only absolute file paths are accepted')
        sys.exit(1)
    if not os.path.isfile(old_json_path):
        print('ERROR: file not found', old_json_path)
        sys.exit(1)
    old_device_to_metric_dict = json.load(open(old_json_path))
    current_device_to_metric_dict = create_device_to_metric_dict_for_all_devices(restart=True)
    pprint.pprint(old_device_to_metric_dict)
    print()
    pprint.pprint(current_device_to_metric_dict)
    old_device_ids = set(old_device_to_metric_dict.keys())
    new_device_ids = set(current_device_to_metric_dict.keys())

    # if there are old device IDs that are now missing
    if old_device_ids.difference(new_device_ids):
        print('INFO: Some devices present before upgrade are no longer present.')
        print('INFO: We will compare only devices that appear both before and after upgrade.')
        print('INFO: Missing devices:')
        for device_id in old_device_ids.difference(new_device_ids):
            print(f'\t{device_id}')
    overlapping_ids = old_device_ids.intersection(new_device_ids)
    devices_with_diffs = set()
    for overlapping_id in overlapping_ids:
        old_set = set(old_device_to_metric_dict[overlapping_id])
        new_set = set(current_device_to_metric_dict[overlapping_id])
        if old_set == new_set:
            continue
        else:
            devices_with_diffs.add(overlapping_id)
    if devices_with_diffs:
        print('Metric diff:\n')
        print('  Changes:')
    else:
        print('No changes detected')
    for device_with_diff_id in devices_with_diffs:
        print(f'    Device: {device_with_diff_id}', fetch_device_details_string(device_with_diff_id))
        old_set = set(old_device_to_metric_dict[device_with_diff_id])
        new_set = set(current_device_to_metric_dict[device_with_diff_id])
        for removed_metric in old_set.difference(new_set):
            print(f'      - {removed_metric}')
        for added_metric in new_set.difference(old_set):
            print(f'      + {added_metric}')


def view_all_overwrite_files():
    print('Overwrite Files:')
    all_overwrites = glob.glob('/usr/share/indeni-knowledge/overwrite/**/*', recursive=True)
    file_overwrites = [x for x in all_overwrites if os.path.isfile(x)]
    for overwrite in file_overwrites:
        print(f'\t{overwrite}')


def show_knowledge_version():
    try:
        with open(KNOWLEDGE_VERSION_PROPERTIES_PATH, 'r') as f:
            lines = ''.join(f.readlines())
            print(lines)
    except IOError as e:
        print('Failure while fetching version')
        print(e)


def delete_all_overwrite_files(cli_inp: str):
    if cli_inp == 'all':
        print('Are you sure you want to delete all overwrite files? yes, no')
        inp = input()
        if inp != 'yes':
            print('No files deleted')
            return
        all_overwrites = glob.glob('/usr/share/indeni-knowledge/overwrite/**/*', recursive=True)
        file_overwrites = [x for x in all_overwrites if os.path.isfile(x)]
        for file in file_overwrites:
            os.remove(file)
            print(f'Deleted: {file}')
    elif cli_inp.startswith('/usr/share/indeni-knowledge/overwrite'):
        if os.path.isfile(cli_inp):
            os.remove(cli_inp)
        else:
            print(f'No such file found: {cli_inp}')
    else:
        print('Can only remove files under the overwrite folder (absolute path required)')


def get_metric(metric_name, device_id):
    import requests
    credentials = Credentials(DB_LOCATION, DB_USERNAME, DB_PASSWORD)
    db_device_id = make_device_id(credentials, device_id)
    requests.packages.urllib3.disable_warnings()
    params = (
        ('query', f'(im.name=={metric_name} and device-id==\'{db_device_id}\')'),
    )
    response = requests.get('https://localhost:9009/api/v1/metrics',
                            verify=False, auth=('admin', 'admin123!'), params=params)
    pprint.pprint(response.json())


def fetch_details_from_vendor_and_metric(vendor: str, metric: str):
    import requests
    query_url = 'https://localhost:9004/api/v1/catalog/rules'
    response = requests.get(query_url, verify=False)
    json_dict = response.json()
    rules_matching_metric_and_vendor = set()
    for rule_dict in json_dict['catalog_rules_response']['rules']:
        if rule_dict['type'] != 'rule':
            continue
        metrics_and_monitoring_scripts: dict = rule_dict['metrics_and_monitoring_scripts']
        metrics_of_rule = metrics_and_monitoring_scripts['metrics']
        basic_metadata: dict = rule_dict['basic_metadata']
        supported_vendors: list = rule_dict['supported_vendors_and_operating_systems'].keys()

        if metric in metrics_of_rule and vendor in supported_vendors:
            rules_matching_metric_and_vendor.add(basic_metadata['rule_file_path'])

    for rule in sorted(rules_matching_metric_and_vendor):
        print(rule)


def restart_indeni_services_foreground():
    import pexpect
    imanage = pexpect.spawn('python /usr/share/indeni-services/triton/deploy/roles/itools/files/imanage.py')
    time.sleep(1)
    imanage.sendline('3')
    time.sleep(1)
    imanage.sendline('yes')
    imanage.interact()


def backup_log_files_and_restart_indeni_services():
    device_logs_path = '/usr/share/indeni-collector/logs/devices/'
    device_backup_path = '/home/indeni/log_backup/'
    os.makedirs(device_backup_path, exist_ok=True)
    device_ip_paths = fetch_all_current_log_file_paths()
    device_ips = [os.path.splitext(os.path.basename(x))[0] for x in device_ip_paths]
    log_file_paths = []
    for device_ip in device_ips:
        print('Processing device', device_ip)
        log_file_path = os.path.join(device_logs_path, device_ip + '.log')
        log_file_backup_path = os.path.join(device_backup_path, device_ip + '.log')
        if not os.path.isfile(log_file_path):
            print(log_file_path, 'not found')
            continue
        log_file_paths.append(log_file_path)
        shutil.move(log_file_path, log_file_backup_path)
    restart_indeni_services_foreground()


def backup_log_files():
    device_logs_path = '/usr/share/indeni-collector/logs/devices/'
    device_backup_path = '/home/indeni/iktool_device_log_backup/'
    os.makedirs(device_backup_path, exist_ok=True)
    device_ip_paths = fetch_all_current_log_file_paths()
    device_ips = [os.path.splitext(os.path.basename(x))[0] for x in device_ip_paths]
    log_file_paths = []
    for device_ip in device_ips:
        print('Processing device', device_ip)
        log_file_path = os.path.join(device_logs_path, device_ip + '.log')
        log_file_backup_path = os.path.join(device_backup_path, device_ip + '.log')
        if not os.path.isfile(log_file_path):
            print(log_file_path, 'not found')
            continue
        log_file_paths.append(log_file_path)
        shutil.move(log_file_path, log_file_backup_path)
        print(f'"{log_file_path}" -> "{log_file_backup_path}"')


def ensure_metric_collection_completed() -> dict:
    def thread_run_until_device_ready(_name: str, _file_path: str) -> set:
        max_failures_to_conclude_offline = 3
        current_failures = 0
        while True:
            try:
                _log_file = open(_file_path, 'r')
                break
            except IOError:
                time.sleep(2)
        # at this point, the file has been opened
        _num_ind_files_to_wait_for = -1
        _ind_files_found = set()
        try:
            while (_num_ind_files_to_wait_for == -1 or len(_ind_files_found) < _num_ind_files_to_wait_for) and \
                    current_failures < max_failures_to_conclude_offline:
                _line = _log_file.readline()
                if _line:
                    # If the _num_ind_files_to_wait_for hasn't been set, set it
                    if 'Start running command' in _line and _num_ind_files_to_wait_for == -1:
                        _num_ind_files_to_wait_for = int(_line.strip().split()[-1])
                        time.sleep(5)
                    elif 'com.indeni.collector.actors.SchedulerActor: Command' in _line and ' scheduled' in _line:
                        _between_quotes = re.findall(r"'([^']*)'", _line)[0]
                        _ind_files_found.add(_between_quotes)
                    elif 'Disconnected. Re-interrogating in' in _line:
                        current_failures += 1
                        time.sleep(1)
                else:
                    time.sleep(1)
        except Exception as e:
            print(f'Device {_name}: {e}')
            raise e
        if current_failures < max_failures_to_conclude_offline:
            print(f'Device {_name}:\tfinished')
        else:
            print(f'Device {_name}:\tconcluded as offline')
        return _ind_files_found

    device_logs_path = '/usr/share/indeni-collector/logs/devices/'
    device_backup_path = '/home/indeni/log_backup/'
    os.makedirs(device_backup_path, exist_ok=True)
    device_ip_paths = fetch_all_current_log_file_paths()
    device_ips = [os.path.splitext(os.path.basename(x))[0] for x in device_ip_paths]
    log_file_paths = []
    for device_ip in device_ips:
        print('Processing device', device_ip)
        log_file_path = os.path.join(device_logs_path, device_ip + '.log')
        log_file_backup_path = os.path.join(device_backup_path, device_ip + '.log')
        if not os.path.isfile(log_file_path):
            print(log_file_path, 'not found')
            continue
        log_file_paths.append(log_file_path)
        shutil.move(log_file_path, log_file_backup_path)
    restart_indeni_services_foreground()
    print('Beginning device inspection (this may take some time)')
    device_ip_to_ind_set_dict = {}
    for number, path in enumerate(log_file_paths, start=1):
        name = os.path.splitext(os.path.basename(path))[0]
        device_ip_to_ind_set_dict[name] = thread_run_until_device_ready(name, path)
        print(f'Progress: {number} / {len(log_file_paths)}')

    # print('End of function')
    return device_ip_to_ind_set_dict


def fetch_all_current_log_file_paths():
    device_logs_path = '/usr/share/indeni-collector/logs/devices/'
    return glob.glob(f'{device_logs_path}*.log')


def print_metrics_from_device(device_id):
    metric_list = fetch_metrics_from_device(device_id)
    for metric in metric_list:
        print(metric)


def fetch_metrics_from_device(device_id):
    import requests
    device_id = make_device_id(DB_CREDENTIALS, device_id)
    requests.packages.urllib3.disable_warnings()
    params = (
        ('filter', f'{{"device-id":"{device_id}"}}'),
        ('key', 'im.name'),
        ('limit', '1000'),
    )

    response = requests.get('https://localhost:9009/api/v1/metrics/metadata/tag-value-suggest', params=params,
                            verify=False)
    metric_list = list(sorted(response.json()))
    return metric_list


def run_workflow_on_device(arguments=None):
    def get_vendor_tuple(_wf_path):
        _full_path = os.path.abspath(_wf_path)
        if 'checkpoint' in _full_path:
            return 'checkpoint', 'gaia'
        elif 'paloaltonetworks' in _full_path:
            return 'panw', 'panos'
        else:
            print(f'No vendor name found along path: {_full_path}')
            sys.exit(1)

    from indeni_workflow.remote.device_credentials import SshCredentials, HttpsCredentials
    from indeni_workflow.workflow_response import WorkflowIssueItemsConclusionResponse
    from indeni_workflow.workflow_test_tool import WorkflowTestTool
    from indeni_workflow.workflow import DeviceData, WorkflowSuccessResponse, WorkflowFailedResponse
    arg_dict = vars(arguments) if type(arguments) is not dict else arguments
    workflow_path = arg_dict['workflow']
    device_ip = arg_dict['device_ip']
    device_os_version = arg_dict['device_os_version']
    device_user = arg_dict.get('device_user', 'indeni')
    device_pass = arg_dict.get('device_pass', 'indeni123')
    vendor_tuple = get_vendor_tuple(workflow_path)
    vendor_dict = {'vendor': vendor_tuple[0], 'os.name': vendor_tuple[1], 'os.version': device_os_version}
    device_data = DeviceData(device_ip, vendor_dict, [SshCredentials(device_user, device_pass),
                                                      HttpsCredentials(device_user, device_pass)])
    issue_items = arg_dict.get('issue_items', set())
    result = WorkflowTestTool.run_workflow(workflow_path, device_data, issue_items=issue_items)
    if type(result) == WorkflowFailedResponse:
        result = cast(WorkflowFailedResponse, result)
        print('EXCEPTION:')
        print(result.exception)
    elif type(result) == WorkflowSuccessResponse:
        result = cast(WorkflowSuccessResponse, result)
        print('Conclusion reached:')
        print(result.conclusion.conclusion_title)
    elif type(result) == WorkflowIssueItemsConclusionResponse:
        print('ISSUE ITEM RESPONSES NOT SUPPORTED YET')
    else:
        print('Unknown response type, did indeni-workflow update unexpectedly?')


def run_workflow_block_on_device(arguments):
    def get_vendor_tuple(_wf_path):
        _full_path = os.path.abspath(_wf_path)
        if 'checkpoint' in _full_path:
            return 'checkpoint', 'gaia'
        elif 'paloaltonetworks' in _full_path:
            return 'panw', 'panos'
        else:
            print(f'No vendor name found along path: {_full_path}')
            sys.exit(1)

    from indeni_workflow.remote.device_credentials import SshCredentials, HttpsCredentials
    from indeni_workflow.workflow_response import WorkflowIssueItemsConclusionResponse
    from indeni_workflow.workflow_test_tool import WorkflowTestTool
    from indeni_workflow.workflow import DeviceData, WorkflowSuccessResponse, WorkflowFailedResponse, \
        WorkflowBlocksResponse
    arg_dict = vars(arguments) if type(arguments) is not dict else arguments
    workflow_path = arg_dict['workflow']
    device_ip = arg_dict['device_ip']
    device_os_version = arg_dict['device_os_version']
    device_user = arg_dict.get('device_user', 'indeni')
    device_pass = arg_dict.get('device_pass', 'indeni123')
    vendor_tuple = get_vendor_tuple(workflow_path)
    vendor_dict = {'vendor': vendor_tuple[0], 'os.name': vendor_tuple[1], 'os.version': device_os_version}
    device_data = DeviceData(device_ip, vendor_dict, [SshCredentials(device_user, device_pass),
                                                      HttpsCredentials(device_user, device_pass)])
    scope = arg_dict.get('scope', dict())
    block_id = arg_dict['block']
    issue_items = arg_dict.get('issue_items', set())
    result = WorkflowTestTool.run_workflow_block(workflow_path, block_id, device_data, scope,
                                                 run_single_block=True, issue_items=issue_items)
    if type(result) == WorkflowFailedResponse:
        result = cast(WorkflowFailedResponse, result)
        print('EXCEPTION:')
        print(result.exception)
    elif type(result) == WorkflowSuccessResponse:
        result = cast(WorkflowSuccessResponse, result)
        print('Conclusion reached:')
        print(result.conclusion.conclusion_title)
    elif type(result) == WorkflowIssueItemsConclusionResponse:
        print('ISSUE ITEM RESPONSES NOT SUPPORTED YET')
    elif type(result) == WorkflowBlocksResponse:
        result = cast(WorkflowBlocksResponse, result)
        print('Block Details')
        for block in result.block_records:
            print(block)
    else:
        print('Unknown response type, did indeni-workflow update unexpectedly?')


def run_workflow_on_mock_data(yaml_vars):
    from indeni_workflow.remote.device_credentials import MockCredentials, MockInput
    from indeni_workflow.workflow_response import WorkflowIssueItemsConclusionResponse
    from indeni_workflow.workflow_test_tool import WorkflowTestTool
    from indeni_workflow.workflow import DeviceData, WorkflowSuccessResponse, WorkflowFailedResponse
    arg_dict = vars(yaml_vars) if type(yaml_vars) is not dict else yaml_vars
    mock_inputs = [MockInput(*pair) for pair in arg_dict['pairs'].items()]
    mock_credentials = MockCredentials(mock_inputs)
    mock_device_data = DeviceData('', arg_dict.get('tags', dict()), [mock_credentials])
    issue_items = arg_dict.get('issue_items', set())
    workflow_path = arg_dict['workflow']
    result = WorkflowTestTool.run_workflow(workflow_path, mock_device_data, issue_items)
    if type(result) == WorkflowFailedResponse:
        result = cast(WorkflowFailedResponse, result)
        print('EXCEPTION:')
        print(result.exception)
    elif type(result) == WorkflowSuccessResponse:
        result = cast(WorkflowSuccessResponse, result)
        print('Conclusion reached:')
        print(result.conclusion.conclusion_title)
    elif type(result) == WorkflowIssueItemsConclusionResponse:
        print('ISSUE ITEM RESPONSES NOT SUPPORTED YET')
    else:
        print('Unknown response type, did indeni-workflow update unexpectedly?')


def run_workflow_block_on_mock_data(yaml_vars):
    from indeni_workflow.remote.device_credentials import MockCredentials, MockInput
    from indeni_workflow.workflow_response import WorkflowIssueItemsConclusionResponse
    from indeni_workflow.workflow_test_tool import WorkflowTestTool
    from indeni_workflow.workflow import DeviceData, WorkflowSuccessResponse, WorkflowFailedResponse, \
        WorkflowBlocksResponse
    arg_dict = vars(yaml_vars) if type(yaml_vars) is not dict else yaml_vars
    mock_inputs = [MockInput(*pair) for pair in arg_dict['pairs'].items()]
    mock_credentials = MockCredentials(mock_inputs)
    block_id = arg_dict['block']
    scope = arg_dict.get('scope', dict())
    mock_device_data = DeviceData('', arg_dict.get('tags', dict()), [mock_credentials])
    issue_items = arg_dict.get('issue_items', set())
    workflow_path = arg_dict['workflow']
    result = WorkflowTestTool.run_workflow_block(workflow_path, block_id, mock_device_data, scope,
                                                 run_single_block=True, issue_items=issue_items)
    if type(result) == WorkflowFailedResponse:
        result = cast(WorkflowFailedResponse, result)
        print('EXCEPTION:')
        print(result.exception)
    elif type(result) == WorkflowSuccessResponse:
        result = cast(WorkflowSuccessResponse, result)
        print('Conclusion reached:')
        print(result.conclusion.conclusion_title)
    elif type(result) == WorkflowIssueItemsConclusionResponse:
        print('ISSUE ITEM RESPONSES NOT SUPPORTED YET')
    elif type(result) == WorkflowBlocksResponse:
        result = cast(WorkflowBlocksResponse, result)
        print('Block Details')
        for block in result.block_records:
            print(block)
    else:
        print('Unknown response type, did indeni-workflow update unexpectedly?')


def print_log_file(log_file_name: str):
    log_file_name_to_path_dict = {
        'indeni.log': '/usr/share/indeni/logs/indeni.log',
        'collector.log': '/usr/share/indeni-collector/logs/collector.log',
        'backup.log': '/usr/share/indeni-services/backup/logs/backup-service.log'
    }
    log_file_path = log_file_name_to_path_dict[log_file_name]
    with open(log_file_path) as f:
        print(f.read(), end='')


def print_device_log_file(device_file_id):
    device_ip = make_device_ip(DB_CREDENTIALS, device_file_id)
    log_path = f'/usr/share/indeni-collector/logs/devices/{device_ip}.log'
    if not os.path.isfile(log_path):
        print(f'Log file not found for given device ID at {log_path}')
        sys.exit(1)
    with open(log_path) as f:
        print(f.read(), end='')


def parse_config_file(file_path: str) -> Dict[str, str]:
    import yaml
    return yaml.safe_load(open(file_path))


def create_cli_parser():
    class QuietArgumentParser(argparse.ArgumentParser):
        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)

        def error(self, message):
            print(custom_usage())
            print(message)
            self.exit(2)
    parser = QuietArgumentParser(description='Indeni Knowledge Tool', usage=custom_usage())
    parser.add_argument('--version', action='version', version='Version:  2.1.0')
    subparsers = parser.add_subparsers(dest='command')

    # OP actions
    parser_op = subparsers.add_parser('op')
    subparsers_db = parser_op.add_subparsers(dest='sub_command')

    # noinspection PyUnusedLocal
    parser_db_devices_show_all = subparsers_db.add_parser('devices_show_all')

    parser_db_device_info_show = subparsers_db.add_parser('device_info_show')
    parser_db_device_info_show.add_argument('device_id')

    parser_db_alert_delete_all = subparsers_db.add_parser('alert_delete_all')
    parser_db_alert_delete_all.add_argument('device_id')

    parser_db_alert_delete_single = subparsers_db.add_parser('alert_delete_single')
    parser_db_alert_delete_single.add_argument('alert_id')

    parser_db_metric_get = subparsers_db.add_parser('metric_get')
    parser_db_metric_get.add_argument('metric_name')
    parser_db_metric_get.add_argument('device_id')

    parser_db_metric_show_all = subparsers_db.add_parser('metric_show_all')
    parser_db_metric_show_all.add_argument('device_id')

    parser_sh_overwrite_file_create = subparsers_db.add_parser('overwrite_file_create')
    parser_sh_overwrite_file_create.add_argument('path_to_stable_ind')

    # noinspection PyUnusedLocal
    parser_sh_overwrite_files_show = subparsers_db.add_parser('overwrite_files_show')

    parser_sh_overwrite_file_delete = subparsers_db.add_parser('overwrite_file_delete')
    parser_sh_overwrite_file_delete.add_argument('specific_ind_file')  # Can also be "all" to delete all files

    # noinspection PyUnusedLocal
    parser_sh_knowledge_version_show = subparsers_db.add_parser('knowledge_version_show')

    parser_sh_catalog_search = subparsers_db.add_parser('catalog')
    parser_sh_catalog_search.add_argument('vendor')
    parser_sh_catalog_search.add_argument('metric')

    # Before/After upgrade actions
    parser_upgrade_test = subparsers.add_parser('snapshot')
    subparsers_upgrade = parser_upgrade_test.add_subparsers(dest='sub_command')

    # parser_upgrade_create = subparsers_upgrade.add_parser('create_json')
    # parser_upgrade_create.add_argument('target_file_name')
    parser_upgrade_create = subparsers_upgrade.add_parser('create')
    parser_upgrade_create_json = parser_upgrade_create.add_subparsers(dest='file_type')
    parser_upgrade_create_json_parser = parser_upgrade_create_json.add_parser('json')
    parser_upgrade_create_json_parser.add_argument('scheme', choices=['ind_per_device'])
    parser_upgrade_create_json_parser.add_argument('target_file_name')
    parser_upgrade_create_json_parser.add_argument('device_list')

    # noinspection PyUnusedLocal
    # parser_upgrade_restart = subparsers_upgrade.add_parser('server_restart')

    # parser_upgrade_compare = subparsers_upgrade.add_parser('compare_json')
    parser_upgrade_compare = subparsers_upgrade.add_parser('compare')
    parser_upgrade_compare_json = parser_upgrade_compare.add_subparsers(dest='file_type')
    parser_upgrade_compare_json_parser = parser_upgrade_compare_json.add_parser('json')
    parser_upgrade_compare_json_parser.add_argument('old_json_file')
    parser_upgrade_compare_json_parser.add_argument('new_json_file')

    # Workflow actions
    parser_wf = subparsers.add_parser('workflow')
    subparsers_wf = parser_wf.add_subparsers(dest='sub_command')

    parser_wf_real_device_workflow = subparsers_wf.add_parser('real_device_workflow')
    parser_wf_real_device_workflow.add_argument('config_file')

    parser_wf_real_device_block = subparsers_wf.add_parser('real_device_block')
    parser_wf_real_device_block.add_argument('config_file')

    parser_wf_mock_data_workflow = subparsers_wf.add_parser('mock_data_workflow')
    parser_wf_mock_data_workflow.add_argument('mock_data_file')

    parser_wf_mock_data_block = subparsers_wf.add_parser('mock_data_block')
    parser_wf_mock_data_block.add_argument('mock_data_file')

    # Test actions
    # noinspection PyUnusedLocal
    parser_debug = subparsers.add_parser('debug')

    parser_logs = subparsers.add_parser('logs')
    parser_logs = parser_logs.add_subparsers(dest='sub_command')
    parser_logs_devices = parser_logs.add_parser('devices')
    parser_logs_devices.add_argument('action', choices=['backup'])

    parser_logs_show = parser_logs.add_parser('show')
    parser_logs_show.add_argument('action', choices=['device', 'collector.log', 'indeni.log', 'backup.log'])

    return parser


def custom_usage():
    return """Usage: iktool [-h] <options> [parameters]

op
    devices_show_all
    device_info_show            <device_id>
    alert_delete_all            <device_id>
    alert_delete_single         <alert_id>
    metric_get                  <metric_name> <device_id>
    metric_show_all             <device_id>
    overwrite_file_create       <path_to_stable_ind_file>
    overwrite_files_show
    overwrite_file_delete       <all|<specific_ind_file>
    knowledge_version_show
    catalog                     <vendor> <metric>

workflow
    real_device_workflow        <config_file>
    real_device_block           <config_file>
    mock_data_workflow          <config_file>
    mock_data_block             <config_file>

snapshot
    create
          json
              ind_per_device    <target_file_name> <devices.txt>
    compare
          json                  <old_json_file> <new_json_file>

logs
    devices
          backup
    show
          device                <device_id>
    show                        <collector.log|indeni.log|backup.log>

--help, -h
--version
"""


def main():
    import argcomplete
    parser = create_cli_parser()
    argcomplete.autocomplete(parser)
    # noinspection PyBroadException
    try:
        args, unknown_args = parser.parse_known_args()
        if args.command == 'op':
            if args.sub_command == 'devices_show_all':
                show_all_devices(DB_CREDENTIALS)
            elif args.sub_command == 'device_info_show':
                show_device_info(DB_CREDENTIALS, args.device_id)
            elif args.sub_command == 'alert_delete_all':
                delete_all_alerts(DB_CREDENTIALS, args.device_id)
            elif args.sub_command == 'alert_delete_single':
                delete_alert(DB_CREDENTIALS, args.alert_id)
            elif args.sub_command == 'metric_get':
                get_metric(args.metric_name, args.device_id)
            elif args.sub_command == 'overwrite_file_create':
                overwrite_file(args.path_to_stable_ind)
            elif args.sub_command == 'overwrite_files_show':
                view_all_overwrite_files()
            elif args.sub_command == 'overwrite_file_delete':
                delete_all_overwrite_files(args.specific_ind_file)
            elif args.sub_command == 'knowledge_version_show':
                show_knowledge_version()
            elif args.sub_command == 'catalog':
                fetch_details_from_vendor_and_metric(args.vendor, args.metric)
            elif args.sub_command == 'metric_show_all':
                print_metrics_from_device(args.device_id)
                pass
            else:
                print(custom_usage())
        elif args.command == 'snapshot':
            if args.sub_command == 'create' and args.file_type == 'json':
                write_device_to_metric_dict_to_file(args.target_file_name, args.device_list)
            elif args.sub_command == 'compare' and args.file_type == 'json':
                diff_two_json_files(args.old_json_file, args.new_json_file)
            # elif args.sub_command == 'server_restart':
            #     backup_log_files_and_restart_indeni_services()
            else:
                print(custom_usage())
        elif args.command == 'workflow':
            if args.sub_command == 'real_device_workflow':
                run_workflow_on_device(parse_config_file(args.config_file))
            elif args.sub_command == 'real_device_block':
                run_workflow_block_on_device(parse_config_file(args.config_file))
            elif args.sub_command == 'mock_data_workflow':
                run_workflow_on_mock_data(parse_config_file(args.mock_data_file))
            elif args.sub_command == 'mock_data_block':
                run_workflow_block_on_mock_data(parse_config_file(args.mock_data_file))
            else:
                print(custom_usage())
        elif args.command == 'logs':
            if args.sub_command == 'devices' and args.action == 'backup':
                backup_log_files()
            elif args.sub_command == 'show' and args.action == 'device':
                if len(unknown_args) == 0:
                    raise Exception('device_id is a required parameter for "iktool show device"')
                print_device_log_file(unknown_args[0])
            else:
                print_log_file(args.action)
        else:
            print(custom_usage())
    # noinspection PyBroadException
    except Exception as e:
        print(custom_usage())
        print('ERROR:', e)
        raise e


if __name__ == '__main__':
    main()
