#!/bin/bash -eu
[ "x${DEBUG:-}" = "x" ] || set -x
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
__script="${__dir}/$(basename "${BASH_SOURCE[0]}")"
__start_dir=`pwd`

## if the running script is symlink - find a target script location
if [[ -L "${__script}" ]]; then
	__dir="$(dirname "$(readlink "${__script}")")"
fi

cd ${__dir}
# echo ":: Create/Enable virtual environment ::"
virtualenv env 2>&1 >/dev/null
source ${__dir}/env/bin/activate 2>&1 >/dev/null
# echo "Params: $@"

# echo ":: Install requirements ::"
pip3 install -r requirements.txt 2>&1 >/dev/null

# echo ":: Start iktool.py ::"
python3 ${__dir}/iktool.py $@
deactivate

