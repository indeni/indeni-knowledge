#!/usr/bin/python2.7

# Goes over a file, looking for lines beginning with any of writeComplexMetricObjectArray, writeComplexMetricObjectArrayWithLiveConfig, writeComplexMetricString,
# writeComplexMetricStringWithLiveConfig, writeDoubleMetric, writeDoubleMetricWithLiveConfig.
# When found, it tokenizes the line.
# It then makes sure that the command ends on the same line. If not, a warning is issued.
# If it does, then it sees how many parameters it has, and according to the number of parameters, issues a specific correction

import argparse
import logging
import re
from collections import namedtuple, deque
from enum import Enum
from re import Scanner

_SAFEGUARD_COMMENT = '# Converted to new syntax by change_ind_scripts.py script'

LineDescriptor = namedtuple('LineDescriptor', ['before_function', 'function_name', 'arguments', 'after_function'])

class Change:
    def perform_change(self, line_descriptor):
        raise NotImplementedError

class ArgumentChangers(Change):
    def perform_change(self, line_descriptor):
        return LineDescriptor(line_descriptor.before_function, line_descriptor.function_name, self._change_arguments(line_descriptor.arguments), line_descriptor.after_function)
    def _change_arguments(self, arguments):
        raise NotImplementedError

class AddArgument(ArgumentChangers):

    def __init__(self, arg_content, arg_index):
        self.arg_content = arg_content
        self.arg_index = arg_index

    def _change_arguments(self, arguments):
        if self.arg_index < 0 or self.arg_index > len(arguments):
            raise Exception('Invalid change request: ' + str(self))
        return arguments[:self.arg_index] + [self.arg_content] + arguments[self.arg_index:]

    def __str__(self):
        return 'AddArgument %s at index %d' % (self.arg_content, self.arg_index)


class RemoveArgument(ArgumentChangers):
    def __init__(self, arg_index):
        self.arg_index = arg_index

    def _change_arguments(self, arguments):
        if self.arg_index < 0 or self.arg_index >= len(arguments):
            raise Exception('Invalid change request: ' + str(self))
        return arguments[:self.arg_index] + arguments[self.arg_index+1:]

    def __str__(self):
        return 'RemoveArgument at index %d' % self.arg_index


class ChangeFunctionName(Change):
    def __init__(self, new_function_name):
        self.new_function_name = new_function_name

    def perform_change(self, line_descriptor):
        return LineDescriptor(line_descriptor.before_function, self.new_function_name, line_descriptor.arguments, line_descriptor.after_function)

    def __str__(self):
        return 'Change function name to %s' % self.new_function_name


class ParseException(Exception):
    def __init__(self, message):
        super().__init__()
        self.message = message


_CHANGES = {  # funcName -> numberOfArgs -> list of changes
    'writeComplexMetricObjectArray': {
        3: [AddArgument('"false"', 3)]
    },
    'writeComplexMetricObjectArrayWithLiveConfig': {
        4: [
            ChangeFunctionName('writeComplexMetricObjectArray'),
            AddArgument('"true"', 3)
        ]
    },
    'writeComplexMetricString': {
        3: [AddArgument('"false"', 3)]
    },
    'writeComplexMetricStringWithLiveConfig': {
        4: [
            ChangeFunctionName('writeComplexMetricString'),
            AddArgument('"true"', 3)
        ]
    },
    'writeDoubleMetric': {
        5: [
            RemoveArgument(3),
            AddArgument('"false"', 4)
        ]
    },
    'writeDoubleMetricWithLiveConfig': {
        6: [
            ChangeFunctionName('writeDoubleMetric'),
            RemoveArgument(3),
            AddArgument('"true"', 4)
        ],
        7: [
            ChangeFunctionName('writeDoubleMetric'),
            RemoveArgument(3),
            AddArgument('"true"', 4)
        ],
        8: [
            ChangeFunctionName('writeDoubleMetric'),
            RemoveArgument(3),
            AddArgument('"true"', 4)
        ]
    }
}

_FUNC_NAMES = sorted(set(_CHANGES.keys()), key=lambda s: len(s), reverse=True)
_FUNC_RE = '|'.join(_FUNC_NAMES)
_MATCH_PAREN = {
    '(': ')',
    '[': ']',
    '{': '}'
}

class TokenType(Enum):
    OPEN_PAREN = 1
    CLOSE_PAREN = 2
    QUOTE = 3
    COMMA = 4
    GENERIC_TEXT = 5

def parse_line(line):
    """Parses the line.

    If the line is irrelevant, returns None
    If the line is relevant, and parsing was successful, returns a LineDescriptor.
    If the line is relevant, and aprsing was unsuccessful, raises a ParseException"""
    if _SAFEGUARD_COMMENT in line:  # Not changing lines that were already changed by this script.
        return None
    funcs_in_line = list(re.finditer(_FUNC_RE, line))
    if len(funcs_in_line) == 0:
        return None
    if len(funcs_in_line) > 1:
        raise ParseException('More than one function found in line')
    func_in_line = funcs_in_line[0]
    func_name = func_in_line.group(0)
    func_start = func_in_line.span()[0]
    func_end = func_in_line.span()[1]
    before_function = line[:func_start]
    after_function_name = line[func_end:].rstrip()  # Remove whitespace between function name and open paren
    if not after_function_name.startswith('('):
        raise ParseException('Expected "(" but got: """%s"""' % after_function_name)
    scanner = Scanner([
        ('\(|\[|{', lambda _, token: (TokenType.OPEN_PAREN, token)),
        ('\)|\]|}', lambda _, token: (TokenType.CLOSE_PAREN, token)),
        ('\'|"', lambda _, token: (TokenType.QUOTE, token)),
        (',', lambda _, token: (TokenType.COMMA, token)),
        (r'[^(){}\[\]\'",]+', lambda _, token: (TokenType.GENERIC_TEXT, token)),
    ])
    tokens, unparsed = scanner.scan(after_function_name)
    if unparsed:
        raise ParseException('Cannot parse part of line: """%s"""' % unparsed)
    stack = deque()
    arguments = []
    cur_argument = ''
    in_quote = None  # Either None, or ' or "
    index = 0
    for token_type, token in tokens:
        if in_quote:
            cur_argument += token
            if token_type == TokenType.QUOTE and in_quote == token:
                in_quote = False
        else:
            if token_type == TokenType.OPEN_PAREN:
                stack.append(token)
                if len(stack) > 1:
                    cur_argument += token
            elif token_type == TokenType.CLOSE_PAREN:
                if len(stack) == 0:
                    raise ParseException('Unmatched ' + token)
                latest_paren = stack.pop()
                if _MATCH_PAREN[latest_paren] != token:
                    raise ParseException('Mismatch between %s and %s' % (latest_paren, token))
                if len(stack) == 0:
                    break # Rest of tokens don't matter, continue parsing outside for loop
                else:
                    cur_argument += token
            elif token_type == TokenType.QUOTE:
                in_quote = token
                cur_argument += token
            elif token_type == TokenType.COMMA:
                if len(stack) == 1:
                    arguments.append(cur_argument.strip())
                    cur_argument = ''
                else:
                    cur_argument += token
            elif token_type == TokenType.GENERIC_TEXT:
                cur_argument += token
            else:
                raise ParseException('Unexpected token %s of type %s ' % (token, token_type))
        index += len(token)
    else:  # Means break command was not used, so we've gone through all the line's tokens and haven't finished parsing the functino
        raise ParseException('Function call not finished within line')
    arguments.append(cur_argument.strip())
    after_function = after_function_name[index+1:]
    return LineDescriptor(before_function, func_name, arguments, after_function)

def rebuild_line(line_descriptor):
    arguments = ', '.join(line_descriptor.arguments)
    return '%s%s(%s)%s  %s\n' % (line_descriptor.before_function, line_descriptor.function_name, arguments, line_descriptor.after_function, _SAFEGUARD_COMMENT)

def process_line(line):
    """Processes a line of text and returns the line to be written in the output file.

    Note that this function should preserve newline structure and anything else that is not in the scope of the
    changes it aims to make.
    If the line does not contain one of the function_names we're interested in, it is returned as is.
    If it does contain the function_name but either a. the parentheses or quotes aren't balanced, or b. the number of
    arguments doesn't fit the dictionary of changes to make, or c. it contains more than one function name, then it is
    returned as is but a warning is issued to the log.
    Otherwise, manipulations are performed on the line as needed and the resulting line is returned
    """
    logging.info('Parsing line: ' + line.rstrip())
    try:
        line_descriptor = parse_line(line)
    except ParseException as e:
        logging.warn(e.message + ': ' + line)
        return line
    if line_descriptor is None:
        return line
    logging.info('Line descriptor: %s', line_descriptor)
    changes_for_func = _CHANGES[line_descriptor.function_name]
    num_arguments = len(line_descriptor.arguments)
    if num_arguments not in changes_for_func:
        logging.warn('Number of arguments %d not expected for function %s: %s', num_arguments, line_descriptor.function_name, line)
        return line
    for change in changes_for_func[num_arguments]:
        logging.info('Performing change: %s', change)
        line_descriptor = change.perform_change(line_descriptor)
        logging.info('Line descriptor is now: %s', line_descriptor)
    resulting_line = rebuild_line(line_descriptor)
    logging.info('Built result line: ' + resulting_line)
    return resulting_line


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--input_file', required=True, help='Input file to read from')
    parser.add_argument('--output_file', required=True, help='Output file to write to')
    parser.add_argument('--debug', required=False, help='Use to get verbose output', action='store_true')
    args = parser.parse_args()
    logging.basicConfig(level=(logging.DEBUG if args.debug else logging.WARN))
    with open(args.input_file, 'r') as input_file:
        with open(args.output_file, 'w') as output_file:
            for line in input_file:
                output_file.write(process_line(line))


if __name__ == '__main__':
    main()
