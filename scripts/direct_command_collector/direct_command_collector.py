# Script Background:
# The idea behind the script is to be able to evaluate an environment towards the Indeni server.
# Once this script is collected, we can utilize the outputs to evaluate the scale of the environment, and how Indeni server will behave with such scale and amount and size of outputs.

# Instructions:
# 1. Customer will need to create 2 folders under: /home/admin/:
#   # mkdir direct_command_collector
#   # mkdir direct_command_collector/data
# 2. Move the script to the folder direct_command_collector.
# 3. Run the following, to prepare the script:
# 	# dos2unix direct_command_collector.py
# 	# chmod +x direct_command_collector.py
# 4. Run the script, with the syntax:
# 	# ./direct_command_collector.py
# 5. Once the script finished, compress the folder:
# 	# tar -zcvf /home/admin/direct_command_collector.tar.gz /home/admin/direct_command_collector
# 6. Provide us with the zipped file.

#!/usr/bin/python
import subprocess
import datetime

commands_dict = {
    # This is the dictionary which include the ind file name (key) + the command needs to be run (value).
    # The format is: 'ind file name': '''command'''
    # Examples:
    # 'vpn_ipafile_check_vsx': '''/bin/nice -n 15 fw vsx stat -l | awk '/^VSID:/{ vsid=$NF } /^Type:/{type=$0} /^Name:/{ if (type ~ "Virtual System" || type ~ "VSX Gateway") {print vsid } }' | while read id; do vsenv $id && /bin/nice -n 15 fw vsx stat $id && (/bin/nice -n 15 vpn ipafile_check <(/bin/nice -n 15 cat $FWDIR/conf/ipassignment.conf | /bin/nice -n 15 sed 's/^Gateway        Type.*//g' | /bin/nice -n 15 sed 's/^=======.*//g')); done''',
    # 'interface-fake-tx-hang': '''awk '$0>=from' from="$(date +%b" "%e" "%H:%M:%S -d -5min)" /var/log/messages* | grep "Fake Tx hang"'''
}

path = '/home/admin/direct_command_collector/'

output_log = open(path + 'log.txt', 'wa')
i = 1
total_time_start = datetime.datetime.now()
for key in commands_dict.keys():
    err_msg = None
    time_start = datetime.datetime.now()
    try:
        cmd_out = subprocess.check_output(commands_dict[key], shell=True, stderr=subprocess.STDOUT)
    except subprocess.CalledProcessError as e:
        err_msg = 'Warning: {}'.format(e)
        cmd_out = e.output
    finally:
        time_end = datetime.datetime.now()
        diff = time_end - time_start
        output_log.write('{}) Timestamp: {}. Executing IND: {}. Duration: {} {}\n'.format(i, time_end, key, diff, err_msg))
        output_file = open(path + 'data/%s.txt' % key, 'wa')
        output_file.write(cmd_out)
        output_file.close()
        i += 1

total_time_end = datetime.datetime.now()
summary = '''\
Scripts executed: {} times
Total runtime: {}\
'''.format((i-1), total_time_end - total_time_start)
output_log.write(summary)
output_log.close()
print(summary)
