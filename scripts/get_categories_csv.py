#!/usr/bin/python3

# Goes over all yaml template-based rules under given directory, and produces a csv at the given path

import argparse
import logging
import os
import yaml
import csv

from yaml.parser import ParserError


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--rules_dir', required=True, help='Directory to scan for yaml rules')
    parser.add_argument('--output_file', required=True, help='Name/path of file to write output csv to')
    args = parser.parse_args()
    logging.basicConfig(level=logging.INFO)
    yaml_files = [os.path.join(root, f) for root, dirs, files in os.walk(args.rules_dir) for f in files if f.endswith('.yaml')]
    with open(args.output_file, 'w') as csv_file:
        csv_writer = csv.writer(csv_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        csv_writer.writerow(['name or file name', 'friendly_name', 'categories'])
        for yaml_file_path in yaml_files:
            with open(yaml_file_path, 'r') as f:
                try:
                    yaml_dict = yaml.safe_load(f)
                except ParserError:
                    logging.warn('Ignoring invalid yaml file: %s', yaml_file_path)
                    continue
            if yaml_dict.get('rule_type') != 'template-based':
                logging.warn('Ignoring yaml file that is not a template-based rule: %s', yaml_file_path)
                continue
            categories = yaml_dict.get('rule_categories', [])
            name = yaml_dict.get('rule_name', '')
            if not name:
                name = os.path.basename(yaml_file_path)
            friendly_name = yaml_dict.get('rule_friendly_name', '')
            csv_writer.writerow([name, friendly_name, '#'.join(categories)])



if __name__ == '__main__':
    main()
