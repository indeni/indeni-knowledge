
SEVERITY = {#Map CVE severity to BCIA/Indeni alerts/rules severity
    'LOW' : 'INFO',
    'MEDIUM' : 'INFO',
    'HIGH' : 'WARN',
    'CRITICAL' : 'WARN'
}

CVSS_SUPPORTED_VERSIONS = ['cvssV3_1','cvssV4_0']

VENDORS = {
    '897c38be-0345-43cd-b6cf-fe179e0c4f45': {
        'orgId': '897c38be-0345-43cd-b6cf-fe179e0c4f45',
        'ind_name': 'checkpoint',
        'device_category': 'CheckPointDevices',
        'products' : {
            'Check Point Gaia Portal': {
                'os_name' : 'Gaia',
                },
            'Check Point IPsec VPN': {
                'os_name' : 'Gaia',
                },
            'Check Point Security': {
                'os_name' : 'Gaia',
                },
            'Gateway & Management, IPsec VPN blade SNX portal.': {
                'os_name' : 'Gaia',
                },
    }
    },
    '9dacffd4-cb11-413f-8451-fbbfd4ddc0ab': {
        'orgId': '9dacffd4-cb11-413f-8451-fbbfd4ddc0ab',
        'ind_name': 'f5',
        'device_category': 'F5Devices', #Pending to create category
        'products': {
            'BIG-IP':{
                'os_name' : 'BIG-IP',
                },
#            'BIG-IQ':{
#                'os_name' : 'BIG-IQ',
#                },
#            'F5OS':{
#                'os_name' : 'BIG-Next', #Pending to validate
#                },
        }
    },
    'd6c1279f-00f6-4ef7-9217-f89ffe703ec0': {
        'orgId': 'd6c1279f-00f6-4ef7-9217-f89ffe703ec0',
        'ind_name': 'panos',
        'device_category': 'PaloAltoNetworks',
        'products': {
            'PAN-OS':{
                'os_name' : 'panos',
                },
#            'Cloud NGFW':{
#                'os_name' : 'panos-cloud',
#                },
            }
    },
    '6abe59d8-c742-4dff-8ce8-9b0ca1073da8': {
        'orgId': '6abe59d8-c742-4dff-8ce8-9b0ca1073da8',
        'ind_name': 'fortinet',
        'device_category': 'FortinetDevices',
        'products': {
            'FortiOS': {
                'os_name' : 'FortiOS',
                },
        }
    },
    'd1c1063e-7a18-46af-9102-31f8928bc633': {
        'orgId': 'd1c1063e-7a18-46af-9102-31f8928bc633',
        'ind_name': 'cisco',
        'device_category': 'CiscoDevices',
        'products': {
            'Cisco Adaptive Security Appliance (ASA) Software': {
                'os_name' : 'asa',
                },
        }
    },
    '80d3bcb6-88de-48c2-a47e-aebf795f19b5': {
        'orgId': '80d3bcb6-88de-48c2-a47e-aebf795f19b5',
        'ind_name': 'symantec',
        'device_category': 'SymantecDevices',
        'products': {
            'Advanced Secure Gateway, Content Analysis': {
                'os_name' : 'cas',
                },
        }
    }
}

ALL_PRODUCTS = [product for vendor in VENDORS.keys() for product in VENDORS[vendor]['products']]

TEMPLATE_MANDATORY_FIELDS = ['rule_name', 
                             'rule_friendly_name', 
                             'alert_description', 
                             'version_ranges', 
                             'base_remediation_text', 
                             'vendor_severity_rating']

