import yaml
from yaml.loader import SafeLoader
import os

class IndentDumper(yaml.Dumper): #to generate indentation on yaml
    def increase_indent(self, flow=False, indentless=False):
        return super(IndentDumper, self).increase_indent(flow, False)

class YamlHelpers:
    def load_relative_path (relative_path:str):
        script_dir = os.path.dirname(__file__) #<-- absolute dir the script is in
        with open(os.path.join(script_dir, relative_path)) as f:
            data = yaml.load(f, Loader=SafeLoader)
            return(data)

    def write_relative_path (relative_path:str, raw_data):
        script_dir = os.path.dirname(__file__) #<-- absolute dir the script is in
        with open(os.path.join(script_dir, relative_path), 'w') as f:
            yaml.dump(raw_data, f, width=65536, Dumper=IndentDumper, sort_keys=False)


class TextTransform:
    def oneline_to_multiline(text:str):
        list_of_lines = text.split('\n')
        formatted_text = f''''''
        for line in list_of_lines:
            formatted_text = formatted_text + line + '\n'
        return formatted_text

