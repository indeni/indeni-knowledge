#!/usr/bin/env python3
# PYTHON_ARGCOMPLETE_OK

import argparse
import os
import git  # pip install gitpython
import glob
import json
import helpers
import constants
import requests
from git.remote import RemoteProgress

current_dir = os.path.dirname(os.path.realpath(__file__))
tuple_all_versions =('*','x','x*','*x')


class Progress(RemoteProgress):
    """
    Show git clone progress in terminal
    """
    def update(self, *args):
        print(self._cur_line)

def git_repo_clone():
    git.Repo.clone_from("https://github.com/CVEProject/cvelistV5.git", 
                        f"{current_dir}/cvelistV5", 
                        progress=Progress())
def git_repo_update(): 
    if os.path.isdir(f"{current_dir}/cvelistV5"):      # if repo exists, pull newest data 
        repo = git.Repo(f"{current_dir}/cvelistV5") 
        repo.remotes.origin.pull(progress=Progress())

def rules_folder_skeleton(cve_vendor):
    # Creating rules folder structure if not present
    if not os.path.exists(f"{current_dir}/../../rules/templatebased/cve"):
        os.mkdir(f"{current_dir}/../../rules/templatebased/cve")
    for vendor in constants.VENDORS.keys():
        if cve_vendor == 'all' :
            cve_vendor = [['ind_name'] for vendor_key in  constants.VENDORS[vendor]] 
        if constants.VENDORS[vendor]['ind_name'] in cve_vendor:
            if not os.path.exists(f"{current_dir}/../../rules/templatebased/cve/{constants.VENDORS[vendor]['ind_name']}"):
                os.mkdir(f"{current_dir}/../../rules/templatebased/cve/{constants.VENDORS[vendor]['ind_name']}")
                for severity in constants.SEVERITY.keys():
                    if not os.path.exists(f"{current_dir}/../../rules/templatebased/cve/{constants.VENDORS[vendor]['ind_name']}/{severity.lower()}"):
                        os.mkdir(f"{current_dir}/../../rules/templatebased/cve/{constants.VENDORS[vendor]['ind_name']}/{severity.lower()}")

def download_nist_cve_data(cve_id: str):
    """
    Donwload NIST CVE data for a given CVE-ID
    """
    try:
        data_url = f"https://services.nvd.nist.gov/rest/json/cves/2.0?cveId={str(cve_id)}"
        # Download NIST data for a given CVE-ID
        content = requests.get(data_url)
        # If download was not successful raise exception
        if content.status_code != 200:
            return (None, f"Error requesting CVE-ID {str(cve_id)} NIST JSON.")
        try:
            return (content.json(), None)
        except json.decoder.JSONDecodeError as err:
            return (None, f"Error decoding CVE-ID {str(cve_id)} NIST JSON: {err}")
    except Exception as err:
        return (None, f"Exception in download CVE-ID {str(cve_id)} data from NIST: {err}")



def generate_rules(cve_id=None, cve_year=None, cve_vendor=None):
    if cve_id:
        filename_regex = f'{cve_id}.json'
    elif cve_year:
        filename_regex = f'CVE-{cve_year}*.json'
    else:
        filename_regex = f'CVE-*.json'
    if filename_regex:
        files = glob.glob(f"{current_dir}/cvelistV5/cves/**/{filename_regex}", recursive=True)

        for file in files:
            with open(file, "r") as stream:
                try:
                    json_file = json.load(stream)
                    if json_file['dataType'] == 'CVE_RECORD' and json_file['dataVersion'] >= '5.0':
                        if json_file['cveMetadata'].get('assignerShortName') and json_file['cveMetadata']['assignerOrgId'] in constants.VENDORS.keys():
                            if constants.VENDORS[json_file['cveMetadata']['assignerOrgId']]['ind_name'] in cve_vendor:
                                if json_file['containers']['cna'].get('affected') is not None:
                                    for product in json_file['containers']['cna']['affected']:
                                        if product['product'] in constants.VENDORS[json_file['cveMetadata']['assignerOrgId']]['products'].keys():
                                            new_rule = {}
                                            path = ''
                                            new_rule['rule_type'] = 'template-based'
                                            new_rule['template_name'] = 'SecurityVulnerabilityChecks'
                                            new_rule['rule_name'] = f"{json_file['cveMetadata']['assignerShortName']}_vulnerability_{json_file['cveMetadata']['cveId'].replace('-', '_').lower()}_{constants.VENDORS[json_file['cveMetadata']['assignerOrgId']]['products'][product['product']]['os_name'].replace('-', '_').lower()}_rule"
                                            new_rule['rule_categories'] = ['CVE']
                                            new_rule['rule_friendly_name'] = f"{json_file['cveMetadata']['cveId']}"
                                            for description in json_file['containers']['cna']['descriptions']:#TODO: not mandatory field
                                                if description['lang'] == 'en':
                                                    new_rule['alert_description'] = helpers.TextTransform.oneline_to_multiline(description['value'])
                                            if json_file['containers']['cna'].get('metrics')is not None:
                                                for metric_type in json_file['containers']['cna']['metrics']:
                                                    cvss_version = [version for version in constants.CVSS_SUPPORTED_VERSIONS if version in list(metric_type.keys())]
                                                    if cvss_version:
                                                        new_rule['vendor_severity_rating'] = metric_type[cvss_version[0]]['baseSeverity']
                                                        new_rule['severity'] = constants.SEVERITY[metric_type[cvss_version[0]]['baseSeverity']]
                                                        path = f"{constants.VENDORS[json_file['cveMetadata']['assignerOrgId']]['ind_name']}/{metric_type[cvss_version[0]]['baseSeverity'].lower()}/"
                                                    #if 'cvssV3_1' in metric_type.keys():
                                                    #    new_rule['vendor_severity_rating'] = metric_type['cvssV3_1']['baseSeverity']
                                                    #    new_rule['severity'] = constants.SEVERITY[metric_type['cvssV3_1']['baseSeverity']]
                                                    #    path = f"{constants.VENDORS[json_file['cveMetadata']['assignerOrgId']]['ind_name']}/{metric_type['cvssV3_1']['baseSeverity'].lower()}/"
                                            else: #case no CVSS from MITRE, pick from NIST
                                                nist_data = download_nist_cve_data(json_file['cveMetadata']['cveId'])
                                                if nist_data[1] is None:
                                                    new_rule['vendor_severity_rating'] = nist_data[0]['vulnerabilities'][0]['cve']['metrics']['cvssMetricV31'][0]['cvssData']['baseSeverity']
                                                    new_rule['severity'] = constants.SEVERITY[nist_data[0]['vulnerabilities'][0]['cve']['metrics']['cvssMetricV31'][0]['cvssData']['baseSeverity']]
                                                    path = f"{constants.VENDORS[json_file['cveMetadata']['assignerOrgId']]['ind_name']}/{nist_data[0]['vulnerabilities'][0]['cve']['metrics']['cvssMetricV31'][0]['cvssData']['baseSeverity'].lower()}/"
                                                    print(f"Info from NIST: {new_rule['rule_name']}")
                                                else:
                                                    print(nist_data[1])
                                            new_rule['os_name'] = constants.VENDORS[json_file['cveMetadata']['assignerOrgId']]['products'][product['product']]['os_name']
                                            new_rule['device_category'] =  constants.VENDORS[json_file['cveMetadata']['assignerOrgId']]['device_category']
                                            if json_file['containers']['cna'].get('solutions') is not None:
                                                for solution in json_file['containers']['cna']['solutions']:
                                                    if solution['lang'] in ('en', 'eng'):
                                                        new_rule['base_remediation_text'] = solution['value']
                                            else:
                                                new_rule['base_remediation_text'] = 'Not provided by CVE'
                                            #Generation of versions affected fields
                                            new_rule['version_ranges'] = []
                                            for versions_affected in product['versions']:
                                                if versions_affected['status'] == 'affected':
                                                    versions_to_add = []
                                                    if len(versions_affected['version'].split(',')) > 1:
                                                        version_list = versions_affected['version'].split(',')
                                                        for list_item in version_list:
                                                            versions_to_add = []
                                                            if 'before' in list_item:
                                                                init_version = list_item.strip().split(' ')[0].replace('x','0')
                                                                end_version = list_item.split(' ')[-1]
                                                            elif any(x in list_item for x  in ['all versions of', 'All versions of']):
                                                                init_version = list_item.split('ll versions of ')[-1].strip().replace('x','0')
                                                                end_version = list_item.split('ll versions of ')[-1].strip().replace('x','9999')
                                                            elif list_item.endswith('.x'):
                                                                init_version = list_item.split(' ')[-1].strip().replace('x','0')
                                                                end_version = list_item.split(' ')[-1].strip().replace('x','9999')
                                                            else:
                                                                init_version = list_item
                                                                end_version = list_item
                                                            versions_to_add.append(f"{'.'.join(init_version.split('.')[:3])}")
                                                            versions_to_add.append(f"{'.'.join(end_version.split('.')[:3])}")
                                                            new_rule['version_ranges'].append(versions_to_add)
                                                    else:
                                                        if len(versions_affected['version'].split('.')) == 2:
                                                            init_version = f"{versions_affected['version']}.0"
                                                        else:
                                                            init_version = f"{'.'.join(versions_affected['version'].split('.')[:3])}"
                                                        if init_version.endswith(tuple_all_versions):
                                                            init_version_splitted = init_version.split('.')[:3]
                                                            init_version_splitted[-1] = '0'
                                                            init_version_joined = f"{'.'.join(init_version_splitted)}"
                                                            versions_to_add.append(f"{init_version_joined}".strip())
                                                        else:
                                                            versions_to_add.append(f"{init_version}".strip())
                                                        major_version =  ".".join(init_version.split(".")[:-1])
                                                        if versions_affected.get('lessThan') is not None : #Check if possible value 'All'
                                                            if versions_affected['lessThan'].endswith('*'):
                                                                versions_to_add.append(f"{major_version}.9999".strip())
                                                            else:
                                                                limit_version = versions_affected['lessThan'].split('.')[:3]
                                                                if '-h' not in limit_version[-1]:
                                                                    limit_version[-1] = str(max(int(limit_version[-1])-1,0))
                                                                elif '-h1' in limit_version[-1]:
                                                                    limit_version[-1] = limit_version[-1].replace('-h1','')
                                                                else:
                                                                    hotfix_version = limit_version[-1].split('-h')
                                                                    hotfix_version[-1] = str(int(hotfix_version[-1])-1) #TODO: what if h0?
                                                                    # TODO: Enable next line and delete 2nd line when support for  hotfix level in panos versioning
                                                                    #limit_version[-1] = '-h'.join(hotfix_version)
                                                                    limit_version[-1] = hotfix_version[0]
                                                                versions_to_add.append( '.'.join(limit_version).strip() )
                                                        elif versions_affected.get('lessThanOrEqual') is not None :
                                                            if versions_affected['lessThanOrEqual'] ==('All' or '*'):
                                                                versions_to_add.append(f"{major_version}.9999".strip())
                                                            else:
                                                                versions_to_add.append(f"{versions_affected['lessThanOrEqual']}".strip())
                                                        else: # Case one single version per affected entry
                                                            if init_version.endswith(tuple_all_versions):
                                                                init_version_splitted = init_version.split('.')[:3]
                                                                init_version_splitted[-1] = '9999'
                                                                init_version = f"{'.'.join(init_version_splitted)}"
                                                            versions_to_add.append(f"{init_version}".strip())
                                                        new_rule['version_ranges'].append(versions_to_add) #TODO: remove duplicates on version affected
                                            versions_no_dup  = []
                                            versions_no_dup.extend(x for x in new_rule['version_ranges'] if x not in versions_no_dup)
                                            # Prettifying the rule
                                            new_rule['version_ranges'] = list(versions_no_dup)
                                            if new_rule.get('vendor_severity_rating')is None:
                                                new_rule['vendor_severity_rating']= 'INFO'
                                                new_rule['alert_description'] = new_rule['alert_description']+' (This CVE has no CVSS severity assigned and is automatically generated as INFO alert )'
                                            # Writing the rule to a file
                                            if len(new_rule['version_ranges']) > 0 and set(constants.TEMPLATE_MANDATORY_FIELDS) <= new_rule.keys(): #Validation needed,maybe some missing fields
                                                if path:
                                                    helpers.YamlHelpers.write_relative_path(f"{current_dir}/../../rules/templatebased/cve/{path}{json_file['cveMetadata']['assignerShortName']}_vulnerability_{json_file['cveMetadata']['cveId'].replace('-', '_').lower()}_{constants.VENDORS[json_file['cveMetadata']['assignerOrgId']]['products'][product['product']]['os_name'].replace('-', '_').lower()}_rule.yaml", new_rule)
                                            elif len(new_rule['version_ranges']) != 0:
                                                print(f"failed to create a new rule from file {file}, missing fields ")
                except json.JSONDecodeError as exc:
                    print(exc)



def main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("--cve_repo", help="git repo actions")
    parser.add_argument("--cve_all", help="All CVEs")
    parser.add_argument("--cve_id", help="CVE ID filter")
    parser.add_argument("--cve_year", help="CVE year filter")
    parser.add_argument("--cve_vendor", type=str, 
                                        nargs="*" ,
                                        choices =  ['checkpoint', 'f5', 'panos', 'fortinet', 'cisco', 'bluecoat', 'gigamon', 'symantec','all'],help="one(or multiple) available vendors separated by space")
    args = parser.parse_args()
    if args.cve_repo:
        if args.cve_repo == 'update':
            git_repo_update()
        if args.cve_repo == 'clone':
            git_repo_clone()
    if (args.cve_all or args.cve_id or args.cve_year or args.cve_vendor):
        if (args.cve_vendor == 'all' or args.cve_vendor is None):
            args.cve_vendor = [constants.VENDORS[x]['ind_name'] for x in constants.VENDORS]
        rules_folder_skeleton(args.cve_vendor)
        generate_rules(args.cve_id, 
                       args.cve_year, 
                       args.cve_vendor)


if __name__ == '__main__':
    main()