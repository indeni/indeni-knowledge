#!/bin/bash

PYTHON_SCRIPT=$( dirname $0 )/change_ind_scripts.py
for file in $( find $1 -name '*.ind' ); do echo Current file: $file; /usr/bin/python $PYTHON_SCRIPT --input_file $file --output_file /tmp/change_ind_scripts_output; cp /tmp/change_ind_scripts_output $file; done

