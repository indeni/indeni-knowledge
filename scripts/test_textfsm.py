import os
import pprint
import sys

from parser_service.public.helper_methods import parse_data_as_object, parse_data_as_list

COMMAND_OUTPUT_PATH = ''
TEXTFSM_PATH = ''

if not os.path.isfile(COMMAND_OUTPUT_PATH):
    print('Command output file does not exist')
    sys.exit(1)
elif not os.path.isfile(TEXTFSM_PATH):
    print('TextFSM file does not exist')
    sys.exit(1)


with open(COMMAND_OUTPUT_PATH, 'r') as file_handle:
    raw_data = ''.join(file_handle.readlines())

parsed = ''
# noinspection PyBroadException
try:
    parsed = parse_data_as_object(raw_data, template_path=TEXTFSM_PATH)
except Exception:
    parsed = parse_data_as_list(raw_data, template_path=TEXTFSM_PATH)
finally:
    pprint.pprint(parsed)
