#!/usr/bin/python

import fnmatch
import os
import psutil
import sys
import datetime
import time
import subprocess
import re

#Process used to look for last server restart time
PROCESS_NAME = 'indeni-parser'

#Files wher elooking for errors
LOG_FILES_LIST = [
    '/usr/share/indeni/logs/indeni.log',
    '/usr/share/indeni-services/logs/parser.log',
    '/usr/share/indeni-services/logs/knowledge-catalog.log',
    '/usr/share/indeni-collector/logs/collector.log',
    '/usr/share/indeni-collector/logs/devices/*.log',
    '/usr/share/indeni/logs/rules/*.log',
]

MSG_TO_AVOID = [
    "Alert created",
    "https://127.0.0.1:9020",
]

#To store a summary of all checks
errors_summary = {}

#Count of log files parsed
log_files_parsed = 0

#Collect process starting time

def checkIfProcessRunning(processName):
    '''
    Check if there is any running process that contains the given name processName.
    '''
    #Iterate over the all the running process
    for proc in psutil.process_iter():
        try:
            # Check if process name contains the given name string.
            if processName.lower() == proc.name().lower():
                return True
        except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
            pass
    return False

def findProcessIdByName(processName):
    '''
    Get a list of all the PIDs of a all the running process whose name contains
    the given string processName
    '''
    listOfProcessObjects = []
    #Iterate over the all the running process
    for proc in psutil.process_iter():
        try:
            pinfo = proc.as_dict(attrs=['pid', 'name', 'create_time'])
            # Check if process name contains the given name string.
            if processName.lower() in pinfo['name'].lower() :
                listOfProcessObjects.append(pinfo)
        except (psutil.NoSuchProcess, psutil.AccessDenied , psutil.ZombieProcess) :
            pass
    return listOfProcessObjects

if checkIfProcessRunning(PROCESS_NAME):
    listOfProcessIds = findProcessIdByName(PROCESS_NAME)
    for elem in listOfProcessIds:
        processID = elem['pid']
    process = psutil.Process(processID)
    process_create_time = process.create_time()
    process_create_time_formated = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(process_create_time))
    print ('\nChecking log files since last server reboot: {}\n'.format(process_create_time_formated))
    #Opening file to print outputs
    script_log_file_path = '{}/{}_log_error_parser.log'.format(os.path.dirname(os.path.abspath(__file__)), datetime.datetime.now().strftime("%Y_%m_%d-%I_%M_%S_%p"))
    print ('\nOutput log file: {}\n'.format(script_log_file_path))
    with open(script_log_file_path,'w') as script_log_file:
        #Read log files and parse errors
        for file_name in LOG_FILES_LIST:
            path_and_file = os.path.split(file_name)
            for file in os.listdir(path_and_file[0]):
                if fnmatch.fnmatch(file, path_and_file[1]):
                    file_to_read = path_and_file[0]+'/'+file
                    with open(file_to_read, 'r') as f:
                        log_files_parsed +=1
                        header = file_to_read
                        errors_found = 0
                        for line in f:
                            if 'ERROR' in line and line.find('ERROR')==0 and not any(x in line for x in MSG_TO_AVOID) :
                                line_date_formated = ''
                                match = re.search(r'\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2},\d{3}', line)
                                if match is not None:
                                    line_date_formated = match.group().split(',')[0]
                                    if time.strptime(line_date_formated, "%Y-%m-%d %H:%M:%S") > time.strptime(process_create_time_formated, "%Y-%m-%d %H:%M:%S"):
                                        if errors_found == 0:
                                            script_log_file.write ('\n\n')
                                            script_log_file.write ('#'*len(header))
                                            script_log_file.write ('\n')
                                            script_log_file.write(header)
                                            script_log_file.write ('\n')
                                            script_log_file.write ('#'*len(header))
                                            script_log_file.write ('\n')
                                        script_log_file.write (line)
                                        errors_found +=1
                                match_2 = re.search(r'\d{2}\/\d{2}\/\d{2} \d{2}:\d{2}:\d{2},\d{3}', line)
                                if match_2 is not None:
                                    line_date_formated = match_2.group().split(',')[0]
                                    if time.strptime(line_date_formated, "%d/%m/%y %H:%M:%S") > time.strptime(process_create_time_formated, "%Y-%m-%d %H:%M:%S"):
                                        if errors_found == 0:
                                            script_log_file.write ('\n\n')
                                            script_log_file.write ('#'*len(header))
                                            script_log_file.write ('\n')
                                            script_log_file.write(header)
                                            script_log_file.write ('\n')
                                            script_log_file.write ('#'*len(header))
                                            script_log_file.write ('\n')
                                        script_log_file.write (line)
                                        errors_found +=1
                        if errors_found > 0:
                            errors_summary[file_to_read] = errors_found
                    f.close()
    script_log_file.close()
    headers = ['ERRORS', 'FILE']
    print ('{:>8} {:<100}'.format( *headers))
    for key,value in errors_summary.items():
        print ('{:>8} {:<100}'.format( str(value),key))
else:
    print('No indeni server process is running\n')
