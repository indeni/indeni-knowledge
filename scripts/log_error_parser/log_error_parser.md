# Log Error Parser

Log Error Parser is a Python script for dealing with Indeni Server errors related with Indeni Knowledge code. All errors collected are those with a timestap between last Indeni service restart and the time when the script is executed in order to provide only error related with the version of the code in use by the server.

## Installation

Copy the file **log_error_parser.py** which contains the code.

## Usage
```bash
python3 log_error_parser.py
```

After some minutes a log file is generated (**log_error_parser.log**) with all errors found separated in different sections with the source log filename as header.

Also a prompt otput is returned with a summary:
```bash
########## SUMMARY ###########

-------------------------
| LOG FILES PARSED: 531 |
-------------------------

FILENAME: /usr/share/indeni-services/logs/parser.log
ERRORS:   2

FILENAME: /usr/share/indeni-collector/logs/collector.log
ERRORS:   48

FILENAME: /usr/share/indeni-collector/logs/devices/10.11.94.173.log
ERRORS:   1641

FILENAME: /usr/share/indeni-collector/logs/devices/10.11.94.40.log
ERRORS:   5

########## SUMMARY ###########

```



## Version info

|Version|Date|Name|Email|
|-------|----|----|-----|
|0.1|22/07/2020|Jorge Riveiro|jorge.r@indeni.com|